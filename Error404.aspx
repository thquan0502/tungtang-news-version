﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error404.aspx.cs" Inherits="Error404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Không tìm thấy</title>
</head>
<body style="width: 100vw; height: 100vh;display: flex; justify-content: center; align-items: center;">
    <img src="/asset/img/error-404.png" style="display: block; margin: auto; max-height: 50%;"/>
</body>
</html>
