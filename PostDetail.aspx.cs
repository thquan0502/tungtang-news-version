﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class TinDangChiTiet : System.Web.UI.Page
{
    string idTinDang = "";
    static string SDT_ThanhVien = "";
    string Domain = "";
    string idThanhVien = "";
    string DuongDan = "";
    string metaTitle = "";
    string metaUrl = "";
    string metaHinhAnh = "";
    string metaDescription = "";
    string metaType = "";
    string metaTwtitle = "";
    string metaTwdes = "";
    string metaTwimg = "";
    string metaTwcard = "";
    string metaIgurl = "";

    protected string refCode = "";
    protected string refPhoneNumber = "";
    protected string refId = "";

    protected string contactPhone = "";
    protected string contactFullname = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlLink canonical = new HtmlLink();
        canonical.Href = HttpContext.Current.Request.Url.OriginalString;
        canonical.Attributes["rel"] = "canonical";
        Page.Header.Controls.Add(canonical);
        Page.Header.Controls.Add(canonical);
        HtmlLink alternate = new HtmlLink();
        alternate.Href = HttpContext.Current.Request.Url.OriginalString;
        alternate.Attributes["rel"] = "alternate";
        Page.Header.Controls.Add(alternate);
        if (!IsPostBack)
        {
            try
            {
                HttpContext context = HttpContext.Current;
                DuongDan = context.Items["Action"].ToString();
            }
            catch{ }
            if (DuongDan != "" && DuongDan != null)
            {
                this.initRefInfo();
                LoadTinDang();
                LoadTinCungDanhMuc();
                foreach (HtmlMeta metaTag in Page.Header.Controls.OfType<HtmlMeta>())
                {
                    if (metaTag.Name.Equals("ogtitle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTitle;
                    }
                    if (metaTag.Name.Equals("ogurl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaUrl;
                    }
                    if (metaTag.Name.Equals("ogimage", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaHinhAnh;
                    }
                    if (metaTag.Name.Equals("ogdes", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaDescription;
                    }
                    if (metaTag.Name.Equals("ogtype", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaType;
                    }
                    if (metaTag.Name.Equals("ogtwtitle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwtitle;
                    }
                    if (metaTag.Name.Equals("ogtwdes", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwdes;
                    }
                    if (metaTag.Name.Equals("ogtwimg", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwimg;
                    }
                    if (metaTag.Name.Equals("ogtwcard", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwcard;
                    }
                    if (metaTag.Name.Equals("igurl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaIgurl;
                    }
                }
                this.countViewed();
            }
        }
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
    }
    private void LoadTinDang()
    {
        string reqRefCode = Request.QueryString["RefCode"];
        string URL = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
        if (string.IsNullOrWhiteSpace(reqRefCode) && !string.IsNullOrWhiteSpace(this.refCode))
        {
            if (Request.QueryString.Count == 0)
            {
                URL += "?RefCode=" + this.refCode;
            }
            else
            {
                URL += "&RefCode=" + this.refCode;
            }
        }

        string sqlTinDang = "";
        sqlTinDang += @"
            SELECT 
	            TBPosts.idTinDang AS idTinDang,
	            TBPosts.SoLuotXem AS SoLuotXem,
	            TBPosts.idThanhVien AS idThanhVien,
	            TBCategories1.TenDanhMucCap1 AS TenDanhMucCap1,
	            TBPosts.idDanhMucCap1 AS idDanhMucCap1,
	            TBPosts.idDanhMucCap2 AS idDanhMucCap2,
	            TBPosts.TieuDe AS TieuDe,
	            TBPosts.idHuyen AS idHuyen,
	            TBPosts.idTinh AS idTinh,
	            TBPosts.idPhuongXa AS idPhuongXa,
	            TBPosts.NoiDung AS NoiDung,
	            TBCategories2.TenDanhMucCap2 AS TenDanhMucCap2,
	            TBPosts.DuongDan AS DuongDan,
	            TBPosts.TuGia AS TuGia,
	            TBPosts.MaTinDang AS MaTinDang,
	            TBPosts.NgayDang AS NgayDang,
	            ISNULL(TBPosts.Code, '') AS PostCode,
	            ISNULL(TBOwners.TenCuaHang, '') AS OwnerFullname,
	            ISNULL(TBOwners.SoDienThoai, '') AS OwnerPhone
            FROM tb_TinDang TBPosts 
	            INNER JOIN tb_ThanhVien TBOwners
		            ON TBPosts.idThanhVien = TBOwners.idThanhVien
	            LEFT JOIN tb_DanhMucCap1 TBCategories1 
		            ON TBPosts.idDanhMucCap1=TBCategories1.idDanhMucCap1 
	            LEFT JOIN tb_DanhMucCap2 TBCategories2 
		            ON TBPosts.idDanhMucCap2 = TBCategories2.idDanhMucCap2 
            WHERE ISNULL(TBPosts.isHetHan, 0) = 0 
	            AND ISNULL(TBPosts.isDuyet, 0) = 1 
	            AND ISNULL(TBPosts.IsDraft, 0)  = 0 
	            AND TBPosts.DuongDan='" + DuongDan + @"'
        ";
        
        DataTable tbTinDang = Connect.GetTable(sqlTinDang);
        if (tbTinDang.Rows.Count > 0)
        {
            this.idTinDang = tbTinDang.Rows[0]["idTinDang"].ToString();
            this.loadContactInfo(tbTinDang.Rows[0]["idThanhVien"].ToString());
            //SET Số lượt xem
            int SoLuotXem = int.Parse(KiemTraKhongNhap_LoadLen(tbTinDang.Rows[0]["SoLuotXem"].ToString()));
            Connect.Exec("update tb_TinDang SET SoLuotXem='" + (++SoLuotXem) + "' where idTinDang='" + idTinDang + "'");

            SDT_ThanhVien = StaticData.getField("tb_ThanhVien", "TenDangNhap", "idThanhVien", tbTinDang.Rows[0]["idThanhVien"].ToString());
            //Load link title
            string sTrangChu = "";
            string sDanhMucCap1 = "";
            string sDanhMucCap2 = "";
            sTrangChu = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='" + Domain + "'><span>Tin đăng</span></a></li>";

            string titleDanhMucCap1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDang.Rows[0]["TenDanhMucCap1"].ToString().ToLower()));
            string DMC1 = StaticData.getField("tb_DanhMucCap1", "LinkTenDanhMucCap1", "idDanhMucCap1", tbTinDang.Rows[0]["idDanhMucCap1"].ToString());
            string DMC2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", tbTinDang.Rows[0]["idDanhMucCap2"].ToString());
            sDanhMucCap1 = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu' ><a href='" + Domain + "/tin-dang/" + DMC1 + "'><span> " + tbTinDang.Rows[0]["TenDanhMucCap1"] + " </span></a></li>";
            sDanhMucCap2 = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu' ><a href='" + Domain + "/tin-dang/" + DMC1 + "?" + DMC2 + "'><span >" + StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", tbTinDang.Rows[0]["idDanhMucCap2"].ToString()) + "</span></a></li>";
            dvLinkTitle.InnerHtml = @"<ol class='breadcrumb _3WIL_EScB1tm02Oqj0vbu8'>" + sTrangChu + "" + sDanhMucCap1 + "" + sDanhMucCap2 + @"</ol>";
            //End load link title

            dvTitle.InnerHtml = tbTinDang.Rows[0]["TieuDe"].ToString();
            //Load slide
            string htmlSlide = "";
            string htmlDotSlide = "";
            string sqlHinhAnh = "select * from tb_HinhAnh where idTinDang='" + idTinDang + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            if (tbHinhAnh.Rows.Count > 0)
            {
               htmlSlide += "<ul>";
                for (int i = 0; i < tbHinhAnh.Rows.Count; i++)
                {

                    string urlHinhAnh = "";
                    urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[i]["UrlHinhAnh"].ToString();
                    metaHinhAnh = urlHinhAnh;
                    metaTwcard = urlHinhAnh;
                    metaTwimg = urlHinhAnh;
                    metaIgurl = urlHinhAnh;
                    if (i == 0)
                    {

                    }
                    if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                    System.Drawing.Bitmap img = new System.Drawing.Bitmap(Server.MapPath(urlHinhAnh));

                    string contentDetail = "";
                    if (!string.IsNullOrWhiteSpace(this.contactFullname))
                    {
                        contentDetail += "<p class='slide-detail-name color-yellow' style='font-weight: bold;'>"+ this.contactFullname + "</p>";
                    }
                    contentDetail += "<p class='slide-detail-name' style='font-weight: bold;'><a href='tel:"+ this.contactPhone + "' class='color-yellow'>"+ this.contactPhone + "</a></p>";


                    htmlSlide += @"
 <li style='position: relative;'>
<img src='" + urlHinhAnh + @"'  style='width:100%;height:300px;box-shadow: -5px -3px 15px 1px #6363637d;object-fit:cover;'
alt='Hinh" + tbHinhAnh.Rows[i]["idHinhAnh"].ToString() + @"' 
title='Hinh" + tbHinhAnh.Rows[i]["idHinhAnh"].ToString() + @"' 
 />
<div class='slide-detail'>
"+ contentDetail + @"
</div>
</li>                    
                    ";


                     htmlDotSlide += " <a   title='Hinh" + tbHinhAnh.Rows[i]["idHinhAnh"].ToString() + @"'><span>" + (i + 1).ToString() + "</span></a>";
                }
                htmlSlide += "</ul>";
            }
            else
            {
                string urlHinhAnh = Domain + "/images/icons/noimage.png";
                htmlSlide += @"
 <li>
<img src='" + urlHinhAnh + @"' style='width:100%;height:auto;box-shadow: -5px -3px 15px 1px #6363637d;object-fit:contain;'
 />

</li>                    
                    ";
            }


            dvSlide.InnerHtml = htmlSlide;
            dvDotSlide.InnerHtml = htmlDotSlide;
            //End load slide
            string TenHuyen1 = StaticData.getField("District", "Ten", "Id", tbTinDang.Rows[0]["idHuyen"].ToString());
            string idTinh1 = tbTinDang.Rows[0]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            string idPhuongXa = tbTinDang.Rows[0]["idPhuongXa"].ToString();
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa);

            Title = tbTinDang.Rows[0]["TieuDe"].ToString();
            //ogtitle.Attributes["content"] = Title;
            metaTwdes = tbTinDang.Rows[0]["NoiDung"].ToString();
            metaTwtitle = tbTinDang.Rows[0]["TieuDe"].ToString();
            metaTitle = tbTinDang.Rows[0]["TieuDe"].ToString();
            metaUrl = HttpContext.Current.Request.Url.OriginalString;
            metaDescription = tbTinDang.Rows[0]["NoiDung"].ToString();
            metaType = tbTinDang.Rows[0]["TieuDe"].ToString();
            MetaDescription = tbTinDang.Rows[0]["NoiDung"].ToString();   
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script> var imgs = document.querySelectorAll('img');imgs.forEach(img => {img.alt = '" + metaTitle + "';});</script>");
            
			//Script
            string metaU = HttpContext.Current.Request.Url.OriginalString;
            string name = "name";
            string mainEntityOfPage = "mainEntityOfPage";
            string idd = "id";
            string context = "@context";
            string type = "@type";
            string iddd = "@id";
            string hl = "headline";
            string d = "description";
            string ig = "image";
            string ul = "url";
            string con = "@context";
            string t = "@type";
            string dp = "datePublished";
            string dm = "dateModified";
            string au = "author";
            string na = "name";
            string c = "@context";
            string tp = "@type";
            string pb = "publisher";
            string lg = "logo";
            string width = "width";
            string he = "height";
            string hea = "application/ld+json";
            string urlchu = "https://tungtang.com.vn";
            string paa = "WebPage";
            string imgo = "ImageObject";
            string per = "Person";
            string og = "Organization";
            string news = "NewsArticle";
            string chema = "https://schema.org";
            string imoc = "https://tungtang.com.vn/images/icons/logo.png";
            string Crea = "CreativeWorkSeries";
            Page.Header.Controls.AddAt(1,
                       new LiteralControl(
                           "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                       )
                       );
            //End script
            //	BreadcrumbList
            string Brea = "BreadcrumbList";
            string TenDMC1 = tbTinDang.Rows[0]["TenDanhMucCap1"].ToString();
            string TenDMC2 = tbTinDang.Rows[0]["TenDanhMucCap2"].ToString();
            string DuongDanDMC1 = Domain + "/tin-dang/1-" + tbTinDang.Rows[0]["idDanhMucCap1"] + "-" + titleDanhMucCap1;
            string DuongDanDMC2 = Domain + "/tin-dang/1-" + tbTinDang.Rows[0]["idDanhMucCap1"] + "-" + titleDanhMucCap1 + "?C2=" + tbTinDang.Rows[0]["idDanhMucCap2"];
            string UrlPost = Domain + "/tdct/" + tbTinDang.Rows[0]["DuongDan"];
            string item = "itemListElement";
            Page.Header.Controls.AddAt(7,
                      new LiteralControl(
                           "<script type =" + '"' + hea + '"' + "> { " + '"' + con + '"' + ":" + '"' + chema + '"' + "," + '"' + t + '"' + ":" + '"' + Brea + '"' + "," + '"' + item + '"' + ":[{" + '"' + t + '"' + ": " + '"' + "ListItem" + '"' + "," + '"' + "position" + '"' + ":" + 1 + "," + '"' + "item" + '"' + ":{" + '"' + "@id" + '"' + ":" + '"' + urlchu + '"' + "," + '"' + name + '"' + ": " + '"' + "Tung Tăng Rao Vặt" + '"' + "}}" + "," + "{" + '"' + t + '"' + ": " + '"' + "ListItem" + '"' + "," + '"' + "position" + '"' + ":" + 2 + "," + '"' + "item" + '"' + ":{ " + '"' + "@id" + '"' + ":" + '"' + DuongDanDMC1 + '"' + "," + '"' + name + '"' + ": " + '"' + TenDMC1 + '"' + "}}" + ", " + "{ " + '"' + t + '"' + ": " + '"' + "ListItem" + '"' + ", " + '"' + "position" + '"' + ":" + 3 + "," + '"' + "item" + '"' + ":{ " + '"' + "@id" + '"' + ":" + '"' + DuongDanDMC2 + '"' + "," + '"' + name + '"' + ": " + '"' + TenDMC2 + '"' + "}}"+ ", " + "{ " + '"' + t + '"' + ": " + '"' + "ListItem" + '"' + ", " + '"' + "position" + '"' + ":" + '"' + metaTitle + '"' + ", " + '"' + "item" + '"' + ":{ " + '"' + "@id" + '"' + ":" + '"' + UrlPost + '"' + "," + '"' + name + '"' + ": " + '"' + metaTitle + '"' + "}}  ]}</script>"
                       )
                       );

            string Chuoi_DiaDiem = "";
            if (TenTinh1 == "")
                Chuoi_DiaDiem = "Toàn quốc";
            else
            {
                if (TenHuyen1 == "")
                    Chuoi_DiaDiem = TenTinh1;
                else
                {
                    if (TenPhuongXa == "")
                        Chuoi_DiaDiem = TenHuyen1 + " - " + TenTinh1;
                    else
                        Chuoi_DiaDiem = TenPhuongXa + " - " + TenHuyen1 + " - " + TenTinh1;
                }
            }
            dvLoaiThue.InnerHtml = "<i class='fa fa-map-marker' style='padding:0px 14px 0px 6px;font-size: 20px;'></i> " + Chuoi_DiaDiem;

            string sGia = "";
            if (tbTinDang.Rows[0]["TuGia"].ToString() != "")
                sGia = "<span class='Gia'>" + double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + " VND" + "</span>";

                sGia = "<span class='Gia'>" + double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + " VND" + "</span>";

            dvGia.InnerHtml = sGia;
            dvNgayDang.InnerHtml = "<p style='font-size: 13px; color: #a9a9a9;' >"+DateTime.Parse(tbTinDang.Rows[0]["NgayDang"].ToString()).ToString("dd/MM/yyyy hh:MM tt")+"</p>";
            dvMaTinDang.InnerHtml = "<i class='fa fa-indent' style='padding:1px 4px 0px 2px;font-size: 20px;'></i> <strong>" + tbTinDang.Rows[0]["PostCode"].ToString().Trim() + "</strong>";

            //dvNgayDang.InnerHtml = "<i class='fa fa-calendar' style='padding: 0px 10px 0px 2px;font-size: 20px;'></i> Đăng từ " + DateTime.Parse(tbTinDang.Rows[0]["NgayDang"].ToString()).ToString("dd-MM-yyyy");

            
            dvNoiDung.InnerHtml = tbTinDang.Rows[0]["NoiDung"].ToString();
            dvChiaSeBaiViet.InnerHtml = @"

                                          <a style='padding: 0 5px' target='_blank' href='http://www.facebook.com/share.php?u=" + URL + @"' title='Chia sẻ lên Facebook'><img src='https://cdn4.iconfinder.com/data/icons/social-media-icons-the-circle-set/48/facebook_circle-512.png' style='border:0; height:40px;' /></a>
                                          <a style='padding: 0 5px' target='_blank' href='https://www.facebook.com/v2.9/dialog/send?app_id=123456789&channel_url=" + URL + @"&app_id=123456789' title='Chia sẻ qua Messenger'><img src='https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Facebook_Messenger-512.png' style='border:0; height:40px;' /></a>
                                          <a class='zalo-share-button'  style='padding: 0 5px;cursor:pointer;'  data-href='" + URL + @"' data-oaid='579745863508352884' data-layout='2' data-color='blue' data-customize='true'><img src='https://seeklogo.com/images/Z/zalo-logo-B0A0B2B326-seeklogo.com.png' style='border:0; height:40px;' />
                                          
                                          <a style='padding: 0 5px;cursor:pointer;' onclick='CopyLinkURL()' title='Sao chép liên kết'><img src='https://cdn4.iconfinder.com/data/icons/web-links/512/41-512.png' style='border:0; height:40px;' /></a>
                                          <span id='spSaoChepThanhCong' style='display: none;width: max-content;padding: 4px 15px;background-color: #c7c7c7;color:white;'>Đã sao chép</span>
                                           ";
            myLinkCopy.Value = URL;
        }
        else
        {
            Response.Redirect("~/Error404.aspx");
        }
		 string sqltableAnh = "select * from tb_BannerTinDang where idBannerTinDang=3";
        DataTable tbHinhAnhTinDang = Connect.GetTable(sqltableAnh);
        string srcHinhAnh = StaticData.getField("tb_BannerTinDang", "'../' + LinkAnh", "idBannerTinDang", "3");
        
        if (tbHinhAnhTinDang.Rows.Count > 0)
        {
            for (int i = 0; i < tbHinhAnhTinDang.Rows.Count; i++)
            {
                string LinkAnh = tbHinhAnhTinDang.Rows[i]["Url"].ToString();
                AnhTinDang.InnerHtml = @"
                               <a href='" + LinkAnh + "' target='_blank'>  <img style='width: 100%;' alt='Tung Tăng' src = '" + srcHinhAnh + "'/> </a>";
            }
        }
    }
    private void LoadTinCungDanhMuc()
    {
        string idDanhMucCap1 = StaticData.getField("tb_TinDang", "idDanhMucCap1", "idTinDang", idTinDang);
        string sqlTinDangCungDanhMuc = "select top 15 * from tb_TinDang where isnull(isHetHan,'False')='False' and isnull(isDuyet,'False')='True' and idTinDang!='" + idTinDang + "' and idDanhMucCap1='" + idDanhMucCap1 + "' order by idTinDang desc";
        DataTable tbTinDangCungDanhMuc = Connect.GetTable(sqlTinDangCungDanhMuc);
        string html = "";
        for (int i = 0; i < tbTinDangCungDanhMuc.Rows.Count; i++)
        {
            string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDangCungDanhMuc.Rows[i]["TieuDe"].ToString().Trim()));
            string url = Domain + "/tdct/" + tbTinDangCungDanhMuc.Rows[i]["DuongDan"].ToString();
            //
            string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + tbTinDangCungDanhMuc.Rows[i]["idTinDang"] + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            string urlHinhAnh = "";
            if (tbHinhAnh.Rows.Count > 0)
            {
                urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"].ToString();
                if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                    urlHinhAnh = Domain + "/images/icons/noimage.png";
            }
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            //
             string TenHuyen1 = StaticData.getField("District", "Ten", "Id", tbTinDangCungDanhMuc.Rows[i]["idHuyen"].ToString());
            string idTinh1 = tbTinDangCungDanhMuc.Rows[i]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            string idPhuongXa1 = tbTinDangCungDanhMuc.Rows[i]["idPhuongXa"].ToString();
            string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
            string chuoi_DiaDiem = "";
            decimal Gia = decimal.Parse(KiemTraKhongNhap_LoadLen(tbTinDangCungDanhMuc.Rows[i]["TuGia"].ToString()));
            //  int LuotXem = int.Parse(tbTinDang.Rows[i]["SoLuotXem"].ToString()) + 100;
            chuoi_DiaDiem = TenTinh1;
            if (TenTinh1 == "")
                chuoi_DiaDiem = "Toàn quốc";
            string LoaiTinDang = "Cần bán";
            if (tbTinDangCungDanhMuc.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                LoaiTinDang = "Cần mua";

            html += @"
                            <li style = 'height: 250px;' >
 
                             <a href = '" + url + @"' title = '" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"' >   
                                <span><img alt = '" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"' src='" + urlHinhAnh + @"' style='display: block;width: 100%;height: 130px;object-fit: cover;' /></span>
                                <span class='title-TinDangLienQuan'>" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"</span>
                                <span class='title-TinDangLienQuan' style='color: red;font-weight: 600;'>" + Gia.ToString("N0") + 'đ' + @"</span>
                                <span class='title-TinDangLienQuan' style='color: #666'>" + LoaiTinDang + ' ' + '|' + ' ' + TinhThoiGianLucDang(DateTime.Parse(tbTinDangCungDanhMuc.Rows[i]["NgayDayLenTop"].ToString())) + ' ' + '|' + ' ' + chuoi_DiaDiem + '|' + " <i class='fa fa-eye'></i> " + tbTinDangCungDanhMuc.Rows[i]["SoLuotXem"] + @" </span>      
                            </a>
                        
                        </li>";

        }
        ulTinDang.InnerHtml = html;
    }
    string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }
    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    protected void loadContactInfo(string  ownerId)
    {
        string contactId = !string.IsNullOrEmpty(this.refId) ? this.refId : ownerId;
        string sql1 = @"
            SELECT 
	            TBAccounts.idThanhVien AS AccountId,
                ISNULL(TBAccounts.TenCuaHang, '') AS AccountFullname,
                ISNULL(TBAccounts.SoDienThoai, '') AS AccountPhone,
                ISNULL(TBAccounts.Email, '') AS AccountEmail,
                ISNULL(TBAccounts.TenDangNhap, '') AS AccountUsername,
	            ISNULL(TBAccounts.LinkAnh, '') AS AccountThumb,
	            ISNULL(TBCities.Ten, '') AS AccountCity
            FROM tb_ThanhVien TBAccounts
	            LEFT JOIN City TBCities
		            ON TBCities.id = TBAccounts.idTinh
            WHERE TBAccounts.idThanhVien = '" + contactId + @"'
        ";
        DataTable table1 = Connect.GetTable(sql1);
        if (table1.Rows.Count > 0)
        {
            dvAnhDaiDien.InnerHtml = "<img class='img-circle KJuQ6UgNHKTybxph5jqh- _38Q-ZJPwiUyqL82cU4XpN6 ' src='" + Utilities.Account.getLinkThumbWithPath(table1.Rows[0]["AccountThumb"].ToString()) + "?width=160'/>";
            dvHoTen.InnerHtml = "<b>" + table1.Rows[0]["AccountFullname"] + "</b>";
            dvSoDienThoai.InnerHtml = @"<a href='tel:" + table1.Rows[0]["AccountPhone"].ToString() + @"'>
                                                <div class='sc-gisBJw ikcTlv'>
                                                    <div class='sc-kjoXOD icTLCU'>
                                                        <div class='sc-cHGsZl iTxwzU'>
                                                            <span>
                                                                <img class='sc-TOsTZ bnAkpx' src='../images/icons/phone.png' /></span>
                                                            <span style='font-weight: bold;font-size: 16px;'>" + table1.Rows[0]["AccountPhone"].ToString() + @"</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>";
            dvEmail.InnerHtml = table1.Rows[0]["AccountEmail"].ToString();
            dvDiaChi.InnerHtml = table1.Rows[0]["AccountCity"].ToString();
            btnXemTrang.Attributes.Add("href", "/thong-tin-ca-nhan/ttcn-" + table1.Rows[0]["AccountUsername"].ToString() + "?Tab=TDN");

            this.contactPhone = table1.Rows[0]["AccountPhone"].ToString();
            this.contactFullname = table1.Rows[0]["AccountFullname"].ToString();
        }
    }
    private void initRefInfo()
    {
        string refCode = Request.QueryString["RefCode"];
        if(refCode != null)
        {
            refCode = refCode.Trim();
            this.refCode = refCode;

            HttpCookie cookie = new HttpCookie("TungTang_RefCode", refCode);
            cookie.Expires = DateTime.Now.AddDays(60);
            Response.Cookies.Add(cookie);
        }
        else
        {
            HttpCookie cookie = Request.Cookies["TungTang_RefCode"];
            if(cookie != null)
            {
                this.refCode = Request.Cookies["TungTang_RefCode"].Value;
            }
        }
        if(this.refCode != "")
        {
            string sql1 = "";
            sql1 = sql1 + " SELECT ";
            sql1 = sql1 + " 	tb_ThanhVien.SoDienThoai AS PhoneNumber, ";
            sql1 = sql1 + " 	tb_ThanhVien.idThanhVien AS AccountId ";
            sql1 = sql1 + " FROM tb_ThanhVien ";
            sql1 = sql1 + " WHERE 1 = 1 ";
            sql1 = sql1 + " 	AND tb_ThanhVien.RefCode = '" + this.refCode + "'";
            DataTable table1 = Connect.GetTable(sql1);
            if (table1.Rows.Count > 0)
            {
                this.refPhoneNumber = table1.Rows[0]["PhoneNumber"].ToString();
                this.refId = table1.Rows[0]["AccountId"].ToString();
            }
        }
    }
    private void countViewed()
    {
        if(this.refCode == "")
        {
            return;
        }
        Utilities.ViewedManager.hasNewViewedByRefCode(refCode);
    }
}