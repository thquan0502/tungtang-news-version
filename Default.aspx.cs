﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    string Domain = "";
    string metaTitle = "";
    string metaDesciption = "";
    string metaUrl = "";
    string metaType = "";
    string metaTwtitle = "";
    string metaTwdes = "";
    int Pages = 1;
    int MaxPage = 0;
    int PageSize = 20;
    // string idTinDang = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            metaUrl = HttpContext.Current.Request.Url.OriginalString;


            Domain = "";
        }
        if (Request.Cookies["TungTang_Login"] != null)
        {
            if (Request.Cookies["TungTang_Login"].Value.Trim() != "")
            {
                dvSubBanner.Visible = false;
				// ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Chào mừng bạn đến với Tung Tăng !')", true);
            }
        }
        if (!IsPostBack)
        {

            //foreach (HtmlMeta metatag in Page.Header.Controls.OfType<HtmlMeta>())
            //{
            //    // Response.Redirect("https://google.com");
            //    if (metatag.Name.Equals("ogtitle", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        metatag.Content = metaTitle;
            //    }
            //    if (metatag.Name.Equals("ogdes", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        metatag.Content = metaDesciption;
            //    }
            //    if (metatag.Name.Equals("ogurl", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        metatag.Content = metaUrl;
            //    }
            //    if (metatag.Name.Equals("ogtype", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        metatag.Content = metaType;
            //    }
            //    if (metatag.Name.Equals("ogtwtitle", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        metatag.Content = metaTwtitle;
            //    }
            //    if (metatag.Name.Equals("ogtwdes", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        metatag.Content = metaTwdes;
            //    }

            //}
            HtmlLink canonical = new HtmlLink();
            canonical.Href = HttpContext.Current.Request.Url.OriginalString;
            canonical.Attributes["rel"] = "canonical";

            Page.Header.Controls.Add(canonical); HtmlLink alternate = new HtmlLink();
            alternate.Href = HttpContext.Current.Request.Url.OriginalString;
            alternate.Attributes["rel"] = "alternate";
            Page.Header.Controls.Add(alternate);

            DataTable tableTag = Connect.GetTable(@"select    Title
                      ,Desciption
                     from Seo where 1=1");
            if (tableTag.Rows.Count > 0)
            {
                Title = tableTag.Rows[0]["Title"].ToString();
                MetaDescription = tableTag.Rows[0]["Desciption"].ToString();
                //metaTitle = tableTag.Rows[0]["Title"].ToString();
                //metaDesciption = tableTag.Rows[0]["Desciption"].ToString();
                //metaType = tableTag.Rows[0]["Title"].ToString();
                //metaTwtitle = tableTag.Rows[0]["Title"].ToString();
                //metaTwdes = tableTag.Rows[0]["Desciption"].ToString();
                //Response.Redirect("https://google.com"); 
				
				 string metaU = HttpContext.Current.Request.Url.OriginalString;
                string name = "name";
                string mainEntityOfPage = "mainEntityOfPage";
                string idd = "id";
                string context = "@context";
                string type = "@type";
                string iddd = "@id";
                string hl = "headline";
                string d = "description";
                string ig = "image";
                string ul = "url";
                string con = "@context";
                string t = "@type";
                string dp = "datePublished";
                string dm = "dateModified";
                string au = "author";
                string na = "name";
                string c = "@context";
                string tp = "@type";
                string pb = "publisher";
                string lg = "logo";
                string width = "width";
                string he = "height";
                string hea = "application/ld+json";
                string urlchu = "https://tungtang.com.vn";
                string paa = "WebPage";
                string imgo = "ImageObject";
                string per = "Person";
                string og = "Organization";
                string news = "NewsArticle";
                string chema = "https://schema.org";
                string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                string Crea = "CreativeWorkSeries";
                Page.Header.Controls.AddAt(7,
                           new LiteralControl(
                               "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + tableTag.Rows[0]["Title"].ToString() +'"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": " + '"' + "5" + '"' + "," + '"' + "bestRating" + '"' + ": " + '"' + "5" + '"' + ", " + '"' + "ratingCount" + '"' + ": " + '"' + "7" + '"' + "}}</script> "
                           )
                           );
				
				
				

            }
          
            LoadDanhMuc();
            Load_BannerTrangChu();
			LoadTinDang();
            LoadLienQuan();
			LoadMoTa();
        }
    }
    void Load_BannerTrangChu()
    {
        string sql = "select * from tb_BannerTrangChu";
        DataTable tb = Connect.GetTable(sql);
        string html = @"
            <li><a href='"+ tb.Rows[0]["Url"] + @"'><img src='../" + tb.Rows[0]["LinkAnh"].ToString() + @"' alt='Hinh1' title='Hinh1' id='wows1_0'/></a></li>
            <li><a href='" + tb.Rows[1]["Url"] + @"'><img src='../" + tb.Rows[1]["LinkAnh"].ToString() + @"' alt='Hinh1' title='Hinh1' id='wows1_0'/></a></li>
            <li><a href='" + tb.Rows[2]["Url"] + @"'><img src='../" + tb.Rows[2]["LinkAnh"].ToString() + @"' alt='Hinh1' title='Hinh1' id='wows1_0'/></a></li>";
        nope.InnerHtml = html;
    }
    private void LoadBannerTrangChu()
    {
        string sqlBNTC = "select * from tb_BannerTrangChu";
        DataTable tbBNTC = Connect.GetTable(sqlBNTC);
        if (tbBNTC.Rows.Count > 0)
        {
            string html = ""; 
            html += @"<div aria-hidden='true' data-swipeable='true' style='width: 100%; flex-shrink: 0; overflow: auto;'>
                            <div>
                                <a itemprop='url' href='" + tbBNTC.Rows[0]["Url"].ToString().Trim() + @"'>
                                    <img class='img-responsive Image-fDMyDA kGHnsd' style='box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);' src='../" + tbBNTC.Rows[0]["LinkAnh"].ToString().Trim() + @"'></a>
                            </div>
                        </div> ";
            //dvBannerTrangChu.InnerHtml = html;
        }
    }
												  
	 private void LoadMoTa()
    {
        Motahomepage.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "MOTAHOMEPAGE");
    }
												  
    private void LoadDanhMuc()
    {
        string htmlDanhMuc = "";
        string sqlDanhMucCap1 = "select * from tb_DanhMucCap1 order by SoThuTu asc";
        DataTable tbDanhMucCap1 = Connect.GetTable(sqlDanhMucCap1);
        for (int i = 0; i < tbDanhMucCap1.Rows.Count; i++)
        {
            string TieuDe1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString().Trim()));
            string url1 = "/tin-dang/" + TieuDe1;

            if (tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() == "14" || tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() == "16")//Bất động sản và Xe cộ 
                htmlDanhMuc += @"<li class='_22xdSDGM_qM0I0v1S_9O4N'>";
            else
                htmlDanhMuc += @"<li>";
            htmlDanhMuc += @"   <a href='" + url1 + @"'>
                                    <img itemprop='image' class='img-responsive' src='" + tbDanhMucCap1.Rows[i]["LinkAnh"] + @"' alt='" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"] + @"' title='" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"] + @"'>
                                    <div class='_8HbYpNHde0mRwbwonofVj'><span>" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"] + @"</span></div>
                                </a>
                            </li>";  
        }
        ulKhamPhaDanhMuc.InnerHtml = htmlDanhMuc;
    }

    private void LoadTinDang()
    {

        string sqlTinDang = "select top 75 * from tb_TinDang where isnull(isHetHan,'False')='False' and isnull(isDuyet,'False')='True'  and Ishot='True'  order by idTinDang desc";
        DataTable tbTinDang = Connect.GetTable(sqlTinDang);
        string html = "";
        for (int i = 0; i < tbTinDang.Rows.Count; i++)
        {
            // string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDang.Rows[i]["TieuDe"].ToString().Trim()));
            string url = Domain + "/tdct/" + tbTinDang.Rows[i]["DuongDan"].ToString();
            //
            string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + tbTinDang.Rows[i]["idTinDang"] + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            string urlHinhAnh = "";
            if (tbHinhAnh.Rows.Count > 0)
            {
                urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"].ToString();
                if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                    urlHinhAnh = Domain + "/images/icons/noimage.png";
            }
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            //
            string TenHuyen1 = StaticData.getField("District", "Ten", "Id", tbTinDang.Rows[i]["idHuyen"].ToString());
            string idTinh1 = tbTinDang.Rows[i]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            string idPhuongXa1 = tbTinDang.Rows[i]["idPhuongXa"].ToString();
            string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
            string chuoi_DiaDiem = "";
            decimal Gia = decimal.Parse(KiemTraKhongNhap_LoadLen(tbTinDang.Rows[i]["TuGia"].ToString()));
            chuoi_DiaDiem = TenTinh1;
            if (TenTinh1 == "")
                chuoi_DiaDiem = "Toàn quốc";
            string LoaiTinDang = "Cần bán";
            if (tbTinDang.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                LoaiTinDang = "Cần mua";
            html += @" 
                            
                            <li style='height: 250px;'>
                            <a href='" + url + @"' title='" + tbTinDang.Rows[i]["TieuDe"] + @"'>
                                <div class='ctRibbonAd' style='background-color: rgb(245, 154, 0); border-color: rgb(245, 154, 0); color: rgb(255, 255, 255);z-index:0;'>PRO</div>
                                <span><img alt='" + tbTinDang.Rows[i]["TieuDe"] + @"' src='" + urlHinhAnh + @"' style='display: block;width: 100%;height: 130px;object-fit: cover;' /></span>
                                <span class='title-TinDangLienQuan'>" + tbTinDang.Rows[i]["TieuDe"] + @"</span>
                                <span class='title-TinDangLienQuan' style='color: red;font-weight: 600;'>" + Gia.ToString("N0") +'đ'+ @"</span>
                                <span class='title-TinDangLienQuan' style='color: #666'>" + LoaiTinDang + ' '+'|' + ' '  + TinhThoiGianLucDang(DateTime.Parse(tbTinDang.Rows[i]["NgayDayLenTop"].ToString())) + ' ' + '|' + ' ' + chuoi_DiaDiem + '|' + " <i class='fa fa-eye'></i> " + tbTinDang.Rows[i]["SoLuotXem"] + @"</span>      
                            </a>
                        
                        </li>";
        }
        ulTinDang.InnerHtml = html;

    }
    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }
												  
    public void LoadLienQuan()
    {
        string SapXepTheo = " TD.NgayDang desc ";
        string GiaTu = "", GiaDen = "";
        string CanXem = "";


        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  ( order by " + SapXepTheo + @"  )AS RowNumber
	              ,TD.*
                  from tb_TinTuc TD 
            where  KichHoat='1' ";


        sql += ") as tb1 ";
        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += "WHERE RowNumber BETWEEN (" + Pages + " - 1) * " + PageSize + " + 1 AND (((" + Pages + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        DataTable table = Connect.GetTable(sql);
        // metaUrl = HttpContext.Current.Request.Url.OriginalString;
        string html = "";
        if (table.Rows.Count > 0)
        {
            //   html += @"<div><table style='width:100%;'><tr>";
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string TieuDe = table.Rows[i]["TieuDe"].ToString();
                string LinkAnh = table.Rows[i]["AnhDaiDien"].ToString().Trim();
                //string Desciption = table.Rows[i]["MoTaNgan"].ToString().Trim();
                string NgayDang = table.Rows[i]["NgayDang"].ToString().Trim();
                if (LinkAnh != "")
                {
                    LinkAnh = "../" + LinkAnh;
                    // metaHinhAnh = LinkAnh;
                }
                if (TieuDe.Length > 1000)
                {
                    TieuDe = TieuDe.Substring(0, 1000) + "...";
                }
                //if (Desciption.Length > 1000)
                //{
                //    TieuDe = TieuDe.Substring(0, 1000) + "...";
                //}


                //                html += @"  <div class='item'>
                //                            <a href='' >
                //                                <span><img src='" + LinkAnh + @"' /></span>
                //                                <span class='title-TinDangLienQuan'>" + TieuDe + @"</span>
                //                              
                //                            </a>
                //                        </div> ";

                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim()));
                string urlTinDangChiTiet = Domain + "/blogtt/" + table.Rows[i]["DuongDan"].ToString();
             //   metaUrl = HttpContext.Current.Request.Url.OriginalString;
                if (i == 0)
                {

                }

                html += @"  <div class='item' style='margin-bottom: 14px;'>
                            <a href='" + urlTinDangChiTiet + @"' >
                                <span><img alt='" + table.Rows[i]["TieuDe"].ToString() + @" - Tung Tăng' src='" + LinkAnh + @"' /></span>
                               
                                <strong><span class='title-TinDangLienQuan'>" + TieuDe + @"</span></strong>
                               
                                <span class='title-TinDangLienQuan' style='color: #fe9900;'><b>Đọc Tiếp</b></span>
                                <span class='cost-TinDangLienQuan' style='color: red;display:none;'></span>
                            </a>
                        </div> ";

                //                html += @"
                //                                                             
                //                    
                //                               <td>
                //                              
                //                                    <div style='padding-left:1%'>
                //                                        <a href='' >
                //                                            <span><img src='" + LinkAnh + @"' style='width: 120px;height:120px'></span>
                //                                           
                //                                          <br /> <span >" + TieuDe + @"</span>
                //                                        </a>
                //                                    </div>
                //                               
                //                     </td>
                //              
                //             
                //                ";
            }
            // html += @"</tr><table></div>";




        }

        abc.InnerHtml = html;
			
			 string sqltags = "select * from Toptrend";
        DataTable tables = Connect.GetTable(sqltags);
        string htmltags = "";
        if (tables.Rows.Count > 0)
        {
            //   html += @"<div><table style='width:100%;'><tr>";
            for (int i = 0; i < tables.Rows.Count; i++)
            {
                //string domain = "https://tungtang.com.vn/tag/";
                string Titlet = tables.Rows[i]["DuongDan"].ToString();
                // Response.Redirect("https://google.com");
                string Titletags = tables.Rows[i]["Title"].ToString().Trim();

                if (i == 0)
                {

                }

                htmltags += @"  
                            <div style='white-space: nowrap;display: inline-block;height: auto;'>
                                 <span style='font-size:10px;'>●</span>
                                        <h2 style='font-size:14px;display:inline-block;line-height: 1.42857;font-weight: 400;margin-top:0px; margin-bottom:0px;'>
                                        <a target='" + "_blank"+"' href='" + Titlet + @"' style='color: #ffba00;' >
                                            " + Titletags + @"
                                        </a>
                                        </h2>
              
                            </div>
                       ";


                //                html += @"
                //                                                             
                //                    
                //                               <td>
                //                              
                //                                    <div style='padding-left:1%'>
                //                                        <a href='' >
                //                                            <span><img src='" + LinkAnh + @"' style='width: 120px;height:120px'></span>
                //                                           
                //                                          <br /> <span >" + TieuDe + @"</span>
                //                                        </a>
                //                                    </div>
                //                               
                //                     </td>
                //              
                //             
                //                ";
            }
            // html += @"</tr><table></div>";




        }

        dvTop.InnerHtml = htmltags;
    }

}