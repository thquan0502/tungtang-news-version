﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

public partial class AccountPost : System.Web.UI.Page
{
    protected string idThanhVien = "";
    protected string refCode = null;
    protected Dictionary<string, object> configs;

    protected string templateJsonCategories1 = "";
    protected string templateJsonCategories2 = "";

    protected string templateJsonCities = "";
    protected string templateJsonDistricts = "";
    protected string templateJsonWards = "";

    protected string templateFilterPostCode = "";
    protected string filterPostCode = null;

    protected string templateFilterPostOwner = "";
    protected string filterPostOwner = null;
    protected string templateJsonOwners = "";
	protected string templateJsonCode = "";

    protected string templateFilterPostTitle = "";
    protected string filterPostTitle = null;

    protected string templateFilterPostPrice = "";
    protected string filterPostPrice = null;
    protected double filterFromPostPrice = 0;
    protected double filterToPostPrice = 0;

    protected string templateFilterPostCommission = "";
    protected string filterPostCommission = null;
    protected double filterFromPostCommission = 0;
    protected double filterToPostCommission = 0;

    protected string templateFilterCreatedAtFrom = "";
    protected string filterCreatedAtFromAltDateTime = "";
    protected string filterCreatedAtFromAltDate = "";

    protected string templateFilterCreatedAtTo = "";
    protected string filterCreatedAtToAltDateTime = "";
    protected string filterCreatedAtToAltDate = "";

    protected string templateFilterCategory1 = "";
    protected string filterCategory1 = null;

    protected string templateFilterCategory2 = "";
    protected string filterCategory2 = null;

    protected string templateFilterCity = "";
    protected string filterCity = null;

    protected string templateFilterDistrict = "";
    protected string filterDistrict = null;

    protected string templateFilterWard = "";
    protected string filterWard = null;
	string sGia = "";
    string sHoaHong = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch
        {
            Response.Redirect("/dang-nhap/dn");
        }
        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString["Gia"].Trim() != "")
                {
                    sGia = Request.QueryString["Gia"].Trim();
                    txtRangeLuotXem.Value = sGia;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["HoaHong"].Trim() != "")
                {
                    sHoaHong = Request.QueryString["HoaHong"].Trim();
                    txtRangeHoaHong.Value = sHoaHong;
                }
            }
            catch { }
        }
        else
        {

        }
        this.configs = Utilities.Config.getConfigs();
        this.generateFilterInputs();
        this.loadThongTinThanhVien();
        this.loadCategories();
        this.loadOwners();
		this.loadidPost();
        this.renderAccountPostTable();
        //checkSoDT();//Code new
    }
    public void checkSoDT()//Code new
    {
        string result = "";
        string Email = txtShopEmailID.InnerHtml;
        string sql = "Select top 1 SoDienThoai from tb_ThanhVien where Email='" + Email + "' and isnull(isKhoa,'False')!='True'";
        try
        {
            DataTable tb = Connect.GetTable(sql);
            if (tb.Rows.Count > 0)
                result = tb.Rows[0][0].ToString();
        }
        catch
        { }
        if (result != "")
        {
            //Response.Redirect("/tai-khoan/chia-se");   
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn tc!')", true);
            string a = "";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn cần cập nhật thông tin số điện thoại!')", true);
            Response.Redirect("/thong-tin/tt");
        }
    }
    protected void loadThongTinThanhVien()
    {
         String sql1 = "";
        sql1 = sql1 + " SELECT ";
        sql1 = sql1 + "   tb_ThanhVien.TenCuaHang AS TenCuaHang, ";
        sql1 = sql1 + "   tb_ThanhVien.Code AS Code, ";
        sql1 = sql1 + "   tb_ThanhVien.Email AS Email, ";
        sql1 = sql1 + "   tb_ThanhVien.SoDienThoai AS SoDienThoai, ";
        sql1 = sql1 + "   tb_ThanhVien.TenDangNhap AS TenDangNhap, ";
        sql1 = sql1 + "   tb_ThanhVien.NgayDangKy AS NgayDangKy, ";
        sql1 = sql1 + "   tb_ThanhVien.linkAnh AS linkAnh, ";
        sql1 = sql1 + "   tb_ThanhVien.idTinh AS idTinh, ";
        sql1 = sql1 + "   tb_ThanhVien.idHuyen AS idHuyen, ";
        sql1 = sql1 + "   tb_ThanhVien.idPhuongXa AS idPhuongXa, ";
        sql1 = sql1 + "   tb_ThanhVien.DiaChi AS DiaChi, ";
        sql1 = sql1 + "   tb_ThanhVien.RefCode AS RefCode, ";
        sql1 = sql1 + "   tb_ThanhVien.MST AS MST, ";
        sql1 = sql1 + "   tb_ThanhVien.NgayCap AS NgayCap, ";
        sql1 = sql1 + "   tb_ThanhVien.NoiCap AS NoiCap, ";
        sql1 = sql1 + "   tb_ThanhVien.StsPro AS StsPro, ";
        sql1 = sql1 + "   tb_ThanhVien.LyDo_KhongDuyet AS LyDo,";
        sql1 = sql1 + "   tb_ThanhVien.ProAt AS ProAt,";
        sql1 = sql1 + "   tb_ThanhVien.isDuyet AS isDuyet, ";
        sql1 = sql1 + "   tb_ThanhVien.idAdmin_Duyet AS AdminDuyet";
        sql1 = sql1 + " FROM tb_ThanhVien ";
        sql1 = sql1 + " WHERE idThanhVien = " + idThanhVien;
        DataTable table = Connect.GetTable(sql1);
        if (table.Rows.Count > 0)
        {
            txtNameShopID.InnerHtml = "<h4>" + (table.Rows[0]["TenCuaHang"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["TenCuaHang"]) + "</h4>";
            txtInfoAccountCode.InnerText = "Mã thành viên: " + table.Rows[0]["Code"].ToString().Trim();
            txtShopEmailID.InnerHtml = (table.Rows[0]["Email"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["Email"].ToString());
            imgLinkAnh.Src = table.Rows[0]["LinkAnh"].ToString().Trim();
			
			  txtLoaiUser.InnerText = "Bình Thường";
           // string htmlIndenti = "";
           // htmlIndenti += "<a class='MainFunctionButton EditProfile' runat='server' href='/thong-tin/pro'>Chỉnh sửa thông tin</a>";
            if (table.Rows[0]["StsPro"].ToString().Trim() == "1")
            {
               // UserIdenti.InnerHtml = htmlIndenti;
              //  btnEditInfoUserID.Visible = false;
                txtLoaiUser.InnerText = "Thành Viên PRO";
                buttonUser.Visible = false;
                ImagePro.Style.Add("display", "");
            }
            string htmluser = "";
            if (table.Rows[0]["LyDo"].ToString().Trim() != "")
            {
                htmluser += "<div style='background-color:#fcf8e3;border-radius:10px;padding: 0 11px;'><p>Nội dung từ chối:" + table.Rows[0]["LyDo"].ToString().Trim() + "</p></div>";
                htmluser += "<a class='MainFunctionButton EditProfile' id='button_Forwad_mobi' style='background-color:#4cb050;color:white;margin-bottom:10px;height: 34px;' href='/thong-tin/pro'>Đăng ký lại</a>";
                ProNow.InnerHtml = htmluser;
                buttonUser.Visible = false;
                ImagePro.Visible = false;
            }
            if (table.Rows[0]["StsPro"].ToString().Trim() == "0" && table.Rows[0]["AdminDuyet"].ToString().Trim() == "-1")
            {
                txtLoaiUser.InnerText = "Thành viên Pro đang chờ duyệt";
                ImagePro.Visible = false;
                buttonUser.Visible = false;
            }
			
            // txtMST.InnerHtml = (table.Rows[0]["MST"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["MST"].ToString());
            // DateTime ngayCap = DateTime.MinValue;
            // if (table.Rows[0]["NgayCap"].ToString().Trim() != "")
            // {
            //     ngayCap = (DateTime)table.Rows[0]["NgayCap"];
            // }
            // string NgayCap = (table.Rows[0]["NgayCap"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayCap.ToString("MM-dd-yyyy")));
            // if (NgayCap == "01/01/1900")
            // {
            //     txtNgayCap.InnerHtml = "Chưa cung cấp";
            // }
            // else
            // {
            //     txtNgayCap.InnerHtml = NgayCap;
            // }
            // txtNoiCap.InnerHtml = (table.Rows[0]["NoiCap"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["NoiCap"].ToString());

            txtShopPhoneID.InnerHtml = (table.Rows[0]["SoDienThoai"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["SoDienThoai"].ToString());
            DateTime ngayDangKy = (DateTime)table.Rows[0]["NgayDangKy"];
            txtDateShopApprovedID.InnerHtml = (table.Rows[0]["NgayDangKy"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayDangKy.ToString("MM-dd-yyyy")));
            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim();
            string TenTinhThanh = StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString());
            string TenQuanHuyen = StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString());
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString());
            string DiaChi = table.Rows[0]["DiaChi"].ToString().Trim();
            if (DiaChi == "" && TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                txtShopAddressID.InnerHtml = "Chưa cung cấp";
            else
            {
                txtShopAddressID.InnerHtml = DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh;
                txtShopAddressID.Attributes.Add("title", DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh);
            }

            string idThanhVien_Cookie = null;
            try
            {
                idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
            }
            catch { }
            if (idThanhVien != idThanhVien_Cookie)
            {
              //  UserIdenti.Visible = false;
              //  btnEditInfoUserID.Visible = false;  
                buttonUser.Visible = false;
                ProNow.Visible = false;
            }
            this.refCode = table.Rows[0]["RefCode"].ToString();
        }
    }
    protected void renderAccountPostTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();

        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        if (this.refCode != null)
        {
            postParams = new Dictionary<string, string>();
            postParams.Add("RefCode", this.refCode);
        }

        sql1 += " FROM ";
        sql1 += "   ( ";
        sql1 += "     SELECT ";
        sql1 += "       ROW_NUMBER() OVER ( ";
        sql1 += "         ORDER BY ";
        sql1 += "           tbPost.NgayGuiDuyet DESC ";
        sql1 += "       ) AS RowNumber, ";
        sql1 += "       tbPost.idTinDang AS PostId, ";
        sql1 += "       tbPost.TieuDe AS PostTitle, ";
        sql1 += "       tbPost.Code AS PostCode, ";
        sql1 += "       tbPost.NgayDang AS PostCreatedAt, ";
		 sql1 += "       tbPost.NgayGuiDuyet AS PostSumbitAt, ";
        sql1 += "       ISNULL(tbPost.TuGia, 0) AS PostPrice, ";
        sql1 += "       tbPost.DuongDan AS PostPath, ";
        sql1 += "       tbPost.LinkAnh AS PostThumb, ";
        sql1 += "       ISNULL(tbAccount.idThanhVien, '') AS OwnerId, ";
        sql1 += "       ISNULL(tbAccount.SoDienThoai, '') AS OwnerPhone, ";
        sql1 += "       ISNULL(tbAccount.Code, '') AS OwnerCode, ";
        sql1 += "       ISNULL(tbAccount.TenCuaHang, '') AS OwnerFullname, ";
        sql1 += "       ISNULL(tbAccount.LinkAnh, '') AS OwnerThumbnail, ";
        sql1 += "       ISNULL(tbAccount.Email, '') AS OwnerEmail, ";
        sql1 += "       ISNULL(tbAccount.NgayDangKy, '') AS OwnerRegisterAt, ";
        sql1 += "       ISNULL(tbAccount.DiaChi, '') AS OwnerAddress, ";
        sql1 += "       ISNULL(TBOwnerCities.Ten, '') AS OwnerCity, ";
		sql1 += "       ISNULL(TBCity.Ten, '') AS PostCity, ";
		sql1 += "       ISNULL(TBDistrict.Ten, '') AS PostDistrict, ";
        sql1 += "       ISNULL(TBPhuongXa.Ten, '') AS PostPhuongXa, ";
        sql1 += "       ISNULL(TBOwnerDistricts.Ten, '') AS OwnerDistrict, ";
        sql1 += "       ISNULL(TBOwnerWards.Ten, '') AS OwnerWard, ";
        sql1 += "       ISNULL(tbPost.isHetHan, 0) AS IsPostExpired, ";
        sql1 += "       ISNULL(tbPost.IsDraft, 0) AS IsPostDraft, ";
        sql1 += "       ISNULL(tbPost.isDuyet, 0) AS IsPostApproved, ";
        sql1 += "       ISNULL(tbPost.ReferralRate, 0) AS ReferralRate, ";
        sql1 += "       ISNULL(tbPost.ReferralAmount, 0) AS ReferralAmount, ";
        sql1 += "       ISNULL(tbPost.Commission, 0) AS ReferralCommission, ";
        sql1 += "       ISNULL(TBCategories1.TenDanhMucCap1, '') AS CategoryName1, ";
        sql1 += "       ISNULL(TBCategories2.TenDanhMucCap2, '') AS CategoryName2 ";
        sql1 += "     FROM  tb_TinDang tbPost ";
        sql1 += "       LEFT JOIN tb_ThanhVien tbAccount ON tbPost.idThanhVien = tbAccount.idThanhVien ";
        sql1 += "       LEFT JOIN tb_DanhMucCap1 TBCategories1 ";
        sql1 += "           ON tbPost.idDanhMucCap1 = TBCategories1.idDanhMucCap1 ";
        sql1 += "       LEFT JOIN tb_DanhMucCap2 TBCategories2 ";
        sql1 += "           ON tbPost.idDanhMucCap2 = TBCategories2.idDanhMucCap2 ";
        sql1 += "       LEFT JOIN City TBOwnerCities ";
        sql1 += "           ON tbAccount.idTinh = TBOwnerCities.id ";
        sql1 += "       LEFT JOIN District TBOwnerDistricts ";
        sql1 += "           ON tbAccount.idHuyen = TBOwnerDistricts.id ";
        sql1 += "       LEFT JOIN tb_PhuongXa TBOwnerWards ";
        sql1 += "           ON tbAccount.idPhuongXa = TBOwnerWards.id ";
		sql1 += "       LEFT JOIN City TBCity ";
        sql1 += "           ON tbPost.idTinh = TBCity.id ";
		sql1 += "       LEFT JOIN District TBDistrict ";
        sql1 += "           ON tbPost.idHuyen = TBDistrict.id ";
        sql1 += "       LEFT JOIN tb_PhuongXa TBPhuongXa ";
        sql1 += "           ON tbPost.idPhuongXa = TBPhuongXa.id ";
        sql1 += "     WHERE ";
        sql1 += "       ISNULL(tbPost.Commission, 0) > 0 ";
        sql1 += "       AND ISNULL(tbPost.isHetHan, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.IsDraft, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.isDuyet, 0) = 1 ";
        if (!string.IsNullOrWhiteSpace(filterPostCode))
        {
            sql1 += "       AND tbPost.Code LIKE '%" + filterPostCode + "%' ";
            paginationParams.Add("FilterPostCode", filterPostCode);
        }
        if (!string.IsNullOrWhiteSpace(filterPostOwner))
        {
            sql1 += "       AND tbPost.idThanhVien = " + filterPostOwner + " ";
            paginationParams.Add("FilterPostOwner", filterPostOwner);
        }
        if (!string.IsNullOrWhiteSpace(filterPostTitle))
        {
            sql1 += "       AND tbPost.TieuDe LIKE '%" + filterPostTitle + "%' ";
            paginationParams.Add("FilterPostTitle", filterPostTitle);
        }
         //Giá
      
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "0")
        {            
            if (this.filterFromPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += " OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += " AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "500")
        {
            sql1 += " AND (tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100500")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500500")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500500")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        //Hoa Hồng
        
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "0")
        {
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  >= " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "500")
        {
            sql1 += " AND (tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100500")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500500")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500500")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtFromAltDate))
        {
            sql1 += "       AND tbPost.NgayGuiDuyet >= '" + filterCreatedAtFromAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtToAltDate))
        {
            sql1 += "       AND tbPost.NgayGuiDuyet <= '" + filterCreatedAtToAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory1))
        {
            sql1 += "       AND tbPost.idDanhMucCap1 = '" + filterCategory1 + "' ";
            paginationParams.Add("FilterCategory1", filterCategory1);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory2))
        {
            sql1 += "       AND tbPost.idDanhMucCap2 = '" + filterCategory2 + "' ";
            paginationParams.Add("FilterCategory2", filterCategory2);
        }
        if (!string.IsNullOrWhiteSpace(filterCity))
        {
            sql1 += "       AND tbPost.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict))
        {
            sql1 += "       AND tbPost.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard))
        {
            sql1 += "       AND tbPost.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        sql1 += "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());

        DataTable table3 = Connect.GetTable(sql3);
        html += "<table id='postTable' class='table table-bordered app-table dataTable no-footer text-sm'>";
        html += "<thead>";
        html += "<tr>";
		html += "<th class='datacol-sharess'>Thông tin<br/>Công ty/Cửa hàng/<br/>Người trích hoa hồng<br/>(Liên hệ)</th>";
        html += "<th>Mã bài đăng</th>";
        html += "<th>Hình ảnh</th>";
        html += "<th>Tiêu đề</th>";
        html += "<th>Danh mục</th>";
		html += "<th>Khu vực</th>";
        
        html += "<th>Giá</th>";
        html += "<th>Chia sẻ</th>";
        html += "<th>Hoa hồng</th>";
        html += "<th>Đăng lúc</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        if (table3.Rows.Count == 0)
        {
            html += "<tr><td colspan='8' style='text-align: center;'>Không có kết quả</td></tr>";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(table3.Rows[i]["PostId"].ToString());
            string url = Utilities.Post.getUrl(this.configs["domain"].ToString(), table3.Rows[i]["PostPath"].ToString(), postParams);
            string ownerInfoText = "";
            ownerInfoText += " data-thumbnail='" + Utilities.Account.getLinkThumbWithPath(table3.Rows[i]["OwnerThumbnail"].ToString()) + "' ";
            ownerInfoText += " data-fullname='" + table3.Rows[i]["OwnerFullName"].ToString() + "' ";
            ownerInfoText += " data-code='" + table3.Rows[i]["OwnerCode"].ToString() + "' ";
            ownerInfoText += " data-phone='" + table3.Rows[i]["OwnerPhone"].ToString() + "' ";
            ownerInfoText += " data-email='" + table3.Rows[i]["OwnerEmail"].ToString() + "' ";
            ownerInfoText += " data-addr='" + table3.Rows[i]["OwnerAddress"].ToString() + ", " + table3.Rows[i]["OwnerWard"].ToString() + ", " + table3.Rows[i]["OwnerDistrict"].ToString() + ", " + table3.Rows[i]["OwnerCity"].ToString() + "' ";
            ownerInfoText += " data-register-at='" + ((DateTime)table3.Rows[i]["OwnerRegisterAt"]).ToString("dd-MM-yyyy HH:mm:ss") + "' ";

            html += "<tr>";
			// owner info
            html += "<td class='datacol-sharess'>";
            html += "<span style='text-align: center;font-weight:bold;'>Mã TV:</span><a href='javascript:void(0);' style='font-weight:bold;' class='color-yellow user-info-btn' data-account-id='" + table3.Rows[i]["OwnerId"].ToString() + "' " + ownerInfoText + ">" + table3.Rows[i]["OwnerCode"].ToString() + "</a></br>";
            html += "<span style='text-align: center;font-weight:bold;'>Tên:</span><a href='javascript:void(0);' style='color:#4cb050 !important;font-weight: bold' class='color-yellow user-info-btn' data-account-id='" + table3.Rows[i]["OwnerId"].ToString() + "' " + ownerInfoText + ">" + table3.Rows[i]["OwnerFullname"].ToString() + "</a></br>";
            html += "<span style='text-align: center;font-weight:bold;'>SĐT:</span><a href='tel:" + table3.Rows[i]["OwnerPhone"].ToString() + "' style='color:red !important;font-weight: bold' class='color-yellow'>" + table3.Rows[i]["OwnerPhone"].ToString() + "</a>";
            html += "</td>";
            html += "</td>";
            // code
            html += "<td>";
            html += "<a href='" + url + "' style='color:#fe9900;'>" + table3.Rows[i]["PostCode"].ToString() + "</a>";
            html += "</td>";
            //image
            html += "<td class='datacol-share'>";
            html += "<p><img class='small-thumb' style='width: 100%;height: 130px;margin-left: 15px;margin-top: 4px;' src='" + urlHinhAnh + "' /></p>";
            html += "</td>";
            // title
            html += "<td class='datacol-share' style='text-align: center;'>";
            html += "<a href='" + url + "'>" + table3.Rows[i]["PostTitle"].ToString() + "</a>";
            html += "</td>";
            // category
            html += "<td class='datacol-shares' >";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["CategoryName1"].ToString()))
            {
                html += "<div>" + table3.Rows[i]["CategoryName1"].ToString() + "<div>";
            }
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["CategoryName2"].ToString()))
            {
                html += "<div>" + table3.Rows[i]["CategoryName2"].ToString() + "<div>";
            }
            html += "</td>";
			//dictrict
            html += "<td class='datacol-shares' >";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostCity"].ToString()))
            {
                html += "<div>" + table3.Rows[i]["PostCity"].ToString() + "</div>";
            }
			if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostDistrict"].ToString()))
            {
                html += "<div>" + table3.Rows[i]["PostDistrict"].ToString() + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostPhuongXa"].ToString()))
            {
                html += "<div>" + table3.Rows[i]["PostPhuongXa"].ToString() + "</div>";
            }
            else
            {
                html += "<div>Toàn Quốc</div>"; 
            }
            html += "</td>";
            
            // price
            html += "<td style='text-align: center;color:red;' >" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString()) + "</td>";
            // share
            html += "<td class='datacol-share'>";
            html += "<button type='button' class='btn btn-icon btn-sm btn-success btn-copy' data-content='" + url + "'><i class='fa fa-copy'></i></button>";
            html += "<a href='https://www.facebook.com/sharer/sharer.php?u=" + url + "' target='_blank' class='btn btn-icon btn-sm btn-info'><i class='fa fa-facebook-f'></i></a>";
            html += "<a href='https://twitter.com/intent/tweet?url=" + url + "' target='_blank' class='btn btn-icon btn-sm btn-info'><i class='fa fa-twitter'></i></a>";
            html += "<a href='https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=&body=" + url + "&ui=2&tf=1&pli=1' target='_blank' class='btn btn-icon btn-sm btn-info'><i class='fa fa-envelope'></i></a>";
            html += "<a href='javascript:void(0);' class='btn btn-icon btn-sm btn-info'><div class='zalo-share-button' data-href='" + url + "' data-oaid='579745863508352884' data-layout='2' data-color='white' data-customize='true' onclick='stopPropagation();'><i class='icon-zalo'></i></div></a>";
            html += "</td>";
            // referral commission
            html += "<td style='text-align: center;color:red;'>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralCommission"].ToString()) + "</td>";
            // created at
            html += "<td>" + ((DateTime)table3.Rows[i]["PostSumbitAt"]).ToString("dd/MM/yyyy HH:mm:ss") + "</td>";

            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        divAccountPostTable.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "/tai-khoan/chia-se", paginationParams);
        divAccountPostPagination1.InnerHtml = htmlPagination;
        divAccountPostPagination2.InnerHtml = htmlPagination;
    }
    protected void loadCategories()
    {
        string sql1 = @"
            SELECT
	            TBCategories1.idDanhMucCap1 AS CategoryId1,
	            TBCategories1.TenDanhMucCap1 AS CategoryName1,
	            TBCategories2.idDanhMucCap2 AS CategoryId2,
	            TBCategories2.TenDanhMucCap2 AS CategoryName2
            FROM tb_DanhMucCap1 TBCategories1
	            LEFT JOIN tb_DanhMucCap2 TBCategories2
		            ON TBCategories1.idDanhMucCap1 = TBCategories2.idDanhMucCap1
            ORDER BY TBCategories1.idDanhMucCap1 ASC, TBCategories2.idDanhMucCap2 ASC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        string currentCate = null;
        templateJsonCategories1 = "[";
        templateJsonCategories2 = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            if (currentCate != table1.Rows[i]["CategoryId1"].ToString())
            {
                currentCate = table1.Rows[i]["CategoryId1"].ToString();
                templateJsonCategories1 += @"{id1:'" + table1.Rows[i]["CategoryId1"].ToString() + @"', name1: '" + table1.Rows[i]["CategoryName1"].ToString() + @"'},";
            }
            else
            {
                templateJsonCategories2 += @"{id1:'" + table1.Rows[i]["CategoryId1"].ToString() + @"', id2:'" + table1.Rows[i]["CategoryId2"].ToString() + @"', name2: '" + table1.Rows[i]["CategoryName2"].ToString() + @"'},";
            }
        }
        templateJsonCategories1 += "]";
        templateJsonCategories2 += "]";
    }
      protected void loadOwners()
    {
        string sql1 = @"
            SELECT 
	            TBOwners.idThanhVien AS OwnerId,
	            TBOwners.Code AS OwnerCode,
	            TBOwners.TenCuaHang AS OwnerFullname,
                TBOwners.SoDienThoai AS OwnerPhone, 
				COUNT(TBPost.idTinDang)
            FROM tb_ThanhVien TBOwners , tb_TinDang TBPost
			WHERE TBOwners.idThanhVien = TBPost.idThanhVien AND TBPost.Commission > 0 AND TBPost.isDuyet != 0
			GROUP BY TBOwners.idThanhVien,
	            TBOwners.Code,
	            TBOwners.TenCuaHang,
                TBOwners.SoDienThoai,
				NgayDangKy
			HAVING COUNT(TBPost.idTinDang) > 0
            ORDER BY TBOwners.NgayDangKy DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonOwners = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonOwners += @"{id:'" + table1.Rows[i]["OwnerId"].ToString() + @"', code:'" + table1.Rows[i]["OwnerCode"].ToString() + @"', phone:'" + table1.Rows[i]["OwnerPhone"].ToString() + @"', fullname: '" + table1.Rows[i]["OwnerFullname"].ToString() + @"'},";
        }
        this.templateJsonOwners += "]";
    }
    protected void loadidPost()
    {
        string sql1 = @"
            SELECT 
	            TBPost.idTinDang AS PostId,
	            TBPost.Code AS PostCode	            
            FROM tb_TinDang TBPost
            WHERE TBPost.Commission > 0 AND TBPost.isDuyet != 0
            ORDER BY NgayDang DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonCode = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonCode += @"{id:'" + table1.Rows[i]["PostId"].ToString() + @"', code:'" + table1.Rows[i]["PostCode"].ToString() + @"'},";
        }
        this.templateJsonCode += "]";
    }
    protected void generateFilterInputs()
    {
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostCode")))
        {
            string filterPostCode = Request.QueryString.Get("FilterPostCode").Trim();
            this.filterPostCode = filterPostCode;
            this.templateFilterPostCode = filterPostCode;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostOwner")))
        {
            string filterPostOwner = Request.QueryString.Get("FilterPostOwner").Trim();
            this.filterPostOwner = filterPostOwner;
            this.templateFilterPostOwner = filterPostOwner;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostTitle")))
        {
            string filterPostTitle = Request.QueryString.Get("FilterPostTitle").Trim();
            this.filterPostTitle = filterPostTitle;
            this.templateFilterPostTitle = filterPostTitle;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostPrice")))
        {
            string filterPostPrice = Request.QueryString.Get("FilterPostPrice").Trim();
            this.filterPostPrice = filterPostPrice;
            this.templateFilterPostPrice = filterPostPrice;
            string[] temp = filterPostPrice.Split(';');
            if (temp.Length == 2)
            {
                Double.TryParse(temp[0], out this.filterFromPostPrice);
                Double.TryParse(temp[1], out this.filterToPostPrice);
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostCommission")))
        {
            string filterPostCommission = Request.QueryString.Get("FilterPostCommission").Trim();
            this.filterPostCommission = filterPostCommission;
            this.templateFilterPostCommission = filterPostCommission;
            string[] temp = filterPostCommission.Split(';');
            if (temp.Length == 2)
            {
                Double.TryParse(temp[0], out this.filterFromPostCommission);
                Double.TryParse(temp[1], out this.filterToPostCommission);
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedAtFrom")))
        {
            DateTime filterCreatedAtFrom_dateTime;
            string filterCreatedAtFrom = Request.QueryString.Get("FilterCreatedAtFrom").Trim();
            if (DateTime.TryParseExact(filterCreatedAtFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedAtFrom_dateTime))
            {
                this.filterCreatedAtFromAltDate = filterCreatedAtFrom;
                this.filterCreatedAtFromAltDateTime = filterCreatedAtFrom + " 00:00:00:000";
                this.templateFilterCreatedAtFrom = filterCreatedAtFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedAtTo")))
        {
            DateTime filterCreatedAtTo_dateTime;
            string filterCreatedAtTo = Request.QueryString.Get("FilterCreatedAtTo").Trim();
            if (DateTime.TryParseExact(filterCreatedAtTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedAtTo_dateTime))
            {
                this.filterCreatedAtToAltDate = filterCreatedAtTo;
                this.filterCreatedAtToAltDateTime = filterCreatedAtTo + " 23:23:23:999";
                this.templateFilterCreatedAtTo = filterCreatedAtTo;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCategory1")))
        {
            string filterCategory1 = Request.QueryString.Get("FilterCategory1").Trim();
            this.filterCategory1 = filterCategory1;
            this.templateFilterCategory1 = filterCategory1;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCategory2")))
        {
            string filterCategory2 = Request.QueryString.Get("FilterCategory2").Trim();
            this.filterCategory2 = filterCategory2;
            this.templateFilterCategory2 = filterCategory2;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCity")))
        {
            string filterCity = Request.QueryString.Get("FilterCity").Trim();
            this.filterCity = filterCity;
            this.templateFilterCity = filterCity;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterDistrict")))
        {
            string filterDistrict = Request.QueryString.Get("FilterDistrict").Trim();
            this.filterDistrict = filterDistrict;
            this.templateFilterDistrict = filterDistrict;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterWard")))
        {
            string filterWard = Request.QueryString.Get("FilterWard").Trim();
            this.filterWard = filterWard;
            this.templateFilterWard = filterWard;
        }
    }
}