﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Ajax : System.Web.UI.Page
{
    string Domain = "";
    string idNguoiDung = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        if (Request.Cookies["TungTang_Login"] != null && Request.Cookies["TungTang_Login"].Value.Trim() != "")
        {
            idNguoiDung = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        string action = Request.QueryString["Action"].Trim();
        switch (action)
        {
            case "DangXuat":
                DangXuat(); break;
            case "ClickDanhMuc":
                ClickDanhMuc(); break;
            case "ClickTinhThanh":
                ClickTinhThanh(); break;
            case "TimKiem":
                TimKiem(); break;
            case "DeleteTinDang":
                DeleteTinDang(); break;
            case "CapNhatHetHan":
                CapNhatHetHan(); break;
            case "UpdatePostStatusUser":
                UpdatePostStatus("USER"); break;
            case "LayThongTinDangKy":
                LayThongTinDangKy(); break;
            case "LoadTinhThanh":
                LoadTinhThanh(); break;
            case "LoadQuanHuyen":
                LoadQuanHuyen(); break;
            case "LoadPhuongXa":
                LoadPhuongXa(); break;
            case "LoadDanhMuc_TimKiem":
                LoadDanhMuc_TimKiem(); break;
            case "LoadDanhMuc":
                LoadDanhMuc(); break;
            case "LoadDanhMucCap2":
                LoadDanhMucCap2(); break;
            case "SoSanhMatKhau":
                SoSanhMatKhau(); break;
            case "ThongKeTinDang":
                ThongKeTinDang(); break;
        }

    }
    private void DangXuat()
    {
        HttpCookie TungTang_Login = new HttpCookie("TungTang_Login", "");
        // Gán thời gian sống của Cookie là thời gian hiện tại 
        TungTang_Login.Expires = DateTime.Now;
        // Thêm Cookie 
        Response.Cookies.Add(TungTang_Login);
        Response.Write("True");
    }
    string KiemTraKhongCo(string Number)
    {
        try
        {
            Number.Trim();
        }
        catch
        {
            Number = "";
        }
        try
        {
            decimal a = decimal.Parse(Number);
        }
        catch
        {
            Number = "0";
        }
        if (Number == "")
        {
            Number = "0";
        }
        else if (Number == "-")
        {
            Number = "0";
        }
        else//bỏ dấu chấm 100.000  => 100000
        {
            try
            {
                Number = Number.Replace(",", "").Trim();
            }
            catch { }
        }
        return Number;
    }
    void ThongKeTinDang()
    {
        string html = "";
        string idTinDang = StaticData.ValidParameter(Request.QueryString["TD"].Trim());
        string idDanhMucCap1 = StaticData.getField("tb_TinDang", "idDanhMucCap1", "idTinDang", idTinDang);
        //and idDanhMucCap1='" + idDanhMucCap1 + @"'
        DataTable table = Connect.GetTable(@" WITH MyTable AS
                                                (
                                                    SELECT TD.idTinDang,TD.MaTinDang,TD.TuGia,TD.NgayDang,TD.idTinh,TD.idHuyen,TD.idPhuongXa,TD.idDanhMucCap1,TD.idDanhMucCap2,TD.TieuDe,TD.LoaiTinDang,isnull(TD.SoLuotXem,0) as SoLuotXem  ,
                                                    ROW_NUMBER() OVER ( ORDER BY TD.idTinDang desc ) AS 'RowNumber'
                                                    FROM tb_TinDang TD
	                                                WHERE  isnull(TD.isDuyet,'False') = 'True' 
                                                )  
                                                SELECT RowNumber, (select COUNT(*) from MyTable) AllRowNumber, idTinDang,MaTinDang,TuGia,NgayDang,idTinh,idHuyen,idPhuongXa,(select TenDanhMucCap1 from tb_DanhMucCap1 where idDanhMucCap1=MyTable.idDanhMucCap1) TenDanhMucCap1,idDanhMucCap2,TieuDe,LoaiTinDang, (select  top 1 urlHinhAnh from tb_HinhAnh where idTinDang='" + idTinDang + @"') UrlHinhAnh,SoLuotXem
                                                FROM MyTable 
                                                WHERE idTinDang='" + idTinDang + @"'");


        if (table.Rows.Count > 0)
        {
            int allRowNumber = int.Parse(table.Rows[0]["AllRowNumber"].ToString());
            int SoDongCuaTinDang_DB = int.Parse(table.Rows[0]["RowNumber"].ToString());
            string SoLuotXem = int.Parse(table.Rows[0]["SoLuotXem"].ToString()).ToString("N0") + " lượt xem";
            int TongSoTrang = 0;
            int KichCoMotTrang = 50;
            int SoTrangCuaTinDang = 0;

            if (allRowNumber % KichCoMotTrang == 0)
                TongSoTrang = allRowNumber / KichCoMotTrang;
            else
                TongSoTrang = allRowNumber / KichCoMotTrang + 1;

            if (SoDongCuaTinDang_DB % KichCoMotTrang == 0)
                SoTrangCuaTinDang = SoDongCuaTinDang_DB / KichCoMotTrang;
            else
                SoTrangCuaTinDang = SoDongCuaTinDang_DB / KichCoMotTrang + 1;


            try
            {
                DataTable tbXacDinhSoTrang
                    = Connect.GetTable("exec proc_XacDinhSoTrangCuaTinDang @idTinDang=" + idTinDang + ",@TongSoTrang=" + TongSoTrang + ",@KichCoTrang=" + KichCoMotTrang + "");
                if (tbXacDinhSoTrang.Rows.Count > 0)
                {
                    SoTrangCuaTinDang = int.Parse(tbXacDinhSoTrang.Rows[0][0].ToString());
                }
            }
            catch
            {

            }

            try
            {
                DataTable tbXacDinhSoTrang
                    = Connect.GetTable("exec proc_XacDinhSoTrangCuaTinDang @idTinDang=" + idTinDang + ",@TongSoTrang=" + TongSoTrang + ",@KichCoTrang=" + KichCoMotTrang + "");
                if (tbXacDinhSoTrang.Rows.Count > 0)
                {
                    SoTrangCuaTinDang = int.Parse(tbXacDinhSoTrang.Rows[0][0].ToString());
                }
            }
            catch
            {

            }

            ////// HINH ANH
            string urlHinhAnh = "";
            if (System.IO.File.Exists(Server.MapPath("/images/td/slides/" + table.Rows[0]["UrlHinhAnh"])))
                urlHinhAnh = Domain + "/images/td/slides/" + table.Rows[0]["UrlHinhAnh"].ToString();
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            ////////
            //////// DIA DIEM
            //string TenHuyen1 = StaticData.getField("District", "Ten", "Id", table.Rows[0]["idHuyen"].ToString());
            string idTinh1 = table.Rows[0]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            //string idPhuongXa1 = table.Rows[0]["idPhuongXa"].ToString();
            //string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
            string chuoi_DiaDiem = "";

            //if (TenPhuongXa1 != "")
            //    chuoi_DiaDiem = TenPhuongXa1;
            //else
            //{
            //    if (TenHuyen1 != "")
            //        chuoi_DiaDiem = TenHuyen1;
            //    else
            //    {
            //        if (TenTinh1 != "")
            //            chuoi_DiaDiem = TenTinh1;
            //        else
            //            chuoi_DiaDiem = "Toàn quốc";
            //    }
            //}
            chuoi_DiaDiem = TenTinh1;
            if (TenTinh1 == "")
                chuoi_DiaDiem = "Toàn quốc";
            string LoaiTinDang = "Cần bán";
            if (table.Rows[0]["LoaiTinDang"].ToString().Trim() != "CanBan")
                LoaiTinDang = "Cần mua";

            html = @"<div class='row'>
                         <div class='col-md-3'  style='padding:0;'>
                             <img src='" + urlHinhAnh + @"' style='width:100%;height:85px;object-fit:cover;' />
                         </div>
                         <div class='col-md-8 '>
                             <div class='ThongTinTinDang' style='font-size:12px;'>
                                 <h5>" + table.Rows[0]["TieuDe"] + @"</h5>
                                 <p style='font-weight: 600;'>" + decimal.Parse(KiemTraKhongCo(table.Rows[0]["TuGia"].ToString())).ToString("N0").Replace(" ", "0").Replace(',', '.') + @" đ</p>
                                 <span>" + LoaiTinDang + " | " + TinhThoiGianLucDang(DateTime.Parse(table.Rows[0]["NgayDang"].ToString())) + " | " + chuoi_DiaDiem + @"</span>
                             </div>
                         </div>
                     </div>
                     <div class='row' style='border-bottom:1px solid;'>
                         <div style='width: 100%;padding:10px 0;'>
                            <div style='float:left;font-size: 14px;font-weight: bold;'>Vị trí tin đăng</div>
                            <div style='float:right;font-size: 12px;'>" + table.Rows[0]["TenDanhMucCap1"] + " - " + SoLuotXem + @"</div>
                        </div>
                     </div>
                     <div class='row'>
                         <div class='container-sliderTrang'>
                            <div class='range-slider'>";
           // html += @"   <span id='rs-bullet' class='rs-label' onclick=" + "\"" + "javascript:window.location.href='/tin-dang/1-" + idDanhMucCap1 + "-?LV=" + idDanhMucCap1 + "&P=" + SoTrangCuaTinDang + "';" + "\"" + @">Trang <span id='rs-bullet2'>" + SoTrangCuaTinDang + @"</span><div class='ArrowBullet'></div></span>";
            html += @"   <span id='rs-bullet' class='rs-label' onclick=" + "\"" + "javascript:window.location.href='/tin-dang/td?P=" + SoTrangCuaTinDang + "';" + "\"" + @">Trang <span id='rs-bullet2'>" + SoTrangCuaTinDang + @"</span><div class='ArrowBullet'></div></span>";                  

                              html+=@"  <input id='rs-range-line' class='rs-range' type='range' value='" + SoTrangCuaTinDang + @"' min='1' max='" + TongSoTrang + @"'> 
                                <script>showSliderValue();</script>
                            </div> 
                            <div class='box-minmax'>
                                <span>Đầu trang</span><span>Cuối trang</span>
                            </div> 
                        </div>
                     </div>";

        }
        Response.Write(html);

    }
    string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }
    private void CapNhatHetHan()
    {
        string idTinDang = Request.QueryString["idTinDang"].Trim();
        string HetHan = Request.QueryString["HetHan"].Trim();
        string sqlUpdateTD = "update tb_TinDang";
        if (HetHan.ToUpper().Trim() == "HETHAN")
            sqlUpdateTD += " set isHetHan='True'";
        else
            sqlUpdateTD += " set isHetHan='False'";
        sqlUpdateTD += " where idTinDang='" + idTinDang + "'";
        bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
        if (ktUpdateTD)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void UpdatePostStatus(string type)
    {
        string idTinDang = Request.QueryString["IdTinDang"].Trim();
        string newStatus = Request.QueryString["Status"].Trim();
        string setSql = " ";
        switch (newStatus)
        {
            case Models.TinDang.STATUS_ACTIVE:
                if(type == "USER")
                {
                    break;
                }
                setSql += " isHetHan = 0, IsDraft = 0, isDuyet = 1 ";
                break;
            case Models.TinDang.STATUS_PENDING:
                setSql += " isHetHan = 0, IsDraft = 0, isDuyet = null ";
                break;
            case Models.TinDang.STATUS_DENY:
                if (type == "USER")
                {
                    break;
                }
                setSql += " isHetHan = 0, IsDraft = 0, isDuyet = 0 ";
                break;
            case Models.TinDang.STATUS_DRAFT:
                setSql += " isHetHan = 0, IsDraft = 1 ";
                break;
            case Models.TinDang.STATUS_EXPIRED:
                setSql += " isHetHan = 1 ";
                break;
        }
        if(setSql.Trim() == "")
        {
            Response.Write("False");
        }
        string updateSql = " update tb_TinDang SET " + setSql;
        updateSql += " where idTinDang='" + idTinDang + "'";
        bool ktUpdateTD = Connect.Exec(updateSql);
        if (ktUpdateTD)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void DeleteTinDang()
    {
        string idTinDang = Request.QueryString["idTinDang"].Trim();
        string sqlDeleteHinhAnh = "delete from tb_HinhAnh where idTinDang='" + idTinDang + "'";
        bool ktDeleteHinhAnh = Connect.Exec(sqlDeleteHinhAnh);
        string sqlDeleteTinDang = "delete from tb_TinDang where idTinDang='" + idTinDang + "'";
        bool ktDeleteTinDang = Connect.Exec(sqlDeleteTinDang);
        if (ktDeleteTinDang)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void TimKiem()
    {
        string idDanhMucCap1 = Request.QueryString["idDanhMucCap1"].Trim();
        string idDanhMucCap2 = Request.QueryString["idDanhMucCap2"].Trim();
        string idTinh = Request.QueryString["idTinh"].Trim();
        string idHuyen = Request.QueryString["idHuyen"].Trim();
        string DiaChi = Request.QueryString["DiaChi"].Trim();
        string KeySearch = Request.QueryString["KeySearch"].Trim();
        string url = Domain;
        if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
        {
            string TenDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2);
            string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TenDanhMucCap2.ToLower()));
            url += "/tin-dang/2-" + idDanhMucCap2 + "-" + TieuDeSau;
        }
        else
        {
            if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
            {
                string TenDanhMucCap1 = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1);
                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TenDanhMucCap1.ToLower()));
                url += "/tin-dang/1-" + idDanhMucCap1 + "-" + TieuDeSau;
            }
            else
            {
                url += "/tin-dang/td";
            }
        }
        if ((idTinh != "" && idTinh != "0") || (idHuyen != "" && idHuyen != "0") || DiaChi != "" || KeySearch != "")
        {
            url += "?";
            if (idTinh != "" && idTinh != "0")
                url += "idTinh=" + idTinh + "&";
            if (idHuyen != "" && idHuyen != "0")
                url += "idHuyen=" + idHuyen + "&";
            if (DiaChi != "")
                url += "DiaChi=" + DiaChi + "&";
            if (KeySearch != "")
                url += "KeySearch=" + KeySearch + "&";
        }

        Response.Write(url);
    }
    private void ClickDanhMuc()
    {
        string idDanhMucCap1 = Request.QueryString["idDanhMucCap1"].Trim();
        string sqlDanhMucCap2 = "select * from tb_DanhMucCap2 where idDanhMucCap1='" + idDanhMucCap1 + "'";
        DataTable tbDanhMucCap2 = Connect.GetTable(sqlDanhMucCap2);
        string html = "";
        for (int i = 0; i < tbDanhMucCap2.Rows.Count; i++)
        {
            html += "<option value='" + tbDanhMucCap2.Rows[i]["idDanhMucCap2"].ToString() + "'>" + tbDanhMucCap2.Rows[i]["TenDanhMucCap2"].ToString() + "</option>";
        }
        if (html != "")
        {
            html += "<option value='' selected='selected'>Tất cả</option>";
        }
        Response.Write(html);
    }
    private void ClickTinhThanh()
    {
        string idCity = Request.QueryString["idCity"].Trim();
        string sqlHuyen = "select * from District where cityid='" + idCity + "'";
        DataTable tbHuyen = Connect.GetTable(sqlHuyen);
        string html = "";
        for (int i = 0; i < tbHuyen.Rows.Count; i++)
        {
            html += "<option value='" + tbHuyen.Rows[i]["id"].ToString() + "'>" + tbHuyen.Rows[i]["name"].ToString() + "</option>";
        }
        if (html != "")
        {
            html += "<option value='' selected='selected'>Tất cả</option>";
        }
        Response.Write(html);
    }
    private void LayThongTinDangKy()
    {
        string KetQua = "";
        string sql = "select * from tb_ThanhVien where idThanhVien='" + idNguoiDung + "'";
        DataTable tbThanhVien = Connect.GetTable(sql);
        if (tbThanhVien.Rows.Count > 0)
        {
            KetQua += tbThanhVien.Rows[0]["TenCuaHang"].ToString().Trim() + "|~~|";
            KetQua += tbThanhVien.Rows[0]["SoDienThoai"].ToString().Trim() + "|~~|";
            KetQua += tbThanhVien.Rows[0]["Email"].ToString().Trim() + "|~~|";
            KetQua += tbThanhVien.Rows[0]["DiaChi"].ToString().Trim();
            KetQua += " - " + StaticData.getField("District", "name", "id", tbThanhVien.Rows[0]["idHuyen"].ToString().Trim());
            KetQua += " - " + StaticData.getField("City", "name", "id", tbThanhVien.Rows[0]["idTinh"].ToString().Trim());
        }
        Response.Write(KetQua);
    }
    void LoadTinhThanh()
    {
        string Type = Request.QueryString["Type"].Trim();
        string html = "";
        if (Type == "ALL")
        {
            html += "<tr onclick='ChonPhuongXa(\"" + "" + "\",\"" + "" + "\",\"" + "0" + "\",\"" + "" + "\",\"" + "Toàn quốc" + "\",\"" + "" + "\")'>";
            html += "   <td>Toàn quốc</td>";
            html += "</tr>";
        }
        string strSql = "select  * from City order by type asc,Ten asc";
        DataTable table = Connect.GetTable(strSql);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "<tr onclick='LoadQuanHuyen(" + table.Rows[i]["id"].ToString().Trim() + ",\"" + table.Rows[i]["ten"] + "\")'>";
            html += "   <td>" + table.Rows[i]["ten"] + "</td>";
            html += "</tr>";
        }
        Response.Write(html);
    }
    void LoadQuanHuyen()
    {
        string Type = Request.QueryString["Type"].Trim();
        string TT = Request.QueryString["TT"].Trim();
        string TTT = Request.QueryString["TTT"].Trim();
        string html = "";
        if (Type == "ALL")
        {
            html += "<tr onclick='ChonPhuongXa(\"" + "" + "\",\"" + "" + "\"," + TT + ",\"" + "" + "\",\"" + TTT + "\",\"" + "" + "\")'>";
            html += "   <td>Tất cả</td>";
            html += "</tr>";
        }
        string strSql = "select  * from District where idTinhTP=" + TT + " order by type desc, ten asc";
        DataTable table = Connect.GetTable(strSql);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "<tr onclick='LoadPhuongXa(" + table.Rows[i]["id"] + "," + TT + ",\"" + table.Rows[i]["ten"] + "\",\"" + TTT + "\")'>";
            html += "   <td>" + table.Rows[i]["ten"] + "</td>";
            html += "</tr>";
        }
        Response.Write(html);
    }
    void LoadPhuongXa()
    {
        string Type = Request.QueryString["Type"].Trim();
        string QH = Request.QueryString["QH"].Trim();
        string TQH = Request.QueryString["TQH"].Trim();
        string TT = Request.QueryString["TT"].Trim();
        string TTT = Request.QueryString["TTT"].Trim();
        string html = "";
        if (Type == "ALL")
        {
            html += "<tr onclick='ChonPhuongXa(\"" + "" + "\",\"" + "" + "\"," + TT + "," + QH + ",\"" + TTT + "\",\"" + TQH + "\")'>";
            html += "   <td>Tất cả</td>";
            html += "</tr>";
        }
        string strSql = "select  * from tb_PhuongXa where idQuanHuyen=" + QH + " order by ten asc";
        DataTable table = Connect.GetTable(strSql);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "<tr onclick='ChonPhuongXa(" + table.Rows[i]["id"] + ",\"" + table.Rows[i]["ten"] + "\"," + TT + "," + QH + ",\"" + TTT + "\",\"" + TQH + "\")'>";
            html += "   <td>" + table.Rows[i]["ten"] + "</td>";
            html += "</tr>";
        }
        Response.Write(html);
    }
    void LoadDanhMuc()
    {
        string Type = Request.QueryString["Type"].Trim();
        string strSql = "select * from tb_DanhMucCap1 order by SoThuTu asc";
        string html = "";
        if (Type == "ALL")
        {
            html += "<tr>";
            html += "   <td>Tất cả danh mục</td>";
            html += "</tr>";
        }
        DataTable table = Connect.GetTable(strSql);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "<tr onclick='LoadDanhMucCap2(" + table.Rows[i]["idDanhMucCap1"].ToString().Trim() + ",\"" + table.Rows[i]["TenDanhMucCap1"] + "\")'>";
            html += "   <td><img src='/" + table.Rows[i]["linkIcon"] + "' style='width:7%;margin-right:10px;'/>" + table.Rows[i]["TenDanhMucCap1"] + "</td>";
            html += "</tr>";
        }
        Response.Write(html);
    }

    void LoadDanhMuc_TimKiem()
    {
        string Type = Request.QueryString["Type"].Trim();
        string strSql = "select * from tb_DanhMucCap1 order by SoThuTu asc";
        string html = "";
        if (Type == "ALL")
        {
            html += "<tr onclick='ChonDanhMuc(\"" + "0" + "\",\"" + "Tất cả danh mục" + "\")'>";
            html += "   <td>Tất cả danh mục</td>";
            html += "</tr>";
        }
        DataTable table = Connect.GetTable(strSql);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "<tr onclick='ChonDanhMuc(" + table.Rows[i]["idDanhMucCap1"].ToString().Trim() + ",\"" + table.Rows[i]["TenDanhMucCap1"] + "\")'>";
            html += "   <td><img src='/" + table.Rows[i]["linkIcon"] + "' style='width:7%;margin-right:10px;'/>" + table.Rows[i]["TenDanhMucCap1"] + "</td>";
            html += "</tr>";
        }
        Response.Write(html);
    }
    void LoadDanhMucCap2()
    {
        string Type = Request.QueryString["Type"].Trim();
        string C1 = Request.QueryString["C1"].Trim();
        string TC1 = Request.QueryString["TC1"].Trim();
        string html = "";
        if (Type == "ALL")
        {
            html += "<tr onclick='ChonDanhMuc(\"" + C1 + "\",\"" + TC1 + "\",\"" + "" + "\",\"" + "" + "\")'>";
            html += "   <td>Tất cả</td>";
            html += "</tr>";
        }
        string strSql = "select  * from tb_DanhMucCap2 where idDanhMucCap1=" + C1 + " order by SoThuTu asc";
        DataTable table = Connect.GetTable(strSql);
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "<tr onclick='ChonDanhMuc(\"" + C1 + "\",\"" + TC1 + "\",\"" + table.Rows[i]["idDanhMucCap2"] + "\",\"" + table.Rows[i]["TenDanhMucCap2"] + "\")'>";
            html += "   <td><img src='/" + table.Rows[i]["linkIcon"] + "' style='width:7%;margin-right:10px;background-color: black;border-radius: 0 30% 0% 30%;'/>" + table.Rows[i]["TenDanhMucCap2"] + "</td>";
            html += "</tr>";
        }
        Response.Write(html);
    }
    void SoSanhMatKhau()
    {
        if (Request.Cookies["TungTang_Login"] != null)
        {
            string MK = Request.QueryString["MK"].Trim();
            string idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
            string MatKhauHienTai = StaticData.getField("tb_ThanhVien", "MatKhau", "idThanhVien", idThanhVien).Trim();
            if (MatKhauHienTai == MK)
                Response.Write("equal");
        }
    }
}