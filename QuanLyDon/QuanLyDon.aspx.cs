﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML;

public partial class DanhMuc_SanPham : System.Web.UI.Page
{
    string pChuoiTimKiem = "";
    string TN = "";
    string DN = "";
    string N = "";
    string MDH = "";
    string MDS = "";
    string SDT = "";
    string TKH = "";
    string TT = "";
    string seach = "";
    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 1;
    int MaxPage = 0;
    int PageSize = 20;
    string idTaiKhoan = "";
    string mQuyen;
    string tenDangNhap_HienTai = "";
    string kqq = "";
    string tkn = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["CC_DSDH"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["CC_DSDH"].Value;
            idTaiKhoan = StaticData.getField("tb_TaiKhoan", "idtaiKhoan", "TenDangNhap", TenDangNhap_Cookie.ToString());
            string idLoaiTaiKhoan = StaticData.getField("tb_TaiKhoan", "idLoaiTaiKhoan", "TenDangNhap", TenDangNhap_Cookie.ToString());

            mQuyen = StaticData.getField("tb_LoaiTaiKhoan", "TenLoaiTaiKhoan", "idLoaiTaiKhoan", idLoaiTaiKhoan);
            if (mQuyen == "KhoVN")
                Response.Redirect("/home");
            if (mQuyen == "QUANLYCAP1" || mQuyen == "QUANLYCAP2" || mQuyen == "NHANVIENKHO" || mQuyen == "NHANVIENKINHDOANH")
            {
                string s = StaticData.getField("tb_NguoiDung", "idKey", "idTaiKhoan", idTaiKhoan);
                kqq = s;
            }
            else if (mQuyen == "ADMINCON")
            {
                kqq = idTaiKhoan;
            }
        }
        else
        {
            Response.Redirect("/login");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            try
            {
                pChuoiTimKiem = Request.QueryString["pChuoiTimKiem"].ToString();
                //Select1.Value = pChuoiTimKiem;
            }
            catch
            {
            }
            try
            {
                TN = Request.QueryString["TuNgay"].ToString();
                txtTuNgay_Popup.Value = TN;
            }
            catch
            {
            }
            try
            {
                DN = Request.QueryString["DenNgay"].ToString();
                txtDenNgay_Popup.Value = DN;
            }
            catch
            {
            }
            try
            {
                MDH = Request.QueryString["MDH"].ToString();
                seachMaDonHang.Value = MDH;
            }
            catch
            {
            }
            try
            {
                MDS = Request.QueryString["MDS"].ToString();
                seachMaDoiSoat.Value = MDS;
            }
            catch
            {
            }
            try
            {
                TKH = Request.QueryString["TKH"].ToString();
                seachTenKhachHang.Value = TKH;
            }
            catch
            {
            }
            try
            {
                N = Request.QueryString["N"].ToString();
                seachNgay.Value = StaticData.ConvertMMDDYYtoDDMMYY(N);
            }
            catch
            {
            }
            try
            {
                SDT = Request.QueryString["SDT"].ToString();
                seachSDT.Value = SDT;
            }
            catch
            {
            }
            try
            {
                TT = Request.QueryString["TT"].ToString();
                Select1.Value = TT;
            }
            catch
            {
            }
            LoadCongTrinh();
            LoadCongTrinh1();

            //LoadMauNen();
        }
    }

    private void LoadCongTrinh()
    {

        string html = "";
        string sql = "select * from tb_KenhBan where idKey=" + kqq + " order by TenKenhBan desc";
        DataTable tb = Connect.GetTable(sql);
        if (tb.Rows.Count > 0)
        {
            for (int i = 0; i < tb.Rows.Count; i++)
            {
                string sql1 = "select count(MaDonHang) as SoDon from( select count(MaDonHang) as MaDonHang from tb_DonHang where idKenhBan=" + tb.Rows[i]["idKenhBan"] + " and idKey=" + kqq + " group by MaDonHang) as tb";
                DataTable tb1 = Connect.GetTable(sql1);
                html += @"<div onclick='ChuyenTrang(" + tb.Rows[i]["idKenhBan"] + @")' class='col-sm-3 col-md-2'style='height:90px;'>
                            <div id='KenhBanhover' style='height:80px;background-color:#" + tb.Rows[i]["MauNen"] + @"'>
                              <h4 class='text-center'>" + tb.Rows[i]["TenKenhBan"] + @"</h4>
                              <div class='color-palette-set'>
                                <div class='text-center'><span>Tổng:" + tb1.Rows[0]["SoDon"] + @"</span></div>
                              </div>
                                </div>
                        </div>";
            }
            ListKenh.InnerHtml = html;
        }

    }
    private void SetPage()
    {
        string sql = @"select count(idDonHang) from tb_DonHang where '1'='1' and idKey=" + kqq;
        if (TN != "")
        {
            sql += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
        }
        if (DN != "")
        {
            sql += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
        }
        if (MDH != "")
        {
            sql += " and MaDonHang like '%" + MDH + "%'";
        }
        if (MDS != "")
        {
            sql += " and MaDoiSoat like '%" + MDS + "%'";
        }
        if (SDT != "")
        {
            sql += " and SDT like '%" + SDT + "%'";
        }
        if (TKH != "")
        {
            sql += " and TenKhachHang like '%" + TKH + "%'";
        }
        if (N != "")
        {
            sql += " and NgayNhap = '" + N + "'";
        }
        if (TT != "")
        {
            sql += " and TinhTrang = N'" + TT + "'";
        }
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    private void LoadCongTrinh1()
    {

        string sql = "";
        sql += @"select * from
            (
                    SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY dh.idDonHang desc
                  )AS RowNumber
                      ,dh.*,kb.TenKenhBan,sp.MaSanPham
                  FROM tb_DonHang dh left join tb_KenhBan kb on kb.idKenhBan=dh.idKenhBan left join tb_SanPham sp on dh.idSanPham=sp.idSanPham  where '1'='1' and dh.idKey=" + kqq;
        if (TN != "")
        {
            sql += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
        }
        if (DN != "")
        {
            sql += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
        }
        if (TN != "")
        {
            sql += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
        }
        if (DN != "")
        {
            sql += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
        }
        if (MDH != "")
        {
            sql += " and MaDonHang like '%" + MDH + "%'";
        }
        if (MDS != "")
        {
            sql += " and MaDoiSoat like '%" + MDS + "%'";
        }
        if (SDT != "")
        {
            sql += " and SDT like '%" + SDT + "%'";
        }
        if (TKH != "")
        {
            sql += " and TenKhachHang like '%" + TKH + "%'";
        }
        if (N != "")
        {
            sql += " and NgayNhap = '" + N + "'";
        }
        if (TT != "")
        {
            sql += " and TinhTrang = N'" + TT + "'";
        }
        sql += ") as tb1 ";

        sql += "WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";

        DataTable table = Connect.GetTable(sql);
        SetPage();
        string html = "";
        if (table != null)
        {
           
            for (int i = 0; i < table.Rows.Count; i++)
            {
                html += " <tr>";
                html += "       <td style='text-align:center;'>" + (((Page - 1) * PageSize) + i + 1) + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["TenKenhBan"].ToString() + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["MaDonHang"] + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["MaDoiSoat"] + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["MaSanPham"] + "</td>";
                html += "       <td style='text-align:center;'>" + double.Parse(KiemTraKhongCo(table.Rows[i]["GiaBan"].ToString())).ToString("N0") + "</td>";
                if (mQuyen=="ADMINCON")
                html += "       <td style='text-align:center;'>" + double.Parse(KiemTraKhongCo(table.Rows[i]["GiaGocHienTai"].ToString())).ToString("N0") + "</td>";
                else
                    html += "       <td style='text-align:center;'>0</td>";
                
                html += "       <td style='text-align:center;'>" + table.Rows[i]["TenKhachHang"] + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["SDT"] + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["TinhTrang"] + "</td>";
                html += "       <td style='text-align:center;'>" + table.Rows[i]["GhiChu"] + "</td>";
                html += "       <td style='text-align:center;'>" + StaticData.ConvertMMDDYYtoDDMMYY(table.Rows[i]["NgayNhap"].ToString()) + "</td>";
                html += "<td style='text-align:center;'>";
                    html += "           <a class='ui blue horizontal label' href='#' onclick='ShowPopup_ChinhSua(" + table.Rows[i]["idDonHang"] + ")' >Sửa</a>";
                if (mQuyen != "NHANVIENKINHDOANH" && mQuyen != "NHANVIENKHO")
                {
                    html += "           <a class='ui red horizontal label' onclick='XoaSanPham(" + table.Rows[i]["idDonHang"] + ")' >Xóa</a> ";
                }
                html += "       </td>";
                html += " </tr>";

            }
            string tt = @"select(select count(sl) from(select count(MaDonHang) as sl from tb_DonHang where TinhTrang=N'Đang Giao' and idKey=" + kqq;


            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (MDH != "")
            {
                tt += " and MaDonHang like '%" + MDH + "%'";
            }
            if (MDS != "")
            {
                tt += " and MaDoiSoat like '%" + MDS + "%'";
            }
            if (SDT != "")
            {
                tt += " and SDT like '%" + SDT + "%'";
            }
            if (TKH != "")
            {
                tt += " and TenKhachHang like '%" + TKH + "%'";
            }
            if (N != "")
            {
                tt += " and NgayNhap = '" + N + "'";
            }
            if (TT != "")
            {
                tt += " and TinhTrang = N'" + TT + "'";
            }
            tt += @" group by MaDonHang)as tb1)as DangGiao,
                                (select count(sl) from(select count(MaDonHang) as sl from tb_DonHang where TinhTrang = N'Thanh Toán' and idKey = " + kqq;
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (MDH != "")
            {
                tt += " and MaDonHang like '%" + MDH + "%'";
            }
            if (MDS != "")
            {
                tt += " and MaDoiSoat like '%" + MDS + "%'";
            }
            if (SDT != "")
            {
                tt += " and SDT like '%" + SDT + "%'";
            }
            if (TKH != "")
            {
                tt += " and TenKhachHang like '%" + TKH + "%'";
            }
            if (N != "")
            {
                tt += " and NgayNhap = '" + N + "'";
            }
            if (TT != "")
            {
                tt += " and TinhTrang = N'" + TT + "'";
            }
            tt += @" group by MaDonHang)as tb2) as ThanhToan,
                                (select count(sl) from(select count(MaDonHang) as sl from tb_DonHang where TinhTrang = N'Trả Về' and idKey = " + kqq;
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (MDH != "")
            {
                tt += " and MaDonHang like '%" + MDH + "%'";
            }
            if (MDS != "")
            {
                tt += " and MaDoiSoat like '%" + MDS + "%'";
            }
            if (SDT != "")
            {
                tt += " and SDT like '%" + SDT + "%'";
            }
            if (TKH != "")
            {
                tt += " and TenKhachHang like '%" + TKH + "%'";
            }
            if (N != "")
            {
                tt += " and NgayNhap = '" + N + "'";
            }
            if (TT != "")
            {
                tt += " and TinhTrang = N'" + TT + "'";
            }
            tt += @" group by MaDonHang)as tb) as TraVe,
                                (select count(sl) from(select count(MaDonHang) as sl from tb_DonHang where  idKey = " + kqq;
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (TN != "")
            {
                tt += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
            }
            if (DN != "")
            {
                tt += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
            }
            if (MDH != "")
            {
                tt += " and MaDonHang like '%" + MDH + "%'";
            }
            if (MDS != "")
            {
                tt += " and MaDoiSoat like '%" + MDS + "%'";
            }
            if (SDT != "")
            {
                tt += " and SDT like '%" + SDT + "%'";
            }
            if (TKH != "")
            {
                tt += " and TenKhachHang like '%" + TKH + "%'";
            }
            if (N != "")
            {
                tt += " and NgayNhap = '" + N + "'";
            }
            if (TT != "")
            {
                tt += " and TinhTrang = N'" + TT + "'";
            }
            tt += @" group by MaDonHang)as tb3) as TongDon";
            DataTable tb = Connect.GetTable(tt);
            html += " <tr>";
            html += " <tr style='background-color:red; color:white;font-size:17px'>";
            html += "       <td colspan='6' style='text-align:left;'>Tổng đơn: " + tb.Rows[0]["TongDon"] + "</td>";
            html += "       <td colspan='2' style='text-align:center;'>Đang giao: " + tb.Rows[0]["DangGiao"] + "</td>";
            html += "       <td colspan='2' style='text-align:center;'>Trả về: " + tb.Rows[0]["TraVe"] + "</td>";
            html += "       <td colspan='3' style='text-align:center;'>Thanh toán: " + tb.Rows[0]["ThanhToan"] + "</td>";


            html += " </tr>";
            html += "       <td colspan='14' class='footertable'>";
            string url = "QuanLyDon.aspx?";
            if (TN != "")
            {
                url += "&TuNgay="+TN;
            }
            if (DN != "")
            {
                url += "&DenNgay=" + DN;
            }
            if (TT != "")
            {
                url += "&TT=" + TT;
            }
            url += "&Page=";
            html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
            //Page 1
            if (txtPage1 != "")
            {
                if (Page.ToString() == txtPage1)
                    html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
                else
                    html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            }
            else
            {
                html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            }
            //Page 2
            if (txtPage2 != "")
            {
                if (Page.ToString() == txtPage2)
                    html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
                else
                    html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            }
            else
            {
                html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            }
            //Page 3
            if (txtPage3 != "")
            {
                if (Page.ToString() == txtPage3)
                    html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
                else
                    html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            }
            else
            {
                html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            }
            //Page 4
            if (txtPage4 != "")
            {
                if (Page.ToString() == txtPage4)
                    html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
                else
                    html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            }
            else
            {
                html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            }
            //Page 5
            if (txtPage5 != "")
            {
                if (Page.ToString() == txtPage5)
                    html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
                else
                    html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            }
            else
            {
                html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            }

            html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
            html += "   </td></tr>";

        }
        dvDanhSach.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string TuNgay = txtTuNgay_Popup.Value.Trim();
        string DenNgay = txtDenNgay_Popup.Value.Trim();
        string MDH = seachMaDonHang.Value.Trim();
        string MDS = seachMaDoiSoat.Value.Trim();
        string TKH  =seachTenKhachHang.Value.Trim();
        string SDT =seachSDT.Value.Trim();
        string Ngay = StaticData.ConvertDDMMtoMMDD(seachNgay.Value.Trim());

        string TinhTrang=Select1.Value.Trim();
        string url = "QuanLyDon.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if(MDH != "")
            url += "MDH=" + MDH + "&";
        if(MDS != "")
            url += "MDS=" + MDS + "&";
        if(SDT != "")
            url += "SDT=" + SDT + "&";
        if(TKH != "")
            url += "TKH=" + TKH + "&";
        if (Ngay != "")
            url += "N=" + Ngay + "&";
        if (TinhTrang != "")
            url += "TT=" + TinhTrang + "&";
        Response.Redirect(url);
    }
    string KiemTraKhongCo(string Number)
    {
        try
        {
            Number.Trim();
        }
        catch
        {
            Number = "";
        }
        try
        {
            decimal a = decimal.Parse(Number);
        }
        catch
        {
            Number = "0";
        }
        if (Number == "")
        {
            Number = "0";
        }
        else if (Number == "-")
        {
            Number = "0";
        }
        else//bỏ dấu chấm 100.000  => 100000
        {
            try
            {
                Number = Number.Replace(",", "").Trim();
            }
            catch { }
        }
        return Number;
    }
    private void ExportToExcel(DataTable dt)
    {
        DataTable table = dt;
        ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
        wbook.Worksheets.Add(table, "tab1");
        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //Provide you file name here
        httpResponse.AddHeader("content-disposition", "attachment;filename=\"XuatFile(" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ").xlsx\"");

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            wbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }
    protected void btXuatExel_Click(object sender, EventArgs e)
    {
        string TN = "";
        string DN = "";
        string head = "";
        string q = "";
        try
        {
            q = Request.QueryString["TT"].ToString();
        }
        catch
        {

        }
        if (q == "000")
        {
            head = "";
        }
        if (q == "111")
        {
            head = " and TinhTrang=N'Đang Giao'";
        }
        if (q == "222")
        {
            head = " and TinhTrang=N'Trả Về'";
        }
        if (q == "333")
        {
            head = " and TinhTrang=N'Thanh Toán'";
        }
        string sql = "select (select TenKenhBan from tb_KenhBan where idKenhBan=dh.idKenhBan) as TenKenhBan, MaDonHang,MaDoiSoat,(select MaSanPham from tb_SanPham where idSanPham=dh.idSanPham) as MaSanPham,GiaBan,TenKhachHang,SDT,TinhTrang,NgayNhap from tb_DonHang dh where and idKey=" + kqq + head;
        try
        {
            TN = Request.QueryString["TuNgay"].ToString();
        }
        catch
        {
        }
        try
        {
            DN = Request.QueryString["DenNgay"].ToString();
        }
        catch
        {
        }
        if (TN != "")
        {
            sql += " and NgayNhap>='" + StaticData.ConvertDDMMtoMMDD(TN) + "'";
        }
        else if (DN != "")
        {
            sql += " and NgayNhap<='" + StaticData.ConvertDDMMtoMMDD(DN) + "'";
        }
        DataTable tb = Connect.GetTable(sql);
        ExportToExcel(tb);
    }
}