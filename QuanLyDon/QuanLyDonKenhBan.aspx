﻿<%@ Page Language="C#" MasterPageFile="~/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="QuanLyDonKenhBan.aspx.cs" Inherits="DanhMuc_SanPham" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #KenhBanhover {
            border: 0 solid;
            box-shadow: inset 0 0 20px rgba(255, 255, 255, 0);
            outline: 1px solid;
            outline-color: rgba(255, 255, 255, .5);
            outline-offset: 0px;
            text-shadow: none;
            transition: all 1250ms cubic-bezier(0.19, 1, 0.22, 1);
        }

            #KenhBanhover:hover {
                width: 95%;
                border: 1px solid;
                box-shadow: inset 0 0 20px rgba(255, 255, 255, .5), 0 0 20px rgba(255, 255, 255, .2);
                outline-color: rgba(255, 255, 255, 0);
                outline-offset: 15px;
                text-shadow: 1px 1px 2px #427388;
            }
    </style>
    <script>
        function getUrlVars() {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.has('idKenhBan')
            let param = searchParams.get('idKenhBan')
            return param;
        }
        function ChuyenTrang(idKenhBan) {
            window.location = "ListDonHangKenhBan.aspx?idKenhBan=" + idKenhBan;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server" class="ui form" autocomplete="off">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="box box-default color-palette-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-tag"></i>QUẢN LÝ ĐƠN HÀNG THEO KÊNH
                        <i class="right chevron icon divider"></i>
                        <%=tkn %></h3>
                    </div>
                    <div class="box-body">
                        
                        <div class="field" style="margin-top: 10px">
                            <div class="field">
                                        <a style="width: 130px" class="ui labeled icon blue button" href="QuanLyDon.aspx"><i class="left chevron icon"></i>Trở lại</a>
                                </div>

                            <div class="fields">
                                <select id="slSeach" runat="server" style="width:120px">
                                    <option value="000">Tất cả</option>
                                    <option value="111">Đang giao</option>
                                    <option value="222">Trả về</option>
                                    <option value="333">Thanh Toán</option>
                                </select>
                                <div class="ui left icon input" style="width:200px">
                                    <input id="txtTuNgay_Popup" type="text" placeholder="Từ ngày" runat="server"/>
                                    <i class="date icon"></i>
                                </div>
                                <div class="ui left icon input" style="width:200px">
                                    <input id="txtDenNgay_Popup" type="text" placeholder="Đến Ngày" runat="server"/>
                                    <i class="date icon"></i>
                                </div>
                                
                                <div class="field">
                                    <asp:LinkButton ID="btTimKiem" class="ui waves-effect waves-light blue button" Width="130px" OnClick="btTimKiem_Click" runat="server">  <i class="search icon"></i> Tìm kiếm </asp:LinkButton>
                                </div>
                                <script>
                                    function refesh() {
                                        window.location = "QuanLyDonKenhBan.aspx?idKenhBan=" + getUrlVars();
                                    }
                                </script>
                                <div class="field" style="">
                                    <a onclick="refesh()" class="ui icon blue button"><i class="refresh icon"></i></a>
                                </div>
                                <div class="field">
                                    <asp:LinkButton ID="LinkButton1" class="ui waves-effect waves-light blue button" Width="130px" OnClick="btXuatExel_Click" runat="server">  <i class="file icon"></i>Xuất</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div  style='font-size: 12px; overflow: auto; margin-bottom: 12px;'>
                            <table id="tbGoiCuoc" class="ui unstackable table">
                                <thead>
                                    <th style="text-align: center; width: 50px">STT
                                    </th>
                                    <th>Kênh bán
                                    </th>
                                    <th>Mã đơn hàng
                                    </th>
                                    <th>Mã đối soát
                                    </th>
                                    <th>Mã sản phẩm
                                    </th>
                                    <th>Giá bán
                                    </th>
                                    <th>Tên khách hàng
                                    </th>
                                    <th>SDT
                                    </th>
                                    <th>Tình trạng
                                    </th>
                                    <th>Ngày nhập
                                    </th>

                                </thead>
                                <tbody>
                                    <tr>

                                        <td style="text-align: center;"></td>


                                        <td style="text-align: center;min-width:100px">
                                           </td>
                                        <td style="text-align: center;">
                                            <input style="background-color:antiquewhite" id="seachMaDonHang" type="text" placeholder="" runat="server" /></td>
                                        <td style="text-align: center;">
                                            <input style="background-color:antiquewhite" id="seachMaDoiSoat" type="text" placeholder="" runat="server" /></td>
                                        <td style="text-align: center;min-width:100px">
                                            </td>
                                        <td style="text-align: center;min-width:100px">
                                            </td>
                                        <td style="text-align: center;">
                                            <input style="background-color:antiquewhite" id="seachTenKhachHang" type="text" placeholder="" runat="server" /></td>
                                        <td style="text-align: center;">
                                            <input style="background-color:antiquewhite" id="seachSDT" type="text" placeholder="" runat="server" /></td>
                                        <td style="text-align: center;min-width:100px">
                                            </td>
                                        <td style="text-align: center;min-width:100px">
                                            <input style="background-color:antiquewhite" id="seachNgay" type="text" placeholder="" runat="server" />
                                            </td>


                                    </tr>
                                </tbody>
                                <tbody id="dvDanhSach" runat="server">

                                </tbody>
                        </table>
                            </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>

        <!--Popup cập nhật nhập kho-->
        <div class="ui modal scrolling transition hidden" id="dvPopup" style="padding: 10px; top: 10px; display: block !important;">
            <div style="font-weight: bold; font-size: 18px;" id="title_Popup">TITLE</div>
            <div class="box-body">
                <div class="content">
                    <div class="ui form">
                        <div class="three fields">
                            <div class="field">
                                <label>Tên kênh bán(*): </label>
                                <input placeholder="" type="text" id="txtTenKenhBan_Popup" />
                            </div>
                            <div class="field">
                                <label>Số kí tự mã đơn(*): </label>
                                <input placeholder="" type="text" id="txtSoKiTuMaDon_Popup" onkeypress="onlyNumber(event)" />
                            </div>
                            <div class="field">
                                <label>Màu nền(*): </label>
                                <select id="txtMauNen_Popup" runat="server" style="width: 100%">
                                </select>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class='actions'>
                <a class='waves-effect waves-light negative ui cancel right labeled icon button' style=""><i class='remove icon'></i>Đóng </a>
                <a id="btnAction" class='waves-effect waves-light ui green right labeled icon button'><i class='checkmark icon'></i>Lưu </a>
            </div>
        </div>

    </form>
    <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
    <link href="../plugins/datetimePicker1/build/jquery.datetimepicker.min.css" rel="stylesheet" />
    <script>
        $("#ContentPlaceHolder1_txtMauNen_Popup").select2();
        $('#ContentPlaceHolder1_txtTuNgay_Popup,#ContentPlaceHolder1_txtDenNgay_Popup,#ContentPlaceHolder1_seachNgay').datetimepicker({
            //dayOfWeekStart : 1,
            //todayBtn: "linked",
            language: "it",
            autoclose: true,
            todayHighlight: true,
            timepicker: false,
            dateFormat: 'dd/mm/yyyy',
            format: 'd/m/Y',
            formatDate: 'Y/m/d',
            //value: 'today'
        });
        function hasUnicode(str) {
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) return true;
            }
            return false;
        }
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
</asp:Content>


