﻿<%@ Page Language="C#" MasterPageFile="~/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="QuanLyDon.aspx.cs" Inherits="DanhMuc_SanPham" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #KenhBanhover {
            border: 0 solid;
            box-shadow: inset 0 0 20px rgba(255, 255, 255, 0);
            outline: 1px solid;
            outline-color: rgba(255, 255, 255, .5);
            outline-offset: 0px;
            text-shadow: none;
            transition: all 1250ms cubic-bezier(0.19, 1, 0.22, 1);
        }

            #KenhBanhover:hover {
                width: 95%;
                border: 1px solid;
                box-shadow: inset 0 0 20px rgba(255, 255, 255, .5), 0 0 20px rgba(255, 255, 255, .2);
                outline-color: rgba(255, 255, 255, 0);
                outline-offset: 15px;
                text-shadow: 1px 1px 2px #427388;
            }
    </style>
    <script>
        function ChuyenTrang(idKenhBan) {
            window.location = "QuanLyDonKenhBan.aspx?idKenhBan=" + idKenhBan;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <input type="hidden" name="name" value="" runat="server" id="MaSanPham" />
    <input type="hidden" name="name" value="" runat="server" id="txtDATAVatTu" />
    <input type="hidden" name="name" value="" runat="server" id="txtSTTVatTu" />
    <input type="hidden" name="name" value="" runat="server" id="txtidKey" />
    <form runat="server" class="ui form" autocomplete="off">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="box box-default color-palette-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-tag"></i>QUẢN LÝ ĐƠN HÀNG</h3>
                        
                    </div>
                    <div class="box-body">
                        <div class="row" id="ListKenh" runat="server">
                        </div>
                        <div class="field" style="margin-top: 10px">
                            <div class="fields">
                                <div class="ui left icon input" style="width:300px">
                                <select id="Select1" runat="server">
                                                <option value="" >Tất cả</option>
                                                <option value="Đang Giao" >Đang Giao</option>
                                                <option value="Trả Về">Trả Về</option>
                                                <option value="Lên Đơn">Lên Đơn</option>
                                                <option value="Thanh Toán">Thanh Toán</option>
                                                </select>
                                    </div>
                                <div class="ui left icon input">
                                    <input id="txtTuNgay_Popup" type="text" placeholder="Từ ngày" runat="server" />
                                    <i class="date icon"></i>
                                </div>
                                <div class="ui left icon input">
                                    <input id="txtDenNgay_Popup" type="text" placeholder="Đến Ngày" runat="server" />
                                    <i class="date icon"></i>
                                </div>

                                <div class="field">
                                    <asp:LinkButton ID="btTimKiem" class="ui waves-effect waves-light blue button" Width="130px" OnClick="btTimKiem_Click" runat="server">  <i class="search icon"></i> Tìm kiếm </asp:LinkButton>
                                </div>
                                <script>
                                    function refesh() {
                                        window.location = "QuanLyDon.aspx";
                                    }
                                </script>
                                <div class="field" style="">
                                    <a onclick="refesh()" class="ui icon blue button"><i class="refresh icon"></i></a>
                                </div>
                                <div class="field">
                                    <asp:LinkButton ID="LinkButton1" class="ui waves-effect waves-light blue button" Width="130px" OnClick="btXuatExel_Click" runat="server">  <i class="file icon"></i>Xuất</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div  style='font-size: 12px; overflow: auto; margin-bottom: 12px;'>
                            <table id="tbGoiCuoc" class="ui unstackable table">
                                <thead>
                                    <th style="text-align: center; width: 50px">STT
                                    </th>
                                    <th>Kênh bán
                                    </th>
                                    <th>Mã đơn hàng
                                    </th>
                                    <th>Mã đối soát
                                    </th>
                                    <th>Mã sản phẩm
                                    </th>
                                    <th>Giá bán
                                    </th>
                                    <th>Giá vốn
                                    </th>
                                    <th>Tên khách hàng
                                    </th>
                                    <th>SDT
                                    </th>
                                    <th>Tình trạng
                                    </th>
                                    <th>Ghi chú
                                    </th>
                                    <th>Ngày nhập
                                    </th>
                                    <th>
                                    </th>
                                    
                                </thead>
                                <tbody>
                                    <tr>

                                        <td style="text-align: center;"></td>
                                        <td style="text-align: center;min-width:70px">
                                           </td>
                                        <td style="text-align: center;min-width:70px">
                                            <input style="background-color:antiquewhite" id="seachMaDonHang" type="text" placeholder="" runat="server" /></td>
                                        <td style="text-align: center;min-width:100px">
                                            <input style="background-color:antiquewhite" id="seachMaDoiSoat" type="text" placeholder="" runat="server" /></td>
                                        <td style="">
                                            </td>
                                        <td style="">
                                            </td>
                                        <td style="">
                                            </td>
                                        <td style="text-align: center;min-width:60px">
                                            <input style="background-color:antiquewhite" id="seachTenKhachHang" type="text" placeholder="" runat="server" /></td>
                                        <td style="text-align: center;min-width:40px">
                                            <input style="background-color:antiquewhite" id="seachSDT" type="text" placeholder="" runat="server" /></td>
                                        <td style="min-width:120px;top:10px">
                                            
                                            </td>
                                        <td style="">
                                            </td>
                                        <td style="text-align: center;min-width:100px">
                                            <input style="background-color:antiquewhite" id="seachNgay" type="text" placeholder="" runat="server" />
                                            </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody id="dvDanhSach" runat="server">

                                </tbody>
                        </table>
                            </div>
                    </div>
                </div>
                
            </section>
            <!-- /.content -->
        </div>
        <div class="ui modal scrolling transition hidden" id="dvPopup" style="padding: 10px; top: 10px; display: block !important;">
            <div style="font-weight: bold; font-size: 18px;" id="title_Popup">TITLE</div>
            <div class="box-body">
                <div class="content">
                    <div class="ui form">
                        <div class="two fields">
                            <div class="field">
                                <label>Mã đơn hàng(*): </label>
                                <input placeholder="" type="text" id="txtMaDọnHang_Popup" />
                            </div>
                            <div class="field">
                                <label>Mã đối soát(*): </label>
                                <input placeholder="" type="text" id="txtMaDoiSoat_Popup" />
                            </div>

                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label>Giá bán(*): </label>
                                <input placeholder="" type="text" id="txtGiaBan_Popup" onkeypress="onlyNumber(event)" />
                            </div>
                            <div class="field">
                                <label>Tên khách Hàng(*): </label>
                                <input placeholder="" type="text" id="txtTenKhachHang_Popup" />
                            </div>
                            <div class="field">
                                <label>Số điện thoại(*): </label>
                                <input placeholder="" type="text" id="txtSDT_Popup" onkeypress="onlyNumber(event)" />
                            </div>
                        </div>
                        <div class="field">
                            <label style="font-size: 17px; background-color: bisque; color: black; padding: 10px;">CHI TIẾT SẢN PHẨM</label>
                        </div>
                        <div class="four fields">
                            <div class="field" id="MSPUD">
                                <label>Mã sản phẩm(*): </label>
                                <input placeholder="" type="text" id="txtMaSanPham"/>
                                <script>
                                    function handle(e) {
                                        if (e.keyCode === 13) {
                                            e.preventDefault(); // Ensure it is only this code that rusn
                                        }
                                    }
                                    $(function () {
                                        debugger
                                          var arr = $("#ContentPlaceHolder1_MaSanPham").val().trim().split("@");
                                          $("#txtMaSanPham").autocomplete({
                                          source: arr
                                        });
                                      } );
                                      </script>
                            </div>
                            <style>
                                #txtMaSanPham {
                                        position: relative;
                                        z-index: 10000;
                                    }
                                    .ui-autocomplete {
                                         z-index: 9999 !important;
                                    }
                            </style>
                            <div class="field">
                                <label style="visibility: hidden;">A</label>
                                <a class="ui labeled icon green button" id="btn-action"><i class="add icon"></i>Thêm</a>
                            </div>
                        </div>
                        <div class="four fields" style="height: 241px; overflow: auto;">
                            <table class="ui unstackable table" id="tb">
                                <tbody>
                                    <tr style="background-color: #0000009e; color: white;">
                                        <th style="min-width: 50px;">STT</th>
                                        <th style="min-width: 150px;">Mã sản phẩm</th>
                                        <th style=""></th>
                                    </tr>
                                </tbody>
                                <tbody id="ContentPlaceHolder1_dvDanhSachHangHoa">
                                </tbody>
                                <tbody>
                                    <tr>
                                    </tr>
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>
            <div class='actions'>
                <a class='waves-effect waves-light negative ui cancel right labeled icon button' style=""><i class='remove icon'></i>Đóng </a>
                <a id="btnAction" class='waves-effect waves-light ui green right labeled icon button'><i class='checkmark icon'></i>Lưu </a>
            </div>
        </div>
        <!--Popup cập nhật nhập kho-->
        

    </form>
    <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
    <link href="../plugins/datetimePicker1/build/jquery.datetimepicker.min.css" rel="stylesheet" />
     <script>
        function getUrlVars() {
            let searchParams = new URLSearchParams(window.location.search)
            searchParams.has('idKenhBan')
            let param = searchParams.get('idKenhBan')
            return param;
        }
        function ShowPopup_ThemMoi() {
            $("#txtMaDọnHang_Popup").val('');
            $("#txtMaSanPham").val('').trigger('change');
            $("#txtMaDoiSoat_Popup").val('');
            $("#txtGiaBan_Popup").val('');
            $("#txtTenKhachHang_Popup").val('');
            $("#txtSDT_Popup").val('');
            $("#btn-action").attr("onclick", "ThemMau()");
            $("#title_Popup").html("THÊM MỚI ĐƠN HÀNG THEO KÊNH");
            $("#btnAction").attr("onclick", "ThemSanPham()");
            $('#dvPopup').modal('setting', 'closable', false).modal('show');
        }
        function ShowPopup_ChinhSua(idDonHang) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        var arr = xmlhttp.responseText.split("@_@");
                        $("#txtMaDọnHang_Popup").val(arr[0].trim());
                        $("#txtMaDoiSoat_Popup").val(arr[1].trim());
                        $("#txtGiaBan_Popup").val(arr[2].trim());
                        $("#txtTenKhachHang_Popup").val(arr[3].trim());
                        $("#txtSDT_Popup").val(arr[4].trim());
                        $("#txtMaSanPham").val(arr[5].trim());
                        $("#txtMaSanPham").attr('readonly', true);
                        $("#title_Popup").html("CHỈNH SỬA THÔNG TIN ĐƠN HÀNG THEO KÊNH");
                        $("#btnAction").attr("onclick", "SuaDonHang(" + idDonHang + ")");
                        $('#dvPopup').modal('setting', 'closable', false).modal('show');
                    }
                    else {
                        alert("Lỗi load thông tin đơn hàng !")
                    }

                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadThongTinDonHang&idDonHang=" + idDonHang, true);
            xmlhttp.send();

        }
        function ThemMau() {
            var idSanPham = $("#txtMaSanPham").val();
            var STT = 0;
            if ($("#ContentPlaceHolder1_txtDATAVatTu").val().length > 0) {
                var array = $("#ContentPlaceHolder1_txtDATAVatTu").val().split("@_@");
                STT = array.length - 1;
            }
            if (idSanPham == "") {
                $("#txtMaSanPham").notify("Vui lòng nhập mã sản phẩm !");
            }
            else {
                var html = "";
                html += "<tr id='tr_" + STT + "'>";
                html += "   <td id='td_DataString_" + STT + "' style='display:none;'>" + idSanPham + "</td>";
                html += "   <td>" + (STT + 1) + "</td>";
                html += "   <td>" + idSanPham + "</td>";
                html += "   <td><a class='ui red horizontal label' onclick='XoaMau(" + STT + ")'><i class='fa fa-trash'></i></a></td>";
                html += "</tr>";
                $("#ContentPlaceHolder1_txtSTTVatTu").val(STT);
                $("#ContentPlaceHolder1_dvDanhSachHangHoa").append(html);
                var laychuoi = $("#ContentPlaceHolder1_txtDATAVatTu").val();
                laychuoi += idSanPham + "@_@";
                $("#ContentPlaceHolder1_txtDATAVatTu").val(laychuoi);
            }
        }
        function XoaMau(STT) {
            if (confirm("Bạn có muốn xóa không?")) {
                $("#tr_" + STT).remove();
                var chuoimoi = "";
                var laychuoi = $("#ContentPlaceHolder1_txtDATAVatTu").val().split("@_@");
                for (var i = 0; i < laychuoi.length - 1; i++) {
                    if (i.toString() != STT.toString()) {
                        chuoimoi += laychuoi[i] + "@_@";
                    }

                }
                $("#ContentPlaceHolder1_txtDATAVatTu").val(chuoimoi);


            }
        }
        function XoaTatCaDon() {
            var idKey = $("#ContentPlaceHolder1_txtidKey").val().trim();
            if (confirm("Bạn có chắc muốn xoá ?")) {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "XoaThanhCong") {
                            window.location.reload();
                        }
                        else {
                            alert("Lỗi xoá thông tin đơn hàng !");
                        }

                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=XoaTatCaDonHang&idKey=" + idKey + "&idKenhBan=" + getUrlVars(), true);
                xmlhttp.send();
            }
        }
        function ThemSanPham() {
            debugger

            var idKey = $("#ContentPlaceHolder1_txtidKey").val().trim();
            var MaDonHang = $("#txtMaDọnHang_Popup").val().trim();
            var idSanPham = $("#ContentPlaceHolder1_txtDATAVatTu").val().trim();
            var MaDoiSoat = $("#txtMaDoiSoat_Popup").val().trim();
            var GiaBan = $("#txtGiaBan_Popup").val().trim();
            var TenKhachHang = $("#txtTenKhachHang_Popup").val().trim();
            var SDT = $("#txtSDT_Popup").val().trim();
            if (MaDonHang == "")
                $("#ContentPlaceHolder1_slMaDonHang_Popup").notify("Vui lòng nhập mã đơn hàng !");
            if (GiaBan == "")
                $("#txtGiaBan_Popup").notify("Vui lòng nhập giá bán !");
            if (idSanPham == "")
                $("#ContentPlaceHolder1_slMaSanPham").notify("Vui lòng nhập mã sản phẩm !");
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "ThemThanhCong") {
                            window.location.reload();
                        }
                        else if (xmlhttp.responseText == "TrungMa") {
                            alert("Trùng Mã!");
                        }
                        else {
                            alert("Lỗi thêm sản phẩm đơn hàng kênh bán!");
                        }

                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=ThemDonHangKenhBan&MaDonHang=" + MaDonHang + "&MaDoiSoat=" + MaDoiSoat + "&GiaBan=" + GiaBan + "&TenKhachHang=" + TenKhachHang + "&SDT=" + SDT + "&idSanPham=" + idSanPham + "&idKey=" + idKey + "&idKenhBan=" + getUrlVars(), true);
                xmlhttp.send();
            }
        }

        function XoaSanPham(idSanPham) {
            if (confirm("Bạn có chắc muốn xoá ?")) {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "XoaThanhCong") {
                            window.location.reload();
                        }
                        else {
                            alert("Lỗi xoá thông tin đơn hàng !");
                        }

                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=XoaDonHang&idDonHang=" + idSanPham, true);
                xmlhttp.send();
            }
        }

        function SuaDonHang(idDonHang) {
            var MaDonHang = $("#txtMaDọnHang_Popup").val().trim();
            var MaDoiSoat = $("#txtMaDoiSoat_Popup").val().trim();
            var idSanPham = $("#txtMaSanPham").val().trim();
            var GiaBan = $("#txtGiaBan_Popup").val().trim();
            var TenKhachHang = $("#txtTenKhachHang_Popup").val().trim();
            var SDT = $("#txtSDT_Popup").val().trim();
            if (MaDonHang == "")
                $("#ContentPlaceHolder1_slMaDonHang_Popup").notify("Vui lòng nhập mã đơn hàng !");
            if (GiaBan == "")
                $("#txtGiaBan_Popup").notify("Vui lòng nhập giá bán !");
            if (idSanPham == "")
                $("#ContentPlaceHolder1_slMaSanPham").notify("Vui lòng nhập mã sản phẩm !");
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "SuaThanhCong") {
                            window.location.reload();
                        }
                        else {
                            alert("Lỗi sửa thông tin đơn hàng!");
                        }

                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=SuaDonHang&MaDonHang=" + MaDonHang + "&MaDoiSoat=" + MaDoiSoat + "&GiaBan=" + GiaBan + "&TenKhachHang=" + TenKhachHang + "&SDT=" + SDT + "&idSanPham=" + idSanPham + "&idDonHang=" + idDonHang, true);
                xmlhttp.send();
            }
        }
        function taimau() {
            window.location.href = "/MauExcel/MauCapNhatCuoiNgay.xlsx";
        }
        function taimau1() {
            window.location.href = "/MauExcel/MauCapNhatNeuLoi.xlsx";
        }
        $("#ContentPlaceHolder1_slMaSanPham").select2();
        $('.select2-selection__rendered').removeAttr('title');
        $('#').datetimepicker({
            //dayOfWeekStart : 1,
            //todayBtn: "linked",
            language: "it",
            autoclose: true,
            todayHighlight: true,
            timepicker: false,
            dateFormat: 'dd/mm/yyyy',
            format: 'd/m/Y',
            formatDate: 'Y/m/d',
            //value: 'today'
        });
        function hasUnicode(str) {
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) return true;
            }
            return false;
        }
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <script>
        $("#ContentPlaceHolder1_txtMauNen_Popup").select2();
        $('#ContentPlaceHolder1_txtTuNgay_Popup,#ContentPlaceHolder1_txtDenNgay_Popup,#ContentPlaceHolder1_seachNgay').datetimepicker({
            //dayOfWeekStart : 1,
            //todayBtn: "linked",
            language: "it",
            autoclose: true,
            todayHighlight: true,
            timepicker: false,
            dateFormat: 'dd/mm/yyyy',
            format: 'd/m/Y',
            formatDate: 'Y/m/d',
            //value: 'today'
        });
        function hasUnicode(str) {
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) return true;
            }
            return false;
        }
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
</asp:Content>


