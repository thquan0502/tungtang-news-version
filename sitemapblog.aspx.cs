﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

public partial class sitemapblog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string dateTime = DateTime.Now.ToString();
        string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
        Response.Clear();
        Response.ContentType = "text/xml";
        using (XmlTextWriter writer = new XmlTextWriter(Server.MapPath("sitemap/sitemap-blog.xml"), Encoding.UTF8))
        {
            writer.WriteStartDocument();
            // writer.WriteStartElement("?xml", "version='1.0' encoding='UTF - 8'");
            writer.WriteStartElement("urlset");
            writer.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writer.WriteAttributeString("xmlns:news", "http://www.google.com/schemas/sitemap-news/0.9");
            writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd http://www.google.com/schemas/sitemap-news/0.9 http://www.google.com/schemas/sitemap-news/0.9/sitemap-news.xsd");

            string dbstring = @"Data Source=125.253.127.73;Initial Catalog=tungtang;User ID=tungtang; Password=cu@61Fx3;Connect Timeout=10";
            SqlConnection connection = new SqlConnection(dbstring);
            connection.Open();
            //load trang chủ

            //load tin tức
            string sqltt = "select DuongDan,NgayDang,TieuDe,KichHoat from tb_TinTuc where KichHoat='True'";
            DataTable tbtt = Connect.GetTable(sqltt);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbtt.Rows.Count > 0)
            {
                // string date = tbtt.Rows[i]["DuongDan"].ToString();
                for (int i = 0; i < tbtt.Rows.Count; i++)
                {
                    string date = tbtt.Rows[i]["NgayDang"].ToString();
                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/blog/" + tbtt.Rows[i]["DuongDan"].ToString() + "");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(date).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("changefreq","daily");
                    writer.WriteElementString("priority", "0.9");
                    writer.WriteStartElement("news:news");
                    writer.WriteStartElement("news:publication");
                    writer.WriteElementString("news:name", "Tung Tăng");
                    writer.WriteElementString("news:language", "vi");
                    writer.WriteEndElement();
                    writer.WriteElementString("news:publication_date", Convert.ToDateTime(date).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("news:title", tbtt.Rows[i]["TieuDe"].ToString() + "");
                    writer.WriteEndElement();
                    writer.WriteEndElement();

                }
            }

            writer.WriteEndElement();
            writer.Flush();
        }
    }


}