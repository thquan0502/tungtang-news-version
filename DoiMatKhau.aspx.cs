﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangKy : System.Web.UI.Page
{
    string idThanhVien = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["TungTang_Login"] == null || Request.Cookies["TungTang_Login"].Value.Trim() == "")
        {
            Response.Redirect("/dang-nhap/dn");
        }
        else
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        if (!IsPostBack)
        {

        }
    }

    protected void btnLuuThayDoi_Click(object sender, EventArgs e)
    {
        string matkhau = txtMatKhau.Value.Trim();
        if (Connect.Exec("update tb_ThanhVien Set MatKhau='" + matkhau + "' where idThanhVien=" + idThanhVien))
        { 
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Đã thay đổi mật khẩu');");//nhận được thông báo xong mới refesh page
            Response.Write("document.location.href='../thong-tin/tt';");
            Response.Write("</script>");
        } 
    }
}