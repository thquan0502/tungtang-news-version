﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using ASPSnippets.GoogleAPI;
using ASPSnippets.FaceBookAPI;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Configuration;

#region Facebook and Google
public static class SocialNetworkType
{
    public const string FACEBOOK = "facebook";
    public const string GOOGLE = "google";
}
#endregion Facebook and Google

public partial class DangNhap : System.Web.UI.Page
{
    string Domain = "";
    //Khai báo biến
    string id;
    string name;
    string picture;
    string email;
    string tendn;
    DateTime ngaydangki = DateTime.Now;
    private int a = 0;
    string Refcode = "";
    //End khai báo biến
    protected void Page_Load(object sender, EventArgs e)
    {
        // get account id
        string accountId = Utilities.Account.getAuthAccountId(Request);
        if (!string.IsNullOrWhiteSpace(accountId))
        {
            Response.Redirect("/thong-tin-ca-nhan/ttcn");
        }

        // check is social response
        if (!this.IsPostBack)
        {
            this.checkSocialLogin();
        }

        HtmlLink canonical = new HtmlLink();
        canonical.Href = HttpContext.Current.Request.Url.OriginalString;
        canonical.Attributes["rel"] = "canonical";
        Page.Header.Controls.Add(canonical);

        Page.Header.Controls.Add(canonical); HtmlLink alternate = new HtmlLink();
        alternate.Href = HttpContext.Current.Request.Url.OriginalString;
        alternate.Attributes["rel"] = "alternate";
        Page.Header.Controls.Add(alternate);

        string sqlDomainWebsite = "select * from tb_Domain";
        DataTable tbDomainWebsite = Connect.GetTable(sqlDomainWebsite);
        if (tbDomainWebsite.Rows.Count > 0)
        {
            Domain = "";
        }
        try
        {
            if (Request.QueryString["dk"].Trim().ToUpper() == "TC")
            {
                dvDKTC.InnerHtml = "<b>Đăng ký thành công, bạn có thể đăng nhập để đăng tin!</b>";
            }
        }
        catch { }
        if (Request.Cookies["TungTang_Login"] != null && Request.Cookies["TungTang_Login"].Value.Trim() != "")
        {
            Response.Redirect("/");
        }

    }
    protected void btDangNhap_Click(object sender, EventArgs e)
    {
        string TenDangNhap = txtTenDangNhap.Value.Trim();
        string MatKhau = txtMatKhau.Value.Trim();
        if (TenDangNhap != "" && MatKhau != "")
        {
            DataTable tbCheckDangNhap = Connect.GetTable("select top 1 idThanhVien from tb_ThanhVien where TenDangNhap='" + TenDangNhap + "' and MatKhau='" + MatKhau + "' and isnull(isKhoa,'False')!='True'");
            if (tbCheckDangNhap.Rows.Count > 0)
            {
                HttpCookie cookie_TungTang_Login = new HttpCookie("TungTang_Login", tbCheckDangNhap.Rows[0]["idThanhVien"].ToString());
                cookie_TungTang_Login.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_TungTang_Login);
                Response.Redirect("../");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Tài khoản không hợp lệ!')", true);
                return;
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn chưa nhập số điện thoại hoặc mật khẩu!')", true);
            return;
        }
    }

    protected void btnfacebook_ServerClick(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM SocialData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        this.connectFacebook();
        FaceBookConnect.Authorize("user_photos,email", Request.Url.AbsoluteUri.Split('?')[0]);
    }

    protected void btngoogle_ServerClick(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM SocialData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        this.connectGoogle();
        GoogleConnect.Authorize("profile", "email");
    }

    /**
     * Check if the request is social's reponse
     */
    protected void checkSocialLogin()
    {
        if (Request.QueryString["error"] == "access_denied")
        {
            Session["register_error"] = "Xảy ra lỗi liên kết social media";
            return;
        }
        string code = Request.QueryString["code"];
        string socialNetwork = Request.QueryString["social_network"];
        if (code != null && !string.IsNullOrEmpty(code))
        {
            if (socialNetwork == "google") // is google response
            {
                // fetch data
                this.connectGoogle();
                string json = GoogleConnect.Fetch("me", code);
                GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);

                DataTable tableExisted = Utilities.Account.getTableByGoogleId(profile.Id);
                if (tableExisted.Rows.Count > 0) // if already connected facebook account before
                {
                    Utilities.Auth.attempLogin(Response, tableExisted.Rows[0]["idThanhVien"].ToString());
                }
                else // if it's firt time that connect to facebook account
                {
                    string sql = @"
                        INSERT INTO SocialData (
                            SessionId,
                            SocialType,
                            GoogleId,
                            GoogleName,
                            GooglePicture,
                            GoogleEmail,
                            GoogleVerifiedEmail,
                            CreatedAt
                        ) VALUES (
                            '" + Session.SessionID + @"',
                            'google',
                            '" + profile.Id + @"',
                            N'" + profile.Name + @"',
                            '" + profile.Picture + @"',
                            '" + profile.Email + @"',
                            '" + profile.Verified_Email + @"',
                            '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                        )
                    ";
                    Connect.Exec(sql);
                    Response.Redirect("/tai-khoan/social-media");
                }
            }
            else // is facebook response
            {
                this.connectFacebook();
                string json = FaceBookConnect.Fetch(code, "me?fields=id,name,email");
                FacebookProfile profile = new JavaScriptSerializer().Deserialize<FacebookProfile>(json);
                profile.Picture = string.Format("https://graph.facebook.com/{0}/picture", profile.Id);

                DataTable tableExisted = Utilities.Account.getTableByFacebookId(profile.Id);
                if (tableExisted.Rows.Count > 0) // if already connected google account before
                {
                    Utilities.Auth.attempLogin(Response, tableExisted.Rows[0]["idThanhVien"].ToString());
                }
                else // if it's firt time that connect to google account
                {
                    string sql = @"
                        INSERT INTO SocialData (
                            SessionId,
                            SocialType,
                            FacebookId,
                            FacebookName,
                            FacebookUserName,
                            FacebookPicture,
                            FacebookEmail,
                            CreatedAt
                        ) VALUES (
                            '" + Session.SessionID + @"',
                            'facebook',
                            '" + profile.Id + @"',
                            N'" + profile.Name + @"',
                            N'" + profile.UserName + @"',
                            '" + profile.Picture + @"',
                            '" + profile.Email + @"',
                            '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                        )
                    ";
                    Connect.Exec(sql);
                    Response.Redirect("/tai-khoan/social-media");
                }
            }
        }
    }

    /**
     * connect api google
     */
    protected void connectGoogle()
    {
        GoogleConnect.ClientId = ConfigurationManager.AppSettings["googleclientid"];
        GoogleConnect.ClientSecret = ConfigurationManager.AppSettings["googleclientsecret"];
        GoogleConnect.RedirectUri = Request.Url.AbsoluteUri.Split('?')[0] + "?social_network=google";
    }

    /**
     * connect api facebook
     */
    protected void connectFacebook()
    {
        FaceBookConnect.API_Key = ConfigurationManager.AppSettings["facebookapikey"];
        FaceBookConnect.API_Secret = ConfigurationManager.AppSettings["facebookapisecret"];
    }

    /**
     * Social response entities
     */
    #region Profile Facebook & Google
    public class FacebookProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Picture { get; set; }
        public string Email { get; set; }
    }
    public class GoogleProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Email { get; set; }
        public string Verified_Email { get; set; }
    }
    #endregion Profile Facebook & Google
}