﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ThongTinCaNhan : System.Web.UI.Page
{
    string Domain = "";
    string idThanhVien = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }

        {
            try
            {
                string[] Url = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "").Split('-');
                idThanhVien = StaticData.getField("Tb_ThanhVien", "idThanhVien", "TenDangNhap", Url[Url.Length - 1]).Trim();
                if (idThanhVien == "")
                {
                    try
                    {
                        idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
                    }
                    catch
                    {
                        Response.Redirect("/dang-nhap/dn");
                    }
                }
            }
            catch
            {
                try
                {
                    idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
                }
                catch
                {
                    Response.Redirect("/dang-nhap/dn");
                }
            }
        }
        LoadThongTinThanhVien();
        LoadTinDang();
    }
    void LoadThongTinThanhVien()
    {
        DataTable table = Connect.GetTable("select * from tb_ThanhVien where idThanhVien=" + idThanhVien);
        if (table.Rows.Count > 0)
        {
            txtNameShopID.InnerHtml = "<h4>" + (table.Rows[0]["TenCuaHang"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["TenCuaHang"]) + "</h4>";
            txtShopEmailID.InnerHtml = (table.Rows[0]["Email"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["Email"].ToString());
            txtShopPhoneID.InnerHtml = table.Rows[0]["TenDangNhap"].ToString();
            txtDateShopApprovedID.InnerHtml = (table.Rows[0]["NgayDangKy"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(table.Rows[0]["NgayDangKy"].ToString()));

            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim();
            string TenTinhThanh = StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString());
            string TenQuanHuyen = StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString());
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString());
            string DiaChi = table.Rows[0]["DiaChi"].ToString().Trim();
            if (DiaChi == "" && TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                txtShopAddressID.InnerHtml = "Chưa cung cấp";
            else
                txtShopAddressID.InnerHtml = DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh;

            string idThanhVien_Cookie = null;
            try
            {
                idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
            }
            catch { }
            if (idThanhVien != idThanhVien_Cookie)
            {
                btnEditInfoUserID.Visible = false;
            }
        }
    }
    private void LoadTinDang()
    {
        string html = "";
        string idThanhVien_Cookie = null;
        try
        {
            idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch { }
        if (idThanhVien == idThanhVien_Cookie)
        {
            string sqlTinDang = "select * from tb_TinDang where idThanhVien='" + idThanhVien + "' order by isHetHan asc";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            #region Mặt hàng của chính mình

            html += "<table class='table table-bordered' style='text-align:center;'>";
            for (int i = 0; i < tbTinDang.Rows.Count; i++)
            {
                //Hình ảnh
                string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + tbTinDang.Rows[i]["idTinDang"].ToString() + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string urlHinhAnh = "";
                if (tbHinhAnh.Rows.Count > 0)
                {
                    if (System.IO.File.Exists(Server.MapPath("/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"])))
                        urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"].ToString();
                    else
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                }
                else
                    urlHinhAnh = Domain + "/images/icons/noimage.png";

                html += "     <tr>";
                html += "         <td style='width:200px;'>"; string TieuDeTinDang = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDang.Rows[i]["TieuDe"].ToString().Trim()));
                html += "                  <a href='" + "/tdct/" + tbTinDang.Rows[i]["idTinDang"] + "-" + TieuDeTinDang + "'><img src='" + urlHinhAnh + "' style='object-fit:cover;height:100px;' /> </a>";
                html += "         </td>";
                html += "         <td style='text-align:left;'>";
                html += "           <div style='width:250px;'>";
                html += "             <p onclick=\"" + "javascript:window.location.href='" + "/tdct/" + tbTinDang.Rows[i]["idTinDang"] + "-" + TieuDeTinDang + "'\" style='padding-top:5px;margin: 0 0 10px;cursor:pointer;'><b>" + tbTinDang.Rows[i]["TieuDe"].ToString() + "</b></p>";
                if (tbTinDang.Rows[i]["TuGia"].ToString() != "")
                    html += "             <p>Giá: " + double.Parse(tbTinDang.Rows[i]["TuGia"].ToString()).ToString("#,##").Replace(",", ".");
                html += "             </p>";
                if (tbTinDang.Rows[i]["NgayDang"].ToString() != "")
                    html += "             <p>Ngày đăng: " + DateTime.Parse(tbTinDang.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy") + "</p>";
                html += "           </div>";
                html += "          </td>";
                html += "         <td>";
                if (tbTinDang.Rows[i]["isDuyet"].ToString() == "")
                    html += "             Đang chờ ...";
                if (tbTinDang.Rows[i]["isDuyet"].ToString() == "True")
                    html += "             Đã duyệt";
                if (tbTinDang.Rows[i]["isDuyet"].ToString() == "False")
                    html += "             Không duyệt";
                html += "         </td>";
                html += "         <td>";
                html += "           <select id='slHetHan_" + tbTinDang.Rows[i]["idTinDang"] + "' disabled='disabled' class='form-control' style='width:122px'>";
                if (tbTinDang.Rows[i]["isHetHan"].ToString() != "True")
                {
                    html += "           <option value='ConHieuLuc' selected='selected'>Còn hạn</option>";
                    html += "           <option value='HetHan'>Hết hạn</option>";
                }
                else
                {
                    html += "           <option value='ConHieuLuc'>Còn hạn</option>";
                    html += "           <option value='HetHan' selected='selected'>Hết hạn</option>";
                }
                html += "           </select>";
                html += "         </td>";
                html += "         <td style='width: 100px;'><a id='btHetHan_" + tbTinDang.Rows[i]["idTinDang"].ToString() + "' style='cursor:pointer' onclick='CapNhatHetHan(\"" + tbTinDang.Rows[i]["idTinDang"].ToString() + "\")'><img class='imgedit' src='../images/icons/edit.png' style='width:25px; cursor:pointer'/>Sửa hạn</a></td>";
                html += "         <td style='width: 100px;'>";
                html += "             <a href='" + Domain + "/dang-tin/dt?TDTD=" + tbTinDang.Rows[i]["idTinDang"].ToString() + "'><img src='../images/icons/edit.png' style='width:25px; cursor:pointer' />Sửa tin</a>";
                html += "         </td>";
                html += "         <td style='width: 100px;'><a href='javascript:ThongKe(" + tbTinDang.Rows[i]["idTinDang"] + ");'>Thống kê</a></td>";
                html += "     </tr>";
            }
            html += "</table>";
            #endregion
        }
        else
        {
            string sqlTinDang = "select * from tb_TinDang where idThanhVien='" + idThanhVien + "'  and isDuyet='1'  order by isHetHan asc";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            #region Mặt hàng của người khác
            html += "<ul>";
            for (int i = 0; i < tbTinDang.Rows.Count; i++)
            {
                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDang.Rows[i]["TieuDe"].ToString().Trim()));
                string urlTinDangChiTiet = Domain + "/tdct/" + tbTinDang.Rows[i]["idTinDang"].ToString() + "-" + TieuDeSau;
                html += "<li class='hot-job' style='position:relative;'>";
                html += "   <a href='" + urlTinDangChiTiet + "' title='" + tbTinDang.Rows[i]["TieuDe"].ToString() + " - Tung Tăng'>";
                if (tbTinDang.Rows[i]["isHot"].ToString() == "True")
                    html += "<div class='hot-tag'>HOT</div>";
                html += "      <span class='score'>";
                string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + tbTinDang.Rows[i]["idTinDang"].ToString() + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string urlHinhAnh = "";
                if (tbHinhAnh.Rows.Count > 0)
                {
                    if (System.IO.File.Exists(Server.MapPath("/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"])))
                        urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"];
                    else
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                }
                else
                    urlHinhAnh = Domain + "/images/icons/noimage.png";

                html += "          <img src='" + urlHinhAnh + "' class='imgmenu' />";
                html += "      </span>";
                html += "      <span class='job-title'>";

                if (tbTinDang.Rows[i]["isHot"].ToString() == "True")
                    html += "           <strong class='text-clip' style='font-weight:600;'>" + tbTinDang.Rows[i]["TieuDe"] + "</strong>";
                else
                    html += "           <strong class='text-clip'>" + tbTinDang.Rows[i]["TieuDe"] + "</strong>";
                string TenHuyen1 = StaticData.getField("District", "Ten", "Id", tbTinDang.Rows[i]["idHuyen"].ToString());
                string idTinh1 = tbTinDang.Rows[i]["idTinh"].ToString();
                string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
                string idPhuongXa1 = tbTinDang.Rows[i]["idPhuongXa"].ToString();
                string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
                string chuoi_DiaDiem = "";
                if (TenPhuongXa1 != "")
                    chuoi_DiaDiem = TenPhuongXa1 + ", ";
                if (TenHuyen1 != "")
                    chuoi_DiaDiem += TenHuyen1 + ", ";
                if (TenTinh1 != "")
                    chuoi_DiaDiem += TenTinh1;

                if (TenPhuongXa1 == "" && TenHuyen1 == "" && TenTinh1 == "")
                    chuoi_DiaDiem = "Toàn quốc";
                html += "<em class='text-clip'><i class='fa fa-map-marker' style='width: 15px;padding: 1px 0 0 3px;'></i> : " + chuoi_DiaDiem + "</em>";

                html += "<em class='text-clip'><i class='fa fa-usd' style='width: 15px;padding: 3px;'></i> : <span style='color:red;font-weight:600;'>";
                if (tbTinDang.Rows[i]["TuGia"].ToString() != "")
                    html += double.Parse(tbTinDang.Rows[i]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + "đ";
                html += "</span></em>";
                html += "           <em class='text-clip'><i class='fa fa-calendar' style='width: 15px;padding: 1px;'></i> : " + DateTime.Parse(tbTinDang.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy") + "</em>";
                html += "      </span>";
                html += "   </a>";
                html += "</li>";
            }
            html += "</ul>";
            #endregion
        }
        dvQuanLyTinDang.InnerHtml = html;
    }
}