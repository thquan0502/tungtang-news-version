﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class MasterPage : System.Web.UI.MasterPage
{
    string Domain = "";
    string idDanhMucCap1 = "";
    string idDanhMucCap2 = "";
    string idTinh = "";
    string idHuyen = "";
    string idPhuongXa = "";
    string DiaChi = "";
    string TenCuaHang = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("iPhone")))
        //{
        //    Response.Write("<script>alert('iphone')</script>");
        //}
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        LoadDangNhap();

        if (!IsPostBack)
        {
            string Url = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "");

            string[] arrUrl = Url.Split('/');

            //LoadDanhMucChinh();
            //Get tiêu chí tìm kiếm
            try
            {
                if (arrUrl[2] != null && arrUrl[2].Trim() != "")
                {
                    string[] arrTinDang = arrUrl[2].Trim().Split('-');
                    if (arrTinDang[0] == "1")
                        idDanhMucCap1 = arrTinDang[1];
                    if (arrTinDang[0] == "2")
                    {
                        idDanhMucCap1 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "idDanhMucCap2", arrTinDang[1]);
                        idDanhMucCap2 = arrTinDang[1];
                    }
                }
            }
            catch { }

        }
    }
    private void LoadDangNhap()
    {
        if (Request.Cookies["TungTang_Login"] != null && Request.Cookies["TungTang_Login"].Value.Trim() != "")
        {
            string html = "";
            string idThanhVien = Request.Cookies["TungTang_Login"].Value.ToString();
            string sqlThanhVien = "select * from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
            DataTable tbThanhVien = Connect.GetTable(sqlThanhVien);
            if (tbThanhVien.Rows.Count > 0)
            {
                //html += "<a style='margin-top:6px' href='" + Domain + "/thong-tin-ca-nhan/ttcn'><img src='" + Domain + "/images/icons/profile.png' style='width: 20px;margin-top: -2px;'> Tin đăng của " + tbThanhVien.Rows[0]["TenCuaHang"].ToString().Split(' ')[tbThanhVien.Rows[0]["TenCuaHang"].ToString().Split(' ').Length - 1] + "</a>";
                html += "<a  href='" + Domain + "/thong-tin-ca-nhan/ttcn'><i class='fa fa-clipboard' style='font-size: 25px;margin-right:5px;'></i> Quản lý tin đăng</a>";
            }
            liDangNhap.InnerHtml = html;
            liDangKy.InnerHtml = "<a style='cursor:pointer;' onclick='DangXuat()'><i class='fa fa-sign-out' style='font-size: 25px;margin-right:5px;'></i> Đăng xuất</a>";

            string html_NavBar = @" <div class='side-nav' ondragenter=" + "\"" + "OpenNavSide('hide');" + "\"" + @">
                                        <a href='/'>Trang chủ</a>
                                        <a href='/thong-tin-ca-nhan/ttcn'>Quản lý tin đăng</a>
                                        <a href='/tin-dang/td'>Tìm tin đăng khác</a>
                                        <a href='/dang-tin/dt'>Đăng tin</a>
                                        <a href='/thue-an-toan/tat'>Giới thiệu</a>
                                        <a href='#' onclick='DangXuat();'>Đăng xuất</a>
                                    </div>";
            td_NAV.InnerHtml = html_NavBar;

        }
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
    }
}