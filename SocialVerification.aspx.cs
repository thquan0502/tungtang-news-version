﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using ASPSnippets.GoogleAPI;
using ASPSnippets.FaceBookAPI;
using System.Configuration;

public partial class SocialVerification : System.Web.UI.Page
{
    protected string pageMode = null;
    protected string dataSocialType = null;
    protected string dataGoogleId = null;
    protected string dataGoogleName = null;
    protected string dataGooglePicture = null;
    protected string dataGoogleEmail = null;
    protected string dataGoogleVerifiedEmail = null;
    protected string dataFacebookId = null;
    protected string dataFacebookName = null;
    protected string dataFacebookUserName = null;
    protected string dataFacebookPicture = null;
    protected string dataFacebookEmail = null;
    protected DateTime dataCreatedAt = DateTime.MinValue;
    protected string dataInputPass = null;
    protected string dataSmsPhone = null;
    protected string dataSmsCode = null;
    protected string dataSmsResponse = null;
    protected string dataSmsMessage = null;
    protected DateTime dataSmsSentAt = DateTime.MinValue;
    protected long diffSeconds = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        // get account id
        string accountId = Utilities.Account.getAuthAccountId(Request);
        if (!string.IsNullOrWhiteSpace(accountId))
        {
            Response.Redirect("/thong-tin-ca-nhan/ttcn");
        }

        // fetch data
        string sql = @"
            SELECT TOP 1
                SocialData.*
            FROM SocialData
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        DataTable table = Connect.GetTable(sql);
        if (table.Rows.Count > 0)
        {
            this.dataSocialType = table.Rows[0]["SocialType"].ToString();
            this.dataGoogleId = table.Rows[0]["GoogleId"].ToString();
            this.dataGoogleName = table.Rows[0]["GoogleName"].ToString();
            this.dataGooglePicture = table.Rows[0]["GooglePicture"].ToString();
            this.dataGoogleEmail = table.Rows[0]["GoogleEmail"].ToString();
            this.dataGoogleVerifiedEmail = table.Rows[0]["GoogleVerifiedEmail"].ToString();
            this.dataFacebookId = table.Rows[0]["FacebookId"].ToString();
            this.dataFacebookName = table.Rows[0]["FacebookName"].ToString();
            this.dataFacebookUserName = table.Rows[0]["FacebookUserName"].ToString();
            this.dataFacebookPicture = table.Rows[0]["FacebookPicture"].ToString();
            this.dataFacebookEmail = table.Rows[0]["FacebookEmail"].ToString();
            if (!string.IsNullOrWhiteSpace(table.Rows[0]["CreatedAt"].ToString()))
            {
                this.dataCreatedAt = (DateTime)table.Rows[0]["CreatedAt"];
            }
            this.dataInputPass = table.Rows[0]["InputPass"].ToString();
            this.dataSmsPhone = table.Rows[0]["SmsPhone"].ToString();
            this.dataSmsCode = table.Rows[0]["SmsCode"].ToString();
            this.dataSmsResponse = table.Rows[0]["SmsResponse"].ToString();
            this.dataSmsMessage = table.Rows[0]["SmsMessage"].ToString();
            if (!string.IsNullOrWhiteSpace(table.Rows[0]["SmsSentAt"].ToString()))
            {
                this.dataSmsSentAt = (DateTime)table.Rows[0]["SmsSentAt"];
                this.diffSeconds = (long)(DateTime.Now - this.dataSmsSentAt).TotalSeconds;
            }
        }
        else
        {
            Response.Redirect("/dang-nhap/dn");
        }

        if (!IsPostBack)
        {
            // identify mode
            if (string.IsNullOrWhiteSpace(this.dataSmsCode))
            {
                this.pageMode = "INPUT";
            }
            else
            {
                this.pageMode = "VERIFICATION";
            }

            // reset elements
            this.txtPass.Attributes["type"] = "password";
            this.txtPass2.Attributes["type"] = "password";
            this.divInput.Visible = false;
            this.divVerification.Visible = false;

            // process page
            switch (this.pageMode)
            {
                case "INPUT":
                    this.divInput.Visible = true;
                    break;
                case "VERIFICATION":
                    this.divVerification.Visible = true;
                    break;
                default:
                    Response.Redirect("/error/404");
                    break;
            }
        }
    }

    /**
     * handle register click
     */
    protected void btnInput_Click(object sender, EventArgs e)
    {
        // get input values
        string inputPhone = this.txtPhone.Text.Trim();
        string inputPass = this.txtPass.Text.Trim();
        string inputPass2 = this.txtPass2.Text.Trim();

        // check required
        if (
            string.IsNullOrWhiteSpace(inputPhone)
            || string.IsNullOrWhiteSpace(inputPass)
            || string.IsNullOrWhiteSpace(inputPass2)
        )
        {
            Session["social_verification_error"] = "Hãy điền đủ các thông tin";
            return;
        }

        // check phone number contains digits only
        if (!Utilities.Validation.isDigitsOnly(inputPhone))
        {
            Session["social_verification_error"] = "Số điện thoại không đúng định dạng";
            return;
        }

        // check phone format
        if (!inputPhone.StartsWith("0"))
        {
            Session["social_verification_error"] = "Số điện thoại không đúng định dạng";
            return;
        }

        // check phone number length
        if (inputPhone.Length > 10)
        {
            Session["social_verification_error"] = "Số điện thoại không hợp lệ";
            return;
        }

        // check existed account with phone number
        if (!string.IsNullOrWhiteSpace(Utilities.Account.getIdByPhone(inputPhone)))
        {
            Session["social_verification_error"] = "Số điện thoại đã được sử dụng";
            return;
        }

        // check password contain accented characters
        if (Utilities.Validation.containAccented(inputPass))
        {
            Session["social_verification_error"] = "Mật khẩu không được có dấu";
            return;
        }

        // check retype password
        if (inputPass != inputPass2)
        {
            Session["social_verification_error"] = "Mật khẩu không khớp nhau";
            return;
        }

        // check terms and privacy
        if(!this.chbxPrivacy.Checked)
        {
            Session["social_verification_error"] = "Vui lòng đồng ý chấp nhận điều khoản và chính sách bảo mật";
            return;
        }

        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(inputPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // save db
        string sql = @"
            UPDATE SocialData
            SET
                InputPass = '" + inputPass + @"',
                SmsPhone = '" + inputPhone + @"',
                SmsCode = '" + code + @"',
                SmsResponse = '" + res + @"',
                SmsMessage = '" + message + @"',
                SmsSentAt = '" + sNow + @"'
            WHERE SessionId = '"+ Session.SessionID +@"'
        ";
        Connect.Exec(sql);

        Response.Redirect("/tai-khoan/social-media");
    }

    /**
     * handle verify click
     */
    protected void btnVerify_Click(object sender, EventArgs e)
    {
        // get input values
        string inputCode = this.txtCode.Text.Trim();

        // check code
        if(inputCode != dataSmsCode)
        {
            Session["social_verification_errors"] = "Mã xác thực không chính xác";
            return;
        }

        // remove register data
        string sql = @"
            DELETE FROM SocialData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // save new account
        string sql2;
        if(this.dataSocialType == "google")
        {
            sql2 = @"
                INSERT INTO tb_ThanhVien (
                    TenCuaHang,
                    SoDienThoai,
                    TenDangNhap,
                    MatKhau,
                    Email,
                    LinkAnh,
                    GoogleId,
                    NgayDangKy,
                    RefCode,
                    IsVerified,
					TongTinDang
                ) OUTPUT INSERTED.idThanhVien VALUES (
                    N'" + this.dataGoogleName + @"',
                    '" + this.dataSmsPhone + @"',
                    '" + this.dataSmsPhone + @"',
                    '" + this.dataInputPass + @"',
                    '" + this.dataGoogleEmail + @"',
                    '" + this.dataGooglePicture + @"',
                    '" + this.dataGoogleId + @"',
                    '" + this.dataCreatedAt.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                    '" + System.Guid.NewGuid() + @"',
                    '1',
					'0'
                )
            ";
        }
        else
        {
            sql2 = @"
                INSERT INTO tb_ThanhVien (
                    TenCuaHang,
                    SoDienThoai,
                    TenDangNhap,
                    MatKhau,
                    Email,
                    LinkAnh,
                    GoogleId,
                    NgayDangKy,
                    RefCode,
                    IsVerified,
				    TongTinDang
                ) OUTPUT INSERTED.idThanhVien VALUES (
                    N'" + this.dataFacebookName + @"',
                    '" + this.dataSmsPhone + @"',
                    '" + this.dataSmsPhone + @"',
                    '" + this.dataInputPass + @"',
                    '" + this.dataFacebookEmail + @"',
                    '" + this.dataFacebookPicture + @"',
                    '" + this.dataFacebookId + @"',
                    '" + this.dataCreatedAt.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                    '" + System.Guid.NewGuid() + @"',
                    '1',
					'0'
                )
            ";
        }
        DataTable table2 = Connect.GetTable(sql2);
        string accountId = table2.Rows[0]["idThanhVien"].ToString();
        Utilities.ViewedManager.createInstanceForAccount(accountId);
        Utilities.CommissionManager.createInstanceForAccount(accountId);
        Utilities.FeeManager.createInstanceForAccount(accountId);

        // login
        Session["success"] = "Chúc mừng! Bạn đã liên kết tài khoản thành công";
        Utilities.Auth.attempLogin(Response, accountId);
    }

    /**
     * handle re-send click
     */
    protected void btnResend_Click(object sender, EventArgs e)
    {
        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(dataSmsPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // data register data
        string sql = @"
            UPDATE SocialData
            SET
                SmsCode = '" + code + @"',
                SmsResponse = '" + res + @"',
                SmsMessage = '" + message + @"',
                SmsSentAt = '" + sNow + @"'
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // end
        Response.Redirect("/tai-khoan/social-media");
    }

    /**
     * handle re-input click
     */
    protected void btnReinput_Click(object sender, EventArgs e)
    {
        string sql = @"
            UPDATE SocialData
            SET
                InputPass = NULL,
                SmsPhone = NULL,
                SmsCode = NULL,
                SmsResponse = NULL,
                SmsMessage = NULL,
                SmsSentAt = NULL
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        Response.Redirect("/tai-khoan/social-media");
    }
}