﻿<%@ Page Title="Lịch sử giao dịch" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="BalanceHistory.aspx.cs" Inherits="BalanceHistory" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	
	<script src="../Js/Page/TinDang.min.js"></script>

    <link href="../Css/Page/ThongTinCaNhan.css" rel="stylesheet" />
    <link href="../Css/Page/AccountPost.css" rel="stylesheet" />
	<link href="/asset/css/zalo/style.css" rel="stylesheet" />
    <script src="../Js/Page/ThongTinCaNhan.min.js"></script>
    <link href="../Css/favicoin.css" rel="stylesheet" />
    <link href="/Css/Page/BalanceSpents.css" rel="stylesheet" />

	<script src="/Js/Page/BalanceSpents.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-content">
        <div class="container WrapperContainer">
            <div class="breadcrumWrapper" style="margin-top: 10px;">
                <ul class="sc-jzJRlG cIiMVT">
                    <li class="sc-cSHVUG jbltJC"><a href="/" class="sc-kAzzGY eZQMkE">Trang chủ</a></li>
                    <li class="sc-cSHVUG jbltJC"><a class="sc-kAzzGY llFQGq">Trang cá nhân</a></li>
                </ul>
            </div>
            <div class="PaperContainer contactInfo false">
                <div class="PaperInfoWrapper" style="color: rgba(0, 0, 0, 0.87); background-color: #ffffff; transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Verdana, Arial, sans-serif; -webkit-tap-highlight-color: rgba(0,0,0,0); box-shadow: 0 1px 2px rgba(0,0,0,.1); border-radius: 2px">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 BasicInfo">
                            <div class="AvatarWrapper">
                                <img id="imgLinkAnh" runat="server" src="/images/icons/signin.png" style="color: #ffffff; border: .25px solid #ffba00; user-select: none; display: inline-flex; align-items: center; justify-content: center; font-size: 40px; border-radius: 50%; height: 80px; width: 80px"
                                    size="80">
                            </div>
                            <div class="InfoWrapper">
                                <span class="name" id="txtNameShopID" runat="server"></span>
                                <span><p id="txtInfoAccountCode" class="color-yellow" runat="server" style="font-weight: 600;display:inline-block;"></p></span><br />
                                <span><p style="display: inline-block;">Số dư: </p><p id="balance" runat="server" style="font-weight: 600;color:#4cb050;display: inline-block;"></p><img class="appWrapper-Header-icon" src="../images/TT.png" alt="Quản lý Tài chính"></span><br />
                                <p style="display: inline-block;">Loại TV: </p>
                                <h6 id="txtLoaiUser" runat="server" style="font-weight: 600;color:#4cb050;display: inline-block;"></h6>
                                 <img id="ImagePro" runat="server" src="/images/vuongniem_tungtang.png" alt="Thành viên Pro Tung Tăng" style="width: 21px;margin-top: -17px;margin-left: 3px;display:none;" />
                                <div class="UltiRow" id="ProNow" runat="server" style="margin-bottom:8px;">
                                    <span>
                                   <a class="MainFunctionButton EditProfile" id="buttonUser" style="background-color:#4cb050;color:white;margin-bottom:10px;font-size: 9px;" href="/thong-tin/pro" runat="server">Đăng ký PRO</a>    
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ExtraInfo">
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày tham gia:<span id="txtDateShopApprovedID" runat="server"></span>
                            </div>
                            <div class="itemRow" style="height:auto;display:block;">
                                <i class="fa fa-map-marker" style="padding: 0 13px;"></i>Địa chỉ:<span id="txtShopAddressID" runat="server" style="white-space:normal;"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-phone" style="padding: 0 11px;"></i>Số điện thoại:<span id="txtShopPhoneID" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-envelope" style="padding: 0 10px;"></i>Email:<span id="txtShopEmailID" runat="server"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
            <% if (!string.IsNullOrWhiteSpace(rechargeSuccessMessage))
                { %>
            <script>
                Swal.fire('Thành công', '<%= rechargeSuccessMessage %>', 'success');
            </script>
            <% } %>

            <div class="container indexPage PaperContainer" style="height:auto">	
                <div class="PaperWrapper app-box" >	
                    <div class="TitleHeading" style="margin-top:-17px;">
                        <h4><span id="TitleHeader" style="color:#33a837;font-weight: bold;font-size: 13px;" runat="server">LỊCH SỬ GIAO DỊCH</span></h4>
                        <div id="btnTin" runat="server">
                            <a href="/tai-khoan/thong-tin-vi" class="btn btn-success btndaduyet"><p class="btnTabql">Thông tin về ví<span class="titlethh" style="color:white"></span></p></a>	
                            <a href="/tai-khoan/lich-su-giao-dich" class="btn btn-info btnchoduyet colordanhmuc"><p class="btnTabql">Lịch sử giao dịch<span class="titlethh" style="color:white"></span></p></a>	
                            <a href="/tai-khoan/tai-chinh-ho-tro" class="btn btn-danger btnbituchoi"><p class="btnTabql">Kháng nghị và hỗ trợ<span class="titlethh" style="color:white"></span></p></a>
                        </div>
                        <div class="row list">
                            <div style="padding: 8px 0px 8px 0px; width: 100%;">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card account-filter-card">
                                                    <div class="filter-bar d-flex flex-row mt-3">
                                                        <div class="filter-bar-pagination flex-grow-1 d-flex justify-content-start align-items-center">
                                                            <label class="text-sm">Phân trang:&nbsp;&nbsp;</label>
                                                            <div id="divPagination2" class="app-table-pagination d-flex justify-content-end align-items-center" runat="server"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-scrollable app-table-wrap mt-5" id="divTable" runat="server"></div>
                                        <div id="divPagination1" class="app-table-pagination" runat="server"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

