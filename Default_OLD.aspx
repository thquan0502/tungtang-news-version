﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default_OLD.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        window.onload = function () {
            //document.getElementById("search-widget").style = "display:none";
        }
    </script>
    <style type="text/css">
        a > div > p {
            position: absolute;
            top: 0;
            left: 0;
            z-index: 1;
            padding: 10px;
            font-size: 15px;
            font-weight: 500;
            color: white;
            width: 100%;
            height: 100%;
            background: linear-gradient(to bottom, rgba(0,0,0,0.65) 0%, transparent 60%, transparent 100%);
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container">
        <div class="row">
            <!--Top Jobs-->
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div class="panel jobs-board-listing with-mc no-padding no-border" style="background-color:unset;">
                        <div id="dvBannerTrangChu" runat="server" class="dvBannerTrangChucls">
                        </div>
                        <div id="dvSubBanner" runat="server" class="dvSubBannercls" >
                            <div class="banner-left">
                                <img src="/images/Category/sub-banner.png" /></div>
                            <div class="banner-right">
                                <div style="text-align:center;">
                                    <h2 class="banner-h2">Đăng nhập để không bỏ lỡ món đồ giá tốt!</h2>
                                    <a class="banner-a1" href="/dang-nhap/dn" title="Đăng nhập ngay">Đăng nhập ngay</a>
                                    <a class="banner-a2" href="/dang-ky/dk" title="Đăng ký tài khoản">Đăng ký tài khoản</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-content" id="tabCanThue">
                            <div class="job-list scrollbar m-t-lg">
                                <div class="title">Khám phá danh mục</div>
                                <div id="dvKhamPhaDanhMuc" runat="server" class="hot-job" style="border-bottom: none; padding-bottom: 0px; padding-top: 0px;">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Campaign-->
</asp:Content>
