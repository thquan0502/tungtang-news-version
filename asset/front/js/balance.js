﻿$(document).ready(function(){
    let finalPrice = 0;
    let finalCode = undefined;

    const updateFinalPrice = function (price, code) {
        let text = "";
        if (price) {
            text += price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
            text += " VND";
        } else {
            price = 0;
        }
        finalPrice = price;
        finalCode = code;
        $("#modalAddBalance .price-final").text(text);
    }

    updateFinalPrice();

    const fnCheck = function () {
        const method = $(`#modalAddBalance input[name="method"]:checked`).val();
        $(".modal-add-balance .detail").removeClass("active");
        $(`.modal-add-balance .${method}-detail`).addClass("active");
    }
    $(`#modalAddBalance input[name="method"]`).on("change", function () {
        fnCheck();
    });
    fnCheck();

    // click on recharge package
    $(".appjs-add-balance-btn").on("click", function (event) {
        event.preventDefault();
        let price = $(this).data("price");
        let code = $(this).data("code");
        price = parseFloat(price);
        updateFinalPrice(price, code);
        $("#modalAddBalance").modal();
    });

    // when click vnpay recharge
    $("#modalAddBalance .btn-vnpay").on('click', function () {
        if (!finalCode) {
           return;
        }
        $("#GFake_MasterPageNew_VnpSubmit_Code").val(finalCode);
        $("#GFake_MasterPageNew_VnpSubmit_Submit").click();
    });
});