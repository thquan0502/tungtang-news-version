﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangKy : System.Web.UI.Page
{
    string idThanhVien = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["TungTang_Login"] == null || Request.Cookies["TungTang_Login"].Value.Trim() == "")
        {
            Response.Redirect("/dang-nhap/dn");
        }
        else
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        if (!IsPostBack)
        {
			this.txtPassOld.Attributes["type"] = "password";
            this.txtPass.Attributes["type"] = "password";
            this.txtPass2.Attributes["type"] = "password";
        }
    }

    protected void btnLuuThayDoi_Click(object sender, EventArgs e)
    {
        string MatKhauOLD = this.txtPassOld.Text.Trim();
        string MatKhauMoi = this.txtPass.Text.Trim();
        string ReviewMK = this.txtPass2.Text.Trim();
        //string matkhau = txtMatKhau.Value.Trim();
		
		 MessPassOld.InnerText = "";
           MessPass.InnerText = "";
          MessPass1.InnerText = "";

        
       if (
             string.IsNullOrWhiteSpace(MatKhauOLD) 
         )
        {
            MessPassOld.InnerText = "Vui lòng nhập mật khẩu cũ!";
            return;
        }
        else
        {
            string sqlquery = "Select MatKhau from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
            DataTable tb = Connect.GetTable(sqlquery);
            if (tb.Rows.Count > 0)
            {
                if (MatKhauOLD != tb.Rows[0]["MatKhau"].ToString())
                {
                     MessPassOld.InnerText = "Mật khẩu cũ không đúng.Vui lòng nhập lại!";
                    return;
                }
            }
        }
        if (
             string.IsNullOrWhiteSpace(MatKhauMoi)
         )
        {
            MessPass.InnerText = "Vui lòng nhập mật khẩu mới!";
            return;
        }
        if (
             string.IsNullOrWhiteSpace(ReviewMK)
         )
        {
            MessPass1.InnerText = "Vui lòng nhập lại mật khẩu!";
            return;
        }
        if (Utilities.Validation.containAccented(MatKhauMoi))
        {
            MessPass.InnerText = "Mật khẩu không được có dấu.Vui lòng nhập lại!";
            return;
        }
       if (MatKhauMoi != ReviewMK)
        {
            MessPass1.InnerText = "Mật khẩu không khớp.Vui lòng nhập lại!";
            return;
        }
        string sqluser = @"
            UPDATE tb_ThanhVien
            SET
                MatKhau = '" + MatKhauMoi + @"'
            WHERE idThanhVien = '" + idThanhVien + @"'
        ";
        Connect.Exec(sqluser);
            Response.Write("<script type='text/javascript'>");
            Response.Write("alert('Đã thay đổi mật khẩu thành công!');");//nhận được thông báo xong mới refesh page
            Response.Write("document.location.href='/thong-tin-ca-nhan/ttcn';");
            Response.Write("</script>");
    }
}