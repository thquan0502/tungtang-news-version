﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class TinDangChiTiet : System.Web.UI.Page
{
    string idTinDang = "";
    string Domain = "";
    string idThanhVien = "";
    string DuongDan = "";
    string metaTitle = "";
    string metaUrl = "";
    string metaHinhAnh = "";
    string metaDescription = "";
    string metaType = "";
    string metaTwtitle = "";
    string metaTwdes = "";
    string metaTwimg = "";
    string metaTwcard = "";
    string metaIgurl = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlLink canonical = new HtmlLink();
        canonical.Href = HttpContext.Current.Request.Url.OriginalString;
        canonical.Attributes["rel"] = "canonical";
        Page.Header.Controls.Add(canonical);

        Page.Header.Controls.Add(canonical); HtmlLink alternate = new HtmlLink();
        alternate.Href = HttpContext.Current.Request.Url.OriginalString;
        alternate.Attributes["rel"] = "alternate";
        Page.Header.Controls.Add(alternate);
        
        if (!IsPostBack)
        {
            try
            {
                HttpContext context = HttpContext.Current;
                //string[] Action = context.Items["Action"].ToString().Split('-');

                // change
                DuongDan = context.Items["Action"].ToString();


                // idTinTuc = Action[0];
                //idTinTuc = Action[Action.Length - 1];
                // idTinTuc = Request.QueryString["idTinTuc"].ToString();
                //if (Page <= 0)
                //{
                //    Response.Redirect(Domain + "/tin-dang/td");
                //}
            }
            catch
            {

            }

            //if (idTinTuc != "")
            //     LoadTinDang();

            //string sql = "";
            //sql += @"select * from
            //       tb_TinTuc  
            //where  KichHoat='1' and DuongDan = '" + DuongDan + @"' ";
            //DataTable table = Connect.GetTable(sql);
            //if (table.Rows.Count > 0)
            //{
            //    for (int i = 0; i < table.Rows.Count; i++)
            //    {
            //        string TieuDeSau = table.Rows[i]["TieuDe"].ToString().Trim();
            //        dvTieuDe.InnerHtml = "<a href='/' class='aTC'>Trang chủ</a> > " + TieuDeSau;
            //        innerscript += "<script>";
            //        innerscript += "document.getElementsByTagName('title')[0].innerHTML='" + TieuDeSau + "';";

            //        innerscript += "</script>";
            //        //string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + table.Rows[i]["idTinDang"].ToString() + "'";
            //        // DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            //        string MoTa = table.Rows[i]["MoTa"].ToString();

            //        html += @"

            //                  <center><h2>" + TieuDeSau + @"</h2></center>

            //                " + MoTa + @"
            //                      ";
            //        Title = table.Rows[i]["TieuDe"].ToString();
            //        MetaDescription = table.Rows[i]["TieuDe"].ToString();


            //    }

            if (DuongDan != "" && DuongDan != null)
            {
                LoadTinDang();
                LoadTinCungDanhMuc();
                foreach (HtmlMeta metaTag in Page.Header.Controls.OfType<HtmlMeta>())
                {
                    if (metaTag.Name.Equals("ogtitle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTitle;
                    }
                    if (metaTag.Name.Equals("ogurl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaUrl;
                    }
                    if (metaTag.Name.Equals("ogimage", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaHinhAnh;
                    }
                    if (metaTag.Name.Equals("ogdes", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaDescription;
                    }
                    if (metaTag.Name.Equals("ogtype", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaType;
                    }
                    if (metaTag.Name.Equals("ogtwtitle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwtitle;
                    }
                    if (metaTag.Name.Equals("ogtwdes", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwdes;
                    }
                    if (metaTag.Name.Equals("ogtwimg", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwimg;
                    }
                    if (metaTag.Name.Equals("ogtwcard", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwcard;
                    }
                    if (metaTag.Name.Equals("igurl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaIgurl;
                    }
                    //if (metaTag.Name.Equals("urlc", StringComparison.CurrentCultureIgnoreCase))
                    //{
                    //    metaTag.Content = Linkurl;
                    //}

                }
            }
        }

        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        //HttpContext context = HttpContext.Current;
        //string[] Action = context.Items["Action"].ToString().Split('-');
        //idTinDang = Action[0];
        //string TieuDe = StaticData.getField("tb_TinDang", "TieuDe", "idTinDang", idTinDang);

        ////this.Title = TieuDe + " | Nhà Đất Đây Rồi";
        //if (!IsPostBack)
        //{
        //    LoadTinDang();
        //    LoadTinCungDanhMuc();
        //    //LoadDanhMuc();
        //}
    }
    private void LoadTinDang()
    {
        string URL = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
        string sqlTinDang = "select * from tb_TinDang td inner join tb_DanhMucCap1 dmc1 on td.idDanhMucCap1=dmc1.idDanhMucCap1";
        sqlTinDang += " where isnull(isHetHan,'False')='False' and DuongDan='" + DuongDan + "'";
        DataTable tbTinDang = Connect.GetTable(sqlTinDang);
        if (tbTinDang.Rows.Count > 0)
        {
            this.idTinDang = tbTinDang.Rows[0]["idTinDang"].ToString();
            //SET Số lượt xem
            int SoLuotXem = int.Parse(KiemTraKhongNhap_LoadLen(tbTinDang.Rows[0]["SoLuotXem"].ToString()));
            Connect.Exec("update tb_TinDang SET SoLuotXem='" + (++SoLuotXem) + "' where idTinDang='" + idTinDang + "'");

            SDT_ThanhVien = StaticData.getField("tb_ThanhVien", "TenDangNhap", "idThanhVien", tbTinDang.Rows[0]["idThanhVien"].ToString());
            //Load link title
            string sTrangChu = "";
            string sDanhMucCap1 = "";
            string sDanhMucCap2 = "";
            sTrangChu = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='" + Domain + "'><span>Tin đăng</span></a></li>";

            string titleDanhMucCap1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDang.Rows[0]["TenDanhMucCap1"].ToString().ToLower()));
            sDanhMucCap1 = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu' ><a href='" + Domain + "/tin-dang/1-" + tbTinDang.Rows[0]["idDanhMucCap1"] + "-" + titleDanhMucCap1 + "'><span> " + tbTinDang.Rows[0]["TenDanhMucCap1"] + " </span></a></li>";
            sDanhMucCap2 = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu' ><a href='" + Domain + "/tin-dang/1-" + tbTinDang.Rows[0]["idDanhMucCap1"] + "-" + titleDanhMucCap1 + "?C2=" + tbTinDang.Rows[0]["idDanhMucCap2"] + "'><span >" + StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", tbTinDang.Rows[0]["idDanhMucCap2"].ToString()) + "</span></a></li>";

            dvLinkTitle.InnerHtml = @"<ol class='breadcrumb _3WIL_EScB1tm02Oqj0vbu8'>" + sTrangChu + "" + sDanhMucCap1 + "" + sDanhMucCap2 + @"</ol>";
            //End load link title

            dvTitle.InnerHtml = tbTinDang.Rows[0]["TieuDe"].ToString();
            //Load slide
            string htmlSlide = "";
            string htmlDotSlide = "";
            string sqlHinhAnh = "select * from tb_HinhAnh where idTinDang='" + idTinDang + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            if (tbHinhAnh.Rows.Count > 0)
            {
               htmlSlide += "<ul>";
                for (int i = 0; i < tbHinhAnh.Rows.Count; i++)
                {

                    string urlHinhAnh = "";
                    urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[i]["UrlHinhAnh"].ToString();
                    metaHinhAnh = urlHinhAnh;
                    metaTwcard = urlHinhAnh;
                    metaTwimg = urlHinhAnh;
                    metaIgurl = urlHinhAnh;
                    if (i == 0)
                    {

                    }
                    if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                    System.Drawing.Bitmap img = new System.Drawing.Bitmap(Server.MapPath(urlHinhAnh));


                    // htmlSlide += "<div class='mySlides' style='max-height:450px;text-align:center;'>";
                    //  htmlSlide += "<div class='numbertext'></div>";

                    //if (img.Width >= img.Height)
                    //    htmlSlide += "  <img src='" + urlHinhAnh + "' style='width:100%;box-shadow: -5px -3px 15px 1px #6363637d;object-fit:contain;'>";
                    //else
                    //    htmlSlide += "  <img src='" + urlHinhAnh + "' style='width:50%;box-shadow: 0px 11px 131px 35px #6363637d;object-fit:contain;'>";

                    //htmlSlide += "</div>";
                    //htmlSlide += "<a class='prev' onclick='plusSlides(-1)'>&#10094;</a>";
                    //htmlSlide += "<a class='next' onclick='plusSlides(1)'>&#10095;</a>";


                    htmlSlide += @"
 <li>
<img src='" + urlHinhAnh + @"'  style='width:100%;height:300px;box-shadow: -5px -3px 15px 1px #6363637d;object-fit:contain;'
alt='Hinh" + tbHinhAnh.Rows[i]["idHinhAnh"].ToString() + @"' 
title='Hinh" + tbHinhAnh.Rows[i]["idHinhAnh"].ToString() + @"' 
 />

</li>                    
                    ";


                     htmlDotSlide += " <a   title='Hinh" + tbHinhAnh.Rows[i]["idHinhAnh"].ToString() + @"'><span>" + (i + 1).ToString() + "</span></a>";


                    //htmlDotSlide += "<span class='dot' onclick='currentSlide(" + (i + 1).ToString() + ")'></span>";
                }
                htmlSlide += "</ul>";
            }
            else
            {
                //string urlHinhAnh = Domain + "/images/icons/noimage.png";
                //htmlSlide += "<div class='mySlides fade'>";
                //htmlSlide += "<div class='numbertext'></div>";
                //htmlSlide += "  <img src='" + urlHinhAnh + "' style='width:100%;box-shadow: -5px -3px 15px 1px #6363637d;'>";
                ////htmlSlide += "  <div class='text'><%--Caption Text--%></div>";
                //htmlSlide += "</div>";
                //htmlSlide += "<a class='prev' onclick='plusSlides(-1)'>&#10094;</a>";
                //htmlSlide += "<a class='next' onclick='plusSlides(1)'>&#10095;</a>";

                string urlHinhAnh = Domain + "/images/icons/noimage.png";
                //metaHinhAnh = urlHinhAnh;
                htmlSlide += @"
 <li>
<img src='" + urlHinhAnh + @"' style='width:100%;height:auto;box-shadow: -5px -3px 15px 1px #6363637d;object-fit:contain;'
 />

</li>                    
                    ";
            }


            dvSlide.InnerHtml = htmlSlide;
            dvDotSlide.InnerHtml = htmlDotSlide;
            //End load slide
            string TenHuyen1 = StaticData.getField("District", "Ten", "Id", tbTinDang.Rows[0]["idHuyen"].ToString());
            string idTinh1 = tbTinDang.Rows[0]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            string idPhuongXa = tbTinDang.Rows[0]["idPhuongXa"].ToString();
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa);

            Title = tbTinDang.Rows[0]["TieuDe"].ToString();
            //ogtitle.Attributes["content"] = Title;
            metaTwdes = tbTinDang.Rows[0]["NoiDung"].ToString();
            metaTwtitle = tbTinDang.Rows[0]["TieuDe"].ToString();
            metaTitle = tbTinDang.Rows[0]["TieuDe"].ToString();
            metaUrl = HttpContext.Current.Request.Url.OriginalString;
            metaDescription = tbTinDang.Rows[0]["NoiDung"].ToString();
            metaType = tbTinDang.Rows[0]["TieuDe"].ToString();
            MetaDescription = tbTinDang.Rows[0]["NoiDung"].ToString();   
            Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script> var imgs = document.querySelectorAll('img');imgs.forEach(img => {img.alt = '" + metaTitle + "';});</script>");
            
			//Script
            string metaU = HttpContext.Current.Request.Url.OriginalString;
            string name = "name";
            string mainEntityOfPage = "mainEntityOfPage";
            string idd = "id";
            string context = "@context";
            string type = "@type";
            string iddd = "@id";
            string hl = "headline";
            string d = "description";
            string ig = "image";
            string ul = "url";
            string con = "@context";
            string t = "@type";
            string dp = "datePublished";
            string dm = "dateModified";
            string au = "author";
            string na = "name";
            string c = "@context";
            string tp = "@type";
            string pb = "publisher";
            string lg = "logo";
            string width = "width";
            string he = "height";
            string hea = "application/ld+json";
            string urlchu = "https://tungtang.com.vn";
            string paa = "WebPage";
            string imgo = "ImageObject";
            string per = "Person";
            string og = "Organization";
            string news = "NewsArticle";
            string chema = "https://schema.org";
            string imoc = "https://tungtang.com.vn/images/icons/logo.png";
            string Crea = "CreativeWorkSeries";
            Page.Header.Controls.AddAt(1,
                       new LiteralControl(
                           "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                       )
                       );
            //End script			

            string Chuoi_DiaDiem = "";
            if (TenTinh1 == "")
                Chuoi_DiaDiem = "Toàn quốc";
            else
            {
                if (TenHuyen1 == "")
                    Chuoi_DiaDiem = TenTinh1;
                else
                {
                    if (TenPhuongXa == "")
                        Chuoi_DiaDiem = TenHuyen1 + " - " + TenTinh1;
                    else
                        Chuoi_DiaDiem = TenPhuongXa + " - " + TenHuyen1 + " - " + TenTinh1;
                }
            }
            dvLoaiThue.InnerHtml = "<i class='fa fa-map-marker' style='padding:0px 14px 0px 6px;font-size: 20px;'></i> " + Chuoi_DiaDiem;

            string sGia = "";
            if (tbTinDang.Rows[0]["TuGia"].ToString() != "")
                sGia = "<span style='color:red;font-size: 16px;font-weight:700;'>" + double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + "đ" + "</span>";

            if (tbTinDang.Rows[0]["isHetHan"].ToString() == "True")
                sGia = "<span style='color:#888888;font-size: 16px;font-weight:700;'>" + double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + "đ" + "</span>" + "<span style='color:red;font-size: 20px;'> - ĐÃ HẾT HẠN</span>";

            dvGia.InnerHtml = sGia;
            dvMaTinDang.InnerHtml = "<i class='fa fa-indent' style='padding:0px 14px 0px 2px;font-size: 20px;'></i> " + tbTinDang.Rows[0]["MaTinDang"].ToString().Trim();

            //dvNgayDang.InnerHtml = "<i class='fa fa-calendar' style='padding: 0px 10px 0px 2px;font-size: 20px;'></i> Đăng từ " + DateTime.Parse(tbTinDang.Rows[0]["NgayDang"].ToString()).ToString("dd-MM-yyyy");

            string sqlThanhVien = "select * from tb_ThanhVien where idThanhVien='" + tbTinDang.Rows[0]["idThanhVien"].ToString() + "'";
            DataTable tbThanhVien = Connect.GetTable(sqlThanhVien);
            if (tbThanhVien.Rows.Count > 0)
            {
                dvAnhDaiDien.InnerHtml = "<img class='img-circle KJuQ6UgNHKTybxph5jqh- _38Q-ZJPwiUyqL82cU4XpN6 ' src='" + Domain + "../images/user/" + tbThanhVien.Rows[0]["LinkAnh"].ToString().Trim() + "?width=160'/>";
                if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/"+ tbThanhVien.Rows[0]["LinkAnh"].ToString().Trim()))
                    dvAnhDaiDien.InnerHtml = "<img class='img-circle KJuQ6UgNHKTybxph5jqh- _38Q-ZJPwiUyqL82cU4XpN6 ' src='" + Domain + "/images/icons/noUser.png" + "?width=160'/>";

                dvHoTen.InnerHtml = "<b>" + tbThanhVien.Rows[0]["TenCuaHang"] + "</b>";
                dvSoDienThoai.InnerHtml = @"<a href='tel:" + tbThanhVien.Rows[0]["TenDangNhap"] + @"'>
                                                <div class='sc-gisBJw ikcTlv'>
                                                    <div class='sc-kjoXOD icTLCU'>
                                                        <div class='sc-cHGsZl iTxwzU'>
                                                            <span>
                                                                <img class='sc-TOsTZ bnAkpx' src='../images/icons/phone.png' /></span>
                                                            <span>" + tbThanhVien.Rows[0]["TenDangNhap"] + @"</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>";
                dvEmail.InnerHtml = tbThanhVien.Rows[0]["Email"].ToString();
                dvDiaChi.InnerHtml = StaticData.getField("City", "Ten", "id", tbThanhVien.Rows[0]["idTinh"].ToString());
                btnXemTrang.Attributes.Add("href", "/thong-tin-ca-nhan/ttcn-" + SDT_ThanhVien);
            }
            dvNoiDung.InnerHtml = tbTinDang.Rows[0]["NoiDung"].ToString();
            dvChiaSeBaiViet.InnerHtml = @"

                                          <a style='padding: 0 5px' target='_blank' href='http://www.facebook.com/share.php?u=" + URL + @"' title='Chia sẻ lên Facebook'><img src='https://cdn4.iconfinder.com/data/icons/social-media-icons-the-circle-set/48/facebook_circle-512.png' style='border:0; height:40px;' /></a>
                                          <a style='padding: 0 5px' target='_blank' href='https://www.facebook.com/v2.9/dialog/send?app_id=123456789&channel_url=" + URL + @"&app_id=123456789' title='Chia sẻ qua Messenger'><img src='https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Facebook_Messenger-512.png' style='border:0; height:40px;' /></a>
                                          <a class='zalo-share-button'  style='padding: 0 5px;cursor:pointer;'  data-href='" + URL + @"' data-oaid='579745863508352884' data-layout='2' data-color='blue' data-customize='true'><img src='https://seeklogo.com/images/Z/zalo-logo-B0A0B2B326-seeklogo.com.png' style='border:0; height:40px; width: auto;' /></a><script src='https://sp.zalo.me/plugins/sdk.js'></script> 
                                          
                                          <a style='padding: 0 5px;cursor:pointer;' onclick='CopyLinkURL()' title='Sao chép liên kết'><img src='https://cdn4.iconfinder.com/data/icons/web-links/512/41-512.png' style='border:0; height:40px;' /></a>
                                          <span id='spSaoChepThanhCong' style='display: none;width: max-content;padding: 4px 15px;background-color: #c7c7c7;color:white;'>Đã sao chép</span>
                                           ";
            myLinkCopy.Value = URL;
        }
		 string sqltableAnh = "select * from tb_BannerTinDang where idBannerTinDang=3";
        DataTable tbHinhAnhTinDang = Connect.GetTable(sqltableAnh);
        string srcHinhAnh = StaticData.getField("tb_BannerTinDang", "'../' + LinkAnh", "idBannerTinDang", "3");
        
        if (tbHinhAnhTinDang.Rows.Count > 0)
        {
            for (int i = 0; i < tbHinhAnhTinDang.Rows.Count; i++)
            {
                string LinkAnh = tbHinhAnhTinDang.Rows[i]["Url"].ToString();
                AnhTinDang.InnerHtml = @"
                               <a href='" + LinkAnh + "' target='_blank'>  <img style='width: 100%;' alt='Tung Tăng' src = '" + srcHinhAnh + "'/> </a>";
            }
        }
    }
    private void LoadTinCungDanhMuc()
    {
        string idDanhMucCap1 = StaticData.getField("tb_TinDang", "idDanhMucCap1", "idTinDang", idTinDang);
        string sqlTinDangCungDanhMuc = "select top 16 * from tb_TinDang where isnull(isHetHan,'False')='False' and isnull(isDuyet,'False')='True' and idTinDang!='" + idTinDang + "' and idDanhMucCap1='" + idDanhMucCap1 + "' order by idTinDang desc";
        DataTable tbTinDangCungDanhMuc = Connect.GetTable(sqlTinDangCungDanhMuc);
        string html = "";
        for (int i = 0; i < tbTinDangCungDanhMuc.Rows.Count; i++)
        {
            string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDangCungDanhMuc.Rows[i]["TieuDe"].ToString().Trim()));
            string url = Domain + "/tdct/" + tbTinDangCungDanhMuc.Rows[i]["DuongDan"].ToString();
            //
            string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + tbTinDangCungDanhMuc.Rows[i]["idTinDang"] + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            string urlHinhAnh = "";
            if (tbHinhAnh.Rows.Count > 0)
            {
                urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"].ToString();
                if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                    urlHinhAnh = Domain + "/images/icons/noimage.png";
            }
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            //
            decimal Gia = decimal.Parse(KiemTraKhongNhap_LoadLen(tbTinDangCungDanhMuc.Rows[i]["TuGia"].ToString()));

            html += @"  <div class='item'>
                            <a href='" + url + @"' title='" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"'>
                                <span><img alt='"+tbTinDangCungDanhMuc.Rows[i]["TieuDe"]+@"' src='" + urlHinhAnh + @"' /></span>
                                <span class='title-TinDangLienQuan'>" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"</span>
                                <span class='cost-TinDangLienQuan' style='color: red'>" + Gia.ToString("N0") + @"</span>
                            </a>
                        </div> ";
        }
        abc.InnerHtml = html;
    }
    static string SDT_ThanhVien = "";
    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    public bool URLExists(string url)
    {
        System.Net.WebRequest webRequest = System.Net.WebRequest.Create(url.Replace("/", "\\").Replace("\\\\", "\\"));
        webRequest.Method = "HEAD";
        try
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)webRequest.GetResponse())
            {
                if (response.StatusCode.ToString() == "OK")
                {
                    return true;
                }
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    protected void aThongTinLienHe_Click(object sender, EventArgs e)
    {
        Response.Redirect("/thong-tin-ca-nhan/ttcn-" + SDT_ThanhVien);
    }
}