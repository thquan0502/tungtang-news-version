﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_adAjax : System.Web.UI.Page
{
    string sTenDangNhap = "";
    string mQuyen = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["QuanLyHoaDongDifoco_Login"] != null && Session["QuanLyHoaDongDifoco_Login"].ToString() != "")
        //{
        //    sTenDangNhap = Session["QuanLyHoaDongDifoco_Login"].ToString();
        //    mQuyen = StaticData.getField("tb_Admin", "MaNhomAdmin", "TenDangNhap", sTenDangNhap);
        //}
        //else
        //{
        //    //Response.Redirect("Home/DangNhap.aspx"); 
        //}
        string action = Request.QueryString["Action"].Trim();
        switch (action)
        {
            case "DangXuat":
                DangXuat(); break;
            case "DuyetTinDang":
                DuyetTinDang(); break;
            case "KhoaThanhVien":
                KhoaThanhVien(); break;
            case "DeleteDanhMucCap1":
                DeleteDanhMucCap1(); break;
            case "DeleteDanhMucCap2":
                DeleteDanhMucCap2(); break;
            case "CapNhatHetHan":
                CapNhatHetHan(); break;
            case "MoXemChiTietTinDang":
                MoXemChiTietTinDang(); break;
            case "DeleteTinTuc":
                DeleteTinTuc(); break;
            case "XoaAdmin":
                XoaAdmin(); break;
            case "LenTop":
                LenTop(); break;
            case "LuuQuyen":
                LuuQuyen(); break;
        }
    }
    private void LuuQuyen()
    {
        string idNhomQuyen = Request.QueryString["idNhomQuyen"].Trim();
        string check = Request.QueryString["check"].Trim();
        
        if (Request.Cookies["cookie_PhanQuyen"] != null)
        {

            string a = HttpContext.Current.Request.Cookies["cookie_PhanQuyen"].Value;

            if (check == "LUU")
                a += idNhomQuyen + ",";
            else
            {
                a = a.Replace(idNhomQuyen+",","");
            }

                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", a);
            cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie_PhanQuyen);
        }
    }
    private void DangXuat()
    {
        HttpCookie cookie_AdminTungTang_Login = new HttpCookie("AdminTungTang_Login", "");
        // Gán thời gian sống của Cookie là thời gian hiện tại 
        cookie_AdminTungTang_Login.Expires = DateTime.Now;
        // Thêm Cookie 
        Response.Cookies.Add(cookie_AdminTungTang_Login);
        Response.Write("True");
    }
    private void DuyetTinDang()
    {
        string idNguoiDuyet = "";
        if (Request.Cookies["AdminTungTang_Login"] != null && Request.Cookies["AdminTungTang_Login"].Value.Trim() != "")
        {
            string mTenDangNhap = Request.Cookies["AdminTungTang_Login"].Value.Trim();
            idNguoiDuyet = StaticData.getField("tb_Admin", "idAdmin", "tenDangNhap", mTenDangNhap);
        }

        string idTinDang = Request.QueryString["idTinDang"].Trim();
        string Duyet = Request.QueryString["Duyet"].Trim();
        string LyDoKhongDuyet = Request.QueryString["LyDo"].Trim();

        string sqlUpdateDuyet = "update tb_TinDang";
        if (Duyet.ToUpper() == "DUYET")
            sqlUpdateDuyet += " set isDuyet='true',LyDo_KhongDuyet=NULL,";
        if (Duyet.ToUpper() == "KHONGDUYET")
            sqlUpdateDuyet += " set isDuyet='false',LyDo_KhongDuyet= N'" + LyDoKhongDuyet + "',";
        if (Duyet.ToUpper() == "")
            sqlUpdateDuyet += " set isDuyet=null,LyDo_KhongDuyet=NULL,";
        sqlUpdateDuyet += "idAdmin_Duyet='" + idNguoiDuyet + "',";
        if (Duyet.ToUpper() == "" || Duyet.ToUpper() == "KHONGDUYET")
            sqlUpdateDuyet += "NgayDayLenTop='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "'";
        else
            sqlUpdateDuyet += "NgayDayLenTop='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "'";
        sqlUpdateDuyet += " where idTinDang='" + idTinDang + "'";
        bool ktUpdateDuyet = Connect.Exec(sqlUpdateDuyet);
        if (ktUpdateDuyet)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void KhoaThanhVien()
    {
        string idThanhVien = Request.QueryString["idThanhVien"].Trim();
        string Khoa = Request.QueryString["Khoa"].Trim();
        string sqlUpdateDuyet = "update tb_ThanhVien";
        sqlUpdateDuyet += " set isKhoa='" + Khoa + "'";
        sqlUpdateDuyet += " where idThanhVien='" + idThanhVien + "'";
        bool ktUpdateDuyet = Connect.Exec(sqlUpdateDuyet);
        if (ktUpdateDuyet)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void DeleteDanhMucCap1()
    {
        string idDanhMucCap1 = StaticData.ValidParameter(Request.QueryString["id"].Trim());
        string LinkIcon = StaticData.getField("tb_DanhMucCap1", "LinkIcon", "idDanhMucCap1", idDanhMucCap1);
        string LinkHinhAnh = StaticData.getField("tb_DanhMucCap1", "LinkAnh", "idDanhMucCap1", idDanhMucCap1);

        {
            string sql = "delete from tb_DanhMucCap1 where idDanhMucCap1='" + idDanhMucCap1 + "'";
            bool ktDelete = Connect.Exec(sql);
            if (ktDelete)
            {
                try
                {
                    File.Delete(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + LinkIcon);
                }
                catch { }
                try
                {
                    File.Delete(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + LinkHinhAnh);
                }
                catch { }

                Response.Write("True");
            }
            else
                Response.Write("False");
        }

    }
    private void DeleteDanhMucCap2()
    {
        string idDanhMucCap2 = StaticData.ValidParameter(Request.QueryString["id"].Trim());
        string LinkIcon = StaticData.getField("tb_DanhMucCap2", "LinkIcon", "idDanhMucCap2", idDanhMucCap2);
        {
            string sql = "delete from tb_DanhMucCap2 where idDanhMucCap2='" + idDanhMucCap2 + "'";
            bool ktDelete = Connect.Exec(sql);
            if (ktDelete)
            {
                try
                {
                    File.Delete(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + LinkIcon);
                }
                catch { }

                Response.Write("True");
            }
            else
                Response.Write("False");
        }

    }
    private void CapNhatHetHan()
    {
        string idTinDang = Request.QueryString["idTinDang"].Trim();
        string HetHan = Request.QueryString["HetHan"].Trim();
        string sqlUpdateTD = "update tb_TinDang";
        if (HetHan.ToUpper().Trim() == "HETHAN")
            sqlUpdateTD += " set isHetHan='True'";
        else
            sqlUpdateTD += " set isHetHan='False'";
        sqlUpdateTD += " where idTinDang='" + idTinDang + "'";
        bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
        if (ktUpdateTD)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void MoXemChiTietTinDang()
    {
        string idTinDang = StaticData.ValidParameter(Request.QueryString["idTinDang"].Trim());

        string sql = "select * from tb_TinDang where idTinDang ='" + idTinDang + "'";
        DataTable table = Connect.GetTable(sql);
        string html = "<div class='titlePouple'>" + table.Rows[0]["TieuDe"].ToString() + "</div>";

        html += "<table class='table table-bordered table-striped' style='font-size:15px'>";


        html += "<tr><td><b>Loại thuê:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + StaticData.getField("tb_LoaiThue", "TenLoaiThue", "MaLoaiThue", table.Rows[0]["MaLoaiThue"].ToString()) + "</td>";
        html += "<td><b>Giá:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["Gia"].ToString() + "</td></tr>";

        html += "<tr><td><b>Người đăng:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + StaticData.getField("tb_ThanhVien", "HoTen", "idThanhVien", table.Rows[0]["idThanhVien"].ToString()) + "</td>";
        html += "<td><b>Ngày đăng:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + StaticData.ConvertMMDDYYtoDDMMYY(table.Rows[0]["NgayDang"].ToString()) + "</td></tr>";

        html += "<tr><td colspan='4'><i><b>Thông tin liên hệ</b><i></td></tr>";

        html += "<tr><td><b>Họ tên:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["HoTen"].ToString() + "</td>";
        html += "<td><b>Số điện thoại:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["SoDienTHoai"].ToString() + "</td></tr>";

        html += "<tr><td><b>Email:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["Email"].ToString() + "</td>";
        html += "<td><b>Địa chỉ:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["DiaChi"].ToString() + "</td></tr>";

        html += "<tr><td style='width:150px'><b>Nội dung:</b></td>";
        html += "<td colspan='3' style='text-align:justify; color:#234f84'>" + table.Rows[0]["NoiDung"].ToString() + "</td></tr>";

        html += "</table>";
        Response.Write(html);
    }
    private void DeleteTinTuc()
    {
        string idTinTuc = StaticData.ValidParameter(Request.QueryString["idTinTuc"].Trim());
        string sql = "delete from tb_TinTuc where idTinTuc='" + idTinTuc + "'";
        bool ktDelete = Connect.Exec(sql);
        if (ktDelete)
            Response.Write("True");
        else
            Response.Write("False");
    }
    void XoaAdmin()
    {
        string admin = StaticData.ValidParameter(Request.QueryString["admin"].Trim());

        string sql1 = "delete from [tb_ChiTietQuyenAdmin] where idadmin='" + admin + "' ";
        bool rs = Connect.Exec(sql1);
        string sql = "delete from tb_ADmin where idadmin='" + admin + "'";

        if (Connect.Exec(sql))
            Response.Write("True");
        else
            Response.Write("False");
    }
    void LenTop()
    {
        string TD = StaticData.ValidParameter(Request.QueryString["TD"].Trim());
        if (Connect.Exec("Update tb_TinDang set NgayDayLenTop ='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where idTinDang='" + TD + "' "))
            Response.Write("True");
    }
}