﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

public partial class TinDang : System.Web.UI.Page
{
    string Domain = "";

    string idDanhMucCap1 = "";
    string idDanhMucCap2 = "";
	string MaDM = "";
    string MaDM2 = "";
    string ChuoiLoai = "";
    string idTinh = "";
    string idHuyen = "";
    string idPhuongXa = "";
    string TenCuaHang = "";
    string BoLocThem = "";
	string sGia = "";
    string sRange = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Pages = 1;
    int MaxPage = 0;
    int PageSize = 50;
    protected void Page_Load(object sender, EventArgs e)
    {

        HtmlLink canonical = new HtmlLink();
        canonical.Href = HttpContext.Current.Request.Url.OriginalString;
        canonical.Attributes["rel"] = "canonical";

        Page.Header.Controls.Add(canonical); HtmlLink alternate = new HtmlLink();
        alternate.Href = HttpContext.Current.Request.Url.OriginalString;
        alternate.Attributes["rel"] = "alternate";
        Page.Header.Controls.Add(alternate);
        if (!IsPostBack)
        {
            try
            {
                Pages = int.Parse(Request.QueryString["P"].ToString());
                if (Pages <= 0)
                {
                    Response.Redirect(Domain + "/tin-dang/td");
                }
            }
            catch
            {
                Pages = 1;
            }
            string sUrl = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "");
            string[] arrUrl = sUrl.Split('/', '?', '&');
           try
            {
                if (arrUrl[2] != null && arrUrl[2].Trim() != "")
                {
                    //string[] arrTinDang = arrUrl[2].Trim().Split('-');
                    //if (arrUrl[1] == "TIN-DANG")
                    //    MaDM = arrUrl2[0];
                    if (arrUrl[2] == "TD")
                    {

                    }
                    else
                    {
                        idDanhMucCap1 = StaticData.getField("tb_DanhMucCap1", "idDanhMucCap1", "LinkTenDanhMucCap1", arrUrl[2]);
                        ddlLoaiDanhMuc.Value = idDanhMucCap1;
                        txtLoaiDanhMuc.InnerHtml = @"<span itemprop='name'>" + StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1) + @"</span><span class='caret _2UAALXHJVap4y2kAfwkH6_'></span>";
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[3] != null && arrUrl[3].Trim() != "")
                {
                    if (arrUrl[3] != "")
                    {
                        idTinh = StaticData.getField("City", "id", "Link", arrUrl[3]);
                        txtTinhThanh.Value = idTinh;
                        if (idTinh == "")
                        {
                            idDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[3]);
                            MaDM = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2);
                        }
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[4] != null && arrUrl[4].Trim() != "")
                {
                    if (arrUrl[4] != "")
                    {
                        idHuyen = StaticData.getField("District", "id", "Link", arrUrl[4]);
                        txtQuanHuyen.Value = idHuyen;
                        if (idHuyen == "")
                        {
                            idDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[4]);
                        }
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[5] != null && arrUrl[5].Trim() != "")
                {
                    if (arrUrl[5] != "")
                    {
                        idPhuongXa = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[5]);
                        txtPhuongXa.Value = idPhuongXa;
                        if (idPhuongXa == "")
                        {
                            idDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[5]);
                        }
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[6] != null && arrUrl[6].Trim() != "")
                {
                    if (arrUrl[6] != "")
                    {
                        idDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
                        MaDM = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2);
                        //if (idDanhMucCap2 == "")
                        //{
                        //    idTinh = StaticData.getField("City", "id", "Link", arrUrl[3]);
                        //}
                    }
                }
            }
            catch { }
            try
            {
                idTinh = Request.QueryString["TT"].ToString().Trim();
                txtTinhThanh.Value = idTinh;
            }
            catch { }
            try
            {
                idHuyen = Request.QueryString["QH"].ToString().Trim();
                txtQuanHuyen.Value = idHuyen;
            }
            catch { }
            try
            {
                idPhuongXa = Request.QueryString["PX"].ToString().Trim();
                txtPhuongXa.Value = idPhuongXa;
            }
            catch { }
            try
            {
                TenCuaHang = Request.QueryString["CH"].ToString().Trim();
                txtKeySearch.Value = TenCuaHang;
            }
            catch { }
            try
            {
                BoLocThem = Request.QueryString["BLT"].ToString().Trim();
                txtBoLocThem.Value = BoLocThem;
            }
            catch { }
			try
            {
                sGia = Request.QueryString["Price"].ToString().Trim();
                listLabel.Text = sGia;
            }
            catch { }
            try
            {
                sRange = Request.QueryString["Range"].ToString().Trim();
                ruler.Text = sRange;
            }
            catch { }
           //try
            //{
            //    idDanhMucCap2 = Request.QueryString["C2"].ToString().Trim();
            //    ddlLoaiDanhMucCap2.Value = idDanhMucCap2;
            //}
            //catch { }
            string TenTinhThanh = StaticData.getField("City", "ten", "id", idTinh);
            string TenQuanHuyen = StaticData.getField("District", "ten", "id", idHuyen);
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "ten", "id", idPhuongXa);
            string chuoi_DiaDiem = "";
            if (TenTinhThanh != "")
                chuoi_DiaDiem = TenTinhThanh;
            if (TenQuanHuyen != "")
                chuoi_DiaDiem = TenQuanHuyen;
            if (TenPhuongXa != "")
                chuoi_DiaDiem = TenPhuongXa;

            if (TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                chuoi_DiaDiem = "Toàn quốc";
            txtDiaDiem.InnerHtml = @"<span itemprop='name'>" + chuoi_DiaDiem + "</span><span class='caret _2UAALXHJVap4y2kAfwkH6_'></span>";


            //Load link title
            string sTrangChu = "";
            string sTenDanhMucCap1 = "";
            string sTenDanhMucCap2 = "";
			string sLinkC1 = "";
            string LinkDM1 = "";
            string TenDM2 = "";
            string LinkDm2 = "";

            sTrangChu = "<a href='" + Domain + "'>Trang chủ</a>";
            sTenDanhMucCap1 = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1);
            sTenDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2);
			

			 sLinkC1 = StaticData.getField("tb_DanhMucCap1", "idDanhMucCap1", "LinkTenDanhMucCap1", arrUrl[2]);
            if (sLinkC1 != "")
            {
                LinkDM1 = StaticData.getField("tb_DanhMucCap1", "LinkTenDanhMucCap1", "idDanhMucCap1", sLinkC1);
            }
            try
            {
                if (sTenDanhMucCap2 == "")
                {
                    TenDM2 = StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[4]);
                    if (TenDM2 != "")
                    {
                        LinkDm2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "TenDanhMucCap2", TenDM2);
                    }
                }
            }
            catch { }


            string[] Url = HttpContext.Current.Request.Url.AbsoluteUri.Replace("://", "").Replace(":\\", "").Split(new[] { "/tin-dang/" }, StringSplitOptions.None);
            ChuoiLoai = LocChuoiLoai(Url[1]);
            string chuoi_Breadscrum = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/'><span>Trang chủ</span></a></li>";
            chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/td'><span>Tin đăng</span></a></li>";
            //if (TenTinhThanh != "")
            //    chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/td?TT=" + idTinh + "&'><span>" + TenTinhThanh + "</span></a></li>";
            //if (TenQuanHuyen != "")
            //    chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/td?TT=" + idTinh + "&QH=" + idHuyen + "'><span>" + TenQuanHuyen + "</span></a></li>";
            //if (TenPhuongXa != "")
            //    chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/td?TT=" + idTinh + "&QH=" + idHuyen + "&PX=" + idPhuongXa + "'><span>" + TenPhuongXa + "</span></a></li>";
            if (sTenDanhMucCap1 != "")
                chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/" + LinkDM1 + "'><span>" + sTenDanhMucCap1 + "</span></a></li>";
            if (sTenDanhMucCap2 != "")
                chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/" + LinkDM1 + "?" + MaDM + "''><span>" + sTenDanhMucCap2 + "</span></a></li>";
            if (sTenDanhMucCap2 == "")
                chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/tin-dang/" + LinkDM1 + "?" + LinkDm2 + "''><span>" + TenDM2 + "</span></a></li>";
            dvLinkTitle2.InnerHtml = chuoi_Breadscrum;
            //End load link title 

            LoadTinDang();
			 LoadLienQuan();
            LoadMoTa();
        }
    }
	#region paging
	 public void LoadLienQuan()
    {
        string sql = "";
        sql += @"select top 10 * from tb_TinDang TD where TD.isHot = 'true' AND (TD.isHetHan = '0' or TD.isHetHan is null ) AND TD.isDuyet = '1' order by TD.NgayGuiDuyet desc ";


        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        DataTable table = Connect.GetTable(sql);
        string html = "";
        if (table.Rows.Count > 0)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string TieuDe = table.Rows[i]["TieuDe"].ToString();
                string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + table.Rows[i]["idTinDang"] + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string urlHinhAnh = "";
                if (tbHinhAnh.Rows.Count > 0)
                {
                    urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"].ToString();
                    if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                }
                else
                    urlHinhAnh = Domain + "/images/icons/noimage.png";
                if (TieuDe.Length > 1000)
                {
                    TieuDe = TieuDe.Substring(0, 1000) + "...";
                }
                string TenHuyen1 = StaticData.getField("District", "Ten", "Id", table.Rows[i]["idHuyen"].ToString());
                string idTinh1 = table.Rows[i]["idTinh"].ToString();
                string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
                string idPhuongXa1 = table.Rows[i]["idPhuongXa"].ToString();
                string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
                string chuoi_DiaDiem = "";
                decimal Gia = decimal.Parse(KiemTraKhongNhap_LoadLen(table.Rows[i]["TuGia"].ToString()));
                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim()));
                string urlTinDangChiTiet = Domain + "/tdct/" + table.Rows[i]["DuongDan"].ToString();
                string LoaiTinDang = "Cần bán";
                if (table.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                    LoaiTinDang = "Cần mua";
                chuoi_DiaDiem = TenTinh1;
                if (TenTinh1 == "")
                    chuoi_DiaDiem = "Toàn quốc";
                if (i == 0)
                {

                }

                html += @"  <div class='item' style='margin-bottom: 14px;width: 100%; height: 205px;'>
                            <a href='" + urlTinDangChiTiet + @"' >
                                <span><img alt='" + table.Rows[i]["TieuDe"].ToString() + @" - Tung Tăng' src='" + urlHinhAnh + @"' /></span>
                                  <div class='list'><div style='transform: rotate(34deg);'>PRO</div></div>
                                <strong><span class='title-TinDangLienQuan'>" + TieuDe + @"</span></strong>
                                  <span class='title-TinDangLienQuan' style='color: red;font-weight: 600;'>" + Gia.ToString("N0") + 'đ' + @"</span>
                                <span class='title-TinDangLienQuan' style='color: #666'>" + LoaiTinDang + ' ' + '|' + ' ' + TinhThoiGianLucDang(DateTime.Parse(table.Rows[i]["NgayDayLenTop"].ToString())) + ' ' + '|' + ' ' + chuoi_DiaDiem + ' ' + '|' + ' '+" <i class='fa fa-eye'></i> " + table.Rows[i]["SoLuotXem"] + @"</span>
                            </a>
                        </div> ";
            }            
        }        
        abc.InnerHtml = html;        
    }
    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Pages == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Pages == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Pages <= MaxPage)
                {
                    for (int i = Pages; i <= MaxPage; i++)
                    {
                        if (i == Pages)
                        {
                            txtPage1 = (Pages - 2).ToString();
                            txtPage2 = (Pages - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Pages)
                                txtPage3 = i.ToString();
                            if (i == (Pages + 1))
                                txtPage4 = i.ToString();
                            if (i == (Pages + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string SapXepTheo = " TD.NgayDayLenTop desc ";
        string GiaTu = "", GiaDen = "";
        string CanXem = "";
	    string GiaLoc = "";
        string[] arrBoLocThem = BoLocThem.Split('|');
        if (arrBoLocThem.Length == 5)
        {
           if(arrBoLocThem[0] == "undefined" && arrBoLocThem[1] == "undefined")
            {
                GiaTu = "0";
                GiaDen = "0";
            }
            else
            {
                GiaTu = arrBoLocThem[0];
                GiaDen = arrBoLocThem[1];
            }            
            GiaLoc = arrBoLocThem[4];
           
            //            if (idDanhMucCap2 == "")
            {
                decimal GiaMax = 0;
                if (idDanhMucCap1 != "" && idDanhMucCap2 == "")
                    GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap1", "MucGia_BoLoc", "idDanhMucCap1", idDanhMucCap1));
                else if (idDanhMucCap1 != "" && idDanhMucCap2 != "")
                    GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap2", "MucGia_BoLoc", "idDanhMucCap2", idDanhMucCap2));

                if (GiaMax >= decimal.Parse(GiaDen))
                    spGiaDen.InnerHtml = decimal.Parse(KiemTraKhongCo_LoadLen(GiaDen)).ToString("N0");
                else if (GiaMax == 0)
                    spGiaDen.InnerHtml = (30000000).ToString("N0");
                else
                    spGiaDen.InnerHtml = (GiaMax).ToString("N0") + "+";
            }
            rgGiaDen.Value = GiaDen;
            spGiaDen.InnerHtml = decimal.Parse(KiemTraKhongCo_LoadLen(GiaDen)).ToString("N0");
            spGiaTu.InnerHtml = decimal.Parse(KiemTraKhongCo_LoadLen(GiaTu)).ToString("N0");
            rgGiaTu.Value = GiaTu;

            if (arrBoLocThem[2].Trim() == "PRICE")
            {
                SapXepTheo = " TD.TuGia asc ";
                radGiaThapTruoc.Checked = true;
            }
            else if (arrBoLocThem[2].Trim() == "DESC")
				SapXepTheo = " TD.NgayDang desc ";
                radTinMoiTruoc.Checked = true;

             if (arrBoLocThem[3].Trim() == "SE")
            {
                CanXem = "CanBan";
                ckbCanBan.Checked = true;
            }
            else if (arrBoLocThem[3].Trim() == "BU")
            {
                CanXem = "CanMua";
                ckbCanMua.Checked = true;
            }
            else if (arrBoLocThem[3].Trim() == "SEaBU")
            {
                ckbCanBan.Checked = true;
                ckbCanMua.Checked = true;
            }
        }

        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  ( order by " + SapXepTheo + @"  )AS RowNumber
	              ,TD.*
                  from tb_TinDang TD 
            where  isnull(TD.isDuyet,'False')='True' and  (isHetHan = '0' or isHetHan is null ) ";
        if (idDanhMucCap1.Trim() != "")
        {
            sql += " and TD.idDanhMucCap1='" + idDanhMucCap1.Trim() + "'";
            //Nếu có danh mục cấp 1 thì load giá để tìm kiếm 
            if (idDanhMucCap2 == "")
                LoadMinMax_rangeBoLoc(idDanhMucCap1, "Cap1", GiaTu, GiaDen);

            //Load Danh Muc Cap 2
            DataTable tableCap2 = Connect.GetTable("select * from tb_DanhMucCap2 where idDanhMucCap1=" + idDanhMucCap1.Trim());
            if (tableCap2.Rows.Count > 0)
            {
                string html_danhMucCap2 = @"";
                for (int i = 0; i < tableCap2.Rows.Count; i++)
                {
                    html_danhMucCap2 += @"<div>
                                            <a href='javascript:void(0)' style='color: inherit;'  onclick='ChonDanhMucCap2(" + tableCap2.Rows[i]["idDanhMucCap2"] + @")'>
                                                <li class='sc-gzVnrw dUodeu' width='80'>
                                                    <div class='sc-htoDjs jDGBYk' style='width: 45px; height:45px;'>
                                                        <img class='sc-gqjmRU cVznjt' src='/" + tableCap2.Rows[i]["linkicon"] + @"' role='presentation'></div>
                                                    <div itemprop='name' class='sc-dnqmqq kCQBIM'><span class='sc-iwsKbI jiPRqw'>" + tableCap2.Rows[i]["TenDanhMucCap2"] + @"</span><span class='sc-gZMcBi jNmBYP'></span></div>
                                                </li>
                                            </a>
                                        </div> ";
                }
                dvDanhMucCap2.InnerHtml = html_danhMucCap2;
                dvDanhMucCap2.Attributes.Add("style", "border-top: none; margin: 0px 0px 1px; padding-bottom: 5px;display:-webkit-box;");
            }
        }
        if (idDanhMucCap2 != "")
        {
            sql += " and TD.idDanhMucCap2='" + idDanhMucCap2.Trim() + "'";
            LoadMinMax_rangeBoLoc(idDanhMucCap2, "Cap2", GiaTu, GiaDen);
        }
        if (CanXem != "")
            sql += " and TD.LoaiTinDang ='" + CanXem + "' ";
		if(GiaTu =="0" && GiaDen =="0" && GiaLoc == "0")
        {
            sql += " and (TD.TuGia >= '" + GiaTu + "')";
        }
         else if (GiaTu != "" && GiaDen != "" && GiaLoc != "10100" && GiaLoc != "100500" && GiaLoc != "500" && GiaLoc != "10100500" && GiaLoc != "100500500" && GiaLoc != "10100100500" && GiaLoc != "10100100500500")
        {
            decimal GiaMax = decimal.Parse( GiaDen);
            if (idDanhMucCap1 != "" && idDanhMucCap2 == "")
                GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap1", "MucGia_BoLoc", "idDanhMucCap1", idDanhMucCap1));
            else if (idDanhMucCap1 != "" && idDanhMucCap2 != "")
                GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap2", "MucGia_BoLoc", "idDanhMucCap2", idDanhMucCap2));
             if (decimal.Parse(GiaTu) <= GiaMax / 2)
            {
                if (GiaMax <= decimal.Parse(GiaDen))
                    sql += " and TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' ";
                else
                    sql += " and TD.TuGia >= '" + GiaTu + "'";
            }
            else
            {
                if (GiaTu == GiaDen)
                {
                    sql += " and TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' ";
                }
                else
                {
                    sql += " and TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' ";
                }
            }
        }
			 if (GiaLoc == "10100" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia between 10000000 and 100000000";
        }
        if (GiaLoc == "100500" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia between 100000000 and 500000000";
        }
        if (GiaLoc == "500" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia >= 500000000";
        }
        if (GiaLoc == "10100100500" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia between 10000000 and 100000000 or TD.TuGia between 100000000 and 500000000";
        }
        if (GiaLoc == "10100500" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia between 10000000 and 100000000 or TD.TuGia >= 500000000";
        }
        if (GiaLoc == "100500500" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia between 100000000 and 500000000 or TD.TuGia >= 500000000";
        }
        if (GiaLoc == "10100100500500" && GiaTu == "0" && GiaDen == "0")
        {
            sql += " and TD.TuGia between 10000000 and 100000000 or TD.TuGia between 100000000 and 500000000 or TD.TuGia >= 500000000";
        }
        if (GiaLoc == "10100" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TuGia between 10000000 and 100000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' )";
        }
        if (GiaLoc == "100500" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TD.TuGia between 100000000 and 500000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "')";
        }
        if (GiaLoc == "500" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TuGia >= 500000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' )";
        }
        if (GiaLoc == "10100500" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TuGia between 10000000 and 100000000 or TuGia >= 500000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' )";
        }
        if (GiaLoc == "100500500" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TuGia between 100000000 and 500000000 or TuGia >= 500000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' )";
        }
        if (GiaLoc == "10100100500" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TD.TuGia between 10000000 and 100000000 or TD.TuGia between 100000000 and 500000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' )";
        }
        if (GiaLoc == "10100100500500" && GiaTu != "1" && GiaDen != "0")
        {
            sql += " and (TD.TuGia between 10000000 and 100000000 or TD.TuGia between 100000000 and 500000000 or TuGia >= 500000000 or TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' )";
        }   
        if (idTinh != "0" && idTinh != "")
            sql += " and TD.idTinh='" + idTinh + "'";
        if (idHuyen != "0" && idHuyen != "")
            sql += " and TD.idHuyen='" + idHuyen + "'";
        if (idPhuongXa != "0" && idPhuongXa != "")
            sql += " and TD.idPhuongXa='" + idPhuongXa + "'";
        if (TenCuaHang != "")
            sql += " and TD.TieuDe LIKE N'%" + TenCuaHang + "%'";

        sql += ") as tb1 ";
        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += "WHERE RowNumber BETWEEN (" + Pages + " - 1) * " + PageSize + " + 1 AND (((" + Pages + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        DataTable table = Connect.GetTable(sql);
        string html = "";
        if (table.Rows.Count > 0)
        {
            html += "<ul>";
            SetPage(AllRowNumber);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim()));
                string urlTinDangChiTiet = Domain + "/tdct/" + table.Rows[i]["DuongDan"].ToString();

                string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + table.Rows[i]["idTinDang"].ToString() + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string urlHinhAnh = "";
                if (tbHinhAnh.Rows.Count > 0)
                {
                    if (System.IO.File.Exists(Server.MapPath("/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"])))
                        urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"];
                    else
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                }
                else
                    urlHinhAnh = Domain + "/images/icons/noimage.png";

                string TenHuyen1 = StaticData.getField("District", "Ten", "Id", table.Rows[i]["idHuyen"].ToString());
                string idTinh1 = table.Rows[i]["idTinh"].ToString();
                string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
                string idPhuongXa1 = table.Rows[i]["idPhuongXa"].ToString();
                string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
                string chuoi_DiaDiem = "";

                //if (TenPhuongXa1 != "")
                //    chuoi_DiaDiem = TenPhuongXa1;
                //else
                //{
                //    if (TenHuyen1 != "")
                //        chuoi_DiaDiem = TenHuyen1;
                //    else
                //    {
                //        if (TenTinh1 != "")
                //            chuoi_DiaDiem = TenTinh1;
                //        else
                //            chuoi_DiaDiem = "Toàn quốc";
                //    }
                //}
                chuoi_DiaDiem = TenTinh1;
                if (TenTinh1 == "")
                    chuoi_DiaDiem = "Toàn quốc";
                string LoaiTinDang = "Cần bán";
                if (table.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                    LoaiTinDang = "Cần mua";
				 string GioDang = TinhThoiGianLucDang(DateTime.Parse(table.Rows[i]["NgayDayLenTop"].ToString()));
                html += @"
                           <div>
                               <li class='ctAdListingWrapper listView' style='border-top: 1px solid #e9ebee;'>
                                   <a rel='nofollow' class='ctAdListingItem' action='push' href='" + urlTinDangChiTiet + @"'>
                                       <div class='ctAdLitingThumbnail'>
                                           <img alt='" + table.Rows[i]["TieuDe"].ToString() + @" - Tung Tăng' class='lazyload thumbnailImageListing' src='" + urlHinhAnh + @"'>
                                       </div>
                                       <div class='ctAdListingBody'>
                                           <h3 class='ctAdListingTitle'>" + (table.Rows[i]["Ishot"].ToString().Trim() == "True" ? ("<b>" + table.Rows[i]["TieuDe"] + "</b>") : (table.Rows[i]["TieuDe"])) + @"</h3>
                                           <div class='ctAdListingUpperBody'>
                                               <span class='ctAdListingPrice'>
                                                   <span class='adPriceMobile320'>" + double.Parse(KiemTraKhongCo_LoadLen(table.Rows[i]["TuGia"].ToString())).ToString("#,##").Replace(",", ".") + @" đ</span><span class='adPriceNormal'>" + double.Parse( KiemTraKhongCo_LoadLen( table.Rows[i]["TuGia"].ToString())).ToString("#,##").Replace(",", ".") + @" đ</span> 
                                               </span>
                                           </div>
                                       </div> ";
                if (table.Rows[i]["Ishot"].ToString().Trim() == "True")
                    html += @"<div class='ctRibbonAd' style='background-color: rgb(245, 154, 0); border-color: rgb(245, 154, 0); color: rgb(255, 255, 255);z-index:0;'>PRO</div>";
                html += @"
                                   </a>
                                   <div class='ctAdListingLowerBody false'>  
                                       <div class='adItemPostedTime'><span>" + LoaiTinDang + @"</span>
									</div>
									 <span class='delimeter'></span>
                                         <div class='adItemPostedTime'><span>"  + GioDang + @"</span></div>
                                      
                                       <span class='delimeter'></span>
                                       <div class='adItemPostedTime'><span>" + chuoi_DiaDiem + @"</span></div>
					<span class='delimeter'></span>
                                       <div class='adItemPostedTime'><span>" + " <i class='fa fa-eye'></i> "+ " " +table.Rows[i]["SoLuotXem"] + @"</span></div>
                                   </div>
                               </li>
                           </div> ";

            }
            html += "</ul>";
            string html_Trang = "";

            string url = "";
            url = Domain + "/tin-dang/" + ChuoiLoai + "?";
			 if (idDanhMucCap2 != "")
            {
                string LinkTenDM2 = StaticData.getField("tb_DanhMucCap2", "LinhTenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2);
                url += LinkTenDM2 + "&";
            }
            if (BoLocThem != "")
                url += "BLT=" + BoLocThem + "&";
			 if (sGia == "10100")
            {
                url += "Price=10100";
            }
            if (sGia == "100500")
            {
                url += "Price=100500";
            }
            if (sGia == "500")
            {
                url += "Price=500";
            }
            if (sGia == "10100100500")
            {
                url += "Price=10100100500";
            }
            if (sGia == "10100500")
            {
                url += "Price=10100500";
            }
            if (sGia == "100500500")
            {
                url += "Price=100500500";
            }
            if (sGia == "10100100500500")
            {
                url += "Price=10100100500500";
            }
            if (idTinh != "")
                url += "TT=" + idTinh + "&";
            if (idHuyen != "")
                url += "QH=" + idHuyen + "&";
            if (idPhuongXa != "")
                url += "PX=" + idPhuongXa + "&";
            if (TenCuaHang != "")
                url += "CH=" + TenCuaHang + "&";
           // if (idDanhMucCap2 != "")
          //      url += "C2=" + idDanhMucCap2 + "&";
            url += "P=";
            html_Trang += @"    <li class='sc-jhAzac jDyZht'><a class='first' itemprop='url' href='" + url + txtFistPage + @"'>«</a></li>";
            //Page 1
            if (txtPage1 != "")
            {
                if (Pages.ToString() == txtPage1)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage1 + "'>" + txtPage1 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage1 + "'>" + txtPage1 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 2
            if (txtPage2 != "")
            {
                if (Pages.ToString() == txtPage2)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage2 + "'>" + txtPage2 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage2 + "'>" + txtPage2 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 3
            if (txtPage3 != "")
            {
                if (Pages.ToString() == txtPage3)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage3 + "'>" + txtPage3 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage3 + "'>" + txtPage3 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 4
            if (txtPage4 != "")
            {
                if (Page.ToString() == txtPage4)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage4 + "'>" + txtPage4 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage4 + "'>" + txtPage4 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 5
            if (txtPage5 != "")
            {
                if (Pages.ToString() == txtPage5)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage5 + "'>" + txtPage5 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage5 + "'>" + txtPage5 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            if (Pages.ToString() == txtLastPage)
                html_Trang += "<li class='sc-jhAzac jDyZht'><a class='last' itemprop='url' href='" + url + txtLastPage + "'>›</a></li>";
            else
                html_Trang += "<li class='sc-jhAzac jDyZht'><a class='last' itemprop='url' href='" + url + (Pages + 1) + "'>›</a></li>";

            ulTrang.InnerHtml = html_Trang;
        }
        else
        {
            html = @"<div class='alert alert-warning'>
                      Không tìm thấy kết quả nào</a>.
                    </div>";
        }
        //Quang Cao 
        //html += "   <div class='ads1'><img src='/images/Advertisement/ads-1.gif' style='width:100%;max-height:600px;'/></div>";
        //html += "   <div class='ads2'><img src='/images/Advertisement/ads-2.gif' style='width:100%;max-height:600px;'/></div>";
        //
        dvTinDang.InnerHtml = html;
			
		 string sqltableAnh = "select * from tb_BannerTinDang where idBannerTinDang=1 or idBannerTinDang=2 ";
        DataTable tbHinhAnhTinDang = Connect.GetTable(sqltableAnh);
        string srcHinhAnh1 = StaticData.getField("tb_BannerTinDang", "'../' + LinkAnh", "idBannerTinDang", "1");
        string srcHinhAnh2 = StaticData.getField("tb_BannerTinDang", "'../' + LinkAnh", "idBannerTinDang", "2");
        string LinkAnh1 = tbHinhAnhTinDang.Rows[0]["Url"].ToString();
        string LinkAnh2 = tbHinhAnhTinDang.Rows[1]["Url"].ToString();
        if (tbHinhAnhTinDang.Rows.Count > 0)
        {
                AnhTinDang.InnerHtml = @"
                               <a href='" + LinkAnh1 + "' target='_blank'>  <img style='width: 100%;max-height: 600px;' alt='Tung Tăng' src = '" + srcHinhAnh1 + "'/> </a> <hr style='margin: 5px; ' /> <a href = '" + LinkAnh2 + "' target = '_blank'>  <img style = 'width: 100%;' alt = 'Tung Tăng' src = '" + srcHinhAnh2 + "' /> </a> ";
        }

    }
    string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }
    void LoadMinMax_rangeBoLoc(string idDanhMuc, string CapDanhMuc, string GiaTuHienTai, string GiaDenHienTai)
    {
        DataTable tableMucGiaDanhMuc;
        decimal GiaBoLoc = 10000000;
        decimal StepGiaBoLoc = 100000;
        try
        {
            if (CapDanhMuc == "Cap2")
                tableMucGiaDanhMuc = Connect.GetTable("select MucGia_BoLoc,StepMucGia_BoLoc from tb_DanhMucCap2 where idDanhMucCap2=" + idDanhMuc);
            else
                tableMucGiaDanhMuc = Connect.GetTable("select MucGia_BoLoc,StepMucGia_BoLoc from tb_DanhMucCap1 where idDanhMucCap1=" + idDanhMuc);

            GiaBoLoc = decimal.Parse(tableMucGiaDanhMuc.Rows[0]["MucGia_BoLoc"].ToString());
            StepGiaBoLoc = decimal.Parse(tableMucGiaDanhMuc.Rows[0]["StepMucGia_BoLoc"].ToString());
        }
        catch
        {
             GiaBoLoc = 10000000;
            StepGiaBoLoc = 100000;
        }
        //decimal 

        if (GiaBoLoc == 0)
        {
            GiaBoLoc = 10000000;
        }



        //rgGiaTu.Attributes.Add("max", (GiaBoLoc / 2).ToString());
        rgGiaTu.Attributes.Add("step", StepGiaBoLoc.ToString());

        rgMaxGiaDen.Attributes.Add("max", GiaBoLoc.ToString());
        rgGiaDen.Attributes.Add("max", (GiaBoLoc + StepGiaBoLoc).ToString());
        //rgGiaDen.Attributes.Add("min", (GiaBoLoc / 2).ToString());
        rgGiaDen.Attributes.Add("step", StepGiaBoLoc.ToString());




        if (GiaDenHienTai == "")
        {
            rgGiaDen.Value = (GiaBoLoc + StepGiaBoLoc).ToString();
            spGiaDen.InnerHtml = GiaBoLoc.ToString("N0") + "";
        }
       // if (decimal.Parse(KiemTraKhongCo_LoadLen(GiaTuHienTai)) > (GiaBoLoc / 2))
       // {
           // rgGiaTu.Value = "0";
            //spGiaTu.InnerHtml = "0";
       // }
        //rgGiaDen.Value = (GiaBoLoc).ToString();
        spGiaDen.InnerHtml = (GiaBoLoc / 2).ToString("N0") + "";
    }
    string LocChuoiLoai(string chuoiLoai)
    {
        string[] arr = chuoiLoai.ToCharArray().Select(c => c.ToString()).ToArray();
        string KQ = "";
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == "?")
                break;
            else
                KQ += arr[i];
        }
        return KQ;
    }
    string KiemTraKhongCo_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }


    private void LoadMoTa()
    {
        if(idDanhMucCap1 !="")
        {
            Motahomepage.InnerHtml = StaticData.getField("[DanhMucSeo]", "[NoiDung]", "[IdDanhMuc]",idDanhMucCap1);
        }
        else if(idDanhMucCap1 =="")
        {
            Motahomepage.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "MOTAHOMEPAGE");
        }
       
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        string idLinhVuc = ddlLoaiDanhMuc.Value.Trim();
        string idLinhVucCap2 = ddlLoaiDanhMucCap2.Value.Trim();
        string TenLinhVuc = txtLoaiDanhMuc.InnerHtml.Trim().Split('>')[0].Split('<')[0];
        string TinhThanh = txtTinhThanh.Value.Trim();
        string QuanHuyen = txtQuanHuyen.Value.Trim();
        string PhuongXa = txtPhuongXa.Value.Trim();
        string TenCuaHang = txtKeySearch.Value.Trim();
        string BoLocThem = txtBoLocThem.Value.Trim();
        string url = "";
        string Linkid2 = "";

        string sUrl = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "").Replace("%2F", "/");
        string BodauURL = StaticData.BoDauTiengViet(sUrl);
        string[] arrUrl = BodauURL.Split('/', '?', '&');
        url = Request.Url.AbsolutePath + "?";
        if (idLinhVuc != "")
        {
            string TieuDe1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TenLinhVuc));
            url = "/tin-dang/" + TieuDe1;
            if (idLinhVuc == "0")
            {
                url = "/tin-dang/td?";
            }
        }
        try
        {
            if (arrUrl[2] != "")
            {
                if (idLinhVuc != "")
                {
                    string TenLVCap1 = StaticData.getField("tb_DanhMucCap1", "LinkTenDanhMucCap1", "idDanhMucCap1", idLinhVuc);
                    if (TenLVCap1 != "")
                    {
                        url += TenLVCap1 + "?";
                    }
                    else
                    {
                        url += TenLVCap1;
                    }
                }
            }
            //if (idLinhVucCap2 != "")
            //{
            //    // url += "/";
            //    string TenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idLinhVucCap2);
            //    url += TenLVCap2;
            //}            
        }
        catch { }
        try
        {
            if (TinhThanh != "" && TinhThanh != "0")
            {
                string LinkTThanh = StaticData.getField("City", "Link", "id", TinhThanh);
                if (LinkTThanh != "")
                {
                    url += LinkTThanh;
                }
            }
        }
        catch { }
        try
        {
            if (QuanHuyen != "" && QuanHuyen != "0")
            {
                string LinkQH = StaticData.getField("District", "Link", "id", QuanHuyen);
                if (LinkQH != "")
                {
                    url += "/" + LinkQH;
                }
            }
        }
        catch { }
        try
        {
            if (PhuongXa != "" && PhuongXa != "0")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "Link", "id", PhuongXa);
                if (LinkPX != "")
                {
                    url += "/" + LinkPX;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[3] != "" && idLinhVucCap2 == "" && TinhThanh == "0")
            {
                string LinkTThanh = StaticData.getField("City", "id", "Link", arrUrl[3]);
                if (LinkTThanh != "")
                {
                    string LinkTT = StaticData.getField("City", "Link", "id", LinkTThanh);
                    string TestTT = StaticData.getField("City", "Link", "id", TinhThanh);
                    if (LinkTT != "" && TestTT != "" && TestTT == LinkTT)
                    {
                        url += LinkTT;
                    }
                }
                if (LinkTThanh == "")
                {
                    string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[3]);
                    if (LinkTenLVCap2 != "")
                    {
                        string LinkTC2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                        string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", LinkTC2);
                        string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                        if (Test != "" && LinkTC2 != "" && idLinhVuc == LinkTC2Test)
                        {
                            url += "/" + LinkTC2;
                        }
                    }
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[3] != "" && idLinhVucCap2 != "" && TinhThanh == "0")
            {
                string LinkTThanh = StaticData.getField("City", "id", "Link", arrUrl[3]);
                if (LinkTThanh != "")
                {
                    string LinkTT = StaticData.getField("City", "Link", "id", LinkTThanh);
                    string TestTT = StaticData.getField("City", "Link", "id", TinhThanh);
                    if (LinkTT != "" && TestTT != "" && TestTT == LinkTT)
                    {
                        url += LinkTT;
                    }
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[3] != "" && idLinhVucCap2 == "" && TinhThanh != "0")
            {
                string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[3]);
                if (LinkTenLVCap2 != "")
                {
                    string LinkTC2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                    string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", LinkTC2);
                    string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                    if (Test != "" && LinkTC2 != "" && idLinhVuc == LinkTC2Test)
                    {
                        url += "/" + LinkTC2;
                    }
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[4] != "" && idLinhVucCap2 == "" && QuanHuyen == "")
            {
                string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[4]);
                string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
                string TestQH = StaticData.getField("District", "Link", "id", QuanHuyen);
                if (LinkQH2 != "" && TestQH != "" && TestQH == LinkQH2)
                {
                    url += "/" + LinkQH2;
                }
                if (LinkQH == "")
                {
                    string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[4]);
                    if (LinkTenLVCap2 != "")
                    {
                        string LinkTC2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                        string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", LinkTC2);
                        string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                        if (Test != "" && LinkTC2 != "" && idLinhVuc == LinkTC2Test)
                        {
                            url += "/" + LinkTC2;
                        }
                    }
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[4] != "" && idLinhVucCap2 != "" && QuanHuyen == "")
            {
                string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[4]);
                string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
                string TestQH = StaticData.getField("District", "Link", "id", QuanHuyen);
                if (LinkQH2 != "" && TestQH != "" && TestQH == LinkQH2)
                {
                    url += "/" + LinkQH2;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[4] != "" && idLinhVucCap2 == "" && QuanHuyen != "")
            {
                string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[4]);
                string LinkTenLVCap21 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", LinkTenLVCap21);
                string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                if (Test != "" && LinkTenLVCap21 != "" && idLinhVuc == LinkTC2Test)
                {
                    url += "/" + LinkTenLVCap21;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[5] != "" && idLinhVucCap2 == "" && PhuongXa == "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[5]);
                string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                string TestPX = StaticData.getField("tb_PhuongXa", "Link", "id", PhuongXa);
                if (LinkPX2 != "" && TestPX != "" && TestPX == LinkPX2)
                {
                    url += "/" + LinkPX2;
                }
                if (LinkPX2 == "")
                {
                    string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[5]);
                    string LinkTenLVCap21 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                    string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", LinkTenLVCap21);
                    string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                    if (Test != "" && LinkTenLVCap21 != "" && idLinhVuc == LinkTC2Test)
                    {
                        url += "/" + LinkTenLVCap21;
                    }
                }
            }

        }
        catch { }
        try
        {
            if (arrUrl[5] != "" && idLinhVucCap2 != "" && PhuongXa == "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[5]);
                string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                string TestPX = StaticData.getField("tb_PhuongXa", "Link", "id", PhuongXa);
                if (LinkPX2 != "" && TestPX != "" && TestPX == LinkPX2)
                {
                    url += "/" + LinkPX2;
                }
            }

        }
        catch { }
        try
        {
            if (arrUrl[5] != "" && idLinhVucCap2 == "" && PhuongXa != "")
            {
                string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[5]);
                string LinkTenLVCap21 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", LinkTenLVCap21);
                string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                if (Test != "" && LinkTenLVCap21 != "" && idLinhVuc == LinkTC2Test)
                {
                    url += "/" + LinkTenLVCap21;
                }
            }

        }
        catch { }
        string idLinhVucCap22 = ddlLoaiDanhMucCap2.Value.Trim();
        try
        {
            if (arrUrl[6] != "" && idLinhVucCap22 == "" && QuanHuyen == "" && TinhThanh == "0")
            {
                string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
                string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                if (LinkTenLVCap2 != "" && Test != "")
                {
                    string Linkc2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                    string Test2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", Test);
                    string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", Linkc2);
                    if (Test != "" && Linkc2 != "" && idLinhVuc == LinkTC2Test)
                    {
                        url += "/" + Linkc2;
                    }
                }
                if (LinkTenLVCap2 == "")
                {
                    string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[6]);
                    if (LinkQH != "")
                    {
                        string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
                        url += "/" + LinkQH2;
                    }
                }
            }
        }

        catch { }
        try
        {
            if (arrUrl[6] != "" && idLinhVucCap22 == "" && TinhThanh != "0" && QuanHuyen == "")
            {
                string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
                string Linkc2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", Linkc2);
                if (Test != "" && Linkc2 != "" && idLinhVuc == LinkTC2Test)
                {
                    url += "/" + Linkc2;
                }
                //if (LinkTenLVCap2 == "")
                //{
                //    string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[6]);
                //    if (LinkQH != "")
                //    {
                //        string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
                //        url += "/" + LinkQH2;
                //    }
                //}
            }
        }

        catch { }
        try
        {
            if (arrUrl[6] != "" && idLinhVucCap22 == "" && TinhThanh != "0" && QuanHuyen != "")
            {
                string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
                if (LinkTenLVCap2 != "")
                {
                    string Linkc2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                    string Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "idDanhMucCap1", idLinhVuc);
                    string LinkTC2Test = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "LinkTenDanhMucCap2", Linkc2);
                    if (Test != "" && Linkc2 != "" && idLinhVuc == LinkTC2Test)
                    {
                        url += "/" + Linkc2;
                    }
                }
                //if (LinkTenLVCap2 == "")
                //{
                //    string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[6]);
                //    if (LinkQH != "")
                //    {
                //        string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
                //        url += "/" + LinkQH2;
                //    }
                //}
            }
        }

        catch { }
        try
        {
            if (arrUrl[6] != "" && idLinhVucCap22 != "" && TinhThanh == "0" && QuanHuyen == "")
            {
                //string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idLinhVucCap22);
                //if (LinkTenLVCap2 != "")
                //{
                //   // string Linkc2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkTenLVCap2);
                //    url += "/" + LinkTenLVCap2;
                //}
                string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[6]);
                if (LinkQH != "")
                {
                    string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
                    url += "/" + LinkQH2;
                }
            }
        }

        catch { }
        //try
        //{
        //    string KiemtraBang = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
        //    string KiemTraBang2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
        //    if (arrUrl[6] != "" && KiemTraBang2 == "" && TinhThanh == "0")
        //    {
        //        string LinkQH = StaticData.getField("District", "id", "Link", arrUrl[6]);
        //        if (LinkQH != "")
        //        {
        //            string LinkQH2 = StaticData.getField("District", "Link", "id", LinkQH);
        //            url += "/" + LinkQH2;
        //        }                
        //        //string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idLinhVucCap22);
        //        //if (LinkTenLVCap2 != "")
        //        //{
        //        //    url += "/" + LinkTenLVCap2;
        //        //}
        //    }
        //}
        //catch { }
        try
        {
            if (arrUrl[9] != "" && PhuongXa == "" && TinhThanh == "0" && QuanHuyen == "" && idLinhVucCap22 == "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[9]);
                if (LinkPX != "")
                {
                    string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                    url += "/" + LinkPX2;
                }
                if (LinkPX == "")
                {
                    string LinkPX2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[9]);
                    string LinkPX3 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkPX2);
                    url += "/" + LinkPX3;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[9] != "" && PhuongXa == "" && TinhThanh == "0" && QuanHuyen == "" && idLinhVucCap22 != "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[9]);
                if (LinkPX != "")
                {
                    string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                    url += "/" + LinkPX2;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[9] != "" && PhuongXa == "" && TinhThanh != "0" && QuanHuyen == "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[9]);
                //if (LinkPX != "")
                //{
                //    string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                //    url += "/" + LinkPX2;
                //}
                if (LinkPX == "")
                {
                    string LinkPX2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[9]);
                    string LinkPX3 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkPX2);
                    url += "/" + LinkPX3;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[9] != "" && PhuongXa == "" && TinhThanh != "0" && QuanHuyen != "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[9]);
                //if (LinkPX != "")
                //{
                //    string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                //    url += "/" + LinkPX2;
                //}
                if (LinkPX == "")
                {
                    string LinkPX2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[9]);
                    string LinkPX3 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkPX2);
                    url += "/" + LinkPX3;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[9] != "" && PhuongXa != "" && TinhThanh != "0" && QuanHuyen != "")
            {
                string LinkPX = StaticData.getField("tb_PhuongXa", "id", "Link", arrUrl[9]);
                //if (LinkPX != "")
                //{
                //    string LinkPX2 = StaticData.getField("tb_PhuongXa", "Link", "id", LinkPX);
                //    url += "/" + LinkPX2;
                //}
                if (LinkPX == "")
                {
                    string LinkPX2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[9]);
                    string LinkPX3 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkPX2);
                    url += "/" + LinkPX3;
                }
            }
        }
        catch { }
        try
        {
            if (arrUrl[12] != "" && idLinhVucCap22 == "")
            {
                string LinkC2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[12]);
                if (LinkC2 != "")
                {
                    string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", LinkC2);
                    url += "/" + LinkTenLVCap2;
                }
            }
        }
        catch { }
        if (idLinhVucCap22 != "")
        {
            string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idLinhVucCap22);
            if (LinkTenLVCap2 != "")
            {
                try
                {
                    if (arrUrl[3] != "" || arrUrl[6] != "")
                    {
                        string LinkTenLVCap22 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[3]);
                        if (LinkTenLVCap22 != "")
                        {
                            url += LinkTenLVCap2;
                        }
                        else
                        {
                            string LinkTenLVCap23 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
                            if (LinkTenLVCap23 != "")
                            {
                                url += "/" + LinkTenLVCap2;
                            }
                            else
                            {
                                url += "/" + LinkTenLVCap2;
                            }
                        }
                    }
                }
                catch
                {
                    if (arrUrl.Count() > 3)
                        url += "/" + LinkTenLVCap2;
                }
                try
                {
                    if (arrUrl.Count() <= 3)
                    {
                        url += LinkTenLVCap2;
                    }
                }
                catch { }
            }
        }

        //string Label = listLabel.Text.Trim();
        //DataTable idLVCap2 = Connect.GetTable("select * from tb_DanhMucCap2 where idDanhMucCap1=" + idLinhVuc.Trim());                       
        //try
        //{
        //    if (idLinhVucCap22 != "" && TinhThanh != "0")
        //    {
        //        string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idLinhVucCap22);
        //        url += "/" + LinkTenLVCap2;
        //    }
        //    if (idLinhVucCap22 != "" && arrUrl[3] != "")
        //    {
        //        string LinkTenLVCap2 = StaticData.getField("tb_DanhMucCap2", "LinkTenDanhMucCap2", "idDanhMucCap2", idLinhVucCap22);
        //        url += "/" + LinkTenLVCap2;
        //    }
        //}
        //catch { }
        if (BoLocThem != "")
        {
            if (BoLocThem != "" && TinhThanh != "" || QuanHuyen != "" || PhuongXa != "" || idDanhMucCap2 != "")
            {
                url += "&" + "BLT=" + BoLocThem + "&";
            }
            else if (BoLocThem != "" && TinhThanh == "" || QuanHuyen == "" || PhuongXa == "" || idDanhMucCap2 == "")
            {
                url += "BLT=" + BoLocThem + "&";
            }
        }
        if (TenCuaHang != "")
            url += "CH=" + TenCuaHang + "&";
        Response.Redirect(url);
    }
}