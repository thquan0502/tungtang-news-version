﻿<%@ Page Title="Blog" Language="C#" MasterPageFile="~/MasterPage - Copy.master" AutoEventWireup="true" CodeFile="News-Copy.aspx.cs" Inherits="News_Copy" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="../Css/Page/TinDang.css" rel="stylesheet" />
    <script src="../Js/Page/TinDang.min.js"></script>
    <style>
        .ur:hover{
            background-color:#ffba00;
        }
        .form-control {
            border: 0.5px solid #ddd;
            padding-right: 20px;
        }

        #vnw-home .top-job ul li {
            margin: 3px 0;
        }

        .jDyZht a.first {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .ctStickyAdsListing .ctAdListingWrapper .ctAdListingLowerBody .adItemPostedTime span {
            opacity: 0.5;
        }

        .foricon{
            width: 15px;
        }
         .foricon2{
            width: 15px;
        }
        .spDT
        {
           font-size: 11px;color: gray;  padding-top:5px; color:#008C72;
        }
        @media only screen and (max-width: 767px) {
.spDT
        {
           font-size: 10px;color: gray;color:#008C72;
         padding-top:1px;
        }


.h5An
{
    display:none;
}

 .foricon{
            width: 15px;
        }

  .foricon2{
            width: 15px; margin-bottom: 1px;
        }
}
		.r{
			height:125px;
		}
        /*ur .i:hover{
            color:#ffba00;

        }
        cursor .span:hover{
            color:#ffba00;
        }*/
    </style> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<div class="modal-DiaDiem-cls">
        <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
            <div id="dvDSDiaDiem" style="overflow: auto;">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbDiaDiem_tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal-DiaDiem-cls">
        <div id="modalDanhMuc" class="modal" style="overflow: hidden;">
            <div id="dvDSDanhMuc" style="overflow: auto;">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                <div style="text-align: center; font-size: 17px;">Chọn danh mục</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbDanhMuc_tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal-DiaDiem-cls">
        <div id="modalBoLoc" class="modal" style="overflow: hidden;">
            <div id="dvBoLoc" style="overflow: auto;">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <div style="text-align: center; font-size: 17px;">Lọc kết quả</div>
                            </th>
                        </tr>
                    </thead>
                </table>
                <br />
                <div class="col-sm-6">
                    <label>Giá từ <span id="spGiaTu" runat="server">0</span></label>
                    <input type="range" id="rgGiaTu" value="0" max="15000000000" step="100000000" oninput="GiaTu(this.value)" runat="server" />
                </div>
                <div class="col-sm-6">
                    <label>Đến <span id="spGiaDen" runat="server">3,000,000,000</span></label>
                    <input type="range" id="rgMaxGiaDen" runat="server" style="display: none;" />
                    <input type="range" id="rgGiaDen" value="15000000000" min="15000000000" max="30000000000" step="100000000" oninput="GiaDen(this.value)" runat="server" />
                </div>
                <br />
                <label>Sắp xếp theo</label>
                <div class="DanhMucBoLoc">
                    <div class="styled-input-single radio">
                        <input type="radio" name="fieldset-3" id="radTinMoiTruoc" runat="server" />
                        <label for="ContentPlaceHolder1_radTinMoiTruoc"><i class="fa fa-clock-o"></i>Tin mới trước</label>
                    </div>
                    <div class="styled-input-single radio">
                        <input type="radio" name="fieldset-3" id="radGiaThapTruoc" runat="server" />
                        <label for="ContentPlaceHolder1_radGiaThapTruoc"><i class="fa fa-dollar"></i>Giá thấp trước</label>
                    </div>
                </div>
                <br />
                <label>Bạn cần</label>
                <div class="DanhMucBoLoc">
                    <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="fieldset-5" id="ckbCanMua" runat="server" checked />
                        <label for="ContentPlaceHolder1_ckbCanMua"><i class="fa fa-cart-arrow-down"></i>Mua/thuê</label>
                    </div>
                    <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="fieldset-5" id="ckbCanBan" runat="server" checked />
                        <label for="ContentPlaceHolder1_ckbCanBan"><i class="fa fa-archive"></i>Bán/Cho Thuê</label>
                    </div>
                </div>

                <div class="col-sm-12">
                    <a class="btn btn-primary btn-search-all" style="width: 100%; margin: 10px 0;" onclick="btnBoLoc_click();">ÁP DỤNG</a>
                </div>
            </div>
        </div>
    </div>--%>

    <main class="App__Main-fcmPPF dlWmGF">
        <div class="XauAgRup4nyZifS5XeIT8 container">
            <div class="row v68hxsOjZ91PAUhcMxbPI">
                <div class="col-xs-12 _13_3ohNSwkOXu5Kk_Rwn4F">
                    <header>
                        <div class="gEQeZ-pfX6E9qs-T0zFQG">
                            <ol class="breadcrumb _3WIL_EScB1tm02Oqj0vbu8" id="dvLinkTitle2" runat="server">
                                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu"><a href="/"><span>Trang chủ</span></a></li>
                                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu">
                                    <span itemprop="item"><strong itemprop="name">Blog</strong></span>
                                </li>
                            </ol>
                        </div>
                        
                    </header>
                    <main>
                        <div class="row no-margin">
                            <div class="col-md-12 _2kTkY3Nmr5OemSQ8lNaNal">
                                
                                 <div class="row ">
                                    <div class="col-xs-12 undefined">
                                        <div class="row">
                                            <div class="UKoJK4TUh1CFDy77SUIO2 col-xs-12">
                                                
                                                <input id="ddltags" runat="server" style="padding-left: 0; padding-right: 0; display: none;" />
                                              
                                                <asp:LinkButton ID="btnTimKiem" runat="server" class="btn btn-primary btn-search-all" OnClick="btnTimKiem_Click" Style="display: none;">TK</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                <div>
                                    <div class="pgjTolSNq4-L--tpQ7Xp5" style="border-left: 3px solid #ffba00;border-radius:0px;margin-left:5px;">Tags</div>
                                    <ul class="sc-chPdSV UEYyu r" width="100%" style="height: 114px;margin: 0px 0px 1px; padding-bottom: 5px; display: none; border:none;"  id="dvTags" runat="server">
                                    </ul>
                                </div>
                                <div class="pgjTolSNq4-L--tpQ7Xp5">
                                    <div>
                                        <div class="ctStickyAdsListing" id="dvTinDang" runat="server">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="sc-bRBYWo sjho">
                                        <ul class="sc-hzDkRC bmmuID" id="ulTrang" runat="server">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                         <%--   <div class="col-md-4">
                                <img src='/images/Advertisement/ads-1.png' style='width: 100%; max-height: 600px;' />
                                <img src='/images/Advertisement/ads-2.gif' style='width: 100%; max-height: 600px;' />
                            </div>--%>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </main>

</asp:Content>

