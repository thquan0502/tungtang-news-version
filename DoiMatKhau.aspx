﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DoiMatKhau.aspx.cs" Inherits="DangKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
        }
    </style>
    <script>
        window.onload = function () {
             
        }
        function hasUnicode(str)
        {
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) return true;
            }
            return false;
        }
        function LuuThongTin() {
            var flag = false;
            var MatKhauOLD = $("#ContentPlaceHolder1_txtMatKhauOLD").val();
            var MatKhau1 = $("#ContentPlaceHolder1_txtMatKhau").val();
            var MatKhau2 = $("#ContentPlaceHolder1_txtNhapLaiMatKhau").val();

            //////////////////////////////
            if (MatKhauOLD == "") { 
                $("#MessageMatKhauOLD").html('Vui lòng nhập Mật khẩu cũ');
                $("#MessageMatKhauOLD").show();
                flag = true;
            }
            else
                $("#MessageMatKhauOLD").hide();
            //////////////////////////////
            if (MatKhau1 == "") {
                $("#MessageMatKhau1").show();
                flag = true;
            }
            else
                $("#MessageMatKhau1").hide(); 
            //////////////////////////////
            if (MatKhau2 == "") {
                $("#MessageMatKhau2").show();
                flag = true;
            }
            else
                $("#MessageMatKhau2").hide();
            //////////////////////////////
            if (hasUnicode(MatKhau1)) {
                $("#MessageMatKhau1").html('Mật khẩu không được có dấu');
                $("#MessageMatKhau1").show();
                flag = true;
            }
            else
                $("#MessageMatKhau1").hide();
            //////////////////////////////
            if (MatKhau1.trim() != MatKhau2.trim()) {
                $("#MessageMatKhau2").show();
                flag = true;
            }
            else
                $("#MessageMatKhau2").hide();

            if (flag)
                return;

            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "equal") {
                        document.getElementById("ContentPlaceHolder1_btnLuuThayDoi").click();
                    }
                    else {
                        $("#MessageMatKhauOLD").html('Mật khẩu cũ không khớp'); 
                        $("#MessageMatKhauOLD").show();
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=SoSanhMatKhau&MK="+MatKhauOLD, true);
            xmlhttp.send();

        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px;">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div id="vnw-log-in" class="container main-content"> 
                        <div class="col-md-8">
                            <h3>ĐỔI MẬT KHẨU </h3>
                            <hr />
                            <div class="row"> 
                                <div class="col-md-8 col-sm-8">
                                    <div class="infoUser">
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Mật khẩu cũ(<span style="color: red">*</span>):</b></div>
                                            <input type="password" id="txtMatKhauOLD" name="form[password]" required="required" tabindex="2" class="form-control" runat="server" />
                                            <p id="MessageMatKhauOLD" style="color: red; font-size: 12px; display: none;">Vui lòng nhập Mật khẩu cũ</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Mật khẩu mới (<span style="color: red">*</span>):</b></div>
                                            <input type="password" id="txtMatKhau" name="form[password]" required="required" tabindex="2" class="form-control" runat="server" />
                                            <p id="MessageMatKhau1" style="color: red; font-size: 12px; display: none;">Vui lòng nhập Mật khẩu mới</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Nhập lại mật khẩu mới(<span style="color: red">*</span>):</b></div>
                                            <input type="password" id="txtNhapLaiMatKhau" name="form[password]" required="required" tabindex="2" class="form-control" runat="server" />
                                            <p id="MessageMatKhau2" style="color: red; font-size: 12px; display: none;">Xác nhận mật khẩu không khớp</p>
                                        </div>    
                                        <!-- Buttons-->
                                        <div class="form-group" style="margin-top: 20px">
                                            <a class="btn btn-primary btn-block" onclick="LuuThongTin()">LƯU THAY ĐỔI</a>
                                            <asp:LinkButton ID="btnLuuThayDoi" runat="server" class="btn btn-primary btn-block" OnClick="btnLuuThayDoi_Click" Style="display: none;">LƯU THAY ĐỔI</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </section>
    <script type="text/javascript">
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        function UploadHinhAnh_Onchange(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                $("#ContentPlaceHolder1_DaThayDoi_imgHinhAnh").val('1');
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</asp:Content>
