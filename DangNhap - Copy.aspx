﻿<%@ Page Title="Đăng Nhập" EnableEventValidation="false" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="DangNhap - Copy.aspx.cs" Inherits="DangNhap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script> 
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     <style type="text/css">
        .btn-circle {   
        width: 30px;    
        height: 30px;   
        text-align: center; 
        padding: 6px 0; 
        font-size: 12px;    
        line-height: 1.428571429;   
        border-radius: 15px;
        }
        .btn1:hover {
            background-color:#1778f2;
        }
        .btn1 {
            background-color:#1778f2;
        }
        .btn-circle.btn-lg {    
        width: 50px;    
        height: 50px;   
        padding: 10px 16px; 
        font-size: 18px;    
        line-height: 1.33;  
        border-radius: 25px;
        }
        .btn-circle.btn-xl {    
        width: 70px;    
        height: 70px;   
        padding: 10px 16px; 
        font-size: 24px;    
        line-height: 1.33;  
        border-radius: 35px;
        }
        .btn-email{
            color:white;
        }
        @media (min-width:992px) {
            #myModal {
                 width:550px;
                 height:290px;
                 top: calc(20% - 25px);
                left: calc(35% - 50px);
                position:fixed;                
            }      
            #txtEmail{
                width:350px;
                margin-left:100px;
            }      
            #txtPassword{
                 width:350px;
                margin-left:100px
            }
            #txtloginemail{
                color: #0000ff;
                margin-left:70px;
                margin-top:10px;
            }
            #ortt{
                margin-left:75px;
                padding-top:20px;
                color:#555
            }
            .colorfb{
                margin-left:32px;                
            }
            .colorgg{
                margin-left:32px;
                margin-top: 10px;                
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
            }

            .btn-google {
                color: black;
                width:225px;
                background-color: white;
                border: 1px solid #ccc;
            }
            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social:hover {
                color: #eee;
            }
            .btn-social1 {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social1:hover {
                color: black;
            }

            .btn-social1 :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .btn-social :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
        }
        @media (max-width:600px) {
			.social{
				margin-left:20px;
			}
            .colorfb{
                margin-left:7px;                
            }
            .colorgg{
                margin-left:7px;
                margin-top: 10px;                
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
            }

            .btn-google {
                color: black;
                width:225px;
                background-color: white;
                border: 1px solid #ccc;
            }
            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social:hover {
                color: #eee;
            }
            .btn-social1 {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social1:hover {
                color: black;
            }

            .btn-social1 :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .btn-social :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            #txtloginemail{
                margin-left:60px;
                color: #0000ff;
                margin-bottom:-10px;
            }
            #txtEmail{
                width:350px;
                margin-left:10px
            }
            #txtPassword{
                 width:350px;
                margin-left:70px
            }
            #myModal {
                 width:370px;
                 height:290px;
                 top: calc(25% - 25px);
                left: calc(14% - 50px);
                position:fixed;                
            }  
            #ortt{
                margin-left:65px;
                padding-top:20px;
                color:#555
            }
        }
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            .colorfb{
                margin-left:-5px;                
            }
            .colorgg{
                margin-left:-5px;
                margin-top: 10px;                
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
            }

            .btn-google {
                color: black;
                width:225px;
                background-color: white;
                border: 1px solid #ccc;
            }
            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social:hover {
                color: #eee;
            }
            .btn-social1 {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social1:hover {
                color: black;
            }

            .btn-social1 :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .btn-social :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            #ortt{
                margin-left:35px;
                padding-top:20px;
                color:#555
            }
        #myModal {
                 width:550px;
                 height:290px;
                 top: calc(20% - 25px);
                left: calc(25% - 50px);
                position:fixed;                
            } 
        #txtloginemail{
            margin-left:30px;
            color: #0000ff;
            margin-top:10px;
            } 
        #txtEmail{
                width:350px;
                margin-left:100px;
            }      
            #txtPassword{
                 width:350px;
                margin-left:100px
            }
   }

    </style>

    <link href="../Css/Page/DangNhap.css" rel="stylesheet" />
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
    <section class="feature container">
        <div class="row">
            <!--Top Jobs-->
            <div class="col-md-12 col-sm-12">
                <div class="top-job">
                    <div class="panel jobs-board-listing with-mc no-padding no-border">
                        <div class="panel-content" id="tabChoThue" style="padding: 10px;margin-top:88px;">
                            <div class="job-list scrollbar m-t-lg">
                                <div id="vnw-log-in" class="container main-content">
                                    <div class="col-sm-4 col-sm-push-4">
										<h5 class="text-center"><strong>ĐĂNG NHẬP</strong></h5>
                                        <div>
                                            <div class="form-group">
                                                <div >
                                                    <input type="text" id="txtTenDangNhap" name="form[username]" required="required" placeholder="Số điện thoại" tabindex="1" class="form-control" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div>
                                                    <input type="password" id="txtMatKhau" name="form[password]" required="required" placeholder="Mật khẩu" tabindex="2" class="form-control" runat="server" />
                                                </div>
                                            </div>
                                             <!-- Buttons-->                                            
                                            <div class="form-group">
                                                <div>                                                  
                                                    <%--<button type="submit" id="form_sign_in" name="form[sign_in]" tabindex="3" class="btn btn-primary btn-block">Đăng Nhập</button>--%>
                                                    <asp:Button ID="btDangNhap" runat="server" Text="Đăng nhập" OnClick="btDangNhap_Click" class="btn btn-primary btn-block" />
                                                    <%--Email new--%>
                                                   <div class="form-group" style="margin-top:15px;margin-bottom: 10px;">
                                                <a href="/dang-ky/dk">Bạn chưa có tải khoản?</a><br />
                                                <a href="/tai-khoan/quen-mat-khau">Quên mật khẩu?</a><br />
                                            </div>
                                                    <%--End Email new --%> 
                                                    <%--Facebook va Google new--%>                                                                         
                                                    <label style="margin-left:85px;padding-top:20px;color:#555;margin-top: -39px;">Hoặc tiếp tục bằng</label><br />
                                                    <%--Facebook--%>                                                                                                      
                                                    <%--<asp:ImageButton  runat="server" OnClick="btnfacebook_Click" type="button" ImageUrl="~/Images/facebook2.png" style="background-color:#1778f2;box-shadow:0px 0px 8px rgb(0 0 0 / 30%);" class=" btn btn-primary btn-circle btn-lg"></asp:ImageButton>--%>
													<div class="social">
                                                    <div class="container"><a class="btn btn-lg btn-social btn-facebook colorfb" ID="btnfacebook" runat="server" onserverclick="btnfacebook_ServerClick" style="padding-left: 43px;"><i class="fa fa-facebook-square" style="margin-top:-2px;"></i> Đăng nhập bằng Facebook</a></div>
                                                    <asp:Panel ID="pnlFaceBookUser" runat="server" Visible="false">
                                                        <asp:Image ID="ImageFace" runat="server" Width="50" Height="50" />
                                                        <asp:Label ID="lblIdFace" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lblNameFace" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lblEmailFace" runat="server" Text=""></asp:Label>
                                                    </asp:Panel>
                                                    <%--Google--%>                                                    
                                                    <%--<asp:Button ID="btngoogle" runat="server" class="btn btn-primary btn-circle btn-lg" style="background-color: white;box-shadow:0px 0px 8px rgb(0 0 0 / 30%);background-image:url(Images/google.png) no-repeat;display:block; " type="button" OnClick="btngoogle_Click"></asp:Button>--%>                                                 
                                                     <div class="container"><a class="btn btn-lg btn-social1 btn-google colorgg" ID="btngoogle" runat="server" onserverclick="btngoogle_ServerClick" style="padding-left: 29px;"><img src="/Images/google.png" style="width:38px;margin-top:-3px;" /> Đăng nhập bằng Google</a></div> 
                                                    <asp:Panel ID="pnlProfile" runat="server" Visible="false">
                                                    <asp:Image ID="ProfileImage" runat="server" Width="50" Height="50" />
                                                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label> 
                                                    <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="lblVerified" runat="server" Text=""></asp:Label>                                                     
                                                    </asp:Panel>   
                                                    <%--End Facebook va Google new--%>                                                                                                                                                    
                                                </div>
												</div>
                                            </div>

                                           
                                            <%--<div class="form-group">
                                             <div class="col-md-offset-2 col-md-8">
                                             <button type="submit" id="form_sign_in" name="form[sign_in]" tabindex="3" class="btn btn-facebook btn-block">Đăng Nhập Bằng Facebook</button>
                                             </div>
                                         </div>
                                        <div class="form-group">
                                             <div class="col-md-offset-2 col-md-8">
                                             <button type="submit" id="form_sign_in" name="form[sign_in]" tabindex="3" class="btn btn-google btn-block">Đăng Nhập Bằng Google</button>
                                             </div>
                                         </div>--%>
                                        </div>
                                        <div id="dvDKTC" runat="server" style="text-align: center; color: #05c705; font-style: italic;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
