﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

public partial class AccountReferral : System.Web.UI.Page
{
    protected string idThanhVien = "";
    protected Dictionary<string, object> configs;

    protected string templateFilterCode = "";
    protected string filterPostCode = null;

    protected string templateFilterPostOwner = "";
	 protected string templateFilterPostCode = "";
    protected string filterPostOwner = null;
    protected string templateJsonOwners = "";
	protected string templateJsonCode = "";

    protected string templateFilterOwnerApprovedAtFrom = "";
    protected string filterOwnerApprovedAtFromAltDateTime = "";
    protected string filterOwnerApprovedAtFromAltDate = "";

    protected string templateFilterOwnerApprovedAtTo = "";
    protected string filterOwnerApprovedAtToAltDateTime = "";
    protected string filterOwnerApprovedAtToAltDate = "";

    protected string templateJsonReferralStatus = "";
    protected string templateFilterReferralStatus = "";
    protected string filterReferralStatus = null;

    // protected string templateJsonReferralStatusOther2 = "";
    protected string templateFilterReferralStatusOther2 = "";
    protected string filterReferralStatusOther2 = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch
        {
            Response.Redirect("/dang-nhap/dn");
        }
        if (IsPostBack)
        {

        }
        else
        {

        }
        this.configs = Utilities.Config.getConfigs();
        this.generateFilterInputs();
        this.loadThongTinThanhVien();
        this.loadStatusReferralTemplate();
        this.loadOwners();
		 this.loadCode();
        renderAccountReferralTable();
        //checkSoDT();//Code new
    }

    public void checkSoDT()//Code new
    {
        string result = "";
        string Email = txtShopEmailID.InnerHtml;
        string sql = "Select top 1 SoDienThoai from tb_ThanhVien where Email='" + Email + "' and isnull(isKhoa,'False')!='True'";
        try
        {
            DataTable tb = Connect.GetTable(sql);
            if (tb.Rows.Count > 0)
                result = tb.Rows[0][0].ToString();
        }
        catch
        { }
        if (result != "")
        {
            //Response.Redirect("/tai-khoan/chia-se");   
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn tc!')", true);
            string a = "";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn cần cập nhật thông tin số điện thoại!')", true);
            Response.Redirect("/thong-tin/tt");
        }
    }
    protected void loadThongTinThanhVien()
    {
         String sql1 = "";
        sql1 = sql1 + " SELECT ";
        sql1 = sql1 + "   tb_ThanhVien.TenCuaHang AS TenCuaHang, ";
        sql1 = sql1 + "   tb_ThanhVien.Code AS Code, ";
        sql1 = sql1 + "   tb_ThanhVien.Email AS Email, ";
        sql1 = sql1 + "   tb_ThanhVien.SoDienThoai AS SoDienThoai, ";
        sql1 = sql1 + "   tb_ThanhVien.TenDangNhap AS TenDangNhap, ";
        sql1 = sql1 + "   tb_ThanhVien.NgayDangKy AS NgayDangKy, ";
        sql1 = sql1 + "   tb_ThanhVien.linkAnh AS linkAnh, ";
        sql1 = sql1 + "   tb_ThanhVien.idTinh AS idTinh, ";
        sql1 = sql1 + "   tb_ThanhVien.idHuyen AS idHuyen, ";
        sql1 = sql1 + "   tb_ThanhVien.idPhuongXa AS idPhuongXa, ";
        sql1 = sql1 + "   tb_ThanhVien.DiaChi AS DiaChi, ";
        sql1 = sql1 + "   tb_ThanhVien.RefCode AS RefCode, ";
        sql1 = sql1 + "   tb_ThanhVien.MST AS MST, ";
        sql1 = sql1 + "   tb_ThanhVien.NgayCap AS NgayCap, ";
        sql1 = sql1 + "   tb_ThanhVien.NoiCap AS NoiCap, ";
        sql1 = sql1 + "   tb_ThanhVien.StsPro AS StsPro, ";
        sql1 = sql1 + "   tb_ThanhVien.LyDo_KhongDuyet AS LyDo,";
        sql1 = sql1 + "   tb_ThanhVien.ProAt AS ProAt,";
        sql1 = sql1 + "   tb_ThanhVien.isDuyet AS isDuyet, ";
        sql1 = sql1 + "   tb_ThanhVien.idAdmin_Duyet AS AdminDuyet";
        sql1 = sql1 + " FROM tb_ThanhVien ";
        sql1 = sql1 + " WHERE idThanhVien = " + idThanhVien;
        DataTable table = Connect.GetTable(sql1);
        if (table.Rows.Count > 0)
        {
            txtNameShopID.InnerHtml = "<h4>" + (table.Rows[0]["TenCuaHang"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["TenCuaHang"]) + "</h4>";
            txtInfoAccountCode.InnerText = "Mã thành viên: " + table.Rows[0]["Code"].ToString().Trim();
            txtShopEmailID.InnerHtml = (table.Rows[0]["Email"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["Email"].ToString());
            imgLinkAnh.Src = table.Rows[0]["LinkAnh"].ToString().Trim();
			
			 txtLoaiUser.InnerText = "Bình Thường";
          //  string htmlIndenti = "";
           // htmlIndenti += "<a class='MainFunctionButton EditProfile' runat='server' href='/thong-tin/pro'>Chỉnh sửa thông tin</a>";
            if (table.Rows[0]["StsPro"].ToString().Trim() == "1")
            {
              //  UserIdenti.InnerHtml = htmlIndenti;
             //   btnEditInfoUserID.Visible = false;
                txtLoaiUser.InnerText = "Thành Viên PRO";
                buttonUser.Visible = false;
                ImagePro.Style.Add("display", "");
            }
            string htmluser = "";
            if (table.Rows[0]["LyDo"].ToString().Trim() != "")
            {
                htmluser += "<div style='background-color:#fcf8e3;border-radius:10px;padding: 0 11px;'><p>Nội dung từ chối:" + table.Rows[0]["LyDo"].ToString().Trim() + "</p></div>";
                htmluser += "<a class='MainFunctionButton EditProfile' id='button_Forwad_mobi' style='background-color:#4cb050;color:white;margin-bottom:10px;height: 34px;' href='/thong-tin/pro'>Đăng ký lại</a>";
                ProNow.InnerHtml = htmluser;
                buttonUser.Visible = false;
                ImagePro.Visible = false;
            }
            if (table.Rows[0]["StsPro"].ToString().Trim() == "0" && table.Rows[0]["AdminDuyet"].ToString().Trim() == "-1")
            {
                txtLoaiUser.InnerText = "Thành viên Pro đang chờ duyệt";
                ImagePro.Visible = false;
                buttonUser.Visible = false;
            }
            // txtMST.InnerHtml = (table.Rows[0]["MST"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["MST"].ToString());
            // DateTime ngayCap = DateTime.MinValue;
            // if (table.Rows[0]["NgayCap"].ToString().Trim() != "")
            // {
            //     ngayCap = (DateTime)table.Rows[0]["NgayCap"];
            // }
            // string NgayCap = (table.Rows[0]["NgayCap"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayCap.ToString("MM-dd-yyyy")));
            // if (NgayCap == "01/01/1900")
            // {
            //     txtNgayCap.InnerHtml = "Chưa cung cấp";
            // }
            // else
            // {
            //     txtNgayCap.InnerHtml = NgayCap;
            // }
            // txtNoiCap.InnerHtml = (table.Rows[0]["NoiCap"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["NoiCap"].ToString());

            txtShopPhoneID.InnerHtml = (table.Rows[0]["SoDienThoai"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["SoDienThoai"].ToString());
            DateTime ngayDangKy = (DateTime)table.Rows[0]["NgayDangKy"];
            txtDateShopApprovedID.InnerHtml = (table.Rows[0]["NgayDangKy"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayDangKy.ToString("MM-dd-yyyy")));
            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim();
            string TenTinhThanh = StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString());
            string TenQuanHuyen = StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString());
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString());
            string DiaChi = table.Rows[0]["DiaChi"].ToString().Trim();
            if (DiaChi == "" && TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                txtShopAddressID.InnerHtml = "Chưa cung cấp";
            else
            {
                txtShopAddressID.InnerHtml = DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh;
                txtShopAddressID.Attributes.Add("title", DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh);
            }

            string idThanhVien_Cookie = null;
            try
            {
                idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
            }
            catch { }
            if (idThanhVien != idThanhVien_Cookie)
            {
              //  UserIdenti.Visible = false;
             //   btnEditInfoUserID.Visible = false;  
                buttonUser.Visible = false;
                ProNow.Visible = false;
            }
        }
    }
    protected void renderAccountReferralTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = new Dictionary<string, string>();
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();

        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 = sql1 + @"FROM 
                            ( 
                                SELECT 
                                    ROW_NUMBER() OVER (
                                        ORDER BY Referrals.UpdatedAt DESC 
                          ) AS RowNumber, 
                          ISNULL(TBOwners.Code, '') AS OwnerCode, 
                          ISNULL(TBOwners.TenCuaHang, '') AS OwnerFullname, 
                          ISNULL(TBOwners.SoDienThoai, '') AS OwnerPhone, 
                          ISNULL(TBOwners.LinkAnh, '') AS OwnerThumbnail, 
                          ISNULL(TBOwners.Email, '') AS OwnerEmail, 
                          ISNULL(TBOwners.NgayDangKy, '') AS OwnerRegisterAt, 
                          ISNULL(TBOwners.DiaChi, '') AS OwnerAddress, 
                          ISNULL(TBOwnerCities.Ten, '') AS OwnerCity, 
                          ISNULL(TBOwnerDistricts.Ten, '') AS OwnerDistrict, 
                          ISNULL(TBOwnerWards.Ten, '') AS OwnerWard, 
                          Referrals.Id AS ReferralId, 
                          Referrals.TinDangId AS TinDangId, 
                          Referrals.CollabId AS ReferralCollabId, 
                          Referrals.OwnerId AS ReferralOwnerId, 
                          TBPosts.Code AS PostCode,
						  TBPosts.idTinDang AS PostId,
                          TBPosts.LinkAnh AS PostThumb, 
                          TBPosts.TieuDe AS PostTitle, 
                          TBPosts.DuongDan AS PostSlug, 
                          TBPosts.isHetHan AS PostIsHetHan, 
                          TBPosts.isDraft AS PostIsDraft, 
                          TBPosts.isDuyet AS PostIsDuyet, 
						  ISNULL(TBPosts.TuGia, 0) AS PostPrice, 
						  TBPosts.NgayDang AS PostCreatedAt,
						  ISNULL(TBCategories1.TenDanhMucCap1, '') AS PostCategory1,
						  ISNULL(TBCategories2.TenDanhMucCap2, '') AS PostCategory2,
                          ISNULL(Referrals.Rate, 0) AS ReferralRate, 
                          ISNULL(Referrals.Amount, 0) AS ReferralAmount, 
                          ISNULL(Referrals.Commission, 0) AS ReferralCommission, 
                          Referrals.Status AS ReferralStatus, 
                          ISNULL(Referrals.StsOwnerApproved, 0) AS ReferralStsOwnerApproved,
                          ISNULL(Referrals.StsOwnerCanceled, 0) AS ReferralStsOwnerCanceled,
                          ISNULL(Referrals.StsCollabApproved, 0) AS ReferralStsCollabApproved,
                          ISNULL(Referrals.StsCollabDenied, 0) AS ReferralStsCollabDenied,
                          ISNULL(Referrals.StsAdminApproved, 0) AS ReferralStsAdminApproved,
                          Referrals.OwnerApprovedAt AS ReferralOwnerApprovedAt, 
                          Referrals.OwnerCanceledAt AS ReferralOwnerCanceledAt,
                          Referrals.CollabApprovedAt AS ReferralCollabApprovedAt, 
                          Referrals.CollabDeniedAt AS ReferralCollabDeniedAt, 
                          Referrals.AdminApprovedAt AS ReferralAdminApprovedAt 
                  FROM Referrals 
                          INNER JOIN tb_ThanhVien TBCollabs
                                  ON Referrals.CollabId = TBCollabs.idThanhVien 
                          INNER JOIN tb_ThanhVien TBOwners 
                                  ON Referrals.OwnerId = TBOwners.idThanhVien 
                          INNER JOIN tb_TinDang TBPosts
                                  ON Referrals.TinDangId = TBPosts.idTinDang 
                          LEFT JOIN City TBOwnerCities 
                                  ON TBOwners.idTinh = TBOwnerCities.id 
                          LEFT JOIN District TBOwnerDistricts 
                                  ON TBOwners.idHuyen = TBOwnerDistricts.id 
                          LEFT JOIN tb_PhuongXa TBOwnerWards 
                                  ON TBOwners.idPhuongXa = TBOwnerWards.id 
                          LEFT JOIN tb_DanhMucCap1 TBCategories1
                                  ON TBPosts.idDanhMucCap1 = TBCategories1.idDanhMucCap1
                          LEFT JOIN tb_DanhMucCap2 TBCategories2
                                  ON TBPosts.idDanhMucCap2 = TBCategories2.idDanhMucCap2
                  WHERE 1 = 1 
                    AND Referrals.CollabId = '" + idThanhVien + @"' ";
        if (!string.IsNullOrWhiteSpace(filterPostCode))
        {
            sql1 += "       AND TBPosts.Code LIKE '%" + filterPostCode + "%' ";
            paginationParams.Add("FilterPostCode", filterPostCode);
        }
        if (!string.IsNullOrWhiteSpace(filterPostOwner))
        {
            sql1 += "       AND TBPosts.idThanhVien = " + filterPostOwner + " ";
            paginationParams.Add("FilterPostOwner", filterPostOwner);
        }
        if (!string.IsNullOrWhiteSpace(filterReferralStatus) || !string.IsNullOrWhiteSpace(filterReferralStatusOther2))
        {
            bool tmpHasFilterStatus1 = !string.IsNullOrWhiteSpace(filterReferralStatus);
            bool tmpHasFilterStatus2 = !string.IsNullOrWhiteSpace(filterReferralStatusOther2);
            sql1 += " AND ( ";

            if (tmpHasFilterStatus1)
            {
                sql1 += " ( " + this.getConditionByStatus(filterReferralStatus, "Referrals") + " ) ";
                paginationParams.Add("FilterReferralStatus", filterReferralStatus);
            }
            if (tmpHasFilterStatus2)
            {
                if (tmpHasFilterStatus1)
                {
                    sql1 += "  OR ";
                }
                sql1 += " ( " + this.getConditionByStatus(filterReferralStatusOther2, "Referrals") + " ) ";
                paginationParams.Add("FilterReferralStatusOther2", filterReferralStatusOther2);
            }

            sql1 += " ) ";
        }
        if (!string.IsNullOrWhiteSpace(filterOwnerApprovedAtFromAltDateTime))
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
            }

            //loc 1
            if (!string.IsNullOrWhiteSpace(filterReferralStatusOther2))
            {

                if (filterReferralStatusOther2 == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                }
                else if (filterReferralStatusOther2 == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                }
            }
            //loc 2
            else 
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
            }
        }
        if (!string.IsNullOrWhiteSpace(filterOwnerApprovedAtToAltDateTime))
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
            }
            //loc 1

            if (!string.IsNullOrWhiteSpace(filterReferralStatusOther2))
                { 
                if (filterReferralStatusOther2 == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                }
                else if (filterReferralStatusOther2 == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                }
            }
            //loc2
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
            }
        }
        sql1 = sql1 + "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());

        DataTable table3 = Connect.GetTable(sql3);
        html += "<table id='referralTable' class='table table-bordered app-table dataTable no-footer text-sm'>";
        html += "<thead>";
        html += "<tr>";
        html += "<th>Mã bài đăng</th>";
        html += "<th>Hình ảnh</th>";
        html += "<th>Tiêu đề</th>";
        html += "<th>Người trích hoa hồng</th>";
        html += "<th>Tiền hoa hồng</th>";
        html += "<th>Trạng thái</th>";
        html += "<th>Duyệt lúc</th>";
        html += "<th>Hành động</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        if (table3.Rows.Count == 0)
        {
            html += "<tr><td colspan='8' style='text-align: center;'>Không có kết quả</td></tr>";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
			 string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(table3.Rows[i]["PostId"].ToString());
            string tinDangStatus = Models.TinDang.getStatus(table3.Rows[i]["PostIsHetHan"].ToString(), table3.Rows[i]["PostIsDraft"].ToString(), table3.Rows[i]["PostIsDuyet"].ToString());
            string postTitle = table3.Rows[i]["PostTitle"].ToString();
            string postUrl = Utilities.Post.getUrl("", table3.Rows[i]["PostSlug"].ToString());
			 string postLinkDraf = "/preview/pv?MaTinDang=" + table3.Rows[i]["PostId"].ToString();
            string postCode = table3.Rows[i]["PostCode"].ToString();
            string postThumbnail = Utilities.Post.getThumb(table3.Rows[i]["PostThumb"].ToString());
            string postCategory = table3.Rows[i]["PostCategory1"].ToString() + " - " + table3.Rows[i]["PostCategory2"].ToString();
            string postCreatedAt = (string.IsNullOrWhiteSpace(table3.Rows[i]["PostCreatedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["PostCreatedAt"]).ToString("dd/MM/yyyy");
            string postPriceFormat = Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString());
            string ownerFullname = table3.Rows[i]["OwnerFullName"].ToString();
            string ownerCode = table3.Rows[i]["OwnerCode"].ToString();
            string ownerPhone = table3.Rows[i]["OwnerPhone"].ToString();
            string ownerEmail = table3.Rows[i]["OwnerEmail"].ToString();
            string referralId = table3.Rows[i]["ReferralId"].ToString();
            string referralCommission = table3.Rows[i]["ReferralCommission"].ToString();
            string referralCommissionFormat = Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralCommission"].ToString());
            string referralStsOwnerApproved = table3.Rows[i]["ReferralStsOwnerApproved"].ToString();
            string referralStsOwnerCanceled = table3.Rows[i]["ReferralStsOwnerCanceled"].ToString();
            string referralStsCollabApproved = table3.Rows[i]["ReferralStsCollabApproved"].ToString();
            string referralStsCollabDenied = table3.Rows[i]["ReferralStsCollabDenied"].ToString();
            string referralStsAdminApproved = table3.Rows[i]["ReferralStsAdminApproved"].ToString();
            string referralOwnerApprovedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerApprovedAt"]).ToString("dd/MM/yyyy");
            string referralOwnerApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralOwnerCanceledAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerCanceledAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerCanceledAt"]).ToString("dd/MM/yyyy");
            string referralOwnerCanceledAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerCanceledAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerCanceledAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabApprovedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabApprovedAt"]).ToString("dd/MM/yyyy");
            string referralCollabApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabDeniedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabDeniedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabDeniedAt"]).ToString("dd/MM/yyyy");
            string referralCollabDeniedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabDeniedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabDeniedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralAdminApprovedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralAdminApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralAdminApprovedAt"]).ToString("dd/MM/yyyy");
            string referralAdminApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralAdminApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralAdminApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");

            string ownerInfoText = "";
            ownerInfoText += " data-thumbnail='" + Utilities.Account.getLinkThumbWithPath(table3.Rows[i]["OwnerThumbnail"].ToString()) + "' ";
            ownerInfoText += " data-fullname='" + ownerFullname + "' ";
            ownerInfoText += " data-code='" + ownerCode + "' ";
            ownerInfoText += " data-phone='" + ownerPhone + "' ";
            ownerInfoText += " data-email='" + ownerEmail + "' ";
            ownerInfoText += " data-addr='" + table3.Rows[i]["OwnerAddress"].ToString() + ", " + table3.Rows[i]["OwnerWard"].ToString() + ", " + table3.Rows[i]["OwnerDistrict"].ToString() + ", " + table3.Rows[i]["OwnerCity"].ToString() + "' ";
            ownerInfoText += " data-register-at='" + ((DateTime)table3.Rows[i]["OwnerRegisterAt"]).ToString("dd-MM-yyyy HH:mm:ss") + "' ";

            string approveReferralText = "";
            approveReferralText += " data-post-title='" + postTitle + "' ";
            approveReferralText += " data-post-url='" + postUrl + "' ";
            approveReferralText += " data-post-code='" + postCode + "' ";
            approveReferralText += " data-post-created-at='" + postCreatedAt + "' ";
            approveReferralText += " data-post-price-format='" + postPriceFormat + "' ";
            approveReferralText += " data-owner-fullname='" + ownerFullname + "' ";
            approveReferralText += " data-owner-code='" + ownerCode + "' ";
            approveReferralText += " data-owner-phone='" + ownerPhone + "' ";
            approveReferralText += " data-owner-email='" + ownerEmail + "' ";
            approveReferralText += " data-referral-id='" + referralId + "' ";
            approveReferralText += " data-referral-commission='" + referralCommissionFormat + "' ";
            approveReferralText += " data-referral-owner-approved-at='" + referralOwnerApprovedAtFormatDatetime + "' ";
            approveReferralText += " data-referral-collab-approved-at='" + referralCollabApprovedAtFormatDatetime + "' ";
            approveReferralText += " data-referral-admin-approved-at='" + referralAdminApprovedAtFormatDatetime + "' ";
			
            html += @"<tr>";

           if (table3.Rows[i]["PostIsHetHan"].ToString() == "True" || table3.Rows[i]["PostIsDuyet"].ToString() != "True")
            {
                // code
                html += @"<td>";
                html += "<a target='_blank' href='" + postLinkDraf + "' class='color-yellow'>" + postCode + "</a>";
                html += @"</td>";
                // image
                html += @"<td class='datacol-share'>";
                html += "<p><img class='small-thumb' style='width: 100%;height: 130px;margin-left: 15px;margin-top: 4px;' src='" + urlHinhAnh + "' /></p>";
                html += @"</td>";
                // title
                html += @"<td class='datacol-share' style='text-align: center;'>";
                html += @"  <a target='_blank' href='" + postLinkDraf + "'>" + postTitle + "</a>";
                html += @"</td>";
            }
            else
            {
                // code
                html += @"<td>";
                html += "<a target='_blank' href='" + postUrl + "' class='color-yellow'>" + postCode + "</a>";
                html += @"</td>";
                // image
                html += @"<td class='datacol-share'>";
                html += "<p><img class='small-thumb' style='width: 100%;height: 130px;margin-left: 15px;margin-top: 4px;' src='" + urlHinhAnh + "' /></p>";
                html += @"</td>";
                // title
                html += @"<td class='datacol-share' style='text-align: center;'>";
                html += @"  <a target='_blank' href='" + postUrl + "'>" + postTitle + "</a>";
                html += @"</td>";
            }
            // owner info
            html += "<td class='datacol-sharess'>";
            html += "<span style='text-align: center;'>Mã TV: </span><a href='javascript:void(0);' class='color-yellow user-info-btn' data-account-id='" + table3.Rows[i]["ReferralOwnerId"].ToString() + "' " + ownerInfoText + ">" + ownerCode + "</a></br>";
            html += "<span style='text-align: center;'>Tên:</span><a href='javascript:void(0);' class='color-yellow user-info-btn' data-account-id='" + table3.Rows[i]["ReferralOwnerId"].ToString() + "' " + ownerInfoText + ">" + ownerFullname + "</a></br>";
            html += "<span style='text-align: center;'>SĐT:</span><a href='tel:" + ownerPhone + "' class='color-yellow'>" + ownerPhone + "</a>";
            html += "</td>";
            // commission
            html += @"  <td style='text-align: center;color:red;'>" + referralCommissionFormat + "</td>";
            // referral status
            html += "<td>";
            if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "1")
            {
                html += @"  <label class='label label-danger text-label'>Người đăng đã hủy trích hoa hồng</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"  <label class='label label-primary text-label'>Người đăng đã xác nhận</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"  <label class='label label-info text-label'>Bạn đã xác nhận</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "1" && referralStsAdminApproved == "0")
            {
                html += @"  <label class='label label-danger text-label'>Bạn đã từ chối</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "1")
            {
                html += @"<label class='label label-success text-label'>Tung Tăng đã xác nhận</label>";
            }
            html += "</td>";
            // approve at
            html += @"  <td class='datacol-share'>";
            if (!string.IsNullOrWhiteSpace(referralOwnerApprovedAtFormatDatetime))
            {
                html += "<p>Người đăng đã duyệt: " + referralOwnerApprovedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabApprovedAtFormatDatetime))
            {
                html += "<p>Bạn đã duyệt: " + referralCollabApprovedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralAdminApprovedAtFormatDatetime))
            {
                html += "<p>Tung Tăng đã duyệt: " + referralAdminApprovedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabDeniedAtFormatDatetime))
            {
                html += "<p>Bạn đã từ chối: " + referralCollabDeniedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralOwnerCanceledAtFormatDatetime))
            {
                html += "<p>Người đăng đã hủy trích hoa hồng: " + referralOwnerCanceledAtFormatDatetime + "</p>";
            }
            html += "</td>";
            // action
            html += @"<td>";
            if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "1")
            {

            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-primary btn-sm btn-approve-referral mt-2' " + approveReferralText + " >Xác nhận hoa hồng</a>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-info btn-sm btn-approve-referral mt-2' " + approveReferralText + " data-readonly='1' >Thông tin hoa hồng</a>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "1" && referralStsAdminApproved == "0")
            {

            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "1")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-success btn-sm btn-approve-referral mt-2' " + approveReferralText + " data-readonly='1' >Thông tin hoa hồng</a>";
            }
            html += "</td>";
            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";

        dvAccountReferralTable.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "/tai-khoan/cong-tac", paginationParams);
        divAccountReferralPagination1.InnerHtml = htmlPagination;
        divAccountReferralPagination2.InnerHtml = htmlPagination;
    }
    protected void PFake_ApproveReferral_Submit_Click(object sender, EventArgs e)
    {
        string reqReferralId = PFake_ApproveReferral_ReferralId.Text.Trim();
        if (reqReferralId == null)
        {
            return;
        }
        Utilities.Referral.collabApproveReferral(reqReferralId, idThanhVien);
        this.renderAccountReferralTable();
        PFake_ApproveReferral_ReferralId.Text = "";
    }
    protected void PFake_DenyReferral_Submit_Click(object sender, EventArgs e)
    {
        string referralId = PFake_DenyReferral_ReferralId.Text.Trim();
        if (
            !string.IsNullOrWhiteSpace(referralId) &&
            !string.IsNullOrWhiteSpace(idThanhVien)
        )
        {
            Utilities.Referral.collabDenyReferral(referralId, idThanhVien);
        }
        Response.Redirect("/tai-khoan/cong-tac");
    }
    protected void generateFilterInputs()
    {
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostCode")))
        {
            string filterPostCode = Request.QueryString.Get("FilterPostCode").Trim();
            this.filterPostCode = filterPostCode;
            this.templateFilterPostCode = filterPostCode;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostOwner")))
        {
            string filterPostOwner = Request.QueryString.Get("FilterPostOwner").Trim();
            this.filterPostOwner = filterPostOwner;
            this.templateFilterPostOwner = filterPostOwner;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterReferralStatus")))
        {
            string filterReferralStatus = Request.QueryString.Get("FilterReferralStatus").Trim();
            this.filterReferralStatus = filterReferralStatus;
            this.templateFilterReferralStatus = filterReferralStatus;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterReferralStatusOther2")))
        {
            string filterReferralStatusOther2 = Request.QueryString.Get("FilterReferralStatusOther2").Trim();
            this.filterReferralStatusOther2 = filterReferralStatusOther2;
            this.templateFilterReferralStatusOther2 = filterReferralStatusOther2;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterOwnerApprovedAtFrom")))
        {
            DateTime filterOwnerApprovedAtFrom_dateTime;
            string filterOwnerApprovedAtFrom = Request.QueryString.Get("FilterOwnerApprovedAtFrom").Trim();
            if (DateTime.TryParseExact(filterOwnerApprovedAtFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterOwnerApprovedAtFrom_dateTime))
            {
                this.filterOwnerApprovedAtFromAltDate = filterOwnerApprovedAtFrom;
                this.filterOwnerApprovedAtFromAltDateTime = filterOwnerApprovedAtFrom + " 00:00:00:000";
                this.templateFilterOwnerApprovedAtFrom = filterOwnerApprovedAtFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterOwnerApprovedAtTo")))
        {
            DateTime filterOwnerApprovedAtTo_dateTime;
            string filterOwnerApprovedAtTo = Request.QueryString.Get("FilterOwnerApprovedAtTo").Trim();
            if (DateTime.TryParseExact(filterOwnerApprovedAtTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterOwnerApprovedAtTo_dateTime))
            {
                this.filterOwnerApprovedAtToAltDate = filterOwnerApprovedAtTo;
                this.filterOwnerApprovedAtToAltDateTime = filterOwnerApprovedAtTo + " 23:23:23:999";
                this.templateFilterOwnerApprovedAtTo = filterOwnerApprovedAtTo;
            }
        }
    }
     protected void loadStatusReferralTemplate()
    {
        this.templateJsonReferralStatus = "[";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_ALL + @"', text: 'Tất cả'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_OWNER_APPROVED + @"', text: 'Người đăng bài đã trích hoa hồng'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_COLLAB_APPROVED + @"', text: 'Bạn đã xác nhận'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_ADMIN_APPROVED + @"', text: 'Tung Tăng đã xác nhận'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_FEE_COMMISSION + @"', text: 'Đã thanh toán'},";
        this.templateJsonReferralStatus += "]";
    }
    protected string getConditionByStatus(string filterReferralStatus, string tableName)
    {
        string s = " ";
        if (filterReferralStatus == Models.Referral.STATUS_ALL)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 0";
            s += @" OR ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" OR ISNULL(" + tableName + ".StsCollabApproved, 0) = 0 ";
            s += @" OR ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            s += @" OR ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_OWNER_APPROVED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
           // s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
           // s += @" OR ( ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 )";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_COLLAB_APPROVED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            //s += @" OR ( ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 )";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_ADMIN_APPROVED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsAdminApproved, 0) = 1 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_FEE_COMMISSION)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
        }
        return s;
    }
    protected void loadOwners()
    {
        string sql1 = @"
          SELECT                                      
						  DISTINCT ISNULL(TBOwners.idThanhVien, '') AS OwnerID,  
                          ISNULL(TBOwners.Code, '') AS OwnerCode, 
                          ISNULL(TBOwners.TenCuaHang, '') AS OwnerFullname, 
                          ISNULL(TBOwners.SoDienThoai, '') AS OwnerPhone                                                                            
                  FROM Referrals 
                          INNER JOIN tb_ThanhVien TBCollabs
                                  ON Referrals.CollabId = TBCollabs.idThanhVien 
                          INNER JOIN tb_ThanhVien TBOwners 
                                  ON Referrals.OwnerId = TBOwners.idThanhVien 
                          INNER JOIN tb_TinDang TBPosts
                                  ON Referrals.TinDangId = TBPosts.idTinDang                       
                  WHERE 1 = 1 
                    AND Referrals.CollabId = '" + idThanhVien + @"'       
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonOwners = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonOwners += @"{id:'" + table1.Rows[i]["OwnerId"].ToString() + @"', code:'" + table1.Rows[i]["OwnerCode"].ToString() + @"', phone:'" + table1.Rows[i]["OwnerPhone"].ToString() + @"', fullname: '" + table1.Rows[i]["OwnerFullname"].ToString() + @"'},";
        }
        this.templateJsonOwners += "]";
    }
    protected void loadCode()
    {
        string sql1 = @"
            SELECT                                      
						  ISNULL(TBPosts.idTinDang, '') AS PostID,  
                          ISNULL(TBPosts.Code, '') AS PostCode                           						                                                                        
                  FROM Referrals 
                          INNER JOIN tb_ThanhVien TBCollabs
                                  ON Referrals.CollabId = TBCollabs.idThanhVien 
                          INNER JOIN tb_ThanhVien TBOwners 
                                  ON Referrals.OwnerId = TBOwners.idThanhVien 
                          INNER JOIN tb_TinDang TBPosts
                                  ON Referrals.TinDangId = TBPosts.idTinDang                       
                  WHERE 1 = 1 
                    AND Referrals.CollabId = '" + idThanhVien + @"'
					ORDER BY TBPosts.NgayDang DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonCode = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonCode += @"{id:'" + table1.Rows[i]["PostId"].ToString() + @"', code:'" + table1.Rows[i]["PostCode"].ToString() + @"'},";
        }
        this.templateJsonCode += "]";
    }
}