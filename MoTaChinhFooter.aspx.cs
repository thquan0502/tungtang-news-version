﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MoTaChinhFooter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            LoadMoTa();
        }
    }
    public void LoadMoTa()
    {
        string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
        if(url.Contains("co-che-giai-quyet-tranh-chap"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "COCHEGIAIQUYETTRANHCHAP");
        }
        if (url.Contains("dieu-khoan-va-dich-vu"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "DIEUKHOANDICHVU");
        }
        if (url.Contains("chinh-sach-bao-mat-thong-tin"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "CHINHSACHBAOMATTHONGTIN");
        }
        if (url.Contains("gioi-thieu"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "GIOITHIEU");
        }
        if (url.Contains("huong-dan"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "HUONGDAN");
        }
        if (url.Contains("quy-che-hoat-dong"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "QUYCHEHOATDONG");
        }
        if (url.Contains("chinh-sach-mua-hang"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "CHINHSACHDATHANG");
        }
        if (url.Contains("chinh-sach-thanh-toan"))
        {
            ShowNoiDung.InnerHtml = StaticData.getField("[tb_footer]", "[Discription]", "[Mafooter]", "CHINHSACHTHANHTOAN");
        }
    }
}