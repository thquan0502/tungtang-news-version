﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
public partial class News_Copy : System.Web.UI.Page
{
    string Domain = "";
    string htmlInnerHtmlTitle = "";
    string idtags = "";
    string nametags = "";
    string idDanhMucCap1 = "";
    string idDanhMucCap2 = "";
    string ChuoiLoai = "";
    string idTinh = "";
    string idHuyen = "";
    string idPhuongXa = "";
    string TenCuaHang = "";
    string BoLocThem = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Pages = 1;
    int MaxPage = 0;
    int PageSize = 20;

    string DuongDan = "";
    //  string titlte = "";

    string metaTitle = "";
    string metaUrl = "";
    string metaHinhAnh = "";
    string metaDescription = "";
    string metaType = "";
    string metaTwtitle = "";
    string metaTwdes = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        HtmlLink canonical = new HtmlLink();
        canonical.Href = HttpContext.Current.Request.Url.OriginalString;
        canonical.Attributes["rel"] = "canonical";
        Page.Header.Controls.Add(canonical);

        Page.Header.Controls.Add(canonical); HtmlLink alternate = new HtmlLink();
        alternate.Href = HttpContext.Current.Request.Url.OriginalString;
        alternate.Attributes["rel"] = "alternate";
        Page.Header.Controls.Add(alternate);

        if (!IsPostBack)
        {
            try
            {
                HttpContext context = HttpContext.Current;
                nametags = context.Items["Action"].ToString();


            }
            catch { }
            try
            {
                Pages = int.Parse(Request.QueryString["P"].ToString());
                if (Pages <= 0)
                {
                    Response.Redirect(Domain + "/tag/tu-khoa");
                }
            }
            catch
            {
                Pages = 1;
            }
           
                foreach (HtmlMeta metaTag in Page.Header.Controls.OfType<HtmlMeta>())
                {
                    if (metaTag.Name.Equals("ogtitle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTitle;
                    }
                    if (metaTag.Name.Equals("ogurl", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaUrl;
                    }
                    if (metaTag.Name.Equals("ogimage", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaHinhAnh;
                    }
                    if (metaTag.Name.Equals("ogdes", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaDescription;
                    }
                    if (metaTag.Name.Equals("ogtype", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaType;
                    }
                    if (metaTag.Name.Equals("ogtwtitle", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwtitle;
                    }
                    if (metaTag.Name.Equals("ogtwdes", StringComparison.CurrentCultureIgnoreCase))
                    {
                        metaTag.Content = metaTwdes;
                    }


                
            }
            //string sUrl = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "");
            //string[] arrUrl = sUrl.Split('/');
            //try
            //{
            //    if (arrUrl[2] != null && arrUrl[2].Trim() != "")
            //    {
            //        string[] arrTinDang = arrUrl[2].Trim().Split('-');
            //        if (arrTinDang[0] == "1")
            //            idtags = arrTinDang[1];
            //        if (arrTinDang[0] == "2")
            //        {
            //            idtags = StaticData.getField("idTinTuc", "tb_TinTuc", "id_tags", arrTinDang[1]);
            //            idtags = (arrTinDang[1]);
            //        }
            //if (idtags != "")
            //    Response.Redirect("https://google.com");
            //    ddltags.Value = idtags;
            //if (idDanhMucCap1 != "")
            //{
            //    txtLoaiDanhMuc.Value = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1);
            //}
            //    }
            //}
            //catch { }
            try
            {
                idtags = Request.QueryString["ddltags"].ToString().Trim();
                ddltags.Value = idtags;
            }
            catch { }
            try
            {
                nametags = Request.QueryString["tu-khoa"].ToString().Trim();
            }
            catch { }
            string sTrangChu = "";
            string sTenDanhMucCap1 = "";
            // Title = idtags.Trim();
            sTrangChu = "<a href='" + Domain + "'>Trang chủ</a>";
            sTenDanhMucCap1 = StaticData.getField("Tags", "Title", "id_tags", idtags);

            string[] Url = HttpContext.Current.Request.Url.AbsoluteUri.Replace("://", "").Replace(":\\", "").Split(new[] { "/tag" }, StringSplitOptions.None);
            ChuoiLoai = LocChuoiLoai(Url[1]);
            string chuoi_Breadscrum = "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/'><span>Trang chủ</span></a></li>";
            chuoi_Breadscrum += "<li class='breadcrumb-item Z9Bevb8OoGSgkMEl67zPu'><a href='/blogs/tag'><span>Tags</span></a></li>";

            LoadTinDang();
        }
    }

    public string boSpace(string str)
    {
        if (str.Replace(" ", "") == "")
            str = "NƠI TÌM KIẾM CƠ HỘI ĐẦU TƯ TỐT NHẤT";

        return str;
    }
    #region paging
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Pages == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Pages == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Pages <= MaxPage)
                {
                    for (int i = Pages; i <= MaxPage; i++)
                    {
                        if (i == Pages)
                        {
                            txtPage1 = (Pages - 2).ToString();
                            txtPage2 = (Pages - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Pages)
                                txtPage3 = i.ToString();
                            if (i == (Pages + 1))
                                txtPage4 = i.ToString();
                            if (i == (Pages + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string SapXepTheo = " TD.idTinTuc desc ";
        //  string GiaTu = "", GiaDen = "";
        // string CanXem = "";


        //Load Tags

        DataTable tableCap2 = Connect.GetTable("select * from Tags ");
        if (tableCap2.Rows.Count > 0)
        {
          
            string html_danhMucCap2 = @"";
            for (int i = 0; i < tableCap2.Rows.Count; i++)
            {
                //  string Tagsname = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tableCap2.Rows[i]["Title"].ToString()));
                string urlTags = Domain + "/tag/" + tableCap2.Rows[i]["DuongDan"].ToString();
                // === begin  ===


                //  cũ

                //html_danhMucCap2 += @"<div>
                //                            <a href='javascript:void(0)' style='color: inherit;'  onclick='ChonDanhMucCap2(" + tableCap2.Rows[i]["id_tags"] + @")'>
                //                                <li class='sc-gzVnrw dUodeu' width='80'>
                //                                    <div class='sc-htoDjs jDGBYk' style='width: 74px; height: 31px;border-radius: 10%;font-size: 15px; background:none;'>

                //                                    <div itemprop='name' class='sc-dnqmqq kCQBIM ur' style='font-size:14px;'>
                //                                          <i class='fa fa-tags' aria-hidden='true'></i>
                //                                            <span class='sc-iwsKbI jiPRqw'>" + tableCap2.Rows[i]["Title"] + @"</span>
                //                                    </div>
                //                                </li>
                //                            </a>
                //                        </div> ";

                // mới

                html_danhMucCap2 += @"<div>
                                            <a href='" + urlTags + @"' style='color: inherit;' >
                                                <li class='sc-gzVnrw dUodeu' width='80'>
                                                    <div class='sc-htoDjs jDGBYk' style='width: 74px; height: 31px;border-radius: 10%;font-size: 15px; background:none;'>

                                                    <div itemprop='name' class='sc-dnqmqq kCQBIM ur' style='font-size:14px;'>
                                                          <i class='fa fa-tags' aria-hidden='true'></i>
                                                            <span class='sc-iwsKbI jiPRqw'>" + tableCap2.Rows[i]["Title"] + @"</span>
                                                    </div>
                                                </li>
                                            </a>
                                        </div> ";

                // === end ===
            }
            dvTags.InnerHtml = html_danhMucCap2;
            dvTags.Attributes.Add("style", "border: none; margin: 0px 0px 1px;display:-webkit-box;");
        }

        //  string idtags = tableCap2.Rows[0]["id_tags"];
        string sql = "";

        //cũ
        //sql += @"select * from
        //    (
        //     SELECT ROW_NUMBER() OVER
        //          ( order by " + SapXepTheo + @"  )AS RowNumber
        //       ,TD.*
        //          from tb_TinTuc TD 
        //    where  KichHoat='1' ";
        //if (idtags != "")
        //{
        //    sql += " and TD.id_tags='" + idtags + "'";
        //}
        // mới
        DataTable tableTags = Connect.GetTable("select tb_tags.* from [Tags] as tb_tags where tb_tags.DuongDan = N'" + nametags + "'");
        //Response.Redirect("select * from Tags where Title = '" + nametags + "'");
        if (tableTags.Rows.Count > 0)
        {
            for (int i = 0; i < tableTags.Rows.Count; i++)
            {
                metaDescription = tableTags.Rows[i]["Desciption"].ToString();
                metaTitle = tableTags.Rows[i]["Title"].ToString();
                metaUrl = HttpContext.Current.Request.Url.OriginalString;
                metaTwdes = tableTags.Rows[i]["Desciption"].ToString();
                metaTwtitle = tableTags.Rows[i]["Title"].ToString();
                metaType = tableTags.Rows[i]["Title"].ToString();
                Title = tableTags.Rows[i]["Title"].ToString();
                MetaDescription = tableTags.Rows[i]["Desciption"].ToString();
            }
			
			string metaU = HttpContext.Current.Request.Url.OriginalString;
            string name = "name";
            string mainEntityOfPage = "mainEntityOfPage";
            string idd = "id";
            string context = "@context";
            string type = "@type";
            string iddd = "@id";
            string hl = "headline";
            string d = "description";
            string ig = "image";
            string ul = "url";
            string con = "@context";
            string t = "@type";
            string dp = "datePublished";
            string dm = "dateModified";
            string au = "author";
            string na = "name";
            string c = "@context";
            string tp = "@type";
            string pb = "publisher";
            string lg = "logo";
            string width = "width";
            string he = "height";
            string hea = "application/ld+json";
            string urlchu = "https://tungtang.com.vn";
            string paa = "WebPage";
            string imgo = "ImageObject";
            string per = "Person";
            string og = "Organization";
            string news = "NewsArticle";
            string chema = "https://schema.org";
            string imoc = "https://tungtang.com.vn/images/icons/logo.png";
            string Crea = "CreativeWorkSeries";
            Page.Header.Controls.AddAt(1,
                       new LiteralControl(
                           "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                       )
                       );
			
                //Response.Redirect("http://google.com");
                sql += @"select * from
            (
             SELECT ROW_NUMBER() OVER
                  ( order by " + SapXepTheo + @"  )AS RowNumber
               ,TD.*
                  from tb_TinTuc TD 
            where  KichHoat='1' and id_tags=" + tableTags.Rows[0]["id_tags"];
        }
        else
        {
            sql += @"select * from
            (
             SELECT ROW_NUMBER() OVER
                  ( order by " + SapXepTheo + @"  )AS RowNumber
               ,TD.*
                  from tb_TinTuc TD 
            where  KichHoat='1' ";
        }

        // === end mới ===

        sql += ") as tb1 ";
        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += "WHERE RowNumber BETWEEN (" + Pages + " - 1) * " + PageSize + " + 1 AND (((" + Pages + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";

        DataTable table = Connect.GetTable(sql);



        // metaUrl = HttpContext.Current.Request.Url.OriginalString;
        string html = "";
        if (table.Rows.Count > 0)
        {


            html += "<ul>";
            SetPage(AllRowNumber);
            for (int i = 0; i < table.Rows.Count; i++)
            {
               

                //string sqlUpdateTD = "update tb_TinTuc set DuongDan=N'" + StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim())) + "'";
                //sqlUpdateTD += " where idTinTuc='" + table.Rows[i]["idTinTuc"].ToString() + "'";
                //bool ktUpdateTD = Connect.Exec(sqlUpdateTD);

                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim()));
                string urlTinDangChiTiet = Domain + "/tag/" + TieuDeSau + "-" + table.Rows[i]["idTinTuc"].ToString();
                string urlTinDangChiTiet2 = Domain + "/blog/" + table.Rows[i]["DuongDan"];
                //metaUrl = urlTinDangChiTiet2;
                //metaTitle = Title;
                //metaHinhAnh = table.Rows[1]["AnhDaiDien"].ToString();
                //string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + table.Rows[i]["idTinDang"].ToString() + "'";
                // DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string urlHinhAnh = table.Rows[i]["AnhDaiDien"].ToString();
                //if (tbHinhAnh.Rows.Count > 0)
                //{
                //    if (System.IO.File.Exists(Server.MapPath("/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"])))
                //        urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"];
                //    else
                //        urlHinhAnh = Domain + "/images/icons/noimage.png";
                //}
                //else
                //    urlHinhAnh = Domain + "/images/icons/noimage.png";




                //string TenHuyen1 = StaticData.getField("District", "Ten", "Id", table.Rows[i]["idHuyen"].ToString());
                //string idTinh1 = table.Rows[i]["idTinh"].ToString();
                //string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
                //string idPhuongXa1 = table.Rows[i]["idPhuongXa"].ToString();
                //string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
                //string chuoi_DiaDiem = "";

                //if (TenPhuongXa1 != "")
                //    chuoi_DiaDiem = TenPhuongXa1;
                //else
                //{
                //    if (TenHuyen1 != "")
                //        chuoi_DiaDiem = TenHuyen1;
                //    else
                //    {
                //        if (TenTinh1 != "")
                //            chuoi_DiaDiem = TenTinh1;
                //        else
                //            chuoi_DiaDiem = "Toàn quốc";
                //    }
                //}

                //string DienTich = table.Rows[i]["DienTich"].ToString();
                //string DienTich2 = table.Rows[i]["DienTich"].ToString();
                //if(DienTich == "")
                //{

                //}
                //else
                //{
                //    DienTich = "  " + "<div class='spDT'><img class='foricon' src='../Images/td/area.png' />&nbsp;" + DienTich + @" m<sup>2</sup></div>";

                //    DienTich2 = "  " + "<span style='font-size: 11px;color: gray;'> Diện tích :" + DienTich2 + @" m<sup>2</sup></span>";

                //}

                //chuoi_DiaDiem = TenTinh1;
                //if (TenTinh1 == "")
                //    chuoi_DiaDiem = "Toàn quốc";
                //string LoaiTinDang = "Cần bán";
                //if (table.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                //    LoaiTinDang = "Cần mua";
                string MoTaNganMobile = table.Rows[i]["MoTaNgan"].ToString();
                if (MoTaNganMobile.Length >= 70)
                {
                    MoTaNganMobile = MoTaNganMobile.Substring(0, 60) + "...";
                }
                html += @"
                           <div>
                               <li class='ctAdListingWrapper listView'>
                                   <a rel='nofollow' class='ctAdListingItem' action='push' href='" + urlTinDangChiTiet2 + @"'>
                                       <div class='ctAdLitingThumbnail'>
                                           <img alt='" + table.Rows[i]["TieuDe"].ToString() + @" - TungTang' class='lazyload thumbnailImageListing' src='" + urlHinhAnh + @"'>
                                       </div>




                                       <div class='ctAdListingBody'>
                                           <h4 class='ctAdListingTitle' style='font-size: 14px;'>" + (table.Rows[i]["Ishot"].ToString().Trim() == "True" ? ("<b>" + table.Rows[i]["TieuDe"].ToString() + "</b>") : ("<b>" + table.Rows[i]["TieuDe"].ToString() + "</b>")) + @"</h4>
                                           


<h5 class='h5An' style='color:black'>" + table.Rows[i]["MoTaNgan"].ToString() + @"</h5>


<div class='ctAdListingUpperBody'>
                                               <span class='ctAdListingPrice'>

<span class='adPriceNormal' style='color:black; font-weight: normal;'>
                                               </span>
                                                   <span class='adPriceMobile320' style='font-size: 10px;'>" + MoTaNganMobile + @"</span>
                                                   
                                                </span>
                                             
                                           </div>
                                       </div>  
";
                if (table.Rows[i]["Ishot"].ToString().ToUpper() == "TRUE")
                {
                    html += "    <div class='ctRibbonAd' style='background-color:  #008C72; border-color:  #008C72; color: rgb(255, 255, 255);z-index:0;'>HOT</div>";
                }

                else
                {

                }
                html += @"</a>";

                string NgayDang = "";
                try
                {
                    NgayDang = DateTime.Parse(table.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy HH:mm");
                }
                catch
                {

                }
                html += @"<div class='ctAdListingLowerBody false'>  
                                       <div class='adItemPostedTime'><sup><span>Ngày: </span></sup></div>
                                      
                                       <div class='adItemPostedTime'><sup><span>" + NgayDang + @"</span></sup></div>
                                      
                                   </div>";
                //                if (table.Rows[i]["Ishot"].ToString().Trim() == "True")
                //                    html += @"<div class='ctRibbonAd' style='background-color:  #008C72; border-color:  #008C72; color: rgb(255, 255, 255);z-index:0;'>HOT</div>";
                //                html += @"
                //                                   </a>
                //                                   <div class='ctAdListingLowerBody false'>  
                //                                       <div class='adItemPostedTime'><span>" + LoaiTinDang + @"</span></div>
                //                                       <span class='delimeter'></span>
                //                                       <div class='adItemPostedTime'><span>" + TinhThoiGianLucDang(DateTime.Parse(table.Rows[i]["NgayDayLenTop"].ToString())) + @"</span></div>
                //                                       <span class='delimeter'></span>
                //                                       <div class='adItemPostedTime'><span>" + chuoi_DiaDiem + @"</span></div>
                //                                   </div>
                //                               </li>
                //                           </div> ";

            }
            html += "</ul>";
            string html_Trang = "";

            string url = "";
            url = Domain + "/tag/" +nametags + "?";
            //if (BoLocThem != "")
            //    url += "BLT=" + BoLocThem + "&";
            //if (idTinh != "")
            //    url += "TT=" + idTinh + "&";
            //if (idHuyen != "")
            //    url += "QH=" + idHuyen + "&";
            //if (idPhuongXa != "")
            //    url += "PX=" + idPhuongXa + "&";
            //if (TenCuaHang != "")
            //    url += "CH=" + TenCuaHang + "&";
            //if (idtags != "")
            //    url += "C2=" + idtags + "&";
            url += "P=";
            html_Trang += @"    <li class='sc-jhAzac jDyZht'><a class='first' itemprop='url' href='" + url + txtFistPage + @"'>«</a></li>";
            //Page 1
            if (txtPage1 != "")
            {
                if (Pages.ToString() == txtPage1)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage1 + "'>" + txtPage1 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage1 + "'>" + txtPage1 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 2
            if (txtPage2 != "")
            {
                if (Pages.ToString() == txtPage2)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage2 + "'>" + txtPage2 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage2 + "'>" + txtPage2 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 3
            if (txtPage3 != "")
            {
                if (Pages.ToString() == txtPage3)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage3 + "'>" + txtPage3 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage3 + "'>" + txtPage3 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 4
            if (txtPage4 != "")
            {
                if (Pages.ToString() == txtPage4)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage4 + "'>" + txtPage4 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage4 + "'>" + txtPage4 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            //Page 5
            if (txtPage5 != "")
            {
                if (Pages.ToString() == txtPage5)
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a class='active' itemprop='url' href='" + url + txtPage5 + "'>" + txtPage5 + "</a></li>";
                else
                    html_Trang += "<li class='sc-jhAzac jDyZht'><a itemprop='url' href='" + url + txtPage5 + "'>" + txtPage5 + "</a></li>";
            }
            else
            {
                html_Trang += "<li class='sc-jhAzac jDyZht hidden'><a itemprop='url' href=''></a></li>";
            }
            if (Pages.ToString() == txtLastPage)
                html_Trang += "<li class='sc-jhAzac jDyZht'><a class='last' itemprop='url' href='" + url + txtLastPage + "'>›</a></li>";
            else
                html_Trang += "<li class='sc-jhAzac jDyZht'><a class='last' itemprop='url' href='" + url + (Pages + 1) + "'>›</a></li>";

            ulTrang.InnerHtml = html_Trang;
        }
        else
        {
            html = @"<div class='alert alert-warning'>
                      Không tìm thấy kết quả nào</a>.
                    </div>";
        }
        //Quang Cao 
        //html += "   <div class='ads1'><img src='/images/Advertisement/ads-1.gif' style='width:100%;max-height:600px;'/></div>";
        //html += "   <div class='ads2'><img src='/images/Advertisement/ads-2.gif' style='width:100%;max-height:600px;'/></div>";
        //
        dvTinDang.InnerHtml = html + htmlInnerHtmlTitle;
    }
    string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }
    void LoadMinMax_rangeBoLoc(string idDanhMuc, string CapDanhMuc, string GiaTuHienTai, string GiaDenHienTai)
    {
        DataTable tableMucGiaDanhMuc;
        decimal GiaBoLoc = 30000000000;
        decimal StepGiaBoLoc = 100000000;
        try
        {
            if (CapDanhMuc == "Cap2")
                tableMucGiaDanhMuc = Connect.GetTable("select MucGia_BoLoc,StepMucGia_BoLoc from tb_DanhMucCap2 where idDanhMucCap2=" + idDanhMuc);
            else
                tableMucGiaDanhMuc = Connect.GetTable("select MucGia_BoLoc,StepMucGia_BoLoc from tb_DanhMucCap1 where idDanhMucCap1=" + idDanhMuc);

            GiaBoLoc = decimal.Parse(tableMucGiaDanhMuc.Rows[0]["MucGia_BoLoc"].ToString());
            StepGiaBoLoc = decimal.Parse(tableMucGiaDanhMuc.Rows[0]["StepMucGia_BoLoc"].ToString());
        }
        catch
        {
            GiaBoLoc = 30000000000;
            StepGiaBoLoc = 100000000;
        }
        //decimal 

        if (GiaBoLoc == 0)
        {
            GiaBoLoc = 30000000000;
        }



        //  rgGiaTu.Attributes.Add("max", (GiaBoLoc / 2).ToString());
        //  rgGiaTu.Attributes.Add("step", StepGiaBoLoc.ToString());

        //  rgMaxGiaDen.Attributes.Add("max", GiaBoLoc.ToString());
        //   rgGiaDen.Attributes.Add("max", (GiaBoLoc + StepGiaBoLoc).ToString());
        //  rgGiaDen.Attributes.Add("min", (GiaBoLoc / 2).ToString());
        // rgGiaDen.Attributes.Add("step", StepGiaBoLoc.ToString());




        //if (GiaDenHienTai == "")
        //{
        //    rgGiaDen.Value = (GiaBoLoc + StepGiaBoLoc).ToString();
        //    spGiaDen.InnerHtml = GiaBoLoc.ToString("N0") + "";
        //}
        //if (decimal.Parse(KiemTraKhongCo_LoadLen(GiaTuHienTai)) > (GiaBoLoc / 2))
        //{
        //    rgGiaTu.Value = "0";
        //    spGiaTu.InnerHtml = "0";
        //}
        //rgGiaDen.Value = (GiaBoLoc / 2).ToString();
        //spGiaDen.InnerHtml = (GiaBoLoc / 2).ToString("N0") + "";

    }
    string LocChuoiLoai(string chuoiLoai)
    {
        string[] arr = chuoiLoai.ToCharArray().Select(c => c.ToString()).ToArray();
        string KQ = "";
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == "?")
                break;
            else
                KQ += arr[i];
        }
        return KQ;
    }
    string KiemTraKhongCo_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        //string idtags = ddltags.Value.Trim();

        //string url = Request.Url.AbsolutePath +"?";

        //// string TieuDe1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TenLinhVuc));
        //if (idtags != "")
        //    url += "C2=" + idtags + "&";
        //Response.Redirect(url);
    }

}