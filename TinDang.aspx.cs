﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TinDang : System.Web.UI.Page
{
    string Domain = "";

    string idDanhMucCap1 = "";
    string idDanhMucCap2 = "";
    string ChuoiLoai = "";
    string idTinh = "";
    string idHuyen = "";
    string idPhuongXa = "";
    string TenCuaHang = "";
    string BoLocThem = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 1;
    int MaxPage = 0;
    int PageSize = 35;
    protected void Page_Load(object sender, EventArgs e)
    {  
        if (!IsPostBack)
        {
            try
            {
                Page = int.Parse(Request.QueryString["P"].ToString());
                if (Page <= 0)
                {
                    Response.Redirect(Domain + "/tin-dang/td");
                }
            }
            catch
            {
                Page = 1;
            }
            string sUrl = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "");
            string[] arrUrl = sUrl.Split('/');
            try
            {
                if (arrUrl[2] != null && arrUrl[2].Trim() != "")
                {
                    string[] arrTinDang = arrUrl[2].Trim().Split('-');
                    if (arrTinDang[0] == "1")
                        idDanhMucCap1 = arrTinDang[1];
                    if (arrTinDang[0] == "2")
                    {
                        idDanhMucCap1 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap1", "idDanhMucCap2", arrTinDang[1]);
                        idDanhMucCap2 = arrTinDang[1];
                    }
                    if (idDanhMucCap1 != "")
                        ddlLoaiDanhMuc.Value = idDanhMucCap1;
                    if (idDanhMucCap1 != "")
                    {
                        txtLoaiDanhMuc.Value = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1);
                    }
                }
            }
            catch { }


            try
            {
                idTinh = Request.QueryString["TT"].ToString().Trim();
                txtTinhThanh.Value = idTinh;
            }
            catch { }
            try
            {
                idHuyen = Request.QueryString["QH"].ToString().Trim();
                txtQuanHuyen.Value = idHuyen;
            }
            catch { }
            try
            {
                idPhuongXa = Request.QueryString["PX"].ToString().Trim();
                txtPhuongXa.Value = idPhuongXa;
            }
            catch { }
            try
            {
                TenCuaHang = Request.QueryString["CH"].ToString().Trim();
                txtKeySearch.Value = TenCuaHang;
            }
            catch { }
            try
            {
                BoLocThem = Request.QueryString["BLT"].ToString().Trim();
                txtBoLocThem.Value = BoLocThem;
            }
            catch { }
            try
            {
                idDanhMucCap2 = Request.QueryString["C2"].ToString().Trim();
                ddlLoaiDanhMucCap2.Value = idDanhMucCap2;
            }
            catch { }
            string TenTinhThanh = StaticData.getField("City", "ten", "id", idTinh);
            string TenQuanHuyen = StaticData.getField("District", "ten", "id", idHuyen);
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "ten", "id", idPhuongXa);
            string chuoi_DiaDiem = "";
            if (TenTinhThanh != "")
                chuoi_DiaDiem = TenTinhThanh;
            if (TenQuanHuyen != "")
                chuoi_DiaDiem = TenQuanHuyen;
            if (TenPhuongXa != "")
                chuoi_DiaDiem = TenPhuongXa;

            if (TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                chuoi_DiaDiem = "Toàn quốc";
            txtDiaDiem.Value = chuoi_DiaDiem;


            //Load link title
            string sTrangChu = "";
            string sTenDanhMucCap1 = "";
            string sTenDanhMucCap2 = "";

            sTrangChu = "<a href='" + Domain + "'>Trang chủ</a>";
            sTenDanhMucCap1 = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1);
            sTenDanhMucCap2 = StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2);

            string[] Url = HttpContext.Current.Request.Url.AbsoluteUri.Replace("://", "").Replace(":\\", "").Split(new[] { "/tin-dang/" }, StringSplitOptions.None);
            ChuoiLoai = LocChuoiLoai(Url[1]);
            if (sTenDanhMucCap1 != "")
            {
                dvTitle.InnerHtml = sTenDanhMucCap1;
                if (sTenDanhMucCap2 != "")
                    dvTitle.InnerHtml = sTenDanhMucCap1 + " - " + sTenDanhMucCap2;
                this.Title = sTenDanhMucCap1 + " | TUNG TĂNG";
            }

            string linkTitle = sTrangChu;
            linkTitle += " <span style='font-family: cursive;'>>></span><a href='/tin-dang/" + Url[1] + "'>Tin đăng</a>";
            dvLinkTitle.InnerHtml = linkTitle;
            //End load link title 

            LoadTinDang();
        }
    }
    #region paging
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string SapXepTheo = " TD.idTinDang desc ";
        string GiaTu = "", GiaDen = "";
        string CanXem = "";
        string[] arrBoLocThem = BoLocThem.Split('|');
        if (arrBoLocThem.Length == 4)
        {
            GiaTu = arrBoLocThem[0];
            GiaDen = arrBoLocThem[1];

            rgGiaDen.Value = GiaDen;
            //            if (idDanhMucCap2 == "")
            {
                decimal GiaMax = 0;
                if (idDanhMucCap1 != "" && idDanhMucCap2 == "")
                    GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap1", "MucGia_BoLoc", "idDanhMucCap1", idDanhMucCap1));
                else if (idDanhMucCap1 != "" && idDanhMucCap2 != "")
                    GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap2", "MucGia_BoLoc", "idDanhMucCap2", idDanhMucCap2));

                if (GiaMax >= decimal.Parse(GiaDen))
                    spGiaDen.InnerHtml = decimal.Parse(KiemTraKhongCo_LoadLen(GiaDen)).ToString("N0");
                else if (GiaMax == 0)
                    spGiaDen.InnerHtml = (30000000).ToString("N0");
                else
                    spGiaDen.InnerHtml = (GiaMax).ToString("N0") + "+";
            }

            spGiaTu.InnerHtml = decimal.Parse(KiemTraKhongCo_LoadLen(GiaTu)).ToString("N0");
            rgGiaTu.Value = GiaTu;

            if (arrBoLocThem[2].Trim() == "PRICE")
            {
                SapXepTheo = " TD.TuGia asc ";
                radGiaThapTruoc.Checked = true;
            }
            else if (arrBoLocThem[2].Trim() == "DESC")
                radTinMoiTruoc.Checked = true;

            if (arrBoLocThem[3].Trim() == "BU")
            {
                CanXem = "CanBan";
                ckbCanMua.Checked = true;
            }
            else if (arrBoLocThem[3].Trim() == "SE")
            {
                CanXem = "CanMua";
                ckbCanBan.Checked = true;
            }
            else if (arrBoLocThem[3].Trim() == "SEaBU")
            {
                ckbCanBan.Checked = true;
                ckbCanMua.Checked = true;
            }
        }

        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  ( order by " + SapXepTheo + @"  )AS RowNumber
	              ,TD.*
                  from tb_TinDang TD 
            where  isnull(TD.isDuyet,'False')='True' ";
        if (idDanhMucCap1.Trim() != "")
        {
            sql += " and TD.idDanhMucCap1='" + idDanhMucCap1.Trim() + "'";
            //Nếu có danh mục cấp 1 thì load giá để tìm kiếm 
            if (idDanhMucCap2 == "")
                LoadMinMax_rangeBoLoc(idDanhMucCap1, "Cap1", GiaTu, GiaDen);

            //Load Danh Muc Cap 2
            DataTable tableCap2 = Connect.GetTable("select * from tb_DanhMucCap2 where idDanhMucCap1=" + idDanhMucCap1.Trim());
            if (tableCap2.Rows.Count > 0)
            {
                string html_danhMucCap2 = @"<table style='width:max-content;'>
                                              <tr>";
                for (int i = 0; i < tableCap2.Rows.Count; i++)
                {
                    html_danhMucCap2 += @"       <td style='width: 90px; padding: 0 10px;text-align:center;height:80px;'>
                                                     <a class='aTagDanhMucCap2' onclick='ChonDanhMucCap2(" + tableCap2.Rows[i]["idDanhMucCap2"] + @")'>
                                                         <img src='/" + tableCap2.Rows[i]["linkicon"] + @"'  />
                                                     </a>
                                                     <div style='height:30px;font-size:0.8em;'>" + tableCap2.Rows[i]["TenDanhMucCap2"] + @"</div>
                                                 </td> ";
                }
                html_danhMucCap2 += @"         </tr>
                                          </table>";
                dvDanhMucCap2.InnerHtml = html_danhMucCap2;
                dvDanhMucCap2.Attributes.Add("style", "overflow-x: scroll;display:block;border: 0.5px solid #ddd;padding-top:10px;padding-bottom:10px;height:max-content;");
            }
        }
        if (idDanhMucCap2 != "")
        {
            sql += " and TD.idDanhMucCap2=" + idDanhMucCap2;
            LoadMinMax_rangeBoLoc(idDanhMucCap2, "Cap2", GiaTu, GiaDen);
        }
        if (CanXem != "")
            sql += " and TD.LoaiTinDang ='" + CanXem + "' ";
        if (GiaTu != "" && GiaDen != "")
        {
            decimal GiaMax = 0;
            if (idDanhMucCap1 != "" && idDanhMucCap2 == "")
                GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap1", "MucGia_BoLoc", "idDanhMucCap1", idDanhMucCap1));
            else if (idDanhMucCap1 != "" && idDanhMucCap2 != "")
                GiaMax = decimal.Parse(StaticData.getField("tb_DanhMucCap2", "MucGia_BoLoc", "idDanhMucCap2", idDanhMucCap2));
            if (decimal.Parse(GiaTu) <= GiaMax / 2)
            {
                if (GiaMax >= decimal.Parse(GiaDen))
                    sql += " and TD.TuGia between '" + GiaTu + "' and '" + GiaDen + "' ";
                else
                    sql += " and TD.TuGia >= '" + GiaTu + "'";
            }
        }
        if (idTinh != "0" && idTinh != "")
            sql += " and TD.idTinh='" + idTinh + "'";
        if (idHuyen != "0" && idHuyen != "")
            sql += " and TD.idHuyen='" + idHuyen + "'";
        if (idPhuongXa != "0" && idPhuongXa != "")
            sql += " and TD.idPhuongXa='" + idPhuongXa + "'";
        if (TenCuaHang != "")
            sql += " and TD.TieuDe LIKE N'%" + TenCuaHang + "%'";

        sql += ") as tb1 ";
        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += "WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        DataTable table = Connect.GetTable(sql);
        string html = "";
        if (table.Rows.Count > 0)
        {
            html += "<ul>";
            SetPage(AllRowNumber);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim()));
                string urlTinDangChiTiet = Domain + "/tdct/" + table.Rows[i]["DuongDan"].ToString();
                html += "<li class='hot-job' style='position:relative;'>";
                html += "   <a href='" + urlTinDangChiTiet + "' title='" + table.Rows[i]["TieuDe"].ToString() + " - Tung Tăng'>";
                if (table.Rows[i]["isHot"].ToString() == "True")
                    html += "<div class='hot-tag'>HOT</div>";
                html += "      <span class='score'>";
                string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + table.Rows[i]["idTinDang"].ToString() + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string urlHinhAnh = "";
                if (tbHinhAnh.Rows.Count > 0)
                {
                    if (System.IO.File.Exists(Server.MapPath("/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"])))
                        urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"];
                    else
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                }
                else
                    urlHinhAnh = Domain + "/images/icons/noimage.png";

                html += "          <img src='" + urlHinhAnh + "' class='imgmenu' />";
                html += "      </span>";
                html += "      <span class='job-title'>";

                if (table.Rows[i]["isHot"].ToString() == "True")
                    html += "           <strong class='text-clip' style='font-weight:600;'>" + table.Rows[i]["TieuDe"] + "</strong>";
                else
                    html += "           <strong class='text-clip'>" + table.Rows[i]["TieuDe"] + "</strong>";
                string TenHuyen1 = StaticData.getField("District", "Ten", "Id", table.Rows[i]["idHuyen"].ToString());
                string idTinh1 = table.Rows[i]["idTinh"].ToString();
                string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
                string idPhuongXa1 = table.Rows[i]["idPhuongXa"].ToString();
                string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
                string chuoi_DiaDiem = "";

                //if (TenPhuongXa1 != "")
                //    chuoi_DiaDiem = TenPhuongXa1;
                //else
                //{
                //    if (TenHuyen1 != "")
                //        chuoi_DiaDiem = TenHuyen1;
                //    else
                //    {
                //        if (TenTinh1 != "")
                //            chuoi_DiaDiem = TenTinh1;
                //        else
                //            chuoi_DiaDiem = "Toàn quốc";
                //    }
                //}
                chuoi_DiaDiem = TenTinh1;
                if (TenTinh1 == "")
                    chuoi_DiaDiem = "Toàn quốc";
                string LoaiTinDang = "Cần bán";
                if (table.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                    LoaiTinDang = "Cần mua";

                html += "<em class='text-clip2'> "+ LoaiTinDang + " | " + TinhThoiGianLucDang(DateTime.Parse(table.Rows[i]["NgayDang"].ToString())) + " | " + chuoi_DiaDiem + "</em>";

                html += "<em class='text-clip' ><span style='color:red;font-weight:600;'>";
                if (table.Rows[i]["TuGia"].ToString() != "")
                    html += double.Parse(table.Rows[i]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + "đ";
                html += "</span></em>";
                html += "      </span>";
                html += "   </a>";
                html += "</li>";
            }
            html += "</ul>";
            html += "   <div class='pagination-TinDang' ><table style='width: 100%;margin: 7px auto;'>";
            html += "   <tr>";
            html += "       <td class='footertable'>";
            string url = "";
            url = Domain + "/tin-dang/" + ChuoiLoai + "?";
            if (BoLocThem != "")
                url += "BLT=" + BoLocThem + "&";
            if (idTinh != "")
                url += "TT=" + idTinh + "&";
            if (idHuyen != "")
                url += "QH=" + idHuyen + "&";
            if (idPhuongXa != "")
                url += "PX=" + idPhuongXa + "&";
            if (TenCuaHang != "")
                url += "CH=" + TenCuaHang + "&";
            if (idDanhMucCap2 != "")
                url += "C2=" + idDanhMucCap2 + "&";
            url += "P=";
            html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
            //Page 1
            if (txtPage1 != "")
            {
                if (Page.ToString() == txtPage1)
                    html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
                else
                    html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            }
            else
            {
                html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            }
            //Page 2
            if (txtPage2 != "")
            {
                if (Page.ToString() == txtPage2)
                    html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
                else
                    html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            }
            else
            {
                html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            }
            //Page 3
            if (txtPage3 != "")
            {
                if (Page.ToString() == txtPage3)
                    html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
                else
                    html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            }
            else
            {
                html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            }
            //Page 4
            if (txtPage4 != "")
            {
                if (Page.ToString() == txtPage4)
                    html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
                else
                    html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            }
            else
            {
                html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            }
            //Page 5
            if (txtPage5 != "")
            {
                if (Page.ToString() == txtPage5)
                    html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
                else
                    html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            }
            else
            {
                html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            }

            html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
            html += "   </td></tr>";
            html += "     </table><br/></div>";
        }
        else
        {
            html = @"<div class='alert alert-warning'>
                      Không tìm thấy kết quả nào</a>.
                    </div>";
        }
        //Quang Cao 
        html += "   <div class='ads1'><img src='/images/Advertisement/ads-1.gif' style='width:100%;max-height:600px;'/></div>";
        html += "   <div class='ads2'><img src='/images/Advertisement/ads-2.gif' style='width:100%;max-height:600px;'/></div>";
        //
        dvTinDang.InnerHtml = html;
    }
    string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }
    void LoadMinMax_rangeBoLoc(string idDanhMuc, string CapDanhMuc, string GiaTuHienTai, string GiaDenHienTai)
    {
        DataTable tableMucGiaDanhMuc;
        if (CapDanhMuc == "Cap2")
            tableMucGiaDanhMuc = Connect.GetTable("select MucGia_BoLoc,StepMucGia_BoLoc from tb_DanhMucCap2 where idDanhMucCap2=" + idDanhMuc);
        else
            tableMucGiaDanhMuc = Connect.GetTable("select MucGia_BoLoc,StepMucGia_BoLoc from tb_DanhMucCap1 where idDanhMucCap1=" + idDanhMuc);

        decimal GiaBoLoc = decimal.Parse(tableMucGiaDanhMuc.Rows[0]["MucGia_BoLoc"].ToString());
        decimal StepGiaBoLoc = decimal.Parse(tableMucGiaDanhMuc.Rows[0]["StepMucGia_BoLoc"].ToString());

        rgGiaTu.Attributes.Add("max", (GiaBoLoc / 2).ToString());
        rgGiaTu.Attributes.Add("step", StepGiaBoLoc.ToString());

        rgMaxGiaDen.Attributes.Add("max", GiaBoLoc.ToString());
        rgGiaDen.Attributes.Add("max", (GiaBoLoc + StepGiaBoLoc).ToString());
        rgGiaDen.Attributes.Add("min", (GiaBoLoc / 2).ToString());
        rgGiaDen.Attributes.Add("step", StepGiaBoLoc.ToString());

        if (GiaDenHienTai == "")
        {
            rgGiaDen.Value = (GiaBoLoc + StepGiaBoLoc).ToString();
            spGiaDen.InnerHtml = GiaBoLoc.ToString("N0") + "+";
        }
        if (decimal.Parse(KiemTraKhongCo_LoadLen(GiaTuHienTai)) > (GiaBoLoc / 2))
        {
            rgGiaTu.Value = "0";
            spGiaTu.InnerHtml = "0";
        }
    }
    string LocChuoiLoai(string chuoiLoai)
    {
        string[] arr = chuoiLoai.ToCharArray().Select(c => c.ToString()).ToArray();
        string KQ = "";
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == "?")
                break;
            else
                KQ += arr[i];
        }
        return KQ;
    }
    string KiemTraKhongCo_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        string idLinhVuc = ddlLoaiDanhMuc.Value.Trim();
        string idLinhVucCap2 = ddlLoaiDanhMucCap2.Value.Trim();
        string TenLinhVuc = txtLoaiDanhMuc.Value.Trim();
        string TinhThanh = txtTinhThanh.Value.Trim();
        string QuanHuyen = txtQuanHuyen.Value.Trim();
        string PhuongXa = txtPhuongXa.Value.Trim();
        string TenCuaHang = txtKeySearch.Value.Trim();
        string BoLocThem = txtBoLocThem.Value.Trim();

        string url = Request.Url.AbsolutePath + "?";
        if (idLinhVuc != "")
        {
            string TieuDe1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TenLinhVuc));
            url = "/tin-dang/1-" + idLinhVuc + "-" + TieuDe1 + "?";
            if (idLinhVuc == "0")
                url = "/tin-dang/td?";
        }
        if (idLinhVuc != "" && idLinhVuc != "0")
            url += "LV=" + idLinhVuc + "&";
        if (TinhThanh != "" && TinhThanh != "0")
            url += "TT=" + TinhThanh + "&";
        if (QuanHuyen != "" && QuanHuyen != "0")
            url += "QH=" + QuanHuyen + "&";
        if (PhuongXa != "" && PhuongXa != "0")
            url += "PX=" + PhuongXa + "&";
        if (TenCuaHang != "")
            url += "CH=" + TenCuaHang + "&";
        if (idLinhVucCap2 != "")
            url += "C2=" + idLinhVucCap2 + "&";
        if (BoLocThem != "")
            url += "BLT=" + BoLocThem + "&";

        Response.Redirect(url);
    }
}