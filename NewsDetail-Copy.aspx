﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="NewsDetail-Copy.aspx.cs" Inherits="NewsDetail_Copy" EnableEventValidation="false"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

		
  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="../Css/Page/TinDang.css" rel="stylesheet" />
    <script src="../Js/Page/TinDang.min.js"></script>
    <style>

        .aTC{
            color:#33659c;
        }
        .aTC:hover{
             color:#33659c;
             text-decoration:underline;
        }

        .form-control {
            border: 0.5px solid #ddd;
            padding-right: 20px;
        }

        #vnw-home .top-job ul li {
            margin: 3px 0;
        }

        .jDyZht a.first {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .ctStickyAdsListing .ctAdListingWrapper .ctAdListingLowerBody .adItemPostedTime span {
            opacity: 0.5;
        }

        .foricon{
            width: 15px;
        }
         .foricon2{
            width: 15px;
        }
        .spDT
        {
           font-size: 11px;color: gray;  padding-top:5px; color:#008C72;
        }
        @media only screen and (max-width: 767px) {
.spDT
        {
           font-size: 10px;color: gray;color:#008C72;
         padding-top:1px;
        }

 .foricon{
            width: 15px;
        }

  .foricon2{
            width: 15px; margin-bottom: 1px;
        }
}
   .gr a{
        color: #ff9600;
        text-decoration: none;
        text-align:left;
    }
    .gr p{
        font-size:16px;
         text-align:left;
    }
    .gr h1{
        font-size:22px;
         text-align:left;
    }
    .gr h2{
        font-size:18px;
         text-align:left;
    }
    .gr h3{
        font-size:16px;
         text-align:left;
    }
	@media only screen and (max-width: 767px) {
            .h5An {
                display: none;
            }
        }
    </style>
	 <style>
           .box-register-new {
        border: 1px dotted #e1e1e1;
        padding: 25px 15px 30px 15px;
        margin-bottom: 15px;
    }
            .text-center {
            text-align: center;
}
                .box-register-new .txt {
        margin: 0;
        color: #2d2d2d;
        font-size: 22px;
        padding: 0;
    }
                .box-register-new .list {
    display: block;
}
        @media (max-width: 480px) {
            .box-register-new ul {
                font-size: 16px;
            }
        }
            .box-register-new ul {
        display: inline-block;
        color: #0884ac;
        font-size: 17px;
        margin: 15px 0 20px 0;
    }
                .box-register-new ul li {
        border-left: 1px solid #ccc;
        padding: 0 7px;
        line-height: 14px;
    }
                @media (max-width: 480px){
                .box-register-new ul li {
                    float: none !important;
                    border: none;
                    margin-top: 10px;
                    display: inline-block;
                }

                }
                        .box-register-new ul li:first-child {
                            border-left: none;
        }
                        .box-register-new .register {
                            line-height: 54px;
                        }
                    @media (max-width: 480px) {
                        .register {
                            max-width: 100%;
                            padding: 0 15px;
                            font-size: 18px;
                        }
                    }
                    .register {
                        white-space: nowrap;
                        background-color: #4cb050;
                        font-size: 24px;
                        font-family: font-helveticaNeueBold;
                        line-height: 65px;
                        padding: 0px 25px;
                        border: none;
                        color: #fff;
                        cursor: pointer;
                        -webkit-transition: all 0.2s ease-in-out;
                        -moz-transition: all 0.2s ease-in-out;
                        -ms-transition: all 0.2s ease-in-out;
                        -o-transition: all 0.2s ease-in-out;
                        border-radius: 3px;
}
                    .register {
    font-family: font-helveticaNeueMedium,arial,sans-serif;
    font-size: 22px;
    padding: 0 45px;
}
    </style>
    
    <style>
        .ico-google-new {
            height: 20px;
            line-height: 20px;
            background: url(../../images/gogl-new.png) right center no-repeat;
            padding-right: 82px;
            position: relative;
            /*padding-left: 9px;*/
            /*font-size: 13px;*/
            background-size: 80px;
            /*color: #f26522;*/
                    }
        .lt {
            display: block;
            float: left;
            }
        .ico-google-new::before {
            content: "";
            display: inline-block;
            width: 1px;
            height: 15px;
            /*background: #dcdcdc;*/
            position: absolute;
            left: 0;
            top: 2px;
            }
    </style>
    
        <link href="../Css/Slide.css" rel="stylesheet" />
    <link href="../Css/Page/ChiTietTinDang.css" rel="stylesheet" />
    <link href="../Css/Page/TinDangBlog.css" rel="stylesheet" />

    <%--OwlCarousel--%>
    <script src="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.js"></script>
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.css" rel="stylesheet" />
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.theme.default.min.css" rel="stylesheet" />

    <meta property="og:title" id="ogtitle" name="ogtitle" content="" runat="server" />
   <%-- <link rel="canonical" id="canonical" name="canonical" runat="server" href="https://tungtang.com.vn">--%>
    <link href="WowSlider/engine1/style.css" rel="stylesheet" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- <header>
        <div class="gEQeZ-pfX6E9qs-T0zFQG">
            <ol id="ContentPlaceHolder1_dvLinkTitle2" class="breadcrumb _3WIL_EScB1tm02Oqj0vbu8">
                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu"><a href="/"><span>&nbsp;Trang chủ</span></a></li>
                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu">
                    <span itemprop="item"><strong id="dvTieuDe" runat="server" itemprop="name">asdxasxz</strong></span>
                </li>
            </ol>
        </div>
        
    </header>--%>
   <main class="App__Main-fcmPPF dlWmGF">

       <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0" nonce="2DKCsLWC"></script>
        <article>
            <div class="container _1OmYs5J4aYBAERDzg-6W8a">
                 <%--<div class="row hKeDdA_G7No3HepOClD8G">
                    <div class="col-sm-12 hidden-xs">
                        <div id="dvTieuDe" class="linktitle" runat="server">
                           <a href="/">Trang chủ</a> > <a href="#">Cho thuê</a> > <a href="#">Thuê bất động sản</a> > Thuê nhà nguyên căn

                        </div>

                    </div>
                </div>--%>
                <div class="row no-gutter-sm-down">
                    <div class="col-md-12" style="z-index:0">
                        <div id="dvTieuDe" class="linktitle" runat="server" style="font-size: 11px;padding-left:3px; margin-top:34px;">
                         

                        </div>

                        
                        <div class="col-md-12" style="padding-left: 6px;padding-right: 1px;" >
                            
                            

                            <div class="container gr" id="dvNoiDung" runat="server" style="padding-left:2%;padding-right:2%;padding-bottom:2%;width:100%;overflow:auto;" >

                            </div>
  
                                <div class="col-md-12 margin-top-10">
                                    <span><img src="https://cdn3.iconfinder.com/data/icons/glypho-free/64/share-256.png" style="width:25px;"/></span>
                                    <span><strong class="text-muted">Chia sẻ bài viết:</strong></span>
                                    
                                    <div class="row container">
                                        <p id="dvChiaSeBaiViet" runat="server" style="color: #888888; margin-bottom: 0;position:relative;">
                                        </p>
                                    </div>                                   
                                    <input type='text' value='' id='myLinkCopy' runat="server" style="position: absolute; top: -10000px;" readonly/>
                                   
                                     
                                </div>
							
							
                                <div class="col-md-12 margin-top-10">
                                <div class="box-register-new text-center">
                                <h4 class="txt">Đăng tin mua bán rao vặt miễn phí</h4>
                            <div class="text-center list">
                                <ul class="clearfix" style="padding-left:0px;">
                                    <li class="pull-left" style="list-style:none;">Với hơn <strong>1000+ </strong>
                                         người dùng tin tưởng sử dụng
                                    </li>
                                   <%-- <li class="pull-left"  style="list-style:none;">
                                        Chỉ từ: <strong>6.000đ</strong>/ ngày
                                    </li>--%>
                                </ul>
                                </div>
                                <button class="register box-popup-register"  data-scrolltop="no">
                                    <a href="https://tungtang.com.vn/dang-ky/dk" target='_blank' style="color:white;font-size:18px;">
                                    <span class="icon-caret">
                                        <i class="fa fa-external-link-square"></i>
                                    </span>
                                     Đăng ký ngay
                                    </a>
                                </button>
                            </div>
                              </div>
							
                              <div class="col-md-12 margin-top-10">
                                    <span><img src="https://cdn2.iconfinder.com/data/icons/complete-common-version-6-6/1024/tags-256.png" style="width:25px;"/></span>
                                    <span><strong class="text-muted">Tags:</strong></span>
                                  
                                    <div class="row container" style="overflow:auto;height: auto;">
                                        <p id="dvTags" runat="server">
                                        </p>
                                    </div>                                                 
                                   
                                     
                                </div>

                                <div>
                                    <!-- react-empty: 2823 -->
                                </div>
                                <div class="_1uIWaTfPiU1VXSU5AnCLHo margin-top-10"></div>
                            </div>

						

  
                 <div class="_2lwN4F8oWK7ISQo377szov" >
                     <h3 class="_1S1hKqDtaGnfV2qZE3uluy">Bài viết liên quan</h3>
          
                       <div class="pgjTolSNq4-L--tpQ7Xp5">
                                    <div>
                                        <div class="ctStickyAdsListing" id="abc" runat="server">
                                        </div>
                                    </div>
                                </div>
        
                </div>
            </div>
				</div>
        </article>
    </main>
    <script src="WowSlider/engine1/wowslider.js"></script>
    <script src="WowSlider/engine1/script.js"></script>
</script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }
        function CopyLinkURL() {
            var copyText = document.getElementById("ContentPlaceHolder1_myLinkCopy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $('#spSaoChepThanhCong').show();
            setTimeout(function () { $('#spSaoChepThanhCong').hide(); }, 3000);
        }

        function showSlides(n) {
            try {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) { slideIndex = 1 }
                if (n < 1) { slideIndex = slides.length }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
            catch (e) {

            }
        }
        var x = $(window).width();
        var owl = $('.owl-carousel');
        if (x < 500) {
            owl.owlCarousel({

                items: 2,
                loop: true,
                margin: 10,
                autoplay: true,
                slideSpeed: 1000,

                autoplayTimeout: 5000,
                autoplayHoverPause: true,

            });
        }
        else {
            owl.owlCarousel({

                items: 4,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 9000,
                autoplayHoverPause: true
            });
        }


    </script>

</asp:Content>

