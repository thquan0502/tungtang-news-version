﻿<%@ Page Title="Đăng ký" EnableEventValidation="false" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/asset/front/css/auth.css" rel="stylesheet" />
	<style>
        @media (min-width:992px) {  
             #ortt{
                margin-left:95px;
                padding-top:20px;
                color:#555
            }          
            .colorfb{
                margin-left:52px;    
                width:210px            
            }
            .colorgg{
                margin-left:52px;
                margin-top: 10px;                
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
            }

            .btn-google {
                color: black;
                width:214px;
                background-color: white;
                border: 1px solid #ccc;
            }
            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social:hover {
                color: #eee;
            }
            .btn-social1 {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social1:hover {
                color: black;
            }

            .btn-social1 :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .btn-social :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .signup{
                margin-left:80px;
            }
        }
        @media (max-width:600px) {
            .signup{
                margin-left:10px;
            }
            .colorfb{
                margin-left:7px;  
                width:210px              
            }
            .colorgg{
                margin-left:7px;
                margin-top: 10px;                
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
            }

            .btn-google {
                color: black;
                width:214px;
                background-color: white;
                border: 1px solid #ccc;
            }
            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social:hover {
                color: #eee;
            }
            .btn-social1 {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social1:hover {
                color: black;
            }

            .btn-social1 :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .btn-social :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }         
            #ortt{
                margin-left:65px;
                padding-top:20px;
                color:#555
            }   
        }
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            .signup{
                margin-left:60px;
            }
            .colorfb{
                margin-left:20px;
                width:210px;                
            }
            .colorgg{
                margin-left:20px;
                margin-top: 10px;                
            }
            .btn-facebook {
                color: #fff;
                background-color: #3b5998;
            }

            .btn-google {
                color: black;
                width:214px;
                background-color: white;
                border: 1px solid #ccc;
            }
            .btn-social {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social:hover {
                color: #eee;
            }
            .btn-social1 {
                position: relative;
                padding-left: 44px;
                text-align: left;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            .btn-social1:hover {
                color: black;
            }

            .btn-social1 :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }
            .btn-social :first-child {
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 40px;
                padding: 7px;
                font-size: 1.6em;
                text-align: center;
            }  
             #ortt{
                margin-left:65px;
                padding-top:20px;
                color:#555
            }         
   }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="container">
        <div class="row" style="margin-top:88px;">
            <div class="col-sm-4 col-sm-push-4">

                <%-- start: page message --%>
                <% if(Session["register_error"] != null) { %>
                    <div class="alert alert-danger">
                        <%= (string) Session["register_error"] %>
                    </div>
                    <% Session["register_error"] = null; %>
                <% } %>
                <%-- end page message --%>
				
				 <%--task 2--%>
                  <%-- start: page message --%>
                <% if(Session["register_errors"] != null) { %>
                    <div class="alert alert-danger btn-resends" id="arlert-head">
                        <%= (string) Session["register_errors"] %>
                    </div>
                    <% Session["register_errors"] = null; %>
                <% } %>
                <%-- end page message --%>

				<h5 class="text-center"><strong>ĐĂNG KÝ</strong></h5>

                <div id="divRegister" runat="server">
                    <div class="form-group">
                        <div class ="first-name">
                            <asp:TextBox id="txtFirstName" TextMode="SingleLine" placeholder="Họ" class="form-control" runat="server" required="required" />
                        </div>
                        <div class="last-name">
                            <asp:TextBox id="txtLastName" TextMode="SingleLine" placeholder="Tên" class="form-control" runat="server" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:TextBox id="txtPhone" TextMode="SingleLine" placeholder="Số điện thoại" class="form-control" runat="server" required="required" />
                    </div>
                    <div class="form-group">
                        <asp:TextBox id="txtPass" TextMode="SingleLine" placeholder="Mật khẩu" class="form-control" runat="server" required="required" />
                    </div>
                    <div class="form-group">
                        <asp:TextBox id="txtPass2" TextMode="SingleLine" placeholder="Nhập lại mật khẩu" class="form-control" runat="server" required="required" />
                    </div>
                    <div class="form-group">
						<div class="register-privacy-wrap">
                            <p class="line">
                                <input type="checkbox" id="chbxPrivacy" runat="server"/> Tôi đồng ý với các <a href="/mo-ta/dieu-khoan-va-dich-vu" target="_blank">điều khoản</a> và <a href="/mo-ta/chinh-sach-bao-mat-thong-tin"  target="_blank">chính sách bảo mật</a>.
                            </p>
                        </div>
                    </div>                                          
                    <div class="form-group ">                              
                        <asp:Button ID="btnRegister" runat="server" Text="Đăng ký" OnClick="btnRegister_Click" class="btn btn-primary btn-block" />
					</div>
					 <div class="form-group social" >
                        <label class="social-label">Hoặc tiếp tục bằng</label><br />
						  <div class="container"><a class="btn btn-lg btn-social btn-facebook colorfb" ID="btnFacebook" runat="server" onserverclick="btnFacebook_Click"><i class="fa fa-facebook-square" style="margin-top:-2px;"></i> Đăng ký bằng Facebook</a></div>
						  <div class="container"><a class="btn btn-lg btn-social1 btn-google colorgg" ID="btnGoogle" runat="server" onserverclick="btnGoogle_Click"><img src="https://tungtang.com.vn/Images/google.png" style="width:38px;margin-top:-3px;" /> Đăng ký bằng Google</a></div> 
                    </div>
                </div>
                 <div id="divVerification" runat="server">
                    <div class="alert alert-success btn-resends" id="alert1">
                        <i><a href="/" target="_blank"><strong>Tungtang.com.vn</strong></a> đã gửi mã xác thực đến số điện thoại <a href="tel:<%= dataSmsPhone %>"><strong><%= dataSmsPhone %></strong></a>, hãy kiểm tra và nhập mã xác thực</i> 
                    </div>
                     <div class="alert alert-danger " id="alert2">
                        <i>Mã xác thực đã hết hiệu lực, xin quý khách gửi lại mã xác thực mới!</i> 
                    </div>
                    <div class="form-group btn-resends" id="annon">
						<div class="first-name" style="width: calc(80% - 2px);">
                        <asp:TextBox id="txtCode" TextMode="SingleLine" placeholder="Nhập mã xác thực" class="form-control" runat="server" />
							 </div>
						<div class="last-name" style="width: calc(20% - 2px);">
							  <asp:TextBox TextMode="SingleLine" placeholder="Gửi lại sau" class="form-control btn-resend text-time " ReadOnly runat="server" />
							</div>
						<p style="color: red;"> Vui lòng nhập mã xác thực!</p>
                    </div>
                    <div class="form-group btn-resends " id="btnVerifys">
                        <asp:Button ID="btnVerify" runat="server" Text="Xác thực" OnClick="btnVerify_Click" class="btn btn-primary btn-block btn-resends" />
                        </div>
                     <div class="form-group" id="btnResends">
                        <asp:Button ID="btnResend" runat="server" Text="Gửi lại mã" OnClick="btnResend_Click" class="btn btn-success btn-block btn-resends" />
                    </div>
                    <div class="form-group">
                    <asp:Button ID="btnReregister1" runat="server" Text="Đăng ký lại từ đầu" OnClick="btnReregister2_Click" class="btn btn-info btn-block" />
                    </div>
                </div>
                <div id="divExpiration" runat="server">
                    <div class="alert alert-warning">
                        <i>Bạn đang thực hiện đăng ký tài khoản <a href="/" target="_blank"><strong>tungtang.com.vn</strong></a> với số điện thoại <a href="tel:<%= dataSmsPhone %>"><%= dataSmsPhone %></a>, hãy xác thực lại số điện thoại của bạn.</i> 
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnReverify" runat="server" Text="Xác thực lại" OnClick="btnReverify_Click" class="btn btn-primary btn-block" />
                        <asp:Button ID="btnReregister2" runat="server" Text="Đăng ký lại từ đầu" OnClick="btnReregister2_Click" class="btn btn-primary btn-block" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        (function () {
            var diffSeconds = <%= diffSeconds %>;
            if (diffSeconds >= 0 && diffSeconds < 60) {
                var remainSeconds = 60 - diffSeconds;
                var text = $('.btn-resend').val();
                var fnLoop = async function () {
                    if (remainSeconds > 0) {
                        $('.btn-resend').attr('disabled', 'disabled');
                        $('#btnResends').addClass('btn-resends');
                        $('#btnVerifys').removeClass('btn-resends');
                        $('#alert2').addClass('btn-resends');
                        $('#annon').removeClass('btn-resends');
						 $('#arlert-head').removeClass('btn-resends');
                        $('#alert1').removeClass('btn-resends');
                        $('.btn-resend').val(`${text} (${remainSeconds})`);
                        remainSeconds = remainSeconds - 1;
                        setTimeout(fnLoop, 1000);
                    } else {
                        $('#btnResends').removeClass('btn-resends');
                        $('#btnVerifys').addClass('btn-resends');
                        $('#alert1').addClass('btn-resends');
                        $('#annon').addClass('btn-resends');
						$('#arlert-head').addClass('btn-resends');
                        $('#alert2').removeClass('btn-resends');
                        $('.btn-resend').addClass('btn-resends');
                        $('.btn-resend').val(text);
                       
                    }
                }
                fnLoop();
            }
        }) ();
    </script>
</asp:Content>
