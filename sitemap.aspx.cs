﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

public partial class sitemap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string dateTime = DateTime.Now.ToString();
       string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
        Response.Clear();
        Response.ContentType = "text/xml";
        using (XmlTextWriter writer = new XmlTextWriter(Server.MapPath("sitemap.xml"), Encoding.UTF8))
        {
            writer.WriteStartDocument();
           // writer.WriteStartElement("?xml", "version='1.0' encoding='UTF - 8'");
            writer.WriteStartElement("urlset");
            writer.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");

            string dbstring = @"Data Source=125.253.127.73;Initial Catalog=tungtang;User ID=tungtang; Password=cu@61Fx3;Connect Timeout=10";
            SqlConnection connection = new SqlConnection(dbstring);
            connection.Open();
            //load trang chủ
            string sqltc = "select * from tb_Domain";
            DataTable tables = Connect.GetTable(sqltc);
           // string dateTime = DateTime.Now.ToString();
         //   string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", System.Globalization.CultureInfo.InvariantCulture);
            if (tables.Rows.Count > 0)
            {
               
                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dt).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "1.00");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/dang-nhap/dn");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();
				
					writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/dang-ky/dk");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/mo-ta/huong-dan");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/mo-ta/dieu-khoan-va-dich-vu");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/mo-ta/chinh-sach-bao-mat-thong-tin");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/mo-ta/co-che-giai-quyet-tranh-chap");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/mo-ta/gioi-thieu");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/mo-ta/quy-che-hoat-dong");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tag/tu-khoa");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/td");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/thong-tin-ca-nhan/ttcn");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/thong-tin/tt");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/dang-tin/dt");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();
					
					writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/sitemap/sitemap-blog.xml");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();
					
					writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/sitemap/sitemap-tindang.xml");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();

            }
            //load danh mục cap 1
            string sqldm = "select idDanhMucCap1,Titlte from tb_DanhMucCap1";
            DataTable tbdm = Connect.GetTable(sqldm);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm.Rows.Count; i++)
                {
                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm.Rows[i]["idDanhMucCap1"].ToString()+"-"+ StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbdm.Rows[i]["Titlte"].ToString().Trim())) + "");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //load danh muc cap 2

            string sqldm1 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='32'";
            DataTable tbdm1 = Connect.GetTable(sqldm1);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm1.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm1.Rows.Count; i++)
                {
                   
                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm1.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm1.Rows[i]["idDanhMucCap1"].ToString() +"&C2="+ tbdm1.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //1
            string sqldm2 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='37'";
            DataTable tbdm2 = Connect.GetTable(sqldm2);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm2.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm2.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm2.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm2.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm2.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //2
            string sqldm3 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='38'";
            DataTable tbdm3 = Connect.GetTable(sqldm3);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm3.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm3.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm3.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm3.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm3.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //3
            string sqldm4 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='39'";
            DataTable tbdm4 = Connect.GetTable(sqldm4);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm4.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm4.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm4.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm4.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm4.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //4
            string sqldm5 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='40'";
            DataTable tbdm5 = Connect.GetTable(sqldm5);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm5.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm5.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm5.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm5.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm5.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //5
            string sqldm6 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='41'";
            DataTable tbdm6 = Connect.GetTable(sqldm6);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm6.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm6.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm6.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm6.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm6.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //6
            string sqldm7 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='42'";
            DataTable tbdm7 = Connect.GetTable(sqldm7);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm7.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm7.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm7.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm7.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm7.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //7
            string sqldm8 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='43'";
            DataTable tbdm8 = Connect.GetTable(sqldm8);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm8.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm8.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm8.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm8.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm8.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //8
            string sqldm9 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='44'";
            DataTable tbdm9 = Connect.GetTable(sqldm9);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm9.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm9.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm9.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm9.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm9.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //9
            string sqldm10 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='45'";
            DataTable tbdm10 = Connect.GetTable(sqldm10);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm10.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm10.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm10.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm10.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm10.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //10
            string sqldm11 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='47'";
            DataTable tbdm11 = Connect.GetTable(sqldm11);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm11.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm11.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm11.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm11.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm11.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //11
            string sqldm12 = "select idDanhMucCap2,idDanhMucCap1,Titlte from tb_DanhMucCap2 where idDanhMucCap1='48'";
            DataTable tbdm12 = Connect.GetTable(sqldm12);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbdm12.Rows.Count > 0)
            {
                for (int i = 0; i < tbdm12.Rows.Count; i++)
                {

                    // Response.Redirect("https://tungtang.com.vn");
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tin-dang/1-" + tbdm12.Rows[i]["idDanhMucCap1"].ToString() + "-?LV=" + tbdm12.Rows[i]["idDanhMucCap1"].ToString() + "&C2=" + tbdm12.Rows[i]["idDanhMucCap2"].ToString() + "&");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.64");
                    writer.WriteEndElement();
                }
            }
            //12

            //load tags
            string sqltags = "select DuongDan from Tags";
            DataTable tbtags = Connect.GetTable(sqltags);
            //   string dateTime = DateTime.Now.ToString();
            // string createddate = Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00");
            //  DateTime dt = DateTime.ParseExact(createddate, "yyyy-MM-dd'T'HH:mm:ss+07:00", CultureInfo.InvariantCulture);
            if (tbtags.Rows.Count > 0)
            {
                for (int i = 0; i < tbtags.Rows.Count; i++)
                {
                    // Response.Redirect("https://tungtang.com.vn");     string date = tbtt.Rows[i]["NgayDang"].ToString();
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", "https://tungtang.com.vn/tag/" + tbtags.Rows[i]["DuongDan"].ToString() + "");
                    writer.WriteElementString("lastmod", Convert.ToDateTime(dateTime).ToString("yyyy-MM-dd'T'HH:mm:ss+07:00"));
                    writer.WriteElementString("priority", "0.84");
                    writer.WriteEndElement();
                }
            }




      
        }
    }


}