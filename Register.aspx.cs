﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using ASPSnippets.GoogleAPI;
using ASPSnippets.FaceBookAPI;
using System.Configuration;

public partial class Register : System.Web.UI.Page
{
    protected string pageMode = null;
    protected string dataFirstName = null;
    protected string dataLastName = null;
    protected string dataPhone = null;
    protected string dataPass = null;
    protected DateTime dataCreatedAt = DateTime.MinValue;
    protected string dataSmsPhone = null;
    protected string dataSmsCode = null;
    protected string dataSmsResponse = null;
    protected string dataSmsMessage = null;
    protected DateTime dataSmsSentAt = DateTime.MinValue;
    protected long diffSeconds = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        // get account id
        string accountId = Utilities.Account.getAuthAccountId(Request);
        if (!string.IsNullOrWhiteSpace(accountId))
        {
            Response.Redirect("/thong-tin-ca-nhan/ttcn");
        }

        // check is social response
        if (!this.IsPostBack)
        {
            this.checkSocialLogin();
        }

        // fetch data
        string sql = @"
            SELECT TOP 1
                RegisterData.*
            FROM RegisterData
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        DataTable table = Connect.GetTable(sql);
        if (table.Rows.Count > 0)
        {
            this.dataFirstName = table.Rows[0]["FirstName"].ToString();
            this.dataLastName = table.Rows[0]["LastName"].ToString();
            this.dataPhone = table.Rows[0]["Phone"].ToString();
            this.dataPass = table.Rows[0]["Pass"].ToString();
            if (!string.IsNullOrWhiteSpace(table.Rows[0]["CreatedAt"].ToString()))
            {
                this.dataCreatedAt = (DateTime)table.Rows[0]["CreatedAt"];
            }
            this.dataSmsPhone = table.Rows[0]["SmsPhone"].ToString();
            this.dataSmsCode = table.Rows[0]["SmsCode"].ToString();
            this.dataSmsResponse = table.Rows[0]["SmsResponse"].ToString();
            this.dataSmsMessage = table.Rows[0]["SmsMessage"].ToString();
            if (!string.IsNullOrWhiteSpace(table.Rows[0]["SmsSentAt"].ToString()))
            {
                this.dataSmsSentAt = (DateTime)table.Rows[0]["SmsSentAt"];
                this.diffSeconds = (long)(DateTime.Now - this.dataSmsSentAt).TotalSeconds;
            }
        }

        if (!IsPostBack)
        {
            // identify mode
            if (string.IsNullOrWhiteSpace(this.dataSmsCode))
            {
                this.pageMode = "REGISTER";
            }
            else
            {
                string isState = Request.QueryString["IsState"];
                if(!string.IsNullOrWhiteSpace(isState) && isState == "1")
                {
                    this.pageMode = "VERIFICATION";
                }
                else
                {
                    this.pageMode = "EXPIRATION";
                }
            }

            // reset elements
            this.txtPass.Attributes["type"] = "password";
            this.txtPass2.Attributes["type"] = "password";
            this.divRegister.Visible = false;
            this.divVerification.Visible = false;
            this.divExpiration.Visible = false;

            // process page
            switch (this.pageMode)
            {
                case "REGISTER":
                    this.divRegister.Visible = true;
                    break;
                case "VERIFICATION":
                    this.divVerification.Visible = true;
                    break;
                case "EXPIRATION":
                    this.divExpiration.Visible = true;
                    break;
                default:
                    Response.Redirect("/error/404");
                    break;
            }
        }

        // SEO settings
        if (!IsPostBack)
        {
            HtmlLink canonical = new HtmlLink();
            canonical.Href = HttpContext.Current.Request.Url.OriginalString;
            canonical.Attributes["rel"] = "canonical";
            Page.Header.Controls.Add(canonical);

            Page.Header.Controls.Add(canonical); HtmlLink alternate = new HtmlLink();
            alternate.Href = HttpContext.Current.Request.Url.OriginalString;
            alternate.Attributes["rel"] = "alternate";
            Page.Header.Controls.Add(alternate);
        }
    }

    /**
     * handle register click
     */
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        // get input values
        string inputFirstName = this.txtFirstName.Text.Trim();
        string inputLastName = this.txtLastName.Text.Trim();
        string inputPhone = this.txtPhone.Text.Trim();
        string inputPass = this.txtPass.Text.Trim();
        string inputPass2 = this.txtPass2.Text.Trim();

        // check required
        if (
            string.IsNullOrWhiteSpace(inputFirstName)
            || string.IsNullOrWhiteSpace(inputLastName)
            || string.IsNullOrWhiteSpace(inputPhone)
            || string.IsNullOrWhiteSpace(inputPass)
            || string.IsNullOrWhiteSpace(inputPass2)
        )
        {
            Session["register_error"] = "Hãy điền đủ các thông tin";
            return;
        }

        // check phone number contains digits only
        if (!Utilities.Validation.isDigitsOnly(inputPhone))
        {
            Session["register_error"] = "Số điện thoại không đúng định dạng";
            return;
        }

        // check phone format
        if (!inputPhone.StartsWith("0"))
        {
            Session["register_error"] = "Số điện thoại không đúng định dạng";
            return;
        }

        // check phone number length
        if (inputPhone.Length > 10)
        {
            Session["register_error"] = "Số điện thoại không hợp lệ";
            return;
        }

        // check existed account with phone number
        if (!string.IsNullOrWhiteSpace(Utilities.Account.getIdByPhone(inputPhone)))
        {
            Session["register_error"] = "Số điện thoại đã được sử dụng";
            return;
        }

        // check password contain accented characters
        if (Utilities.Validation.containAccented(inputPass))
        {
            Session["register_error"] = "Mật khẩu không được có dấu";
            return;
        }

        // check retype password
        if (inputPass != inputPass2)
        {
            Session["register_error"] = "Mật khẩu không khớp nhau";
            return;
        }

        // check terms and privacy
        if(!this.chbxPrivacy.Checked)
        {
            Session["register_error"] = "Vui lòng đồng ý chấp nhận điều khoản và chính sách bảo mật";
            return;
        }

        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(inputPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // save db
        string sql = @"
            INSERT INTO RegisterData (
                SessionId,
                FirstName,
                LastName,
                Phone,
                Pass,
                CreatedAt,
                SmsPhone,
                SmsCode,
                SmsResponse,
                SmsMessage,
                SmsSentAt
            ) VALUES (
                '" + Session.SessionID + @"',
                N'" + inputFirstName + @"',
                N'" + inputLastName + @"',
                '" + inputPhone + @"',
                '" + inputPass + @"',
                '" + sNow + @"',
                '" + inputPhone + @"',
                '" + code + @"',
                '" + res + @"',
                '" + message + @"',
                '" + sNow + @"'
            )
        ";
        Connect.Exec(sql);

        Response.Redirect("/dang-ky/dk?IsState=1");
    }

    /**
     * handle verify click
     */
    protected void btnVerify_Click(object sender, EventArgs e)
    {
        // get input values
        string inputCode = this.txtCode.Text.Trim();

        // check code
        if(inputCode != dataSmsCode)
        {
            Session["register_errors"] = "Mã xác thực không chính xác";
            return;
        }
		

        // remove register data
        string sql = @"
            DELETE FROM RegisterData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // save new account
        string sql2 = @"
            INSERT INTO tb_ThanhVien (
                TenCuaHang,
                SoDienThoai,
                TenDangNhap,
                MatKhau,
                NgayDangKy,
                RefCode,
                IsVerified,
				TongTinDang
            ) OUTPUT INSERTED.idThanhVien VALUES (
                N'" + this.dataFirstName + " " + this.dataLastName + @"',
                '" + this.dataPhone + @"',
                '" + this.dataPhone + @"',
                '" + this.dataPass + @"',
                '" + this.dataCreatedAt.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                '" + System.Guid.NewGuid() + @"',
                '1',
				'0'
            )
        ";
        DataTable table2 = Connect.GetTable(sql2);
        string accountId = table2.Rows[0]["idThanhVien"].ToString();
        Utilities.ViewedManager.createInstanceForAccount(accountId);
        Utilities.CommissionManager.createInstanceForAccount(accountId);
        Utilities.FeeManager.createInstanceForAccount(accountId);

        // login
        Session["success"] = "Chúc mừng! Tài khoản của bạn đã được tạo thành công. Xin vui lòng bạn đăng nhập lại!";
        Response.Redirect("/dang-nhap/dn");
    }

    /**
     * handle re-send click
     */
    protected void btnResend_Click(object sender, EventArgs e)
    {
        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(dataSmsPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // data register data
        string sql = @"
            UPDATE RegisterData
            SET
                SmsCode = '"+ code + @"',
                SmsResponse = '" + res + @"',
                SmsMessage = '" + message + @"',
                SmsSentAt = '" + sNow + @"'
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // end
        Response.Redirect("/dang-ky/dk?IsState=1");
    }

    /**
     * handle re-verify click
     */
    protected void btnReverify_Click(object sender, EventArgs e)
    {
        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(dataSmsPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // data register data
        string sql = @"
            UPDATE RegisterData
            SET
                SmsCode = '" + code + @"',
                SmsResponse = '" + res + @"',
                SmsMessage = '" + message + @"',
                SmsSentAt = '" + sNow + @"'
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // end
        Response.Redirect("/dang-ky/dk?IsState=1");
    }

    /**
     * handle re-register click (verification)
     */
	
    //protected void btnReregister_Click(object sender, EventArgs e)
    //{
       // string sql = @"
        //    DELETE FROM RegisterData WHERE SessionId='"+ Session.SessionID +@"'
      //  ";
       // Connect.Exec(sql);
      //  Response.Redirect("/dang-ky/dk");
   // }

    /**
     * handle re-register click (expiration)
     */
    protected void btnReregister2_Click(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM RegisterData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        Response.Redirect("/dang-ky/dk");
    }
	 /**
     * handle re-register click (expiration)
     */
    protected void btnReregister1_Click(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM RegisterData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        Response.Redirect("/dang-ky/dk");
    }
    /**
     * handle google button click
     */
    protected void btnGoogle_Click(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM SocialData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        this.connectGoogle();
        GoogleConnect.Authorize("profile", "email");
    }

    /**
     * handle facebook button click
     */
    protected void btnFacebook_Click(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM SocialData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        this.connectFacebook();
        FaceBookConnect.Authorize("user_photos,email", Request.Url.AbsoluteUri.Split('?')[0]);
    }

    /**
     * Check if the request is social's reponse
     */
    protected void checkSocialLogin()
    {
        if (Request.QueryString["error"] == "access_denied")
        {
            Session["register_error"] = "Xảy ra lỗi liên kết social media";
            return;
        }
        string code = Request.QueryString["code"];
        string socialNetwork = Request.QueryString["social_network"];
        if (code != null && !string.IsNullOrEmpty(code))
        {
            if (socialNetwork == "google") // is google response
            {
                // fetch data
                this.connectGoogle();
                string json = GoogleConnect.Fetch("me", code);
                GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);

                DataTable tableExisted = Utilities.Account.getTableByGoogleId(profile.Id);
                if (tableExisted.Rows.Count > 0) // if already connected facebook account before
                {
                    Utilities.Auth.attempLogin(Response, tableExisted.Rows[0]["idThanhVien"].ToString());
                }
                else // if it's firt time that connect to facebook account
                {
                    string sql = @"
                        INSERT INTO SocialData (
                            SessionId,
                            SocialType,
                            GoogleId,
                            GoogleName,
                            GooglePicture,
                            GoogleEmail,
                            GoogleVerifiedEmail,
                            CreatedAt
                        ) VALUES (
                            '" + Session.SessionID + @"',
                            'google',
                            '"+ profile.Id + @"',
                            N'" + profile.Name + @"',
                            '" + profile.Picture + @"',
                            '" + profile.Email + @"',
                            '" + profile.Verified_Email + @"',
                            '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                        )
                    ";
                    Connect.Exec(sql);
                    Response.Redirect("/tai-khoan/social-media");
                }
            }
            else // is facebook response
            {
                this.connectFacebook();
                string json = FaceBookConnect.Fetch(code, "me?fields=id,name,email");
                FacebookProfile profile = new JavaScriptSerializer().Deserialize<FacebookProfile>(json);
                profile.Picture = string.Format("https://graph.facebook.com/{0}/picture", profile.Id);

                DataTable tableExisted = Utilities.Account.getTableByFacebookId(profile.Id);
                if (tableExisted.Rows.Count > 0) // if already connected google account before
                {
                    Utilities.Auth.attempLogin(Response, tableExisted.Rows[0]["idThanhVien"].ToString());
                }
                else // if it's firt time that connect to google account
                {
                    string sql = @"
                        INSERT INTO SocialData (
                            SessionId,
                            SocialType,
                            FacebookId,
                            FacebookName,
                            FacebookUserName,
                            FacebookPicture,
                            FacebookEmail,
                            CreatedAt
                        ) VALUES (
                            '" + Session.SessionID + @"',
                            'facebook',
                            '" + profile.Id + @"',
                            N'" + profile.Name + @"',
                            N'" + profile.UserName + @"',
                            '" + profile.Picture + @"',
                            '" + profile.Email + @"',
                            '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                        )
                    ";
                    Connect.Exec(sql);
                    Response.Redirect("/tai-khoan/social-media");
                }
            }
        }
    }

    /**
     * connect api google
     */
    protected void connectGoogle()
    {
        GoogleConnect.ClientId = ConfigurationManager.AppSettings["googleclientid"];
        GoogleConnect.ClientSecret = ConfigurationManager.AppSettings["googleclientsecret"];
        GoogleConnect.RedirectUri = Request.Url.AbsoluteUri.Split('?')[0] + "?social_network=google";
    }

    /**
     * connect api facebook
     */
    protected void connectFacebook()
    {
        FaceBookConnect.API_Key = ConfigurationManager.AppSettings["facebookapikey"];
        FaceBookConnect.API_Secret = ConfigurationManager.AppSettings["facebookapisecret"];
    }

    /**
     * Social response entities
     */
    #region Profile Facebook & Google
    public class FacebookProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Picture { get; set; }
        public string Email { get; set; }
    }
    public class GoogleProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Email { get; set; }
        public string Verified_Email { get; set; }
    }
    #endregion Profile Facebook & Google
}