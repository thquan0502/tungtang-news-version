﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangTin : System.Web.UI.Page
{
    string idTinDang = "";
    string Domain = "";
    string idThanhVien = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["TungTang_Login"] == null || Request.Cookies["TungTang_Login"].Value.Trim() == "")
        {
            //Đăng tin không cần đăng nhập
            Response.Redirect("/dang-nhap/dn");
        }
        else
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
            dvThongBao.Visible = false;
        }
        try
        {
            idTinDang = Request.QueryString["TDTD"].Trim();
        }
        catch { }
        if (!IsPostBack)
        {
            LoadTinDang();
        }
        if (IsPostBack && fileHinhAnh.PostedFile != null)
        {
            if (Request.Files.Count > 10)
            {
                Response.Write("<script>alert('Bạn vui lòng chọn tối đa 10 ảnh!')</script>");
                return;
            }

            for (int j = 0; j < Request.Files.Count; j++)
            {
                HttpPostedFile file = Request.Files[j];
                if (file.ContentLength > 0)
                {
                    //if (fileHinhAnh.PostedFile.FileName.Length > 0)
                    //{
                    string extension = Path.GetExtension(file.FileName);
                    if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                    {
                        if (fileHinhAnh.HasFile)
                        {
                            string Ngay = DateTime.Now.Day.ToString();
                            string Thang = DateTime.Now.Month.ToString();
                            string Nam = DateTime.Now.Year.ToString();
                            string Gio = DateTime.Now.Hour.ToString();
                            string Phut = DateTime.Now.Minute.ToString();
                            string Giay = DateTime.Now.Second.ToString();
                            string Khac = DateTime.Now.Ticks.ToString();
                            string fExtension = Path.GetExtension(file.FileName);

                            string sqlIdTinDang = "select top 1 idTinDang from tb_TinDang order by idTinDang desc";
                            DataTable tbIdTinDang = Connect.GetTable(sqlIdTinDang);
                            string idTinDang = "0";
                            if (tbIdTinDang.Rows.Count > 0)
                                idTinDang = (float.Parse(tbIdTinDang.Rows[0]["idTinDang"].ToString()) + 1).ToString();
                            string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + idTinDang + fExtension;
                            string FilePath = "~/Images/td/slides/" + FileName;

                            if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("iPhone"))) 
                                StaticData.LoadWaterMark(file, Server.MapPath(FilePath), "IOS"); 
                            else
                                StaticData.LoadWaterMark(file, Server.MapPath(FilePath), "");

                            //file.SaveAs(Server.MapPath(FilePath));

                            hdHinhAnh.Value = hdHinhAnh.Value + FileName + "|~~~~|";
                            string htmlHinhAnh = "";
                            string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                            for (int i = 0; i < arrHinhAnh.Length; i++)
                            {
                                if (arrHinhAnh[i].Trim() != "")
                                {
                                    htmlHinhAnh += "<div id='dvHinhAnh_" + arrHinhAnh[i].Trim() + "' class='imgupload'>";
                                    htmlHinhAnh += "<p style='margin:0px'><img src='../Images/td/slides/" + arrHinhAnh[i].Trim() + "' style='width: 150px;height: 110px;' /></p>";
                                    htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + arrHinhAnh[i].Trim() + "\",\"" + arrHinhAnh[i].Trim() + "\")' src='../images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                                    htmlHinhAnh += "</div>";
                                }
                            }
                            dvHinhAnh.InnerHtml = htmlHinhAnh;
                            //imgAnhCuaBan.Src = FilePath;
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                        return;
                    }
                }
            }
            //Response.Write("<script>window.scrollTo(0, document.body.scrollHeight);</script>");
        }

    }


    private void LoadTinDang()
    {
        if (idTinDang != "")
        {
            btDangTin.Text = "SỬA TIN";
            string sqlTinDang = "select * from tb_TinDang where isnull(isHetHan,'False')='False' and idTinDang='" + idTinDang + "'";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            if (tbTinDang.Rows.Count > 0)
            {
                slLoaiDanhMuc.Value = tbTinDang.Rows[0]["idDanhMucCap1"].ToString();
                if (tbTinDang.Rows[0]["idDanhMucCap1"].ToString() != "")
                    slLoaiDanhMuc.Value = tbTinDang.Rows[0]["idDanhMucCap1"].ToString();

                if (tbTinDang.Rows[0]["idDanhMucCap2"].ToString() != "")
                    slLoaiDanhMucCap2.Value = tbTinDang.Rows[0]["idDanhMucCap2"].ToString();
                string chuoi_DanhMuc = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", tbTinDang.Rows[0]["idDanhMucCap1"].ToString());
                if (tbTinDang.Rows[0]["idDanhMucCap2"].ToString() != "")
                    chuoi_DanhMuc += "/" + StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", tbTinDang.Rows[0]["idDanhMucCap2"].ToString());
                txtLoaiDanhMuc.Value = chuoi_DanhMuc;
                txtTinhThanh.Value = tbTinDang.Rows[0]["idTinh"].ToString();
                txtQuanHuyen.Value = tbTinDang.Rows[0]["idHuyen"].ToString();
                txtPhuongXa.Value = tbTinDang.Rows[0]["idPhuongXa"].ToString();
                string TenTinhThanh = StaticData.getField("City", "ten", "id", tbTinDang.Rows[0]["idTinh"].ToString());
                string TenQuanHuyen = StaticData.getField("District", "ten", "id", tbTinDang.Rows[0]["idHuyen"].ToString());
                string TenPhuongXa = StaticData.getField("tb_PhuongXa", "ten", "id", tbTinDang.Rows[0]["idPhuongXa"].ToString());

                string chuoi_DiaDiem = "";
                if (TenPhuongXa != "")
                    chuoi_DiaDiem = TenPhuongXa + ", ";
                if (TenQuanHuyen != "")
                    chuoi_DiaDiem += TenQuanHuyen + ", ";
                if (TenTinhThanh != "")
                    chuoi_DiaDiem += TenTinhThanh;

                if (TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                    chuoi_DiaDiem = "Toàn quốc";
                txtDiaDiem.Value = chuoi_DiaDiem;

                if (tbTinDang.Rows[0]["TuGia"].ToString() != "")
                    txtTuGia.Value = double.Parse(KiemTraKhongNhap_LoadLen(tbTinDang.Rows[0]["TuGia"].ToString())).ToString("#,##");

                if (tbTinDang.Rows[0]["isGiaoHangMatPhi"].ToString() == "True")
                    rdGiaoHangMatPhi.Checked = true;
                else
                    rdGiaoHangKhongMatPhi.Checked = true;

                txtHoTen.Value = tbTinDang.Rows[0]["HoTen"].ToString();
                txtSoDienThoai.Value = tbTinDang.Rows[0]["SoDienThoai"].ToString();
                txtEmail.Value = tbTinDang.Rows[0]["Email"].ToString();
                txtDiaChi.Value = tbTinDang.Rows[0]["DiaChi"].ToString();
                txtTieuDe.Value = tbTinDang.Rows[0]["TieuDe"].ToString();
                txtNoiDung.Value = tbTinDang.Rows[0]["NoiDung"].ToString();
                if (tbTinDang.Rows[0]["LoaiTinDang"].ToString().Trim() == "CanBan")
                    radCanBan.Checked = true;
                else
                    radCanMua.Checked = true;

                string sqlHinhAnh = "select * from tb_HinhAnh where idTinDang='" + idTinDang + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string sHinhAnh = "";
                for (int i = 0; i < tbHinhAnh.Rows.Count; i++)
                {
                    sHinhAnh += tbHinhAnh.Rows[i]["UrlHinhAnh"].ToString().Trim() + "|~~~~|";
                }
                hdHinhAnh.Value = sHinhAnh;
                string htmlHinhAnh = "";
                string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                for (int i = 0; i < arrHinhAnh.Length; i++)
                {
                    if (arrHinhAnh[i].Trim() != "")
                    {
                        htmlHinhAnh += "<div id='dvHinhAnh_" + arrHinhAnh[i].Trim() + "' class='imgupload'>";
                        htmlHinhAnh += "<p style='margin:0px'><img src='../Images/td/slides/" + arrHinhAnh[i].Trim() + "' style='width: 150px;height: 110px;' /></p>";
                        htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + arrHinhAnh[i].Trim() + "\",\"" + arrHinhAnh[i].Trim() + "\")' src='../images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                        htmlHinhAnh += "</div>";
                    }
                }
                dvHinhAnh.InnerHtml = htmlHinhAnh;
            }
        }
    }
    string KiemTraKhongNhap(string Number)
    {
        try
        {
            Number.Trim();
        }
        catch
        {
            Number = "";
        }
        try
        {
            decimal a = decimal.Parse(Number);
        }
        catch
        {
            Number = "0";
        }
        if (Number == "")
        {
            Number = "0";
        }
        else if (Number == "-")
        {
            Number = "0";
        }
        else//bỏ dấu chấm 100.000  => 100000
        {
            try
            {
                Number = Number.Replace(",", "").Trim();
            }
            catch { }
        }
        return Number;
    }

    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    protected void btDangTin_Click(object sender, EventArgs e)
    {
        string idDanhMucCap1 = "";
        string idDanhMucCap2 = "";
        string idTinh = "";
        string idHuyen = "";
        string idPhuong = "";
        string TuGia = "";
        string DenGia = "";
        string isGiaoHangMatPhi = "True";
        string SoLuongTheoLoaiGia = "1";
        string MaLoaiGia = "";
        string HoTen = "";
        string SoDienThoai = "";
        string Email = "";
        string DiaChi = "";
        string TieuDe = "";
        string NoiDung = "";
        string LoaiTinDang = "CanMua";

        string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);

        //Danh mục cấp 1
        if (slLoaiDanhMuc.Value.Trim() != "" && slLoaiDanhMuc.Value.Trim() != "0")
        {
            idDanhMucCap1 = slLoaiDanhMuc.Value.Trim();
        }
        else
        {
            dvLinhVuc.InnerHtml = "Bạn chưa chọn danh mục!";
            return;
        }
        if (radCanBan.Checked)
            LoaiTinDang = "CanBan";

        if (slLoaiDanhMucCap2.Value.Trim() != "")
            idDanhMucCap2 = slLoaiDanhMucCap2.Value.Trim();
        //Tỉnh
        idTinh = txtTinhThanh.Value.Trim();
        //Huyện
        idHuyen = txtQuanHuyen.Value.Trim();
        //Phường
        idPhuong = txtPhuongXa.Value.Trim();
        //Từ giá
        TuGia = KiemTraKhongNhap(txtTuGia.Value.Trim());
        //Đến giá
        //DenGia = txtDenGia.Value.Trim().Replace(",", "").Replace(".", "");
        //Phí giao hàng
        isGiaoHangMatPhi = rdGiaoHangMatPhi.Checked.ToString();
        //Email
        Email = txtEmail.Value.Trim();
        //Địa chỉ
        DiaChi = txtDiaChi.Value.Trim();
        //Tiêu đề
        if (txtTieuDe.Value.Trim() != "")
        {
            TieuDe = txtTieuDe.Value.Trim();
            dvTieuDe.InnerHtml = "";
        }
        else
        {
            dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề cho bài viết!";
            return;
        }
        //Nội dung
        if (txtNoiDung.Value.Trim() != "")
        {
            NoiDung = txtNoiDung.Value.Trim();
            dvNoiDung.InnerHtml = "";
        }
        else
        {
            dvNoiDung.InnerHtml = "Bạn chưa nhập nội dung cho bài viết!";
            return;
        }
        //Hình ảnh nhỏ hơn 3 tấm
        if (arrHinhAnh.Length <= 3)
        {
            dvMessageHinhAnh.InnerHtml = "Bài đăng phải có 3 hình trở lên!";
            return;
        }
        else
            dvMessageHinhAnh.InnerHtml = "";


        if (idTinDang == "")
        {
            //Insert tin đăng
            string sqlInsertTD = "insert into tb_TinDang(idDanhMucCap1,idDanhMucCap2,idTinh,idHuyen,idPhuongXa,TuGia,DenGia,isGiaoHangMatPhi,SoLuongTheoLoaiGia,MaLoaiGia,HoTen,SoDienThoai,Email,DiaChi,TieuDe,NoiDung,idThanhVien,NgayDang,NgayDangLenTop,LoaiTinDang)";
            sqlInsertTD += " values(";
            if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
                sqlInsertTD += "'" + idDanhMucCap1 + "'";
            else
                sqlInsertTD += "null";
            if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
                sqlInsertTD += ",'" + idDanhMucCap2 + "'";
            else
                sqlInsertTD += ",null";
            if (idTinh != "" && idTinh != "0")
                sqlInsertTD += ",'" + idTinh + "'";
            else
                sqlInsertTD += ",null";
            if (idHuyen != "" && idHuyen != "0")
                sqlInsertTD += ",'" + idHuyen + "'";
            else
                sqlInsertTD += ",null";
            if (idPhuong != "" && idPhuong != "0")
                sqlInsertTD += ",'" + idPhuong + "'";
            else
                sqlInsertTD += ",null";
            if (TuGia != "")
                sqlInsertTD += ",'" + TuGia + "'";
            else
                sqlInsertTD += ",null";
            if (DenGia != "")
                sqlInsertTD += ",'" + DenGia + "'";
            else
                sqlInsertTD += ",null";
            sqlInsertTD += ",'" + isGiaoHangMatPhi + "'";
            if (SoLuongTheoLoaiGia != "")
                sqlInsertTD += ",'" + SoLuongTheoLoaiGia + "'";
            else
                sqlInsertTD += ",null";
            if (MaLoaiGia != "")
                sqlInsertTD += ",'" + MaLoaiGia + "'";
            else
                sqlInsertTD += ",null";
            sqlInsertTD += ",N'" + HoTen + "','" + SoDienThoai + "','" + Email + "',N'" + DiaChi + "',N'" + TieuDe + "',N'" + NoiDung + "'";
            if (idThanhVien != "")
                sqlInsertTD += ",'" + idThanhVien + "'";
            else
                sqlInsertTD += ",null";
            sqlInsertTD += ",'" + DateTime.Now.ToString() + "','" + DateTime.Now.ToString() + "','" + LoaiTinDang + "')";

            if (Connect.Exec(sqlInsertTD))
            {
                string idTinDangMoi = "";
                string sqlTinDangMoi = "select top 1 idTinDang from tb_TinDang where '1'='1'";
                if (idThanhVien != "")
                    sqlTinDangMoi += " and idThanhVien='" + idThanhVien + "'";
                sqlTinDangMoi += " order by idTinDang desc";
                DataTable tbTinDangMoi = Connect.GetTable(sqlTinDangMoi);
                if (tbTinDangMoi.Rows.Count > 0)
                    idTinDangMoi = tbTinDangMoi.Rows[0]["idTinDang"].ToString();
                for (int i = 0; i < arrHinhAnh.Length; i++)
                {
                    if (arrHinhAnh[i].Trim() != "")
                    {
                        string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTinDang,UrlHinhAnh) values('" + idTinDangMoi + "','" + arrHinhAnh[i].Trim().Replace("~/", "") + "')";
                        Connect.Exec(sqlInsertHinhAnh);
                    }
                }
                Connect.Exec("update tb_TinDang set MaTinDang='RV" + DateTime.Now.Year + idTinDangMoi + "' where idTinDang='" + idTinDangMoi + "' ");

                if (idThanhVien != "")
                    Response.Redirect("/thong-tin-ca-nhan/ttcn");
                else
                    Response.Redirect(Domain);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!')", true);
            }
        }
        else
        {
            //Sửa tin đăng
            string sqlUpdateTD = "update tb_TinDang set";

            if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
                sqlUpdateTD += " idDanhMucCap1='" + idDanhMucCap1 + "'";
            else
                sqlUpdateTD += "idDanhMucCap1=null";
            if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
                sqlUpdateTD += ", idDanhMucCap2='" + idDanhMucCap2 + "'";
            else
                sqlUpdateTD += ",idDanhMucCap2=null";
            if (idTinh != "" && idTinh != "0")
                sqlUpdateTD += ",idTinh='" + idTinh + "'";
            else
                sqlUpdateTD += ",idTinh=null";
            if (idHuyen != "" && idHuyen != "0")
                sqlUpdateTD += ",idHuyen='" + idHuyen + "'";
            else
                sqlUpdateTD += ",idHuyen=null";
            if (idPhuong != "" && idPhuong != "0")
                sqlUpdateTD += ",idPhuongXa='" + idPhuong + "'";
            else
                sqlUpdateTD += ",idPhuongXa=null";
            if (TuGia != "")
                sqlUpdateTD += ",TuGia='" + TuGia + "'";
            else
                sqlUpdateTD += ",TuGia=null";
            if (DenGia != "")
                sqlUpdateTD += ",DenGia='" + DenGia + "'";
            else
                sqlUpdateTD += ",DenGia=null";
            sqlUpdateTD += ",isGiaoHangMatPhi='" + isGiaoHangMatPhi + "'";
            if (SoLuongTheoLoaiGia != "")
                sqlUpdateTD += ",SoLuongTheoLoaiGia='" + SoLuongTheoLoaiGia + "'";
            else
                sqlUpdateTD += ",SoLuongTheoLoaiGia=null";
            if (MaLoaiGia != "" && MaLoaiGia != "0")
                sqlUpdateTD += ",MaLoaiGia='" + MaLoaiGia + "'";
            else
                sqlUpdateTD += ",MaLoaiGia=null";
            sqlUpdateTD += ",HoTen=N'" + HoTen + "'";
            sqlUpdateTD += ",SoDienThoai='" + SoDienThoai + "'";
            sqlUpdateTD += ",Email='" + Email + "'";
            sqlUpdateTD += ",DiaChi=N'" + DiaChi + "'";
            sqlUpdateTD += ",TieuDe=N'" + TieuDe + "'";
            sqlUpdateTD += ",NoiDung=N'" + NoiDung + "'";
            sqlUpdateTD += ",LoaiTinDang=N'" + LoaiTinDang + "'";
            sqlUpdateTD += ",isDuyet=Null";
            sqlUpdateTD += " where idTinDang='" + idTinDang + "'";

            if (Connect.Exec(sqlUpdateTD))
            {
                string sqlDeleteHinhAnh = "delete from tb_HinhAnh where idTinDang='" + idTinDang + "'";
                bool ktDeleteHinhAnh = Connect.Exec(sqlDeleteHinhAnh);
                if (ktDeleteHinhAnh)
                {
                    for (int i = 0; i < arrHinhAnh.Length; i++)
                    {
                        if (arrHinhAnh[i].Trim() != "")
                        {
                            string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTinDang,UrlHinhAnh) values('" + idTinDang + "','" + arrHinhAnh[i].Trim() + "')";
                            Connect.Exec(sqlInsertHinhAnh);
                        }
                    }
                }
                Response.Redirect("/thong-tin-ca-nhan/ttcn");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!')", true);
            }
        }
    }
}