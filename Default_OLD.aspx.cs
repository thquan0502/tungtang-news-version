﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    string Domain = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        if (Request.Cookies["TungTang_Login"] != null )
        {
            if(Request.Cookies["TungTang_Login"].Value.Trim() != "")
            {
                dvSubBanner.InnerHtml = "";
                dvSubBanner.Visible = false;
            }
        }
        if (!IsPostBack)
        {
            //LoadTinDangMoi();
            LoadDanhMuc();
            //LoadTinHot();
            LoadBannerTrangChu();
        }
    }
    private void LoadBannerTrangChu()
    {
        string sqlBNTC = "select * from tb_BannerTrangChu";
        DataTable tbBNTC = Connect.GetTable(sqlBNTC);
        if (tbBNTC.Rows.Count > 0)
        {
            string html = "";
            html += "<a href='" + tbBNTC.Rows[0]["Url"].ToString().Trim() + "' target='_blank'>";
            html += "   <img src='../" + tbBNTC.Rows[0]["LinkAnh"].ToString().Trim() + "' style='width:100%;height:100%;' />";
            html += "</a>";
            dvBannerTrangChu.InnerHtml = html;
        }
    }
    private void LoadDanhMuc()
    {
        string htmlDanhMuc = "";
        string sqlDanhMucCap1 = "select * from tb_DanhMucCap1 order by SoThuTu asc";
        DataTable tbDanhMucCap1 = Connect.GetTable(sqlDanhMucCap1);
        for (int i = 0; i < tbDanhMucCap1.Rows.Count; i++)
        {
            string TieuDe1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString().Trim()));
            string url1 = "/tin-dang/1-" + tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() + "-" + TieuDe1;
            htmlDanhMuc += "<a href='" + url1 + "'>";
            if (tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() == "14" || tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() == "16")//Bất động sản và Xe cộ
                htmlDanhMuc += "<div class='titlemain'>";
            else
                htmlDanhMuc += "<div class='titlemain2'>";



            htmlDanhMuc += "<img title='" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString() + "' src='" + tbDanhMucCap1.Rows[i]["LinkAnh"].ToString() + "' style='width:100%;height: 100%;object-fit: cover;' alt='" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString() + "'/>";
            htmlDanhMuc += "<p>" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString() + "</p>";
            htmlDanhMuc += " </div>";
            htmlDanhMuc += "</a>";
        }
        dvKhamPhaDanhMuc.InnerHtml = htmlDanhMuc;
    }

    protected void btXemThemTinDangMoi_Click(object sender, EventArgs e)
    {
        Response.Redirect(Domain + "/tin-dang/td");
    }
    protected void btXemThemTinKhac_Click(object sender, EventArgs e)
    {
        Response.Redirect(Domain + "/tin-dang/td");
    }
}