﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected string pageMode = null;
    protected string dataPhone = null;
    protected string dataPass = null;
    protected string dataAccountId = null;
    protected DateTime dataCreatedAt = DateTime.MinValue;
    protected string dataSmsPhone = null;
    protected string dataSmsCode = null;
    protected string dataSmsResponse = null;
    protected string dataSmsMessage = null;
    protected DateTime dataSmsSentAt = DateTime.MinValue;
    protected long diffSeconds = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        // get account id
        string accountId = Utilities.Account.getAuthAccountId(Request);
        if (!string.IsNullOrWhiteSpace(accountId))
        {
            Response.Redirect("/thong-tin-ca-nhan/ttcn");
        }

        // fetch data
        string sql = @"
            SELECT TOP 1
                ForgotPasswordData.*
            FROM ForgotPasswordData
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        DataTable table = Connect.GetTable(sql);
        if (table.Rows.Count > 0)
        {
            this.dataPhone = table.Rows[0]["Phone"].ToString();
            this.dataPass = table.Rows[0]["Pass"].ToString();
            this.dataAccountId = table.Rows[0]["AccountId"].ToString();
            if (!string.IsNullOrWhiteSpace(table.Rows[0]["CreatedAt"].ToString()))
            {
                this.dataCreatedAt = (DateTime)table.Rows[0]["CreatedAt"];
            }
            this.dataSmsPhone = table.Rows[0]["SmsPhone"].ToString();
            this.dataSmsCode = table.Rows[0]["SmsCode"].ToString();
            this.dataSmsResponse = table.Rows[0]["SmsResponse"].ToString();
            this.dataSmsMessage = table.Rows[0]["SmsMessage"].ToString();
            if (!string.IsNullOrWhiteSpace(table.Rows[0]["SmsSentAt"].ToString()))
            {
                this.dataSmsSentAt = (DateTime)table.Rows[0]["SmsSentAt"];
                this.diffSeconds = (long)(DateTime.Now - this.dataSmsSentAt).TotalSeconds;
            }
        }

        if (!IsPostBack)
        {
            // identify mode
            if (string.IsNullOrWhiteSpace(this.dataSmsCode))
            {
                this.pageMode = "INPUT";
            }
            else
            {
                string isState = Request.QueryString["IsState"];
                if (!string.IsNullOrWhiteSpace(isState) && isState == "1")
                {
                    this.pageMode = "VERIFICATION";
                }
                else
                {
                    this.pageMode = "EXPIRATION";
                }
            }

            // reset elements
            this.txtPass.Attributes["type"] = "password";
            this.txtPass2.Attributes["type"] = "password";
            this.divInput.Visible = false;
            this.divVerification.Visible = false;
            this.divExpiration.Visible = false;

            // process page
            switch (this.pageMode)
            {
                case "INPUT":
                    this.divInput.Visible = true;
                    break;
                case "VERIFICATION":
                    this.divVerification.Visible = true;
                    break;
                case "EXPIRATION":
                    this.divExpiration.Visible = true;
                    break;
                default:
                    Response.Redirect("/error/404");
                    break;
            }
        }
    }

    public void btnInput_Click(object sender, EventArgs e)
    {
        // get input values
        string inputPhone = this.txtPhone.Text.Trim();
        string inputPass = this.txtPass.Text.Trim();
        string inputPass2 = this.txtPass2.Text.Trim();

        // check required
        if (
            string.IsNullOrWhiteSpace(inputPhone)
            || string.IsNullOrWhiteSpace(inputPass)
            || string.IsNullOrWhiteSpace(inputPass2)
        )
        {
            Session["forgot_password_error"] = "Hãy điền đủ các thông tin";
            return;
        }

        // check phone number contains digits only
        if (!Utilities.Validation.isDigitsOnly(inputPhone))
        {
            Session["forgot_password_error"] = "Số điện thoại không đúng định dạng";
            return;
        }

        // check phone format
        if (!inputPhone.StartsWith("0"))
        {
            Session["forgot_password_error"] = "Số điện thoại không đúng định dạng";
            return;
        }

        // check phone number length
        if (inputPhone.Length > 10)
        {
            Session["forgot_password_error"] = "Số điện thoại không hợp lệ";
            return;
        }

        string accountId = Utilities.Account.getIdByPhone(inputPhone);
        // check account linked with phone number
        if (string.IsNullOrWhiteSpace(accountId))
        {
            Session["forgot_password_error"] = "Không có tài khoản liên kết với số điện thoại";
            return;
        }

        // check password contain accented characters
        if (Utilities.Validation.containAccented(inputPass))
        {
            Session["forgot_password_error"] = "Mật khẩu không được có dấu";
            return;
        }

        // check retype password
        if (inputPass != inputPass2)
        {
            Session["forgot_password_error"] = "Mật khẩu không khớp nhau";
            return;
        }

        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(inputPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // save db
        string sql = @"
            INSERT INTO ForgotPasswordData (
                SessionId,
                Phone,
                Pass,
                AccountId,
                CreatedAt,
                SmsPhone,
                SmsCode,
                SmsResponse,
                SmsMessage,
                SmsSentAt
            ) VALUES (
                '" + Session.SessionID + @"',
                '" + inputPhone + @"',
                '" + inputPass + @"',
                '" + accountId + @"',
                '" + sNow + @"',
                '" + inputPhone + @"',
                '" + code + @"',
                '" + res + @"',
                '" + message + @"',
                '" + sNow + @"'
            )
        ";
        Connect.Exec(sql);

        Response.Redirect("/tai-khoan/quen-mat-khau?IsState=1");
    }

    public void btnVerify_Click(object sender, EventArgs e)
    {
        // get input values
        string inputCode = this.txtCode.Text.Trim();

        // check code
        if (inputCode != dataSmsCode)
        {
            Session["forgot_password_errors"] = "Mã xác thực không chính xác";
            return;
        }
       

        // remove forgot password data
        string sql = @"
            DELETE FROM ForgotPasswordData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // update password
        string sql2 = @"
            UPDATE tb_ThanhVien
            SET
                MatKhau = '"+ dataPass +@"'
            WHERE idThanhVien = '"+ dataAccountId +@"'
        ";
        Connect.Exec(sql2);

        // login
        Session["success"] = "Mật khẩu đã thay đổi thành công. Hãy đăng nhập lại";
        Response.Redirect("/dang-nhap/dn");
    }

    public void btnResend_Click(object sender, EventArgs e)
    {
        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(dataSmsPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // data forgot password data
        string sql = @"
            UPDATE ForgotPasswordData
            SET
                SmsCode = '" + code + @"',
                SmsResponse = '" + res + @"',
                SmsMessage = '" + message + @"',
                SmsSentAt = '" + sNow + @"'
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // end
        Response.Redirect("/tai-khoan/quen-mat-khau?IsState=1");
    }

    public void btnReinput_Click(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM ForgotPasswordData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        Response.Redirect("/tai-khoan/quen-mat-khau");
    }

    public void btnReverify_Click(object sender, EventArgs e)
    {
        // send sms
        Random rand = new Random();
        string code = "" + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        string message = "[TUNGTANG]Tungtang.com.vn - Ma OTP de xac thuc tai khoan cua ban la: " + code;
        string res = (new AppSMS.SMSService()).send(dataSmsPhone, message);
        string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

        // data forgot password data
        string sql = @"
            UPDATE ForgotPasswordData
            SET
                SmsCode = '" + code + @"',
                SmsResponse = '" + res + @"',
                SmsMessage = '" + message + @"',
                SmsSentAt = '" + sNow + @"'
            WHERE SessionId = '" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);

        // end
        Response.Redirect("/tai-khoan/quen-mat-khau?IsState=1");
    }

    public void btnReinput2_Click(object sender, EventArgs e)
    {
        string sql = @"
            DELETE FROM ForgotPasswordData WHERE SessionId='" + Session.SessionID + @"'
        ";
        Connect.Exec(sql);
        Response.Redirect("/tai-khoan/quen-mat-khau");
    }
}