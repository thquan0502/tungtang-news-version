﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="CapNhatPro.aspx.cs" Inherits="DangKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="../Css/Page/TinDang.css" rel="stylesheet" /> 
    <link href="../Css/Page/DangKyLan2.css" rel="stylesheet" /> 
    <script src="../Js/Page/DangKyLan2.min.js"></script>
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
        } 
        .img-cmnd{
            border-radius: 10px;
        }
       .bg-alert{
           background-color:#fcf8e3;
       }
    </style>
    <script>
        window.onload = function () {  
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px;margin-top: 40px;">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div id="vnw-log-in" class="container main-content" style="margin-top:33px;">
						<input type="hidden" name="" value="0" runat="server" id="DaThayDoi_imgHinhAnhavata" />
                        <input type="hidden" name="" value="0" runat="server" id="DaThayDoi_imgHinhAnh" />
                        <input type="hidden" name="" value="0" runat="server" id="DaThayDoi_imgHinhAnhA" />
                        <div class="col-md-12">
                            <h5><strong>CẬP NHẬT USER PRO</strong></h5>
                            <hr />
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-md-push-4" style="border-radius: 5px;">
                                            <div class="p-3 mb-1 bg-success" style="border-radius: 5px;">
                                                <p><i class="fa fa-lightbulb-o" style="font-size: 150%;"></i> <i><strong>Lợi ích</strong> khi bạn nâng cấp lên <strong>Thành Viên Pro</strong>:</i></p>
                                                <p><i> - <strong>Tin đăng</strong> của bạn sẽ được lan tỏa rộng rãi và tốt hơn, tới được nhiều <strong>Khách Hàng</strong> tiềm năng đang có nhu cầu mua <strong>sản phẩm</strong> của bạn bằng sự hỗ trợ của các <strong>Cộng tác viên(CTV)</strong> (nếu bạn thêm <strong>Hoa hồng</strong>) để tạo động lực họ chia sẻ trên các trang <strong>Mạng xã hội</strong>.</i></p>
                                                <p><i> - <strong>Tin đăng</strong> của bạn sẽ được nằm ở mục <strong>Tin ưu tiên</strong> và ở <strong>Tin đăng chia sẻ</strong>, là những vị trí rất thuận lợi để các CTV vào dễ thấy nhất và lấy <strong>Sản phẩm</strong> của bạn để đi quảng bá với <strong>Khách hàng</strong>. </i></p>
                                            </div>
                                        </div>
                                <div class="col-md-8 col-sm-8 col-md-push-4" style="border-radius: 5px;margin-top:3px;">
                                            <div class="p-3 mb-1 bg-alert" style="border-radius: 5px;">
                                                <p><i class="fa fa-exclamation-circle" style="font-size: 150%;"></i> <i><strong>Trách nhiệm</strong> của bạn khi nâng cấp lên <strong>Thành Viên Pro</strong>:</i></p>
                                                <p><i> - <strong>Đảm bảo</strong> các <strong>thông tin</strong> của bạn đăng lên hoàn toàn là <strong>chính xác</strong>, vì để <strong>CTV</strong> sẽ chủ động liên hệ cho bạn khi có <strong>Khách hàng</strong> hỏi mua <strong>Sản phẩm</strong> đó hoặc có thể điện hỏi chi tiết về <strong>Sản phẩm</strong> mà bạn đăng mời chào.</i></p>
                                                 <p><i> - Khi <strong>Đăng tin</strong>  thì <strong>nội dung chi tiết</strong> của <strong>Sản phẩm</strong> đó phải chính xác so với thực tế và không vi phạm các <strong>Sản phẩm cấm</strong> của <strong>Pháp luật</strong>, tiền <strong>Hoa hồng</strong> bạn trích cho <strong>CTV</strong> phải chuẩn và <strong>thanh toán</strong> cho <strong>CTV</strong> khi đơn hàng được <strong>giao dịch</strong> thành công nhờ sự giới thiệu và chia sẻ <strong>tin đăng</strong> của <strong>CTV</strong> đó.</i></p>
                                            </div>
                                        </div>
                                <div class="col-md-8 col-sm-8 col-md-push-4">
									<div class="avtUser">	
                                        <div class="form-group" style="position: relative;">	
                                            <asp:FileUpload ID="fuHinhDaiDienavata" runat="server" accept=".jpg,.jpeg,.png" Style="display: none;" onchange="UploadHinhAnhavata_Onchange(this,'imgLinkAnhavata')" />	
                                            <img id="imgLinkAnhavata" runat="server" src="/images/icons/signin.png" class="imgUserEdit" style="width:200px;height:190px"/>	
                                            <div class="btnShowFileUpLoad1" title="Đổi ảnh đại diện" onclick="document.getElementById('ContentPlaceHolder1_fuHinhDaiDienavata').click();"><i class="fa fa-camera"></i></div>	
                                        </div>	
                                    </div>	
                                </div>	
                                <div class="col-md-8 col-sm-8 col-md-push-4">
                                    <div class="infoUser">
                                        <div class="form-group">
                                            <p style="font-style: italic">(<span style="color: red">*</span>): Thông tin bắt buộc phải nhập</p>
                                        </div>
                                         <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Họ tên (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtTenCuaHang" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                            <p id="MessageHoTen" style="color: red; font-size: 12px;" runat="server"></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Email(<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtEmail" name="form[username]" tabindex="1" class="form-control" runat="server" />
                                            <p id="MessageEmail" style="color: red; font-size: 12px;"  runat="server"></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Địa chỉ(<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtDiaChi" name="form[username]" placeholder="Số nhà, tên Đường" tabindex="1" class="form-control" runat="server" />
                                             <p id="MessageDiaChi" style="color: red; font-size: 12px;" runat="server"></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Phường(Xã), Quận(Huyện), Thành Phố(Tỉnh) (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtDiaDiem" name="form[username]" placeholder="Chọn" tabindex="1" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDiaDiem();" />
                                            <input type="hidden" id="txtDiaDiemID" runat="server" hidden disabled />
                                            <p id="MessageDiaDiem" style="color: red; font-size: 12px;" runat="server"></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Số điện thoại (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtSoDienThoai" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                             <p id="MessageSoDienThoai" runat="server" style="color: red; font-size: 12px;"></p>
                                        </div>
                                         
                                        <%-- update--%>
                                        <%-- <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Mã số thuế:</b></div>
                                            <input  id="txtMST" name="form[username]"  placeholder="Mã số thuế" tabindex="1" class="form-control" runat="server" />
                                              <div id="MessMST" style="color: red; font-size: 12px; " runat="server"></div>
                                        </div>--%>
                                        <%-- <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Ngày cấp(ngày/tháng/năm):</b></div>
                                            <input id="txtNgayCap"  value="01/01/1990"  type="date"  placeholder="Ngày cấp" tabindex="1" class="form-control" runat="server" />
                                        </div>--%>
                                       <%-- <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Nơi cấp:</b></div>
                                            <input id="txtNoiCap" name="form[username]"  placeholder="Nơi cấp" tabindex="1" class="form-control" runat="server" />
                                        </div>--%>
                                        <%-- <div class="form-group" id="divPass" runat="server">
                                            <div class="titleinput" style="padding-top: 10px"><b>Mật khẩu:(<span style="color: red">*</span>):</b></div>
                                            <input id="txtMatKhau" name="form[username]"  placeholder="Mật khẩu" tabindex="1" class="form-control" runat="server" />
                                        </div>--%>
                                          <%-- update--%>
                                        <div class="form-group"  style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Tên đăng nhập (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtTenDangNhap" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                        </div>

                                        <div class="form-group" style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Tỉnh thành (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtTinhThanh" name="name" value="" runat="server" hidden />
                                            <asp:DropDownList ID="ddlTinh" class="form-control" runat="server" disabled="disabled" Style="padding-left: 0; padding-right: 0;"></asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Quận huyện (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" hidden />
                                            <asp:DropDownList data-search-input-placeholder="Tìm kiếm quận huyện" data-placeholder="Tất cả" class="form-control" ID="slHuyen" name="industry[]" runat="server" disabled="disabled" Style="padding-left: 0; padding-right: 0;">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Phường xã (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtPhuongXa" name="name" value="" runat="server" hidden />
                                            <select data-search-input-placeholder="Tìm kiếm phường xã" data-placeholder="Tất cả" class="form-control" id="slXa" name="industry[]" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0;">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Số CMND/CCCD(<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtCMND" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                            <p id="MessageCMND" style="color: red; font-size: 12px;" runat="server"></p>
                                        </div>
                                         
                                    </div>
                                </div>
                            
                                <div class="col-md-8 col-sm-8 col-md-push-4" style="margin-top:10px;">
                                     <div class="titleinput" style="padding-top: 10px;margin-bottom:5px;"><b>Cập nhật hình ảnh CMND/CCCD:(<span style="color: red">*</span>):</b></div>
                                    <div class="col-md-12 col-sm-6">
                                    
                                    <div class="avtUser">
                                        <div class="form-group" style="position: relative;">  
                                            <div class="titleinput" style="padding-top: 10px"><b>Hình ảnh CMND Mặt trước(<span style="color: red">*</span>):</b></div>               
                                        </div>
                                        <div class="form-group">
                                            <asp:FileUpload ID="fuHinhDaiDien" runat="server" accept=".jpg,.jpeg,.png" Style="display: none;" onchange="UploadHinhAnh_Onchange(this,'imgLinkAnh')" />
                                            <img id="imgLinkAnh" runat="server" src="/images/icons/signin.png" class="imgUserEdit img-cmnd" />
                                            <div class="btnShowFileUpLoad" title="Đổi ảnh CMND mặt trước" onclick="document.getElementById('ContentPlaceHolder1_fuHinhDaiDien').click();"><i class="fa fa-camera"></i></div>
                                        </div>
                                        <div class="form-group">
                                             <p id="MessHeadCMND" style="color: red; font-size: 12px;" runat="server"></p>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="col-md-12 col-sm-6" style="margin-top:10px;">
                                   
                                    <div class="avtUser">
                                        <div class="form-group" style="position: relative;">
                                             <div class="titleinput" style="padding-top: 10px"><b>Hình ảnh CMND Mặt sau:(<span style="color: red">*</span>):</b></div> 
                                        </div>
                                        <div class="form-group">
                                             <asp:FileUpload ID="fuHinhDaiDienA" runat="server" accept=".jpg,.jpeg,.png" Style="display: none;" onchange="UploadHinhAnh_OnchangeA(this,'imgLinkAnhA')" />
                                            <img id="imgLinkAnhA" runat="server" src="/images/icons/signin.png" class="imgUserEdit img-cmnd" />
                                            <div class="btnShowFileUpLoad" title="Đổi ảnh CMND mặt sau" onclick="document.getElementById('ContentPlaceHolder1_fuHinhDaiDienA').click();"><i class="fa fa-camera"></i></div>
                                        </div>
                                        <div class="form-group">
                                        <p id="MessFooterdCMND" style="color: red; font-size: 12px;" runat="server"></p>
                                         </div>
                                    </div>
                                        
                                    </div>
                                </div>
                                    <div class="col-md-8 col-sm-8 col-md-push-4" style="margin-top:20px;">
                                         <p class="line">
                                            <input type="checkbox" id="chbxPrivacy" runat="server"/> Tôi đã đọc và đồng ý với các <a href="/mo-ta/dieu-khoan-va-dich-vu" target="_blank">điều khoản</a> và <a href="/mo-ta/chinh-sach-bao-mat-thong-tin"  target="_blank">chính sách bảo mật</a>.
                                        </p>
                                        <p id="MesDieuKhoan" style="color: red; font-size: 12px;" runat="server"></p>
                                    </div>
                                </div>
                            <div class="col-md-8 col-sm-8 col-md-push-4">
                            <div class="form-group" style="margin-top: 20px">
                                            <div class="row">
                                            <div class="col-6">
                                            <a class="btn btn-primary btn-block" onclick="LuuThongTin()">LƯU THAY ĐỔI</a>
                                            <asp:LinkButton ID="btnDangKy" runat="server" class="btn btn-primary btn-block" OnClick="btnDangKy_Click" Style="display: none;">LƯU THAY ĐỔI</asp:LinkButton>
                                            </div>
                                            <div class="col-6">
                                                 <a class="btn btn btn-success btn-block" href="/thong-tin-ca-nhan/ttcn">TRỞ VỀ</a>
                                            </div>
                                            </div>
                                        </div>
                            </div>
                          </div>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
           
        </div>
         <%--Row--%>

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </section>
	<script>
            function alertTT(){
                alertify.success("Đăng ký Thành viên Pro thành công.Đăng ký sẽ được Admin Tung Tăng duyệt và phản hồi trong 24h. Xin cảm ơn!");                                                 
            }
            function alertSDT() {
                alertify.error("Bạn cần cập nhật thông tin số điện thoại!");
            }
    </script>
    <script type="text/javascript">
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
		function UploadHinhAnhavata_Onchange(input, iddd) {	
            if (input.files && input.files[0]) {	
                var reader = new FileReader();	
                reader.onload = function (e) {	
                    $('#ContentPlaceHolder1_' + iddd)	
                        .attr('src', e.target.result);	
                };	
                $("#ContentPlaceHolder1_DaThayDoi_imgHinhAnhavata").val('1');	
                reader.readAsDataURL(input.files[0]);	
            }	
        }
        function UploadHinhAnh_Onchange(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                $("#ContentPlaceHolder1_DaThayDoi_imgHinhAnh").val('1');
                reader.readAsDataURL(input.files[0]);
            }
        }
        function UploadHinhAnh_OnchangeA(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                $("#ContentPlaceHolder1_DaThayDoi_imgHinhAnhA").val('1');
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

</asp:Content>
