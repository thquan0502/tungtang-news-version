﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;
using System.Activities.Expressions;
using System.Text;

public partial class BalanceHistory : System.Web.UI.Page
{
    protected DataRow authAccount = null;
    protected string rechargeSuccessMessage = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.authAccount = Utilities.Auth.getAccount();
        if(this.authAccount == null)
        {
            Utilities.Redirect.notFound();
        }
        if(!this.IsPostBack)
        {
            this.checkRechargeSuccess();
            this.loadThongTinThanhVien();
            this.loadTable();
        }
    }

    protected void checkRechargeSuccess()
    {
        HttpCookie c = Request.Cookies.Get("recharge_success");
        if (c != null)
        {
            string idHistory = c.Value;
            long id;
            if(long.TryParse(idHistory, out id))
            {
                DataRow row = Services.BalanceHistory.find(id);
                long amount = row.Field<long>("Amount");
                this.rechargeSuccessMessage = "Bạn đã nạp " + Utilities.Formatter.toCurrencyString(amount) + " thành công !";
            }
            Response.Cookies["recharge_success"].Expires = DateTime.Now.AddSeconds(-1);
        }
        else
        {
            this.rechargeSuccessMessage = "";
        }
    }

    protected void loadThongTinThanhVien()
    {
            txtNameShopID.InnerHtml = "<h4>" + (this.authAccount["TenCuaHang"].ToString().Trim() == "" ? "Chưa cung cấp" : this.authAccount["TenCuaHang"]) + "</h4>";
            txtInfoAccountCode.InnerText = "Mã thành viên: " + this.authAccount["Code"].ToString().Trim();
            txtLoaiUser.InnerText = "Bình Thường"; 
            if (this.authAccount["StsPro"].ToString().Trim() == "1")
            {
                txtLoaiUser.InnerText = "Thành Viên PRO";
                buttonUser.Visible = false;
                ImagePro.Style.Add("display", "");
            }
            string htmluser = "";
            if (this.authAccount["LyDo_KhongDuyet"].ToString().Trim() != "")
            {
                htmluser += "<div style='background-color:#fcf8e3;border-radius:10px;padding: 0 11px;'><p>Nội dung từ chối:" + this.authAccount["LyDo_KhongDuyet"].ToString().Trim() + "</p></div>";
                htmluser += "<a class='MainFunctionButton EditProfile' id='button_Forwad_mobi' style='background-color:#4cb050;color:white;margin-bottom:10px;height: 34px;' href='/thong-tin/pro'>Đăng ký lại</a>";
                ProNow.InnerHtml = htmluser;
                buttonUser.Visible = false;
                ImagePro.Visible = false;
            }
            if (this.authAccount["StsPro"].ToString().Trim() == "0" && this.authAccount["idAdmin_Duyet"].ToString().Trim() == "-1")
            {
                txtLoaiUser.InnerText = "Thành viên Pro đang chờ duyệt";
                ImagePro.Visible = false;
                buttonUser.Visible = false;
            }

            txtShopEmailID.InnerHtml = (this.authAccount["Email"].ToString().Trim() == "" ? "Chưa cung cấp" : this.authAccount["Email"].ToString());
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + this.authAccount["LinkAnh"].ToString().Trim()))
            {

                imgLinkAnh.Src = "/images/user/" + this.authAccount["LinkAnh"].ToString().Trim();
            }
            else 
            {
                imgLinkAnh.Src = this.authAccount["LinkAnh"].ToString().Trim();
            }
                
            txtShopPhoneID.InnerHtml = (this.authAccount["SoDienThoai"].ToString().Trim() == "" ? "Chưa cung cấp" : this.authAccount["SoDienThoai"].ToString());
            txtDateShopApprovedID.InnerHtml = (this.authAccount["NgayDangKy"].ToString().Trim() == "" ? "Chưa cung cấp" : DateTime.Parse(this.authAccount["NgayDangKy"].ToString()).ToString("dd-MM-yyyy"));

            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + this.authAccount["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + this.authAccount["linkAnh"].ToString().Trim();
            string TenTinhThanh = StaticData.getField("City", "Ten", "id", this.authAccount["idTinh"].ToString());
            string TenQuanHuyen = StaticData.getField("District", "Ten", "id", this.authAccount["idHuyen"].ToString());
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", this.authAccount["idPhuongXa"].ToString());
            string DiaChi = this.authAccount["DiaChi"].ToString().Trim();
            if (DiaChi == "" && TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                txtShopAddressID.InnerHtml = "Chưa cung cấp";
            else
            {
                txtShopAddressID.InnerHtml = DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh;
                txtShopAddressID.Attributes.Add("title", DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh);
            }

            string idThanhVien_Cookie = null;
            try
            {
                idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
            }
            catch { }
            if (this.authAccount["idThanhVien"].ToString() != idThanhVien_Cookie)
            {
                TitleHeader.InnerText = "TIN CÙNG NGƯỜI ĐĂNG";
                ProNow.Visible = false;
                if (this.authAccount["StsPro"].ToString().Trim() == "1")
                {
                    txtLoaiUser.InnerText = "Thành Viên PRO";
                    ImagePro.Style.Add("display", "");
                }
                else
                {
                    txtLoaiUser.InnerText = "Bình Thường";
                    ImagePro.Style.Add("display", "");
                }
                btnTin.Visible = false;
            }
    }

    protected void loadTable()
    {
        string accountId = Utilities.Auth.getAccountId();
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();

        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 += @"
            FROM (
                SELECT 
                    ROW_NUMBER() OVER ( 
                        ORDER BY BalanceHistory.CreatedAt DESC
                    ) AS RowNumber,
	                ISNULL(BalanceHistory.Id, '') AS Id,
	                ISNULL(BalanceHistory.Type, '') AS Type,
	                ISNULL(TBAccounts.SoDienThoai, '') AS AccountPhone,
	                ISNULL(BalanceHistory.Amount, 0) AS Amount,
	                ISNULL(BalanceHistory.RechargeMethod, '') AS RechargeMethod,
	                ISNULL(BalanceHistory.SpentType, '') AS SpentType,
	                ISNULL(BalanceHistory.CreatedAt, '') AS CreatedAt,
	                ISNULL(BalanceHistory.FeeCode, '') AS FeeCode
                FROM BalanceHistory
                    INNER JOIN tb_ThanhVien TBAccounts
	                    ON TBAccounts.idThanhVien = BalanceHistory.AccountId
                WHERE TBAccounts.idThanhVien = " + accountId + @"
        ";
        sql1 += @"
            ) TBWrap
        ";
        sql2 += " SELECT COUNT(*) AS TOTALROWS " + sql1;
        sql3 += @" SELECT * " + sql1 + @" WHERE RowNumber BETWEEN " + fromIndex + @" AND " + toIndex + @" ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TOTALROWS"].ToString());

        DataTable table3 = Connect.GetTable(sql3);

        html = @"
            <table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                <thead >
                    <tr>
                        <th class='th'>Loại giao dịch</th>
                        <th class='th'>Số tiền</th>
                        <th class='th'>Chi tiết</th>
                        <th class='th'>Thời gian</th>
                    </tr>
                </thead>
                <tbody>
        ";

        if (table3.Rows.Count == 0)
        {
            html += "<tr><td colspan='4' style='text-align: center;'>Không có kết quả</td></tr>";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string type = "";
            string detail = "";
            string amount = Utilities.Formatter.toCurrencyString(table3.Rows[i]["Amount"].ToString());
            if (table3.Rows[i]["Type"].ToString() == Utilities.BalanceHistory.TYPE_RECHARGE)
            {
                type = "Nạp tiền";
                amount = "+ " + amount;
                string rechargeMethod = table3.Rows[i]["RechargeMethod"].ToString();
                detail = "Phương thức: ";
                if (rechargeMethod == Utilities.BalanceHistory.RECHARGE_METHOD_VNPAY)
                {
                    detail += "VNPay";
                } else if (rechargeMethod == Utilities.BalanceHistory.RECHARGE_METHOD_TRANSFER)
                {
                    detail += "Chuyển khoản";
                }
            }
            if (table3.Rows[i]["Type"].ToString() == Utilities.BalanceHistory.TYPE_SPENT)
            {
                type = "Tiêu dùng";
                amount = "- " + amount;
                string spentType = table3.Rows[i]["SpentType"].ToString();
                detail = "Nội dung: ";
                string feeCode = table3.Rows[i]["FeeCode"].ToString();
                if(spentType == Utilities.BalanceHistory.SPENT_TYPE_ADD_POST) {
                    detail += "Đăng tin";
                    if (!string.IsNullOrWhiteSpace(feeCode))
                    {
                        detail += ", " + Utilities.PostFee.getPostCodeHuman(feeCode);
                    }
                }else if(spentType == Utilities.BalanceHistory.SPENT_TYPE_UPGRADE_POST) {
                    detail += "Nâng cấp tin";
                    if (!string.IsNullOrWhiteSpace(feeCode))
                    {
                        detail += ", " + Utilities.PostFee.getPostCodeHuman(feeCode);
                    }
                }
                else if(spentType == Utilities.BalanceHistory.SPENT_TYPE_EXTEND_POST) {
                    detail += "Gia hạn tin";
                    if (!string.IsNullOrWhiteSpace(feeCode))
                    {
                        detail += ", " + Utilities.PostFee.getPostCodeHuman(feeCode);
                    }
                }
                else if(spentType == Utilities.BalanceHistory.SPENT_TYPE_TO_TOP) {
                    detail += "Lên TOP tin đăng";
                }
            }

            html += @"
                <tr>
                   <td>" + type + @"</td>
                   <td>" + amount + @"</td>
                   <td>" + detail + @"</td>
                   <td>" + ((DateTime) table3.Rows[i]["CreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                </tr>
            ";
        }
        html += "</tbody>";
        html += "</table>";
        divTable.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "/tai-khoan/lich-su-giao-dich", paginationParams);
        divPagination1.InnerHtml = htmlPagination;
        divPagination2.InnerHtml = htmlPagination;
    }
}