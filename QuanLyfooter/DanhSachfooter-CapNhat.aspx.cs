﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLyTinTuc_QuanLyTinTuc_CapNhat : System.Web.UI.Page
{
    string idTinTuc = "";
    protected void FileBrowser1_Load(object sender, EventArgs e)
    {
        FileBrowser1 = new CKFinder.FileBrowser();
        FileBrowser1.BasePath = "/ckfinder/";
        FileBrowser1.SetupCKEditor(txtNoiDung);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {
            //Đăng tin không cần đăng nhập
            Response.Redirect("../Home/DangNhap.aspx");
        }
        else
        {
            //idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        try
        {
            idTinTuc = Request.QueryString["idTinTuc"].Trim();
        }
        catch { }
        if (!IsPostBack)
        {
            LoadTinTuc();
          

        }
        //if (IsPostBack && fileHinhAnh.PostedFile != null)
        //{
        //    if (Request.Files.Count > 10)
        //    {
        //        Response.Write("<script>alert('Bạn vui lòng chọn tối đa 10 ảnh!')</script>");
        //        return;
        //    }
        //    for (int j = 0; j < Request.Files.Count; j++)
        //    {
        //        HttpPostedFile file = Request.Files[j];
        //        if (file.ContentLength > 0)
        //        {
        //            //if (fileHinhAnh.PostedFile.FileName.Length > 0)
        //            //{
        //            string extension = Path.GetExtension(file.FileName);
        //            if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
        //            {
        //                if (fileHinhAnh.HasFile)
        //                {
        //                    string Ngay = DateTime.Now.Day.ToString();
        //                    string Thang = DateTime.Now.Month.ToString();
        //                    string Nam = DateTime.Now.Year.ToString();
        //                    string Gio = DateTime.Now.Hour.ToString();
        //                    string Phut = DateTime.Now.Minute.ToString();
        //                    string Giay = DateTime.Now.Second.ToString();
        //                    string Khac = DateTime.Now.Ticks.ToString();
        //                    string fExtension = Path.GetExtension(file.FileName);

        //                    string sqlIdTinDang = "select top 1 idTinTuc from tb_TinTuc order by idTinTuc desc";
        //                    DataTable tbIdTinDang = Connect.GetTable(sqlIdTinDang);
        //                    string idTinDang = "0";
        //                    if (tbIdTinDang.Rows.Count > 0)
        //                        idTinDang = (float.Parse(tbIdTinDang.Rows[0]["idTinTuc"].ToString()) + 1).ToString();
        //                    string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + idTinDang + fExtension;
        //                    string FilePath = "/Images/news/" + FileName;
        //                    StaticData.LoadWaterMark(file, Server.MapPath(FilePath));
        //                    //file.SaveAs(Server.MapPath(FilePath));

        //                    hdHinhAnh.Value = hdHinhAnh.Value + FileName;
        //                    string htmlHinhAnh = "<div id='dvHinhAnh_" + hdHinhAnh.Value.Trim() + "' class='imgupload' style='width:150px'>";
        //                    htmlHinhAnh += "<p style='margin:0px'><img src='/Images/news/" + hdHinhAnh.Value.Trim() + "' style='width: 150px;height: 110px;' /></p>";
        //                    htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + hdHinhAnh.Value.Trim() + "\",\"" + hdHinhAnh.Value.Trim() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
        //                    htmlHinhAnh += "</div>";
        //                    dvHinhAnh.InnerHtml = htmlHinhAnh;
        //                    //imgAnhCuaBan.Src = FilePath;
        //                }
        //            }
        //            else
        //            {
        //                Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
        //                return;
        //            }
        //        }
        //    }
        //    //Response.Write("<script>window.scrollTo(0, document.body.scrollHeight);</script>");
        //}

    }
    private void LoadTinTuc()
    {
        
            btDangTin.Text = "CẬP NHẬT";
            string sqlTinDang = "select ShortText,Discription from [tb_footer] WHERE [idfooter]='" + idTinTuc + "' ";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            if (tbTinDang.Rows.Count > 0)
            {
                //txtTieuDe.Value = tbTinDang.Rows[0]["TieuDe"].ToString();
                //txtTieuDeSeo.Value = tbTinDang.Rows[0]["TieuDeSeo"].ToString();
                //txtTieuDeSeo.Disabled = true;
                txtMoTaNgan.Value = tbTinDang.Rows[0]["ShortText"].ToString();
                txtNoiDung.Text = tbTinDang.Rows[0]["Discription"].ToString();
                //if (tbTinDang.Rows[0]["isHot"].ToString() == "" || tbTinDang.Rows[0]["isHot"].ToString() == "False")
                //    ckisHot.Checked = false;
                //else
                //    ckisHot.Checked = true;
                //if (tbTinDang.Rows[0]["KichHoat"].ToString() == "" || tbTinDang.Rows[0]["KichHoat"].ToString() == "False")
                //    ckKichHoat.Checked = false;
                //else
                //    ckKichHoat.Checked = true;

                
                //string htmlHinhAnh = "";
                //htmlHinhAnh += "<div id='dvHinhAnh_" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "' class='imgupload'>";
                //htmlHinhAnh += "<p style='margin:0px'><img src='/Images/news/" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "' style='width: 150px;height: 110px;' /></p>";
                //htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "\",\"" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                //htmlHinhAnh += "</div>";
               // dvHinhAnh.InnerHtml = htmlHinhAnh;
            
        }
    }
    protected void btDangTin_Click(object sender, EventArgs e)
    {
        //string TieuDe = "";
        //string TieuDeSeo = "";
        //string MoTaNgan = "";
        //string NoiDung = "";
        //string isHot = "False";
        //string KichHoat = "False";
        //string HinhAnh = hdHinhAnh.Value.Trim();
        ////Tiêu đề
        //if (txtTieuDe.Value.Trim() != "")
        //{
        //    TieuDe = txtTieuDe.Value.Trim();
        //    dvTieuDe.InnerHtml = "";
        //}
        //else
        //{
        //    Response.Write("<script>alert('Bạn chưa nhập tiêu đề!');</script>");
        //    dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề!";
        //    return;
        //}
        ////Tiêu đề seo
        //if (txtTieuDeSeo.Value.Trim() != "")
        //{
        //    TieuDeSeo = txtTieuDeSeo.Value.Trim();
        //    dvTieuDeSeo.InnerHtml = "";
        //}
        //else
        //{
        //    Response.Write("<script>alert('Bạn chưa nhập tiêu đề seo!');</script>");
        //    dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề seo!";
        //    return;
        //}
        //MoTaNgan = txtMoTaNgan.Value.Trim();
        //if (ckisHot.Checked)
        //    isHot = "True";
        //if (ckKichHoat.Checked)
        //    KichHoat = "True";
        ////Nội dung
        //if (txtNoiDung.Text.Trim() != "")
        //{
        //    NoiDung = txtNoiDung.Text.Trim();
        //    dvNoiDung.InnerHtml = "";
        //}
        //else
        //{
        //    Response.Write("<script>alert('Bạn chưa nhập nội dung!');</script>");
        //    dvNoiDung.InnerHtml = "Bạn chưa nhập nội dung!";
        //    return;
        //}
        //if (idTinTuc == "")
        //{
        //    //Insert tin đăng
        //    string sqlInsertTD = "insert into tb_TinTuc(TieuDe,TieuDeSeo,MoTaNgan,MoTa,AnhDaiDien,isHot,KichHoat,NgayDang)";
        //    sqlInsertTD += " values(N'" + TieuDe + "'";
        //    sqlInsertTD += ",N'" + TieuDeSeo + "',N'" + MoTaNgan + "',N'" + NoiDung + "','" + HinhAnh + "','" + isHot + "','" + KichHoat + "','" + DateTime.Now + "')";
        //    bool ktInsertTD = Connect.Exec(sqlInsertTD);
        //    if (ktInsertTD)
        //    {
        //        Response.Redirect("DanhSachTinTuc.aspx");
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!');</script>");
        //    }
        //}
        //else
        //{
        //    //Sửa tin đăng
        string sqlUpdateTD = "update tb_footer set ShortText=N'" + txtMoTaNgan.Value.Replace("'", "") + "',Discription =N'" + txtNoiDung.Text.Replace("'", "") + "' where [idfooter]='" + idTinTuc + "' ";
       
        bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
        if (ktUpdateTD)
        {
            Response.Redirect("DanhSachfooter.aspx");
        }
        else
        {
            Response.Write("<script>alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!');</script>");
        }
        //}
    }
}