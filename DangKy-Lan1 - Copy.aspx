﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/MasterPage - Copy.master" AutoEventWireup="true" CodeFile="DangKy-Lan1 - Copy.aspx.cs" Inherits="DangNhap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
    <link href="../Css/Page/DangNhap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     
    <section class="feature container">
        <div class="row">
            <!--Top Jobs-->
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div class="panel jobs-board-listing with-mc no-padding no-border">
                        <div class="panel-content" id="tabChoThue" style="padding: 10px;margin-top:88px;">
                            <div class="job-list scrollbar m-t-lg">
                                <div id="vnw-log-in" class="container main-content">
                                    <div class="col-sm-4 col-sm-push-4">
                                        <h5 class="text-center">ĐĂNG KÝ</h5>
                                        <div>
                                            <div class="form-group">
                                                <div>
                                                    <input type="text" id="txtTenDangNhap" name="form[username]" required="required" placeholder="Số điện thoại" tabindex="1" class="form-control" runat="server" onkeypress="onlyNumber(event)" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div>
                                                    <input type="password" id="txtMatKhau" name="form[password]" required="required" placeholder="Mật khẩu" tabindex="2" class="form-control" runat="server" />
                                                </div>
                                            </div>
                                            <!-- Buttons-->
                                            <div class="form-group">
                                                <div >
                                                    <asp:LinkButton ID="btDangKy" runat="server" OnClick="btDangKy_Click" class="btn btn-primary btn-block">Đăng ký</asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="dvDKTC" runat="server" style="text-align: center; color: #05c705; font-style: italic;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
