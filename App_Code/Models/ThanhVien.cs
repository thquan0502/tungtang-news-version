﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Models
{
    public class ThanhVien : Models.AbstractModel
    {
        public ThanhVien() {
            this.TableName = "tb_ThanhVien";
        }
        public DataRow getDataRowById(string accountId)
        {
            string sql = "select top 1 * from " + this.TableName + " where idThanhVien = "+ accountId;
            DataTable dataTable = Connect.GetTable(sql);
            if (dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0];
            }
            else
            {
                return null;
            }
        }
    }
}
