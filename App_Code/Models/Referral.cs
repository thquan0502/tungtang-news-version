﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Models
{
    public class Referral : Models.AbstractModel
    {
		public const string STATUS_ALL = "ALL";
        public const string STATUS_OWNER_APPROVED = "OWNER_APPROVED";
        public const string STATUS_OWNER_CANCELED = "OWNER_CANCELED";
        public const string STATUS_FEE_COMMISSION = "FEE_COMMISSION";
        public const string STATUS_COLLAB_PENDING = "COLLAB_PENDING";
        public const string STATUS_COLLAB_APPROVED = "COLLAB_APPROVED";
        public const string STATUS_COLLAB_DENIED = "COLLAB_DENIED";
        public const string STATUS_ADMIN_APPROVED = "ADMIN_APPROVED";
        public Referral() {
            this.TableName = "Referrals";
        }
        
        public bool create(string tinDangId, string ownerId, string collabId)
        {
            DataRow dataTinDang = (new Models.TinDang()).getDataRowById(tinDangId);
            if(dataTinDang == null)
            {
                return false;
            }
            string sql = "";
            sql += " INSERT INTO Referrals ";
            sql += "        (OwnerId ";
            sql += "        ,CollabId ";
            sql += "        ,TinDangId ";
            sql += "        ,Rate ";
            sql += "        ,Amount ";
            sql += "        ,Commission ";
            sql += "        ,Status";
            sql += "        ,OwnerApprovedAt)";
            sql += "  VALUES ";
            sql += "        ("+ ownerId + " ";
            sql += "        ,"+ collabId + " ";
            sql += "        ,"+ tinDangId +" ";
            sql += "        ,"+ dataTinDang["ReferralRate"].ToString() + " ";
            sql += "        ," + dataTinDang["ReferralAmount"].ToString() + " ";
            sql += "        ," + dataTinDang["Commission"].ToString() + " ";
            sql += "        , '" + Models.Referral.STATUS_OWNER_APPROVED + "' ";
            sql += "        , SYSDATETIME()) ";
            string[] paramsName = new string[0] {  };
            string[] paramsValue = new string[0] {  };
            if (Connect.Exec(sql, paramsName, paramsValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public DataTable getAllByTinDangId(string tinDangId)
        {
            string sql = "select * from " + this.TableName + " where idTinDang=" + tinDangId;
            DataTable dataTable = Connect.GetTable(sql);
            if (dataTable.Rows.Count > 0)
            {
                return dataTable;
            }
            else
            {
                return null;
            }
        }
        
    }
}
