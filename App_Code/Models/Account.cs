﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Account
    {
        public const string TableName = "tb_ThanhVien";
        public int Id { get; set; }
        public string Code { get; set; }
        public string StoreName { get; set; }
        public string Avartar { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime RegisterAt { get; set; }
        public bool IsBlock { get; set; }
        public Account() {}
    }
}
