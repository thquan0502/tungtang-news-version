﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Models
{
    public class TinDang : Models.AbstractModel
    {
        public const string STATUS_EXPIRED = "EXPIRED";
        public const string STATUS_EXPIRED_TXT = "Đã hết hạn";
        public const string STATUS_ACTIVE = "ACTIVE";
        public const string STATUS_ACTIVE_TXT = "Đang hoạt động";
        public const string STATUS_PENDING = "PENDING";
        public const string STATUS_PENDING_TXT = "Đợi phê duyệt";
        public const string STATUS_DRAFT = "DRAFT";
        public const string STATUS_DRAFT_TXT = "Đang nháp";
        public const string STATUS_DENY = "DENY";
        public const string STATUS_DENY_TXT = "Bị từ chối";

        public TinDang() {
            this.TableName = "tb_TinDang";
        }
        // ===== Public methods =====
        public DataRow getDataRowById(string idTinDang)
        {
            string sql = "select top 1 * from " + this.TableName + " where idTinDang="+ idTinDang;
            DataTable dataTable = Connect.GetTable(sql);
            if (dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0];
            }
            else
            {
                return null;
            }
        }
        public static string getStatus(string isHetHan, string isDraft, string isDuyet)
        {
            if (isHetHan.ToUpper() == "TRUE")
            {
                return TinDang.STATUS_EXPIRED;
            }
            else
            {
                if (isDraft.ToUpper() == "TRUE")
                {
                    return TinDang.STATUS_DRAFT;
                }
                if (isDuyet == null || isDuyet == "")
                {
                    return TinDang.STATUS_PENDING;
                }
                else
                {
                    string isDuyet_upper = isDuyet.ToUpper();
                    if (isDuyet_upper == "TRUE")
                    {
                        return TinDang.STATUS_ACTIVE;
                    }
                    else
                    {
                        return TinDang.STATUS_DENY;
                    }
                }
            }
        }
        public string getWhereConditionByStatus(string status)
        {
            string condition = " ";
            switch (status)
            {
                case TinDang.STATUS_EXPIRED:
                    condition += " isHetHan = 1 ";
                    break;
                case TinDang.STATUS_DRAFT:
                    condition += " (isHetHan = 0 or isHetHan is null) and IsDraft = 1 ";
                    break;
                case TinDang.STATUS_PENDING:
                    condition += " (isHetHan = 0 or isHetHan is null) and (IsDraft = 0 or IsDraft is null) and isDuyet is null  ";
                    break;
                case TinDang.STATUS_DENY:
                    condition += " (isHetHan = 0 or isHetHan is null) and (IsDraft = 0 or IsDraft is null) and isDuyet = 0  ";
                    break;
                case TinDang.STATUS_ACTIVE:
                    condition += " (isHetHan = 0 or isHetHan is null) and (IsDraft = 0 or IsDraft is null) and isDuyet = 1  ";
                    break;
            }
            if(condition.Trim() != "")
            {
                condition = " ( " + condition + " ) ";
            }
            else
            {
                condition = " ( 1 = 1 ) ";
            }
            return condition;
        }
        public double calcCommission(double price, int rateInt, double amount)
        {
            double rate = (double)rateInt;
            double total = 0;
            total += amount;
            total += (price * (rate / 100));
            return total;
        }
    }
}