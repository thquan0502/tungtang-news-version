﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{

    public class PostFees
    {
        public static string tableName = "PostFees";

        public static DataTable getAll()
        {
            string sql = "SELECT * FROM " + tableName;
            DataTable table = Connect.GetTable(sql);
            return table;
        }

        public static DataRow find(string id)
        {
            string sql = "SELECT * FROM " + tableName + " WHERE Id = " + id;
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                return table.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public static DataRow findByCode(string code)
        {
            string sql = "SELECT * FROM " + tableName + " WHERE Code = '" + code + "'";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                return table.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public static void update(string id, string name, long pricePer1Days, long pricePer7Days, long pricePer14Days, long pricePer30Days)
        {
            string sql = @"
                UPDATE " + tableName + @" SET
                   Name = N'" + name + @"',
                   PricePer1Days = '" + pricePer1Days + @"',
                   PricePer7Days = '" + pricePer7Days + @"',
                   PricePer14Days = '" + pricePer14Days + @"',
                   PricePer30Days = '" + pricePer30Days + @"',
                   UpdatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                WHERE 
                    Id = '" + id + @"'
            ";
            Connect.Exec(sql);
        }
    }
}
