﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{
    public class RechargePackages
    {
        public static string tableName = "RechargePackages";

        public static DataTable getAll()
        {
            string sql = "SELECT * FROM " + tableName;
            DataTable table = Connect.GetTable(sql);
            return table;
        }

        public static DataRow find(string id)
        {
            string sql = "SELECT * FROM " + tableName + " WHERE Id = " + id;
            DataTable table = Connect.GetTable(sql);
            if(table.Rows.Count > 0)
            {
                return table.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public static void update(string id, string name, string rechargePrice, string actualAmount, string backgroundColor)
        {
            string sql = @"
            UPDATE " + tableName +@" SET
               Name = N'" + name + @"',
               RechargePrice = '" + rechargePrice + @"',
               ActualAmount = '" + actualAmount + @"',
               BackgroundColor = '" + backgroundColor + @"',
               UpdatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
            WHERE 
                Id = '" + id + @"'
        ";
            Connect.Exec(sql);
        }

        public static DataRow findByCode(string code)
        {
            string sql = "SELECT * FROM " + tableName + " WHERE Code = '" + code + "'";
            DataTable table = Connect.GetTable(sql);
            if(table.Rows.Count <= 0)
            {
                return null;
            }
            else
            {
                return table.Rows[0];
            }
        }
    }
}
