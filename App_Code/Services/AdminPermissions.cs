﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{
    public class AdminPermissions
    {
        public static bool isAuthorize(int permissionId)
        {
            string adminId = Utilities.Auth.getAdminId();
            if(adminId == null)
            {
                return false;
            }
            else
            {
                return isAuthorize(adminId, permissionId);
            }
        }

        public static bool isAuthorize(string adminId, int permissionId)
        {
            string sql = @"
                SELECT COUNT(*) AS TOTALROWS 
                FROM tb_Admin AS TBAdmins
	                LEFT JOIN tb_ChiTietQuyenAdmin AS TBPermissions
		                ON TBAdmins.idAdmin = TBPermissions.idAdmin
                WHERE TBAdmins.TenDangNhap = '" + adminId + @"' AND (TBAdmins.idLoaiAdmin = 1 OR TBPermissions.idNhomQuyen = " + permissionId + @")
            ";
            DataTable table = Connect.GetTable(sql);
            int total = (int) table.Rows[0]["TOTALROWS"];

            if(total > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
