﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{
    public class RenovateFees
    {
        public static string tableName = "RenovateFees";

        public static long getPrice()
        {
            string sql = "SELECT TOP 1 * FROM " + tableName;
            DataTable table = Connect.GetTable(sql);
            long price = long.Parse(table.Rows[0]["Price"].ToString());
            return price;
        }

        public static void updatePrice(long price)
        {
            string sql = "UPDATE " + tableName + " SET Price = " + price + ", UpdatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"' WHERE code = 'default_pack'";
            Connect.Exec(sql);
        }
    }
}
