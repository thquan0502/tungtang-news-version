﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{
    public class Post
    {
        public static DataRow find(string postId)
        {
            if (string.IsNullOrWhiteSpace(postId)) return null;
            string sql = "SELECT TOP 1 * FROM tb_TinDang WHERE idTinDang = '" + postId + "'";
            DataTable table = Connect.GetTable(sql);
            if(table.Rows.Count > 0)
            {
                return table.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public static void upgradePostFee(string postId, string code, DateTime startAt, DateTime endAt, long fee)
        {
            if (string.IsNullOrWhiteSpace(postId))
            {
                return;
            }
            string sql = @"
                UPDATE tb_TinDang SET
	                FeeCode = '" + code + @"',
	                FeeStartTime = '" + startAt.ToString() + @"',
	                FeeEndTime = '" + endAt.ToString() + @"',
                    FeeTotal = " + fee + @"
                WHERE idTinDang = '" + postId + @"'
            ";
            Connect.Exec(sql);
        }

        public static void extendPostFee(string postId, DateTime endAt, long fee)
        {
            if (string.IsNullOrWhiteSpace(postId))
            {
                return;
            }
            long total = getFeeTotal(postId);
            string sql = @"
                UPDATE tb_TinDang SET
	                FeeEndTime = '" + endAt.ToString() + @"',
                    FeeTotal = " + (total + fee) + @"
                WHERE idTinDang = '" + postId + @"'
            ";
            Connect.Exec(sql);
        }

        public static void renovate(string postId)
        {
            if (string.IsNullOrWhiteSpace(postId))
            {
                return;
            }
            string sql = @"
                UPDATE tb_TinDang SET
	                NgayDayLenTop = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                WHERE idTinDang = '" + postId + @"'
            ";
            Connect.Exec(sql);
        }

        public static long getFeeTotal(string postId)
        {
            string sql = "SELECT TOP 1 ISNULL(FeeTotal, 0) AS FeeTotal FROM tb_TinDang WHERE idTinDang = '"+ postId + "';";
            DataTable table = Connect.GetTable(sql);
            if(table.Rows.Count < 0)
            {
                return 0;
            }
            return table.Rows[0].Field<long>("FeeTotal");
        }
    }
}
