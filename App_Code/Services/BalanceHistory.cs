﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{
    public class BalanceHistory
    {
        public static long createRechargeTransfer(string accountId, long amount, long actualAmount, string status, string note)
        {
            string sql = @"
                INSERT INTO BalanceHistory (
                    AccountId,
                    Type,
                    Amount,
                    ActualAmount,
                    RechargeMethod,
                    RechargeStatus,
                    Note,
                    CreatedAt,
                    UpdatedAt
                ) OUTPUT INSERTED.Id VALUES (
                    '" + accountId + @"',
                    '" + Utilities.BalanceHistory.TYPE_RECHARGE + @"',
                    " + amount + @",
                    " + actualAmount + @",
                    '" + Utilities.BalanceHistory.RECHARGE_METHOD_TRANSFER + @"',
                    '" + status + @"',
                    '" + note + @"',
                    '" + Utilities.Database.getNow() + @"',
                    '" + Utilities.Database.getNow() + @"'
                );
            ";
            DataTable table = Connect.GetTable(sql);

            long id = table.Rows[0].Field<long>("Id");
            return id;
        }

        public static long createRecharge(string accountId, string method, long amount, long actualAmount, string status, string transactionId)
        {
            string sql = @"
                INSERT INTO BalanceHistory (
                    AccountId,
                    Type,
                    Amount,
                    ActualAmount,
                    RechargeMethod,
                    RechargeStatus,
                    TransactionId,
                    CreatedAt,
                    UpdatedAt
                ) OUTPUT INSERTED.Id VALUES (
                    '" + accountId + @"',
                    '" + Utilities.BalanceHistory.TYPE_RECHARGE + @"',
                    " + amount + @",
                    " + actualAmount + @",
                    '" + method + @"',
                    '" + status + @"',
                    '" + transactionId + @"',
                    '" + Utilities.Database.getNow() + @"',
                    '" + Utilities.Database.getNow() + @"'
                );
            ";
            DataTable table = Connect.GetTable(sql);

            long id = table.Rows[0].Field<long>("Id");
            return id;
        }

        public static void createSpent(string accountId, string spentType, long amount, string feeCode)
        {
            string sql = @"
                INSERT INTO BalanceHistory (
                    AccountId,
                    Type,
                    Amount,
                    SpentType,
                    CreatedAt,
                    UpdatedAt,
                    FeeCode
                ) VALUES (
                    '" + accountId + @"',
                    '" + Utilities.BalanceHistory.TYPE_SPENT + @"',
                    " + amount + @",
                    '" + spentType + @"',
                    '" + Utilities.Database.getNow() + @"',
                    '" + Utilities.Database.getNow() + @"',
                    '" + feeCode + @"'
                );
            ";
            Connect.Exec(sql);
        }

        public static void createSpentRenovate(string accountId, string spentType, long amount)
        {
            string sql = @"
                INSERT INTO BalanceHistory (
                    AccountId,
                    Type,
                    Amount,
                    SpentType,
                    CreatedAt,
                    UpdatedAt
                ) VALUES (
                    '" + accountId + @"',
                    '" + Utilities.BalanceHistory.TYPE_SPENT + @"',
                    " + amount + @",
                    '" + spentType + @"',
                    '" + Utilities.Database.getNow() + @"',
                    '" + Utilities.Database.getNow() + @"'
                );
            ";
            Connect.Exec(sql);
        }

        public static DataRow find(long id)
        {
            string sql = "SELECT TOP 1 * FROM BalanceHistory WHERE Id = " + id;
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                return table.Rows[0];
            }else
            {
                return null;
            }
        }
    }
}