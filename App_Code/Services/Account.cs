﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Services
{
    public class Account
    {
        public static DataRow getAccount(string accountId)
        {
            if (string.IsNullOrWhiteSpace(accountId)) return null;
            string sql = "SELECT TOP 1 * FROM tb_ThanhVien WHERE idThanhVien = '" + accountId + "'";
            DataTable table = Connect.GetTable(sql);
            if(table.Rows.Count > 0)
            {
                return table.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public static long getBalance(string accountId)
        {
            DataRow account = getAccount(accountId);
            return Utilities.Account.getBalance(account);
        }

        /*
         * [0]: Amount
         * [1]: Recharge
         * [2]: Spent
         */
        public static long[] getBalanceData(string id)
        {
            DataRow account = getAccount(id);
            long[] result = new long[3];
            result[0] = Utilities.Account.getBalance(account);
            result[1] = Utilities.Account.getBalanceRecharge(account);
            result[2] = Utilities.Account.getBalanceSpent(account);
            return result;
        }

        public static void rechargeBalance(string accountId, long amount)
        {
            if (string.IsNullOrWhiteSpace(accountId))
            {
                return;
            }
            long[] balanceData = getBalanceData(accountId);
            string sql = @"
                UPDATE tb_ThanhVien SET
	                BalanceAmount = " + (balanceData[0] + amount) + @",
	                BalanceRecharge = " + (balanceData[1] + amount) + @"
                WHERE idThanhVien = '" + accountId + @"'
            ";
            Connect.Exec(sql);
        }

        public static void spentBalance(string accountId, long amount)
        {
            if (string.IsNullOrWhiteSpace(accountId))
            {
                return;
            }
            long[] balanceData = getBalanceData(accountId);
            string sql = @"
                UPDATE tb_ThanhVien SET
	                BalanceAmount = " + (balanceData[0] - amount) + @",
	                BalanceSpent = " + (balanceData[2] + amount) + @"
                WHERE idThanhVien = '" + accountId + @"'
            ";
            Connect.Exec(sql);
        }
    }
}
