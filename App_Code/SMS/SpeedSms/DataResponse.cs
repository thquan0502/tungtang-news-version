﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppSMS.SpeedSms
{
    public class DataResponse
    {
        public String tranId = null;
        public String name = null;
        public String totalSMS = null;
        public String totalPrice = null;
        public String invalidPhone = null;
    }
}
