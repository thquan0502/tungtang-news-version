﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Configuration;

namespace AppSMS
{
    public class SMSService
    {
        public SMSService()
        {

        }

        public string send(String phone, String content)
        {
            SpeedSMSAPI api = new SpeedSMSAPI(ConfigurationManager.AppSettings["speedsmskey"]);
            String[] phones = new String[] { phone };
            String response = api.sendSMS(phones, content, 2, "TUNGTANG");
            return response;
        }
    }
}