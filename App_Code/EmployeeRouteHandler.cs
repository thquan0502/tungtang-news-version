﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;

public class EmployeeRouteHandler : IRouteHandler
{
    public EmployeeRouteHandler() {}
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        string Dept = requestContext.RouteData.Values["Dept"].ToString();
        string[] Action = requestContext.RouteData.Values["Action"].ToString().Split('-');
        string contextAction = requestContext.RouteData.Values["Action"].ToString();

        HttpContext context = HttpContext.Current;
        context.Items.Add("Dept", requestContext.RouteData.Values["Dept"].ToString());
        context.Items.Add("Action", requestContext.RouteData.Values["Action"].ToString());

        if (Dept.ToUpper() == "DANG-NHAP")
            return BuildManager.CreateInstanceFromVirtualPath("~/DangNhap - Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "DANG-KY")
            return BuildManager.CreateInstanceFromVirtualPath("~/Register.aspx", typeof(Page)) as Page;
         if (Dept.ToUpper() == "THONG-TIN" && contextAction.ToUpper() == "TT")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/DangKy-Lan2 - Copy.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "THONG-TIN" && contextAction.ToUpper() == "PRO")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/CapNhatPro.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TIN-DANG")
            return BuildManager.CreateInstanceFromVirtualPath("~/TinDang - Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "TDCT")
            return BuildManager.CreateInstanceFromVirtualPath("~/PostDetail.aspx", typeof(Page)) as Page;
		if (Dept.ToUpper() == "PREVIEW")
            return BuildManager.CreateInstanceFromVirtualPath("~/PostPreview.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "DANG-TIN")
            return BuildManager.CreateInstanceFromVirtualPath("~/CreatePost.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "THONG-TIN-CA-NHAN")
            return BuildManager.CreateInstanceFromVirtualPath("~/ThongTinCaNhan - Copy.aspx", typeof(Page)) as Page;

        // account routes
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "CONG-TAC")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/AccountReferral.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "CHIA-SE")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/AccountPost.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "QUEN-MAT-KHAU")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/ForgotPassword.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "SOCIAL-MEDIA")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/SocialVerification.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "THONG-TIN-VI")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/BalanceUser.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "LICH-SU-GIAO-DICH")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/BalanceHistory.aspx", typeof(Page)) as Page;
        }
        if (Dept.ToUpper() == "TAI-KHOAN" && contextAction.ToUpper() == "TAI-CHINH-HO-TRO")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/BalanceSupport.aspx", typeof(Page)) as Page;
        }

        if (Dept.ToUpper() == "THUE-AN-TOAN")
            return BuildManager.CreateInstanceFromVirtualPath("~/ThueAnToan - Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "TIN-TUC")
            return BuildManager.CreateInstanceFromVirtualPath("~/TinTuc - Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "DOI-MAT-KHAU")
            return BuildManager.CreateInstanceFromVirtualPath("~/DoiMatKhau - Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "CTTT")
            return BuildManager.CreateInstanceFromVirtualPath("~/ChiTietTinTuc - Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "TAG")
            return BuildManager.CreateInstanceFromVirtualPath("~/News-Copy.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "BLOGTT")
            return BuildManager.CreateInstanceFromVirtualPath("~/NewsDetail-Copy.aspx", typeof(Page)) as Page;
      //  if (Dept.ToUpper() == "BLOGTT")
         //   return BuildManager.CreateInstanceFromVirtualPath("~/NewsDetail.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "ADMIN" && Action[0].ToUpper() == "LOGIN")
            return BuildManager.CreateInstanceFromVirtualPath("~/Admin/Home/DangNhap.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "MO-TA")
            return BuildManager.CreateInstanceFromVirtualPath("~/MoTaChinhFooter.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "ADMIN-HOME" )
            return BuildManager.CreateInstanceFromVirtualPath("~/Admin/Home/Default.aspx", typeof(Page)) as Page;
        if (Dept.ToUpper() == "ADMIN-HOME")
            return BuildManager.CreateInstanceFromVirtualPath("~/Admin/Home/Default.aspx", typeof(Page)) as Page;

        if (Dept.ToUpper() == "ERROR")
        {
            if (contextAction == "404")
            {
                return BuildManager.CreateInstanceFromVirtualPath("~/Error404.aspx", typeof(Page)) as Page;
            }
        }

        return BuildManager.CreateInstanceFromVirtualPath("", typeof(Page)) as Page;
    }
}