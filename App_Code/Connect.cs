﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Connect
/// </summary>
public static class Connect
{
    public static SqlConnection ConnectSQL()
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["app"].ConnectionString);
        return conn;
    }
    public static object FirstResulfExec(string strCommandText)
    {
        object value;
        SqlConnection Conn = ConnectSQL();
        try
        {
            SqlCommand Cmd = new SqlCommand(strCommandText, Conn);
            Cmd.CommandType = CommandType.Text;
            Cmd.Parameters.Clear();
            if (Conn.State == ConnectionState.Closed)
            {
                Conn.Open();
            }
            value = Cmd.ExecuteScalar();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            return value;
        }
        catch (Exception)
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            return null;
        }
    }

    #region Google
    public static Int32 InsertNewMemberGoogle(string idgg, string namegg, string picturegg, string emailgg, string tendn, DateTime ngaydangki, string Refcode)
    {
        Int32 rowsAffected = -1;
       // string macode = "";
      //  macode = System.Guid.NewGuid().ToString();
        //SqlConnection conn = ConnectSQL();
        string _connectionString = @"Data Source=125.253.127.73;Initial Catalog=tungtang;User ID=tungtang; Password=cu@61Fx3;Connect Timeout=10";
        using (SqlConnection conn = new SqlConnection(_connectionString))
        {
            string commandText = "INSERT INTO dbo.tb_ThanhVien(MaThanhVien, TenCuaHang, LinkAnh, Email, TenDangNhap, NgayDangKy, RefCode)"
                                    + "VALUES (@mathanhvien,@tencuahang,@linkanh,@email,@tendangnhap,@ngaydangky,'"+ System.Guid.NewGuid() + "')";
            //SqlCommand command = new SqlCommand(commandText, conn);
            using (SqlCommand command = new SqlCommand(commandText, conn))
            {
                command.Parameters.Add("@mathanhvien", SqlDbType.VarChar).Value = idgg;
                command.Parameters.Add("@tencuahang", SqlDbType.NVarChar).Value = namegg;
                command.Parameters.Add("@linkanh", SqlDbType.NVarChar).Value = picturegg;
                command.Parameters.Add("@email", SqlDbType.VarChar).Value = emailgg;
                command.Parameters.Add("@tendangnhap", SqlDbType.VarChar).Value = emailgg;
                command.Parameters.Add("@ngaydangky", SqlDbType.DateTime).Value = ngaydangki;
                //command.Parameters.Add(macode, SqlDbType.VarChar).Value = Refcode;

                try
                {

                    conn.Open();
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Close();
                        return rowsAffected;
                    }
                    rowsAffected = command.ExecuteNonQuery();
                    Console.WriteLine("RowsAffected: {0}", rowsAffected);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        return rowsAffected;
    }
    #endregion Google
    #region Facebook
    public static Int32 InsertNewMemberFacebook(string idfb, string namefb, string picturefb, string emailfb, string tendn, DateTime ngaydangki, String Refcode)
    {
        Int32 rowsAffected = -1;
        //SqlConnection conn = ConnectSQL();
        string _connectionString = @"Data Source=125.253.127.73;Initial Catalog=tungtang;User ID=tungtang; Password=cu@61Fx3;Connect Timeout=10";
        using (SqlConnection conn = new SqlConnection(_connectionString))
        {
            string commandText = "INSERT INTO dbo.tb_ThanhVien(MaThanhVien, TenCuaHang, LinkAnh, Email, TenDangNhap, NgayDangKy,Refcode)"
                                    + "VALUES (@mathanhvien,@tencuahang,@linkanh,@email,@tendangnhap,@ngaydangky,'"+ System.Guid.NewGuid() + "')";
            //SqlCommand command = new SqlCommand(commandText, conn);
            using (SqlCommand command = new SqlCommand(commandText, conn))
            {
                command.Parameters.Add("@mathanhvien", SqlDbType.VarChar).Value = idfb;
                command.Parameters.Add("@tencuahang", SqlDbType.NVarChar).Value = namefb;
                command.Parameters.Add("@linkanh", SqlDbType.NVarChar).Value = picturefb;
                command.Parameters.Add("@email", SqlDbType.VarChar).Value = emailfb;
                command.Parameters.Add("@tendangnhap", SqlDbType.VarChar).Value = emailfb;
                command.Parameters.Add("@ngaydangky", SqlDbType.DateTime).Value = ngaydangki;

                try
                {

                    conn.Open();
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Close();
                        return rowsAffected;
                    }
                    rowsAffected = command.ExecuteNonQuery();
                    Console.WriteLine("RowsAffected: {0}", rowsAffected);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        return rowsAffected;
    }
    #endregion Facebook
    public static string UploadFile(bool isInsert, FileUpload FileUpload1, string code, string path, string picName, HttpServerUtility server)
    {
        if (FileUpload1.HasFile)
        {
            try
            {
                string filename = "";
                if (isInsert)
                    filename = code + FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.'));
                else
                    filename = code + FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.'));
                string location = server.MapPath(path) + filename;
                FileUpload1.SaveAs(location);
                return filename;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            if (isInsert)
                return "NoImage.png";
            else
                return picName;
        }
    }
    public static DataTable GetTable(string strCommandText)
    {
        SqlConnection Conn = ConnectSQL();
        try
        {
            if (Conn.State == ConnectionState.Closed)
                Conn.Open();
            SqlCommand cmd = new SqlCommand(strCommandText, Conn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = cmd;
            DataSet ds = new DataSet();
            ad.Fill(ds, "table1");
            ad.Dispose();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            return ds.Tables[0];
        }
        catch
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            return null;
        }
    }
    public static DataTable GetTable(string strCommandText, string[] parameterNames, object[] parameterValues)
    {
        SqlConnection Conn = ConnectSQL();
        try
        {
            if (Conn.State == ConnectionState.Closed)
                Conn.Open();
            SqlCommand cmd = new SqlCommand(strCommandText, Conn);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = cmd;
            DataSet ds = new DataSet();
            ad.Fill(ds, "table1");
            ad.Dispose();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            return ds.Tables[0];
        }
        catch
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            return null;
        }
    }
    public static bool Exec(string strCommandText)
    {
        SqlConnection Conn = ConnectSQL();
        bool flag = false;
        try
        {
            SqlCommand Cmd = new SqlCommand(strCommandText, Conn);
            Cmd.CommandType = CommandType.Text;
            Cmd.Parameters.Clear();
            if (Conn.State == ConnectionState.Closed)
            {
                Conn.Open();
            }
            Cmd.ExecuteNonQuery();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            flag = true;
        }
        catch (Exception)
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            flag = false;
        }

        return flag;
    }
    public static bool Exec(string strCommandText, string[] arrParameterName, object[] arrParameterValue)
    {
        SqlConnection Conn = ConnectSQL();
        bool result = false;
        try
        {
            if (Conn.State == ConnectionState.Closed)
                Conn.Open();
            SqlCommand Cmd = new SqlCommand(strCommandText, Conn);
            Cmd.CommandType = CommandType.Text;
            Cmd.Parameters.Clear();
            for (int i = 0; i < arrParameterName.Length; i++)
            {
                SqlParameter s = new SqlParameter(arrParameterName[i], arrParameterValue[i]);
                Cmd.Parameters.Add(s);
            }
            Cmd.ExecuteNonQuery();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            result = true;
        }
        catch (Exception)
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
        }

        return result;
    }
    public static bool Exec(string strCommandText, string[] arrParameterName, object[] arrParameterValue, SqlDbType[] types)
    {
        SqlConnection Conn = ConnectSQL();
        bool result = false;
        try
        {
            if (Conn.State == ConnectionState.Closed)
                Conn.Open();
            SqlCommand Cmd = new SqlCommand(strCommandText, Conn);
            Cmd.CommandType = CommandType.Text;
            Cmd.Parameters.Clear();
            for (int i = 0; i < arrParameterName.Length; i++)
            {
                SqlParameter s = new SqlParameter(arrParameterName[i], arrParameterValue[i]);
                s.SqlDbType = types[i];
                Cmd.Parameters.Add(s);
            }
            Cmd.ExecuteNonQuery();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            result = true;
        }
        catch (Exception)
        {
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
        }
        return result;
    }
}
