﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Net.NetworkInformation;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;

/// <summary>
/// Summary description for MyStaticData
/// </summary>
public class MyStaticData
{
    public static bool CheckNhomQuyen(string idNhomQuyen,string url)
    {
        int flagError = 0;
        url = url.ToUpper();
        if (url.Contains("DEFAULT.ASPX") || url.Contains("DOIMATKHAU.ASPX"))
        {
            return true;
        }
        else
        {
            if (idNhomQuyen.Trim() == "1")
            {
                if (url.Contains("QUANLYDANHMUC"))
                {

                }
                else
                {
                    flagError = 1;
                }
            }
            if (idNhomQuyen.Trim() == "2")
            {
                if (url.Contains("QUANLYTHANHVIENADMIN"))
                {

                }
                else
                {
                    flagError = 1;
                }
            }
            if (idNhomQuyen.Trim() == "3")
            {
                if (url.Contains("QUANLYTHANHVIEN"))
                {

                }
                else
                {
                    flagError = 1;
                }
            }
            if (idNhomQuyen.Trim() == "4")
            {
                if (url.Contains("QUANTRITNDANG") || url.Contains("TINCHODUYET"))
                {

                }
                else
                {
                    flagError = 1;
                }
            }

            if (idNhomQuyen.Trim() == "5")
            {
                if (url.Contains("QUANLYGIOITHIEU") || url.Contains("QUANLYFOOTER"))
                {

                }
                else
                {
                    flagError = 1;
                }
            }

            if (idNhomQuyen.Trim() == "6")
            {
                if (url.Contains("BANNERTRANGCHU"))
                {

                }
                else
                {
                    flagError = 1;
                }
            }

            if (flagError == 1) return false;
            else return true;
        }
    }
    public static double GetSoTienHoaHong(string Loai, double TongTien, string idNguoiMua)
    {
        double TT = 0;
        try
        {
            string MaQuyen = StaticData.getField("tb_NguoiDung", "MaQuyen", "idNguoiDung", idNguoiMua);
            double PhanTramHH = double.Parse(StaticData.getField("tb_MucHoaHong", Loai, "MaQuyen", MaQuyen));
            TT = (TongTien * PhanTramHH) / 100;
        }
        catch { }
        return TT;
    }
    public static double GetTongTienDonDatHang(string idDonDatHang)
    {
        double TongTienDonDatHang = 0;
        string sqlChiTietDonDatHang = "select * from tb_ChiTietDonDatHang where idDonDatHang='" + idDonDatHang + "'";
        DataTable tbChiTietDonDatHang = Connect.GetTable(sqlChiTietDonDatHang);
        for (int i = 0; i < tbChiTietDonDatHang.Rows.Count;i++ )
        {
            double GiaBan = 0;
            double SoLuong = 0;
            if (tbChiTietDonDatHang.Rows[i]["GiaBan"].ToString() != "")
                GiaBan = double.Parse(tbChiTietDonDatHang.Rows[i]["GiaBan"].ToString());
            if (tbChiTietDonDatHang.Rows[i]["SoLuong"].ToString() != "")
                SoLuong = double.Parse(tbChiTietDonDatHang.Rows[i]["SoLuong"].ToString());
            TongTienDonDatHang += GiaBan * SoLuong;
        }
        return TongTienDonDatHang;
    }
    public static string TaoMaDonDatHang()
    {
        double ViTriDonDatHang = 1;
        string sqlDonDatHang = "select top 1 idDonDatHang from tb_DonDatHang order by idDonDatHang desc";
        DataTable tbDonDatHang = Connect.GetTable(sqlDonDatHang);
        if (tbDonDatHang.Rows.Count > 0)
            ViTriDonDatHang = (double.Parse(tbDonDatHang.Rows[0]["idDonDatHang"].ToString()) + 1);

        string MaDonHang = "";
        string Ngay = DateTime.Now.ToString("dd/MM/yyyy").Replace("/","");
        if (ViTriDonDatHang.ToString().Length == 1)
            MaDonHang = "DDH" + Ngay + "00" + ViTriDonDatHang.ToString();
        if (ViTriDonDatHang.ToString().Length == 2)
            MaDonHang = "DDH" + Ngay + "0" + ViTriDonDatHang.ToString();
        if (ViTriDonDatHang.ToString().Length > 2)
            MaDonHang = "DDH" + Ngay + ViTriDonDatHang.ToString();
        return MaDonHang;
    }
	public static string GetSoSanPham(string idLoaiSanPham)
    {
        string sqlSanPham = "select idSanPham from tb_SanPham where isnull(isHienThiWeb,'False')='True'";
        if(idLoaiSanPham != "")
            sqlSanPham += " and idLoaiSanPham='" + idLoaiSanPham + "'";
        DataTable tbSanPham = Connect.GetTable(sqlSanPham);
        return tbSanPham.Rows.Count.ToString();
    }
}