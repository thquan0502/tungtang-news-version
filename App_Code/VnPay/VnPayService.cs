﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data;

namespace AppVnPay
{
    public class VnPayService
    {
        public static string createTransaction()
        {
            string sql = @"
                INSERT INTO Transactions (
                    VnpTxnref
                ) OUTPUT INSERTED.Id VALUES (
                    '" + System.Guid.NewGuid().ToString() + @"'
                );
            ";

            DataTable table = Connect.GetTable(sql);
            string id = table.Rows[0]["Id"].ToString();

            return id;
        }

        public static void sendTransaction(HttpResponse response, string id, long amount, long actualAmount, string text)
        {
            DataRow account = Utilities.Auth.getAccount();
            if(account == null)
            {
                Utilities.Redirect.notFound();
                return;
            }

            DataRow data = findById(id);
            VnPayLibrary vnpay = new VnPayLibrary();

            string vnpVersion = VnPayLibrary.VERSION;
            vnpay.AddRequestData("vnp_Version", vnpVersion);

            string vnpLocale = "vn";
            vnpay.AddRequestData("vnp_Locale", vnpLocale);

            string vnpCommand = "pay";
            vnpay.AddRequestData("vnp_Command", vnpCommand);

            string vnpTmnCode = ConfigurationManager.AppSettings["vnp_TmnCode"];
            vnpay.AddRequestData("vnp_TmnCode", vnpTmnCode);

            long vnpAmount = amount * 100;
            vnpay.AddRequestData("vnp_Amount", vnpAmount.ToString());

            string vnpCurrCode = "VND";
            vnpay.AddRequestData("vnp_CurrCode", vnpCurrCode);

            string vnpOrderInfo = text;
            vnpay.AddRequestData("vnp_OrderInfo", vnpOrderInfo);

            string vnpOrderType = "other";
            vnpay.AddRequestData("vnp_OrderType", vnpOrderType);

            string vnpIpAddr = Utils.GetIpAddress();
            vnpay.AddRequestData("vnp_IpAddr", vnpIpAddr);

            string vnpReturnUrl = ConfigurationManager.AppSettings["vnp_Returnurl"];
            vnpay.AddRequestData("vnp_ReturnUrl", vnpReturnUrl);

            vnpay.AddRequestData("vnp_TxnRef", data["VnpTxnref"].ToString());

            DateTime vnpCreateDate = DateTime.Now;
            vnpay.AddRequestData("vnp_CreateDate", vnpCreateDate.ToString("yyyyMMddHHmmss"));

            DateTime vnpExpireDate = DateTime.Now.AddMinutes(10);
            vnpay.AddRequestData("vnp_ExpireDate", vnpExpireDate.ToString("yyyyMMddHHmmss"));

            string paymentUrl = vnpay.CreateRequestUrl(ConfigurationManager.AppSettings["vnp_Url"], ConfigurationManager.AppSettings["vnp_HashSecret"]);

            string sql = @"
                UPDATE Transactions SET
	                VnpVersion = '" + vnpVersion + @"',
	                VnpLocale = '" + vnpLocale + @"',
	                VnpCommand = 'pay',
	                VnpTmnCode = '" + vnpTmnCode + @"',
	                VnpAmount = " + amount + @",
	                VnpCurrCode = '" + vnpCurrCode + @"',
	                VnpOrderInfo = '" + vnpOrderInfo + @"',
	                VnpOrderType = '" + vnpOrderType + @"',
	                VnpIpAddr = '" + vnpIpAddr + @"',
	                VnpReturnUrl = '" + vnpReturnUrl + @"',
	                VnpCreateDate = '" + vnpCreateDate.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
	                VnpExpireDate = '" + vnpExpireDate.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                    Type = 'RECHARE',
                    Status = 'CREATED',
                    CreatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                    UpdatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                    RawRequest = '" + paymentUrl + @"',
	                ActualAmount = " + actualAmount+ @",
                    AccountId = '" + account["idThanhVien"].ToString() + @"'
                WHERE Id = " + id + @"
            ";

            Connect.Exec(sql);

            response.Redirect(paymentUrl);
        }
        public static DataRow findById(string id)
        {
            string sqlSelect = "SELECT TOP 1 * FROM Transactions WHERE Id = '" + id + "'";
            DataTable tableSelect = Connect.GetTable(sqlSelect);
            if (tableSelect.Rows.Count > 0)
            {
                return tableSelect.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public static DataRow findByTxnref(string txnref)
        {
            string sqlSelect = "SELECT TOP 1 * FROM Transactions WHERE VnpTxnref = '" + txnref + "'";
            DataTable tableSelect = Connect.GetTable(sqlSelect);
            if(tableSelect.Rows.Count > 0)
            {
                return tableSelect.Rows[0];
            }
            else
            {
                return null;
            }
        }
    }
}
