﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class Banks
    {
        public static string createLogoUrl(string baseUrl)
        {
            if(string.IsNullOrWhiteSpace(baseUrl))
            {
                return "/Images/banks/default.jpg";
            }
            else
            {
                return baseUrl;
            }
        }
    }
}
