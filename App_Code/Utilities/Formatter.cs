﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class Formatter
    {
        public Formatter() {}

        public static string toCurrencyString(long amount)
        {
            return Utilities.Formatter.toCurrencyString("" + amount);
        }

        public static string toCurrencyString(string amount)
        {
            return string.Format("{0:N0}", Convert.ToDecimal(amount)) + "đ";
        }

        public static string toPercentString(string percent)
        {
            return string.Format("{0:P2}", Convert.ToDecimal(percent) / 100);
        }
        
        public static string toDateTimeString(string content)
        {
            DateTime dateTime;
            if (DateTime.TryParse(content, out dateTime))
            {
                return dateTime.ToString("dd-MM-yyyy HH:mm");
            }
            return "";
        }

        public static string toDateTimeString2(DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss:fff");
        }

        public static string toDateTimeOrderString(string content)
        {
            DateTime dateTime;
            if (DateTime.TryParse(content, out dateTime))
            {
                return dateTime.ToString("yyyyMMddhhmmss");
            }
            return "";
        }
    }
}
