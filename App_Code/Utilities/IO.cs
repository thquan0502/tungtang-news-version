﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class IO
    {
        public static void removeFile(string path)
        {
            string fullPath = HttpContext.Current.Request.MapPath(path);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }
    }
}
