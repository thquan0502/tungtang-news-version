﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Utilities
{
    public class Control
    {
        public Control() {}
        public static void setSelectedValue(DropDownList dropdownList, string value)
        {
            if(value == null)
            {
                return;
            }
            int index1 = 0;
            foreach(ListItem listItem in dropdownList.Items){
                if(listItem.Value == value)
                {
                    dropdownList.SelectedIndex = index1;
                    break;
                }
                index1 = index1 + 1;
            }
        }
    }
}
