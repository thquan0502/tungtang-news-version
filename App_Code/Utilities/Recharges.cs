﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class Recharges
    {
        public static string createMethodText(string method)
        {
            switch (method)
            {
                case "VNPAY":
                    return "Tự động";
                case "TRANSFER":
                    return "Thủ công";
                default:
                    return "Không xác định";
            }
        }
    }
}
