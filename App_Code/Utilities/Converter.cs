﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Utilities
{
    public class Converter
    {
        public static DateTime toDateTime(string sDate, string format)
        {
            DateTime x = DateTime.MinValue;
            if (DateTime.TryParseExact(sDate, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out x))
            {
                return x;
            }
            return x;
        }
    }
}
