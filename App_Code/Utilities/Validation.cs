﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class Validation
    {
        public Validation(){ }

        /**
         * Check the string contain digits only
         */
        public static bool isDigitsOnly(string input)
        {
            foreach(char c in input)
            {
                if(c < '0' || c > '9')
                {
                    return false;
                }
            }
            return true;
        }

        /**
         * Check wheher contain accented characters or not
         */
        public static bool containAccented(string input)
        {
            return input.Any(c => c > 255);
        }
    }
}
