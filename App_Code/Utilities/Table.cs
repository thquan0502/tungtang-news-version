using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class Table
    {
        public Table() { }
        public static string renderPagination(int totalItems, int perPage, int currentPage, string path, Dictionary<String, String> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if (addionalParams != null)
            {
                foreach (KeyValuePair<string, string> entry in addionalParams)
                {
                    if (index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Table.renderPagination(totalItems, perPage, currentPage, path, sParams);
        }
        public static string renderPagination(int totalItems, int perPage, int currentPage, string path, string addionalParams = "")
        {
            int totalPage = totalItems / perPage;
            int index = 1;
            bool hasBefore = false;
            bool hasAfter = false;
            if (totalItems % perPage > 0)
            {
                totalPage++;
            }
            string html = "";
            html += "<ul class='pagination'>";
            while (index <= totalPage)
            {
                if (
                    index != 1 && index != 2 && index != 3
                    && index != currentPage - 1 && index != currentPage && index != currentPage + 1
                    && index != totalPage && index != totalPage - 1 && index != totalPage - 2
                )
                {
                    if ((!hasBefore && index < currentPage) || (!hasAfter && index > currentPage))
                    {
                        html += "<li class='disabled'><a href='javascript:void(0);'>...</a></li>";
                        if (!hasBefore && index < currentPage)
                        {
                            hasBefore = true;
                        }
                        else if (!hasAfter && index > currentPage)
                        {
                            hasAfter = true;
                        }
                    }
                    index++;
                    continue;
                }
                html += "<li class='" + (index == currentPage ? " active " : "") + "'><a href='" + path + "?Page=" + index + (addionalParams == "" ? "" : "&" + addionalParams) + "'>" + index + "</a></li>";
                index++;
            }
            html += "</ul>";
            return html;
        }
        public static string renderPaginationSmall(int totalItems, int perPage, int currentPage, string path)
        {
            return Utilities.Table.renderPaginationSmall(totalItems, perPage, currentPage, path, new Dictionary<String, String>());
        }
        public static string renderPaginationSmallcd(int totalItems, int perPage, int currentPage, string path)
        {
            return Utilities.Table.renderPaginationSmall(totalItems, perPage, currentPage, path, new Dictionary<String, String>());
        }
        public static string renderPaginationSmallbtc(int totalItems, int perPage, int currentPage, string path)
        {
            return Utilities.Table.renderPaginationSmall(totalItems, perPage, currentPage, path, new Dictionary<String, String>());
        }
        public static string renderPaginationSmallthh(int totalItems, int perPage, int currentPage, string path)
        {
            return Utilities.Table.renderPaginationSmall(totalItems, perPage, currentPage, path, new Dictionary<String, String>());
        }
        public static string renderPaginationSmall(int totalItems, int perPage, int currentPage, string path, Dictionary<String, String> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if (addionalParams != null)
            {
                foreach (KeyValuePair<string, string> entry in addionalParams)
                {
                    if (index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Table.paginate(totalItems, perPage, currentPage, path, sParams, "sm");
        }
        public static string paginate(int totalItems, int perPage, int currentPage, string path, string addionalParams = "", string paginationType = "")
        {
            int totalPage = totalItems / perPage;
            int index = 1;
            bool hasBefore = false;
            bool hasAfter = false;
            if (totalItems % perPage > 0)
            {
                totalPage++;
            }
            string html = "";
            html += "<ul class='pagination " + (string.IsNullOrWhiteSpace(paginationType) ? "" : "pagination-" + paginationType) + "'>";
            while (index <= totalPage)
            {
                if (
                    index != 1 && index != 2 && index != 3
                    && index != currentPage - 1 && index != currentPage && index != currentPage + 1
                    && index != totalPage && index != totalPage - 1 && index != totalPage - 2
                )
                {
                    if ((!hasBefore && index < currentPage) || (!hasAfter && index > currentPage))
                    {
                        html += "<li class='disabled'><a href='javascript:void(0);'>...</a></li>";
                        if (!hasBefore && index < currentPage)
                        {
                            hasBefore = true;
                        }
                        else if (!hasAfter && index > currentPage)
                        {
                            hasAfter = true;
                        }
                    }
                    index++;
                    continue;
                }
                html += "<li class='" + (index == currentPage ? " active " : "") + "'><a href='" + path + "&Page=" + index + (addionalParams == "" ? "" : "&" + addionalParams) + "'>" + index + "</a></li>";
                index++;
            }
            html += "</ul>";
            return html;
        }

        public static string renderPaginationSmallthh(int totalItems, int perPage, int currentPage, string path, Dictionary<String, String> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if (addionalParams != null)
            {
                foreach (KeyValuePair<string, string> entry in addionalParams)
                {
                    if (index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Table.paginatethh(totalItems, perPage, currentPage, path, sParams, "sm");
        }
        public static string paginatethh(int totalItems, int perPage, int currentPage, string path, string addionalParams = "", string paginationType = "")
        {
            int totalPage = totalItems / perPage;
            int index = 1;
            bool hasBefore = false;
            bool hasAfter = false;
            if (totalItems % perPage > 0)
            {
                totalPage++;
            }
            string html = "";
            html += "<ul class='pagination " + (string.IsNullOrWhiteSpace(paginationType) ? "" : "pagination-" + paginationType) + "'>";
            while (index <= totalPage)
            {
                if (
                    index != 1 && index != 2 && index != 3
                    && index != currentPage - 1 && index != currentPage && index != currentPage + 1
                    && index != totalPage && index != totalPage - 1 && index != totalPage - 2
                )
                {
                    if ((!hasBefore && index < currentPage) || (!hasAfter && index > currentPage))
                    {
                        html += "<li class='disabled'><a href='javascript:void(0);'>...</a></li>";
                        if (!hasBefore && index < currentPage)
                        {
                            hasBefore = true;
                        }
                        else if (!hasAfter && index > currentPage)
                        {
                            hasAfter = true;
                        }
                    }
                    index++;
                    continue;
                }
                html += "<li class='" + (index == currentPage ? " active " : "") + "'><a href='" + path + "&Pagethh=" + index + (addionalParams == "" ? "" : "&" + addionalParams) + "'>" + index + "</a></li>";
                index++;
            }
            html += "</ul>";
            return html;
        }

        public static string renderPaginationSmallcd(int totalItems, int perPage, int currentPage, string path, Dictionary<String, String> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if (addionalParams != null)
            {
                foreach (KeyValuePair<string, string> entry in addionalParams)
                {
                    if (index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Table.paginatecd(totalItems, perPage, currentPage, path, sParams, "sm");
        }
        public static string paginatecd(int totalItems, int perPage, int currentPage, string path, string addionalParams = "", string paginationType = "")
        {
            int totalPage = totalItems / perPage;
            int index = 1;
            bool hasBefore = false;
            bool hasAfter = false;
            if (totalItems % perPage > 0)
            {
                totalPage++;
            }
            string html = "";
            html += "<ul class='pagination " + (string.IsNullOrWhiteSpace(paginationType) ? "" : "pagination-" + paginationType) + "'>";
            while (index <= totalPage)
            {
                if (
                    index != 1 && index != 2 && index != 3
                    && index != currentPage - 1 && index != currentPage && index != currentPage + 1
                    && index != totalPage && index != totalPage - 1 && index != totalPage - 2
                )
                {
                    if ((!hasBefore && index < currentPage) || (!hasAfter && index > currentPage))
                    {
                        html += "<li class='disabled'><a href='javascript:void(0);'>...</a></li>";
                        if (!hasBefore && index < currentPage)
                        {
                            hasBefore = true;
                        }
                        else if (!hasAfter && index > currentPage)
                        {
                            hasAfter = true;
                        }
                    }
                    index++;
                    continue;
                }
                html += "<li class='" + (index == currentPage ? " active " : "") + "'><a href='" + path + "&Pagecd=" + index + (addionalParams == "" ? "" : "&" + addionalParams) + "'>" + index + "</a></li>";
                index++;
            }
            html += "</ul>";
            return html;
        }

        public static string renderPaginationSmallbtc(int totalItems, int perPage, int currentPage, string path, Dictionary<String, String> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if (addionalParams != null)
            {
                foreach (KeyValuePair<string, string> entry in addionalParams)
                {
                    if (index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Table.paginatebtc(totalItems, perPage, currentPage, path, sParams, "sm");
        }
        public static string paginatebtc(int totalItems, int perPage, int currentPage, string path, string addionalParams = "", string paginationType = "")
        {
            int totalPage = totalItems / perPage;
            int index = 1;
            bool hasBefore = false;
            bool hasAfter = false;
            if (totalItems % perPage > 0)
            {
                totalPage++;
            }
            string html = "";
            html += "<ul class='pagination " + (string.IsNullOrWhiteSpace(paginationType) ? "" : "pagination-" + paginationType) + "'>";
            while (index <= totalPage)
            {
                if (
                    index != 1 && index != 2 && index != 3
                    && index != currentPage - 1 && index != currentPage && index != currentPage + 1
                    && index != totalPage && index != totalPage - 1 && index != totalPage - 2
                )
                {
                    if ((!hasBefore && index < currentPage) || (!hasAfter && index > currentPage))
                    {
                        html += "<li class='disabled'><a href='javascript:void(0);'>...</a></li>";
                        if (!hasBefore && index < currentPage)
                        {
                            hasBefore = true;
                        }
                        else if (!hasAfter && index > currentPage)
                        {
                            hasAfter = true;
                        }
                    }
                    index++;
                    continue;
                }
                html += "<li class='" + (index == currentPage ? " active " : "") + "'><a href='" + path + "&Pagebtc=" + index + (addionalParams == "" ? "" : "&" + addionalParams) + "'>" + index + "</a></li>";
                index++;
            }
            html += "</ul>";
            return html;
        }
        public static string renderPaginationSmalldnn(int totalItems, int perPage, int currentPage, string path, Dictionary<String, String> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if (addionalParams != null)
            {
                foreach (KeyValuePair<string, string> entry in addionalParams)
                {
                    if (index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Table.paginatednn(totalItems, perPage, currentPage, path, sParams, "sm");
        }
        public static string paginatednn(int totalItems, int perPage, int currentPage, string path, string addionalParams = "", string paginationType = "")
        {
            int totalPage = totalItems / perPage;
            int index = 1;
            bool hasBefore = false;
            bool hasAfter = false;
            if (totalItems % perPage > 0)
            {
                totalPage++;
            }
            string html = "";
            html += "<ul class='pagination " + (string.IsNullOrWhiteSpace(paginationType) ? "" : "pagination-" + paginationType) + "'>";
            while (index <= totalPage)
            {
                if (
                    index != 1 && index != 2 && index != 3
                    && index != currentPage - 1 && index != currentPage && index != currentPage + 1
                    && index != totalPage && index != totalPage - 1 && index != totalPage - 2
                )
                {
                    if ((!hasBefore && index < currentPage) || (!hasAfter && index > currentPage))
                    {
                        html += "<li class='disabled'><a href='javascript:void(0);'>...</a></li>";
                        if (!hasBefore && index < currentPage)
                        {
                            hasBefore = true;
                        }
                        else if (!hasAfter && index > currentPage)
                        {
                            hasAfter = true;
                        }
                    }
                    index++;
                    continue;
                }
                html += "<li class='" + (index == currentPage ? " active " : "") + "'><a href='" + path + "&Pagek=" + index + (addionalParams == "" ? "" : "&" + addionalParams) + "'>" + index + "</a></li>";
                index++;
            }
            html += "</ul>";
            return html;
        }
    }
}
