﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class Redirect
    {
        public static void notFound()
        {
            notFound(HttpContext.Current.Response);
        }
        public static void notFound(HttpResponse response)
        {
            response.Redirect("/Error404.aspx");
        }

        public static void balanceNotEnough(HttpResponse response)
        {
            response.Redirect("/tai-khoan/thong-tin-vi");
        }
    }
}
