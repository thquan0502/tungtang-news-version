﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Utilities
{
    public class Config
    {
        public Config() {}
        public static Dictionary<string, object> getConfigs()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string sql1 = "SELECT Config.Name AS Name, Config.Value AS Value FROM Config";
            DataTable table1 = Connect.GetTable(sql1);
            for(int i = 0; i < table1.Rows.Count; i = i + 1)
            {
                dict.Add(table1.Rows[i]["Name"].ToString(), table1.Rows[i]["Value"].ToString());
            }
            return dict;
        }
    }
}
