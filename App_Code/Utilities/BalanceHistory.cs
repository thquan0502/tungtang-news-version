﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class BalanceHistory
    {
        public static string TYPE_RECHARGE = "RECHARGE";
        public static string TYPE_SPENT = "SPENT";

        public static string RECHARGE_METHOD_VNPAY = "VNPAY";
        public static string RECHARGE_METHOD_TRANSFER = "TRANSFER";

        public static string RECHARGE_STATUS_CREATED = "CREATED";
        public static string RECHARGE_STATUS_ENTERED = "ENTERED";

        public static string SPENT_TYPE_ADD_POST = "ADD_POST";
        public static string SPENT_TYPE_UPGRADE_POST = "UPGRADE_POST";
        public static string SPENT_TYPE_EXTEND_POST = "EXTEND_POST";
        public static string SPENT_TYPE_TO_TOP = "TO_TOP";
    }
}