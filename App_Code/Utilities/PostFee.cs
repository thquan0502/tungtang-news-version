﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Utilities
{
    public class PostFee
    {
        public static string getPostCodeHuman(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                return "";
            }
            switch (code)
            {
                case "default_pack":
                    return "Gói Thường";
                case "vip1_pack":
                    return "Gói VIP 1";
                case "vip2_pack":
                    return "Gói VIP 2";
                case "vip3_pack":
                    return "Gói VIP 3";
                default:
                    return "";
            }
        }
    }
}