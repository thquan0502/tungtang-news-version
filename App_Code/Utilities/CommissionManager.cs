﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;

namespace Utilities
{
    public class CommissionManager
    {
        public CommissionManager() {}
        public static bool applyFilter(string from, string to)
        {
            DateTime dateFrom, dateTo, now = DateTime.Now;
            string sDateFrom, sDateTo;
            if (DateTime.TryParseExact(from, "yyyy-MM-dd HH:mm:ss:fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFrom))
            {
                sDateFrom = dateFrom.ToString("yyyy-MM-dd");
            }
            else
            {
                sDateFrom = null;
            }
            if (DateTime.TryParseExact(to, "yyyy-MM-dd HH:mm:ss:fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTo))
            {
                sDateTo = dateTo.ToString("yyyy-MM-dd");
            }
            else
            {
                sDateTo = null;
            }
            if (string.IsNullOrWhiteSpace(sDateFrom) && string.IsNullOrWhiteSpace(sDateTo))
            {
                return false;
            }
            string term = sDateFrom + ";" + sDateTo;
            string sql1 = @"
                UPDATE TBManagers
                SET 
	                TBManagers.FilterCurrentMark = '" + term + @"',
	                TBManagers.FilterCurrentValue = ISNULL(TBItems.FilterTotal, 0),
	                TBManagers.FilterCurrentLast = '" + now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                FROM CommissionManager TBManagers
	                LEFT JOIN (
		                SELECT 
			                TBDays.CommissionManagerId AS CommissionManagerId,
			                SUM(TBDays.EstimateValue) AS FilterTotal
		                FROM
			                CommissionByDay TBDays
		                WHERE 1 = 1 ";
            if (!string.IsNullOrWhiteSpace(sDateFrom))
            {
                sql1 += " AND TBDays.EstimateDate >= '" + sDateFrom + "' ";
            }
            if (!string.IsNullOrWhiteSpace(sDateTo))
            {
                sql1 += " AND TBDays.EstimateDate <= '" + sDateTo + "' ";
            }
            sql1 += @"
		                GROUP BY TBDays.CommissionManagerId
	                ) TBItems ON TBManagers.Id = TBItems.CommissionManagerId
                WHERE TBManagers.FilterCurrentMark IS NULL
	                OR TBManagers.FilterCurrentMark != '"+term+@"'
	                OR TBManagers.FilterCurrentValue IS NULL
	                OR TBManagers.FilterCurrentLast < TBManagers.UpdatedAt
            ";
            Connect.Exec(sql1);
            return true;
        }
        public static void createInstanceForAccount(string accountId)
        {
            string sql1 = @"
                SELECT COUNT(*) AS TOTALROWS
                FROM CommissionManager
                WHERE CommissionManager.AccountId = '" + accountId + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            int countTotal = Int32.Parse(table1.Rows[0]["TOTALROWS"].ToString());
            if (countTotal == 0)
            {
                string sql2 = @"
                INSERT INTO CommissionManager(
	                AccountId,
	                TotalCommission,
	                UpdatedAt
                )  VALUES (
	                " + accountId + @",
	                0,
	                '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"'
                )";
                Connect.Exec(sql2);
            }
        }
    }
}
