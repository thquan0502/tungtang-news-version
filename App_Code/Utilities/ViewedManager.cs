﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;

namespace Utilities
{
    public class ViewedManager
    {
        public ViewedManager() { }
        public static bool applyFilter(string from, string to)
        {
            DateTime dateFrom, dateTo, now = DateTime.Now;
            string sDateFrom, sDateTo;
            if (DateTime.TryParseExact(from, "yyyy-MM-dd HH:mm:ss:fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFrom))
            {
                sDateFrom = dateFrom.ToString("yyyy-MM-dd");
            }
            else
            {
                sDateFrom = null;
            }
            if (DateTime.TryParseExact(to, "yyyy-MM-dd HH:mm:ss:fff", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTo))
            {
                sDateTo = dateTo.ToString("yyyy-MM-dd");
            }
            else
            {
                sDateTo = null;
            }
            if(string.IsNullOrWhiteSpace(sDateFrom) && string.IsNullOrWhiteSpace(sDateTo))
            {
                return false;
            }
            string term = sDateFrom + ";" + sDateTo;
            string sql1 = @"
                UPDATE TBManagers
                SET 
	                TBManagers.FilterCurrentMark = '" + term + @"',
	                TBManagers.FilterCurrentValue = ISNULL(TBItems.FilterTotal, 0),
	                TBManagers.FilterCurrentLast = '" + now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"'
                FROM ViewedManager TBManagers
	                LEFT JOIN (
		                SELECT 
			                TBDays.ViewedManagerId AS ViewedManagerId,
			                SUM(TBDays.EstimateValue) AS FilterTotal
		                FROM
			                ViewedByDay TBDays
		                WHERE 1 = 1 ";
            if (!string.IsNullOrWhiteSpace(sDateFrom))
            {
                sql1 += " AND TBDays.EstimateDate >= '" + sDateFrom + "' ";
            }
            if (!string.IsNullOrWhiteSpace(sDateTo))
            {
                sql1 += " AND TBDays.EstimateDate <= '" + sDateTo + "' ";
            }
            sql1 += @"
		                GROUP BY TBDays.ViewedManagerId
	                ) TBItems ON TBManagers.Id = TBItems.ViewedManagerId
                WHERE TBManagers.FilterCurrentMark IS NULL
	                OR TBManagers.FilterCurrentMark != '"+ term + @"'
	                OR TBManagers.FilterCurrentValue IS NULL
	                OR TBManagers.FilterCurrentLast < TBManagers.UpdatedAt
            ";
            Connect.Exec(sql1);
            return true;
        }
        public static void createInstanceForAccount(string accountId)
        {
            string sql1 = @"
                SELECT COUNT(*) AS TOTALROWS
                FROM ViewedManager
                WHERE ViewedManager.AccountId = '"+ accountId + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            int countTotal = Int32.Parse(table1.Rows[0]["TOTALROWS"].ToString());
            if(countTotal == 0)
            {
                string sql2 = @"
                INSERT INTO ViewedManager(
	                AccountId,
	                TotalViewed,
	                UpdatedAt
                ) VALUES (
	                " + accountId + @",
	                0,
	                '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"'
                )";
                Connect.Exec(sql2);
            }
        }
        public static DataRow getByAccountId(string accountId)
        {
            string sql1 = @"
                SELECT TOP 1
	                ViewedManager.*
                FROM ViewedManager
                WHERE 1 = 1
	                AND ViewedManager.AccountId = " + accountId + @"
            ";
            DataTable table1 = Connect.GetTable(sql1);
            if(table1.Rows.Count > 0)
            {
                return table1.Rows[0];
            }
            else
            {
                return null;
            }
        }
        public static DataRow getByRefCode(string refCode)
        {
            string sql1 = @"
                SELECT TOP 1
	                ViewedManager.*
                FROM ViewedManager	
	                INNER JOIN tb_ThanhVien ON  ViewedManager.AccountId = tb_ThanhVien.idThanhVien
                WHERE 1 = 1
	                AND tb_ThanhVien.RefCode = '"+ refCode + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            if (table1.Rows.Count > 0)
            {
                return table1.Rows[0];
            }
            else
            {
                return null;
            }
        }
        public static void hasNewViewedByRefCode(string refCode)
        {
            if(refCode == null || refCode == "")
            {
                return;
            }
            DateTime now = DateTime.Now;
            string nowFormatDate = now.ToString("yyyy-MM-dd");
            string nowFormatDateTime = now.ToString("yyyy-MM-dd HH:mm:ss:fff");
            string sql1 = @"
                SELECT TOP 1
                    ViewedManager.AccountId AS AccountId,
	                ViewedManager.Id AS ViewedManagerId,
	                ViewedByDay.Id AS ViewedByDayId
                FROM ViewedManager
	                INNER JOIN tb_ThanhVien ON ViewedManager.AccountId = tb_ThanhVien.idThanhVien
	                LEFT JOIN ViewedByDay ON ViewedManager.Id = ViewedByDay.ViewedManagerId AND ViewedByDay.EstimateDate = '" + nowFormatDate + @"'
                WHERE tb_ThanhVien.RefCode = '"+ refCode + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            if(table1.Rows.Count > 0)
            {
                string accountId = table1.Rows[0]["AccountId"].ToString();
                string viewedManagerId = table1.Rows[0]["ViewedManagerId"].ToString();
                string viewedByDayId = table1.Rows[0]["ViewedByDayId"].ToString();
                if(viewedByDayId == "")
                {
                    string sql2 = @"
                        INSERT INTO ViewedByDay (
	                        AccountId,
	                        ViewedManagerId,
	                        EstimateDate,
	                        EstimateValue,
	                        UpdatedAt
                        ) OUTPUT INSERTED.Id VALUES (
	                        "+ accountId + @",
	                        "+ viewedManagerId + @",
	                        '"+nowFormatDate+@"',
	                        0,
	                        '"+ nowFormatDateTime + @"'
                        )
                    ";
                    DataTable table2 = Connect.GetTable(sql2);
                    if(table2.Rows.Count > 0)
                    {
                        viewedByDayId = table2.Rows[0]["Id"].ToString();
                    }
                    else
                    {
                        return;
                    }
                }
                if(accountId != "" && viewedManagerId != "" && viewedByDayId != "")
                {
                    string sql3 = @"
                        UPDATE ViewedManager
                        SET
	                        TotalViewed = ISNULL(TotalViewed, 0) + 1,
                            UpdatedAt = '" + nowFormatDateTime + @"'
                        WHERE ViewedManager.Id = " + viewedManagerId + @"
                    ";
                    Connect.Exec(sql3);

                    string sql4 = @"
                        UPDATE ViewedByDay
                        SET
	                        EstimateValue = ISNULL(EstimateValue, 0) + 1,
	                        EstimateH" + now.Hour + @" = ISNULL(EstimateH" + now.Hour + @", 0) + 1,
                            UpdatedAt = '" + nowFormatDateTime + @"'
                        WHERE ViewedByDay.Id = "+ viewedByDayId + @"
                    ";
                    Connect.Exec(sql4);
                }
            }
        }
    }
}
