﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Utilities
{
    public class Auth
    {
        public static void attempLogin(HttpResponse response, string accountId, string responseUrl = "/thong-tin-ca-nhan/ttcn")
        {
            HttpCookie cookie_TungTang_Login = new HttpCookie("TungTang_Login", accountId);
            cookie_TungTang_Login.Expires = DateTime.Now.AddDays(30);
            response.Cookies.Add(cookie_TungTang_Login);

            response.Redirect(responseUrl);
        }

        public static string getAccountId()
        {
            return getAccountId(HttpContext.Current.Request);
        }

        public static string getAccountId(HttpRequest request)
        {
            if (request.Cookies["TungTang_Login"] != null && !string.IsNullOrWhiteSpace(request.Cookies["TungTang_Login"].Value))
            {
                return request.Cookies["TungTang_Login"].Value.Trim();
            }
            else
            {
                return null;
            }
        }

        public static string getAdminId()
        {
            return getAdminId(HttpContext.Current.Request);
        }

        public static string getAdminId(HttpRequest request)
        {
            if (request.Cookies["AdminTungTang_Login"] != null && !string.IsNullOrWhiteSpace(request.Cookies["AdminTungTang_Login"].Value))
            {
                return request.Cookies["AdminTungTang_Login"].Value.Trim();
            }
            else
            {
                return null;
            }
        }

        public static DataRow getAccount()
        {
            return getAccount(HttpContext.Current.Request);
        }

        public static DataRow getAccount(HttpRequest request)
        {
            string id = getAccountId(request);
            if(string.IsNullOrWhiteSpace(id))
            {
                return null;
            }
            else
            {
                return Services.Account.getAccount(id);
            }
        }

        public static long getBalance()
        {
            string accountID = getAccountId();
            if(accountID == null)
            {
                return 0;
            }
            long balance = Services.Account.getBalance(accountID);
            return balance;
        }
    }
}