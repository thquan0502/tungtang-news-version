﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Utilities
{
    public class Post
    {
        public Post() {}
        public static string getUrl(string path)
        {
            return Utilities.Post.getUrl("", path);
        }
        public static string getUrl(string domain, string path)
        {
            return Utilities.Post.getUrl(domain, path, "");
        }
        public static string getUrl(string domain, string path, string sParams)
        {
            return domain + "/tdct/" + path + (sParams != "" ? "?" + sParams : "");
        }
        public static string getUrl(string domain, string path, IDictionary<string, string> addionalParams)
        {
            string sParams = "";
            int index1 = 0;
            if(addionalParams != null)
            {
                foreach(KeyValuePair<string, string> entry in addionalParams)
                {
                    if(index1 != 0)
                    {
                        sParams += "&";
                    }
                    sParams += entry.Key + "=" + entry.Value;
                    index1++;
                }
            }
            return Utilities.Post.getUrl(domain, path, sParams);
        }
        public static string getThumb(string path)
        {
            return Utilities.Post.getThumb("", path);
        }
        public static string getThumb(string domain, string path)
        {
            string url = "";
            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/td/slides/" + path))
                url = domain + "/images/td/slides/" + path;
            else
                url = domain + "/images/icons/noimage.png";
            return url;
        }

        /**
         * [0]: string: active pack
         * [1]: string: db pack
         * [2]: DateTime: db from
         * [3]: DateTime: db to
         * [4]: boolean: is in the past
         * [5]: boolean: is in the future
         */
        public static object[] getFeeInfo(DataRow row)
        {
            DateTime startTime = DateTime.Now;
            DateTime endTime = DateTime.Now;
            DateTime now = DateTime.Now;

            object[] result = new object[4];
            result[0] = "default_pack";
            result[1] = row["FeeCode"].ToString();
            result[4] = false;
            result[5] = false;
            if (!string.IsNullOrWhiteSpace(row["FeeStartTime"].ToString())){
                startTime = row.Field<DateTime>("FeeStartTime");
                if(now < startTime)
                {
                    result[4] = true;
                }
            }
            if (!string.IsNullOrWhiteSpace(row["FeeEndTime"].ToString()))
            {
                endTime = row.Field<DateTime>("FeeEndTime");
                if (now > endTime)
                {
                    result[5] = true;
                }
            }
            result[2] = startTime;
            result[3] = endTime;
            

            if (string.IsNullOrWhiteSpace((string)result[1]) || string.IsNullOrWhiteSpace(row["FeeStartTime"].ToString()) || string.IsNullOrWhiteSpace(row["FeeEndTime"].ToString()))
            {
                return result;
            }

            string pack = (string)result[1];

            if ((bool) result[4] || (bool) result[5])
            {
                return result;
            }
            else
            {
                result[0] = result[1];
                return result;
            }
        }

        public static string getPostFee(DataRow row)
        {
            string rs = "default_pack";
            if(string.IsNullOrWhiteSpace(row["FeeCode"].ToString()) || string.IsNullOrWhiteSpace(row["FeeStartTime"].ToString()) || string.IsNullOrWhiteSpace(row["FeeEndTime"].ToString()))
            {
                return rs;
            }

            DateTime startTime = row.Field<DateTime>("FeeStartTime");
            DateTime endTime = row.Field<DateTime>("FeeEndTime");
            DateTime now = DateTime.Now;

            if(now < startTime || now > endTime)
            {
                return rs;
            }

            return row.Field<string>("FeeCode");
        }
    }
}
