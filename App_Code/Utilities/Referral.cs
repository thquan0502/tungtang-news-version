﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Utilities
{
    public class Referral
    {
        public static void ownerApproveReferral(string ownerId, string collabId, string postId, int rate, double amount)
        {
            string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");

            DataRow dataTinDang = (new Models.TinDang()).getDataRowById(postId);
            if (dataTinDang == null)
            {
                return;
            }
            double price;
            try
            {
                price = Double.Parse(dataTinDang["TuGia"].ToString());
            }
            catch (Exception ex)
            {
                price = 0;
            }
            double commission = (new Models.TinDang()).calcCommission(price, rate, amount);

            string sql = @"
                EXEC [dbo].[PRC_OwnerApproveReferral]
		            " + ownerId + @",
		            " + collabId + @",
		            " + postId + @",
		            " + rate + @",
		            " + amount + @",
		            " + commission + @",
		            '" + sNow + @"'
            ";
            Connect.Exec(sql);
        }
        public static void ownerUpdateReferral(string ownerId, string referralId, string postId, int referralRate, double referralAmount)
        {
            string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");
            DataRow dataTinDang = (new Models.TinDang()).getDataRowById(postId);
            if (dataTinDang == null)
            {
                return;
            }
            double price;
            try
            {
                price = Double.Parse(dataTinDang["TuGia"].ToString());
            }
            catch (Exception ex)
            {
                price = 0;
            }
            double commission = (new Models.TinDang()).calcCommission(price, referralRate, referralAmount);
            string sql = @"
                EXEC [dbo].[PRC_OwnerUpdateReferral]
		            " + referralId + @",
		            " + ownerId + @",
		            " + referralRate + @",
		            " + referralAmount + @",
		            " + commission + @",
		            '" + sNow + @"'
            ";
            Connect.Exec(sql);
        }
        public static void ownerCancelReferral(string ownerId, string referralId)
        {
            string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");
            string sql = @"
                EXEC [dbo].[PRC_OwnerCancelReferral]
		            " + ownerId + @",
		            " + referralId + @",
		            '" + sNow + @"'
            ";
            Connect.Exec(sql);
        }
        public static void collabApproveReferral(string referralId, string collabId)
        {
            string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");
            string sql = @"
                EXEC [dbo].[PRC_CollabApproveReferral]
		            " + referralId + @",
		            " + collabId + @",
		            '" + sNow + @"'
            ";
            Connect.Exec(sql);
        }
        public static void collabDenyReferral(string referralId, string collabId)
        {
            string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");
            string sql = @"
                EXEC [dbo].[PRC_CollabDenyReferral]
		            " + collabId + @",
		            " + referralId + @",
		            '" + sNow + @"'
            ";
            Connect.Exec(sql);
        }
        public static void adminApproveReferral(string referralId)
        {
            string sNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff");
            string sql = @"
                EXEC [dbo].[PRC_AdminApproveReferral]
		            " + referralId + @",
		            '" + sNow + @"'
            ";
            Connect.Exec(sql);
        }
    }
}
