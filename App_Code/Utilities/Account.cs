﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Utilities
{
    public class Account
    {
        public Account() { }
        public static string getLinkThumbWithPath(string path)
        {
            string url = "/images/icons/signin.png";
            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + path.Trim()))
                url = "/images/user/" + path.Trim();
            return url;
        }
        public static void initRefCodeValues()
        {
            DataTable table1 = Connect.GetTable("SELECT tb_ThanhVien.idThanhVien FROM tb_ThanhVien WHERE tb_ThanhVien.RefCode IS NULL");
            for (int i = 0; i < table1.Rows.Count; i = i + 1)
            {
                Connect.Exec("UPDATE tb_ThanhVien SET tb_ThanhVien.RefCode = '" + System.Guid.NewGuid() + "' WHERE tb_ThanhVien.idThanhVien = " + table1.Rows[i]["idThanhVien"].ToString());
            }
        }
        public static string getAuthAccountId(HttpRequest request)
        {
            if (request.Cookies["TungTang_Login"] == null || request.Cookies["TungTang_Login"].Value.Trim() == "")
            {
                return null;
            }
            else
            {
                return request.Cookies["TungTang_Login"].Value.Trim();
            }
        }
        public static DataTable getTableByEmail(string email)
        {
            string sql = @"
                SELECT TOP 1 
                    tb_ThanhVien.* 
                FROM tb_ThanhVien
                WHERE tb_ThanhVien.Email = '"+ email + @"'
            ";
            return Connect.GetTable(sql);
        }
        public static DataTable getTableByGoogleId(string googleId)
        {
            string sql = @"
                SELECT TOP 1 
                    tb_ThanhVien.* 
                FROM tb_ThanhVien
                WHERE tb_ThanhVien.GoogleId = '" + googleId + @"'
            ";
            return Connect.GetTable(sql);
        }
        public static DataTable getTableByFacebookId(string facebookId)
        {
            string sql = @"
                SELECT TOP 1 
                    tb_ThanhVien.* 
                FROM tb_ThanhVien
                WHERE tb_ThanhVien.FacebookId = '" + facebookId + @"'
            ";
            return Connect.GetTable(sql);
        }
        public static string getIdByPhone(string phoneNumber)
        {
            string sql = @"
                SELECT TOP 1 
                    tb_ThanhVien.idThanhVien AS AccountId
                FROM tb_ThanhVien
                WHERE tb_ThanhVien.SoDienThoai = '"+ phoneNumber +@"'
            ";
            DataTable table = Connect.GetTable(sql);
            if(table.Rows.Count > 0)
            {
                return table.Rows[0]["AccountId"].ToString();
            }
            else
            {
                return null;
            }
        }
        public static long getBalance(DataRow row)
        {
            if (row == null) return 0;
            if (string.IsNullOrWhiteSpace(row["BalanceAmount"].ToString()))
            {
                return 0;
            }
            else
            {
                return (long)row["BalanceAmount"];
            }
        }
        public static long getBalanceRecharge(DataRow row)
        {
            if (row == null) return 0;
            if (string.IsNullOrWhiteSpace(row["BalanceRecharge"].ToString()))
            {
                return 0;
            }
            else
            {
                return (long)row["BalanceRecharge"];
            }
        }
        public static long getBalanceSpent(DataRow row)
        {
            if (row == null) return 0;
            if (string.IsNullOrWhiteSpace(row["BalanceSpent"].ToString()))
            {
                return 0;
            }
            else
            {
                return (long)row["BalanceSpent"];
            }
        }
    }
}
