﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;

/// <summary>
/// Summary description for ErrorManager
/// </summary>
public class LogManager
{
    public LogManager()
    {
    }
    public static void output(string content)
    {
        string fileName = Path.Combine(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/App_Code/LogManager/Bin", DateTime.Now.ToString("ddMMyyyy") + ".txt");
        File.AppendAllText(fileName, content);
        File.AppendAllText(fileName, "\n");
    }
}