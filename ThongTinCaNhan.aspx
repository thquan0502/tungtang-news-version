﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ThongTinCaNhan.aspx.cs" Inherits="ThongTinCaNhan" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        .container-sliderTrang {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            margin-top:5px;
        }

        .box-minmax {
            width: 100%;
            display: flex;
            justify-content: space-between;
            font-size: 10px;
            color: #888;
        }

        .range-slider {
            width: 100%;
            position: relative;
            padding-top: 35px;
        }

        .rs-range {
            margin-top: 10px;
            width: 100%;
            -webkit-appearance: none;
        }

            .rs-range:focus {
                outline: none;
            }

            .rs-range::-webkit-slider-runnable-track {
                width: 100%;
                height: 28px;
                cursor: pointer;
                box-shadow: none;
                background: url('../../images/icons/colors.png') no-repeat no-repeat;
                background-size: contain;
                border-radius: 0px;
                border: 0px solid black;
            }

            .rs-range::-moz-range-track {
                width: 100%;
                height: 28px;
                cursor: pointer;
                box-shadow: none;
                background: url('../../images/icons/colors.png') no-repeat no-repeat;
                background-size: contain;
                border-radius: 0px;
                border: 0px solid black;
            }

            .rs-range::-webkit-slider-thumb {
                display: none;
            }

            .rs-range::-moz-focus-outer {
                border: 0;
            }

        .rs-label {
            position: absolute;
            display: block;
            width: max-content;
            min-width: 105px;
            height: 30px;
            margin-left: -52px; 
            border-radius: 5px;
            top: 0;
            text-align: center;
            padding-top: 5px;
            border: 1px solid #ccc;
            left: attr(value);
            color: black;
            font-style: normal;
            font-weight: normal;
            line-height: normal;
            font-size: 12px;
            cursor:pointer;
        }
        .rs-label:hover .ArrowBullet
        {
            background-color:#ededed;
        } 
        .rs-label:hover  
        {
            background-color:#ededed;
        }

        .ArrowBullet {
            position: absolute;
            height: 10px;
            width: 10px;
            background-color: white;
            border-bottom: 1px #cccccc solid;
            border-right: 1px #cccccc solid;
            bottom: -5.5px;
            left: 45%;
            transform: rotate(45deg);
        }
    </style>
    <script>
        function DeleteTinDang(idTinDang) {
            if (confirm("Bạn có muốn xóa tin này không ?")) {
                //alert(idSlide);
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Lỗi !")
                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=DeleteTinDang&idTinDang=" + idTinDang, true);
                xmlhttp.send();
            }
        }
        function CapNhatHetHan(idTinDang) {
            var HetHan = document.getElementById("slHetHan_" + idTinDang);
            if (HetHan.disabled == true) {
                document.getElementById("slHetHan_" + idTinDang).disabled = false;
                document.getElementById("btHetHan_" + idTinDang).innerHTML = "<img class='imgedit' src='../images/icons/save.png' style='width:25px; cursor:pointer'/>Lưu</a>";
            } else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Lỗi !")
                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=CapNhatHetHan&idTinDang=" + idTinDang + "&HetHan=" + HetHan.value, true);
                xmlhttp.send();
            }
        }
        function ThongKe(TD) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        $("#modalThongKe").modal({
                            fadeDuration: 200,
                            showClose: false
                        });
                        $("header").css({ "z-index": "0" });
                        menuBar_fixed = false;
                        $("#dvDSThongKe").html(xmlhttp.responseText);
                    }
                    else
                        alert("Lỗi !");
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=ThongKeTinDang&TD=" + TD, true);
            xmlhttp.send();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container">

        <div class="row" style="margin-bottom: 20px;">
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div class="panel jobs-board-listing with-mc no-padding no-border">
                        <div class="panel-content" style="padding: 10px">
                            <div class="job-list scrollbar m-t-lg">
                                <div class="ss-wrapper">
                                    <div class="ss-content">
                                        <div class="titlemain" style="border-right: 1px solid #eee; height: auto;">
                                            <div class="avtShop" id="imgAvtShopID">
                                                <img src="/images/icons/signin.png" style="width: 100%; height: 115px; object-fit: cover;" id="imgLinkAnh" runat="server" />
                                            </div>
                                            <div class="nameShop" id="txtNameShopID" runat="server">
                                            </div>
                                            <a class="btnEditInfoUser" id="btnEditInfoUserID" runat="server" href="/thong-tin/tt">Chỉnh sửa thông tin</a>
                                        </div>

                                        <div class="titlemain" style="height: auto;">
                                            <p class="DateShopApproved"><i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày tham gia: <span id="txtDateShopApprovedID" runat="server" style="color: black;"></span></p>
                                            <p class="ShopAddress"><i class="fa fa-map-marker" style="padding: 0 13px;"></i>Địa chỉ: <span id="txtShopAddressID" runat="server" style="color: black;"></span></p>
                                            <p class="ShopPhone"><i class="fa fa-phone" style="padding: 0 11px;"></i>Số điện thoại: <span id="txtShopPhoneID" runat="server" style="color: black;"></span></p>
                                            <p class="ShopEmail"><i class="fa fa-envelope" style="padding: 0 10px;"></i>Email: <span id="txtShopEmailID" runat="server" style="color: black;"></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div class="panel jobs-board-listing with-mc no-padding no-border" style="border-radius: 0">
                        <div class="panel-content" id="tabChoThue" style="padding: 10px">
                            <div class="job-list scrollbar m-t-lg">
                                <h3>TIN ĐĂNG CỦA KÊNH</h3>
                                <div id="dvQuanLyTinDang" runat="server" style='overflow: auto; margin-bottom: 35px;'>

                                    <%--<table class="table table-bordered">
                                                <tr>
                                                    <td>
                                                        <span class="score">
                                                            <img src="http://shopfull.asia/images/td/slides/25920172055156364196971555521522.jpg" class="imgmenu" />
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <p style="padding-top:5px"><b>CHO THUÊ XE MÁY GIÁ RẺ, THỦ TỤC ĐƠN GIẢN, NHANH GỌN VÀ CHUYÊN NGHIỆP</b></p>
                                                        <p>Giá: 500K/ ngày</p>
                                                        <p>Ngày đăng: 101/10/2017</p>
                                                    </td>
                                                    <td>
                                                        Chưa duyệt
                                                    </td>
                                                    <td>
                                                        <img src="http://shopfull.asia/Images/icons/edit.png" style="width:25px" /> |
                                                        <img src="http://shopfull.asia/Images/icons/delete.png" style="width:25px" />
                                                    </td>
                                                </tr>
                                            </table>--%>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="hdHinhAnh" runat="server" />

        <div class="modal-ThongKe-cls">
            <div id="modalThongKe" class="modal" style="overflow: hidden;">
                <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">Thống kê tin đăng</div>
                <div id="dvDSThongKe" style="overflow: hidden; padding: 10px;">
                </div>
            </div>
        </div>


        <script>
            function showSliderValue() {
                var rangeSlider = document.getElementById("rs-range-line");
                var rangeBullet = document.getElementById("rs-bullet");
                var rangeBullet2 = document.getElementById("rs-bullet2");
                var bulletPosition = (rangeBullet2.innerHTML / rangeSlider.max); 
                if (bulletPosition == 1) {
                    $(".ArrowBullet").attr("style", "left:75%;");
                    rangeBullet.style.right = "0%";
                }
                else
                    rangeBullet.style.left = (bulletPosition * 100) + "%";

                if (rangeBullet2.innerHTML == "1") {
                    $(".ArrowBullet").attr("style", "left:15%;");
                    rangeBullet.style.left = "12%";
                }
            }
        </script>
    </section>
</asp:Content>

