﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;
using System.Activities.Expressions;
using System.Text;

public partial class ThongTinCaNhan : System.Web.UI.Page
{
    string Domain = "";
    string idThanhVien = "";
    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 1;
    int Pagecd = 1;
    int Pagebtc = 1;
    int Pagethh = 1;

    int MaxPage = 0;
    int MaxPagecd = 0;
    int MaxPagebtc = 0;
    int MaxPagethh = 0;

    int PageSize = 10;
    int PageSizecd = 10;
    int PageSizebtc = 10;
    int PageSizethh = 10;
    protected string templateFilterCode = "";
    protected string templateJsonCategories1 = "";
    protected string templateJsonCategories2 = "";
    protected Dictionary<string, object> configs;
    protected string refCode = null;
    protected string templateFilterPostCode = "";
    protected string filterPostCode = null;
    protected string templateFilterPostOwner = "";
    protected string filterPostOwner = null;
    protected string templateJsonOwners = "";
    protected string templateJsonCode = "";
    protected string templateJsonCode2 = "";
    protected string templateJsonCode3 = "";
    protected string templateJsonCode4 = "";

    protected string templateFilterOwnerApprovedAtFrom = "";
    protected string filterOwnerApprovedAtFromAltDateTime = "";
    protected string filterOwnerApprovedAtFromAltDate = "";

    protected string templateFilterOwnerApprovedAtTo = "";
    protected string filterOwnerApprovedAtToAltDateTime = "";
    protected string filterOwnerApprovedAtToAltDate = "";

    protected string templateFilterPostTitle = "";
    protected string filterPostTitle = null;

    protected string templateFilterPostPrice = "";
    protected string filterPostPrice = null;
    protected double filterFromPostPrice = 0;
    protected double filterToPostPrice = 0;

    protected string templateFilterPostCommission = "";
    protected string filterPostCommission = null;
    protected double filterFromPostCommission = 0;
    protected double filterToPostCommission = 0;

    protected string templateFilterCreatedAtFrom = "";
    protected string filterCreatedAtFromAltDateTime = "";
    protected string filterCreatedAtFromAltDate = "";

    protected string templateFilterCreatedAtTo = "";
    protected string filterCreatedAtToAltDateTime = "";
    protected string filterCreatedAtToAltDate = "";

    protected string templateFilterCreatedUpFrom = "";
    protected string filterCreatedUpFromAltDateTime = "";
    protected string filterCreatedUpFromAltDate = "";

    protected string templateFilterCreatedUpTo = "";
    protected string filterCreatedUpToAltDateTime = "";
    protected string filterCreatedUpToAltDate = "";


    protected string templateFilterCreatedPostFrom = "";
    protected string filterCreatedPostFromAltDateTime = "";
    protected string filterCreatedPostFromAltDate = "";

    protected string templateFilterCreatedPostTo = "";
    protected string filterCreatedPostToAltDateTime = "";
    protected string filterCreatedPostToAltDate = "";

    protected string templateFilterCreatedDeniedFrom = "";
    protected string filterCreatedDeniedFromAltDateTime = "";
    protected string filterCreatedDeniedFromAltDate = "";

    protected string templateFilterCreatedDeniedTo = "";
    protected string filterCreatedDeniedToAltDateTime = "";
    protected string filterCreatedDeniedToAltDate = "";

    protected string templateJsonReferralStatus = "";
    protected string templateFilterReferralStatus = "";
    protected string filterReferralStatus = null;

    protected string templateFilterReferralStatusOther2 = "";
    protected string filterReferralStatusOther2 = null;

    protected string templateFilterCategory1 = "";
    protected string filterCategory1 = null;

    protected string templateFilterCategory2 = "";
    protected string filterCategory2 = null;

    protected string templateFilterCity = "";
    protected string filterCity = null;

    protected string templateFilterDistrict = "";
    protected string filterDistrict = null;

    protected string templateFilterWard = "";
    protected string filterWard = null;
    string sGia = "";
    string sHoaHong = "";
    string Tabbtn = "";
    private string CheckTab = "";

    protected Models.TinDang tinDangHelper = new Models.TinDang();

    protected string activeTable_html = "";
    protected int activeTable_total = 0;

    protected string pendingTable_html = "";
    protected int pendingTable_total = 0;

    protected string deniedTable_html = "";
    protected int deniedTable_total = 0;

    protected string completedTable_html = "";
    protected int completedTable_total = 0;

    protected string renovateSuccessMessage = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        {
            try
            {
                string[] Url = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "").Split('-', '?');
                string v = Url[Url.Length - 1];
                idThanhVien = StaticData.getField("Tb_ThanhVien", "idThanhVien", "TenDangNhap", Url[4]).Trim();
                if (idThanhVien == "")
                {
                    try
                    {
                        idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
                    }
                    catch
                    {
                        Response.Redirect("/dang-nhap/dn");
                    }
                }
            }
            catch
            {
                try
                {
                    idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
                }
                catch
                {
                    Response.Redirect("/dang-nhap/dn");
                }
            }

            try
            {
                if (Request.QueryString["Gia"].Trim() != "")
                {
                    sGia = Request.QueryString["Gia"].Trim();
                    txtRangeLuotXem.Value = sGia;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["HoaHong"].Trim() != "")
                {
                    sHoaHong = Request.QueryString["HoaHong"].Trim();
                    txtRangeHoaHong.Value = sHoaHong;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["Tab"].Trim() != "")
                {
                    Tabbtn = Request.QueryString["Tab"].Trim();
                    Tabip.Value = Tabbtn;
                }
            }
            catch { }
            try
            {
                Page = int.Parse(Request.QueryString["Page"].ToString());
                //ipPagedd.Value = Request.QueryString["Page"].ToString();
            }
            catch
            {
                //Page = 1;
            }

            try
            {
                Pagecd = int.Parse(Request.QueryString["Pagecd"].ToString());
                //ipPagecd.Value = Request.QueryString["Pagecd"].ToString();
            }
            catch
            {
                //Pagecd = 1;
            }

            try
            {
                Pagebtc = int.Parse(Request.QueryString["Pagebtc"].ToString());
                //ipPagebtc.Value = Request.QueryString["Pagebtc"].ToString();
            }
            catch
            {
                //Pagebtc = 1;                
            }

            try
            {
                Pagethh = int.Parse(Request.QueryString["Pagethh"].ToString());
                //ipPagethh.Value = Request.QueryString["Pagethh"].ToString();
            }
            catch
            {
                //Pagethh = 1;
            }
        }
        if (!this.IsPostBack)
        {
            this.checkRenovateSuccess();
        }
        this.configs = Utilities.Config.getConfigs();
        generateFilterInputs();
        LoadThongTinThanhVien();
        loadCategories();
        loadidPost();
        loadidPost2();
        loadidPost3();
        loadidPost4();
        //   LoadTinDang();
        loadStatusReferralTemplate();
        initAccountSelledDropdownList();
        checkSoDT();
        renderAccountPostTable();
        generatePendingTable();
        generateDeniedTable();
        generateCompletedTable();
        loadOwners();
        if (Tabbtn == "1" || Tabbtn == "")
        {
            renderAccountPostTable();
            divAccountPostTable2.Visible = false;
            divAccountPostTable3.Visible = false;
            divAccountPostTable4.Visible = false;
            divAccountPostTable6.Visible = false;
            divAccountPostPagination3.Visible = false;
            divAccountPostPagination4.Visible = false;
            divAccountPostPagination5.Visible = false;
            divAccountPostPagination6.Visible = false;
            divAccountPostPagination7.Visible = false;
            divAccountPostPagination8.Visible = false;
            divAccountPostPagination9.Visible = false;
            divAccountPostPagination10.Visible = false;
        }
        if (Tabbtn == "2")
        {
            generatePendingTable();
            divAccountPostTable.Visible = false;
            divAccountPostTable3.Visible = false;
            divAccountPostTable4.Visible = false;
            divAccountPostTable6.Visible = false;
            divAccountPostPagination1.Visible = false;
            divAccountPostPagination2.Visible = false;
            divAccountPostPagination5.Visible = false;
            divAccountPostPagination6.Visible = false;
            divAccountPostPagination7.Visible = false;
            divAccountPostPagination8.Visible = false;
            divAccountPostPagination9.Visible = false;
            divAccountPostPagination10.Visible = false;
        }
        if (Tabbtn == "3")
        {
            generateDeniedTable();
            divAccountPostTable2.Visible = false;
            divAccountPostTable.Visible = false;
            divAccountPostTable4.Visible = false;
            divAccountPostTable6.Visible = false;
            divAccountPostPagination3.Visible = false;
            divAccountPostPagination4.Visible = false;
            divAccountPostPagination1.Visible = false;
            divAccountPostPagination2.Visible = false;
            divAccountPostPagination7.Visible = false;
            divAccountPostPagination8.Visible = false;
            divAccountPostPagination9.Visible = false;
            divAccountPostPagination10.Visible = false;
        }
        if (Tabbtn == "4")
        {

            generateCompletedTable();
            divAccountPostTable2.Visible = false;
            divAccountPostTable3.Visible = false;
            divAccountPostTable.Visible = false;
            divAccountPostTable6.Visible = false;
            divAccountPostPagination3.Visible = false;
            divAccountPostPagination4.Visible = false;
            divAccountPostPagination5.Visible = false;
            divAccountPostPagination6.Visible = false;
            divAccountPostPagination1.Visible = false;
            divAccountPostPagination2.Visible = false;
            divAccountPostPagination9.Visible = false;
            divAccountPostPagination10.Visible = false;
        }
        if (Tabbtn == "TDN")
        {
            LoadTinNgoai();
            divAccountPostTable2.Visible = false;
            divAccountPostTable3.Visible = false;
            divAccountPostTable.Visible = false;
            divAccountPostTable4.Visible = false;
            divAccountPostPagination3.Visible = false;
            divAccountPostPagination4.Visible = false;
            divAccountPostPagination5.Visible = false;
            divAccountPostPagination6.Visible = false;
            divAccountPostPagination1.Visible = false;
            divAccountPostPagination2.Visible = false;
            divAccountPostPagination7.Visible = false;
            divAccountPostPagination8.Visible = false;
        }
        if (idThanhVien == "")
        {
            LoadTinNgoai();
        }

        //loadOwners();        
    }
    static string SDT_ThanhVien = "";
    static string Tab = "";

    protected void checkRenovateSuccess()
    {
        HttpCookie c = Request.Cookies.Get("renovate_success");
        if (c != null)
        {
            string title = c.Value;
            long id;
            if (!string.IsNullOrWhiteSpace(title))
            {
                this.renovateSuccessMessage = "Lên TOP tin đăng thành công, " + title;
            }
            Response.Cookies["renovate_success"].Expires = DateTime.Now.AddSeconds(-1);
        }
        else
        {
            this.renovateSuccessMessage = "";
        }
    }

    protected void LoadTinNgoai()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();
        //  string b = ipktsv.Value.Trim();
        if (Request.QueryString["Pagek"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Pagek"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        if (this.refCode != null)
        {
            postParams = new Dictionary<string, string>();
            postParams.Add("RefCode", this.refCode);
        }

        sql1 += " FROM ";
        sql1 += "   ( ";
        sql1 += "     SELECT ";
        sql1 += "       ROW_NUMBER() OVER ( ";
        sql1 += "         ORDER BY ";
        sql1 += "           tbPost.NgayDang DESC ";
        sql1 += "       ) AS RowNumber, ";
        sql1 += "        tbPost.idTinDang AS PostId,";
        sql1 += "        tbPost.DuongDan AS PostPath,";
        sql1 += "        tbPost.TieuDe AS PostTitle,";
        sql1 += "        tbPost.Code AS PostCode,";
        sql1 += "        tbPost.TuGia AS PostPrice,";
        sql1 += "        tbPost.Ishot AS Ishot,";
        sql1 += "        tbPost.LoaiTinDang AS LoaiTinDang,";
        sql1 += "        tbPost.NgayGuiDuyet AS PostSubmitAt,";
        sql1 += "        tbPost.idTinh AS idTinh,";
        sql1 += "        tbPost.idHuyen AS idHuyen,";
        sql1 += "        tbPost.idPhuongXa AS idPhuongXa,";
        sql1 += "        tbPost.NgayDang AS PostCreatedAt,";
        sql1 += "        tbPost.isDuyet AS isDuyet,";
        sql1 += "        tbPost.SoLuotXem AS SoLuotXem,";
        sql1 += "        ISNULL(tbPost.Commission, 0) AS PostCommission,";
        sql1 += "       ISNULL(tbPost.ReferralRate, 0) AS PostRate,";
        sql1 += "        ISNULL(tbPost.ReferralAmount, 0) AS PostAmount";
        sql1 += "     FROM  tb_TinDang tbPost ";
        sql1 += "       LEFT JOIN tb_ThanhVien tbAccount ON tbPost.idThanhVien = tbAccount.idThanhVien ";
        sql1 += "       LEFT JOIN tb_DanhMucCap1 TBCategories1 ";
        sql1 += "           ON tbPost.idDanhMucCap1 = TBCategories1.idDanhMucCap1 ";
        sql1 += "       LEFT JOIN tb_DanhMucCap2 TBCategories2 ";
        sql1 += "           ON tbPost.idDanhMucCap2 = TBCategories2.idDanhMucCap2 ";
        sql1 += "       LEFT JOIN City TBOwnerCities ";
        sql1 += "           ON tbAccount.idTinh = TBOwnerCities.id ";
        sql1 += "       LEFT JOIN District TBOwnerDistricts ";
        sql1 += "           ON tbAccount.idHuyen = TBOwnerDistricts.id ";
        sql1 += "       LEFT JOIN tb_PhuongXa TBOwnerWards ";
        sql1 += "           ON tbAccount.idPhuongXa = TBOwnerWards.id ";
        sql1 += "     WHERE  1 = 1 ";
        sql1 += "       AND ISNULL(tbPost.isHetHan, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.IsDraft, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.isDuyet, 0) = 1 ";
        sql1 += "       AND tbPost.idThanhVien = '" + idThanhVien + @"'";
        sql1 += "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());
        // nbdaduyet.InnerText = table2.Rows[0]["TotalRows"].ToString();
        DataTable table3 = Connect.GetTable(sql3);
        //  if (table3.Rows.Count == 0)
        //  {
        //      html += "<tr><td colspan='8' style='text-align: center;'>Không có kết quả</td></tr>";
        //   }
        int stt = page * 10 - 9;
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            // string url = Utilities.Post.getUrl(this.configs["domain"].ToString(), table3.Rows[i]["PostPath"].ToString(), postParams);
            string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table3.Rows[i]["PostTitle"].ToString().Trim()));
            string urlTinDangChiTiet = Domain + "/tdct/" + table3.Rows[i]["PostPath"].ToString();
            string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(table3.Rows[i]["PostId"].ToString());
            string LoaiTinDang = "Cần bán";
            if (table3.Rows[i]["LoaiTinDang"].ToString().Trim() != "CanBan")
                LoaiTinDang = "Cần mua";
            string TenHuyen1 = StaticData.getField("District", "Ten", "Id", table3.Rows[i]["idHuyen"].ToString());
            string idTinh1 = table3.Rows[i]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            string idPhuongXa1 = table3.Rows[i]["idPhuongXa"].ToString();
            string TenPhuongXa1 = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa1);
            string chuoi_DiaDiem = "";
            //if (TenPhuongXa1 != "")
            //    chuoi_DiaDiem = TenPhuongXa1;
            //else
            //{
            //    if (TenHuyen1 != "")
            //        chuoi_DiaDiem = TenHuyen1;
            //    else
            //    {
            //        if (TenTinh1 != "")
            //            chuoi_DiaDiem = TenTinh1;
            //        else
            //            chuoi_DiaDiem = "Toàn quốc";
            //    }
            //}
            chuoi_DiaDiem = TenTinh1;
            if (TenTinh1 == "")
                chuoi_DiaDiem = "Toàn quốc";

            if (table3.Rows[i]["isDuyet"].ToString().Trim().ToUpper() == "TRUE" || table3.Rows[i]["isDuyet"].ToString().Trim().ToUpper() == "FALSE")
            {

            }
            else
            {
                urlTinDangChiTiet = "#";
            }


            html += @" <div class='sc-jDwBTQ iVBwZP' style='border-top: 1px solid #e9ebee;'>
                                        <li>
                                            <a href='" + urlTinDangChiTiet + @"'>
                                                <div class='ctAdItemContainer'>
                                                   
                                                    <div class='sc-gPEVay iEksLa' style='width:100px;'>
                                                        <div class='sc-kgoBCf dPyyiW'>
                                                            
                                                            <div class='sc-kGXeez gUVkqr'>
                                                                <img style='width:91%' src='" + urlHinhAnh + @"'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='sc-iRbamj hlfpUQ'>
                                                        <div class='sc-dxgOiQ eBUnVf'>
                                                            <h3 class='sc-ckVGcZ eXhssu'>" + table3.Rows[i]["PostTitle"].ToString() + @"</h3>
                                                        </div>
                                                        <div class='sc-eNQAEJ fjxPoY'>
                                                            <div class='sc-hMqMXs ksOCAJ'><span>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString()) + @"</span></div>
                                                        </div>
                                                    </div>
                                                </div>";
            if (table3.Rows[i]["Ishot"].ToString().Trim() == "True")
                html += @"<div class='ctRibbonAd' style='background-color: rgb(245, 154, 0); border-color: rgb(245, 154, 0); color: rgb(255, 255, 255);z-index:0;margin-top: -13px;margin-right: -3px;'>PRO</div>";
            html += @"       </a>

                                            <div class='sc-kkGfuU hcqRnj'>
                                                <div class='sc-iAyFgw jtnjqF'>
                                                    <div class='sc-hSdWYo lnWlnm'>
                                                        <div>
                                                            <span class='sc-jAaTju iejxrf'>" + LoaiTinDang + @"</span>
                                                        </div>
                                                    </div>
                                                    <div class='sc-brqgnP iQeQii'><span>" + TinhThoiGianLucDang(DateTime.Parse(table3.Rows[i]["PostCreatedAt"].ToString())) + @"</span></div>
                                                    <div class='sc-brqgnP iQeQii'><span>" + chuoi_DiaDiem + @"</span></div>
					 <div class='sc-cMljjf bdlUrd'><span><i class='fa fa-eye'></i> " + table3.Rows[i]["SoLuotXem"] + @"</span></div>
                                                </div>
                                            </div> 
                                        </li>
                                    </div>";
        }

        //string sqlTinDang = "select * from tb_TinDang td";
        //DataTable tbTinDang = Connect.GetTable(sqlTinDang);
        SDT_ThanhVien = StaticData.getField("tb_ThanhVien", "TenDangNhap", "idThanhVien", idThanhVien);
        divAccountPostTable6.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPaginationSmalldnn(totalItems, perPage, page, "/thong-tin-ca-nhan/ttcn-" + SDT_ThanhVien + "?Tab=TDN", paginationParams);
        divAccountPostPagination9.InnerHtml = htmlPagination;
        divAccountPostPagination10.InnerHtml = htmlPagination;
        //divAccountPostTable6.Visible = Visible;
        //divAccountPostPagination9.Visible = true;
        //divAccountPostPagination10.Visible = true;        
    }
    protected void renderAccountPostTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        //   Tab = ipktsv.Value.Trim();
        string kt = ipPagebtc.Value.Trim();
        string ktd = txtRangeLuotXem.Value.Trim();
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();
        //  string b = ipktsv.Value.Trim();
        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        if (this.refCode != null)
        {
            postParams = new Dictionary<string, string>();
            postParams.Add("RefCode", this.refCode);
        }

        sql1 += " FROM ";
        sql1 += "   ( ";
        sql1 += "     SELECT ";
        sql1 += "       ROW_NUMBER() OVER ( ";
        sql1 += "         ORDER BY ";
        sql1 += "           tbPost.NgayGuiDuyet DESC ";
        sql1 += "       ) AS RowNumber, ";
        sql1 += "        tbPost.idTinDang AS PostId,";
        sql1 += "        tbPost.DuongDan AS PostPath,";
        sql1 += "        tbPost.TieuDe AS PostTitle,";
        sql1 += "        tbPost.Code AS PostCode,";
        sql1 += "        tbPost.TuGia AS PostPrice,";
        sql1 += "        tbPost.NgayGuiDuyet AS PostSubmitAt,";
        sql1 += "        tbPost.NgayAdminDuyet AS AdminSubmitAt,";
        sql1 += "        tbPost.NgayDang AS PostCreatedAt,";
        sql1 += "        ISNULL(tbPost.Commission, 0) AS PostCommission,";
        sql1 += "        ISNULL(tbPost.ReferralRate, 0) AS PostRate,";
        sql1 += "        ISNULL(tbPost.ReferralAmount, 0) AS PostAmount,";
        sql1 += "        ISNULL(tbPost.FeeCode, '') AS FeeCode,";
        sql1 += "        ISNULL(tbPost.FeeStartTime, '') AS FeeStartTime,";
        sql1 += "        ISNULL(tbPost.FeeEndTime, '') AS FeeEndTime";
        sql1 += "     FROM  tb_TinDang tbPost ";
        sql1 += "       LEFT JOIN tb_ThanhVien tbAccount ON tbPost.idThanhVien = tbAccount.idThanhVien ";
        sql1 += "       LEFT JOIN tb_DanhMucCap1 TBCategories1 ";
        sql1 += "           ON tbPost.idDanhMucCap1 = TBCategories1.idDanhMucCap1 ";
        sql1 += "       LEFT JOIN tb_DanhMucCap2 TBCategories2 ";
        sql1 += "           ON tbPost.idDanhMucCap2 = TBCategories2.idDanhMucCap2 ";
        sql1 += "       LEFT JOIN City TBOwnerCities ";
        sql1 += "           ON tbAccount.idTinh = TBOwnerCities.id ";
        sql1 += "       LEFT JOIN District TBOwnerDistricts ";
        sql1 += "           ON tbAccount.idHuyen = TBOwnerDistricts.id ";
        sql1 += "       LEFT JOIN tb_PhuongXa TBOwnerWards ";
        sql1 += "           ON tbAccount.idPhuongXa = TBOwnerWards.id ";
        sql1 += "     WHERE  1 = 1 ";
        sql1 += "       AND ISNULL(tbPost.isHetHan, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.IsDraft, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.isDuyet, 0) = 1 ";
        sql1 += "       AND tbPost.idThanhVien = '" + idThanhVien + @"'";
        if (!string.IsNullOrWhiteSpace(filterPostCode) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.Code LIKE '%" + filterPostCode + "%' ";
            paginationParams.Add("FilterPostCode", filterPostCode);
        }
        if (!string.IsNullOrWhiteSpace(filterPostOwner) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.idThanhVien = " + filterPostOwner + " ";
            paginationParams.Add("FilterPostOwner", filterPostOwner);
        }
        if (!string.IsNullOrWhiteSpace(filterPostTitle) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.TieuDe LIKE N'%" + filterPostTitle + "%' ";
            paginationParams.Add("FilterPostTitle", filterPostTitle);
        }
        //Giá

        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "0" && Tabbtn == "1")
        {
            if (this.filterFromPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += " OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += " AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100500" && Tabbtn == "1")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500500" && Tabbtn == "1")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        //Hoa Hồng

        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "0" && Tabbtn == "1")
        {
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  >= " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100" && Tabbtn == "1")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500" && Tabbtn == "1")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500500" && Tabbtn == "1")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500500" && Tabbtn == "1")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtFromAltDate) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.NgayDayLenTop >= '" + filterCreatedAtFromAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtToAltDate) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.NgayDayLenTop <= '" + filterCreatedAtToAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedUpFromAltDate) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.NgayDang >= '" + filterCreatedUpFromAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedUpToAltDate) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.NgayDang <= '" + filterCreatedUpToAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory1) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.idDanhMucCap1 = '" + filterCategory1 + "' ";
            paginationParams.Add("FilterCategory1", filterCategory1);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory2) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.idDanhMucCap2 = '" + filterCategory2 + "' ";
            paginationParams.Add("FilterCategory2", filterCategory2);
        }
        if (!string.IsNullOrWhiteSpace(filterCity) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard) && Tabbtn == "1")
        {
            sql1 += "       AND tbPost.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        sql1 += "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());
        nbdaduyet.InnerText = table2.Rows[0]["TotalRows"].ToString();
        DataTable table3 = Connect.GetTable(sql3);
        html += "<table id='postTable' class='table table-bordered table-active text-sm' style='text-align:center;'>";
        html += "<thead>";
        html += "<tr>";
        html += "<th style='min-width:20px'>STT</th>";
        html += "<th>Hình ảnh</th>";
        html += "<th>Thông Tin</th>";
        html += "<th>Loại Tin</th>";
        html += "<th>Tỉ lệ hoa hồng</th>";
        html += "<th>Hoa hồng cố định</th>";
        html += "<th>Hoa hồng</th>";
        html += "<th>Ngày gửi duyệt</th>";
        html += "<th>Ngày duyệt</th>";
        html += "<th>Hành động</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        //  if (table3.Rows.Count == 0)
        //  {
        //      html += "<tr><td colspan='8' style='text-align: center;'>Không có kết quả</td></tr>";
        //   }
        int stt = page * 10 - 9;
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(table3.Rows[i]["PostId"].ToString());
            string url = Utilities.Post.getUrl(this.configs["domain"].ToString(), table3.Rows[i]["PostPath"].ToString(), postParams);

            html += "<tr> ";
            // STT
            html += "  <td>" + stt + "</td>";
            stt++;
            //image
            html += "<td class='datacol-share'>";
            html += "<p><img class='small-thumb' style='width: 100%;height: 130px;margin-left: 15px;margin-top: 4px;' src='" + urlHinhAnh + "' /></p>";
            html += "</td>";
            // hinh anh
            //html += "  <td style='width: 100px;' class='datacol-share'>";
            //html += "<a href='" + "/tdct/" + table2.Rows[i]["PostPath"] + "'><img src='" + urlHinhAnh + "' style='object-fit:cover;height:100px;' /> </a>";
            //html += "  </td>";
            // thong tin
            html += "<td style='text-align:left;min-width:450px;'>";
            html += "<div>";
            html += "<a href='" + url + "' style='padding-top:5px;margin: 0 0 1px;cursor:pointer;color:rgba(0, 0, 0, 0.87)'><b>" + table3.Rows[i]["PostTitle"].ToString() + "</b></a><br />";
            html += "<a href='" + url + "' style='margin: 0 0 1px;cursor:pointer;color:#fe9900;font-weight: 600;'><b>Mã:" + table3.Rows[i]["PostCode"].ToString() + "</b></a>";
            string a = table3.Rows[i]["PostPrice"].ToString();
            if (table3.Rows[i]["PostPrice"].ToString() != "")
            {
                html += "<p style='color:#ec3222;font-weight: 600'>Giá: " + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString()) + "</p>";
            }
            else if (a == "")
            {
                html += "<p>Giá: 0đ</p>";
            }
            if (table3.Rows[i]["PostCreatedAt"].ToString() != "")
            {
                try
                {
                    html += "<p>Ngày đăng: " + DateTime.Parse(table3.Rows[i]["PostCreatedAt"].ToString()).ToString("dd/MM/yyyy") + "</p>";
                }
                catch { }
            }
            html += "<p><button type='button' class='btn btn-primary btn-sm btn-approve-collab' ";
            html += " data-id='" + table3.Rows[i]["PostId"].ToString() + "' ";
            html += " data-title='" + table3.Rows[i]["PostTitle"].ToString() + "' ";
            html += " data-link='" + "/tdct/" + table3.Rows[i]["PostPath"].ToString() + "' ";
            html += " data-code='" + table3.Rows[i]["PostCode"].ToString() + "' ";
            html += " data-created-at='" + DateTime.Parse(table3.Rows[i]["PostCreatedAt"].ToString()).ToString("dd/MM/yyyy") + "' ";
            if (a != "")
            {
                html += " data-price-format='" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString()) + "' ";
            }
            else if (a == "")
            {
                html += " data-price-format= 0đ";
            }
            html += " data-price='" + table3.Rows[i]["PostPrice"].ToString() + "' ";
            html += " data-rate='" + table3.Rows[i]["PostRate"].ToString() + "' ";
            html += " data-amount='" + table3.Rows[i]["PostAmount"].ToString() + "' ";
            html += " >Trích hoa hồng CTV</button></p>";
            html += "</div>";
            html += "</td>";

            //Loai Tin
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["FeeCode"].ToString() == "" || table3.Rows[i]["FeeCode"].ToString() == "default_pack")
            {
                html += "<strong> Tin Thường</strong>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() == "vip1_pack")
            {
                html += "<strong> Tin Vip 1</strong>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() == "vip2_pack")
            {
                html += "<strong> Tin Vip 2</strong>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() == "vip3_pack")
            {
                html += "<strong> Tin Vip 3</strong>";
            }
            html += "</td>";


            // rate
            html += "  <td style='min-width:100px'>";
            html += "<strong>" + Utilities.Formatter.toPercentString(table3.Rows[i]["PostRate"].ToString()) + "</strong>";
            html += "</td>";
            // amount
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["PostAmount"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostAmount"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            //hoa hong
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["PostCommission"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostCommission"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            // submit at
            html += "  <td style='min-width:100px'>";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["PostSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            else if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostCreatedAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["PostCreatedAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            html += "  </td>";
            // ngay duyet
            html += "  <td style='min-width:100px'>";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["AdminSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["AdminSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            else if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["PostSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            html += "  </td>";
            // action
            html += "  <td style='min-width:110px'>";
            html += "<a style='width: 79px;' href='/dang-tin/dt?TDTD=" + table3.Rows[i]["PostId"].ToString() + "' class='btn btn-primary btn-sm mr-2 mb-2'>Sửa tin</a>";
            html += "<a href='javascript:ThongKe(\"" + table3.Rows[i]["PostId"].ToString() + "\");' class='btn btn-info btn-sm mr-2 mb-2'>Thống kê</a>";
            html += "<a  style='width: 79px;' href='javascript:void(0);' class='btn btn-danger btn-sm mr-2 mb-2 btn-dump-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "'>Hết hạn</a>";



            if (Utilities.Post.getPostFee(table3.Rows[i]) == "default_pack")
            {
                html += "<a style='width: 79px;' href='javascript:void(0);' class='btn btn-info btn-sm mr-2 mb-2 btn-upgrade-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "' data-pack-id='" + table3.Rows[i]["FeeCode"].ToString() + "'>Nâng cấp</a>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() != "default_pack" && table3.Rows[i]["FeeCode"].ToString() != "")
            {
                html += "<a style='width: 79px;' href='javascript:void(0);' class='btn btn-info btn-sm mr-2 mb-2 btn-extend-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "' data-pack-id='" + table3.Rows[i]["FeeCode"].ToString() + "' data-time-end='" + Utilities.Formatter.toDateTimeString2(table3.Rows[i].Field<DateTime>("FeeEndTime")) + "'>Gia hạn</a>";
            }

            html += "<a  style='width: 79px;' href='javascript:void(0);' class='btn btn-primary btn-sm mr-2 mb-2 btn-renovate' data-id='" + table3.Rows[i]["PostId"].ToString() + "' data-title='" + table3.Rows[i]["PostTitle"].ToString() + "'>Lên TOP</a>";
            html += "  </td>";

            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        divAccountPostTable.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPaginationSmall(totalItems, perPage, page, "/thong-tin-ca-nhan/ttcn?Tab=1", paginationParams);
        divAccountPostPagination1.InnerHtml = htmlPagination;
        divAccountPostPagination2.InnerHtml = htmlPagination;
    }

    protected void generatePendingTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPagecd = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int pagecd = 1;
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();

        if (Request.QueryString["Pagecd"] != null)
        {
            int oPagecd;
            if (Int32.TryParse(Request.QueryString["Pagecd"], out oPagecd))
            {
                pagecd = oPagecd;
            }
        }
        fromIndex = ((pagecd - 1) * perPagecd) + 1;
        toIndex = fromIndex + perPagecd - 1;

        if (this.refCode != null)
        {
            postParams = new Dictionary<string, string>();
            postParams.Add("RefCode", this.refCode);
        }

        sql1 += " FROM ";
        sql1 += "   ( ";
        sql1 += "     SELECT ";
        sql1 += "       ROW_NUMBER() OVER ( ";
        sql1 += "         ORDER BY ";
        sql1 += "           tbPost.NgayGuiDuyet DESC ";
        sql1 += "       ) AS RowNumber, ";
        sql1 += "        tbPost.idTinDang AS PostId,";
        sql1 += "        tbPost.DuongDan AS PostPath,";
        sql1 += "        tbPost.TieuDe AS PostTitle,";
        sql1 += "        tbPost.Code AS PostCode,";
        sql1 += "        tbPost.TuGia AS PostPrice,";
        sql1 += "        tbPost.NgayGuiDuyet AS PostSubmitAt,";
        sql1 += "        tbPost.NgayDang AS PostCreatedAt,";
        sql1 += "        ISNULL(tbPost.Commission, 0) AS PostCommission,";
        sql1 += "       ISNULL(tbPost.ReferralRate, 0) AS PostRate,";
        sql1 += "        ISNULL(tbPost.ReferralAmount, 0) AS PostAmount,";
        sql1 += "        ISNULL(tbPost.FeeCode, '') AS FeeCode,";
        sql1 += "        ISNULL(tbPost.FeeStartTime, '') AS FeeStartTime,";
        sql1 += "        ISNULL(tbPost.FeeEndTime, '') AS FeeEndTime";
        sql1 += "     FROM  tb_TinDang tbPost ";
        sql1 += "       LEFT JOIN tb_ThanhVien tbAccount ON tbPost.idThanhVien = tbAccount.idThanhVien ";
        sql1 += "       LEFT JOIN tb_DanhMucCap1 TBCategories1 ";
        sql1 += "           ON tbPost.idDanhMucCap1 = TBCategories1.idDanhMucCap1 ";
        sql1 += "       LEFT JOIN tb_DanhMucCap2 TBCategories2 ";
        sql1 += "           ON tbPost.idDanhMucCap2 = TBCategories2.idDanhMucCap2 ";
        sql1 += "       LEFT JOIN City TBOwnerCities ";
        sql1 += "           ON tbAccount.idTinh = TBOwnerCities.id ";
        sql1 += "       LEFT JOIN District TBOwnerDistricts ";
        sql1 += "           ON tbAccount.idHuyen = TBOwnerDistricts.id ";
        sql1 += "       LEFT JOIN tb_PhuongXa TBOwnerWards ";
        sql1 += "           ON tbAccount.idPhuongXa = TBOwnerWards.id ";
        sql1 += "     WHERE  1 = 1 ";
        sql1 += "       AND ISNULL(tbPost.isHetHan, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.IsDraft, 0) = 0 ";
        sql1 += "       AND TBPost.isDuyet IS NULL ";
        sql1 += "       AND tbPost.idThanhVien = '" + idThanhVien + @"'";
        if (!string.IsNullOrWhiteSpace(filterPostCode) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.Code LIKE '%" + filterPostCode + "%' ";
            paginationParams.Add("FilterPostCode", filterPostCode);
        }
        if (!string.IsNullOrWhiteSpace(filterPostOwner) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.idThanhVien = " + filterPostOwner + " ";
            paginationParams.Add("FilterPostOwner", filterPostOwner);
        }
        if (!string.IsNullOrWhiteSpace(filterPostTitle) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.TieuDe LIKE N'%" + filterPostTitle + "%' ";
            paginationParams.Add("FilterPostTitle", filterPostTitle);
        }
        //Giá

        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "0" && Tabbtn == "2")
        {
            if (this.filterFromPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += " OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += " AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100500" && Tabbtn == "2")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500500" && Tabbtn == "2")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        //Hoa Hồng

        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "0" && Tabbtn == "2")
        {
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  >= " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100" && Tabbtn == "2")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500" && Tabbtn == "2")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500500" && Tabbtn == "2")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500500" && Tabbtn == "2")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtFromAltDate) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.NgayDang >= '" + filterCreatedAtFromAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtToAltDate) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.NgayDang <= '" + filterCreatedAtToAltDateTime + "' ";
            paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedPostFromAltDate) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.NgayGuiDuyet >= '" + filterCreatedPostFromAltDateTime + "' ";
            paginationParams.Add("FilterCreatedPostFrom", filterCreatedPostFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedPostToAltDate) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.NgayGuiDuyet <= '" + filterCreatedPostToAltDateTime + "' ";
            paginationParams.Add("FilterCreatedPostTo", filterCreatedPostToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory1) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.idDanhMucCap1 = '" + filterCategory1 + "' ";
            paginationParams.Add("FilterCategory1", filterCategory1);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory2) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.idDanhMucCap2 = '" + filterCategory2 + "' ";
            paginationParams.Add("FilterCategory2", filterCategory2);
        }
        if (!string.IsNullOrWhiteSpace(filterCity) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard) && Tabbtn == "2")
        {
            sql1 += "       AND tbPost.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        sql1 += "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());
        nbchoduyet.InnerText = table2.Rows[0]["TotalRows"].ToString();
        DataTable table3 = Connect.GetTable(sql3);
        html += "<table id='postTable2' class='table table-bordered app-table dataTable no-footer text-sm' style='text-align:center;'>";
        html += "<thead>";
        html += "<tr>";
        html += "<th style='min-width:20px'>STT</th>";
        html += "<th>Hình ảnh</th>";
        html += "<th>Thông Tin</th>";
        html += "<th>Loại Tin</th>";
        html += "<th>Tỉ lệ hoa hồng</th>";
        html += "<th>Hoa hồng cố định</th>";
        html += "<th>Hoa hồng</th>";
        html += "<th>Ngày gửi duyệt</th>";
        html += "<th>Hành động</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        //if (table3.Rows.Count == 0)
        // {
        //    html += "<tr><td colspan='8' style='text-align: center;'>Không có kết quả</td></tr>";
        // }
        int stt = pagecd * 10 - 9;
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(table3.Rows[i]["PostId"].ToString());
            string url = Utilities.Post.getUrl(this.configs["domain"].ToString(), table3.Rows[i]["PostPath"].ToString(), postParams);
            //string ownerInfoText = "";
            //string postTitle = "";
            //ownerInfoText += " data-thumbnail='" + Utilities.Account.getLinkThumbWithPath(table3.Rows[i]["OwnerThumbnail"].ToString()) + "' ";
            //ownerInfoText += " data-fullname='" + table3.Rows[i]["OwnerFullName"].ToString() + "' ";
            //ownerInfoText += " data-code='" + table3.Rows[i]["OwnerCode"].ToString() + "' ";
            //ownerInfoText += " data-phone='" + table3.Rows[i]["OwnerPhone"].ToString() + "' ";
            //ownerInfoText += " data-email='" + table3.Rows[i]["OwnerEmail"].ToString() + "' ";
            //ownerInfoText += " data-addr='" + table3.Rows[i]["OwnerAddress"].ToString() + ", " + table3.Rows[i]["OwnerWard"].ToString() + ", " + table3.Rows[i]["OwnerDistrict"].ToString() + ", " + table3.Rows[i]["OwnerCity"].ToString() + "' ";
            //ownerInfoText += " data-register-at='" + ((DateTime)table3.Rows[i]["OwnerRegisterAt"]).ToString("dd-MM-yyyy HH:mm:ss") + "' ";

            html += "<tr>";
            // STT
            html += "  <td>" + stt + "</td>";
            stt++;
            //image
            html += "<td class='datacol-share' style='min-width:130px'>";
            html += "<p><img class='small-thumb' style='width: 100%;height: 130px;margin-left: 15px;margin-top: 4px;' src='" + urlHinhAnh + "' /></p>";
            html += "</td>";
            // hinh anh
            //html += "  <td style='width: 100px;' class='datacol-share'>";
            //html += "<a href='" + "/tdct/" + table2.Rows[i]["PostPath"] + "'><img src='" + urlHinhAnh + "' style='object-fit:cover;height:100px;' /> </a>";
            //html += "  </td>";
            // thong tin
            html += "<td style='text-align:left;min-width:450px;' class='datacol-share'>";
            html += "<div>";
            html += "<p style='padding-top:5px;margin: -29px 0 1px;cursor:pointer;'><b>" + table3.Rows[i]["PostTitle"].ToString() + "</b></p>";
            html += "<p style='margin: 0 0 1px;cursor:pointer;color:#fe9900;font-weight: 600;'>Mã: " + table3.Rows[i]["PostCode"].ToString() + "</p>";
            string a = table3.Rows[i]["PostPrice"].ToString();
            if (table3.Rows[i]["PostPrice"].ToString() != "")
            {
                html += "<p style='color:#ec3222;font-weight:600;'>Giá: " + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString()) + "</p>";
            }
            else if (a == "")
            {
                html += "<p style='color:#ec3222;font-weight:600;'>Giá: 0đ</p>";
            }
            if (table3.Rows[i]["PostCreatedAt"].ToString() != "")
            {
                try
                {
                    html += "<p>Ngày đăng: " + DateTime.Parse(table3.Rows[i]["PostCreatedAt"].ToString()).ToString("dd/MM/yyyy") + "</p>";
                }
                catch { }
            }
            html += "</div>";
            html += "</td>";

            //Loai Tin
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["FeeCode"].ToString() == "" || table3.Rows[i]["FeeCode"].ToString() == "default_pack")
            {
                html += "<strong> Tin Thường</strong>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() == "vip1_pack")
            {
                html += "<strong> Tin Vip 1</strong>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() == "vip2_pack")
            {
                html += "<strong> Tin Vip 2</strong>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() == "vip3_pack")
            {
                html += "<strong> Tin Vip 3</strong>";
            }
            html += "</td>";

            // rate
            html += "  <td style='min-width:100px'>";
            html += "<strong>" + Utilities.Formatter.toPercentString(table3.Rows[i]["PostRate"].ToString()) + "</strong>";
            html += "</td>";
            // amount
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["PostAmount"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostAmount"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            //hoa hong
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["PostCommission"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostCommission"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            // submit at
            html += "  <td style='min-width:100px'>";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["PostSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            html += "  </td>";


            // action
            html += "  <td style='min-width:110px'>";
            html += "<a style='width: 84px;' href='/dang-tin/dt?TDTD=" + table3.Rows[i]["PostId"].ToString() + "' class='btn btn-primary btn-sm mr-2 mb-2'>Sửa tin</a>";
            html += "<a target='_blank' href='/preview/pv?MaTinDang=" + table3.Rows[i]["PostId"].ToString() + "' class='btn btn-info btn-sm mr-2 mb-2'>Xem trước</a>";
            html += "<a style='width: 84px;' href='javascript:void(0);' class='btn btn-danger btn-sm mr-2 mb-2 btn-dump-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "'>Hết hạn</a>";
            if (Utilities.Post.getPostFee(table3.Rows[i]) == "default_pack")
            {
                html += "<a style='width: 79px;' href='javascript:void(0);' class='btn btn-info btn-sm mr-2 mb-2 btn-upgrade-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "'>Nâng cấp</a>";
            }
            else if (table3.Rows[i]["FeeCode"].ToString() != "default_pack" && table3.Rows[i]["FeeCode"].ToString() != "")
            {
                html += "<a style='width: 79px;' href='javascript:void(0);' class='btn btn-info btn-sm mr-2 mb-2 btn-extend-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "' data-pack-id='" + table3.Rows[i]["FeeCode"].ToString() + "' data-time-end='" + Utilities.Formatter.toDateTimeString2(table3.Rows[i].Field<DateTime>("FeeEndTime")) + "'>Gia hạn</a>";
            }
            html += "  </td>";

            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        divAccountPostTable2.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPaginationSmallcd(totalItems, perPagecd, pagecd, "/thong-tin-ca-nhan/ttcn?Tab=2", paginationParams);
        divAccountPostPagination3.InnerHtml = htmlPagination;
        divAccountPostPagination4.InnerHtml = htmlPagination;
    }

    protected void generateDeniedTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPagebtc = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int pagebtc = 1;
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();

        if (Request.QueryString["Pagebtc"] != null)
        {
            int oPagebtc;
            if (Int32.TryParse(Request.QueryString["Pagebtc"], out oPagebtc))
            {
                pagebtc = oPagebtc;
            }
        }
        fromIndex = ((pagebtc - 1) * perPagebtc) + 1;
        toIndex = fromIndex + perPagebtc - 1;

        if (this.refCode != null)
        {
            postParams = new Dictionary<string, string>();
            postParams.Add("RefCode", this.refCode);
        }

        sql1 += " FROM ";
        sql1 += "   ( ";
        sql1 += "     SELECT ";
        sql1 += "       ROW_NUMBER() OVER ( ";
        sql1 += "         ORDER BY ";
        sql1 += "           tbPost.NgayGuiDuyet DESC ";
        sql1 += "       ) AS RowNumber, ";
        sql1 += "        tbPost.idTinDang AS PostId,";
        sql1 += "        tbPost.DuongDan AS PostPath,";
        sql1 += "        tbPost.TieuDe AS PostTitle,";
        sql1 += "        tbPost.Code AS PostCode,";
        sql1 += "        tbPost.TuGia AS PostPrice,";
        sql1 += "        tbPost.NgayGuiDuyet AS PostSubmitAt,";
        sql1 += "        tbPost.NgayAdminDuyet AS AdminSubmitAt,";
        sql1 += "        tbPost.NgayDang AS PostCreatedAt,";
        sql1 += "        ISNULL(tbPost.Commission, 0) AS PostCommission,";
        sql1 += "       ISNULL(tbPost.ReferralRate, 0) AS PostRate,";
        sql1 += "        ISNULL(tbPost.ReferralAmount, 0) AS PostAmount,";
        sql1 += "       ISNULL(tbPost.LyDo_KhongDuyet, '') AS DenyNote";
        sql1 += "     FROM  tb_TinDang tbPost ";
        sql1 += "       LEFT JOIN tb_ThanhVien tbAccount ON tbPost.idThanhVien = tbAccount.idThanhVien ";
        sql1 += "       LEFT JOIN tb_DanhMucCap1 TBCategories1 ";
        sql1 += "           ON tbPost.idDanhMucCap1 = TBCategories1.idDanhMucCap1 ";
        sql1 += "       LEFT JOIN tb_DanhMucCap2 TBCategories2 ";
        sql1 += "           ON tbPost.idDanhMucCap2 = TBCategories2.idDanhMucCap2 ";
        sql1 += "       LEFT JOIN City TBOwnerCities ";
        sql1 += "           ON tbAccount.idTinh = TBOwnerCities.id ";
        sql1 += "       LEFT JOIN District TBOwnerDistricts ";
        sql1 += "           ON tbAccount.idHuyen = TBOwnerDistricts.id ";
        sql1 += "       LEFT JOIN tb_PhuongXa TBOwnerWards ";
        sql1 += "           ON tbAccount.idPhuongXa = TBOwnerWards.id ";
        sql1 += "     WHERE  1 = 1 ";
        sql1 += "       AND ISNULL(tbPost.isHetHan, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.IsDraft, 0) = 0 ";
        sql1 += "       AND ISNULL(tbPost.isDuyet, 0) = 0 ";
        sql1 += "       AND tbPost.LyDo_KhongDuyet != '' ";
        sql1 += "       AND tbPost.idThanhVien = '" + idThanhVien + @"'";
        if (!string.IsNullOrWhiteSpace(filterPostCode) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.Code LIKE '%" + filterPostCode + "%' ";
            paginationParams.Add("FilterPostCode", filterPostCode);
        }
        if (!string.IsNullOrWhiteSpace(filterPostOwner) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.idThanhVien = " + filterPostOwner + " ";
            paginationParams.Add("FilterPostOwner", filterPostOwner);
        }
        if (!string.IsNullOrWhiteSpace(filterPostTitle) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.TieuDe LIKE N'%" + filterPostTitle + "%' ";
            paginationParams.Add("FilterPostTitle", filterPostTitle);
        }
        //Giá

        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "0" && Tabbtn == "3")
        {
            if (this.filterFromPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice > 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += " OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += " AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100500" && Tabbtn == "3")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500500" && Tabbtn == "3")
        {
            sql1 += " AND ( tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tbPost.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.TuGia BETWEEN 10000000 AND 100000000 OR tbPost.TuGia BETWEEN 100000000 AND 500000000 OR tbPost.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tbPost.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tbPost.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        //Hoa Hồng

        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "0" && Tabbtn == "3")
        {
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  >= " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100" && Tabbtn == "3")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500" && Tabbtn == "3")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500500" && Tabbtn == "3")
        {
            sql1 += " AND (tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500500" && Tabbtn == "3")
        {
            sql1 += " AND ( tbPost.Commission BETWEEN 10000000 AND 100000000 OR tbPost.Commission BETWEEN 100000000 AND 500000000 OR tbPost.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tbPost.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tbPost.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedPostFromAltDate) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.NgayGuiDuyet >= '" + filterCreatedPostFromAltDateTime + "' ";
            paginationParams.Add("FilterCreatedPostFrom", filterCreatedPostFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedPostToAltDate) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.NgayGuiDuyet <= '" + filterCreatedPostToAltDateTime + "' ";
            paginationParams.Add("FilterCreatedPostTo", filterCreatedPostToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedDeniedFromAltDate) && Tabbtn == "3")
        {
            sql1 += "       AND ((tbPost.NgayAdminDuyet >= '" + filterCreatedDeniedFromAltDateTime + "') OR (tbPost.NgayGuiDuyet >= '" + filterCreatedDeniedFromAltDateTime + "')) ";
            paginationParams.Add("FilterCreatedDeniedFrom", filterCreatedDeniedFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedDeniedToAltDate) && Tabbtn == "3")
        {
            sql1 += "       AND ((tbPost.NgayAdminDuyet <= '" + filterCreatedDeniedToAltDateTime + "') OR (tbPost.NgayGuiDuyet <= '" + filterCreatedDeniedToAltDateTime + "')) ";
            paginationParams.Add("FilterCreatedDeniedTo", filterCreatedDeniedToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory1) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.idDanhMucCap1 = '" + filterCategory1 + "' ";
            paginationParams.Add("FilterCategory1", filterCategory1);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory2) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.idDanhMucCap2 = '" + filterCategory2 + "' ";
            paginationParams.Add("FilterCategory2", filterCategory2);
        }
        if (!string.IsNullOrWhiteSpace(filterCity) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard) && Tabbtn == "3")
        {
            sql1 += "       AND tbPost.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        sql1 += "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());
        nbbituchoi.InnerText = table2.Rows[0]["TotalRows"].ToString();
        DataTable table3 = Connect.GetTable(sql3);
        html += "<table id='postTable3' class='table table-bordered app-table dataTable no-footer text-sm' style='text-align:center;'>";
        html += "<thead>";
        html += "<tr>";
        html += "<th style='min-width:20px'>STT</th>";
        html += "<th>Hình ảnh</th>";
        html += "<th>Thông Tin</th>";
        html += "<th>Tỉ lệ hoa hồng</th>";
        html += "<th>Hoa hồng cố định</th>";
        html += "<th>Hoa hồng</th>";
        html += "<th>Ngày gửi duyệt</th>";
        html += "<th>Ngày từ chối</th>";
        html += "<th>Lý do từ chối</th>";
        html += "<th>Hành động</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        int stt = pagebtc * 10 - 9;
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(table3.Rows[i]["PostId"].ToString());
            string url = Utilities.Post.getUrl(this.configs["domain"].ToString(), table3.Rows[i]["PostPath"].ToString(), postParams);
            html += "<tr>";
            // STT
            html += "  <td>" + stt + "</td>";
            stt++;
            //image
            html += "<td class='datacol-share' style='min-width:130px'>";
            html += "<p><img class='small-thumb' style='width: 100%;height: 130px;margin-left: 15px;margin-top: 4px;' src='" + urlHinhAnh + "' /></p>";
            html += "</td>";
            // thong tin
            html += "<td style='text-align:left;min-width:450px;' class='datacol-share'>";
            html += "<div>";
            html += "<p style='padding-top:5px;margin: -29px 0 1px;cursor:pointer;'><b>" + table3.Rows[i]["PostTitle"].ToString() + "</b></p>";
            html += "<p  style='margin: 0 0 1px;cursor:pointer;color:#fe9900;font-weight: 600;'>Mã: " + table3.Rows[i]["PostCode"].ToString() + "</p>";
            string a = table3.Rows[i]["PostPrice"].ToString();
            if (table3.Rows[i]["PostPrice"].ToString() != "")
            {
                html += "<p style='color:#ec3222;font-weight:600;'>Giá: " + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString()) + "</p>";
            }
            else if (a == "")
            {
                html += "<p style='color:#ec3222;font-weight:600;'>Giá: 0đ</p>";
            }
            if (table3.Rows[i]["PostCreatedAt"].ToString() != "")
            {
                try
                {
                    html += "<p>Ngày đăng: " + DateTime.Parse(table3.Rows[i]["PostCreatedAt"].ToString()).ToString("dd/MM/yyyy") + "</p>";
                }
                catch { }
            }
            html += "</div>";
            html += "</td>";
            // rate
            html += "  <td style='min-width:100px'>";
            html += "<strong>" + Utilities.Formatter.toPercentString(table3.Rows[i]["PostRate"].ToString()) + "</strong>";
            html += "</td>";
            // amount
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["PostAmount"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostAmount"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            //hoa hong
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["PostCommission"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostCommission"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            // submit at
            // submit at
            html += "  <td style='min-width:100px'>";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["PostSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            html += "  </td>";
            // ngay duyet
            html += "  <td style='min-width:100px'>";
            if (!string.IsNullOrWhiteSpace(table3.Rows[i]["AdminSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["AdminSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            else if (!string.IsNullOrWhiteSpace(table3.Rows[i]["PostSubmitAt"].ToString()))
            {
                string sPostSubmitAt = Utilities.Formatter.toDateTimeString(table3.Rows[i]["PostSubmitAt"].ToString());
                html += "<p>" + sPostSubmitAt + "</p>";
            }
            html += "  </td>";
            // deny note
            string LyDo = table3.Rows[i]["DenyNote"].ToString();
            string LyDos = LyDo.Replace("|", "<br/>");
            html += "       <td style='min-width:170px;' class='datacol-share' style='width: 400px'>" + LyDos + "</td>";
            // action	
            html += "  <td style='min-width:110px'>";
            html += "<a style='width: 71px;' href='/dang-tin/dt?TDTD=" + table3.Rows[i]["PostId"].ToString() + "' class='btn btn-primary btn-sm mr-2 mb-2'>Sửa tin</a>";
            html += "<a href='javascript:void(0);' class='btn btn-danger btn-sm mr-2 mb-2 btn-dump-post' data-id='" + table3.Rows[i]["PostId"].ToString() + "'>Hết hạn</a>";
            html += "  </td>";
            html += "</tr>";

            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        divAccountPostTable3.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPaginationSmallbtc(totalItems, perPagebtc, pagebtc, "/thong-tin-ca-nhan/ttcn?Tab=3", paginationParams);
        divAccountPostPagination5.InnerHtml = htmlPagination;
        divAccountPostPagination6.InnerHtml = htmlPagination;
    }

    protected void generateCompletedTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPagethh = 10;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int pagethh = 1;
        // string Test = "1";
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();

        if (Request.QueryString["Pagethh"] != null)
        {
            int oPagethh;
            if (Int32.TryParse(Request.QueryString["Pagethh"], out oPagethh))
            {
                pagethh = oPagethh;
            }
        }
        fromIndex = ((pagethh - 1) * perPagethh) + 1;
        toIndex = fromIndex + perPagethh - 1;

        if (this.refCode != null)
        {
            postParams = new Dictionary<string, string>();
            postParams.Add("RefCode", this.refCode);
        }

        sql1 += " FROM ";
        sql1 += "   ( ";
        sql1 += "     SELECT ";
        sql1 += "       ROW_NUMBER() OVER ( ";
        sql1 += "         ORDER BY ";
        sql1 += "           tb_TinDang.NgayGuiDuyet DESC ";
        sql1 += "       ) AS RowNumber, ";
        sql1 += "        tb_TinDang.idTinDang AS idTinDang,";
        sql1 += "        tb_TinDang.DuongDan AS DuongDan,";
        sql1 += "        tb_TinDang.TieuDe AS TieuDe,";
        sql1 += "        tb_TinDang.Code AS Code,";
        sql1 += "        tb_TinDang.TuGia AS TuGia,";
        sql1 += "        tb_TinDang.isHetHan AS isHetHan,";
        sql1 += "        tb_TinDang.isDuyet AS isDuyet,";
        sql1 += "        tb_TinDang.NgayAdminDuyet AS AdminSubmitAt,";
        sql1 += "        tb_TinDang.NgayGuiDuyet AS PostSubmitAt,";
        sql1 += "        tb_TinDang.NgayDang AS NgayDang,";
        sql1 += "        ISNULL(tb_TinDang.TuGia, 0) AS PostPrice,";
        sql1 += "        ISNULL(Referrals.Rate, 0) AS ReferralRate,";
        sql1 += "        ISNULL(Referrals.Amount, 0) AS ReferralAmount,";
        sql1 += "        ISNULL(Referrals.Commission, 0) AS ReferralCommission,";
        sql1 += "        Referrals.CollabId AS ReferralCollabId,";
        sql1 += "        TBCollabs.Code AS CollabCode,";
        sql1 += "        TBCollabs.TenDangNhap AS CollabUsername,";
        sql1 += "        ISNULL(TBCollabs.SoDienThoai, '') AS CollabPhone,";
        sql1 += "        ISNULL(TBCollabs.Email, '') AS CollabEmail,";
        sql1 += "        ISNULL(TBCollabs.TenCuaHang, '') AS CollabFullname,";
        sql1 += "        ISNULL(TBCollabs.DiaChi, '') AS CollabAddress,";
        sql1 += "        ISNULL(TBCollabCities.Ten, '') AS CollabCityName,";
        sql1 += "        ISNULL(TBCollabDistricts.Ten, '') AS CollabDistrictName,";
        sql1 += "        ISNULL(TBCollabWards.Ten, '') AS CollabWardName,";
        sql1 += "        Referrals.Id AS ReferralId,";
        sql1 += "        Referrals.Status AS ReferralStatus,";
        sql1 += "        ISNULL(Referrals.StsOwnerApproved, 0) AS ReferralStsOwnerApproved,";
        sql1 += "        ISNULL(Referrals.StsOwnerCanceled, 0) AS ReferralStsOwnerCanceled,";
        sql1 += "        ISNULL(Referrals.StsCollabApproved, 0) AS ReferralStsCollabApproved,";
        sql1 += "        ISNULL(Referrals.StsCollabDenied, 0) AS ReferralStsCollabDenied,";
        sql1 += "        ISNULL(Referrals.StsAdminApproved, 0) AS ReferralStsAdminApproved,";
        sql1 += "        Referrals.OwnerApprovedAt AS ReferralOwnerApprovedAt,";
        sql1 += "        Referrals.OwnerCanceledAt AS ReferralOwnerCanceledAt,";
        sql1 += "        Referrals.CollabApprovedAt AS ReferralCollabApprovedAt,";
        sql1 += "        Referrals.CollabDeniedAt AS ReferralCollabDeniedAt,";
        sql1 += "        Referrals.AdminApprovedAt AS ReferralAdminApprovedAt,";
        sql1 += "        Referrals.UpdatedAt AS ReferralUpdatedAt";
        sql1 += "        FROM Referrals";
        sql1 += "        INNER JOIN tb_TinDang";
        sql1 += "           ON Referrals.TinDangId = tb_TinDang.idTinDang";
        sql1 += "        LEFT JOIN tb_ThanhVien TBCollabs";
        sql1 += "           ON Referrals.CollabId = TBCollabs.idThanhVien";
        sql1 += "       LEFT JOIN City TBCollabCities";
        sql1 += "           ON TBCollabs.idTinh = TBCollabCities.id";
        sql1 += "       LEFT JOIN District TBCollabDistricts";
        sql1 += "       ON TBCollabs.idHuyen = TBCollabDistricts.id";
        sql1 += "       LEFT JOIN tb_PhuongXa TBCollabWards";
        sql1 += "       ON TBCollabs.idPhuongXa = TBCollabWards.id";
        sql1 += "       WHERE 1 = 1";
        sql1 += "       AND tb_TinDang.idThanhVien = '" + idThanhVien + @"'";
        if (!string.IsNullOrWhiteSpace(filterPostCode) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.Code LIKE '%" + filterPostCode + "%' ";
            paginationParams.Add("FilterPostCode", filterPostCode);
        }
        if (!string.IsNullOrWhiteSpace(filterPostOwner) && Tabbtn == "4")
        {
            sql1 += "       AND Referrals.CollabId = " + filterPostOwner + " ";
            paginationParams.Add("FilterPostOwner", filterPostOwner);
        }
        if (!string.IsNullOrWhiteSpace(filterPostTitle) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.TieuDe LIKE N'%" + filterPostTitle + "%' ";
            paginationParams.Add("FilterPostTitle", filterPostTitle);
        }
        //Giá

        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "0" && Tabbtn == "4")
        {
            if (this.filterFromPostPrice > 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice > 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + " ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.TuGia BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += " OR ( tb_TinDang.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += " AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tb_TinDang.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tb_TinDang.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.TuGia BETWEEN 10000000 AND 100000000 OR tb_TinDang.TuGia BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tb_TinDang.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + ")) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100500" && Tabbtn == "4")
        {
            sql1 += " AND ( tb_TinDang.TuGia BETWEEN 10000000 AND 100000000 OR tb_TinDang.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tb_TinDang.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "100500500" && Tabbtn == "4")
        {
            sql1 += " AND ( tb_TinDang.TuGia BETWEEN 100000000 AND 500000000 OR tb_TinDang.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR ( tb_TinDang.TuGia  > " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        if (!string.IsNullOrWhiteSpace(filterPostPrice) && sGia == "10100100500500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.TuGia BETWEEN 10000000 AND 100000000 OR tb_TinDang.TuGia BETWEEN 100000000 AND 500000000 OR tb_TinDang.TuGia >= 500000000";
            if (this.filterFromPostPrice >= 0)
            {
                sql1 += "       OR (tb_TinDang.TuGia  >= " + this.filterFromPostPrice + " ";
            }
            if (this.filterToPostPrice >= 0)
            {
                sql1 += "       AND tb_TinDang.TuGia  <= " + this.filterToPostPrice + " )) ";
            }
            paginationParams.Add("FilterPostPrice", filterPostPrice);
            paginationParams.Add("Gia", sGia);
        }
        //Hoa Hồng

        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "0" && Tabbtn == "4")
        {
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  >= " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + " ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100" && Tabbtn == "4")
        {
            sql1 += " AND ( tb_TinDang.Commission BETWEEN 10000000 AND 100000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500" && Tabbtn == "4")
        {
            sql1 += " AND ( tb_TinDang.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.Commission BETWEEN 10000000 AND 100000000 OR tb_TinDang.Commission BETWEEN 100000000 AND 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.Commission BETWEEN 10000000 AND 100000000 OR tb_TinDang.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "100500500" && Tabbtn == "4")
        {
            sql1 += " AND (tb_TinDang.Commission BETWEEN 100000000 AND 500000000 OR tb_TinDang.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR (tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + ")) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterPostCommission) && sHoaHong == "10100100500500" && Tabbtn == "4")
        {
            sql1 += " AND ( tb_TinDang.Commission BETWEEN 10000000 AND 100000000 OR tb_TinDang.Commission BETWEEN 100000000 AND 500000000 OR tb_TinDang.Commission >= 500000000";
            if (this.filterFromPostCommission > 0)
            {
                sql1 += "       OR ( tb_TinDang.Commission  > " + this.filterFromPostCommission + " ";
            }
            if (this.filterToPostCommission > 0)
            {
                sql1 += "       AND tb_TinDang.Commission  <= " + this.filterToPostCommission + " )) ";
            }
            paginationParams.Add("FilterPostCommission", filterPostCommission);
            paginationParams.Add("HoaHong", sHoaHong);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtFromAltDate) && Tabbtn == "4")
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.OwnerCanceledAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabDeniedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.AdminApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')) ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
                else if (filterReferralStatus == "OWNER_CANCELED")
                {
                    sql1 += "       AND Referrals.OwnerCanceledAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
                else if (filterReferralStatus == "COLLAB_DENIED")
                {
                    sql1 += "       AND Referrals.CollabDeniedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')) ";
                    paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
                }
            }
            //loc 1

            if (!string.IsNullOrWhiteSpace(filterReferralStatusOther2))
            {
                if (filterReferralStatusOther2 == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.OwnerCanceledAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabDeniedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.AdminApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')) ";
                }
                else if (filterReferralStatusOther2 == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatus == "OWNER_CANCELED")
                {
                    sql1 += "       AND Referrals.OwnerCanceledAt >= '" + filterCreatedAtFromAltDateTime + "'";
                }
                else if (filterReferralStatusOther2 == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatus == "COLLAB_DENIED")
                {
                    sql1 += "       AND Referrals.CollabDeniedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterCreatedAtFromAltDateTime + "' ";
                }
                //else if (filterReferralStatusOther2 == "FEE_COMMISSION")
                //{
                //    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')) ";
                //}
            }
            //loc 2
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')) ";
            }

            //sql1 += "       AND (( Referrals.OwnerApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.OwnerCanceledAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.CollabDeniedAt >= '" + filterCreatedAtFromAltDateTime + "') OR (Referrals.AdminApprovedAt >= '" + filterCreatedAtFromAltDateTime + "')) ";
            //paginationParams.Add("FilterCreatedAtFrom", filterCreatedAtFromAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCreatedAtToAltDate) && Tabbtn == "4")
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.OwnerCanceledAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR ( Referrals.CollabDeniedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterCreatedAtToAltDateTime + "')) ";
                    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                }
                else if (filterReferralStatus == "OWNER_CANCELED")
                {
                    sql1 += "       AND Referrals.OwnerCanceledAt <= '" + filterCreatedAtToAltDateTime + "'";
                    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                }
                else if (filterReferralStatus == "COLLAB_DENIED")
                {
                    sql1 += "       AND Referrals.CollabDeniedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                }
                //else if (filterReferralStatus == "FEE_COMMISSION")
                //{
                //    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "')) ";
                //    paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
                //}
            }
            //loc 1
            if (!string.IsNullOrWhiteSpace(filterReferralStatusOther2))
            {
                if (filterReferralStatusOther2 == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.OwnerCanceledAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR ( Referrals.CollabDeniedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterCreatedAtToAltDateTime + "')) ";
                }
                else if (filterReferralStatusOther2 == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatus == "OWNER_CANCELED")
                {
                    sql1 += "       AND Referrals.OwnerCanceledAt <= '" + filterCreatedAtToAltDateTime + "'";
                }
                else if (filterReferralStatusOther2 == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatus == "COLLAB_DENIED")
                {
                    sql1 += "       AND Referrals.CollabDeniedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterCreatedAtToAltDateTime + "' ";
                }
                //else if (filterReferralStatusOther2 == "FEE_COMMISSION")
                //{
                //    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "')) ";
                //}
            }
            //loc2
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterCreatedAtToAltDateTime + "')) ";
            }

            //sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.OwnerCanceledAt <= '" + filterCreatedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterCreatedAtToAltDateTime + "') OR ( Referrals.CollabDeniedAt <= '" + filterCreatedAtToAltDateTime + "') OR ( Referrals.AdminApprovedAt <= '" + filterCreatedAtToAltDateTime + "')) ";
            //paginationParams.Add("FilterCreatedAtTo", filterCreatedAtToAltDate);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory1) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.idDanhMucCap1 = '" + filterCategory1 + "' ";
            paginationParams.Add("FilterCategory1", filterCategory1);
        }
        if (!string.IsNullOrWhiteSpace(filterCategory2) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.idDanhMucCap2 = '" + filterCategory2 + "' ";
            paginationParams.Add("FilterCategory2", filterCategory2);
        }
        if (!string.IsNullOrWhiteSpace(filterCity) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard) && Tabbtn == "4")
        {
            sql1 += "       AND tb_TinDang.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        if (!string.IsNullOrWhiteSpace(filterReferralStatus) || !string.IsNullOrWhiteSpace(filterReferralStatusOther2))
        {
            bool tmpHasFilterStatus1 = !string.IsNullOrWhiteSpace(filterReferralStatus);
            bool tmpHasFilterStatus2 = !string.IsNullOrWhiteSpace(filterReferralStatusOther2);
            sql1 += " AND ( ";
            if (tmpHasFilterStatus1)
            {
                sql1 += " ( " + this.getConditionByStatus(filterReferralStatus, "Referrals") + " ) ";
                paginationParams.Add("FilterReferralStatus", filterReferralStatus);
            }
            if (tmpHasFilterStatus2)
            {
                if (tmpHasFilterStatus1)
                {
                    sql1 += "  OR ";
                }
                sql1 += " ( " + this.getConditionByStatus(filterReferralStatusOther2, "Referrals") + " ) ";
                paginationParams.Add("FilterReferralStatusOther2", filterReferralStatusOther2);
            }
            sql1 += " ) ";
        }
        if (!string.IsNullOrWhiteSpace(filterOwnerApprovedAtFromAltDateTime))
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtFrom", filterOwnerApprovedAtFromAltDate);
                }
            }
            //loc 1

            if (!string.IsNullOrWhiteSpace(filterReferralStatusOther2))
            {
                if (filterReferralStatusOther2 == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                }
                else if (filterReferralStatusOther2 == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
                }
            }
            //loc 2
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "') OR (Referrals.CollabApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')  OR (Referrals.AdminApprovedAt >= '" + filterOwnerApprovedAtFromAltDateTime + "')) ";
            }
        }
        if (!string.IsNullOrWhiteSpace(filterOwnerApprovedAtToAltDateTime))
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                    paginationParams.Add("FilterOwnerApprovedAtTo", filterOwnerApprovedAtToAltDate);
                }
            }
            //loc 1
            if (!string.IsNullOrWhiteSpace(filterReferralStatusOther2))
            {
                if (filterReferralStatusOther2 == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                }
                else if (filterReferralStatusOther2 == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "' ";
                }
                else if (filterReferralStatusOther2 == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
                }
            }
            //loc2
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.CollabApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "') OR (Referrals.AdminApprovedAt <= '" + filterOwnerApprovedAtToAltDateTime + "')) ";
            }
        }
        sql1 += "   ) AS tbTemp ";

        sql2 += " SELECT COUNT(*) AS TotalRows " + sql1;
        sql3 += " SELECT * " + sql1;
        sql3 += " WHERE ";
        sql3 += "   RowNumber BETWEEN " + fromIndex + " ";
        sql3 += "   AND " + toIndex + " ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TotalRows"].ToString());
        nbtrichhh.InnerText = table2.Rows[0]["TotalRows"].ToString();
        DataTable table3 = Connect.GetTable(sql3);
        html += "<table id='postTable4' class='table table-bordered app-table dataTable no-footer text-sm' style='text-align:center;'>";
        html += "<thead>";
        html += "<tr>";
        html += "<th style='min-width:20px'>STT</th>";
        html += "<th>Hình ảnh</th>";
        html += "<th>Thông Tin</th>";
        html += "<th>Hoa hồng</th>";
        html += "<th>Cộng tác viên</th>";
        html += "<th>Trạng thái</th>";
        html += "<th>Duyệt lúc</th>";
        html += "<th>Hành động</th>";
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        // if (table3.Rows.Count == 0)
        //  {
        //      html += "<tr><td colspan='8' style='text-align: center;'>Không có kết quả</td></tr>";
        //   }
        int stt = pagethh * 10 - 9;
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string referralId = table3.Rows[i]["ReferralId"].ToString();
            string referralCommission = Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralCommission"].ToString());
            string referralStsOwnerApproved = table3.Rows[i]["ReferralStsOwnerApproved"].ToString();
            string referralStsOwnerCanceled = table3.Rows[i]["ReferralStsOwnerCanceled"].ToString();
            string referralStsCollabApproved = table3.Rows[i]["ReferralStsCollabApproved"].ToString();
            string referralStsCollabDenied = table3.Rows[i]["ReferralStsCollabDenied"].ToString();
            string referralStsAdminApproved = table3.Rows[i]["ReferralStsAdminApproved"].ToString();
            string referralOwnerApprovedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerApprovedAt"]).ToString("dd/MM/yyyy");
            string referralOwnerApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralOwnerCanceledAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerCanceledAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerCanceledAt"]).ToString("dd/MM/yyyy");
            string referralOwnerCanceledAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerCanceledAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerCanceledAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabApprovedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabApprovedAt"]).ToString("dd/MM/yyyy");
            string referralCollabApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabDeniedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabDeniedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabDeniedAt"]).ToString("dd/MM/yyyy");
            string referralCollabDeniedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabDeniedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabDeniedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralAdminApprovedAtFormatDate = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralAdminApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralAdminApprovedAt"]).ToString("dd/MM/yyyy");
            string referralAdminApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralAdminApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralAdminApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string postId = table3.Rows[i]["idTinDang"].ToString();
            string urlHinhAnh = (new Models.HinhAnh()).getImageUrlByIdTD(postId);
            string postTitle = table3.Rows[i]["TieuDe"].ToString();
            string postLink = "/tdct/" + table3.Rows[i]["DuongDan"].ToString();
            string postLinkDraf = "/preview/pv?MaTinDang=" + table3.Rows[i]["idTinDang"].ToString();
            string postCode = table3.Rows[i]["Code"].ToString();
            string postCreatedAt = DateTime.Parse(table3.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy");
            string postPriceFormat = "";
            if (table3.Rows[i]["PostPrice"].ToString() != "")
            {
                postPriceFormat = Utilities.Formatter.toCurrencyString(table3.Rows[i]["PostPrice"].ToString());
            }
            else if (table3.Rows[i]["PostPrice"].ToString() == "")
            {
                postPriceFormat = Utilities.Formatter.toCurrencyString("0");
            }
            string collabId = table3.Rows[i]["ReferralCollabId"].ToString();
            string collabFullname = table3.Rows[i]["CollabFullname"].ToString();
            string collabCode = table3.Rows[i]["CollabCode"].ToString();
            string collabPhone = table3.Rows[i]["CollabPhone"].ToString();
            string collabEmail = table3.Rows[i]["CollabEmail"].ToString();
            string collabAddress = table3.Rows[i]["CollabAddress"].ToString();
            string collabCity = table3.Rows[i]["CollabCityName"].ToString();
            string collabDistrict = table3.Rows[i]["CollabDistrictName"].ToString();
            string collabWard = table3.Rows[i]["CollabWardName"].ToString();
            string textCollabAttributes = "";
            textCollabAttributes += " data-referral-id='" + referralId + "' ";
            textCollabAttributes += " data-referral-commission='" + referralCommission + "' ";
            textCollabAttributes += " data-price-format='" + postPriceFormat + "' ";
            textCollabAttributes += " data-created-at='" + postCreatedAt + "' ";
            textCollabAttributes += " data-code='" + postCode + "' ";
            textCollabAttributes += " data-title='" + postTitle + "' ";
            textCollabAttributes += " data-link='" + postLink + "' ";
            textCollabAttributes += " data-rate='" + table3.Rows[i]["ReferralRate"].ToString() + "' ";
            textCollabAttributes += " data-amount='" + table3.Rows[i]["ReferralAmount"].ToString() + "' ";
            textCollabAttributes += " data-price='" + table3.Rows[i]["PostPrice"].ToString() + "' ";
            textCollabAttributes += " data-collab-id='" + collabId + "' ";
            textCollabAttributes += " data-post-id='" + postId + "' ";
            textCollabAttributes += " data-collab-fullname='" + collabFullname + "' ";
            textCollabAttributes += " data-collab-code='" + collabCode + "' ";
            textCollabAttributes += " data-collab-phone='" + collabPhone + "' ";
            textCollabAttributes += " data-collab-email='" + collabEmail + "' ";
            textCollabAttributes += " data-collab-address='" + collabAddress + "' ";
            textCollabAttributes += " data-collab-city='" + collabCity + "' ";
            textCollabAttributes += " data-collab-district='" + collabDistrict + "' ";
            textCollabAttributes += " data-collab-ward='" + collabWard + "' ";

            // start: row

            //{
            //    html += "<tr style='height: 100px;background-color: rgba(173, 178, 241, 0.5);pointer-events: none;' class='data-row'>";
            //}
            //else
            html += "<tr style='height: 100px;' class='data-row'>";
            // STT
            html += "  <td>" + stt + "</td>";
            stt++;
            // hinh anh
            if (table3.Rows[i]["isHetHan"].ToString() == "True" || table3.Rows[i]["isDuyet"].ToString() != "True")
            {
                html += "  <td style='min-width:130px'>";
                html += "<a target='_blank' href='" + postLinkDraf + "'><img src='" + urlHinhAnh + "' style='object-fit:cover;height:100px;' /> </a>";
                html += "  </td>";
            }
            else
            {
                html += "  <td style='min-width:130px'>";
                html += "<a target='_blank' href='" + postLink + "'><img src='" + urlHinhAnh + "' style='object-fit:cover;height:100px;' /> </a>";
                html += "  </td>";
            }
            // thong tin
            if (table3.Rows[i]["isHetHan"].ToString() == "True" || table3.Rows[i]["isDuyet"].ToString() != "True")
            {
                html += "  <td style='text-align:left;min-width:450px;'>";
                html += "      <div>";
                html += "<p onclick=\"" + "javascript:window.open('" + postLinkDraf + "','_blank')\"  style='margin: -29px 0 1px;padding-top:5px;cursor:pointer;color:rgba(0, 0, 0, 0.87);'><b>" + postTitle + "</b></p>";
                html += "<p onclick=\"" + "javascript:window.open('" + postLinkDraf + "','_blank')\"  style='margin: 0 0 1px;cursor:pointer;color:#fe9900;font-weight: 600;'><b>Mã: " + postCode + "</b></p>";

            }
            else
            {
                html += "  <td style='text-align:left;min-width:450px;'>";
                html += "      <div>";
                html += "<p onclick=\"" + "javascript:window.open('" + postLink + "','_blank')\" style='margin: -29px 0 1px;padding-top:5px;cursor:pointer;color:rgba(0, 0, 0, 0.87);'><b>" + postTitle + "</b></p>";
                html += "<p onclick=\"" + "javascript:window.open('" + postLink + "','_blank')\" style='margin: 0 0 1px;cursor:pointer;color:#fe9900;font-weight: 600;'><b>Mã: " + postCode + "</b></p>";
            }
            string a = table3.Rows[i]["TuGia"].ToString();
            if (table3.Rows[i]["TuGia"].ToString() != "")
            {
                html += "<p style='color:#ec3222;font-weight:600;'>Giá: " + Utilities.Formatter.toCurrencyString(table3.Rows[i]["TuGia"].ToString()) + "</p>";
            }
            else if (a == "")
            {
                html += "<p style='color:#ec3222;font-weight:600;>Giá: 0đ</p>";
            }
            if (table3.Rows[i]["NgayDang"].ToString() != "")
            {
                try
                {
                    html += "<p>Ngày đăng: " + DateTime.Parse(table3.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy") + "</p>";
                }
                catch { }
            }
            html += "      </div>";
            html += "  </td>";
            //hoa hong
            html += "  <td style='min-width:100px'>";
            if (table3.Rows[i]["ReferralCommission"].ToString() == "0")
            {
                html += "<strong>" + Utilities.Formatter.toCurrencyString("0") + "</strong>";
            }
            else
            {
                try
                {
                    html += "<strong>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralCommission"].ToString()) + "</strong>";
                }
                catch { }
            }
            html += "</td>";
            // collab
            html += "<td>";
            html += table3.Rows[i]["CollabCode"].ToString();
            if (table3.Rows[i]["CollabUsername"].ToString() != "")
            {
                html += "<p><a href='tel:" + table3.Rows[i]["CollabUsername"].ToString() + "'>" + table3.Rows[i]["CollabUsername"].ToString() + "</a></p>";
            }
            html += "</td>";
            // trang thai
            html += "  <td style='min-width:145px'>";
            if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "1")
            {
                html += @"  <label class='label label-danger text-label'>Bạn đã hủy trích hoa hồng</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"  <label class='label label-primary text-label'>Bạn đã xác nhận</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"  <label class='label label-info text-label'>CTV đã xác nhận</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "1" && referralStsAdminApproved == "0")
            {
                html += @"  <label class='label label-danger text-label'>CTV đã từ chối</label>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "1")
            {
                html += @"<label class='label label-success text-label'>Tung Tăng đã xác nhận</label>";
            }
            html += "  </td>";
            // approved at
            html += "  <td style='min-width:145px'>";
            if (!string.IsNullOrWhiteSpace(referralOwnerApprovedAtFormatDatetime))
            {
                html += "<p>Bạn đã duyệt: " + referralOwnerApprovedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabApprovedAtFormatDatetime))
            {
                html += "<p>CTV đã duyệt: " + referralCollabApprovedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralAdminApprovedAtFormatDatetime))
            {
                html += "<p>Tung Tăng đã duyệt: " + referralAdminApprovedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabDeniedAtFormatDatetime))
            {
                html += "<p>CTV đã từ chối: " + referralCollabDeniedAtFormatDatetime + "</p>";
            }
            if (!string.IsNullOrWhiteSpace(referralOwnerCanceledAtFormatDatetime))
            {
                html += "<p>Bạn đã hủy trích hoa hồng: " + referralOwnerCanceledAtFormatDatetime + "</p>";
            }
            html += "  </td>";
            // action
            html += @"<td  style='min-width:165px'>";
            string htmlCancelReferralBtn = @"<a style='width: 142px;' href='javascript:void(0);' class='btn btn-danger btn-sm btn-cancel-referral mt-2' data-referral-id='" + referralId + "'>Hủy trích hoa hồng</a>";
            if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "1")
            {

            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-primary btn-sm btn-update-referral mt-2' " + textCollabAttributes + " >Cập nhật hoa hồng</a>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-info btn-sm btn-update-referral mt-2' " + textCollabAttributes + " data-readonly='1' >Đợi Tung Tăng duyệt</a>";
                html += htmlCancelReferralBtn;
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "1" && referralStsAdminApproved == "0")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-primary btn-sm btn-update-referral mt-2' " + textCollabAttributes + " >Cập nhật hoa hồng</a>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "1")
            {
                html += @"<a href='javascript:void(0);' class='btn btn-success btn-sm btn-update-referral mt-2' " + textCollabAttributes + " data-readonly='1' >Tung Tăng đã duyệt</a>";
                html += htmlCancelReferralBtn;
            }
            html += "</td>";
            html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        divAccountPostTable4.InnerHtml = html;
        htmlPagination = Utilities.Table.renderPaginationSmallthh(totalItems, perPagethh, pagethh, "/thong-tin-ca-nhan/ttcn?Tab=4", paginationParams);
        divAccountPostPagination7.InnerHtml = htmlPagination;
        divAccountPostPagination8.InnerHtml = htmlPagination;
    }
    protected string getConditionByStatus(string filterReferralStatus, string tableName)
    {
        string s = " ";
        if (filterReferralStatus == Models.Referral.STATUS_ALL)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 0";
            s += @" OR ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" OR ISNULL(" + tableName + ".StsCollabApproved, 0) = 0 ";
            s += @" OR ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            s += @" OR ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_OWNER_APPROVED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            //  s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 0 ";
            //  s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            //  s += @" AND ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_OWNER_CANCELED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 1 ";
            //  s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 0 ";
            //  s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            //   s += @" AND ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_COLLAB_APPROVED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
            //  s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            //  s += @" AND ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_COLLAB_DENIED)
        {
            // s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            //   s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            // s += @" ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
            s += @" ISNULL(" + tableName + ".StsCollabDenied, 0) = 1 ";
            //     s += @" AND ISNULL(" + tableName + ".StsAdminApproved, 0) = 0 ";
        }
        else if (filterReferralStatus == Models.Referral.STATUS_ADMIN_APPROVED)
        {
            s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
            s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
            s += @" AND ISNULL(" + tableName + ".StsAdminApproved, 0) = 1 ";
        }
        //else if (filterReferralStatus == Models.Referral.STATUS_FEE_COMMISSION)
        //{
        //    s += @"     ISNULL(" + tableName + ".StsOwnerApproved, 0) = 1 ";
        //    s += @" AND ISNULL(" + tableName + ".StsOwnerCanceled, 0) = 0 ";
        //    s += @" AND ISNULL(" + tableName + ".StsCollabApproved, 0) = 1 ";
        //    s += @" AND ISNULL(" + tableName + ".StsCollabDenied, 0) = 0 ";
        //}
        return s;
    }
    public void checkSoDT()//Code new
    {
        string value = "";
        //string Email = "Select top 1 Email from tb_ThanhVien where idThanhVien = '" + idThanhVien + "' and isnull(isKhoa,'False')!= 'True'";
        //try
        //{
        //    DataTable tb = Connect.GetTable(Email);
        //    if (tb.Rows.Count > 0)
        //        value = tb.Rows[0][0].ToString();
        //}
        //catch
        //{ }
        string result = "";
        string sql = "Select top 1 SoDienThoai from tb_ThanhVien where idThanhVien='" + idThanhVien + "' and isnull(isKhoa,'False')!='True'";
        try
        {
            DataTable tb = Connect.GetTable(sql);
            if (tb.Rows.Count > 0)
                result = tb.Rows[0][0].ToString();
        }
        catch
        { }
        if (result != "")
        {
            //Response.Redirect("/dang-tin/dt");         
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn tc!')", true);
            string a = "";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn cần cập nhật thông tin số điện thoại!')", true);
            Response.Redirect("/thong-tin/tt");
        }
    }
    void LoadThongTinThanhVien()
    {
        DataTable table = Connect.GetTable("select * from tb_ThanhVien where idThanhVien=" + idThanhVien);
        if (table.Rows.Count > 0)
        {
            txtNameShopID.InnerHtml = "<h4>" + (table.Rows[0]["TenCuaHang"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["TenCuaHang"]) + "</h4>";
            txtInfoAccountCode.InnerText = "Mã thành viên: " + table.Rows[0]["Code"].ToString().Trim();
            txtLoaiUser.InnerText = "Bình Thường";
            //string htmlIndenti = "";
            //htmlIndenti += "<a class='MainFunctionButton EditProfile' runat='server' href='/thong-tin/pro'>Chỉnh sửa thông tin</a>";  
            if (table.Rows[0]["StsPro"].ToString().Trim() == "1")
            {
                //  UserIdenti.InnerHtml = htmlIndenti;
                //btnEditInfoUserID.Visible = false;
                txtLoaiUser.InnerText = "Thành Viên PRO";
                buttonUser.Visible = false;
                ImagePro.Style.Add("display", "");
            }
            string htmluser = "";
            if (table.Rows[0]["LyDo_KhongDuyet"].ToString().Trim() != "")
            {
                htmluser += "<div style='background-color:#fcf8e3;border-radius:10px;padding: 0 11px;'><p>Nội dung từ chối:" + table.Rows[0]["LyDo_KhongDuyet"].ToString().Trim() + "</p></div>";
                htmluser += "<a class='MainFunctionButton EditProfile' id='button_Forwad_mobi' style='background-color:#4cb050;color:white;margin-bottom:10px;height: 34px;' href='/thong-tin/pro'>Đăng ký lại</a>";
                ProNow.InnerHtml = htmluser;
                buttonUser.Visible = false;
                ImagePro.Visible = false;
            }
            if (table.Rows[0]["StsPro"].ToString().Trim() == "0" && table.Rows[0]["idAdmin_Duyet"].ToString().Trim() == "-1")
            {
                txtLoaiUser.InnerText = "Thành viên Pro đang chờ duyệt";
                ImagePro.Visible = false;
                buttonUser.Visible = false;
            }

            txtShopEmailID.InnerHtml = (table.Rows[0]["Email"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["Email"].ToString());
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["LinkAnh"].ToString().Trim()))
            {

                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["LinkAnh"].ToString().Trim();
            }
            else
                imgLinkAnh.Src = table.Rows[0]["LinkAnh"].ToString().Trim();

            // txtMST.InnerHtml = (table.Rows[0]["MST"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["MST"].ToString());
            // DateTime ngayCap = DateTime.MinValue;
            // if (table.Rows[0]["NgayCap"].ToString().Trim() != "")
            // {
            //     ngayCap = (DateTime)table.Rows[0]["NgayCap"];
            // }
            // string NgayCap = (table.Rows[0]["NgayCap"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayCap.ToString("MM-dd-yyyy")));
            // if (NgayCap == "01/01/1900")
            // {
            //     txtNgayCap.InnerHtml = "Chưa cung cấp";
            // }
            // else
            // {
            //     txtNgayCap.InnerHtml = NgayCap;
            // }
            // txtNoiCap.InnerHtml = (table.Rows[0]["NoiCap"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["NoiCap"].ToString());

            txtShopPhoneID.InnerHtml = (table.Rows[0]["SoDienThoai"].ToString().Trim() == "" ? "Chưa cung cấp" : table.Rows[0]["SoDienThoai"].ToString());
            // DateTime ngayDangKy = (DateTime)table.Rows[0]["NgayDangKy"];
            txtDateShopApprovedID.InnerHtml = (table.Rows[0]["NgayDangKy"].ToString().Trim() == "" ? "Chưa cung cấp" : DateTime.Parse(table.Rows[0]["NgayDangKy"].ToString()).ToString("dd-MM-yyyy"));

            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim();
            string TenTinhThanh = StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString());
            string TenQuanHuyen = StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString());
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString());
            string DiaChi = table.Rows[0]["DiaChi"].ToString().Trim();
            if (DiaChi == "" && TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                txtShopAddressID.InnerHtml = "Chưa cung cấp";
            else
            {
                txtShopAddressID.InnerHtml = DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh;
                txtShopAddressID.Attributes.Add("title", DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh);
            }

            string idThanhVien_Cookie = null;
            try
            {
                idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
            }
            catch { }
            if (idThanhVien != idThanhVien_Cookie)
            {
                //UserIdenti.Visible = false;
                //btnEditInfoUserID.Visible = false;
                TitleHeader.InnerText = "TIN CÙNG NGƯỜI ĐĂNG";
                // buttonUser.Visible = false;
                ProNow.Visible = false;
                if (table.Rows[0]["StsPro"].ToString().Trim() == "1")
                {
                    //  UserIdenti.InnerHtml = htmlIndenti;
                    //btnEditInfoUserID.Visible = false;
                    txtLoaiUser.InnerText = "Thành Viên PRO";
                    ImagePro.Style.Add("display", "");
                }
                else
                {
                    txtLoaiUser.InnerText = "Bình Thường";
                    ImagePro.Style.Add("display", "");
                }
                btnTin.Visible = false;
                btnLoc.Visible = false;
                divAccountPostTable.Visible = false;
                divAccountPostTable2.Visible = false;
                divAccountPostTable3.Visible = false;
                divAccountPostTable4.Visible = false;
            }

        }
    }
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }

    protected string getStatusSelect(string id, string status)
    {
        string html = "<select id='slHetHan_" + id + "' disabled='disabled'  style='font-size: 11px;'>";
        html += "   <option value='" + Models.TinDang.STATUS_ACTIVE + "' " + (status == Models.TinDang.STATUS_ACTIVE ? " selected " : "") + " >" + Models.TinDang.STATUS_ACTIVE_TXT + "</option>";
        html += "   <option value='" + Models.TinDang.STATUS_PENDING + "' " + (status == Models.TinDang.STATUS_PENDING ? " selected " : "") + " >" + Models.TinDang.STATUS_PENDING_TXT + "</option>";
        html += "   <option value='" + Models.TinDang.STATUS_DENY + "' " + (status == Models.TinDang.STATUS_DENY ? " selected " : "") + " >" + Models.TinDang.STATUS_DENY_TXT + "</option>";
        html += "   <option value='" + Models.TinDang.STATUS_EXPIRED + "' " + (status == Models.TinDang.STATUS_EXPIRED ? " selected " : "") + " >" + Models.TinDang.STATUS_EXPIRED_TXT + "</option>";
        html += "</select>";
        return html;
    }
    protected string TinhThoiGianLucDang(DateTime NgayDang)
    {
        DateTime ThoiGianHienTai = DateTime.Now;
        int SoGiay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalSeconds);
        int SoPhut = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalMinutes);
        int SoGio = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalHours);
        int SoNgay = (int)Math.Abs(ThoiGianHienTai.Subtract(NgayDang).TotalDays);

        if (SoNgay > 7)
            return SoNgay / 7 + " tuần trước";
        else
        {
            if (SoNgay > 0)
                return SoNgay + " ngày trước";
            else
            {
                if (SoGio > 0)
                    return SoGio + " giờ trước";
                else
                {
                    if (SoPhut > 0)
                        return SoPhut + " phút trước";
                    else
                        return SoGiay + " giây trước";
                }
            }
        }

        return "Unknown";
    }

    protected void loadStatusReferralTemplate()
    {
        this.templateJsonReferralStatus = "[";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_ALL + @"', text: 'Tất cả'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_OWNER_APPROVED + @"', text: 'Bạn đã xác nhận'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_OWNER_CANCELED + @"', text: 'Bạn đã hủy trích hoa hồng'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_COLLAB_APPROVED + @"', text: 'CTV đã xác nhận'},";
        // this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_COLLAB_DENIED + @"', text: 'CTV đã từ chối'},";
        this.templateJsonReferralStatus += @"{value:'" + Models.Referral.STATUS_ADMIN_APPROVED + @"', text: 'Tung Tăng đã xác nhận'},";
        this.templateJsonReferralStatus += "]";
    }
    protected string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    protected void initAccountSelledDropdownList()
    {
        DataTable table = Connect.GetTable(@"
            SELECT 
	            ISNULL(TBCollabs.idThanhVien, '') AS CollabId,
	            ISNULL(TBCollabs.Code, '') AS CollabCode,
	            ISNULL(TBCollabs.TenCuaHang, '') AS CollabFullname,
	            ISNULL(TBCollabs.SoDienThoai, '') AS CollabPhone, 
	            ISNULL(TBCollabs.Email, '') AS CollabEmail, 
	            ISNULL(TBCollabs.DiaChi, '') AS CollabAddress,
	            ISNULL(TBCollabCities.Ten, '') AS CollabCityName,
	            ISNULL(TBCollabDistricts.Ten, '') AS CollabDistrictName,
	            ISNULL(TBCollabWards.Ten, '') AS CollabWardName
            FROM tb_ThanhVien TBCollabs
	            LEFT JOIN City TBCollabCities
		            ON TBCollabs.idTinh = TBCollabCities.id
	            LEFT JOIN District TBCollabDistricts
		            ON TBCollabs.idHuyen = TBCollabDistricts.id
	            LEFT JOIN tb_PhuongXa TBCollabWards
		            ON TBCollabs.idPhuongXa = TBCollabWards.id
            WHERE TBCollabs.idThanhVien != '" + idThanhVien + @"'
            ORDER BY TBCollabs.NgayDangKy DESC
        ");

        drdlModalAccountSelled.Items.Clear();
        ListItem item0 = new ListItem("Chưa có", "None");
        item0.Attributes.Add("data-collab-id", "");
        item0.Attributes.Add("data-collab-code", "");
        item0.Attributes.Add("data-collab-fullname", "");
        item0.Attributes.Add("data-collab-phone", "");
        item0.Attributes.Add("data-collab-email", "");
        item0.Attributes.Add("data-collab-address", "");
        item0.Attributes.Add("data-collab-city", "");
        item0.Attributes.Add("data-collab-district", "");
        item0.Attributes.Add("data-collab-ward", "");
        drdlModalAccountSelled.Items.Add(item0);

        for (int i = 0; i < table.Rows.Count; i = i + 1)
        {
            ListItem item = new ListItem(table.Rows[i]["CollabCode"].ToString() + " - " + table.Rows[i]["CollabPhone"].ToString(), table.Rows[i]["CollabId"].ToString());
            item.Attributes.Add("data-collab-id", table.Rows[i]["CollabId"].ToString());
            item.Attributes.Add("data-collab-code", table.Rows[i]["CollabCode"].ToString());
            item.Attributes.Add("data-collab-fullname", table.Rows[i]["CollabFullname"].ToString());
            item.Attributes.Add("data-collab-phone", table.Rows[i]["CollabPhone"].ToString());
            item.Attributes.Add("data-collab-email", table.Rows[i]["CollabEmail"].ToString());
            item.Attributes.Add("data-collab-address", table.Rows[i]["CollabAddress"].ToString());
            item.Attributes.Add("data-collab-city", table.Rows[i]["CollabCityName"].ToString());
            item.Attributes.Add("data-collab-district", table.Rows[i]["CollabDistrictName"].ToString());
            item.Attributes.Add("data-collab-ward", table.Rows[i]["CollabWardName"].ToString());
            drdlModalAccountSelled.Items.Add(item);
        }
        drdlModalAccountSelled.DataBind();
    }
    protected void loadCategories()
    {
        string sql1 = @"
            SELECT
	            TBCategories1.idDanhMucCap1 AS CategoryId1,
	            TBCategories1.TenDanhMucCap1 AS CategoryName1,
	            TBCategories2.idDanhMucCap2 AS CategoryId2,
	            TBCategories2.TenDanhMucCap2 AS CategoryName2
            FROM tb_DanhMucCap1 TBCategories1
	            LEFT JOIN tb_DanhMucCap2 TBCategories2
		            ON TBCategories1.idDanhMucCap1 = TBCategories2.idDanhMucCap1
            ORDER BY TBCategories1.idDanhMucCap1 ASC, TBCategories2.idDanhMucCap2 ASC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        string currentCate = null;
        templateJsonCategories1 = "[";
        templateJsonCategories2 = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            if (currentCate != table1.Rows[i]["CategoryId1"].ToString())
            {
                currentCate = table1.Rows[i]["CategoryId1"].ToString();
                templateJsonCategories1 += @"{id1:'" + table1.Rows[i]["CategoryId1"].ToString() + @"', name1: '" + table1.Rows[i]["CategoryName1"].ToString() + @"'},";
            }
            else
            {
                templateJsonCategories2 += @"{id1:'" + table1.Rows[i]["CategoryId1"].ToString() + @"', id2:'" + table1.Rows[i]["CategoryId2"].ToString() + @"', name2: '" + table1.Rows[i]["CategoryName2"].ToString() + @"'},";
            }
        }
        templateJsonCategories1 += "]";
        templateJsonCategories2 += "]";
    }
    protected void loadOwners()
    {
        string sql1 = @"
           SELECT Distinct Referrals.CollabId AS CollabId,	
			TBCollabs.TenCuaHang AS CollabName,
			TBCollabs.SoDienThoai AS CollabPhone,
            TBCollabs.Code AS CollabCode                      	
            FROM Referrals          	
            INNER JOIN tb_TinDang          	
            ON Referrals.TinDangId = tb_TinDang.idTinDang            	
            INNER JOIN tb_ThanhVien TBCollabs           	
            ON Referrals.CollabId = TBCollabs.idThanhVien  	 	
            WHERE 1 = 1 AND tb_TinDang.idThanhVien ='" + idThanhVien + @"'
         ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonOwners = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonOwners += @"{id:'" + table1.Rows[i]["CollabId"].ToString() + @"', code:'" + table1.Rows[i]["CollabCode"].ToString() + @"', phone:'" + table1.Rows[i]["CollabPhone"].ToString() + @"', fullname: '" + table1.Rows[i]["CollabName"].ToString() + @"'},";
        }
        this.templateJsonOwners += "]";
    }
    protected void loadidPost()
    {
        string sql1 = @"
            SELECT 
	            TBPost.idTinDang AS PostId,
	            TBPost.Code AS PostCode	            
            FROM tb_TinDang TBPost
            WHERE idThanhVien ='" + idThanhVien + @"'
            AND TBPost.isDuyet = 1
			AND tbPost.isHetHan = 0 
            AND ISNULL(tbPost.IsDraft, 0) = 0
            ORDER BY NgayDang DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonCode = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonCode += @"{id:'" + table1.Rows[i]["PostId"].ToString() + @"', code:'" + table1.Rows[i]["PostCode"].ToString() + @"'},";
        }
        this.templateJsonCode += "]";
    }
    protected void loadidPost2()
    {
        string sql1 = @"
            SELECT 
	            TBPost.idTinDang AS PostId,
	            TBPost.Code AS PostCode	            
            FROM tb_TinDang TBPost
            WHERE idThanhVien ='" + idThanhVien + @"' AND TBPost.isDuyet IS NULL
					AND tbPost.isHetHan = 0 
            		AND ISNULL(tbPost.IsDraft, 0) = 0
            ORDER BY NgayDang DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonCode2 = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonCode2 += @"{id:'" + table1.Rows[i]["PostId"].ToString() + @"', code:'" + table1.Rows[i]["PostCode"].ToString() + @"'},";
        }
        this.templateJsonCode2 += "]";
    }
    protected void loadidPost3()
    {
        string sql1 = @"
            SELECT 
	            TBPost.idTinDang AS PostId,
	            TBPost.Code AS PostCode	            
            FROM tb_TinDang TBPost
            WHERE idThanhVien ='" + idThanhVien + @"' AND TBPost.isDuyet = 0
            AND tbPost.isHetHan = 0 
            AND ISNULL(tbPost.IsDraft, 0) = 0 
            ORDER BY NgayDang DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonCode3 = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonCode3 += @"{id:'" + table1.Rows[i]["PostId"].ToString() + @"', code:'" + table1.Rows[i]["PostCode"].ToString() + @"'},";
        }
        this.templateJsonCode3 += "]";
    }
    protected void loadidPost4()
    {
        string sql1 = @"
       SELECT   	
            Distinct tb_TinDang.idTinDang AS PostId,	
            tb_TinDang.NgayDang,	
            tb_TinDang.Code AS PostCode                     	
            FROM Referrals          	
            INNER JOIN tb_TinDang          	
            ON Referrals.TinDangId = tb_TinDang.idTinDang            	
            LEFT JOIN tb_ThanhVien TBCollabs           	
            ON Referrals.CollabId = TBCollabs.idThanhVien  	
            WHERE 1 = 1 AND tb_TinDang.idThanhVien ='" + idThanhVien + @"'
            ORDER BY NgayDang DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonCode4 = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonCode4 += @"{id:'" + table1.Rows[i]["PostId"].ToString() + @"', code:'" + table1.Rows[i]["PostCode"].ToString() + @"'},";
        }
        this.templateJsonCode4 += "]";
    }
    protected void generateFilterInputs()
    {
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostCode")))
        {
            string filterPostCode = Request.QueryString.Get("FilterPostCode").Trim();
            this.filterPostCode = filterPostCode;
            this.templateFilterPostCode = filterPostCode;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostOwner")))
        {
            string filterPostOwner = Request.QueryString.Get("FilterPostOwner").Trim();
            this.filterPostOwner = filterPostOwner;
            this.templateFilterPostOwner = filterPostOwner;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostTitle")))
        {
            string filterPostTitle = Request.QueryString.Get("FilterPostTitle").Trim();
            this.filterPostTitle = filterPostTitle;
            this.templateFilterPostTitle = filterPostTitle;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterReferralStatus")))
        {
            string filterReferralStatus = Request.QueryString.Get("FilterReferralStatus").Trim();
            this.filterReferralStatus = filterReferralStatus;
            this.templateFilterReferralStatus = filterReferralStatus;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterReferralStatusOther2")))
        {
            string filterReferralStatusOther2 = Request.QueryString.Get("FilterReferralStatusOther2").Trim();
            this.filterReferralStatusOther2 = filterReferralStatusOther2;
            this.templateFilterReferralStatusOther2 = filterReferralStatusOther2;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterOwnerApprovedAtFrom")))
        {
            DateTime filterOwnerApprovedAtFrom_dateTime;
            string filterOwnerApprovedAtFrom = Request.QueryString.Get("FilterOwnerApprovedAtFrom").Trim();
            if (DateTime.TryParseExact(filterOwnerApprovedAtFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterOwnerApprovedAtFrom_dateTime))
            {
                this.filterOwnerApprovedAtFromAltDate = filterOwnerApprovedAtFrom;
                this.filterOwnerApprovedAtFromAltDateTime = filterOwnerApprovedAtFrom + " 00:00:00:000";
                this.templateFilterOwnerApprovedAtFrom = filterOwnerApprovedAtFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterOwnerApprovedAtTo")))
        {
            DateTime filterOwnerApprovedAtTo_dateTime;
            string filterOwnerApprovedAtTo = Request.QueryString.Get("FilterOwnerApprovedAtTo").Trim();
            if (DateTime.TryParseExact(filterOwnerApprovedAtTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterOwnerApprovedAtTo_dateTime))
            {
                this.filterOwnerApprovedAtToAltDate = filterOwnerApprovedAtTo;
                this.filterOwnerApprovedAtToAltDateTime = filterOwnerApprovedAtTo + " 23:23:23:999";
                this.templateFilterOwnerApprovedAtTo = filterOwnerApprovedAtTo;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostPrice")))
        {
            string filterPostPrice = Request.QueryString.Get("FilterPostPrice").Trim();
            this.filterPostPrice = filterPostPrice;
            this.templateFilterPostPrice = filterPostPrice;
            string[] temp = filterPostPrice.Split(';');
            if (temp.Length == 2)
            {
                Double.TryParse(temp[0], out this.filterFromPostPrice);
                Double.TryParse(temp[1], out this.filterToPostPrice);
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostCommission")))
        {
            string filterPostCommission = Request.QueryString.Get("FilterPostCommission").Trim();
            this.filterPostCommission = filterPostCommission;
            this.templateFilterPostCommission = filterPostCommission;
            string[] temp = filterPostCommission.Split(';');
            if (temp.Length == 2)
            {
                Double.TryParse(temp[0], out this.filterFromPostCommission);
                Double.TryParse(temp[1], out this.filterToPostCommission);
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedAtFrom")))
        {
            DateTime filterCreatedAtFrom_dateTime;
            string filterCreatedAtFrom = Request.QueryString.Get("FilterCreatedAtFrom").Trim();
            if (DateTime.TryParseExact(filterCreatedAtFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedAtFrom_dateTime))
            {
                this.filterCreatedAtFromAltDate = filterCreatedAtFrom;
                this.filterCreatedAtFromAltDateTime = filterCreatedAtFrom + " 00:00:00:000";
                this.templateFilterCreatedAtFrom = filterCreatedAtFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedAtTo")))
        {
            DateTime filterCreatedAtTo_dateTime;
            string filterCreatedAtTo = Request.QueryString.Get("FilterCreatedAtTo").Trim();
            if (DateTime.TryParseExact(filterCreatedAtTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedAtTo_dateTime))
            {
                this.filterCreatedAtToAltDate = filterCreatedAtTo;
                this.filterCreatedAtToAltDateTime = filterCreatedAtTo + " 23:23:23:999";
                this.templateFilterCreatedAtTo = filterCreatedAtTo;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedUpFrom")))
        {
            DateTime filterCreatedUpFrom_dateTime;
            string filterCreatedUpFrom = Request.QueryString.Get("FilterCreatedUpFrom").Trim();
            if (DateTime.TryParseExact(filterCreatedUpFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedUpFrom_dateTime))
            {
                this.filterCreatedUpFromAltDate = filterCreatedUpFrom;
                this.filterCreatedUpFromAltDateTime = filterCreatedUpFrom + " 00:00:00:000";
                this.templateFilterCreatedUpFrom = filterCreatedUpFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedUpTo")))
        {
            DateTime filterCreatedUpTo_dateTime;
            string filterCreatedUpTo = Request.QueryString.Get("FilterCreatedUpTo").Trim();
            if (DateTime.TryParseExact(filterCreatedUpTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedUpTo_dateTime))
            {
                this.filterCreatedUpToAltDate = filterCreatedUpTo;
                this.filterCreatedUpToAltDateTime = filterCreatedUpTo + " 23:23:23:999";
                this.templateFilterCreatedUpTo = filterCreatedUpTo;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedPostFrom")))
        {
            DateTime filterCreatedPostFrom_dateTime;
            string filterCreatedPostFrom = Request.QueryString.Get("FilterCreatedPostFrom").Trim();
            if (DateTime.TryParseExact(filterCreatedPostFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedPostFrom_dateTime))
            {
                this.filterCreatedPostFromAltDate = filterCreatedPostFrom;
                this.filterCreatedPostFromAltDateTime = filterCreatedPostFrom + " 00:00:00:000";
                this.templateFilterCreatedPostFrom = filterCreatedPostFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedPostTo")))
        {
            DateTime filterCreatedPostTo_dateTime;
            string filterCreatedPostTo = Request.QueryString.Get("FilterCreatedPostTo").Trim();
            if (DateTime.TryParseExact(filterCreatedPostTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedPostTo_dateTime))
            {
                this.filterCreatedPostToAltDate = filterCreatedPostTo;
                this.filterCreatedPostToAltDateTime = filterCreatedPostTo + " 23:23:23:999";
                this.templateFilterCreatedPostTo = filterCreatedPostTo;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedDeniedFrom")))
        {
            DateTime filterCreatedDeniedFrom_dateTime;
            string filterCreatedDeniedFrom = Request.QueryString.Get("FilterCreatedDeniedFrom").Trim();
            if (DateTime.TryParseExact(filterCreatedDeniedFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedDeniedFrom_dateTime))
            {
                this.filterCreatedDeniedFromAltDate = filterCreatedDeniedFrom;
                this.filterCreatedDeniedFromAltDateTime = filterCreatedDeniedFrom + " 00:00:00:000";
                this.templateFilterCreatedDeniedFrom = filterCreatedDeniedFrom;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCreatedDeniedTo")))
        {
            DateTime filterCreatedDeniedTo_dateTime;
            string filterCreatedDeniedTo = Request.QueryString.Get("FilterCreatedDeniedTo").Trim();
            if (DateTime.TryParseExact(filterCreatedDeniedTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterCreatedDeniedTo_dateTime))
            {
                this.filterCreatedDeniedToAltDate = filterCreatedDeniedTo;
                this.filterCreatedDeniedToAltDateTime = filterCreatedDeniedTo + " 23:23:23:999";
                this.templateFilterCreatedDeniedTo = filterCreatedDeniedTo;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCategory1")))
        {
            string filterCategory1 = Request.QueryString.Get("FilterCategory1").Trim();
            this.filterCategory1 = filterCategory1;
            this.templateFilterCategory1 = filterCategory1;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCategory2")))
        {
            string filterCategory2 = Request.QueryString.Get("FilterCategory2").Trim();
            this.filterCategory2 = filterCategory2;
            this.templateFilterCategory2 = filterCategory2;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCity")))
        {
            string filterCity = Request.QueryString.Get("FilterCity").Trim();
            this.filterCity = filterCity;
            this.templateFilterCity = filterCity;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterDistrict")))
        {
            string filterDistrict = Request.QueryString.Get("FilterDistrict").Trim();
            this.filterDistrict = filterDistrict;
            this.templateFilterDistrict = filterDistrict;
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterWard")))
        {
            string filterWard = Request.QueryString.Get("FilterWard").Trim();
            this.filterWard = filterWard;
            this.templateFilterWard = filterWard;
        }
    }
    protected void trichhh_ServerClick(object sender, EventArgs e)
    {
        string url = "/thong-tin-ca-nhan/ttcn?Tab=4&";
        Response.Redirect(url);
    }

    protected void bituchoi_ServerClick(object sender, EventArgs e)
    {
        string url = "/thong-tin-ca-nhan/ttcn?Tab=3&";
        Response.Redirect(url);
    }

    protected void choduyet_ServerClick(object sender, EventArgs e)
    {
        string url = "/thong-tin-ca-nhan/ttcn?Tab=2&";
        Response.Redirect(url);
    }

    protected void daduyet_ServerClick(object sender, EventArgs e)
    {
        string url = "/thong-tin-ca-nhan/ttcn?Tab=1&";
        Response.Redirect(url);
    }

    protected void btnUpgradeSubmit_Click(object sender, EventArgs e)
    {
        object[] rs = this.calcFeeData();
        bool isSuccess = (bool)rs[0];
        string code = (string)rs[1];
        long total = (long)rs[2];
        DateTime startDate = (DateTime)rs[3];
        DateTime endDate = (DateTime)rs[4];
        if (!isSuccess)
        {
            Utilities.Redirect.balanceNotEnough(Response);
        }

        string upgradeId = this.txtUpgradeId.Text.Trim();
        string upgradeRedirect = this.txtUpgradeRedirect.Text.Trim();

        if (string.IsNullOrWhiteSpace(upgradeId) || string.IsNullOrWhiteSpace(upgradeRedirect))
        {
            Utilities.Redirect.notFound();
        }

        DataRow post = Services.Post.find(upgradeId);
        if (post == null || Utilities.Post.getPostFee(post) != "default_pack")
        {
            Utilities.Redirect.notFound();
        }

        spentMoney(total, Utilities.BalanceHistory.SPENT_TYPE_UPGRADE_POST, code);
        Services.Post.upgradePostFee(post["idTinDang"].ToString(), code, startDate, endDate, total);

        Response.Redirect(upgradeRedirect);
    }

    protected object[] calcFeeData()
    {
        object[] rs = new object[5];
        string packageCode = txtUpgradePackage.Text.Trim();
        string periodValue = txtUpgradePeriod.Text.Trim();
        string daysValue = txtUpgradeDays.Text.Trim();

        DataRow pack = Services.PostFees.findByCode(packageCode);

        if (pack == null)
        {
            rs[0] = false;
            return rs;
        }
        rs[1] = pack.Field<string>("Code");

        long perPrice = 0;
        long days = 0;
        switch (periodValue)
        {
            case "pricePer1Days":
                perPrice = pack.Field<long>("PricePer1Days");
                days = 1;
                break;
            case "pricePer7Days":
                perPrice = pack.Field<long>("PricePer7Days");
                days = 7;
                break;
            case "pricePer14Days":
                perPrice = pack.Field<long>("PricePer14Days");
                days = 14;
                break;
            case "pricePer30Days":
                perPrice = pack.Field<long>("PricePer30Days");
                days = 30;
                break;
            case "pricePerOption":
                perPrice = pack.Field<long>("PricePer1Days");
                days = long.Parse(daysValue);
                break;
            default:
                rs[0] = false;
                return rs;
        }
        rs[2] = perPrice * days;

        long balance = Utilities.Auth.getBalance();
        if (balance < (long)rs[2])
        {
            rs[0] = false;
        }
        else
        {
            rs[0] = true;
        }

        DateTime startDate = DateTime.Now;
        DateTime endDate = startDate.AddDays(days);

        rs[3] = startDate;
        rs[4] = endDate;

        return rs;
    }

    protected void spentMoney(long total, string spentType, string feeCode)
    {
        if (total <= 0)
        {
            return;
        }
        string accountId = Utilities.Auth.getAccountId();
        Services.Account.spentBalance(accountId, total);
        Services.BalanceHistory.createSpent(accountId, spentType, total, feeCode);
    }

    protected void btnExtendSubmit_Click(object sender, EventArgs e)
    {
        string packageCode = txtExtendPackage.Text.Trim();
        string periodValue = txtExtendPeriod.Text.Trim();
        string daysValue = txtExtendDays.Text.Trim();
        string extendIdValue = txtExtendId.Text.Trim();

        DataRow post = Services.Post.find(extendIdValue);
        if(post == null)
        {
            Utilities.Redirect.notFound();
        }

        if(post.Field<string>("FeeCode") != packageCode)
        {
            Utilities.Redirect.notFound();
        }

        DataRow pack = Services.PostFees.findByCode(packageCode);
        if(pack == null)
        {
            Utilities.Redirect.notFound();
        }

        long perPrice = 0;
        long days = 0;
        switch (periodValue)
        {
            case "pricePer1Days":
                perPrice = pack.Field<long>("PricePer1Days");
                days = 1;
                break;
            case "pricePer7Days":
                perPrice = pack.Field<long>("PricePer7Days");
                days = 7;
                break;
            case "pricePer14Days":
                perPrice = pack.Field<long>("PricePer14Days");
                days = 14;
                break;
            case "pricePer30Days":
                perPrice = pack.Field<long>("PricePer30Days");
                days = 30;
                break;
            case "pricePerOption":
                perPrice = pack.Field<long>("PricePer1Days");
                days = long.Parse(daysValue);
                break;
            default:
                Utilities.Redirect.notFound();
                return;
        }
        long total = perPrice * days;

        long balance = Utilities.Auth.getBalance();
        if (balance < total)
        {
            Utilities.Redirect.balanceNotEnough(Response);
            return;
        }

        DateTime endDate = post.Field<DateTime>("FeeEndTime").AddDays(days);

        spentMoney(total, Utilities.BalanceHistory.SPENT_TYPE_EXTEND_POST, packageCode);
        Services.Post.extendPostFee(post["idTinDang"].ToString(), endDate, total);
    }

    protected void btnRenovateSubmit_Click(object sender, EventArgs e)
    {
        string postId = txtRenovateId.Text.Trim();
        string redirect = txtRenovateRedirect.Text.Trim();

        DataRow post = Services.Post.find(postId);
        if(post == null)
        {
            return;
        }

        string accountId = Utilities.Auth.getAccountId();
        if(string.IsNullOrWhiteSpace(accountId))
        {
            return;
        }

        long balance = Utilities.Auth.getBalance();
        long price = Services.RenovateFees.getPrice();
        if(balance < price)
        {
            return;
        }

        Services.Account.spentBalance(accountId, price);
        Services.BalanceHistory.createSpentRenovate(accountId, Utilities.BalanceHistory.SPENT_TYPE_TO_TOP, price);
        Services.Post.renovate(postId);

        HttpCookie cookieData = new HttpCookie("renovate_success");
        cookieData.Value = "" + post.Field<string>("TieuDe");
        cookieData.Expires = DateTime.Now.AddSeconds(10);

        Response.Cookies.Add(cookieData);
        Response.Redirect(redirect);
    }

    protected string renderFeeActionButtons(string postId)
    {
        string html = "";
        DataRow post = Services.Post.find(postId);

        return html;
    }
}