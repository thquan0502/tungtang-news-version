﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Globalization;
using System.Data;

public partial class AccountAjax : System.Web.UI.Page
{
    protected string reqAction = null;
    protected string authId = null;
    protected bool errorflag = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        Dictionary<string, Object> rs = new Dictionary<string, Object>();

        if (Request.Cookies["TungTang_Login"] != null && !string.IsNullOrWhiteSpace(Request.Cookies["TungTang_Login"].Value))
        {
            this.authId = Request.Cookies["TungTang_Login"].Value.Trim();

            if (!string.IsNullOrWhiteSpace(Request.QueryString["Action"]))
            {
                this.reqAction = Request.QueryString["Action"].Trim();
            }
            switch (this.reqAction)
            {
                case "ChartData":
                    rs = this.getChartData(rs);
                    break;
            }
        }

        rs.Add("errorflag", this.errorflag);
        string json = JsonConvert.SerializeObject(rs);
        Response.ContentType = "text/json";
        Response.Write(json);
    }
    protected Dictionary<string, Object> getChartData(Dictionary<string, Object> rs)
    {
        string reqFrom = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString["from"]))
        {
            reqFrom = Request.QueryString["from"].Trim();
        }
        string reqTo = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString["to"]))
        {
            reqTo = Request.QueryString["to"].Trim();
        }

        if (string.IsNullOrWhiteSpace(reqFrom) || string.IsNullOrWhiteSpace(reqTo))
        {
            this.errorflag = true;
            return rs;
        }

        DateTime from_datetime, to_datetime;
        String sFrom_date = null, sFrom_datetime = null, sTo_date = null, sTo_datetime = null;
        if (DateTime.TryParseExact(reqFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out from_datetime))
        {
            sFrom_date = from_datetime.ToString("yyyy-MM-dd");
            sFrom_datetime = sFrom_date + " 00:00:00";
        }
        if (DateTime.TryParseExact(reqTo, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out to_datetime))
        {
            sTo_date = to_datetime.ToString("yyyy-MM-dd");
            sTo_datetime = sTo_date + " 23:59:59";
        }

        List<Dictionary<string, Object>> data = new List<Dictionary<string, Object>>();

        if (sFrom_date != sTo_date)
        {
            string sql1 = @"
                SELECT 
	                ViewedByDay.EstimateDate AS EstimateDate,
	                ISNULL(ViewedByDay.EstimateValue, 0) AS EstimateValue
                FROM ViewedByDay
                WHERE ViewedByDay.AccountId = '"+this.authId + @"'
	                AND EstimateDate >= '"+ sFrom_date + @"'
	                AND EstimateDate <= '"+ sTo_date + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            for(int i =0; i < table1.Rows.Count; i = i + 1)
            {
                data.Add(new Dictionary<string, Object> { { "date", ((DateTime) table1.Rows[i]["EstimateDate"]).ToString("yyyy-MM-dd") }, { "value", table1.Rows[i]["EstimateValue"].ToString() } });
            }
        }
        else
        {
            string sql2 = @"
                SELECT 
	                ViewedByDay.EstimateDate AS EstimateDate,
	                ISNULL(ViewedByDay.EstimateH0, 0) AS EstimateH0,
	                ISNULL(ViewedByDay.EstimateH1, 0) AS EstimateH1,
	                ISNULL(ViewedByDay.EstimateH2, 0) AS EstimateH2,
	                ISNULL(ViewedByDay.EstimateH3, 0) AS EstimateH3,
	                ISNULL(ViewedByDay.EstimateH4, 0) AS EstimateH4,
	                ISNULL(ViewedByDay.EstimateH5, 0) AS EstimateH5,
	                ISNULL(ViewedByDay.EstimateH6, 0) AS EstimateH6,
	                ISNULL(ViewedByDay.EstimateH7, 0) AS EstimateH7,
	                ISNULL(ViewedByDay.EstimateH8, 0) AS EstimateH8,
	                ISNULL(ViewedByDay.EstimateH9, 0) AS EstimateH9,
	                ISNULL(ViewedByDay.EstimateH10, 0) AS EstimateH10,
	                ISNULL(ViewedByDay.EstimateH11, 0) AS EstimateH11,
	                ISNULL(ViewedByDay.EstimateH12, 0) AS EstimateH12,
	                ISNULL(ViewedByDay.EstimateH13, 0) AS EstimateH13,
	                ISNULL(ViewedByDay.EstimateH14, 0) AS EstimateH14,
	                ISNULL(ViewedByDay.EstimateH15, 0) AS EstimateH15,
	                ISNULL(ViewedByDay.EstimateH16, 0) AS EstimateH16,
	                ISNULL(ViewedByDay.EstimateH17, 0) AS EstimateH17,
	                ISNULL(ViewedByDay.EstimateH18, 0) AS EstimateH18,
	                ISNULL(ViewedByDay.EstimateH19, 0) AS EstimateH19,
	                ISNULL(ViewedByDay.EstimateH20, 0) AS EstimateH20,
	                ISNULL(ViewedByDay.EstimateH21, 0) AS EstimateH21,
	                ISNULL(ViewedByDay.EstimateH22, 0) AS EstimateH22,
	                ISNULL(ViewedByDay.EstimateH23, 0) AS EstimateH23
                FROM ViewedByDay
                WHERE ViewedByDay.AccountId = '"+this.authId+@"'
	                AND EstimateDate = '"+sFrom_date+@"'
            ";
            DataTable table2 = Connect.GetTable(sql2);
            int cnt = table2.Rows.Count;
            for (int i = 0; i < 24; i = i + 1)
            {
                string cntValue = "0";
                if(cnt > 0)
                {
                    cntValue = table2.Rows[0]["EstimateH" + i].ToString();
                }
                data.Add(new Dictionary<string, Object> { { "date", i }, { "value", cntValue } });
            }
        }

        string sql3 = @"
            SELECT
	            COUNT(*) AS TOTALROWS
            FROM Referrals
            WHERE Referrals.CollabId = '"+this.authId+@"'
	            AND OwnerApprovedAt >= '"+sFrom_datetime+@"'
	            AND OwnerApprovedAt <= '"+ sTo_datetime + @"'
        ";
        DataTable table3 = Connect.GetTable(sql3);

        string sql4 = @"
            SELECT
	            ISNULL(SUM(ISNULL(EstimateValue, 0)), 0) AS SUMCOMISSION
            FROM CommissionByDay
            WHERE CommissionByDay.AccountId = '" + this.authId + @"'
	            AND CommissionByDay.EstimateDate >= '" + sFrom_date + @"'
	            AND CommissionByDay.EstimateDate <= '" + sTo_date + @"'
        ";
        DataTable table4 = Connect.GetTable(sql4);

        rs.Add("sBeforeDateTime", sFrom_datetime);
        rs.Add("sBeforeDate", sFrom_date);
        rs.Add("sAfterDateTime", sTo_datetime);
        rs.Add("sAfterDate", sTo_date);
        rs.Add("estimateReferral", table3.Rows[0]["TOTALROWS"].ToString());
        rs.Add("estimateCommissionReal", Utilities.Formatter.toCurrencyString(table4.Rows[0]["SUMCOMISSION"].ToString()));
        rs.Add("data", data);

        this.errorflag = false;

        return rs;
    }
}