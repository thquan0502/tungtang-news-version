﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangNhap : System.Web.UI.Page
{
    string Domain = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomainWebsite = "select * from tb_Domain";
        DataTable tbDomainWebsite = Connect.GetTable(sqlDomainWebsite);
        if (tbDomainWebsite.Rows.Count > 0)
        {
            Domain = "";
        }
        try
        {
            if (Request.QueryString["dk"].Trim().ToUpper() == "TC")
            {
                dvDKTC.InnerHtml = "<b>Đăng ký thành công, bạn có thể đăng nhập để đăng tin!</b>";
            }
        }
        catch { }
        if (Request.Cookies["TungTang_Login"] != null && Request.Cookies["TungTang_Login"].Value.Trim() != "")
        {
            Response.Redirect("../");
        }
    }

    protected void btDangKy_Click(object sender, EventArgs e)
    {
        string TenDangNhap = txtTenDangNhap.Value.Trim();
        string MatKhau = txtMatKhau.Value.Trim();
        if (TenDangNhap == "")
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng nhập số điện thoại !')", true);
            return;
        }
        if (MatKhau == "")
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng nhập mật khẩu !')", true);
            return;
        }

        if (MatKhau.Any(c => c > 255))
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Mật khẩu không được có dấu !')", true);
            return;
        }
        if (KiemTraKhongCo(TenDangNhap).Trim() == "Loi")
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng nhập chính xác số điện thoại !')", true);
            return;
        }
        string idThanhVien_DB = StaticData.getField("tb_ThanhVien", "idThanhVien", "TenDangNhap", TenDangNhap);
        if (idThanhVien_DB.Trim() != "")  
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Số điện thoại này đã được sử dụng.')", true);
            return;
        }
        object idThanhVien = Connect.FirstResulfExec("insert into tb_ThanhVien(MaThanhVien,TenDangNhap,MatKhau,NgayDangKy,SoDienThoai) values(N'TV" + TenDangNhap + "',N'" + TenDangNhap + "',N'" + MatKhau + "','" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',N'" + TenDangNhap + "')  select SCOPE_IDENTITY()");
        if (idThanhVien != null)
        {
            HttpCookie cookie_TungTang_Login = new HttpCookie("TungTang_Login", idThanhVien.ToString());
            cookie_TungTang_Login.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie_TungTang_Login);
            Response.Redirect("/");
        }
        else
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Đăng ký không thành công !')", true);
    }
    string KiemTraKhongCo(string Number)
    {
        try
        {
            Number.Trim();
        }
        catch
        {
            Number = "";
        }
        try
        {
            decimal a = decimal.Parse(Number);
        }
        catch
        {
            Number = "Loi";
        }
        if (Number == "")
        {
            Number = "Loi";
        }
        else if (Number == "-")
        {
            Number = "Loi";
        }
        else//bỏ dấu chấm 100.000  => 100000
        {
            try
            {
                Number = Number.Replace(",", "").Trim();
            }
            catch { }
        }
        return Number;
    }
}