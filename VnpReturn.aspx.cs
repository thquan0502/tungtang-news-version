﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

public partial class VnpReturn : System.Web.UI.Page
{
    protected string announcement = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.Count > 0)
        {
            string vnpHashSecret = ConfigurationManager.AppSettings["vnp_HashSecret"];
            var vnpayData = Request.QueryString;
            AppVnPay.VnPayLibrary vnpay = new AppVnPay.VnPayLibrary();
            foreach (string s in vnpayData)
            {
                if (!string.IsNullOrEmpty(s) && s.StartsWith("vnp_"))
                {
                    vnpay.AddResponseData(s, vnpayData[s]);
                }
            }

            String vnpSecureHash = Request.QueryString["vnp_SecureHash"];
            bool checkSignature = vnpay.ValidateSignature(vnpSecureHash, vnpHashSecret);
            if (checkSignature)
            {
                string vnpTxnref = vnpay.GetResponseData("vnp_TxnRef");
                DataRow data = AppVnPay.VnPayService.findByTxnref(vnpTxnref);
                if (data == null)
                {
                    Utilities.Redirect.notFound();
                }
                if(data["AccountId"].ToString() != Utilities.Auth.getAccountId())
                {
                    Utilities.Redirect.notFound();
                }

                string vnpResponseCode = vnpay.GetResponseData("vnp_ResponseCode");
                string vnpTransactionStatus = vnpay.GetResponseData("vnp_TransactionStatus");
                string status = "CREATED";
                string vnpMessage = vnpay.GetResponseData("vnp_Message");
                string vnpBankCode = vnpay.GetResponseData("vnp_BankCode");
                string vnpPayDateString = vnpay.GetResponseData("vnp_PayDate");
                DateTime vnpPayDate = Utilities.Converter.toDateTime(vnpPayDateString, "yyyyMMddHHmmss");
                string vnpTransactionNo = vnpay.GetResponseData("vnp_TransactionNo");
                string vnpTransactionType = vnpay.GetResponseData("vnp_TransactionType");

                if (vnpResponseCode == "00" && vnpTransactionStatus == "00")
                {
                    status = "SUCCESS";
                }
                else
                {
                    status = "ERROR";
                }
                this.announcement = "TRANSACTION " + status;

                string transactionId = data["Id"].ToString();

                string sql = @"
                    UPDATE Transactions SET
                        VnpMessage = '" + vnpMessage + @"',
                        VnpBankCode = '" + vnpBankCode + @"',
                        VnpPayDate = '" + vnpPayDate.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                        VnpTransactionNo = '" + vnpTransactionNo + @"',
                        VnpTransactionType = '" + vnpTransactionType + @"',
                        Status = '" + status + @"',
                        VnpResponseCode = '" + vnpResponseCode + @"',
                        VnpTransactionStatus = '" + vnpTransactionStatus + @"',
                        UpdatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                        ReturnAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                        RawResponse = '" + Request.RawUrl + @"'
                    WHERE Id = " + transactionId + @"
                ";
                Connect.Exec(sql);

                if(status == "SUCCESS")
                {
                    long amount = (long) data["VnpAmount"];
                    long actualAmount = (long) data["ActualAmount"];
                    long idHistory = recharge(data["AccountId"].ToString(), amount, actualAmount, transactionId);

                    HttpCookie cookieData = new HttpCookie("recharge_success");
                    cookieData.Value = "" +idHistory;
                    cookieData.Expires = DateTime.Now.AddSeconds(10);

                    Response.Cookies.Add(cookieData);
                    Response.Redirect("/tai-khoan/lich-su-giao-dich");
                }
                if (status == "ERROR")
                {
                    Response.Redirect("/TransactionError.aspx");
                }
            }
        }
        Utilities.Redirect.notFound();
    }

    protected long recharge(string accountId, long amount, long actualAmount, string transactionId)
    {
        long id = Services.BalanceHistory.createRecharge(accountId, Utilities.BalanceHistory.RECHARGE_METHOD_VNPAY, amount, actualAmount, Utilities.BalanceHistory.RECHARGE_STATUS_CREATED, transactionId);
        Services.Account.rechargeBalance(accountId, actualAmount);
        return id;
    }
}