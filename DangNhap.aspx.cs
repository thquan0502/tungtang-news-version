﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangNhap : System.Web.UI.Page
{
    string Domain = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomainWebsite = "select * from tb_Domain";
        DataTable tbDomainWebsite = Connect.GetTable(sqlDomainWebsite);
        if (tbDomainWebsite.Rows.Count > 0)
        {
            Domain = "";
        }
        try
        {
            if (Request.QueryString["dk"].Trim().ToUpper() == "TC")
            {
                dvDKTC.InnerHtml = "<b>Đăng ký thành công, bạn có thể đăng nhập để đăng tin!</b>";
            }
        }
        catch { }
        if (Request.Cookies["TungTang_Login"] != null && Request.Cookies["TungTang_Login"].Value.Trim() != "")
        {
            Response.Redirect("../");
        }

    }
    protected void btDangNhap_Click(object sender, EventArgs e)
    {
        string TenDangNhap = txtTenDangNhap.Value.Trim();
        string MatKhau = txtMatKhau.Value.Trim();
        if (TenDangNhap != "" && MatKhau != "")
        {
            DataTable tbCheckDangNhap = Connect.GetTable("select top 1 idThanhVien from tb_ThanhVien where TenDangNhap='" + TenDangNhap + "' and MatKhau='" + MatKhau + "' and isnull(isKhoa,'False')!='True'");
            if (tbCheckDangNhap.Rows.Count > 0)
            {
                HttpCookie cookie_TungTang_Login = new HttpCookie("TungTang_Login", tbCheckDangNhap.Rows[0]["idThanhVien"].ToString());
                cookie_TungTang_Login.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_TungTang_Login); 
                Response.Redirect( "../");
            }
            else
            { 
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Tài khoản không hợp lệ!')", true);
                return;
            }
        }
        else
        { 
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn chưa nhập số điện thoại hoặc mật khẩu!')", true);
            return;
        }
    }
}