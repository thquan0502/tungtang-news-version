﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangTin : System.Web.UI.Page
{
    protected string idTinDang = "";
    string Domain = "";
    string idThanhVien = "";
    protected Models.TinDang tinDangHelper = new Models.TinDang();
    protected Models.ThanhVien accountHelper = new Models.ThanhVien();

    protected bool isErrorCommission = false;

    protected DataTable postFees = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["TungTang_Login"] == null || Request.Cookies["TungTang_Login"].Value.Trim() == "")
        {
            Response.Redirect("/dang-nhap/dn");
        }
        else
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
            dvThongBao.Visible = false;
        }
        try
        {
            idTinDang = Request.QueryString["TDTD"].Trim();
        }
        catch { } 
        if (!IsPostBack)
        { 
            this.postFees = Services.PostFees.getAll();
            LoadTinDang();
            string sqlpro = "select StsPro,idAdmin_Duyet,LyDo_KhongDuyet from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
            DataTable tbPro = Connect.GetTable(sqlpro);
            if (tbPro.Rows.Count > 0)
            {
                if (tbPro.Rows[0]["StsPro"].ToString() == "1")
                {
                    divAlert.Visible = false;
                    divAlert.Style.Add("display", "none");
                }
                else if (tbPro.Rows[0]["idAdmin_Duyet"].ToString() == "-1")
                {
                    divAlert.Style.Add("background", "#d4edda");
                    divAlert.InnerHtml = "<p><i class='fa fa-lightbulb-o' style='font-size: 150%;'></i><i>Bạn đang trong thời gian chờ duyệt <strong> Thành viên Pro </strong>.Tạm thời áp dụng <strong> Hoa hồng </strong> cho <strong> Tin đăng </strong> chưa có hiệu lực.<strong>Cảm ơn bạn!</strong></i></p>";
                }
                else if (tbPro.Rows[0]["LyDo_KhongDuyet"].ToString() != "")
                {
                    divAlert.Style.Add("background","#f8d7da");
                    divAlert.InnerHtml = "<p><i class='fa fa-lightbulb-o' style='font-size: 150%;'></i><i>Đăng ký <strong>Thành Viên Pro </strong> của bạn bị từ chối.Vui lòng kiểm tra và <a href = '/thong-tin/pro' target= '_blank'>Đăng ký lại</a></i>.<br/><strong>Xin cảm ơn!</strong></p>";
                  
                }
                else
                divAlert.InnerHtml = "<p><i class='fa fa-lightbulb-o' style='font-size: 150%;'></i><i>Muốn đặt tiền<strong> Hoa hồng</strong> cho<strong>Tin đăng</strong>, bạn phải là<strong> Thành Viên Pro</strong> của<strong> Tung Tăng</strong>? <a href = '/thong-tin/pro' target= '_blank'>Đăng ký ngay</a></i></p>";
                   
            }
           
        }
       if(IsPostBack)
        {
            if (inputUnCommission.Checked)
            {
                dvContainerReferral.Visible = false;
                divAlert.Visible = false;
            }
            else
            {
                dvContainerReferral.Visible = true;
                divAlert.Visible = true;
            }
        }
        if (IsPostBack && fileHinhAnh.PostedFile != null)
        {


            if (Request.Files.Count > 10)
            {
                Response.Write("<script>alert('Bạn vui lòng chọn tối đa 10 ảnh!')</script>");
                return;
            }

            List<string> filenames = new List<string>();

            for (int j = 0; j < Request.Files.Count; j++)
            {
                HttpPostedFile file = Request.Files[j];
                if (file.ContentLength > 0)
                {
                    //if (fileHinhAnh.PostedFile.FileName.Length > 0)
                    //{
                    string extension = Path.GetExtension(file.FileName);
                    if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG" || extension.ToUpper() == ".WEBP")
                    {
                        if (fileHinhAnh.HasFile)
                        {
                            int ketquatestthu = 1;

                            byte[] imageData = new byte[file.ContentLength];
                            file.InputStream.Read(imageData, 0, file.ContentLength);

                            MemoryStream ms = new MemoryStream(imageData);
                            System.Drawing.Image originalImage = System.Drawing.Image.FromStream(ms);

                            if (originalImage.PropertyIdList.Contains(0x0112))
                            {
                                int rotationValue = originalImage.GetPropertyItem(0x0112).Value[0];



                                switch (rotationValue)
                                {
                                    case 1: // landscape, do nothing
                                        ketquatestthu = 1;
                                        break;

                                    case 8: // rotated 90 right
                                        // de-rotate:
                                        // originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate270FlipNone);
                                        ketquatestthu = 8;
                                        break;

                                    case 3: // bottoms up
                                        //originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate180FlipNone);

                                        ketquatestthu = 3;
                                        break;

                                    case 6: // rotated 90 left
                                        //  originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate90FlipNone);
                                        ketquatestthu = 6;
                                        break;
                                }
                            }

                            string Ngay = DateTime.Now.Day.ToString();
                            string Thang = DateTime.Now.Month.ToString();
                            string Nam = DateTime.Now.Year.ToString();
                            string Gio = DateTime.Now.Hour.ToString();
                            string Phut = DateTime.Now.Minute.ToString();
                            string Giay = DateTime.Now.Second.ToString();
                            string Khac = DateTime.Now.Ticks.ToString();
                            string fExtension = Path.GetExtension(file.FileName);

                            string sqlIdTinDang = "select top 1 idTinDang from tb_TinDang order by idTinDang desc";
                            DataTable tbIdTinDang = Connect.GetTable(sqlIdTinDang);
                            string idTinDang = "0";
                            if (tbIdTinDang.Rows.Count > 0)
                                idTinDang = (float.Parse(tbIdTinDang.Rows[0]["idTinDang"].ToString()) + 1).ToString();
                            string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + idTinDang + ".webp";
                            string FilePath = "~/Images/td/slides/" + FileName;
                            try
                            {
                                if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("iPhone")))
                                    StaticData.LoadWaterMark(file, Server.MapPath(FilePath), ketquatestthu.ToString());
                                else
                                    StaticData.LoadWaterMark(file, Server.MapPath(FilePath), ketquatestthu.ToString());
                            }
                            catch
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Hình này bị lỗi. Vui lòng chọn hình khác!')", false);
                                return;
                            }

                            //file.SaveAs(Server.MapPath(FilePath));

                            filenames.Add(FileName);
                            // hdHinhAnh.Value = hdHinhAnh.Value + FileName + "|~~~~|";
                            hdHinhAnh.Value = hdHinhAnh.Value + FileName + "|~~~~|";
                            string htmlHinhAnh = "";
                            string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                            for (int i = 0; i < arrHinhAnh.Length; i++)
                            {
                                if (arrHinhAnh[i].Trim() != "")
                                {
                                    htmlHinhAnh += "<div id='dvHinhAnh_" + arrHinhAnh[i].Trim() + "' class='imgupload' style='width:auto;float:left;margin-right:10px'>";
                                    htmlHinhAnh += "<p style='margin:0px;text-align:center;'><img src='../Images/td/slides/" + arrHinhAnh[i].Trim() + "' style='width: 150px;height: 110px;object-fit:cover;' /></p>";
                                    htmlHinhAnh += "<p style='text-align:center;margin: 0px 6px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + arrHinhAnh[i].Trim() + "\",\"" + arrHinhAnh[i].Trim() + "\")' src='../images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                                    htmlHinhAnh += "</div>";
                                }
                            }
                            dvHinhAnh.InnerHtml = htmlHinhAnh;

                            //imgAnhCuaBan.Src = FilePath;
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                        return;
                    }
                }
            }

            ////hdHinhAnh.Value = hdHinhAnh.Value + FileName + "|~~~~|";
            //string htmlHinhAnh = "";
            ////string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
            //for (int i = 0; i < filenames.Count; i++)
            //{
            //    if (filenames[i].Trim() != "")
            //    {
            //        htmlHinhAnh += "<div id='dvHinhAnh_" + filenames[i].Trim() + "' class='imgupload' style='width:25%;float:left;'>";
            //        htmlHinhAnh += "<p style='margin:0px;text-align:center;'><img class='uploaded-img' data-name='"+ filenames[i].Trim()+"' src='../Images/td/slides/" + filenames[i].Trim() + "' style='width: 150px;height: 110px;object-fit:cover;' /></p>";
            //        htmlHinhAnh += "<p style='text-align:center;margin: 0px 6px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + filenames[i].Trim() + "\",\"" + filenames[i].Trim() + "\")' src='../images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
            //        htmlHinhAnh += "</div>";
            //    }
            //}
            //dvHinhAnh.InnerHtml = htmlHinhAnh;


            //  Response.Write("<script>window.scrollTo(0, document.body.scrollHeight);</script>");
        }
        checkSoDT();
    }

    public void checkSoDT()//Code new
    {
        string value = "";
        //string Email = "Select top 1 Email from tb_ThanhVien where idThanhVien = '" + idThanhVien + "' and isnull(isKhoa,'False')!= 'True'";
        //try
        //{
        //    DataTable tb = Connect.GetTable(Email);
        //    if (tb.Rows.Count > 0)
        //        value = tb.Rows[0][0].ToString();
        //}
        //catch
        //{ }
        string result = "";
        string sql = "Select top 1 SoDienThoai from tb_ThanhVien where idThanhVien='" + idThanhVien + "' and isnull(isKhoa,'False')!='True'";
        try
        {
            DataTable tb = Connect.GetTable(sql);
            if (tb.Rows.Count > 0)
                result = tb.Rows[0][0].ToString();
        }
        catch
        { }
        if (result != "")
        {
            //Response.Redirect("/dang-tin/dt");         
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn tc!')", true);
            string a = "";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn cần cập nhật thông tin số điện thoại!')", true);
            Response.Redirect("/thong-tin/tt");
        }
    }

    private void LoadTinDang()
    {
        if (idTinDang != "")
        {
           // List<string> filenames = new List<string>();

            btDangTinFake.Text = "SỬA TIN";
            string sqlTinDang = "select * from tb_TinDang where isnull(isHetHan,'False')='False' and idTinDang='" + idTinDang + "'";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            if (tbTinDang.Rows.Count > 0)
            {
                slLoaiDanhMuc.Value = tbTinDang.Rows[0]["idDanhMucCap1"].ToString();
                if (tbTinDang.Rows[0]["idDanhMucCap1"].ToString() != "")
                    slLoaiDanhMuc.Value = tbTinDang.Rows[0]["idDanhMucCap1"].ToString();

                if (tbTinDang.Rows[0]["idDanhMucCap2"].ToString() != "")
                    slLoaiDanhMucCap2.Value = tbTinDang.Rows[0]["idDanhMucCap2"].ToString();
                string chuoi_DanhMuc = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", tbTinDang.Rows[0]["idDanhMucCap1"].ToString());
                if (tbTinDang.Rows[0]["idDanhMucCap2"].ToString() != "")
                    chuoi_DanhMuc += "/" + StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", tbTinDang.Rows[0]["idDanhMucCap2"].ToString());
                txtLoaiDanhMuc.Value = chuoi_DanhMuc;
                txtTinhThanh.Value = tbTinDang.Rows[0]["idTinh"].ToString();
                txtQuanHuyen.Value = tbTinDang.Rows[0]["idHuyen"].ToString();
                txtPhuongXa.Value = tbTinDang.Rows[0]["idPhuongXa"].ToString();
                string TenTinhThanh = StaticData.getField("City", "ten", "id", tbTinDang.Rows[0]["idTinh"].ToString());
                string TenQuanHuyen = StaticData.getField("District", "ten", "id", tbTinDang.Rows[0]["idHuyen"].ToString());
                string TenPhuongXa = StaticData.getField("tb_PhuongXa", "ten", "id", tbTinDang.Rows[0]["idPhuongXa"].ToString());

                string chuoi_DiaDiem = "";
                if (TenPhuongXa != "")
                    chuoi_DiaDiem = TenPhuongXa + ", ";
                if (TenQuanHuyen != "")
                    chuoi_DiaDiem += TenQuanHuyen + ", ";
                if (TenTinhThanh != "")
                    chuoi_DiaDiem += TenTinhThanh;

                if (TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                    chuoi_DiaDiem = "Toàn quốc";
                txtDiaDiem.Value = chuoi_DiaDiem;

                if (tbTinDang.Rows[0]["TuGia"].ToString() != "")
                    txtTuGia.Value = double.Parse(KiemTraKhongNhap_LoadLen(tbTinDang.Rows[0]["TuGia"].ToString())).ToString("#,##");

                if (tbTinDang.Rows[0]["isGiaoHangMatPhi"].ToString() == "True")
                    rdGiaoHangMatPhi.Checked = true;
                else
                    rdGiaoHangKhongMatPhi.Checked = true;

                //if(tbTinDang.Rows[0]["Commission"].ToString() == "0")
                //{
                //   // inputHasCommission.Checked = false;
                //    inputUnCommission.Checked = true;
                //}
                //else
                //    inputHasCommission.Checked = true;
                //  //  inputUnCommission.Checked = false;

                txtHoTen.Value = tbTinDang.Rows[0]["HoTen"].ToString();
                txtSoDienThoai.Value = tbTinDang.Rows[0]["SoDienThoai"].ToString();
                txtEmail.Value = tbTinDang.Rows[0]["Email"].ToString();
                txtDiaChi.Value = tbTinDang.Rows[0]["DiaChi"].ToString();
                txtTieuDe.Value = tbTinDang.Rows[0]["TieuDe"].ToString();
                txtNoiDung.Value = tbTinDang.Rows[0]["NoiDung"].ToString();
                if (tbTinDang.Rows[0]["LoaiTinDang"].ToString().Trim() == "CanBan")
                    radCanBan.Checked = true;
                else
                    radCanMua.Checked = true;

                string sqlHinhAnh = "select * from tb_HinhAnh where idTinDang='" + idTinDang + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string sHinhAnh = "";
                for (int i = 0; i < tbHinhAnh.Rows.Count; i++)
                {
                    sHinhAnh += tbHinhAnh.Rows[i]["UrlHinhAnh"].ToString().Trim() + "|~~~~|";
                }
                //      hdHinhAnh.Value = sHinhAnh;
                //    filenames.Add(sHinhAnh);
                hdHinhAnh.Value = sHinhAnh;
                string htmlHinhAnh = "";
                string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                for (int i = 0; i < arrHinhAnh.Length; i++)
                {
                    if (arrHinhAnh[i].Trim() != "")
                    {
                        htmlHinhAnh += "<div id='dvHinhAnh_" + arrHinhAnh[i].Trim() + "' class='imgupload' style='width:25%;float:left;'>";
                        htmlHinhAnh += "<p style='margin:0px;text-align:center;'><img src='../Images/td/slides/" + arrHinhAnh[i].Trim() + "' style='width: 150px;height: 110px;object-fit:cover;' /></p>";
                        htmlHinhAnh += "<p style='text-align:center;margin: 0px 6px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + arrHinhAnh[i].Trim() + "\",\"" + arrHinhAnh[i].Trim() + "\")' src='../images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                        htmlHinhAnh += "</div>";
                    }
                }
                dvHinhAnh.InnerHtml = htmlHinhAnh;
                txtReferralRate.Text = tbTinDang.Rows[0]["ReferralRate"].ToString();
                txtReferralAmount.Text = tbTinDang.Rows[0]["ReferralAmount"].ToString();
            }
        }
    }
    string KiemTraKhongNhap(string Number)
    {
        try
        {
            Number.Trim();
        }
        catch
        {
            Number = "";
        }
        try
        {
            decimal a = decimal.Parse(Number);
        }
        catch
        {
            Number = "0";
        }
        if (Number == "")
        {
            Number = "0";
        }
        else if (Number == "-")
        {
            Number = "0";
        }
        else//bỏ dấu chấm 100.000  => 100000
        {
            try
            {
                Number = Number.Replace(",", "").Trim();
            }
            catch { }
        }
        return Number;
    }

    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    protected void checkFee ()
    {
        object[] rs = this.calcFeeData();
        bool isSuccess = (bool)rs[0];
        if (!isSuccess)
        {
            Utilities.Redirect.balanceNotEnough(Response);
        }
        string code = (string)rs[1];
        long total = (long)rs[2];
        DateTime startDate = (DateTime)rs[3];
        DateTime endDate = (DateTime)rs[4];
    }

    /**
     * object[]:    
     *      bool: is success
     *      string: code
     *      long: total fee
     *      DateTime: start at
     *      DateTime: end at
     */
    protected object[] calcFeeData()
    {
        object[] rs = new object[5];
        rs[0] = false;
        rs[1] = "default_pack";
        rs[2] = 0;
        rs[3] = DateTime.Now;
        rs[4] = DateTime.Now;

        string packageCode = txtPostPackage.Text.Trim();
        string periodValue = txtPostPeriod.Text.Trim();
        string daysValue = txtPostDays.Text.Trim();

        DataRow pack = Services.PostFees.findByCode(packageCode);

        if(pack == null)
        {
            rs[0] = false;
            return rs;
        }
        rs[1] = pack.Field<string>("Code");

        long perPrice = 0;
        long days = 0;
        switch(periodValue)
        {
            case "pricePer1Days":
                perPrice = pack.Field<long>("PricePer1Days");
                days = 1;
                break;
            case "pricePer7Days":
                perPrice = pack.Field<long>("PricePer7Days");
                days = 7;
                break;
            case "pricePer14Days":
                perPrice = pack.Field<long>("PricePer14Days");
                days = 14;
                break;
            case "pricePer30Days":
                perPrice = pack.Field<long>("PricePer30Days");
                days = 30;
                break;
            case "pricePerOption":
                perPrice = pack.Field<long>("PricePer1Days");
                days = long.Parse(daysValue);
                break;
            default:
                rs[0] = false;
                return rs;
        }
        rs[2] = perPrice * days;

        long balance = Utilities.Auth.getBalance();
        if(balance < (long)rs[2])
        {
            rs[0] = false;
        }
        else
        {
            rs[0] = true;
        }
        DateTime startDate = DateTime.Now;
        DateTime endDate = startDate.AddDays(days);

        rs[3] = startDate;
        rs[4] = endDate;

        return rs;
    }
    protected void btDangTinFake_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;

        }
        string idDanhMucCap1 = "";
        string idDanhMucCap2 = "";
        string idTinh = "";
        string idHuyen = "";
        string idPhuong = "";
        string TuGia = "";
        string DenGia = "";
        string isGiaoHangMatPhi = "True";
        string SoLuongTheoLoaiGia = "1";
        string MaLoaiGia = "";
        string HoTen = "";
        string SoDienThoai = "";
        string Email = "";
        string DiaChi = "";
        string TieuDe = "";
        string NoiDung = "";
        string LoaiTinDang = "CanMua";
        int referralRateValue = 0;
        double referralAmountValue = 0;
        string slug = "";
        divErrorGia.InnerHtml = "";

         //    bool hasError = false;

        string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);

        //Danh mục cấp 1
        if (slLoaiDanhMuc.Value.Trim() != "" && slLoaiDanhMuc.Value.Trim() != "0")
        {
            idDanhMucCap1 = slLoaiDanhMuc.Value.Trim();
            dvLinhVuc.InnerHtml = "";
           // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Là thằng này!')", true);
        }
        else
        {
            dvLinhVuc.InnerHtml = "Bạn chưa chọn danh mục!";
             return;
          //  hasError = true;
        }
        if (radCanBan.Checked)
            LoaiTinDang = "CanBan";

        if (slLoaiDanhMucCap2.Value.Trim() != "")
            idDanhMucCap2 = slLoaiDanhMucCap2.Value.Trim();
        //Tỉnh
        idTinh = txtTinhThanh.Value.Trim();
        //Huyện
        idHuyen = txtQuanHuyen.Value.Trim();
        //Phường
        idPhuong = txtPhuongXa.Value.Trim();
        //Từ giá
        TuGia = KiemTraKhongNhap(txtTuGia.Value.Trim());
        double price = 0;
        try
        {
            price = Double.Parse(TuGia);
        }
        catch (Exception ex)
        {
            price = 0;
        }
        //Đến giá
        //DenGia = txtDenGia.Value.Trim().Replace(",", "").Replace(".", "");
        //Phí giao hàng
        isGiaoHangMatPhi = rdGiaoHangMatPhi.Checked.ToString();
        //Email
        Email = txtEmail.Value.Trim();
        //Địa chỉ
        DiaChi = txtDiaChi.Value.Trim();
        //Tiêu đề


        //Gía

        string Gia = txtTuGia.Value.Trim();
        if (Gia.Trim() == "")
        {
            //DienTich = "NULL";
            divErrorGia.InnerHtml = "Giá phải là số lớn hơn 0 và không được bỏ trống";
            return;
            
          //  hasError = true;
        }
        if (Gia == "NULL")
        {
             divErrorGia.InnerHtml = "Giá phải là số lớn hơn 0 và không được bỏ trống";
            return;
        }
        else
        {
            float n;
            bool rs = float.TryParse(Gia.Replace(",", ""), out n);
            if (!rs)
            {
                divErrorGia.InnerHtml = "Giá phải là số";
                return;
            }
            if(n == 0)
            {
                divErrorGia.InnerHtml = "Giá phải là số lớn hơn 0 và không được bỏ trống";
                return;
            }
            if (idDanhMucCap1 == "37")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thủy - Hải sản thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "38")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thịt - Gia cầm thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "39")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Đồ khô thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "40")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Rau - Củ - Quả thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "41")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thức uống thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "42")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thực phẩm chế biến thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "43")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Gia vị và phụ liệu sản thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "44")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thời Trang sản thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "32")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Các loại khác thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "45" && LoaiTinDang == "CanBan")
            {
                if (n < 1000000)
                {
                    divErrorGia.InnerHtml = "Giá bán xe phải lớn hơn 1 triệu đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "45" && LoaiTinDang == "CanMua")
            {
                if (n < 1000000)
                {
                    divErrorGia.InnerHtml = "Giá mua xe phải lớn hơn 1 triệu đồng";
                    return;
                }
            }

        }  

        // referral rate
        try
        {
            referralRateValue = Int32.Parse(txtReferralRate.Text.Trim().Replace(" ", "").Replace(",", ""));
            if (referralRateValue < 0)
            {
                referralRateValue = 0;
            }
        }
        catch (Exception ex)
        {
            referralRateValue = 0;
        }


        // referral amount
        try
        {
            referralAmountValue = Double.Parse(txtReferralAmount.Text.Trim().Replace(" ", "").Replace(",", ""));
            if (referralAmountValue < 0)
            {
                referralAmountValue = 0;
            }
        }
        catch (Exception ex)
        {
            referralAmountValue = 0;
        }

        if (referralRateValue > 0 && referralAmountValue > 0)
        {
            referralRateValue = 0;
        }
        string sqlpro2 = "select StsPro from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
        DataTable tbPro2 = Connect.GetTable(sqlpro2);
        if (tbPro2.Rows.Count > 0)
        {
            if (tbPro2.Rows[0]["StsPro"].ToString() == "1")
            {
                if (!inputHasCommission.Checked && !inputUnCommission.Checked)
                {
                    divMessCommission.InnerHtml = "Bạn phải chọn 1 trong 2 cách hình thức cho hoa hồng!";
                    divErrorCommission.InnerHtml = "";
                    return;
                }
                else if (inputHasCommission.Checked && referralRateValue == 0 && referralAmountValue == 0)
                {
                   
                     divMessCommission.InnerHtml = "";
                     divErrorCommission.InnerText = "Bạn chưa nhập hoa hồng!";
                     return;
                }
                else
                {
                    divErrorCommission.InnerHtml = "";
                    divMessCommission.InnerHtml = "";
                }
            }
            else
            {
                referralRateValue = 0;
                referralAmountValue = 0;
                if (inputHasCommission.Checked)
                {
                  
                    divErrorCommission.InnerHtml = "Bạn phải là thành viên Pro mới được áp dụng hoa hồng!";
                    return;
                }    
                else
                {
                    divErrorCommission.InnerHtml = "";
                    divMessCommission.InnerHtml = "";
                }
                    
            }
        }

        double commission = tinDangHelper.calcCommission(price, referralRateValue, referralAmountValue);
        if (txtTieuDe.Value.Trim() != "")
        {
            TieuDe = txtTieuDe.Value.Trim();
        }
        else
        {
            dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề cho bài viết!";
            return;
        }

        if (txtNoiDung.Value.Trim() != "")
        {
            NoiDung = txtNoiDung.Value.Trim();
        }
        else
        {
            dvNoiDung.InnerHtml = "Bạn chưa nhập nội dung cho bài viết!";
            return;
        }

        if (arrHinhAnh.Length < 3)
        {
            dvMessageHinhAnh.InnerHtml = "Bài đăng phải có 3 hình trở lên!";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bài đăng phải có 3 hình trở lên!')", true);
            return;
        }
        else
        {
            dvMessageHinhAnh.InnerHtml = "";
        }

        slug = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TieuDe.Trim()));
        do
        {
            string sql4 = @"
                select DuongDan from tb_TinDang
                where DuongDan = '" + slug + @"' and (isHetHan = '0' or isHetHan is null ) 
            ";
            if(idTinDang != "")
            {
                sql4 += " AND idTinDang != '" + idTinDang + "' ";
            }
            DataTable tb4 = Connect.GetTable(sql4);
            if(tb4.Rows.Count == 0)
            {
                break;
            }

            slug += "s";
        } while (true);

        if (idTinDang == "")
        {
            checkFee();
            object[] rs = this.calcFeeData();
            bool isSuccess = (bool)rs[0];
            string code = (string)rs[1];
            long total = (long)rs[2];
            DateTime startDate = (DateTime)rs[3];
            DateTime endDate = (DateTime)rs[4];
            spentMoney(total, code);
            //Insert tin đăng
            string sqlInsertTD = "insert into tb_TinDang(idDanhMucCap1,idDanhMucCap2,idTinh,idHuyen,idPhuongXa,TuGia,DenGia,isGiaoHangMatPhi,SoLuongTheoLoaiGia,MaLoaiGia,HoTen,SoDienThoai,Email,DiaChi,TieuDe,NoiDung,idThanhVien,NgayDang,NgayDayLenTop,LoaiTinDang,NgayGuiDuyet, ReferralRate, ReferralAmount, Commission, DuongDan, FeeCode, FeeStartTime, FeeEndTime, FeeTotal)";
            sqlInsertTD += " values(";
            if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
                sqlInsertTD += "'" + idDanhMucCap1 + "'";
            else
                sqlInsertTD += "null";
            if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
                sqlInsertTD += ",'" + idDanhMucCap2 + "'";
            else
                sqlInsertTD += ",null";
            if (idTinh != "" && idTinh != "0")
                sqlInsertTD += ",'" + idTinh + "'";
            else
                sqlInsertTD += ",null";
            if (idHuyen != "" && idHuyen != "0")
                sqlInsertTD += ",'" + idHuyen + "'";
            else
                sqlInsertTD += ",null";
            if (idPhuong != "" && idPhuong != "0")
                sqlInsertTD += ",'" + idPhuong + "'";
            else
                sqlInsertTD += ",null";
            if (TuGia != "")
                sqlInsertTD += ",'" + TuGia + "'";
            else
                sqlInsertTD += ",null";
            if (DenGia != "")
                sqlInsertTD += ",'" + DenGia + "'";
            else
                sqlInsertTD += ",null";
            sqlInsertTD += ",'" + isGiaoHangMatPhi + "'";
            if (SoLuongTheoLoaiGia != "")
                sqlInsertTD += ",'" + SoLuongTheoLoaiGia + "'";
            else
                sqlInsertTD += ",null";
            if (MaLoaiGia != "")
                sqlInsertTD += ",'" + MaLoaiGia + "'";
            else
                sqlInsertTD += ",null";
            sqlInsertTD += ",N'" + HoTen + "','" + SoDienThoai + "','" + Email + "',N'" + DiaChi + "',N'" + TieuDe + "',@noidung";
            if (idThanhVien != "")
                sqlInsertTD += ",'" + idThanhVien + "'";
            else
                sqlInsertTD += ",null";
            sqlInsertTD += ",'" + DateTime.Now.ToString() + "','" + DateTime.Now.ToString() + "','" + LoaiTinDang + "','" + DateTime.Now.ToString() + "', " + referralRateValue + ", " + referralAmountValue + " , " + commission + ", '" + slug + "', '"+code+"', '"+ startDate.ToString() + "', '"+ endDate.ToString() + "', "+ total + "   )";
            string[] paramsName = new string[1] { "@noidung" };
            string[] paramsValue = new string[1] { NoiDung };

            if (Connect.Exec(sqlInsertTD, paramsName, paramsValue))
            {
                string idTinDangMoi = "";
                string sqlTinDangMoi = "select top 1 idTinDang from tb_TinDang where '1'='1'";
                if (idThanhVien != "")
                    sqlTinDangMoi += " and idThanhVien='" + idThanhVien + "'";
                sqlTinDangMoi += " order by idTinDang desc";
                DataTable tbTinDangMoi = Connect.GetTable(sqlTinDangMoi);
                if (tbTinDangMoi.Rows.Count > 0)
                    idTinDangMoi = tbTinDangMoi.Rows[0]["idTinDang"].ToString();
                for (int i = 0; i < arrHinhAnh.Length; i++)
                {
                    if (arrHinhAnh[i].Trim() != "")
                    {
                        string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTinDang,UrlHinhAnh) values('" + idTinDangMoi + "','" + arrHinhAnh[i].Trim().Replace("~/", "") + "')";
                        Connect.Exec(sqlInsertHinhAnh);
                    }
                }
                Connect.Exec("update tb_TinDang set MaTinDang='RV" + DateTime.Now.Year + idTinDangMoi + "' where idTinDang='" + idTinDangMoi + "' ");

                if (idThanhVien != "")
                    Response.Redirect("/thong-tin-ca-nhan/ttcn");
                else
                    Response.Redirect(Domain);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!')", true);
            }
        }
        else
        {
            //Sửa tin đăng
            string sqlUpdateTD = "update tb_TinDang set ";

            if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
                sqlUpdateTD += " idDanhMucCap1='" + idDanhMucCap1 + "'";
            else
                sqlUpdateTD += "idDanhMucCap1=null";
            if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
                sqlUpdateTD += ", idDanhMucCap2='" + idDanhMucCap2 + "'";
            else
                sqlUpdateTD += ",idDanhMucCap2=null";
            if (idTinh != "" && idTinh != "0")
                sqlUpdateTD += ",idTinh='" + idTinh + "'";
            else
                sqlUpdateTD += ",idTinh=null";
            if (idHuyen != "" && idHuyen != "0")
                sqlUpdateTD += ",idHuyen='" + idHuyen + "'";
            else
                sqlUpdateTD += ",idHuyen=null";
            if (idPhuong != "" && idPhuong != "0")
                sqlUpdateTD += ",idPhuongXa='" + idPhuong + "'";
            else
                sqlUpdateTD += ",idPhuongXa=null";
            if (TuGia != "")
                sqlUpdateTD += ",TuGia='" + TuGia + "'";
            else
                sqlUpdateTD += ",TuGia=null";
            if (DenGia != "")
                sqlUpdateTD += ",DenGia='" + DenGia + "'";
            else
                sqlUpdateTD += ",DenGia=null";
            sqlUpdateTD += ",isGiaoHangMatPhi='" + isGiaoHangMatPhi + "'";
            if (SoLuongTheoLoaiGia != "")
                sqlUpdateTD += ",SoLuongTheoLoaiGia='" + SoLuongTheoLoaiGia + "'";
            else
                sqlUpdateTD += ",SoLuongTheoLoaiGia=null";
            if (MaLoaiGia != "" && MaLoaiGia != "0")
                sqlUpdateTD += ",MaLoaiGia='" + MaLoaiGia + "'";
            else
                sqlUpdateTD += ",MaLoaiGia=null";
            sqlUpdateTD += ",HoTen=N'" + HoTen + "'";
            sqlUpdateTD += ",SoDienThoai='" + SoDienThoai + "'";
            sqlUpdateTD += ",Email='" + Email + "'";
            sqlUpdateTD += ",DiaChi=N'" + DiaChi + "'";
            sqlUpdateTD += ",TieuDe=N'" + TieuDe + "'";
            sqlUpdateTD += ",NoiDung=@noidung";
            sqlUpdateTD += ",LyDo_KhongDuyet=''";
            sqlUpdateTD += ",LoaiTinDang=N'" + LoaiTinDang + "'";
            sqlUpdateTD += ",isHetHan = 0, IsDraft = 0, isDuyet = null";
            sqlUpdateTD += ",NgayGuiDuyet='" + DateTime.Now.ToString() + "' ";
            sqlUpdateTD += ", ReferralRate=" + referralRateValue + " ";
            sqlUpdateTD += ", ReferralAmount=" + referralAmountValue + " ";
            sqlUpdateTD += ", Commission=" + commission + " ";
            sqlUpdateTD += ", DuongDan='" + slug + "' ";
            sqlUpdateTD += " where idTinDang='" + idTinDang + "'";

            string[] paramsName = new string[1] { "@noidung" };
            string[] paramsValue = new string[1] { NoiDung };

            if (Connect.Exec(sqlUpdateTD, paramsName, paramsValue))
            {
                string sqlDeleteHinhAnh = "delete from tb_HinhAnh where idTinDang='" + idTinDang + "'";
                bool ktDeleteHinhAnh = Connect.Exec(sqlDeleteHinhAnh);
                if (ktDeleteHinhAnh)
                {
                    for (int i = 0; i < arrHinhAnh.Length; i++)
                    {
                        if (arrHinhAnh[i].Trim() != "")
                        {
                            string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTinDang,UrlHinhAnh) values('" + idTinDang + "','" + arrHinhAnh[i].Trim() + "')";
                            Connect.Exec(sqlInsertHinhAnh);
                        }
                    }
                }

                Response.Redirect("/thong-tin-ca-nhan/ttcn");



            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!')", true);
            }
        }
    }
    //protected void CongChuoi(string a, string b)
    //{
    //    a = a + b;
    //    return a;

    //}
    protected void btnPreview_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;

        }
        string idDanhMucCap1 = "";
        string idDanhMucCap2 = "";
        string idTinh = "";
        string idHuyen = "";
        string idPhuong = "";
        string TuGia = "";
        string DenGia = "";
        string isGiaoHangMatPhi = "True";
        string SoLuongTheoLoaiGia = "1";
        string MaLoaiGia = "";
        string HoTen = "";
        string SoDienThoai = "";
        string Email = "";
        string DiaChi = "";
        string TieuDe = "";
        string NoiDung = "";
        string LoaiTinDang = "CanMua";
        int referralRateValue = 0;
        double referralAmountValue = 0;
        string slug = "";
        divErrorGia.InnerHtml = "";

        string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);

        //Danh mục cấp 1
        if (slLoaiDanhMuc.Value.Trim() != "" && slLoaiDanhMuc.Value.Trim() != "0")
        {
            idDanhMucCap1 = slLoaiDanhMuc.Value.Trim();
            dvLinhVuc.InnerHtml = "";
        }
        else
        {
            dvLinhVuc.InnerHtml = "Bạn chưa chọn danh mục!";
            return;
        }
        if (radCanBan.Checked)
            LoaiTinDang = "CanBan";

        if (slLoaiDanhMucCap2.Value.Trim() != "")
            idDanhMucCap2 = slLoaiDanhMucCap2.Value.Trim();
        //Tỉnh
        idTinh = txtTinhThanh.Value.Trim();
        //Huyện
        idHuyen = txtQuanHuyen.Value.Trim();
        //Phường
        idPhuong = txtPhuongXa.Value.Trim();
        //Từ giá
        TuGia = KiemTraKhongNhap(txtTuGia.Value.Trim());
        double price = 0;
        try
        {
            price = Double.Parse(TuGia);
        }
        catch (Exception ex)
        {
            price = 0;
        }
        //Đến giá
        //DenGia = txtDenGia.Value.Trim().Replace(",", "").Replace(".", "");
        //Phí giao hàng
        isGiaoHangMatPhi = rdGiaoHangMatPhi.Checked.ToString();
        //Email
        Email = txtEmail.Value.Trim();
        //Địa chỉ
        DiaChi = txtDiaChi.Value.Trim();
        //Tiêu đề


        //Gía

        string Gia = txtTuGia.Value.Trim();
        if (Gia.Trim() == "")
        {
            //DienTich = "NULL";
            divErrorGia.InnerHtml = "Giá phải là số lớn hơn 0 và không được bỏ trống";
            return;
        }
        if (Gia == "NULL")
        {
            divErrorGia.InnerHtml = "Giá phải là số lớn hơn 0 và không được bỏ trống";
            return;
        }
        else
        {
            float n;
            bool rs = float.TryParse(Gia.Replace(",", ""), out n);
            if (!rs)
            {
                divErrorGia.InnerHtml = "Giá phải là số";
                return;
            }
            if (n == 0)
            {
                divErrorGia.InnerHtml = "Giá phải là số lớn hơn 0 và không được bỏ trống";
                return;
            }
            if (idDanhMucCap1 == "37")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thủy - Hải sản thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "38")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thịt - Gia cầm thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "39")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Đồ khô thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "40")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Rau - Củ - Quả thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "41")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thức uống thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "42")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thực phẩm chế biến thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "43")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Gia vị và phụ liệu sản thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "44")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Thời Trang sản thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "32")
            {
                if (n < 1000)
                {
                    divErrorGia.InnerHtml = "Giá bán/mua của Các loại khác thấp nhất là 1000 đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "45" && LoaiTinDang == "CanBan")
            {
                if (n < 1000000)
                {
                    divErrorGia.InnerHtml = "Giá bán xe phải lớn hơn 1 triệu đồng";
                    return;
                }
            }
            if (idDanhMucCap1 == "45" && LoaiTinDang == "CanMua")
            {
                if (n < 1000000)
                {
                    divErrorGia.InnerHtml = "Giá mua xe phải lớn hơn 1 triệu đồng";
                    return;
                }
            }

        }

        // referral rate
        try
        {
            referralRateValue = Int32.Parse(txtReferralRate.Text.Trim().Replace(" ", "").Replace(",", ""));
            if (referralRateValue < 0)
            {
                referralRateValue = 0;
            }
        }
        catch (Exception ex)
        {
            referralRateValue = 0;
        }


        // referral amount
        try
        {
            referralAmountValue = Double.Parse(txtReferralAmount.Text.Trim().Replace(" ", "").Replace(",", ""));
            if (referralAmountValue < 0)
            {
                referralAmountValue = 0;
            }
        }
        catch (Exception ex)
        {
            referralAmountValue = 0;
        }

        if (referralRateValue > 0 && referralAmountValue > 0)
        {
            referralRateValue = 0;
        }
        string sqlpro2 = "select StsPro from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
        DataTable tbPro2 = Connect.GetTable(sqlpro2);
        if (tbPro2.Rows.Count > 0)
        {
            if (tbPro2.Rows[0]["StsPro"].ToString() == "1")
            {
                if (!inputHasCommission.Checked && !inputUnCommission.Checked)
                {
                    divMessCommission.InnerHtml = "Bạn phải chọn 1 trong 2 cách hình thức cho hoa hồng!";
                    divErrorCommission.InnerHtml = "";
                    return;
                }
                else if (inputHasCommission.Checked && referralRateValue == 0 && referralAmountValue == 0)
                {
                    divErrorCommission.InnerHtml = "Bạn chưa nhập hoa hồng!";
                    divMessCommission.InnerHtml = "";
                    return;
                }
                else
                {
                    divErrorCommission.InnerHtml = "";
                    divMessCommission.InnerHtml = "";
                }
            }
            else
            {
                if (inputHasCommission.Checked)
                {
                    divErrorCommission.InnerHtml = "Bạn phải là thành viên Pro mới được áp dụng hoa hồng!";
                    return;
                }
                else
                {
                    divErrorCommission.InnerHtml = "";
                    divMessCommission.InnerHtml = "";
                }

            }
        }



        // commission
        double commission = tinDangHelper.calcCommission(price, referralRateValue, referralAmountValue);

        if (txtTieuDe.Value.Trim() != "")
        {
            TieuDe = txtTieuDe.Value.Trim();
            dvTieuDe.InnerHtml = "";
        }
        else
        {
            dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề cho bài viết!";
            return;
        }
        //Nội dung
        if (txtNoiDung.Value.Trim() != "")
        {
            NoiDung = txtNoiDung.Value.Trim();
            dvNoiDung.InnerHtml = "";
        }
        else
        {
            dvNoiDung.InnerHtml = "Bạn chưa nhập nội dung cho bài viết!";
            return;
        }
        //Hình ảnh nhỏ hơn 3 tấm
        if (arrHinhAnh.Length <= 3)
        {
            dvMessageHinhAnh.InnerHtml = "Bài đăng phải có 3 hình trở lên!";
            return;
        }
        else
            dvMessageHinhAnh.InnerHtml = "";



        // slug
        slug = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TieuDe.Trim()));

        //Insert tin đăng
        string sqlInsertTD = "insert into tb_Preview (idDanhMucCap1,idDanhMucCap2,idTinh,idHuyen,idPhuongXa,TuGia,DenGia,isGiaoHangMatPhi,SoLuongTheoLoaiGia,MaLoaiGia,TieuDe,NoiDung,idThanhVien,NgayDang,NgayDayLenTop,LoaiTinDang,NgayGuiDuyet)";
        sqlInsertTD += " values(";
        if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
            sqlInsertTD += "'" + idDanhMucCap1 + "'";
        else
            sqlInsertTD += "null";
        if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
            sqlInsertTD += ",'" + idDanhMucCap2 + "'";
        else
            sqlInsertTD += ",null";
        if (idTinh != "" && idTinh != "0")
            sqlInsertTD += ",'" + idTinh + "'";
        else
            sqlInsertTD += ",null";
        if (idHuyen != "" && idHuyen != "0")
            sqlInsertTD += ",'" + idHuyen + "'";
        else
            sqlInsertTD += ",null";
        if (idPhuong != "" && idPhuong != "0")
            sqlInsertTD += ",'" + idPhuong + "'";
        else
            sqlInsertTD += ",null";
        if (TuGia != "")
            sqlInsertTD += ",'" + TuGia + "'";
        else
            sqlInsertTD += ",null";
        if (DenGia != "")
            sqlInsertTD += ",'" + DenGia + "'";
        else
            sqlInsertTD += ",null";
        sqlInsertTD += ",'" + isGiaoHangMatPhi + "'";
        if (SoLuongTheoLoaiGia != "")
            sqlInsertTD += ",'" + SoLuongTheoLoaiGia + "'";
        else
            sqlInsertTD += ",null";
        if (MaLoaiGia != "")
            sqlInsertTD += ",'" + MaLoaiGia + "'";
        else
            sqlInsertTD += ",null";
        sqlInsertTD += ",N'" + TieuDe + "',@noidung";
        if (idThanhVien != "")
            sqlInsertTD += ",'" + idThanhVien + "'";
        else
            sqlInsertTD += ",null";
        sqlInsertTD += ",'" + DateTime.Now.ToString() + "','" + DateTime.Now.ToString() + "','" + LoaiTinDang + "','" + DateTime.Now.ToString() + "')";
        string[] paramsName = new string[1] { "@noidung" };
        string[] paramsValue = new string[1] { NoiDung };

        if (Connect.Exec(sqlInsertTD, paramsName, paramsValue))
        {
            string idTinDangMoi = "";
            string sqlTinDangMoi = "select top 1 idTinDang from tb_PreView where '1'='1'";
            if (idThanhVien != "")
                sqlTinDangMoi += " and idThanhVien='" + idThanhVien + "'";
            sqlTinDangMoi += " order by idTinDang desc";
            DataTable tbTinDangMoi = Connect.GetTable(sqlTinDangMoi);
            if (tbTinDangMoi.Rows.Count > 0)
                idTinDangMoi = tbTinDangMoi.Rows[0]["idTinDang"].ToString();
            for (int i = 0; i < arrHinhAnh.Length; i++)
            {
                if (arrHinhAnh[i].Trim() != "")
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Húp')", true);
                    //return;
                    string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTD,UrlHinhAnh) values ('" + idTinDangMoi + "','" + arrHinhAnh[i].Trim().Replace("~/", "") + "')";
                    Connect.Exec(sqlInsertHinhAnh);
                }
            }
            Connect.Exec("update tb_PreView set MaTinDang='RV" + DateTime.Now.Year + idTinDangMoi + "' where idTinDang='" + idTinDangMoi + "' ");

            if (idThanhVien != "")
                Response.Write("<script>window.open('/preview/pv?MaTD=" + idTinDangMoi+ "','_blank');</script>");
            //Response.Redirect("/preview/pv?MaTD=" + idTinDangMoi);
            else
                Response.Redirect(Domain);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!')", true);
        }
    }
    private bool approveCollab(string tinDangId, string ownerId, string collabId)
    {
        return (new Models.Referral()).create(tinDangId, ownerId, collabId);
    }

    protected void spentMoney(long total, string feeCode)
    {
        if(total <= 0)
        {
            return;
        }
        string accountId = Utilities.Auth.getAccountId();
        Services.Account.spentBalance(accountId, total);
        Services.BalanceHistory.createSpent(accountId, Utilities.BalanceHistory.SPENT_TYPE_ADD_POST, total, feeCode);
    }
}