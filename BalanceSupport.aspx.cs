﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;
using System.Activities.Expressions;
using System.Text;

public partial class BalanceSupport : System.Web.UI.Page
{
    protected DataRow authAccount = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.authAccount = Utilities.Auth.getAccount();
        if(this.authAccount == null)
        {
            Utilities.Redirect.notFound();
        }
        LoadThongTinThanhVien();
    }
    void LoadThongTinThanhVien()
    {
            txtNameShopID.InnerHtml = "<h4>" + (this.authAccount["TenCuaHang"].ToString().Trim() == "" ? "Chưa cung cấp" : this.authAccount["TenCuaHang"]) + "</h4>";
            txtInfoAccountCode.InnerText = "Mã thành viên: " + this.authAccount["Code"].ToString().Trim();
            txtLoaiUser.InnerText = "Bình Thường"; 
            if (this.authAccount["StsPro"].ToString().Trim() == "1")
            {
                txtLoaiUser.InnerText = "Thành Viên PRO";
                buttonUser.Visible = false;
                ImagePro.Style.Add("display", "");
            }
            string htmluser = "";
            if (this.authAccount["LyDo_KhongDuyet"].ToString().Trim() != "")
            {
                htmluser += "<div style='background-color:#fcf8e3;border-radius:10px;padding: 0 11px;'><p>Nội dung từ chối:" + this.authAccount["LyDo_KhongDuyet"].ToString().Trim() + "</p></div>";
                htmluser += "<a class='MainFunctionButton EditProfile' id='button_Forwad_mobi' style='background-color:#4cb050;color:white;margin-bottom:10px;height: 34px;' href='/thong-tin/pro'>Đăng ký lại</a>";
                ProNow.InnerHtml = htmluser;
                buttonUser.Visible = false;
                ImagePro.Visible = false;
            }
            if (this.authAccount["StsPro"].ToString().Trim() == "0" && this.authAccount["idAdmin_Duyet"].ToString().Trim() == "-1")
            {
                txtLoaiUser.InnerText = "Thành viên Pro đang chờ duyệt";
                ImagePro.Visible = false;
                buttonUser.Visible = false;
            }

            txtShopEmailID.InnerHtml = (this.authAccount["Email"].ToString().Trim() == "" ? "Chưa cung cấp" : this.authAccount["Email"].ToString());
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + this.authAccount["LinkAnh"].ToString().Trim()))
            {

                imgLinkAnh.Src = "/images/user/" + this.authAccount["LinkAnh"].ToString().Trim();
            }
            else 
            {
                imgLinkAnh.Src = this.authAccount["LinkAnh"].ToString().Trim();
            }
                
            txtShopPhoneID.InnerHtml = (this.authAccount["SoDienThoai"].ToString().Trim() == "" ? "Chưa cung cấp" : this.authAccount["SoDienThoai"].ToString());
            txtDateShopApprovedID.InnerHtml = (this.authAccount["NgayDangKy"].ToString().Trim() == "" ? "Chưa cung cấp" : DateTime.Parse(this.authAccount["NgayDangKy"].ToString()).ToString("dd-MM-yyyy"));

            if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + this.authAccount["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + this.authAccount["linkAnh"].ToString().Trim();
            string TenTinhThanh = StaticData.getField("City", "Ten", "id", this.authAccount["idTinh"].ToString());
            string TenQuanHuyen = StaticData.getField("District", "Ten", "id", this.authAccount["idHuyen"].ToString());
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", this.authAccount["idPhuongXa"].ToString());
            string DiaChi = this.authAccount["DiaChi"].ToString().Trim();
            if (DiaChi == "" && TenPhuongXa == "" && TenQuanHuyen == "" && TenTinhThanh == "")
                txtShopAddressID.InnerHtml = "Chưa cung cấp";
            else
            {
                txtShopAddressID.InnerHtml = DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh;
                txtShopAddressID.Attributes.Add("title", DiaChi + ", " + TenPhuongXa + ", " + TenQuanHuyen + ", " + TenTinhThanh);
            }

            string idThanhVien_Cookie = null;
            try
            {
                idThanhVien_Cookie = Request.Cookies["TungTang_Login"].Value.Trim();
            }
            catch { }
            if (this.authAccount["idThanhVien"].ToString() != idThanhVien_Cookie)
            {
                TitleHeader.InnerText = "TIN CÙNG NGƯỜI ĐĂNG";
                ProNow.Visible = false;
                if (this.authAccount["StsPro"].ToString().Trim() == "1")
                {
                    txtLoaiUser.InnerText = "Thành Viên PRO";
                    ImagePro.Style.Add("display", "");
                }
                else
                {
                    txtLoaiUser.InnerText = "Bình Thường";
                    ImagePro.Style.Add("display", "");
                }
                btnTin.Visible = false;
            }
    }
}