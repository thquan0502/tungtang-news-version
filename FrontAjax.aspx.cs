﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Globalization;
using System.Data;

public partial class FrontAjax : System.Web.UI.Page
{
    protected string reqAction = null;
    protected bool isSuccess = true;
    protected DataRow authAccount = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        Dictionary<string, Object> rs = new Dictionary<string, Object>();
        object data = null;

        if (!string.IsNullOrWhiteSpace(Request.QueryString["Action"]))
        {
            this.reqAction = Request.QueryString["Action"].Trim();
        }
        this.authAccount = Utilities.Auth.getAccount();

        switch(this.reqAction)
        {
            case "getPostPackages":
                data = this.getPostPackages();
                break;
            default:
                break;
        }

        rs.Add("isSuccess", this.isSuccess);
        rs.Add("data", data);

        string json = JsonConvert.SerializeObject(rs);
        Response.ContentType = "text/json";
        Response.Write(json);
    }

    protected List<Dictionary<string, Object>> getPostPackages()
    {
        DataTable postFees = Services.PostFees.getAll();

        List<Dictionary<string, Object>> list = new List<Dictionary<string, Object>>();

        for (int i = 0; i < postFees.Rows.Count; i = i + 1)
        {
            Dictionary<string, Object> row = new Dictionary<string, Object>();
            row.Add("id", postFees.Rows[i].Field<long>("Id"));
            row.Add("code", postFees.Rows[i].Field<string>("Code"));
            row.Add("name", postFees.Rows[i].Field<string>("Name"));
            row.Add("pricePer1Days", postFees.Rows[i].Field<long>("PricePer1Days"));
            row.Add("pricePer7Days", postFees.Rows[i].Field<long>("PricePer7Days"));
            row.Add("pricePer14Days", postFees.Rows[i].Field<long>("PricePer14Days"));
            row.Add("pricePer30Days", postFees.Rows[i].Field<long>("PricePer30Days"));
            list.Add(row);
        }

        return list;
    }
}