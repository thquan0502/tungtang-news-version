﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ChiTietTinTuc : System.Web.UI.Page
{
    string idTinTuc = "";
    string Domain = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        HttpContext context = HttpContext.Current;
        string[] Action = context.Items["Action"].ToString().Split('-');
        idTinTuc = Action[0];
        string TieuDe = StaticData.getField("tb_TinTuc", "TieuDe", "idTinTuc", idTinTuc);
        this.Title = TieuDe + " | Tung Tăng";
        if (!IsPostBack)
        {
            LoadTinTuc();
        }
    }
    private void LoadTinTuc()
    {
        string URL = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
        string sqlTinDang = "select * from tb_TinTuc where KichHoat='True' and idTinTuc='" + idTinTuc + "'";
        DataTable tbTinDang = Connect.GetTable(sqlTinDang);
        if (tbTinDang.Rows.Count > 0)
        {
            //Load link title
            string sTrangChu = "";
            sTrangChu = "<a href='" + Domain + "'>Trang chủ</a>";
            dvLinkTitle.InnerHtml = sTrangChu + " <span style='font-family: cursive;'>></span><a href='" + Domain + "/tin-tuc/tat'>Tin tức </a><span style='font-family: cursive;'>></span> " + tbTinDang.Rows[0]["TieuDe"];
            //End load link title

            dvTitle.InnerHtml = tbTinDang.Rows[0]["TieuDe"].ToString().ToUpper();
            
            dvNoiDung.InnerHtml = tbTinDang.Rows[0]["MoTa"].ToString().Replace("<a", "<a rel='nofollow'");
        }
    }
    private void LoadTinHot()
    {
        string sqlTinDangCungDanhMuc = "select top 5 * from tb_TinTuc where KichHoat='True' and isHot = 'True' and idTinTuc!='" + idTinTuc + "' order by idTinTuc desc";
        DataTable tbTinDangCungDanhMuc = Connect.GetTable(sqlTinDangCungDanhMuc);
        string html = "";
        for (int i = 0; i < tbTinDangCungDanhMuc.Rows.Count; i++)
        {
            string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDangCungDanhMuc.Rows[i]["TieuDeSeo"].ToString().Trim()));
            string url = Domain + "/cttt/" + tbTinDangCungDanhMuc.Rows[i]["idTinTuc"].ToString() + "-" + TieuDeSau;
            html += "<li class='hot-job'>";
            html += "   <a href='" + url + "' title='" + tbTinDangCungDanhMuc.Rows[i]["TieuDeSeo"].ToString() + " - Tung Tăng'>";
            html += "      <span class='score'>";
            string urlHinhAnh = "";
            if (tbTinDangCungDanhMuc.Rows[0]["AnhDaiDien"].ToString() != "")
                urlHinhAnh = Domain + "/images/news/" + tbTinDangCungDanhMuc.Rows[0]["AnhDaiDien"].ToString();
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            html += "          <img src='" + urlHinhAnh + "' class='imgmenu' />";
            html += "      </span>";
            html += "      <span class='job-title'>";
            html += "           <strong class='text-clip'>" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"].ToString() + "</strong>";
            html += "           <em class='text-clip'><span>" + tbTinDangCungDanhMuc.Rows[i]["MoTaNgan"].ToString() + "</span></em>";
            html += "           <em class='text-clip'>Ngày: " + DateTime.Parse(tbTinDangCungDanhMuc.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy") + "</em>";
            html += "      </span>";
            html += "   </a>";
            html += "</li>";
        }
        ulTinCungDanhMuc.InnerHtml = html;
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        Response.Redirect(Domain + "/tin-tuc/tat");
    }
}