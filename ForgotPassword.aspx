﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/asset/front/css/auth.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="container">
        <div class="row" style="margin-top:88px;">
            <div class="col-sm-4 col-sm-push-4">

                <%-- start: page message --%>
                <% if(Session["forgot_password_error"] != null) { %>
                    <div class="alert alert-danger">
                        <%= (string) Session["forgot_password_error"] %>
                    </div>
                    <% Session["forgot_password_error"] = null; %>
                <% } %>
                <%-- end page message --%>
				
				 <%--task 2--%>
                  <%-- start: page message --%>
                <% if(Session["forgot_password_errors"] != null) { %>
                    <div class="alert alert-danger btn-resends" id="arlert-head">
                        <%= (string) Session["forgot_password_errors"] %>
                    </div>
                    <% Session["forgot_password_errors"] = null; %>
                <% } %>
                <%-- end page message --%>

				<h5 class="text-center"><strong>TẠO LẠI MẬT KHẨU</strong></h5>

                <div id="divInput" runat="server">
                    <div class="form-group">
                        <asp:TextBox id="txtPhone" TextMode="SingleLine" placeholder="Số điện thoại" class="form-control" runat="server" required="required" />
                    </div>
                    <div class="form-group">
                        <asp:TextBox id="txtPass" TextMode="SingleLine" placeholder="Mật khẩu" class="form-control" runat="server" required="required" />
                    </div>
                    <div class="form-group">
                        <asp:TextBox id="txtPass2" TextMode="SingleLine" placeholder="Nhập lại mật khẩu" class="form-control" runat="server" required="required" />
                    </div>                                        
                    <div class="form-group">                              
                        <asp:Button ID="btnInput" runat="server" Text="Tiếp tục" OnClick="btnInput_Click" class="btn btn-primary btn-block" />
                    </div>
                </div>
               <div id="divVerification" runat="server">
                    <div class="alert alert-success btn-resends" id="alert1">
                        <i><a href="/" target="_blank"><strong>Tungtang.com.vn</strong></a> đã gửi mã xác thực đến số điện thoại <a href="tel:<%= dataSmsPhone %>"><strong><%= dataSmsPhone %></strong></a>, hãy kiểm tra và nhập mã xác thực</i> 
                    </div>
                     <div class="alert alert-danger " id="alert2">
                        <i>Mã xác thực đã hết hiệu lực, xin quý khách gửi lại mã xác thực mới!</i> 
                    </div>
                    <div class="form-group btn-resends" id="annon">
						<div class="first-name" style="width: calc(80% - 2px);">
                        <asp:TextBox id="txtCode" TextMode="SingleLine" placeholder="Nhập mã xác thực" class="form-control" runat="server"  />
							 </div>
						<div class="last-name" style="width: calc(20% - 2px);">
							  <asp:TextBox TextMode="SingleLine" placeholder="Gửi lại sau" class="form-control btn-resend text-time" ReadOnly runat="server" />
							</div>
						<p style="color: red;"> Vui lòng nhập mã xác thực!</p>
                    </div>
                    <div class="form-group btn-resends " id="btnVerifys">
                        <asp:Button ID="btnVerify" runat="server" Text="Xác thực" OnClick="btnVerify_Click" class="btn btn-primary btn-block btn-resends" />
                        </div>
                     <div class="form-group" id="btnResends">
                        <asp:Button ID="btnResend" runat="server" Text="Gửi lại mã" OnClick="btnResend_Click" class="btn btn-success btn-block btn-resends" />
                    </div>
                    <div class="form-group">
                    <asp:Button ID="btnReinput" runat="server" Text="Thay đổi số điện thoại" OnClick="btnReinput_Click" class="btn btn-info btn-block" />
                    </div>
                </div>
                <div id="divExpiration" runat="server">
                    <div class="alert alert-warning">
                        <i>Bạn đang thực hiện tạo lại mật khẩu với số điện thoại <a href="tel:<%= dataSmsPhone %>"><%= dataSmsPhone %></a>, hãy xác thực lại số điện thoại của bạn.</i> 
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnReverify" runat="server" Text="Xác thực lại" OnClick="btnReverify_Click" class="btn btn-primary btn-block" />
                        <asp:Button ID="btnReinput2" runat="server" Text="Thay đổi số điện thoại" OnClick="btnReinput2_Click" class="btn btn-primary btn-block" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        (function () {
            var diffSeconds = <%= diffSeconds %>;
            if (diffSeconds >= 0 && diffSeconds < 60) {
                var remainSeconds = 60 - diffSeconds;
                var text = $('.btn-resend').val();
                var fnLoop = async function () {
                    if (remainSeconds > 0) {
                        $('.btn-resend').attr('disabled', 'disabled');
                        $('#btnResends').addClass('btn-resends');
                        $('#btnVerifys').removeClass('btn-resends');
                        $('#alert2').addClass('btn-resends');
                        $('#annon').removeClass('btn-resends');
                        $('#arlert-head').removeClass('btn-resends');
                        $('#alert1').removeClass('btn-resends');
                        $('.btn-resend').val(`${text} (${remainSeconds})`);
                        remainSeconds = remainSeconds - 1;
                        setTimeout(fnLoop, 1000);
                    } else {
                        $('#btnResends').removeClass('btn-resends');
                        $('#btnVerifys').addClass('btn-resends');
                        $('#alert1').addClass('btn-resends');
                        $('#arlert-head').addClass('btn-resends');
                        $('#annon').addClass('btn-resends');
                        $('#alert2').removeClass('btn-resends');
                        $('.btn-resend').addClass('btn-resends');
                        $('.btn-resend').val(text);
                       
                    }
                }
                fnLoop();
            }
        }) ();
    </script>
</asp:Content>
