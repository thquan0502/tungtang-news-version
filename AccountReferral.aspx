﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPageNew.master" CodeFile="AccountReferral.aspx.cs" Inherits="AccountReferral" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="/asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link href="../Css/Page/ThongTinCaNhan.css" rel="stylesheet" />
    <link href="../Css/Page/ThongTinCaNhan-Copy.css" rel="stylesheet" />
    <link href="../Css/Page/AccountReferral.css" rel="stylesheet" />
    <script src="../Js/Page/ThongTinCaNhan.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-content">
        <div class="container WrapperContainer">
            <div class="breadcrumWrapper" style="margin-top: 10px;">
                <ul class="sc-jzJRlG cIiMVT">
                    <li class="sc-cSHVUG jbltJC"><a href="/" class="sc-kAzzGY eZQMkE">Trang chủ</a></li>
                    <li class="sc-cSHVUG jbltJC"><a class="sc-kAzzGY llFQGq">Trang cá nhân</a></li>
                </ul>
            </div>
            <div class="PaperContainer contactInfo false">
                <div class="PaperInfoWrapper" style="color: rgba(0, 0, 0, 0.87); background-color: #ffffff; transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Verdana, Arial, sans-serif; -webkit-tap-highlight-color: rgba(0,0,0,0); box-shadow: 0 1px 2px rgba(0,0,0,.1); border-radius: 2px">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 BasicInfo">
                            <div class="AvatarWrapper">
                                <img id="imgLinkAnh" runat="server" src="/images/icons/signin.png" style="color: #ffffff; border: .25px solid #ffba00; user-select: none; display: inline-flex; align-items: center; justify-content: center; font-size: 40px; border-radius: 50%; height: 80px; width: 80px"
                                    size="80">
                            </div>
                             <div class="InfoWrapper">
                                <span class="name" id="txtNameShopID" runat="server"></span>
                                <span><p id="txtInfoAccountCode" class="color-yellow" runat="server" style="font-weight: 600;display:inline-block;"></p></span>
                                <div class="UltiRow" id="ProNow" runat="server" style="margin-bottom:8px;"> 
                                    <span>
                                   <p style="display: inline-block;">Loại TV: </p>
                                   <h6 id="txtLoaiUser" runat="server" style="font-weight: 600;color:#4cb050;display: inline-block;"></h6>
                                    <img id="ImagePro" runat="server" src="/images/vuongniem_tungtang.png" alt="Thành viên Pro Tung Tăng" style="width: 21px;margin-top: -17px;margin-left: 3px;display:none;" />
                                   <a class="MainFunctionButton EditProfile" id="buttonUser" style="background-color:#4cb050;color:white;margin-bottom:10px;font-size: 9px;" href="/thong-tin/pro" runat="server">Đăng ký PRO</a>    
                                    </span>
                                </div>
                               <%--  <div class="UltiRow" id="UserIdenti" runat="server">
                                    <a class="MainFunctionButton EditProfile" id="btnEditInfoUserID" runat="server" href="/thong-tin/tt">Chỉnh sửa thông tin</a>
                                </div>--%>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ExtraInfo">
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày tham gia: <span id="txtDateShopApprovedID" runat="server"></span>
                            </div>
                            <div class="itemRow" style="height:auto;display:block;">
                                <i class="fa fa-map-marker" style="padding: 0 13px;"></i>Địa chỉ: <span id="txtShopAddressID" runat="server" style="white-space:normal;"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-phone" style="padding: 0 11px;"></i>Số điện thoại: <span id="txtShopPhoneID" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-envelope" style="padding: 0 10px;"></i>Email: <span id="txtShopEmailID" runat="server"></span>
                            </div>
                            <%--<div class="itemRow">
                                <i class="fa fa-credit-card" style="padding: 0 10px;"></i>Mã số thuế: &nbsp;  <span id="txtMST" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày cấp: &nbsp;  <span id="txtNgayCap" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-map-marker" style="padding: 0 10px;"></i>Nơi cấp: &nbsp;  <span id="txtNoiCap" runat="server"></span>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
        <div class="container indexPage PaperContainer" style="height:auto">
            <div class="PaperWrapper app-box">
                <h4 class="TitleHeading" ><span style="color:#33a837;font-weight:bold;font-size: 13px;">HOA HỒNG THỤ HƯỞNG</span></h4>
                <div class="row list">
                    <div style="padding: 8px 0px 8px 0px; width: 100%;">
                        <div class="portlet box">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card account-filter-card">
                                            <div class="card-body" style="font-size: 11px;">
                                                <div id="filterPostCodeWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Mã bài đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterPostCodeInput" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostOwnerWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Người đăng bài</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterPostOwnerInput" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterOwnerApprovedAtWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Ngày duyệt</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                                <input id="filterOwnerApprovedAtFromInput" type="text" class="form-control" />
                                                                <span class="input-group-addon"> đến </span>
                                                                <input id="filterOwnerApprovedAtToInput" type="text" class="form-control"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterReferralStatusWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Trạng thái</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterReferralStatusInput" class="form-control" style="padding: 5px 10px;">

                                                            </select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterReferralStatusWrapOther2" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Trạng thái</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterReferralStatusInputOther2" class="form-control" style="padding: 5px 10px;">

                                                            </select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterAddWrap" class="filter-row last">
                                                    <div class="row mb-2">
                                                            <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                                <button type="button" class="btn btn-primary btn-sm filter-add-btn" style="padding: 0px 5px;"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                            <div class="col-md-5">
                                                                
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="filter-bar d-flex flex-row mt-3">
                                                <div class="filter-bar-pagination flex-grow-1 d-flex justify-content-start align-items-center">
                                                    <label class="text-sm">Phân trang:&nbsp;&nbsp;</label>
                                                    <div id="divAccountReferralPagination2" class="app-table-pagination d-flex justify-content-end align-items-center" runat="server"></div>
                                                </div>
                                                <div class="filter-bar-buttons flex-grow-0">
                                                    <button id="btnFilterSubmit" type="button" class="btn btn-primary text-sm">Lọc tin</button>
                                                    <button id="btnFilterCancel" type="button" class="btn btn-danger text-sm" style="display: none;">Hủy lọc tin</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-scrollable app-table-wrap mt-5" id="dvAccountReferralTable" runat="server"></div>
                                <div id="divAccountReferralPagination1" class="app-table-pagination" runat="server"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalSelectFilter" class="modal new-modal modal-sm" style="overflow: hidden;">
        <ul class="list-group" style="margin-bottom: 0;">
             <a href="javascript:void(0);" data-target="filterPostCodeWrap" class="list-group-item filter-anchor">Mã bài đăng</a>
            <a href="javascript:void(0);" data-target="filterPostOwnerWrap" class="list-group-item filter-anchor">Người đăng bài</a>
            <a href="javascript:void(0);" data-target="filterOwnerApprovedAtWrap" class="list-group-item filter-anchor">Ngày duyệt</a>
            <a href="javascript:void(0);" data-status="1" class="list-group-item filter-anchor">Trạng thái</a>
          </ul>
    </div>
    <div id="modalAccountInfo" class="modal new-modal" style="overflow: hidden;">
        <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">
            Thông tin Người đăng bài
        </div>
        <div class="mt-4">
            <div class="row">
                <div class="col-md-3 profile-col">
                    <p>
                        <img class="modal-account-thumb mt-3" id="modalAccountInfo_imgThumbnail" src="" />
                    </p>
                </div>
                <div class="col-md-9 app-grid">
                    <div class="row">
                        <div class="col-md-12">
                            <p><label>Họ tên: </label> <span id="modalAccountInfo_spanFullName">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Mã NĐB: </label> <span id="modalAccountInfo_spanCode">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Số điện thoại: </label> <a id="modalAccountInfo_linkPhone" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Email: </label> <a id="modalAccountInfo_linkEmail" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Địa chỉ: </label> <span id="modalAccountInfo_spanAddr">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Đăng ký lúc: </label> <span id="modalAccountInfo_spanRegisterAt">(Chưa có thông tin)</span></p>
                        </div>
					</div>
                </div>
			</div>
        </div>
    </div>
    <div id="modalApproveReferral" class="new-modal modal" style="overflow: hidden;">
        <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">
            Hoa hồng thụ hưởng
        </div>
        <div style="margin-top: 10px;">
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-12">
                    <div id="modalApproveReferral_details"></div>
                </div>
            </div>
            <div class="row wrap-readonly" style="margin-bottom: 8px;">
                <div class="col-12">
                    <hr />
                </div>
                <div class="col-12 d-flex justify-content-around">
                    <a id="btnApproveReferral" href='javascript:void(0);' class='btn btn-primary btn-sm'>Xác nhận hoa hồng</a>
                    <a href='javascript:void(0);' class='btn btn-danger btn-sm btn-deny-referral' data-referral-id=''>Từ chối</a>
                </div>
            </div>
        </div>
    </div>

    <asp:TextBox ID="PFake_ApproveReferral_ReferralId" runat="server" style="display: none;" />
    <asp:Button ID="PFake_ApproveReferral_Submit" runat="server" style="display: none;" OnClick="PFake_ApproveReferral_Submit_Click" />

    <asp:TextBox ID="PFake_DenyReferral_ReferralId" runat="server" type="hidden" />
    <asp:Button ID="PFake_DenyReferral_Submit" runat="server" style="display: none;" OnClick="PFake_DenyReferral_Submit_Click" />

    <script>
        (function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        })();
        (function(){
            $(document).ready(function () {
                $("#btnApproveReferral").click(function () {
                    $('#ContentPlaceHolder1_PFake_ApproveReferral_Submit').click();
                });
            });
        })();
        (function(){
            $('.filter-add-btn').click(function(){
                $('#modalSelectFilter').modal();
            });
            var fnCheckShowBtnAdd = function(){
                if(
                     $('#filterPostCodeWrap').is(':visible') ||
                    $('#filterPostOwnerWrap').is(':visible') ||
                    $('#filterReferralStatusWrap').is(':visible') ||
                    $('#filterReferralStatusWrapOther2').is(':visible') ||
                    $('#filterOwnerApprovedAtWrap').is(':visible')
                ){
                    $('#filterAddWrap').show();
                    $('#btnFilterCancel').show();
                }else{
                    $('#filterAddWrap').hide();
                    $('#btnFilterCancel').hide();
                }
            }
            $('.filter-anchor').click(function(){
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
                var targetId = $(this).data('target');
                var dataStatus = $(this).data("status");
                if (dataStatus == '1') {
                    var arrTargets = ['filterReferralStatusWrap', 'filterReferralStatusWrapOther2'];
                    for (var i = 0; i < arrTargets.length; i = i + 1) {
                        if ($("#" + arrTargets[i]).is(":visible") == false) {
                            targetId = arrTargets[i];
                            break;
                        }
                    }
                }
                if (targetId != undefined && targetId != null && targetId != "") {
                    $('#' + targetId).show();
                    if (targetId == 'filterPostOwnerWrap') {
                        $('#filterPostOwnerInput').select2();
                    }
					  else if (targetId == 'filterPostCodeWrap') {
                        $('#filterPostCodeInput').select2();
                    }
                }
                $('#modalSelectFilter .close-modal').click();
            });

            $('#filterPostCodeWrap .filter-minus-btn').click(function(){
                $('#filterPostCodeInput').val("");
                $('#filterPostCodeWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostOwnerWrap .filter-minus-btn').click(function () {
                $('#filterPostOwnerInput').val("");
                $('#filterPostOwnerWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterReferralStatusWrap .filter-minus-btn').click(function(){
                $('#filterReferralStatusInput').val("");
                $('#filterReferralStatusWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterReferralStatusWrapOther2 .filter-minus-btn').click(function () {
                $('#filterReferralStatusInputOther2').val("");
                $('#filterReferralStatusWrapOther2').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterOwnerApprovedAtWrap .filter-minus-btn').click(function(){
                $('#filterOwnerApprovedAtFromInput').datepicker('setDate', null);
                $('#filterOwnerApprovedAtToInput').datepicker('setDate', null);
                $('#filterOwnerApprovedAtWrap').hide();
                fnCheckShowBtnAdd();
            });
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterOwnerApprovedAtFromInput').datepicker('setDate', dayx);
                $('#filterOwnerApprovedAtToInput').datepicker('setDate', dayy);
                $('#filterOwnerApprovedAtWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterOwnerApprovedAtFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterOwnerApprovedAtToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
         (function () {
            var templateDefaultOption = "<option value=''>Chọn</option>";
            var templateJsonOwners = <%= templateJsonOwners %>;
            var selectHtml1 = "";
            templateJsonOwners.forEach(function (ele1, pos1, arr1) {
                selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + " - " + ele1.phone + " - " + ele1.fullname + "</option>";
            });
            selectHtml1 = templateDefaultOption + selectHtml1;
            $('#filterPostOwnerInput').html(selectHtml1);
        })();
          (function () {
              var templateDefaultOption = "<option value=''>Chọn</option>";
            var templateJsonCode = <%= templateJsonCode %>;
              var selectHtml1 = "";
            templateJsonCode.forEach(function (ele1, pos1, arr1) {
                selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
            });
            selectHtml1 = templateDefaultOption + selectHtml1;
            $('#filterPostCodeInput').html(selectHtml1);
        })();
        (function () {
            var template = "";
            var data = <%= templateJsonReferralStatus %>;
            template += "<option value=''>Chọn</option>";
            data.forEach(function (ele) {
                template += "<option value='" + ele.value+"'>" + ele.text + "</option>";
            });
            $('#filterReferralStatusInput').html(template);
            $('#filterReferralStatusInput').val("<%= templateFilterReferralStatus %>");
            $('#filterReferralStatusInputOther2').html(template);
            $('#filterReferralStatusInputOther2').val("<%= templateFilterReferralStatusOther2 %>");

            var initFilterCode = "<%= templateFilterCode %>";
            $('#filterCodeInput').val(initFilterCode);
            if(initFilterCode != ""){
                $('#filterCodeWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterReferralStatus = "<%= templateFilterReferralStatus %>";
            $('#filterReferralStatusInput').val(initFilterReferralStatus);
            if(initFilterReferralStatus != ""){
                $('#filterReferralStatusWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterReferralStatusOther2 = "<%= templateFilterReferralStatusOther2 %>";
            $('#filterReferralStatusInputOther2').val(initFilterReferralStatusOther2);
            if (initFilterReferralStatusOther2 != "") {
                $('#filterReferralStatusWrapOther2').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
        })();
        (function () {
            var initFilterPostOwner = "<%= templateFilterPostOwner %>";

            $('#filterPostOwnerInput').val(initFilterPostOwner)

            if (initFilterPostOwner != "") {
                $('#filterPostOwnerWrap').show();
                $('#filterPostOwnerInput').select2();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
        })();
		 (function () {
            var initFilterPostCode = "<%= templateFilterPostCode %>";

            $('#filterPostCodeInput').val(initFilterPostCode)

            if (initFilterPostCode != "") {
                $('#filterPostCodeWrap').show();
                $('#filterPostCodeInput').select2();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
        })();
        (function () {
            var fnSlashToDash = function (sSlash) {
                var rs = "";
                if (sSlash != undefined && sSlash != null && sSlash != "") {
                    sSlash = sSlash.trim();
                    var parts = sSlash.split("/");
                    if (parts.length == 3) {
                        return parts[2] + "-" + parts[1] + "-" + parts[0];
                    }
                }
                return rs;
            }
            var baseUrl = "/tai-khoan/cong-tac";
            $("#btnFilterSubmit").click(function () {
                var filterPostCodeInput = $('#filterPostCodeInput').val();
                var filterPostOwnerInput = $('#filterPostOwnerInput').val();
                var filterReferralStatusInput = $('#filterReferralStatusInput').val();
                var filterReferralStatusInputOther2 = $('#filterReferralStatusInputOther2').val();
                var filterOwnerApprovedAtFromInput = fnSlashToDash($('#filterOwnerApprovedAtFromInput').val());
                var filterOwnerApprovedAtToInput = fnSlashToDash($('#filterOwnerApprovedAtToInput').val());
                var url = baseUrl;
                var counterParams = 0;
                if (filterPostCodeInput != undefined && filterPostCodeInput != null && filterPostCodeInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCode=" + filterPostCodeInput;
                }
                if (filterPostOwnerInput != undefined && filterPostOwnerInput != null && filterPostOwnerInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostOwner=" + filterPostOwnerInput;
                }
                if (filterReferralStatusInput != undefined && filterReferralStatusInput != null && filterReferralStatusInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterReferralStatus=" + filterReferralStatusInput;
                }
                if (filterReferralStatusInputOther2 != undefined && filterReferralStatusInputOther2 != null && filterReferralStatusInputOther2 != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterReferralStatusOther2=" + filterReferralStatusInputOther2;
                }
                if (filterOwnerApprovedAtFromInput != undefined && filterOwnerApprovedAtFromInput != null && filterOwnerApprovedAtFromInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterOwnerApprovedAtFrom=" + filterOwnerApprovedAtFromInput;
                }
                if (filterOwnerApprovedAtToInput != undefined && filterOwnerApprovedAtToInput != null && filterOwnerApprovedAtToInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterOwnerApprovedAtTo=" + filterOwnerApprovedAtToInput;
                }
                if(counterParams > 0){
                    window.location.href = url;
                }else{
                    $('.filter-add-btn').click();
                }
            });
            $('#btnFilterCancel').click(function () {
                window.location.href = baseUrl;
            });
        })();
        (function () {
            $('.user-info-btn').click(function () {
                var dataAccountId = $(this).data("account-id");
                var dataThumbnail = $(this).data("thumbnail");
                var dataFullname = $(this).data("fullname");
                var dataCode = $(this).data("code");
                var dataPhone = $(this).data("phone");
                var dataEmail = $(this).data("email");
                var dataAddr = $(this).data("addr");
                var dataRegisterAt = $(this).data("register-at");
                if (dataAccountId == undefined || dataAccountId == null || dataAccountId == "") {
                    return;
                }
                $('#modalAccountInfo_imgThumbnail').attr("src", (dataThumbnail != "" ? dataThumbnail : "#"));
                $('#modalAccountInfo_spanFullName').text((dataFullname != "" ? dataFullname : "(Chưa có thông tin)"));
                $('#modalAccountInfo_spanCode').text((dataCode != "" ? dataCode : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkPhone').text((dataPhone != "" ? dataPhone : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkPhone').attr("href", (dataPhone != "" ? ("tel:" + dataPhone) : "javascript:void(0);"));
                $('#modalAccountInfo_linkEmail').text((dataEmail != "" ? dataEmail : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkEmail').attr("href", (dataEmail != "" ? "mailto:" + dataEmail : "javascript:void(0);"));
                $('#modalAccountInfo_spanAddr').text((dataAddr != "" ? dataAddr : "(Chưa có thông tin)"));
                $('#modalAccountInfo_spanRegisterAt').text((dataRegisterAt != "" ? dataRegisterAt : "(Chưa có thông tin)"));
                $('#modalAccountInfo').modal();
            });
        })();
        (function () {
            $('.btn-approve-referral').click(function () {
                $('#ContentPlaceHolder1_PFake_ApproveReferral_ReferralId').val("");
                var referralId = $(this).data('referral-id');
                if (referralId != undefined && referralId != null && referralId != "") {
                    $('#ContentPlaceHolder1_PFake_ApproveReferral_ReferralId').val("" + referralId);
                    $('#modalApproveReferral .btn-deny-referral').data('referral-id', referralId);
                } else {
                    $('#ContentPlaceHolder1_PFake_ApproveReferral_ReferralId').val("");
                }

                var isReadOnly = $(this).data('readonly');
                if (isReadOnly != undefined && isReadOnly != null) {
                    isReadOnly = "" + isReadOnly;
                    isReadOnly = isReadOnly.trim().toUpperCase();
                    if (isReadOnly == "TRUE" || isReadOnly == "1") {
                        isReadOnly = true;
                    } else {
                        isReadOnly = false;
                    }
                } else {
                    isReadOnly = false;
                }

                if (isReadOnly) {
                    $('.wrap-readonly').css("display", "none");
                } else {
                    $('.wrap-readonly').css("display", "");
                }

                var divDetail = $('#modalApproveReferral_details');
                divDetail.html('');

                var postUrl = $(this).data('post-url');
                if (postUrl != undefined && postUrl != null && postUrl != "") {
                    postUrl = 'javascript:void(0);'
                }
                var postTitle = $(this).data('post-title');
                if (postTitle != undefined && postTitle != null && postTitle != "") {
                    divDetail.append('<p><strong>Bài viết:</strong> <a href="' + postUrl + '">' + postTitle+'</a></p>');
                }
                var postCode = $(this).data('post-code');
                if (postCode != undefined && postCode != null && postCode != "") {
                    divDetail.append('<p><strong>Mã bài viết:</strong> <span>' + postCode + '</span></p>');
                }
                var postCreatedAt = $(this).data('post-created-at');
                if (postCreatedAt != undefined && postCreatedAt != null && postCreatedAt != "") {
                    divDetail.append('<p><strong>Ngày đăng:</strong> <span>' + postCreatedAt + '</span></p>');
                }
                var postPriceFormat = $(this).data('post-price-format');
                if (postPriceFormat != undefined && postPriceFormat != null && postPriceFormat != "") {
                    divDetail.append('<p><strong>Giá:</strong> <span>' + postPriceFormat + '</span></p>');
                }
                var ownerFullname = $(this).data('owner-fullname');
                if (ownerFullname != undefined && ownerFullname != null && ownerFullname != "") {
                    divDetail.append('<p><strong>Người đăng bài:</strong> <span>' + ownerFullname + '</span></p>');
                }
                var ownerCode = $(this).data('owner-code');
                if (ownerCode != undefined && ownerCode != null && ownerCode != "") {
                    divDetail.append('<p><strong>Mã NĐB:</strong> <span>' + ownerCode + '</span></p>');
                }
                var ownerPhone = $(this).data('owner-phone');
                if (ownerPhone != undefined && ownerPhone != null && ownerPhone != "") {
                    divDetail.append('<p><strong>Số điện thoại:</strong> <a href="tel:' + ownerPhone+'">' + ownerPhone + '</a></p>');
                }
                var ownerEmail = $(this).data('owner-email');
                if (ownerEmail != undefined && ownerEmail != null && ownerEmail != "") {
                    divDetail.append('<p><strong>Email:</strong> <a href="mailto:' + ownerEmail + '">' + ownerEmail + '</a></p>');
                }
                var referralCommission = $(this).data('referral-commission');
                if (referralCommission != undefined && referralCommission != null && referralCommission != "") {
                    divDetail.append('<p><strong>Hoa hồng thụ hưởng:</strong> <span>' + referralCommission + '</span></p>');
                }
                var referralOwnerApprovedAt = $(this).data('referral-owner-approved-at');
                if (referralOwnerApprovedAt != undefined && referralOwnerApprovedAt != null && referralOwnerApprovedAt != "") {
                    divDetail.append('<p><strong>Người đăng bài duyệt lúc:</strong> <span>' + referralOwnerApprovedAt + '</span></p>');
                }
                var referralCollabApprovedAt = $(this).data('referral-collab-approved-at');
                if (referralCollabApprovedAt != undefined && referralCollabApprovedAt != null && referralCollabApprovedAt != "") {
                    divDetail.append('<p><strong>Bạn duyệt lúc:</strong> <span>' + referralCollabApprovedAt + '</span></p>');
                }
                var referralAdminApprovedAt = $(this).data('referral-admin-approved-at');
                if (referralAdminApprovedAt != undefined && referralAdminApprovedAt != null && referralAdminApprovedAt != "") {
                    divDetail.append('<p><strong>Tung Tăng duyệt lúc:</strong> <span>' + referralAdminApprovedAt + '</span></p>');
                }

                $('#modalApproveReferral').modal();
            });
        })();
        (function () {
            $('.btn-deny-referral').click(function () {
                var referralId = $(this).data('referral-id');

                if (confirm("Bạn từ chối cộng tác này ?")) {
                    $('#ContentPlaceHolder1_PFake_DenyReferral_ReferralId').val(referralId);
                    $('#ContentPlaceHolder1_PFake_DenyReferral_Submit').click();
                }
            });
        })();
    </script>
</asp:Content>