﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TinDang.aspx.cs" Inherits="TinDang" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly], #ContentPlaceHolder1_txtLoaiDanhMuc[readonly], #ContentPlaceHolder1_txtLocThem[readonly] {
            background-color: white;
            box-shadow: none;
        }

        .form-control {
            border: 0.5px solid #ddd;    padding-right: 20px;
        }

        #vnw-home .top-job ul li {
            margin: 3px 0;
        }
    </style>
    <script>
        function ShowBoLoc() {
            $("#modalBoLoc").modal({
                fadeDuration: 200,
                showClose: false
            });
        }
        function ShowDanhMuc() {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {

                        $("#modalDanhMuc").modal({
                            fadeDuration: 200,
                            showClose: false
                        });
                        $("header").css({ "z-index": "0" });
                        menuBar_fixed = false;
                        $("#tbDanhMuc_tbody").html(xmlhttp.responseText);
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadDanhMuc_TimKiem&Type=ALL", true);
            xmlhttp.send();
        }

        function ShowDiaDiem(type) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        if (type != "GoBack") {
                            $("#modalDiaDiem").modal({
                                fadeDuration: 200,
                                showClose: false
                            });
                            $("header").css({ "z-index": "0" });
                            menuBar_fixed = false;
                        }
                        $("#btnBackKhuVuc").css("display", "none");
                        $("#tbDiaDiem_tbody").html(xmlhttp.responseText);
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadTinhThanh&Type=ALL", true);
            xmlhttp.send();
        }
        function LoadQuanHuyen(TT, TTT) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        $("#tbDiaDiem_tbody").html(xmlhttp.responseText);
                        $("#btnBackKhuVuc").css("display", "block");
                        $("#btnBackKhuVuc").attr("onclick", "ShowDiaDiem('GoBack');");
                        $("#ContentPlaceHolder1_ddlTinh").html("<option value='" + TT + "'>" + TTT + "</option>");
                        $("#ContentPlaceHolder1_ddlTinh").val(TT);
                        $("#ContentPlaceHolder1_txtTinhThanh").val(TT);
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadQuanHuyen&Type=ALL&&TT=" + TT + "&TTT=" + TTT, true);
            xmlhttp.send();
        }
        function LoadPhuongXa(QH, TT, TQH, TTT) {

            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        $("#tbDiaDiem_tbody").html(xmlhttp.responseText);
                        $("#btnBackKhuVuc").css("display", "block");
                        $("#btnBackKhuVuc").attr("onclick", "LoadQuanHuyen('" + TT + "','" + TTT + "');");

                        $("#ContentPlaceHolder1_slHuyen").html("<option value='" + QH + "'>" + TQH + "</option>");
                        $("#ContentPlaceHolder1_slHuyen").val(QH);
                        $("#ContentPlaceHolder1_txtQuanHuyen").val(QH);
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadPhuongXa&Type=ALL&&QH=" + QH + "&TT=" + TT + "&TTT=" + TTT + "&TQH=" + TQH, true);
            xmlhttp.send();
        }
        function ChonPhuongXa(PX, TQX, TT, QH, TTT, TQH) {
            $("#ContentPlaceHolder1_ddlTinh").html("<option value='" + TT + "'>" + TTT + "</option>");
            $("#ContentPlaceHolder1_ddlTinh").val(TT);
            $("#ContentPlaceHolder1_txtTinhThanh").val(TT);
            if (QH != "") {
                $("#ContentPlaceHolder1_slHuyen").html("<option value='" + QH + "'>" + TQH + "</option>");
                $("#ContentPlaceHolder1_slHuyen").val(QH);
                $("#ContentPlaceHolder1_txtQuanHuyen").val(QH);
            }
            else {
                $("#ContentPlaceHolder1_slHuyen").html("<option value=''></option>");
                $("#ContentPlaceHolder1_slHuyen").val('');
                $("#ContentPlaceHolder1_txtQuanHuyen").val('');
            }

            if (PX != "") {
                $("#ContentPlaceHolder1_slXa").html("<option value='" + PX + "'>" + TQX + "</option>");
                $("#ContentPlaceHolder1_slXa").val(PX);
                $("#ContentPlaceHolder1_txtPhuongXa").val(PX);
            }
            else {
                $("#ContentPlaceHolder1_slXa").html("<option value=''></option>");
                $("#ContentPlaceHolder1_slXa").val('');
                $("#ContentPlaceHolder1_txtPhuongXa").val('');
            }

            $.modal.close();
            var TinhThanh = $("#ContentPlaceHolder1_ddlTinh option:selected").text();
            var QuanHuyen = $("#ContentPlaceHolder1_slHuyen option:selected").text();
            var PhuongXa = $("#ContentPlaceHolder1_slXa option:selected").text();
            var chuoi = "";
            if (PhuongXa != "")
                chuoi = PhuongXa + ", ";
            if (QuanHuyen != "")
                chuoi = QuanHuyen + ", ";
            if (TinhThanh != "")
                chuoi = TinhThanh;
            $("#ContentPlaceHolder1_txtDiaDiem").val(chuoi);
            document.getElementById("ContentPlaceHolder1_btnTimKiem").click();

        }
        function ChonDanhMuc(MDM, DM) {
            $("#ContentPlaceHolder1_ddlLoaiDanhMuc").val(MDM);
            $("#ContentPlaceHolder1_txtLoaiDanhMuc").val(DM);
            $("#ContentPlaceHolder1_ddlLoaiDanhMucCap2").val('');
            $("#ContentPlaceHolder1_txtBoLocThem").val('');
            $.modal.close();
            document.getElementById("ContentPlaceHolder1_btnTimKiem").click();
        }
        function ChonDanhMucCap2(DMC2) {
            $("#ContentPlaceHolder1_ddlLoaiDanhMucCap2").val(DMC2);
            document.getElementById("ContentPlaceHolder1_btnTimKiem").click();
        }
        function EnterPress(e) {
            if (e.keyCode == 13) {
                document.getElementById("ContentPlaceHolder1_btnTimKiem").click();
            }
        }
        function GiaTu(value) {
            $("#ContentPlaceHolder1_spGiaTu").html(value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
        function GiaDen(value) {
            var max = document.getElementById("ContentPlaceHolder1_rgMaxGiaDen").max;
            if (parseFloat(max) < parseFloat(value))
                $("#ContentPlaceHolder1_spGiaDen").html(max.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '+');
            else
                $("#ContentPlaceHolder1_spGiaDen").html(value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
        function btnBoLoc_click() {
            var GiaTu = $("#ContentPlaceHolder1_rgGiaTu").val();
            var GiaDen = $("#ContentPlaceHolder1_rgGiaDen").val();
            var SapXepTheo = "DESC";
            if (!document.getElementById("ContentPlaceHolder1_radTinMoiTruoc").checked)
                SapXepTheo = "PRICE";
            var CanXem = "SEaBU";
            if (document.getElementById("ContentPlaceHolder1_ckbCanMua").checked && !document.getElementById("ContentPlaceHolder1_ckbCanBan").checked)
                CanXem = "BU";
            else if (!document.getElementById("ContentPlaceHolder1_ckbCanMua").checked && document.getElementById("ContentPlaceHolder1_ckbCanBan").checked)
                CanXem = "SE";
            $("#ContentPlaceHolder1_txtBoLocThem").val(GiaTu + "|" + GiaDen + "|" + SapXepTheo + "|" + CanXem);
            document.getElementById("ContentPlaceHolder1_btnTimKiem").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="modal-DiaDiem-cls">
        <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
            <div id="dvDSDiaDiem" style="overflow: auto;">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbDiaDiem_tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal-DiaDiem-cls">
        <div id="modalDanhMuc" class="modal" style="overflow: hidden;">
            <div id="dvDSDanhMuc" style="overflow: auto;">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <div style="text-align: center; font-size: 17px;">Chọn danh mục</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbDanhMuc_tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal-DiaDiem-cls">
        <div id="modalBoLoc" class="modal" style="overflow: hidden;">
            <div id="dvBoLoc" style="overflow: auto;">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <div style="text-align: center; font-size: 17px;">Lọc kết quả</div>
                            </th>
                        </tr>
                    </thead>
                </table>
                <br />
                <div class="col-sm-6">
                    <label>Giá từ <span id="spGiaTu" runat="server">0</span></label>
                    <input type="range" id="rgGiaTu" value="0" max="15000000" step="300000" oninput="GiaTu(this.value)" runat="server" />
                </div>
                <div class="col-sm-6">
                    <label>Đến <span id="spGiaDen" runat="server">30,000,000</span></label>
                    <input type="range" id="rgMaxGiaDen" runat="server" style="display:none;"/>
                    <input type="range" id="rgGiaDen" value="30000000" min="15000000" max="30000000" step="300000" oninput="GiaDen(this.value)" runat="server" />
                </div>
                <br />
                <label>Sắp xếp theo</label>
                <div class="DanhMucBoLoc">
                    <div class="styled-input-single radio">
                        <input type="radio" name="fieldset-3" id="radTinMoiTruoc" runat="server" />
                        <label for="ContentPlaceHolder1_radTinMoiTruoc"><i class="fa fa-clock-o"></i>Tin mới trước</label>
                    </div>
                    <div class="styled-input-single radio">
                        <input type="radio" name="fieldset-3" id="radGiaThapTruoc" runat="server" />
                        <label for="ContentPlaceHolder1_radGiaThapTruoc"><i class="fa fa-dollar"></i>Giá thấp trước</label>
                    </div>
                </div>
                <br />
                <label>Bạn cần</label>
                <div class="DanhMucBoLoc">
                    <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="fieldset-5" id="ckbCanMua" runat="server" checked />
                        <label for="ContentPlaceHolder1_ckbCanMua"><i class="fa fa-cart-arrow-down"></i>Mua</label>
                    </div>
                    <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="fieldset-5" id="ckbCanBan" runat="server" checked/>
                        <label for="ContentPlaceHolder1_ckbCanBan"><i class="fa fa-archive"></i>Bán</label>
                    </div>
                </div>

                <div class="col-sm-12">
                    <a class="btn btn-primary btn-search-all" style="width: 100%; margin: 10px 0;" onclick="btnBoLoc_click();">ÁP DỤNG</a>
                </div>
            </div>
        </div>
    </div>

    <section class="feature container">
        <div class="row">
            <!--Top Jobs-->
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div class="panel jobs-board-listing with-mc no-padding no-border">
                        <div class="panel-content" id="tabChoThue">
                            <div class="job-list scrollbar m-t-lg">
                                <div id="dvLinkTitle" runat="server" class="linktitle"><%--<a href="/">Trang chủ</a> > <a href="#">Cho thuê</a> > <a href="#">Thuê bất động sản</a> > Thuê nhà nguyên căn--%></div>
                                <div id="dvTitle" runat="server" class="title" style="background-color: white; border: 0;"><%--Thuê nhà nguyên căn--%></div>

                                <div class="clsTinDang" style="position: relative; padding: 0 10px;">
                                    <div class="searchV">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <i class="fa fa-search" style="position: absolute; top: 13px; left: 20px; border-right: 1px solid; padding-right: 7px;"></i>
                                                <input type="text" class="form-control" id="txtKeySearch" value="" placeholder="Bạn muốn tìm ..." runat="server" style="padding-left: 35px;" onkeypress="EnterPress(event);" />
                                            </div>
                                            <div class="col-sm-5">
                                                <input id="ddlLoaiDanhMuc" runat="server" style="padding-left: 0; padding-right: 0; display: none;" />
                                                <input id="ddlLoaiDanhMucCap2" runat="server" style="padding-left: 0; padding-right: 0; display: none;" />
                                                <i class="fa fa-angle-down" style="position: absolute; top: 13px; right: 12px; padding-left: 5px;"></i>
                                                <input type="text" id="txtLoaiDanhMuc" value="Tất cả danh mục" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDanhMuc();" />
                                            </div>
                                            <div class="col-sm-5">
                                                <select id="ddlTinh" runat="server" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                                <input type="text" id="txtTinhThanh" name="name" value="0" runat="server" style="display: none;" />
                                                <select id="slHuyen" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                                <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" style="display: none;" />
                                                <select id="slXa" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                                <input type="text" id="txtPhuongXa" name="name" value="" runat="server" style="display: none;" />
                                                <i class="fa fa-angle-down" style="position: absolute; top: 13px; right: 12px; padding-left: 5px;"></i>
                                                <input type="text" id="txtDiaDiem" value="Toàn quốc" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDiaDiem();" />
                                                <asp:LinkButton ID="btnTimKiem" runat="server" class="btn btn-primary btn-search-all" OnClick="btnTimKiem_Click" Style="display: none;">TK</asp:LinkButton>
                                            </div>
                                            <div class="col-sm-2">
                                                <i class="fa fa-angle-down" style="position: absolute; top: 13px; right: 12px; padding-left: 5px;"></i>
                                                <input type="text" id="txtBoLocThem" value="" runat="server" style="display: none;" />
                                                <input type="text" id="txtLocThem" value="Lọc" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowBoLoc();" />
                                            </div>
                                            <div class="col-sm-12">
                                                <div style="display: none;" id="dvDanhMucCap2" runat="server">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="dvTinDang" runat="server">
                                        <%--<ul>
                                           <li class="hot-job">
                                              <a href="#" title="Job - Nh&acirc;n Vi&ecirc;n Kinh Doanh Bất Động Sản - C&Ocirc;NG TY Cổ Phần Đầu Tư ASIA Land">
                                              <span class="score">
                                              <img src="images/products/xe1.png" class="imgmenu" />
                                              </span>
                                              <span class="job-title">
                                              <strong class="text-clip">Cho thuê xe sirius giá rẻ</strong>
                                              <em class="text-clip">Cho thuê xe máy giá tốt nhất TP HCM, chỉ với 50K bạn có thể tự do sơ hữu xe máy trong 1 ngày</em>
                                              <em class="text-clip">Giá thuê: <span style="color:red">50,000 đ/ 1 ngày</span></em>
                                              <em class="text-clip">15/05/2017</em>
                                              </span>
                                              </a>
                                           </li>
                                        </ul>--%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

