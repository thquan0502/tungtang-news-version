﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="QuanLyBanner.aspx.cs" Inherits="Admin_QuanLyBanner_QuanLyBanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function DeleteBanner(idBanner) {
            if (confirm("Bạn có muốn xóa không ?")) {
                //alert(idSlide);
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Lỗi !")
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteBanner&idBanner=" + idBanner, true);
                xmlhttp.send();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="title">QUẢN LÝ BANNER</div>
    <div class="box">
        <div class="box-body">
            <%--<div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Tên ảnh:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtMaSanPham" runat="server" name="Content.ContentName" type="text" value="" />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Tên sản phẩm:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtTenSanPham" runat="server" name="Content.ContentName" type="text" value="" />
                          </div>
                        </div>
                      </div>
                </div>--%>
            <div class="row">
                <div class="col-sm-9">
                    <a class="btn btn-primary btn-flat" href="QuanLyBanner-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
                </div>
                <div class="col-sm-3">
                        <div style="text-align:right;">
                            <%--<asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />--%>
                        </div>
                    </div>
            </div>
            <div id="dvBanner" runat="server">

            </div>
            <%--<table class="table table-bordered table-striped">
                <tr>
                    <th class="th">
                        STT
                    </th>
                    <th class="th">
                        Tên loại khách hàng
                    </th>
                    <th class="th">
                        Ghi chú
                    </th>
                    <th id="command" class="th"></th>
                </tr>
                <tr>
                        <td>
                            1
                        </td>
                        <td>
                            Loại khách hàng 1
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <a href="#"><img class="imgedit" src="../images/edit.png" />Sửa</a>|
                            <a href="#"><img class="imgedit" src="../images/delete.png" />Xóa</a>
                        </td>
                    </tr>
            </table>
            <br />
            <div class="pagination-container" style="text-align:center">
                <ul class="pagination">
                    <li class="active"><a>1</a></li>
                    <li class=""><a>2</a></li>
                    <li class=""><a>3</a></li>
                    <li class=""><a>4</a></li>
                    <li class=""><a>5</a></li>
                </ul>
            </div>--%>
        </div>
    </div>
</section>

    <!-- /.content -->
  </div>
        </form>
</asp:Content>

