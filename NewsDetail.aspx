﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="NewsDetail.aspx.cs" Inherits="TinDang" EnableEventValidation="false"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="../Css/Page/TinDang.css" rel="stylesheet" />
    <script src="../Js/Page/TinDang.min.js"></script>
    <style>

        .aTC{
            color:#33659c;
        }
        .aTC:hover{
             color:#33659c;
             text-decoration:underline;
        }

        .form-control {
            border: 0.5px solid #ddd;
            padding-right: 20px;
        }

        #vnw-home .top-job ul li {
            margin: 3px 0;
        }

        .jDyZht a.first {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .ctStickyAdsListing .ctAdListingWrapper .ctAdListingLowerBody .adItemPostedTime span {
            opacity: 0.5;
        }

        .foricon{
            width: 15px;
        }
         .foricon2{
            width: 15px;
        }
        .spDT
        {
           font-size: 11px;color: gray;  padding-top:5px; color:#008C72;
        }
        @media only screen and (max-width: 767px) {
.spDT
        {
           font-size: 10px;color: gray;color:#008C72;
         padding-top:1px;
        }

 .foricon{
            width: 15px;
        }

  .foricon2{
            width: 15px; margin-bottom: 1px;
        }
}


    </style>
    
    
    
        <link href="../Css/Slide.css" rel="stylesheet" />
    <link href="../Css/Page/ChiTietTinDang.css" rel="stylesheet" />
    <link href="../Css/Page/TinDangBlog.css" rel="stylesheet" />

    <%--OwlCarousel--%>
    <script src="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.js"></script>
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.css" rel="stylesheet" />
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.theme.default.min.css" rel="stylesheet" />

    <meta property="og:title" id="ogtitle" name="ogtitle" content="" runat="server" />
   <%-- <link rel="canonical" id="canonical" name="canonical" runat="server" href="https://tungtang.com.vn">--%>
    <link href="WowSlider/engine1/style.css" rel="stylesheet" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- <header>
        <div class="gEQeZ-pfX6E9qs-T0zFQG">
            <ol id="ContentPlaceHolder1_dvLinkTitle2" class="breadcrumb _3WIL_EScB1tm02Oqj0vbu8">
                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu"><a href="/"><span>&nbsp;Trang chủ</span></a></li>
                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu">
                    <span itemprop="item"><strong id="dvTieuDe" runat="server" itemprop="name">asdxasxz</strong></span>
                </li>
            </ol>
        </div>
        
    </header>--%>
   <main class="App__Main-fcmPPF dlWmGF">

       <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0" nonce="2DKCsLWC"></script>
        <article>
            <div class="container _1OmYs5J4aYBAERDzg-6W8a">
                 <%--<div class="row hKeDdA_G7No3HepOClD8G">
                    <div class="col-sm-12 hidden-xs">
                        <div id="dvTieuDe" class="linktitle" runat="server">
                           <a href="/">Trang chủ</a> > <a href="#">Cho thuê</a> > <a href="#">Thuê bất động sản</a> > Thuê nhà nguyên căn

                        </div>

                    </div>
                </div>--%>
                <div class="row no-gutter-sm-down">
                    <div class="col-md-12" style="z-index:0">
                        <div id="dvTieuDe" class="linktitle" runat="server" style="font-size: 11px;padding-left:3px; margin-top:34px;">
                         

                        </div>

                        
                        <div >
                            
                            

                            <div class="container" id="dvNoiDung" runat="server" style="padding-left:2%;padding-right:2%;padding-bottom:2%;width:100%;overflow:auto;" >

                            </div>

                            
                            

                           
                        </div>

                          <div class="margin-top-05">
                                <div class="col-xs-12 margin-top-10">
                                    <span><img src="https://cdn3.iconfinder.com/data/icons/glypho-free/64/share-256.png" style="width:25px;"/></span>
                                    <span><strong class="text-muted">Chia sẻ bài viết:</strong></span>
                                    <hr style="margin: 5px;">
                                    <div class="row">
                                        <p id="dvChiaSeBaiViet" runat="server" style="color: #888888; margin-bottom: 0;position:relative;">
                                        </p>
                                    </div>
                                    <input type='text' value='' id='myLinkCopy' runat="server" style="position: absolute; top: -10000px;" readonly/>
                                    <hr style="margin: 5px;" />
                                </div>

                                <div>
                                    <!-- react-empty: 2823 -->
                                </div>
                                <div class="_1uIWaTfPiU1VXSU5AnCLHo margin-top-10"></div>
                            </div>





                <%--<div class="col-sm-12" style="padding: 0px">
                    <div class="top-job">
                        <div class="panel jobs-board-listing with-mc no-padding no-border"> 
                            <div class="panel-content"  style="overflow: auto;">
                                <div class="job-list scrollbar m-t-lg">
                                    <strong class="text-muted">Tin cùng danh mục 22</strong>
                                    <hr style="margin: 5px;" />
                                    <div class="owl-carousel" id="Div1" runat="server"  style="z-index: 0;">

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>


                 
                <div class="_2lwN4F8oWK7ISQo377szov" >
                     <h1 class="_1S1hKqDtaGnfV2qZE3uluy">Bài viết liên quan</h1>
                    <div class="top-job">
                        <div class="panel jobs-board-listing with-mc no-padding no-border"> 
                            <div class="panel-content" id="tabTinCungDanhMuc" style="overflow: auto;" >
                                <div class="job-list scrollbar m-t-lg">
                                   
                                    <hr style="margin: 5px;" />
                                    <div class="owl-carousel" id="abc" runat="server" style="z-index: 0; overflow: auto;">

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>
    </main>
    <script src="WowSlider/engine1/wowslider.js"></script>
    <script src="WowSlider/engine1/script.js"></script>
</script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }
        function CopyLinkURL() {
            var copyText = document.getElementById("ContentPlaceHolder1_myLinkCopy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $('#spSaoChepThanhCong').show();
            setTimeout(function () { $('#spSaoChepThanhCong').hide(); }, 3000);
        }

        function showSlides(n) {
            try {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) { slideIndex = 1 }
                if (n < 1) { slideIndex = slides.length }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
            catch (e) {

            }
        }
        var x = $(window).width();
        var owl = $('.owl-carousel');
        if (x < 500) {
            owl.owlCarousel({

                items: 2,
                loop: true,
                margin: 10,
                autoplay: true,
                slideSpeed: 1000,

                autoplayTimeout: 5000,
                autoplayHoverPause: true,

            });
        }
        else {
            owl.owlCarousel({

                items: 4,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 9000,
                autoplayHoverPause: true
            });
        }


    </script>

</asp:Content>

