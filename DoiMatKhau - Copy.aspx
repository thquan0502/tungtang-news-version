﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="DoiMatKhau - Copy.aspx.cs" Inherits="DangKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script src="../Js/Page/DoiMatKhau.min.js"></script>
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
        }
    </style>
    <script>
        window.onload = function () {
             
        }
        function hasUnicode(str)
        {
            for (var i = 0; i < str.length; i++) {
                if (str.charCodeAt(i) > 127) return true;
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px;margin-top:40px;">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div id="vnw-log-in" class="container main-content"> 
                       <div class="col-md-8 col-md-push-4" style="margin-top: 36px;">
                            <h5 style="text-align: center;"><strong>ĐỔI MẬT KHẨU</strong></h5>
                            <hr />
                            <div class="row"> 
                                <div class="col-md-12 col-sm-8">
                                    <div class="infoUser">
                                        <div class="form-group">
                                            <asp:TextBox id="txtPassOld" TextMode="SingleLine" placeholder="Mật khẩu cũ" class="form-control" runat="server" required="required" />
											 <p id="MessPassOld" runat="server" style="color:red;"></p>
                                        </div>
                                         <div class="form-group">
                                            <asp:TextBox id="txtPass" TextMode="SingleLine" placeholder="Mật khẩu mới" class="form-control" runat="server" required="required" />
											 <p id="MessPass" runat="server" style="color:red;"></p>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox id="txtPass2" TextMode="SingleLine" placeholder="Nhập lại mật khẩu mới" class="form-control" runat="server" required="required" />
											 <p id="MessPass1" runat="server" style="color:red;"></p>
                                        </div>
                                        <!-- Buttons-->
                                        <div class="form-group" style="margin-top: 20px">
                                            <asp:LinkButton ID="btnLuuThayDoi" runat="server" class="btn btn-primary btn-block" OnClick="btnLuuThayDoi_Click">LƯU THAY ĐỔI</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </section>
    <script type="text/javascript">
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        function UploadHinhAnh_Onchange(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                $("#ContentPlaceHolder1_DaThayDoi_imgHinhAnh").val('1');
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</asp:Content>
