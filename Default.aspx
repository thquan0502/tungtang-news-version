﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        window.onload = function () {
            //document.getElementById("search-widget").style = "display:none";
        }
    </script>
    <style>
        .img-responsive {
            width: 100%;
            display: block;
            max-width: 100%;
            object-fit: cover;
            height: 100%;
        }

        ._2lwN4F8oWK7ISQo377szov ul li {
            height: 150px;
        }

    </style>
    <link href="../Css/Page/TinDang.css" rel="stylesheet" />
    <script src="../Js/Page/TinDang.min.js"></script>
    <style>

        .aTC{
            color:#33659c;
        }
        .aTC:hover{
             color:#33659c;
             text-decoration:underline;
        }

        .form-control {
            border: 0.5px solid #ddd;
            padding-right: 20px;
        }

        #vnw-home .top-job ul li {
            margin: 3px 0;
        }

        .jDyZht a.first {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .ctStickyAdsListing .ctAdListingWrapper .ctAdListingLowerBody .adItemPostedTime span {
            opacity: 0.5;
        }

        .foricon{
            width: 15px;
        }
         .foricon2{
            width: 15px;
        }
        .spDT
        {
           font-size: 11px;color: gray;  padding-top:5px; color:#008C72;
        }
        @media only screen and (max-width: 767px) {
.spDT
        {
           font-size: 10px;color: gray;color:#008C72;
         padding-top:1px;
        }

 .foricon{
            width: 15px;
        }

  .foricon2{
            width: 15px; margin-bottom: 1px;
        }
}

        .mb-5 {
            text-align:center;
            margin-top: 20px;
            margin-bottom: 3rem !important;
        }
      .rounded-circle{
                        border-radius: 50% !important;
                     }
      .img-fluid
      {
          max-width: 100%;
          height:175px;
      }
      
      .font-weight-bold{
          text-align:center;
          font-weight: 700!important;
          margin-bottom: 1rem!important;
      }
      .orange-text{
          text-align:center;
          color:#ff9800!important;
      }
  
	 .MoTaHomePage a{
      color: #ffba00;
  }
    </style>
    
    
    
        <link href="../Css/Slide.css" rel="stylesheet" />
    <link href="../Css/Page/ChiTietTinDang.css" rel="stylesheet" />
    <link href="../Css/Page/TinDangBlog.css" rel="stylesheet" />
	<link href="../Css/Page/TinDangChiTiet.css" rel="stylesheet" />

    <link href="WowSlider/engine1/style.css" rel="stylesheet" /> 
      <%--OwlCarousel--%>
    <script src="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.js"></script>
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.css" rel="stylesheet" />
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.theme.default.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <main class="App__Main-fcmPPF dlWmGF" style="margin-top:63px;">
        <div class="container HomePage__WrapperHome-ifDVfb bgYray">
            <!-- react-empty: 36 -->
            <div class="HomePage__WrapperBanner-iHGLGD jacNqP" style="z-index:0;">
                <div style="overflow-x: hidden;">
                    <div class="react-swipeable-view-container" runat="server">
                        <link rel="stylesheet" type="text/css" href="./WowSlider/engine1/style.css" />
                        <!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
	                    <div id="wowslider-container1">
	                    <div class="ws_images"><ul id="nope" runat="server">
		                    
	                    </ul></div>
	                    <div class="ws_bullets"><div>
		                    <a href="#" title="Hinh1"><span>1</span></a>
		                    <a href="#" title="Hinh2"><span>2</span></a>
		                    <a href="#" title="Hinh3"><span>3</span></a>
	                    </div></div><div class="ws_script" style="position:absolute;left:-99%"></div>
	                    <div class="ws_shadow"></div>
	                    </div>	
	                    <script type="text/javascript" src="./WowSlider/engine1/wowslider.js"></script>
	                    <script type="text/javascript" src="./WowSlider/engine1/script.js"></script>
                    </div>
                </div>
            </div>

            <div class="HomePage__Wrapper-bOcyCg foEGej" id="dvSubBanner" runat="server">
                <div class="HomePage__WrapperImage-fuwpOu iijBxM">
                    <img class="img-responsive" src="/images/Category/sub-banner.png" alt=""></div>
                <div class="HomePage__WrapperSubBanner-oypWk chcGnu">
                    <h2 class="HomePage__Text-dCwrYm ifPVbe" style="margin-right:5px;">Đăng nhập để không bỏ lỡ món hời giá tốt!</h2>
                    <a class="HomePage__ButtonLogin-efjLAA eilCNe" href="/dang-nhap/dn" title="Đăng nhập ngay">Đăng nhập ngay</a><a class="HomePage__ButtonRegister-fqiNUa hwtsqy" href="/dang-ky/dk" title="Đăng ký tài khoản">Đăng ký</a></div>
            </div>

            <div class="_2lwN4F8oWK7ISQo377szov" id="boxListCate">
				 <h1 class="_1S1hKqDtaGnfV2qZE3uluy" style="display:none;">Tung Tăng Mua Sắm: Sàn TMĐT mua bán, rao vặt đăng tin miễn phí</h1>
                <h3 class="_1S1hKqDtaGnfV2qZE3uluy">Khám phá danh mục</h3>
                <ul id="ulKhamPhaDanhMuc" runat="server">
                </ul>
            </div>
			 <div class="_2lwN4F8oWK7ISQo377szov">
                <h3 class="_1S1hKqDtaGnfV2qZE3uluy" style="font-size:19px;font-weight: bold;font-style: italic;">Tin ưu tiên</h3>
                <ul id="ulTinDang" runat="server">
                </ul>
            </div>
            <div class="_2lwN4F8oWK7ISQo377szov" >
             <p class="_1S1hKqDtaGnfV2qZE3uluy">Tại sao bạn nên đăng tin tại Tung Tăng?</p> 

                <div class="col-md-4 mb-md-0 mb-5">
                    <div class="testimonial">
                        <div class="avatar mx-auto">
                            <img src="images/su.png" alt="Đăng ký" style="width:50%; height:50%;" class="z-depth-1 img-fluid">

                        </div> 
                        <h4 class="font-weight-bold dark-grey-text mt-4">Đăng ký tài khoản miễn phí</h4>         
                    </div>
                </div>

                  <div class="col-md-4 mb-md-0 mb-5">
                    <div class="testimonial">
                        <div class="avatar mx-auto">
                            <img src="images/dt.png" alt="Quy trình Tung Tăng" style="width:50%; height:50%;" class="z-depth-1 img-fluid" >

                        </div> 
                        <h4 class="font-weight-bold dark-grey-text mt-4">Quy trình duyệt bài nhanh </h4>         
                    </div>
                </div>

                  <div class="col-md-4 mb-md-0 mb-5">
                    <div class="testimonial">
                        <div class="avatar mx-auto">
                            <img src="images/tk.png" alt="Tiết kiệm Tung Tăng" style="width:50%; height:50%;" class="z-depth-1 img-fluid">
                        </div>  
                        <h4 class="font-weight-bold dark-grey-text mt-4">Tiết kiệm chi phí quảng cáo</h4>           
                    </div>
                </div>

            </div>
			    <%--  div mô tả homepage--%>
             <div class="_2lwN4F8oWK7ISQo377szov" >
                 <div class="MoTaHomePage" runat="server" id="Motahomepage">

                 </div>
                 </div>
                
                <div class="_2lwN4F8oWK7ISQo377szov" >
                     <h3 class="_1S1hKqDtaGnfV2qZE3uluy">Bài viết hữu ích</h3>
                    <div class="top-job">
                        <div class="panel jobs-board-listing with-mc no-padding no-border"> 
                            <div class="panel-content" id="tabTinCungDanhMuc" style="overflow: auto;" >
                                <div class="job-list scrollbar m-t-lg">
                                   
                                    <hr style="margin: 5px;" />
                                    <div class="owl-carousel" id="abc" runat="server" style="z-index: 0; overflow: auto;">

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
					    <div class="col-xs-12 margin-top-10 _2lwN4F8oWK7ISQo377szov">
                                     <h3 class="_1S1hKqDtaGnfV2qZE3uluy">Liên kết nổi bật</h3>
                                    
                                    <div class="row container" style="overflow:auto;height:auto;margin-bottom: 10px;">
                                        <p id="dvTop" runat="server">
                                        </p>
                                    </div>                                                 
                                   
                                     
                                </div>

                      </div>
    </main>
   <%--     <script src="WowSlider/engine1/wowslider.js"></script>
    <script src="WowSlider/engine1/script.js"></script>--%>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }
        function CopyLinkURL() {
            var copyText = document.getElementById("ContentPlaceHolder1_myLinkCopy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $('#spSaoChepThanhCong').show();
            setTimeout(function () { $('#spSaoChepThanhCong').hide(); }, 3000);
        }

        function showSlides(n) {
            try {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) { slideIndex = 1 }
                if (n < 1) { slideIndex = slides.length }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
            catch (e) {

            }
        }
        var x = $(window).width();
        var owl = $('.owl-carousel');
        if (x < 500) {
            owl.owlCarousel({

                items: 2,
                loop: true,
                margin: 10,
                autoplay: true,
                slideSpeed: 1000,

                autoplayTimeout: 5000,
                autoplayHoverPause: true,

            });
        }
        else {
            owl.owlCarousel({

                items: 4,
                loop: true,
                margin: 10,
                autoplay: true,
                autoplayTimeout: 9000,
                autoplayHoverPause: true
            });
        }


    </script>

</asp:Content>
