﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TinTuc : System.Web.UI.Page
{
    string Domain = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 10;
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
            if (Page <= 0)
            {
                Response.Redirect(Domain + "/tin-tuc/tat");
            }
        }
        catch
        {
            Page = 1;
        }
        if (!IsPostBack)
        {
            LoadTinDang();
        }
        //Load link title
        string sTrangChu = "<a href='" + Domain + "'>Trang chủ</a>>><a href='/tin-tuc/tat'>Tin tức</a>";

        dvLinkTitle.InnerHtml = sTrangChu;
        //End load link title
        if (!IsPostBack)
        {
            //LoadDanhMuc();
        }
    }
    #region paging
    private void SetPage()
    {
        string sql = @"select count(idTinTuc) from tb_TinTuc where KichHoat='True'";
        
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (";
        sql += " order by idTinTuc desc";
        sql += @"  )AS RowNumber
	              ,*
                  from tb_TinTuc where KichHoat='True'";
        
        sql += ") as tb1 WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        DataTable table = Connect.GetTable(sql);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();
        SetPage();
        string html = "<ul>";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            string TieuDeSeo = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDeSeo"].ToString().Trim()));
            string urlTinDangChiTiet = Domain + "/cttt/" + table.Rows[i]["idTinTuc"].ToString() + "-" + TieuDeSeo;
            html += "<li class='hot-job' style='width:100%; padding-right:5px'>";
            html += "   <a href='" + urlTinDangChiTiet + "' title='" + table.Rows[i]["TieuDeSeo"].ToString() + " - Tung Tăng'>";
            html += "      <span class='score' style='width:300px;height:200px'>";
            string urlHinhAnh = table.Rows[i]["AnhDaiDien"].ToString();
            if (urlHinhAnh != "")
                urlHinhAnh = Domain + "/images/news/" + urlHinhAnh;
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            html += "          <img src='" + urlHinhAnh + "' class='imgmenu' />";
            html += "      </span>";
            html += "      <span class='job-title' style='width:69%;font-size:18px;text-align: justify;'>";
            html += "           <strong style='font-size:22px'>" + table.Rows[i]["TieuDe"].ToString() + "</strong>";
            html += "           <em>" + table.Rows[i]["MoTaNgan"].ToString() + "</em>";
            html += "           <em class='text-clip'>Ngày: " + DateTime.Parse(table.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy") + "</em>";
            html += "      </span>";
            html += "   </a>";
            html += "</li>";
        }
        html += "</ul>";
        html += "   <div style='text-align:center'><table style='width:400px; margin:auto auto'>";
        html += "   <tr>";
        html += "       <td class='footertable'>";
        string url = "";
        url = Domain + "/tin-tuc/tat?";
        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table></div>";
        dvTinDang.InnerHtml = html;
    }
    //private void LoadDanhMuc()
    //{
    //    string htmlTrai = "";
    //    string htmlPhai = "";
    //    string sqlDanhMucCap1 = "select * from tb_DanhMucCap1 order by SoThuTu asc";
    //    DataTable tbDanhMucCap1 = Connect.GetTable(sqlDanhMucCap1);
    //    for (int i = 0; i < tbDanhMucCap1.Rows.Count; i++)
    //    {
    //        string TieuDe1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString().Trim()));
    //        string url1 = Domain + "/tin-dang/1-" + tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() + "-" + TieuDe1;
    //        if (i % 2 == 0)
    //        {
    //            htmlTrai += "<div>";
    //            htmlTrai += "   <div class='titlemain'>+ <a href='" + url1 + "'>" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString() + "</a></div>";
    //            string sqlDanhMucCap2 = "select * from tb_DanhMucCap2 where idDanhMucCap1='" + tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() + "' order by SoThuTu asc";
    //            DataTable tbDanhMucCap2 = Connect.GetTable(sqlDanhMucCap2);
    //            for (int j = 0; j < tbDanhMucCap2.Rows.Count; j++)
    //            {
    //                string TieuDe2 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbDanhMucCap2.Rows[j]["TenDanhMucCap2"].ToString().Trim()));
    //                string url2 = Domain + "/tin-dang/2-" + tbDanhMucCap2.Rows[j]["idDanhMucCap2"].ToString() + "-" + TieuDe2;
    //                htmlTrai += "   <div class='amain'><a href='" + url2 + "'>- " + tbDanhMucCap2.Rows[j]["TenDanhMucCap2"].ToString() + "</div>";
    //            }
    //            htmlTrai += "</div>";
    //        }
    //        else
    //        {
    //            htmlPhai += "<div>";
    //            htmlPhai += "   <div class='titlemain'>+ <a href='" + url1 + "'>" + tbDanhMucCap1.Rows[i]["TenDanhMucCap1"].ToString() + "</a></div>";
    //            string sqlDanhMucCap2 = "select * from tb_DanhMucCap2 where idDanhMucCap1='" + tbDanhMucCap1.Rows[i]["idDanhMucCap1"].ToString() + "'";
    //            DataTable tbDanhMucCap2 = Connect.GetTable(sqlDanhMucCap2);
    //            for (int j = 0; j < tbDanhMucCap2.Rows.Count; j++)
    //            {
    //                string TieuDe2 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbDanhMucCap2.Rows[j]["TenDanhMucCap2"].ToString().Trim()));
    //                string url2 = Domain + "/tin-dang/2-" + tbDanhMucCap2.Rows[j]["idDanhMucCap2"].ToString() + "-" + TieuDe2;
    //                htmlPhai += "   <div class='amain'><a href='" + url2 + "'>- " + tbDanhMucCap2.Rows[j]["TenDanhMucCap2"].ToString() + "</div>";
    //            }
    //            htmlPhai += "</div>";
    //        }
    //    }
    //    liDanhMucTrai.InnerHtml = htmlTrai;
    //    liDanhMucPhai.InnerHtml = htmlPhai;
    //}
}