﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class TinDangChiTiet : System.Web.UI.Page
{
    string idTinDang = "";
    string Domain = "";
    string idThanhVien = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
        }
        HttpContext context = HttpContext.Current;
        string[] Action = context.Items["Action"].ToString().Split('-');
        idTinDang = Action[0];
        string TieuDe = StaticData.getField("tb_TinDang", "TieuDe", "idTinDang", idTinDang);

        //this.Title = TieuDe + " | Tung Tăng";
        if (!IsPostBack)
        {
            LoadTinDang();
            LoadTinCungDanhMuc();
            //LoadDanhMuc();
        }
    }
    private void LoadTinCungDanhMuc()
    {
        string idDanhMucCap1 = StaticData.getField("tb_TinDang", "idDanhMucCap1", "idTinDang", idTinDang);
        string sqlTinDangCungDanhMuc = "select * from tb_TinDang where isnull(isHetHan,'False')='False' and isnull(isDuyet,'False')='True' and idTinDang!='" + idTinDang + "' and idDanhMucCap1='" + idDanhMucCap1 + "' order by idTinDang desc";
        DataTable tbTinDangCungDanhMuc = Connect.GetTable(sqlTinDangCungDanhMuc);
        string html = "";
        for (int i = 0; i < tbTinDangCungDanhMuc.Rows.Count; i++)
        {
            string TieuDeSau = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDangCungDanhMuc.Rows[i]["TieuDe"].ToString().Trim()));
            string url = Domain + "/tdct/" + tbTinDangCungDanhMuc.Rows[i]["idTinDang"].ToString() + "-" + TieuDeSau;
            //
            string sqlHinhAnh = "select top 1 * from tb_HinhAnh where idTinDang='" + tbTinDangCungDanhMuc.Rows[i]["idTinDang"] + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            string urlHinhAnh = "";
            if (tbHinhAnh.Rows.Count > 0)
            {
                urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[0]["UrlHinhAnh"].ToString();
                if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                    urlHinhAnh = Domain + "/images/icons/noimage.png";
            }
            else
                urlHinhAnh = Domain + "/images/icons/noimage.png";
            //
            decimal Gia = decimal.Parse(KiemTraKhongNhap_LoadLen(tbTinDangCungDanhMuc.Rows[i]["TuGia"].ToString()));

            html += @"  <div class='item' >
                            <a href='" + url + @"' title='" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"'>
                                <span><img src='" + urlHinhAnh + @"' /></span>
                                <span class='title-TinDangLienQuan'>" + tbTinDangCungDanhMuc.Rows[i]["TieuDe"] + @"</span>
                                <span class='cost-TinDangLienQuan' style='color: red'>" + Gia.ToString("N0") + @"</span>
                            </a>
                        </div> ";
        }
        ulTinCungDanhMuc.InnerHtml = html;
    }
    static string SDT_ThanhVien = "";
    private void LoadTinDang()
    {
        string URL = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
        string sqlTinDang = "select * from tb_TinDang td inner join tb_DanhMucCap1 dmc1 on td.idDanhMucCap1=dmc1.idDanhMucCap1";
        sqlTinDang += " where isnull(isHetHan,'False')='False' and idTinDang='" + idTinDang + "'";
        DataTable tbTinDang = Connect.GetTable(sqlTinDang);
        if (tbTinDang.Rows.Count > 0)
        {
            SDT_ThanhVien = StaticData.getField("tb_ThanhVien", "TenDangNhap", "idThanhVien", tbTinDang.Rows[0]["idThanhVien"].ToString());
            //Load link title
            string sTrangChu = "";
            string sDanhMucCap1 = "";
            string sDanhMucCap2 = "";
            sTrangChu = "<a href='" + Domain + "'>Tin đăng</a>";

            string titleDanhMucCap1 = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(tbTinDang.Rows[0]["TenDanhMucCap1"].ToString().ToLower()));
            sDanhMucCap1 = "<a href='" + Domain + "/tin-dang/1-" + tbTinDang.Rows[0]["idDanhMucCap1"] + "-" + titleDanhMucCap1 + "'>" + tbTinDang.Rows[0]["TenDanhMucCap1"] + "</a>";
            sDanhMucCap2 = "<a href='" + Domain + "/tin-dang/1-" + tbTinDang.Rows[0]["idDanhMucCap1"] + "-" + titleDanhMucCap1 + "?C2=" + tbTinDang.Rows[0]["idDanhMucCap2"] + "'>" + StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", tbTinDang.Rows[0]["idDanhMucCap2"].ToString()) + "</a>";

            dvLinkTitle.InnerHtml = sTrangChu + " <span style='font-family: cursive;'>>></span> " + sDanhMucCap1 + " <span style='font-family: cursive;'>>></span> " + sDanhMucCap2;
            //End load link title

            dvTitle.InnerHtml = "<b>" + tbTinDang.Rows[0]["TieuDe"] + "</b>";
            //Load slide
            string htmlSlide = "";
            string htmlDotSlide = "";
            string sqlHinhAnh = "select * from tb_HinhAnh where idTinDang='" + idTinDang + "'";
            DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
            if (tbHinhAnh.Rows.Count > 0)
            {
                for (int i = 0; i < tbHinhAnh.Rows.Count; i++)
                {
                    string urlHinhAnh = "";
                    urlHinhAnh = Domain + "/images/td/slides/" + tbHinhAnh.Rows[i]["UrlHinhAnh"].ToString();
                    if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + urlHinhAnh))
                        urlHinhAnh = Domain + "/images/icons/noimage.png";
                    System.Drawing.Bitmap img = new System.Drawing.Bitmap(Server.MapPath(urlHinhAnh));


                    htmlSlide += "<div class='mySlides fade' style='max-height:450px;text-align:center;'>";
                    htmlSlide += "<div class='numbertext'></div>";

                    if (img.Width >= img.Height)
                        htmlSlide += "  <img src='" + urlHinhAnh + "' style='width:100%;box-shadow: -5px -3px 15px 1px #6363637d;'>";
                    else
                        htmlSlide += "  <img src='" + urlHinhAnh + "' style='width:50%;box-shadow: 0px 11px 131px 35px #6363637d;'>";

                    htmlSlide += "</div>";
                    htmlSlide += "<a class='prev' onclick='plusSlides(-1)'>&#10094;</a>";
                    htmlSlide += "<a class='next' onclick='plusSlides(1)'>&#10095;</a>";

                    htmlDotSlide += "<span class='dot' onclick='currentSlide(" + (i + 1).ToString() + ")'></span>";
                }
            }
            else
            {
                string urlHinhAnh = Domain + "/images/icons/noimage.png";
                htmlSlide += "<div class='mySlides fade'>";
                htmlSlide += "<div class='numbertext'></div>";
                htmlSlide += "  <img src='" + urlHinhAnh + "' style='width:100%;box-shadow: -5px -3px 15px 1px #6363637d;'>";
                //htmlSlide += "  <div class='text'><%--Caption Text--%></div>";
                htmlSlide += "</div>";
                htmlSlide += "<a class='prev' onclick='plusSlides(-1)'>&#10094;</a>";
                htmlSlide += "<a class='next' onclick='plusSlides(1)'>&#10095;</a>";
            }

            dvSlide.InnerHtml = htmlSlide;
            dvDotSlide.InnerHtml = htmlDotSlide;
            //End load slide
            string TenHuyen1 = StaticData.getField("District", "Ten", "Id", tbTinDang.Rows[0]["idHuyen"].ToString());
            string idTinh1 = tbTinDang.Rows[0]["idTinh"].ToString();
            string TenTinh1 = StaticData.getField("City", "Ten", "Id", idTinh1);
            string idPhuongXa = tbTinDang.Rows[0]["idPhuongXa"].ToString();
            string TenPhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "Id", idPhuongXa);
            string Chuoi_DiaDiem = "";
            if (TenTinh1 == "")
                Chuoi_DiaDiem = "Toàn quốc";
            else
            {
                if (TenHuyen1 == "")
                    Chuoi_DiaDiem = TenTinh1;
                else
                {
                    if (TenPhuongXa == "")
                        Chuoi_DiaDiem = TenHuyen1 + " - " + TenTinh1;
                    else
                        Chuoi_DiaDiem = TenPhuongXa + " - " + TenHuyen1 + " - " + TenTinh1;
                }
            }
            dvLoaiThue.InnerHtml = "<i class='fa fa-map-marker' style='padding:0px 14px 0px 6px;font-size: 20px;'></i> " + Chuoi_DiaDiem;

            string sGia = "";
            if (tbTinDang.Rows[0]["TuGia"].ToString() != "")
                sGia = "<span style='color:red;font-size: 16px;font-weight:700;'>" + double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + "đ" + "</span>";

            if (tbTinDang.Rows[0]["isHetHan"].ToString() == "True")
                sGia = "<span style='color:#888888;font-size: 16px;font-weight:700;'>" + double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".") + "đ" + "</span>" + "<span style='color:red;font-size: 20px;'> - ĐÃ HẾT HẠN</span>";

            dvGia.InnerHtml = sGia;
            dvMaTinDang.InnerHtml = "<i class='fa fa-indent' style='padding:0px 14px 0px 2px;font-size: 20px;'></i> " + tbTinDang.Rows[0]["MaTinDang"].ToString().Trim();

            //dvNgayDang.InnerHtml = "<i class='fa fa-calendar' style='padding: 0px 10px 0px 2px;font-size: 20px;'></i> Đăng từ " + DateTime.Parse(tbTinDang.Rows[0]["NgayDang"].ToString()).ToString("dd-MM-yyyy");

            string sqlThanhVien = "select * from tb_ThanhVien where idThanhVien='" + tbTinDang.Rows[0]["idThanhVien"].ToString() + "'";
            DataTable tbThanhVien = Connect.GetTable(sqlThanhVien);
            if (tbThanhVien.Rows.Count > 0)
            {
                dvAnhDaiDien.InnerHtml = "<img style='width: 45px;border-radius: 40px;border:solid 1px;box-shadow: 0px 0px 15px -5px #6363637d;' src='" + Domain + "/" + tbThanhVien.Rows[0]["idThanhVien"].ToString().Trim() + "'/>";
                if (!File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + tbThanhVien.Rows[0]["idThanhVien"].ToString().Trim()))
                    dvAnhDaiDien.InnerHtml = "<img style='width: 45px;border-radius: 40px;border:solid 1px;box-shadow: 0px 0px 15px -5px #6363637d;' src='" + Domain + "/images/icons/noUser.png" + "'/>";

                dvHoTen.InnerHtml = "<b>" + tbThanhVien.Rows[0]["TenCuaHang"] + "</b>";
                dvSoDienThoai.InnerHtml = " <b style='color:red'>" + tbThanhVien.Rows[0]["TenDangNhap"].ToString() + "</b>";
                dvEmail.InnerHtml = tbThanhVien.Rows[0]["Email"].ToString();
                dvDiaChi.InnerHtml = tbThanhVien.Rows[0]["DiaChi"].ToString();
                btnXemTrang.Attributes.Add("href", "/thong-tin-ca-nhan/ttcn-" + SDT_ThanhVien);
            }
            dvNoiDung.InnerHtml = tbTinDang.Rows[0]["NoiDung"].ToString();
            dvChiaSeBaiViet.InnerHtml = @"
                                          <a style='padding: 0 5px' target='_blank' href='http://www.facebook.com/share.php?u=" + URL + @"' title='Chia sẻ lên Facebook'><img src='https://cdn4.iconfinder.com/data/icons/social-media-icons-the-circle-set/48/facebook_circle-512.png' style='border:0; height:40px;' /></a>
                                          <a style='padding: 0 5px' target='_blank' href='https://www.facebook.com/v2.9/dialog/send?app_id=123456789&channel_url=" + URL + @"&app_id=123456789' title='Chia sẻ qua Messenger'><img src='https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Facebook_Messenger-512.png' style='border:0; height:40px;' /></a>
                                          <a class='zalo-share-button'  style='padding: 0 5px;cursor:pointer;'  data-href='" + URL + @"' data-oaid='579745863508352884' data-layout='2' data-color='blue' data-customize='true'><img src='https://seeklogo.com/images/Z/zalo-logo-B0A0B2B326-seeklogo.com.png' style='border:0; height:40px;' /></a><script src='https://sp.zalo.me/plugins/sdk.js'></script> 
                                          <input hidden id='txtURL' value='" + URL + @"' />  <script src='https://cdn.jsdelivr.net/clipboard.js/1.5.12/clipboard.min.js'></script>
                                          <a style='padding: 0 5px;cursor:pointer;'  data-clipboard-target='#txtURL' title='Sao chép liên kế' onclick=" + "\"" + "copyToClipboard('#txtURL')" + "\"" + @"><img src='https://cdn4.iconfinder.com/data/icons/web-links/512/41-512.png' style='border:0; height:40px;' /></a>
                                           ";
        }
    }
    string KiemTraKhongNhap_LoadLen(string SoTien)
    {
        string KQ = "0";
        try
        {
            KQ = decimal.Parse(SoTien).ToString();
        }
        catch { }
        return KQ;
    }
    public bool URLExists(string url)
    {
        System.Net.WebRequest webRequest = System.Net.WebRequest.Create(url.Replace("/", "\\").Replace("\\\\", "\\"));
        webRequest.Method = "HEAD";
        try
        {
            using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)webRequest.GetResponse())
            {
                if (response.StatusCode.ToString() == "OK")
                {
                    return true;
                }
                return false;
            }
        }
        catch
        {
            return false;
        }
    }

    protected void aThongTinLienHe_Click(object sender, EventArgs e)
    {
        Response.Redirect("/thong-tin-ca-nhan/ttcn-" + SDT_ThanhVien);
    }
}