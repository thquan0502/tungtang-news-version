﻿<%@ Page Language="C#"  MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="AccountPost.aspx.cs" Inherits="AccountPost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>

    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="/asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>
	
	<script src="../Js/Page/TinDang.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>
	
    <link href="../Css/Page/ThongTinCaNhan.css" rel="stylesheet" />
    <link href="../Css/Page/AccountPost.css" rel="stylesheet" />
    <link href="/asset/css/zalo/style.css" rel="stylesheet" />
    <script src="../Js/Page/ThongTinCaNhan.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        #filterCategory1, #filterCategory2{
            padding-top: 1px; 
        }
		 #checkbox-container{
          margin: 10px 5px;
        }

        #checkbox-container div{
          margin-bottom: 0px;
        }

        #checkbox-container button{
          margin-top: 5px;
        }
        #checkbox-container1{
          margin: 10px 5px;
        }

        #checkbox-container1 div{
          margin-bottom: 0px;
        }

        #checkbox-container1 button{
          margin-top: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-content">
        <div class="container WrapperContainer">
            <div class="breadcrumWrapper" style="margin-top: 10px;">
                <ul class="sc-jzJRlG cIiMVT">
                    <li class="sc-cSHVUG jbltJC"><a href="/" class="sc-kAzzGY eZQMkE">Trang chủ</a></li>
                    <li class="sc-cSHVUG jbltJC"><a class="sc-kAzzGY llFQGq">Trang cá nhân</a></li>
                </ul>
            </div>
            <div class="PaperContainer contactInfo false">
                <div class="PaperInfoWrapper" style="color: rgba(0, 0, 0, 0.87); background-color: #ffffff; transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Verdana, Arial, sans-serif; -webkit-tap-highlight-color: rgba(0,0,0,0); box-shadow: 0 1px 2px rgba(0,0,0,.1); border-radius: 2px">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 BasicInfo">
                            <div class="AvatarWrapper">
                                <img id="imgLinkAnh" runat="server" src="/images/icons/signin.png" style="color: #ffffff; border: .25px solid #ffba00; user-select: none; display: inline-flex; align-items: center; justify-content: center; font-size: 40px; border-radius: 50%; height: 80px; width: 80px"
                                    size="80">
                            </div>
                              <div class="InfoWrapper">
                                <span class="name" id="txtNameShopID" runat="server"></span>
                                <span><p id="txtInfoAccountCode" class="color-yellow" runat="server" style="font-weight: 600;display:inline-block;"></p></span>
                                <div class="UltiRow" id="ProNow" runat="server" style="margin-bottom:8px;"> 
                                    <span>
                                   <p style="display: inline-block;">Loại TV: </p>
                                   <h6 id="txtLoaiUser" runat="server" style="font-weight: 600;color:#4cb050;display: inline-block;"></h6>
                                    <img id="ImagePro" runat="server" src="/images/vuongniem_tungtang.png" alt="Thành viên Pro Tung Tăng" style="width: 21px;margin-top: -17px;margin-left: 3px;display:none;" />
                                   <a class="MainFunctionButton EditProfile" id="buttonUser" style="background-color:#4cb050;color:white;margin-bottom:10px;font-size: 9px;" href="/thong-tin/pro" runat="server">Đăng ký PRO</a>    
                                    </span>
                                </div>
                                <%-- <div class="UltiRow" id="UserIdenti" runat="server">
                                    <a class="MainFunctionButton EditProfile" id="btnEditInfoUserID" runat="server" href="/thong-tin/tt">Chỉnh sửa thông tin</a>
                                </div>--%>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ExtraInfo">
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày tham gia: <span id="txtDateShopApprovedID" runat="server"></span>
                            </div>
                            <div class="itemRow" style="height:auto;display:block;">
                                <i class="fa fa-map-marker" style="padding: 0 13px;"></i>Địa chỉ:<span id="txtShopAddressID" runat="server" style="white-space:normal;"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-phone" style="padding: 0 11px;"></i>Số điện thoại: <span id="txtShopPhoneID" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-envelope" style="padding: 0 10px;"></i>Email: <span id="txtShopEmailID" runat="server"></span>
                            </div>
                            <%--<div class="itemRow">
                                <i class="fa fa-credit-card" style="padding: 0 10px;"></i>Mã số thuế: &nbsp;  <span id="txtMST" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày cấp: &nbsp;  <span id="txtNgayCap" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-map-marker" style="padding: 0 10px;"></i>Nơi cấp: &nbsp;  <span id="txtNoiCap" runat="server"></span>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
        <div class="container indexPage PaperContainer" style="height:auto">
            <div class="PaperWrapper app-box">
                <h4 class="TitleHeading"><span style="color:#33a837;font-weight:bold;font-size: 13px;">CHIA SẺ LIỀN TAY - NHẬN NGAY HOA HỒNG</span></h4>
                <div class="row list">
                    <div style="padding: 8px 0px 8px 0px; width: 100%;">
                        <div class="portlet box">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card account-filter-card">
                                            <div class="card-body" style="font-size: 11px;">
                                                <div id="filterPostCodeWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Mã bài đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterPostCodeInput" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostOwnerWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Người đăng bài</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterPostOwnerInput" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostTitleWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Tiêu đề bài đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <input id="filterPostTitleInput" type="text" class="form-control" />
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostPriceWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Giá</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div id="checkbox-container">                                      
                                                            <div>
                                                                <label style="font-weight:bold"><input type="checkbox" id="chonruler" name="check" value="Chọn"/> Chọn Range lọc</label> 
                                                                <input id="filterPostPriceInput" type="text" class="form-control" />
                                                                <p></p>
                                                                <a id="textLX" style="color:blue"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thêm bộ lọc <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <a id="textLXAN" style="color:blue;display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ẩn <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <div id="BoLocLX" style="display:none;">
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb10100" name="check" value="Từ 10 đến 100"/> Từ 10 - 100 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb100500" name="check" value="Từ 100 đến 500" /> Từ 100 - 500 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb500" name="check" value="Từ 500 trở lên" /> 500 triệu trở lên</label>
                                                                </div>
                                                                <div style="margin-top:9px;">
                                                                         <input type="button" id="btn" value="Áp dụng Giá" class="btn btn-success btn-flat"/>
                                                                        <p id="messagex" style="color:#ffba00;padding-top:5px"></p>
                                                                    </div>
                                                                <input type="hidden" id="txttest" />
                                                                <input type="hidden" id="txtRangeLuotXem" runat="server" />
                                                                <input type="hidden" id="ruler" />
                                                                    <input type="hidden" id="rulersv" runat="server" />
                                         <script language="javascript">
                                            document.getElementById('btn').onclick = function()
                                            {
                                                document.getElementById('messagex').innerText = "Đã áp dụng!";
                                                // Khai báo tham số
                                                var checkbox = document.getElementsByName('check');
                                                var result = "";
                 
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkbox.length; i++){
                                                    if (checkbox[i].checked === true){
                                                        result += '' + checkbox[i].value + '';
                                                    }
                                                }                                              
                                                document.getElementById('txttest').value = result;
                                                document.getElementById('<%= txtRangeLuotXem.ClientID %>').value = document.getElementById('txttest').value;
                                                if(document.getElementById('chonruler').checked)
                                                {
                                                    document.getElementById('ruler').value = document.getElementById('chonruler').value;
                                                    document.getElementById('<%= rulersv.ClientID %>').value = document.getElementById('ruler').value;
                                                }
                                                
                                            };
                                        </script>   
                                        <script>
                                               document.getElementById('textLX').onclick = function () {
                                                   document.getElementById("BoLocLX").style.display = "";
                                                   document.getElementById("textLX").style.display = "none";
                                                   document.getElementById("textLXAN").style.display = "";
                                               }
                                               document.getElementById("textLXAN").onclick = function () {
                                                   document.getElementById("textLX").style.display = "";
                                                   document.getElementById("BoLocLX").style.display = "none";
                                                   document.getElementById("textLXAN").style.display = "none";
                                               };
                                               </script>   
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostCommissionWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Hoa hồng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                             <div id="checkbox-container1">                                      
                                                            <div>
                                                                <label style="font-weight:bold"><input type="checkbox" id="chonrulerhh" name="checkhh" value="Chọn"/> Chọn Range lọc</label> 
                                                                <input id="filterPostCommissionInput" type="text" class="form-control" />
                                                                <p></p>
                                                                <a id="textHH" style="color:blue"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thêm bộ lọc <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <a id="textHHAN" style="color:blue;display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ẩn <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <div id="BoLocHH" style="display:none;">
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb10100hh" name="checkhh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb100500hh" name="checkhh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb500hh" name="checkhh" value="Từ 500 trở lên" /> 500 triệu trở lên</label>
                                                                </div>
                                                                <div style="margin-top:9px;">
                                                                         <input type="button" id="btnhh" value="Áp dụng Hoa Hồng" class="btn btn-success btn-flat"/>
                                                                        <p id="messagexhh" style="color:#ffba00;padding-top:5px"></p>
                                                                    </div>
                                                                <input type="hidden" id="txttesthh" />
                                                                <input type="hidden" id="txtRangeHoaHong" runat="server" />
                                                                <input type="hidden" id="rulerhh" />
                                                                <input type="hidden" id="rulersvhh" runat="server" />
                                         <script language="javascript">                                            
                                            document.getElementById('btnhh').onclick = function()
                                            {
                                                document.getElementById('messagexhh').innerText = "Đã áp dụng!";
                                                // Khai báo tham số
                                                var checkboxhh = document.getElementsByName('checkhh');
                                                var resulthh = "";
                 
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkboxhh.length; i++){
                                                    if (checkboxhh[i].checked === true){
                                                        resulthh += '' + checkboxhh[i].value + '';
                                                    }
                                                }                                                                                              
                                                document.getElementById('txttesthh').value = resulthh;
                                                document.getElementById('<%= txtRangeHoaHong.ClientID %>').value = document.getElementById('txttesthh').value;
                                                if(document.getElementById('chonrulerhh').checked)
                                                {
                                                    document.getElementById('rulerhh').value = document.getElementById('chonrulerhh').value;
                                                    document.getElementById('<%= rulersvhh.ClientID %>').value = document.getElementById('rulerhh').value;
                                                }
                                            };                                        
                                             function chonLuotXemchh(obj) {

                                                 window.onload = function () {
                                                     //Giá
                                                     if (sessionStorage.getItem('select') == "select") {
                                                         return;
                                                     }
                                                     if (sessionStorage.getItem('luotxem') == "luotxem") {
                                                         return;
                                                     } 
                                                     if (sessionStorage.getItem('rangegia') == "rangegia") {
                                                         return;
                                                     }

                                                     var name = sessionStorage.getItem('select');
                                                     if (name !== null) $('#filterPostPriceInput').val(name);

                                                     var range = sessionStorage.getItem('luotxem');
                                                     if (range !== null) $('#txttest').val(range);

                                                     var rangeg = sessionStorage.getItem('rangegia');
                                                     if (rangeg !== null) $('#ruler').val(rangeg);
                                                     //Hoa Hồng
                                                     if (sessionStorage.getItem('selecthh') == "selecthh") {
                                                         return;
                                                     }
                                                     if (sessionStorage.getItem('luotxemhh') == "luotxemhh") {
                                                         return;
                                                     }
                                                     if (sessionStorage.getItem('rangegiahh') == "rangegiahh") {
                                                         return;
                                                     }

                                                     var rangeghh = sessionStorage.getItem('rangegiahh');
                                                     if (rangeghh !== null) $('#rulerhh').val(rangeghh);

                                                     var namehh = sessionStorage.getItem('selecthh');
                                                     if (namehh !== null) $('#filterPostCommissionInput').val(namehh);

                                                     var rangehh = sessionStorage.getItem('luotxemhh');
                                                     if (rangehh !== null) $('#txttesthh').val(rangehh);               

                                                 }
                                                 window.onbeforeunload = function () {
                                                     //Giá
                                                     sessionStorage.setItem("select", $('#filterPostPriceInput').val());
                                                     sessionStorage.setItem("luotxem", $('#txttest').val());
                                                     sessionStorage.setItem("rangegia", $('#ruler').val());
                                                     //Hoa Hồng
                                                     sessionStorage.setItem("selecthh", $('#filterPostCommissionInput').val());
                                                     sessionStorage.setItem("luotxemhh", $('#txttesthh').val());
                                                     sessionStorage.setItem("rangegiahh", $('#rulerhh').val());
                                                 }
                                             }    
                                        </script>   
                                        <script>
                                               document.getElementById('textHH').onclick = function () {
                                                   document.getElementById("BoLocHH").style.display = "";
                                                   document.getElementById("textHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "";
                                               }
                                               document.getElementById("textHHAN").onclick = function () {
                                                   document.getElementById("textHH").style.display = "";
                                                   document.getElementById("BoLocHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "none";
                                               };
                                               </script>   
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterCreatedAtWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Ngày đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                                <input id="filterCreatedAtFromInput" type="text" class="form-control"/>
										                        <span class="input-group-addon"> đến </span>
										                        <input id="filterCreatedAtToInput" type="text" class="form-control"/>
									                        </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterCategoryWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Danh mục</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterCategory1" class="form-control"></select>
                                                            <select id="filterCategory2" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterAddressWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Khu vực</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterCity" class="form-control"></select>
                                                            <select id="filterDistrict" class="form-control"></select>
                                                            <select id="filterWard" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div id="filterAddWrap" class="filter-row last">
                                                    <div class="row mb-2">
                                                            <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                                <button type="button" class="btn btn-primary btn-sm filter-add-btn" style="padding: 0px 5px;"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                            <div class="col-md-5">
                                                                
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="filter-bar d-flex flex-row mt-3">
                                                <div class="filter-bar-pagination flex-grow-1 d-flex justify-content-start align-items-center">
                                                    <label class="text-sm">Phân trang:&nbsp;&nbsp;</label>
                                                    <div id="divAccountPostPagination2" class="app-table-pagination d-flex justify-content-end align-items-center" runat="server"></div>
                                                </div>
                                                <div class="filter-bar-buttons flex-grow-0">
                                                    <button id="btnFilterSubmit" type="button" class="btn btn-primary text-sm">Lọc tin</button>
                                                    <button id="btnFilterCancel" type="button" class="btn btn-danger text-sm" style="display: none;">Hủy lọc tin</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-scrollable app-table-wrap mt-5" id="divAccountPostTable" runat="server"></div>
                                <div id="divAccountPostPagination1" class="app-table-pagination" runat="server"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalSelectFilter" class="modal new-modal modal-sm" style="overflow: hidden;">
        <ul class="list-group" style="margin-bottom: 0;">
            <a href="javascript:void(0);" data-target="filterPostCodeWrap" class="list-group-item filter-anchor">Mã bài đăng</a>
            <a href="javascript:void(0);" data-target="filterPostOwnerWrap" class="list-group-item filter-anchor">Người đăng bài</a>
            <a href="javascript:void(0);" data-target="filterPostTitleWrap" class="list-group-item filter-anchor">Tiêu đề bài đăng</a>
            <a href="javascript:void(0);" data-target="filterPostPriceWrap" class="list-group-item filter-anchor">Giá</a>
            <a href="javascript:void(0);" data-target="filterPostCommissionWrap" class="list-group-item filter-anchor">Hoa hồng</a>
            <a href="javascript:void(0);" data-target="filterCreatedAtWrap" class="list-group-item filter-anchor">Ngày đăng</a>
            <a href="javascript:void(0);" data-target="filterCategoryWrap" class="list-group-item filter-anchor">Danh mục</a>
            <a href="javascript:void(0);" data-target="filterAddressWrap" class="list-group-item filter-anchor">Khu vực</a>
          </ul>
    </div>
    <div id="modalAccountInfo" class="modal new-modal" style="overflow: hidden;">
        <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">
            Thông tin Người đăng bài
        </div>
        <div class="mt-4">
            <div class="row">
                <div class="col-md-3 profile-col">
                    <p>
                        <img class="modal-account-thumb mt-3" id="modalAccountInfo_imgThumbnail" src="" />
                    </p>
                </div>
                <div class="col-md-9 app-grid">
                    <div class="row">
                        <div class="col-md-12">
                            <p><label>Họ tên: </label> <span id="modalAccountInfo_spanFullName">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Mã NĐB: </label> <span id="modalAccountInfo_spanCode">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Số điện thoại: </label> <a id="modalAccountInfo_linkPhone" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Email: </label> <a id="modalAccountInfo_linkEmail" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Địa chỉ: </label> <span id="modalAccountInfo_spanAddr">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Đăng ký lúc: </label> <span id="modalAccountInfo_spanRegisterAt">(Chưa có thông tin)</span></p>
                        </div>
					</div>
                </div>
			</div>
        </div>
    </div>
    <script>
        (function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        })();
        (function () {
            $('.filter-add-btn').click(function () {
                $('#modalSelectFilter').modal();
            });
            var fnCheckShowBtnAdd = function () {
                if (
                    $('#filterPostCodeWrap').is(':visible') ||
                    $('#filterPostOwnerWrap').is(':visible') ||
                    $('#filterPostTitleWrap').is(':visible') ||
                    $('#filterPostPriceWrap').is(':visible') ||
                    $('#filterPostCommissionWrap').is(':visible') ||
                    $('#filterCreatedAtWrap').is(':visible') ||
                    $('#filterCategoryWrap').is(':visible') ||
                    $('#filterAddressWrap').is(':visible')
                ) {
                    $('#filterAddWrap').show();
                    $('#btnFilterCancel').show();
                } else {
                    $('#filterAddWrap').hide();
                    $('#btnFilterCancel').hide();
                }
            }
            $('.filter-anchor').click(function () {
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
                var target = $(this).data('target');
                $('#' + target).show();
                if (target == 'filterPostPriceWrap') {
                    console.log("aaa");
                     $('#filterPostPriceInput').ionRangeSlider({
                        min: 0,
                        max: 10000000,
                        type: 'double',
                        step: 100000,                        
                        postfix: "",                        
                        hasGrid: true,
                        prettifyFunc: function(num){                            
                            var le = parseInt(num % 1000);
                            var chan = parseInt(num / 1000);
                            var trieu = parseInt(chan / 1000);
                            var tram = chan % 1000;
                            var text = "";
                            if (chan > 1000 && le < 1000 && trieu % 1000) {
                                if (tram == 0)
                                {
                                    text += " " + trieu + " Triệu ";
                                }
                                else {
                                    text += " " + trieu + " Triệu " + tram + " Nghìn";
                                }                                
                            }
                            else if (trieu / 1000) {
                                text += " " + trieu + " Triệu";
                            }
                            else if (chan < 1000) {
                                text += " " + chan + " Nghìn";
                            }
                            text = text.trim();
                            if (text == "") {
                                text = "0 Nghìn";
                            }
                            return text;
                        },
                    });
                } else if (target == 'filterPostCommissionWrap') {
                    $('#filterPostCommissionInput').ionRangeSlider({
                        min: 0,
                        max: 10000000,
                        type: 'double',
                        step: 100000,                        
                        postfix: "",                        
                        hasGrid: true,
                        prettifyFunc: function(num){                            
                            var le = parseInt(num % 1000);
                            var chan = parseInt(num / 1000);
                            var trieu = parseInt(chan / 1000);
                            var tram = chan % 1000;
                            var text = "";
                            if (chan > 1000 && le < 1000 && trieu % 1000) {
                                if (tram == 0)
                                {
                                    text += " " + trieu + " Triệu ";
                                }
                                else {
                                    text += " " + trieu + " Triệu " + tram + " Nghìn";
                                }                                
                            }
                            else if (trieu / 1000) {
                                text += " " + trieu + " Triệu";
                            }
                            else if (chan < 1000) {
                                text += " " + chan + " Nghìn";
                            }
                            text = text.trim();
                            if (text == "") {
                                text = "0 Nghìn";
                            }
                            return text;
                        },
                    });
                } else if (target == 'filterPostOwnerWrap') {
                    $('#filterPostOwnerInput').select2();
                }
				else if (target == 'filterPostCodeWrap') {
                    $('#filterPostCodeInput').select2();
                }
                $('#modalSelectFilter .close-modal').click();
            });

            $('#filterPostCodeWrap .filter-minus-btn').click(function () {
                $('#filterPostCodeInput').val("");
                $('#filterPostCodeWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostOwnerWrap .filter-minus-btn').click(function () {
                $('#filterPostOwnerInput').val("");
                $('#filterPostOwnerWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostTitleWrap .filter-minus-btn').click(function () {
                $('#filterPostTitleInput').val("");
                $('#filterPostTitleWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostPriceWrap .filter-minus-btn').click(function () {
                $('#filterPostPriceInput').val("");
                $('#filterPostPriceWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostCommissionWrap .filter-minus-btn').click(function () {
                $('#filterPostPriceInput').val("");
                $('#filterPostCommissionWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterCategoryWrap .filter-minus-btn').click(function () {
                $('#filterCategory1').val("");
                $('#filterCategory2').val("");
                $('#filterCategoryWrap').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterAddressWrap .filter-minus-btn').click(function () {
                $('#filterCity').val("");
                $('#filterDistrict').val("");
                $('#filterWard').val("");
                $('#filterAddressWrap').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterCreatedAtWrap .filter-minus-btn').click(function () {
                $('#filterCreatedAtFromInput').datepicker('setDate', null);
                $('#filterCreatedAtToInput').datepicker('setDate', null);
                $('#filterCreatedAtWrap').hide();
                fnCheckShowBtnAdd();
            });
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterCreatedAtFromInput').datepicker('setDate', dayx);
                $('#filterCreatedAtToInput').datepicker('setDate', dayy);
                $('#filterCreatedAtWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterCreatedAtFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterCreatedAtToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
        (function () {
            var templateJsonCategories1 = <%= templateJsonCategories1 %>;
            var templateJsonCategories2 = <%= templateJsonCategories2 %>;

            var templateDefaultOption = "<option value=''>Chọn</option>";

            var selectHtml1 = "";
            templateJsonCategories1.forEach(function (ele1, pos1, arr1) {
                selectHtml1 += "<option value='" + ele1.id1+"'>" + ele1.name1 + "</option>";
            });
            selectHtml1 = templateDefaultOption + selectHtml1;
            $('#filterCategory1').html(selectHtml1);
            $('#filterCategory2').hide();

            $('#filterCategory1').on("change", function () {
                var selectHtml2 = templateDefaultOption;
                var val1 = $('#filterCategory1').val();   
                var countSubs = 0;
                templateJsonCategories2.forEach(function (ele1, pos1, arr1) {
                    if (ele1.id1 == val1) {
                        countSubs++;
                        selectHtml2 += "<option value='" + ele1.id2 + "'>" + ele1.name2+"</option>"
                    }
                });
                $('#filterCategory2').html(selectHtml2);
                $('#filterCategory2').val("");
                if (countSubs > 0) {
                    $('#filterCategory2').show();
                } else {
                    $('#filterCategory2').hide();
                }
            });
        })();
          (function () {
            var templateDefaultOption = "<option value=''>Chọn</option>";
            var templateJsonOwners = <%= templateJsonOwners %>;
            var selectHtml1 = "";
            templateJsonOwners.forEach(function (ele1, pos1, arr1) {
                selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + " - " + ele1.phone + " - " + ele1.fullname + "</option>";
            });
            selectHtml1 = templateDefaultOption + selectHtml1;
            $('#filterPostOwnerInput').html(selectHtml1);
        })();
          (function () {
              var templateDefaultOption = "<option value=''>Chọn</option>";
            var templateJsonCode = <%= templateJsonCode %>;
              var selectHtml1 = "";
            templateJsonCode.forEach(function (ele1, pos1, arr1) {
                selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
            });
            selectHtml1 = templateDefaultOption + selectHtml1;
            $('#filterPostCodeInput').html(selectHtml1);
        })();
        (function () {
            var defaultCity = "<%= templateFilterCity %>";
            var defaultDistrict = "<%= templateFilterDistrict %>";
            var defaultWard = "<%= templateFilterWard %>";
            var fnInitCities = function () {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLCities";
                var defValue = defaultCity;
                defaultCity = "";
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterCity').html(xmlhttp.responseText);
                        $('#filterCity').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
            var fnInitDistricts = function (cityId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLDistricts";
                var defValue = defaultDistrict;
                defaultDistrict = "";
                if (cityId != undefined && cityId != null && cityId != "") {
                    url += "&CityId=" + cityId;
                } else {
                    cityId = "";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterDistrict').html(xmlhttp.responseText);
                        $('#filterDistrict').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
            var fnInitWards = function (districtId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLWards";
                var defValue = defaultWard;
                defaultWard = "";
                if (districtId != undefined && districtId != null && districtId != "") {
                    url += "&DistrictId=" + districtId;
                } else {
                    districtId = "";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterWard').html(xmlhttp.responseText);
                        $('#filterWard').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
            $('#filterCity').on("change", function () {
                var value = $(this).val();
                fnInitDistricts(value);
            });
            $('#filterDistrict').on("change", function () {
                var value = $(this).val();
                fnInitWards(value);
            });
            fnInitCities();
        })();
        (function () {
            var fnSlashToDash = function (sSlash) {
                var rs = "";
                if (sSlash != undefined && sSlash != null && sSlash != "") {
                    sSlash = sSlash.trim();
                    var parts = sSlash.split("/");
                    if (parts.length == 3) {
                        return parts[2] + "-" + parts[1] + "-" + parts[0];
                    }
                }
                return rs;
            }
            var baseUrl = "/tai-khoan/chia-se";
            $("#btnFilterSubmit").click(function () {
                var filterPostCodeInput = $('#filterPostCodeInput').val();
                var filterPostOwnerInput = $('#filterPostOwnerInput').val();
                var filterPostPriceInput = $('#filterPostPriceInput').val();
                var filterPostCommissionInput = $('#filterPostCommissionInput').val();
                var filterPostTitleInput = $('#filterPostTitleInput').val();
                var filterCreatedAtFromInput = fnSlashToDash($('#filterCreatedAtFromInput').val());
                var filterCreatedAtToInput = fnSlashToDash($('#filterCreatedAtToInput').val());
                var filterCategory1 = $('#filterCategory1').val();
                var filterCategory2 = $('#filterCategory2').val();
                var filterCity = $('#filterCity').val();
                var filterDistrict = $('#filterDistrict').val();
                var filterWard = $('#filterWard').val();
				var txttest = $('#txttest').val();
                var txttesthh = $('#txttesthh').val();
                var txtruler = $('#ruler').val();
                var txtrulerhh = $('#rulerhh').val();
                var url = baseUrl;
                var counterParams = 0;
                if (filterPostCodeInput != undefined && filterPostCodeInput != null && filterPostCodeInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCode=" + filterPostCodeInput;
                }
                if (filterPostOwnerInput != undefined && filterPostOwnerInput != null && filterPostOwnerInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostOwner=" + filterPostOwnerInput;
                }
                if (filterPostTitleInput != undefined && filterPostTitleInput != null && filterPostTitleInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostTitle=" + filterPostTitleInput;
                }
               //Giá                        
                if (txtruler == "Chọn" && txttest == "Chọn"){
                    if (filterPostPriceInput != undefined && filterPostPriceInput != null && filterPostPriceInput != "") {                    
                        counterParams++;
                        counterParams == 1 ? url += "?" : url += "&";
                        url += "FilterPostPrice=" + filterPostPriceInput;
                        url += "&Gia=0";
                    }
                }                                                                       
                
                if (txttest == 'Từ 10 đến 100' && txtruler == '')
                {
                        counterParams++;
                        counterParams == 1 ? url += "?" : url += "&";
                        url += "Gia=10100";
                        url += "&FilterPostPrice=" +0; 
                }

                if (txttest == 'ChọnTừ 10 đến 100' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100";
                }
                if (txttest == 'Từ 100 đến 500' && txtruler == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "Gia=100500";
                    url += "&FilterPostPrice=" +0; 
                }
                if (txttest == 'ChọnTừ 100 đến 500' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=100500";
                }
                if (txttest == 'Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "Gia=500";
                    url += "&FilterPostPrice=" +0; 
                }
                if (txttest == 'ChọnTừ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=500";
                }
                if (txttest == 'Từ 10 đến 100Từ 100 đến 500' && txtruler == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "Gia=10100100500";
                    url += "&FilterPostPrice=" +0; 
                }
                if (txttest == 'ChọnTừ 10 đến 100Từ 100 đến 500' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100100500";
                }
                if (txttest == 'Từ 100 đến 500Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "Gia=100500500"; 
                    url += "&FilterPostPrice=" +0; 
                }
                if (txttest == 'ChọnTừ 100 đến 500Từ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=100500500";
                }
                if (txttest == 'Từ 10 đến 100Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "Gia=10100500";
                    url += "&FilterPostPrice=" +0; 
                }
                if (txttest == 'ChọnTừ 10 đến 100Từ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100500";
                }
                if (txttest == 'Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "Gia=10100100500500";  
                    url += "&FilterPostPrice=" +0; 
                }
                if (txttest == 'ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100100500500";
                }
                //Hoa Hồng               
                if (txtrulerhh == "Chọn" && txttesthh == "Chọn"){
                    if (filterPostCommissionInput != undefined && filterPostCommissionInput != null && filterPostCommissionInput != "") {
                        counterParams++;
                        counterParams == 1 ? url += "?" : url += "&";
                        url += "FilterPostCommission=" + filterPostCommissionInput;
                        url += "&HoaHong=0";
                    }
                }
                if (txttesthh == 'Từ 10 đến 100' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=10100";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 10 đến 100' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100";
                }
                if (txttesthh == 'Từ 100 đến 500' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=100500";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 100 đến 500' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=100500";
                }
                if (txttesthh == 'Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=500";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=500";
                }
                if (txttesthh == 'Từ 10 đến 100Từ 100 đến 500' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=10100100500";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 10 đến 100Từ 100 đến 500' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100100500";
                }
                if (txttesthh == 'Từ 100 đến 500Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=100500500";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 100 đến 500Từ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=100500500"; 
                }
                if (txttesthh == 'Từ 10 đến 100Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=10100500";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 10 đến 100Từ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100500";
                }
                if (txttesthh == 'Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "HoaHong=10100100500500";
                    url += "FilterPostCommission=" +0;
                }
                if (txttesthh == 'ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100100500500";
                }
                if (filterCreatedAtFromInput != undefined && filterCreatedAtFromInput != null && filterCreatedAtFromInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterCreatedAtFrom=" + filterCreatedAtFromInput;
                }
                if (filterCreatedAtToInput != undefined && filterCreatedAtToInput != null && filterCreatedAtToInput != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterCreatedAtTo=" + filterCreatedAtToInput;
                }
                if (filterCategory1 != undefined && filterCategory1 != null && filterCategory1 != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterCategory1=" + filterCategory1;
                }
                if (filterCategory2 != undefined && filterCategory2 != null && filterCategory2 != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterCategory2=" + filterCategory2;
                }
                if (filterCity != undefined && filterCity != null && filterCity != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterCity=" + filterCity;
                }
                if (filterDistrict != undefined && filterDistrict != null && filterDistrict != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterDistrict=" + filterDistrict;
                }
                if (filterWard != undefined && filterWard != null && filterWard != "") {
                    counterParams++;
                    counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterWard=" + filterWard;
                }
                if (counterParams > 0) {
                    window.location.href = url;
                } else {
                    $('.filter-add-btn').click();
                }
            });
            $('#btnFilterCancel').click(function () {
                window.location.href = baseUrl;
            });
        })();
        (function () {
            var initFilterCategory1 = "<%= templateFilterCategory1 %>";
            var initFilterCategory2 = "<%= templateFilterCategory2 %>";
            var initFilterPostOwner = "<%= templateFilterPostOwner %>";

            $('#filterCategory1').val(initFilterCategory1).trigger("change");
            $('#filterCategory2').val(initFilterCategory2).trigger("change");
            $('#filterPostOwnerInput').val(initFilterPostOwner)

            if (initFilterCategory1 != "" || initFilterCategory2 != "") {
                $('#filterCategoryWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }

            if (initFilterPostOwner != "") {
                $('#filterPostOwnerWrap').show();
                $('#filterPostOwnerInput').select2();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }

            var defaultCity = "<%= templateFilterCity %>";
            var defaultDistrict = "<%= templateFilterDistrict %>";
            var defaultWard = "<%= templateFilterWard %>";
            if (defaultCity != "" || defaultDistrict != "" || defaultWard != "") {
                $('#filterAddressWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterPostCode = "<%= templateFilterPostCode %>";
            $('#filterPostCodeInput').val(initFilterPostCode)
            if (initFilterPostCode != "") {
                $('#filterPostCodeWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
                $('#filterPostCodeInput').select2();
            }
            var initFilterPostTitle = "<%= templateFilterPostTitle %>";
            $("#filterPostTitleInput").val(initFilterPostTitle);
            if (initFilterPostTitle != "") {
                $('#filterPostTitleWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterPostPrice = "<%= templateFilterPostPrice %>";
            $("#filterPostPriceInput").val(initFilterPostPrice);
            if (initFilterPostPrice != "") {
                $('#filterPostPriceWrap').show();
               $('#filterPostPriceInput').ionRangeSlider({
                    min: 0,
                    max: 10000000,
                    type: 'double',
                    step: 100000,                        
                    postfix: "",                        
                    hasGrid: true,
                    prettifyFunc: function(num){                            
                        var le = parseInt(num % 1000);
                        var chan = parseInt(num / 1000);
                        var trieu = parseInt(chan / 1000);
                        var tram = chan % 1000;
                        var text = "";
                        if (chan > 1000 && le < 1000 && trieu % 1000) {
                            if (tram == 0)
                            {
                                text += " " + trieu + " Triệu ";
                            }
                            else {
                                text += " " + trieu + " Triệu " + tram + " Nghìn";
                            }                                
                        }
                        else if (trieu / 1000) {
                            text += " " + trieu + " Triệu";
                        }
                        else if (chan < 1000) {
                            text += " " + chan + " Nghìn";
                        }
                        text = text.trim();
                        if (text == "") {
                            text = "0 Nghìn";
                        }
                        return text;
                    },
                });
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterPostCommission = "<%= templateFilterPostCommission %>";
            $("#filterPostCommissionInput").val(initFilterPostCommission);
            if (initFilterPostCommission != "") {
                $('#filterPostCommissionWrap').show();
                $('#filterPostCommissionInput').ionRangeSlider({
                    min: 0,
                    max: 10000000,
                    type: 'double',
                    step: 100000,                        
                    postfix: "",                        
                    hasGrid: true,
                    prettifyFunc: function(num){                            
                        var le = parseInt(num % 1000);
                        var chan = parseInt(num / 1000);
                        var trieu = parseInt(chan / 1000);
                        var tram = chan % 1000;
                        var text = "";
                        if (chan > 1000 && le < 1000 && trieu % 1000) {
                            if (tram == 0)
                            {
                                text += " " + trieu + " Triệu ";
                            }
                            else {
                                text += " " + trieu + " Triệu " + tram + " Nghìn";
                            }                                
                        }
                        else if (trieu / 1000) {
                            text += " " + trieu + " Triệu";
                        }
                        else if (chan < 1000) {
                            text += " " + chan + " Nghìn";
                        }
                        text = text.trim();
                        if (text == "") {
                            text = "0 Nghìn";
                        }
                        return text;
                    },
                });
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
        })();
        (function () {
            $('.user-info-btn').click(function () {
                var dataAccountId = $(this).data("account-id");
                var dataThumbnail = $(this).data("thumbnail");
                var dataFullname = $(this).data("fullname");
                var dataCode = $(this).data("code");
                var dataPhone = $(this).data("phone");
                var dataEmail = $(this).data("email");
                var dataAddr = $(this).data("addr");
                var dataRegisterAt = $(this).data("register-at");
                if (dataAccountId == undefined || dataAccountId == null || dataAccountId == "") {
                    return;
                }
                $('#modalAccountInfo_imgThumbnail').attr("src", (dataThumbnail != "" ? dataThumbnail : "#"));
                $('#modalAccountInfo_spanFullName').text((dataFullname != "" ? dataFullname : "(Chưa có thông tin)"));
                $('#modalAccountInfo_spanCode').text((dataCode != "" ? dataCode : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkPhone').text((dataPhone != "" ? dataPhone : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkPhone').attr("href", (dataPhone != "" ? ("tel:" + dataPhone) : "javascript:void(0);"));
                $('#modalAccountInfo_linkEmail').text((dataEmail != "" ? dataEmail : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkEmail').attr("href", (dataEmail != "" ? "mailto:" + dataEmail : "javascript:void(0);"));
                $('#modalAccountInfo_spanAddr').text((dataAddr != "" ? dataAddr : "(Chưa có thông tin)"));
                $('#modalAccountInfo_spanRegisterAt').text((dataRegisterAt != "" ? dataRegisterAt : "(Chưa có thông tin)"));
                $('#modalAccountInfo').modal();
            });
        })();
    </script>
	 <script>                                            
        var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},
        $checkboxes = $("#checkbox-container :checkbox");

        $checkboxes.on("change", function () {
            $checkboxes.each(function () {
                checkboxValues[this.id] = this.checked;
            });

            sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
        });

        // On page load
        $.each(checkboxValues, function (key, value) {
            $("#" + key).prop('checked', value);
        });

    </script>
     <script>                                            
        var checkboxValues1 = JSON.parse(sessionStorage.getItem('checkboxValues1')) || {},
        $checkboxes1 = $("#checkbox-container1 :checkbox");

        $checkboxes1.on("change", function () {
            $checkboxes1.each(function () {
                checkboxValues1[this.id] = this.checked;
            });

            sessionStorage.setItem("checkboxValues1", JSON.stringify(checkboxValues1));
        });

        // On page load
        $.each(checkboxValues1, function (key, value) {
            $("#" + key).prop('checked', value);
        });

    </script>
</asp:Content>