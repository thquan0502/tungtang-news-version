﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="DangKy-Lan2 - Copy.aspx.cs" Inherits="DangKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <link href="../Css/Page/TinDang.css" rel="stylesheet" /> 
    <link href="../Css/Page/DangKyLan2.css" rel="stylesheet" /> 
    <script src="../Js/Page/DangKyLan2.min.js"></script>
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
        } 
    </style>
    <script>
        window.onload = function () {  
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px;margin-top: 40px;">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <div class="top-job">
                    <div id="vnw-log-in" class="container main-content" style="margin-top:33px;">
                        <input type="hidden" name="" value="0" runat="server" id="DaThayDoi_imgHinhAnh" />
                        <div class="col-md-12">
                            <h5>THÔNG TIN CÁ NHÂN </h5>
                            <hr />
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="avtUser">
                                        <div class="form-group" style="position: relative;">
                                            <asp:FileUpload ID="fuHinhDaiDien" runat="server" accept=".jpg,.jpeg,.png" Style="display: none;" onchange="UploadHinhAnh_Onchange(this,'imgLinkAnh')" />
                                            <img id="imgLinkAnh" runat="server" src="/images/icons/signin.png" class="imgUserEdit" />
                                            <div class="btnShowFileUpLoad" title="Đổi ảnh đại diện" onclick="document.getElementById('ContentPlaceHolder1_fuHinhDaiDien').click();"><i class="fa fa-camera"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <div class="infoUser">
                                        <div class="form-group">
                                            <p style="font-style: italic">(<span style="color: red">*</span>): Thông tin bắt buộc phải nhập</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Họ tên (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtTenCuaHang" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                            <p id="MessageHoTen" style="color: red; font-size: 12px; display: none;">Vui lòng nhập Họ tên</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Email:</b></div>
                                            <input type="text" id="txtEmail" name="form[username]" tabindex="1" class="form-control" runat="server" />
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Địa chỉ:</b></div>
                                            <input type="text" id="txtDiaChi" name="form[username]" required="required" placeholder="Số nhà, tên Đường" tabindex="1" class="form-control" runat="server" />
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Phường(Xã), Quận(Huyện), Thành Phố(Tỉnh) (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtDiaDiem" name="form[username]" placeholder="Chọn" tabindex="1" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDiaDiem();" />
                                            <input type="hidden" id="txtDiaDiemID" runat="server" hidden disabled />
                                            <p id="MessageDiaDiem" style="color: red; font-size: 12px; display: none;">Vui lòng chọn địa điểm</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Số điện thoại (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtSoDienThoai" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                             <p id="MessageSoDienThoai" runat="server" style="color: red; font-size: 12px;"></p>
                                         
                                        <%-- update--%>
                                        <%-- <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Mã số thuế:</b></div>
                                            <input  id="txtMST" name="form[username]"  placeholder="Mã số thuế" tabindex="1" class="form-control" runat="server" />
                                              <div id="MessMST" style="color: red; font-size: 12px; " runat="server"></div>
                                        </div>--%>
                                        <%-- <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Ngày cấp(ngày/tháng/năm):</b></div>
                                            <input id="txtNgayCap"  value="01/01/1990"  type="date"  placeholder="Ngày cấp" tabindex="1" class="form-control" runat="server" />
                                        </div>--%>
                                       <%-- <div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><b>Nơi cấp:</b></div>
                                            <input id="txtNoiCap" name="form[username]"  placeholder="Nơi cấp" tabindex="1" class="form-control" runat="server" />
                                        </div>--%>
                                        <%-- <div class="form-group" id="divPass" runat="server">
                                            <div class="titleinput" style="padding-top: 10px"><b>Mật khẩu:(<span style="color: red">*</span>):</b></div>
                                            <input id="txtMatKhau" name="form[username]"  placeholder="Mật khẩu" tabindex="1" class="form-control" runat="server" />
                                        </div>--%>
                                          <%-- update--%>
                                        <div class="form-group"  style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Tên đăng nhập (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtTenDangNhap" name="form[username]" required="required" tabindex="1" class="form-control" runat="server" />
                                        </div>

                                        <div class="form-group" style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Tỉnh thành (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtTinhThanh" name="name" value="" runat="server" hidden />
                                            <asp:DropDownList ID="ddlTinh" class="form-control" runat="server" disabled="disabled" Style="padding-left: 0; padding-right: 0;"></asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Quận huyện (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" hidden />
                                            <asp:DropDownList data-search-input-placeholder="Tìm kiếm quận huyện" data-placeholder="Tất cả" class="form-control" ID="slHuyen" name="industry[]" runat="server" disabled="disabled" Style="padding-left: 0; padding-right: 0;">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="display: none;">
                                            <div class="titleinput" style="padding-top: 10px"><b>Phường xã (<span style="color: red">*</span>):</b></div>
                                            <input type="text" id="txtPhuongXa" name="name" value="" runat="server" hidden />
                                            <select data-search-input-placeholder="Tìm kiếm phường xã" data-placeholder="Tất cả" class="form-control" id="slXa" name="industry[]" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0;">
                                            </select>
                                        </div>
                                        <%--<div class="form-group">
                                            <div class="titleinput" style="padding-top: 10px"><a href="/doi-mat-khau/dmk" >Đổi mật khẩu</a></div> 
                                        </div>--%>
                                        <!-- Buttons-->
                                        <div class="form-group" style="margin-top: 20px">
                                            <a class="btn btn-primary btn-block" onclick="LuuThongTin()">LƯU THAY ĐỔI</a>
                                            <asp:LinkButton ID="btnDangKy" runat="server" class="btn btn-primary btn-block" OnClick="btnDangKy_Click" Style="display: none;">LƯU THAY ĐỔI</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </section>
	<script>
            function alertTT(){
                alertify.success("Cập nhật thông tin thành công!");                                                 
            }
            function alertSDT() {
                alertify.error("Bạn cần cập nhật thông tin số điện thoại!");
            }
    </script>
    <script type="text/javascript">
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        function UploadHinhAnh_Onchange(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                $("#ContentPlaceHolder1_DaThayDoi_imgHinhAnh").val('1');
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

   <%--  <script>
         $('##ContentPlaceHolder1_txtNgayCap').datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                });
            </script>--%>
</asp:Content>
