﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DangTin.aspx.cs" Inherits="DangTin" %>

<%@ Register Namespace="CKEditor.NET" Assembly="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Namespace="CKFinder" Assembly="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
            box-shadow: none;
        }

        #ContentPlaceHolder1_txtLoaiDanhMuc[readonly] {
            background-color: white;
            box-shadow: none;
        }
    </style>
    <script>
        window.onload = function () {
            if (document.getElementById('ContentPlaceHolder1_hdHinhAnh').value != "")
                window.scrollTo(0, document.body.scrollHeight);
        }
        $(function () {
            CKEDITOR.config.extraPlugins = 'justify';
        });
        function format_curency(a) {
            var money = a.value.replace(/\./g, "");
            a.value = money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
        function XoaHinhAnh(id, TenAnh) {
            document.getElementById(id).style.display = "none";
            document.getElementById("ContentPlaceHolder1_hdHinhAnh").value = document.getElementById("ContentPlaceHolder1_hdHinhAnh").value.replace(TenAnh + "|~~~~|", "");
        }
        function LayThongTinDangKy() {
            var ckbLayThongTinDangKy = document.getElementById("ckbLayThongTinDangKy").checked;
            //alert(ckbLayThongTinDangKy);
            if (ckbLayThongTinDangKy == true) {
                //alert('aaa');
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText != "") {
                            var arrThanhVien = xmlhttp.responseText.split("|~~|");
                            document.getElementById("ContentPlaceHolder1_txtHoTen").value = arrThanhVien[0];
                            document.getElementById("ContentPlaceHolder1_txtSoDienThoai").value = arrThanhVien[1];
                            document.getElementById("ContentPlaceHolder1_txtEmail").value = arrThanhVien[2];
                            document.getElementById("ContentPlaceHolder1_txtDiaChi").value = arrThanhVien[3];
                        }
                        else
                            alert("Lỗi !")
                    }
                }
                xmlhttp.open("GET", "http://shopfull.asia/Ajax.aspx?Action=LayThongTinDangKy", true);
                xmlhttp.send();
            }
            else {
                document.getElementById("ContentPlaceHolder1_txtHoTen").value = "";
                document.getElementById("ContentPlaceHolder1_txtSoDienThoai").value = "";
                document.getElementById("ContentPlaceHolder1_txtEmail").value = "";
                document.getElementById("ContentPlaceHolder1_txtDiaChi").value = "";

            }
        }
        function ShowDanhMuc(type) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        if (type != "GoBack") {
                            $("#modalDanhMuc").modal({
                                fadeDuration: 200,
                                showClose: false
                            });
                            $("header").css({ "z-index": "0" });
                            menuBar_fixed = false;
                        }
                        $("#btnBackDanhMuc").css("display", "none");
                        $("#tbDanhMuc_tbody").html(xmlhttp.responseText);
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadDanhMuc&Type=ALL", true);
            xmlhttp.send();
        }

        function ShowDiaDiem(type) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        if (type != "GoBack") {
                            $("#modalDiaDiem").modal({
                                fadeDuration: 200,
                                showClose: false
                            });
                            $("header").css({ "z-index": "0" });
                            menuBar_fixed = false;
                        }
                        $("#btnBackKhuVuc").css("display", "none");
                        $("#tbDiaDiem_tbody").html(xmlhttp.responseText);
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadTinhThanh&Type=ALL", true);
            xmlhttp.send();
        }
        function LoadQuanHuyen(TT, TTT) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        $("#tbDiaDiem_tbody").html(xmlhttp.responseText);
                        $("#btnBackKhuVuc").css("display", "block");
                        $("#btnBackKhuVuc").attr("onclick", "ShowDiaDiem('GoBack');");
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadQuanHuyen&Type=ALL&&TT=" + TT + "&TTT=" + TTT, true);
            xmlhttp.send();
        }
        function LoadPhuongXa(QH, TT, TQH, TTT) {

            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        $("#tbDiaDiem_tbody").html(xmlhttp.responseText);
                        $("#btnBackKhuVuc").css("display", "block");
                        $("#btnBackKhuVuc").attr("onclick", "LoadQuanHuyen('" + TT + "','" + TTT + "');");
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadPhuongXa&Type=ALL&&QH=" + QH + "&TT=" + TT + "&TTT=" + TTT + "&TQH=" + TQH, true);
            xmlhttp.send();
        }
        function ChonPhuongXa(PX, TQX, TT, QH, TTT, TQH) {
            $("#ContentPlaceHolder1_ddlTinh").html("<option value='" + TT + "'>" + TTT + "</option>");
            $("#ContentPlaceHolder1_ddlTinh").val(TT);
            $("#ContentPlaceHolder1_txtTinhThanh").val(TT);
            if (QH != "") {
                $("#ContentPlaceHolder1_slHuyen").html("<option value='" + QH + "'>" + TQH + "</option>");
                $("#ContentPlaceHolder1_slHuyen").val(QH);
                $("#ContentPlaceHolder1_txtQuanHuyen").val(QH);
            }
            else {
                $("#ContentPlaceHolder1_slHuyen").html("<option value=''></option>");
                $("#ContentPlaceHolder1_slHuyen").val('');
                $("#ContentPlaceHolder1_txtQuanHuyen").val('');
            }

            if (PX != "") {
                $("#ContentPlaceHolder1_slXa").html("<option value='" + PX + "'>" + TQX + "</option>");
                $("#ContentPlaceHolder1_slXa").val(PX);
                $("#ContentPlaceHolder1_txtPhuongXa").val(PX);
            }
            else {
                $("#ContentPlaceHolder1_slXa").html("<option value=''></option>");
                $("#ContentPlaceHolder1_slXa").val('');
                $("#ContentPlaceHolder1_txtPhuongXa").val('');
            }

            $.modal.close();
            var TinhThanh = $("#ContentPlaceHolder1_ddlTinh option:selected").text();
            var QuanHuyen = $("#ContentPlaceHolder1_slHuyen option:selected").text();
            var PhuongXa = $("#ContentPlaceHolder1_slXa option:selected").text();
            var chuoi = "";
            if (PhuongXa != "")
                chuoi += PhuongXa + ", ";
            if (QuanHuyen != "")
                chuoi += QuanHuyen + ", ";
            if (TinhThanh != "")
                chuoi += TinhThanh;
            $("#ContentPlaceHolder1_txtDiaDiem").val(chuoi);

        }
        function LoadDanhMucCap2(C1, TC1) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        $("#tbDanhMuc_tbody").html(xmlhttp.responseText);
                        $("#btnBackDanhMuc").css("display", "block");
                        $("#btnBackDanhMuc").attr("onclick", "ShowDanhMuc('GoBack');");
                    }
                    else {
                        alert("Lỗi !");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadDanhMucCap2&Type=ALL&&C1=" + C1 + "&TC1=" + TC1, true);
            xmlhttp.send();
        }
        function ChonDanhMuc(MDM, DM, C2, TC2) {
            $("#ContentPlaceHolder1_slLoaiDanhMuc").val(MDM);
            $("#ContentPlaceHolder1_slLoaiDanhMucCap2").val(C2);
            var chuoi = DM;
            if (TC2 != "")
                chuoi += "/" + TC2
            $("#ContentPlaceHolder1_txtLoaiDanhMuc").val(chuoi);
            $.modal.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px; overflow: auto">

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal-DiaDiem-cls">
            <div id="modalDanhMuc" class="modal" style="overflow: hidden;">
                <div id="dvDSDanhMuc" style="overflow: auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackDanhMuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn danh mục</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDanhMuc_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="job-list scrollbar m-t-lg">
            <h1 class="text-center">ĐĂNG TIN</h1>
            <div>
                <div id="dvThongBao" runat="server" class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="background: #eea751; padding: 3px 6px; border-radius: 3px; color: white; border: 1px solid #FF9800;">
                        Bạn nên đăng nhập để quản lý được tin đăng
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Danh mục<span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtLoaiDanhMuc" value="Chọn danh mục" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDanhMuc()" />
                                    <input id="slLoaiDanhMuc" runat="server" style="padding-left: 0; padding-right: 0; display: none;" value="0" />
                                    <input id="slLoaiDanhMucCap2" runat="server" style="padding-left: 0; padding-right: 0; display: none;" value="0" />
                                    <div id="dvLinhVuc" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Khu vực:
                                </td>
                                <td>
                                    <select id="ddlTinh" runat="server" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtTinhThanh" name="name" value="0" runat="server" style="display: none;" />
                                    <select id="slHuyen" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" style="display: none;" />
                                    <select id="slXa" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtPhuongXa" name="name" value="" runat="server" style="display: none;" />

                                    <input type="text" id="txtDiaDiem" value="Toàn quốc" tabindex="1" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDiaDiem();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Giá:
                                </td>
                                <td>
                                    <input type="text" id="txtTuGia" placeholder="VNĐ" class="form-control" runat="server" oninput='DinhDangTien(this.id)' onkeypress='onlyNumber(event)' />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none;">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd"></td>
                                <td class="nametd">
                                    <label for="ContentPlaceHolder1_rdGiaoHangMatPhi">Giao hàng mất phí</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="rdGiaoHangMatPhi" runat="server" type="radio" checked name="GiaoHang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_rdGiaoHangKhongMatPhi">Giao hàng không mất phí</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="rdGiaoHangKhongMatPhi" runat="server" type="radio" name="GiaoHang" style="margin-top: 3px;" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd"></td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_radCanBan">Bạn đang cần bán</label>  
                                </td>
                                <td style="width: 50px;">
                                    <input id="radCanBan" runat="server" type="radio" checked name="LoaiTinDang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_radCanMua">Bạn đang cần mua</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="radCanMua" runat="server" type="radio" name="LoaiTinDang" style="margin-top: 3px;" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">
                            THÔNG TIN LIÊN HỆ
                        </div>
                        <div>
                            <table style="width: 100%; margin-top: 10px;">
                                <tr>
                                    <td class="nametd" style="padding-top: 4px;">
                                        <input type="checkbox" id="ckbLayThongTinDangKy" onclick="LayThongTinDangKy()" />
                                    </td>
                                    <td>Lấy thông tin đăng ký
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Họ tên <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtHoTen" name="form[password]" placeholder="Họ tên" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvHoTen" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Số điện thoại <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtSoDienThoai" name="form[password]" placeholder="Số điện thoại" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvSoDienThoai" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Email:
                                </td>
                                <td>
                                    <input type="text" id="txtEmail" name="form[password]" placeholder="Email" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvEmail" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Địa chỉ:
                                </td>
                                <td>
                                    <input type="text" id="txtDiaChi" name="form[password]" placeholder="Số nhà, Đường, Phường - Quận huyện - Tỉnh thành" tabindex="2" class="form-control" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">NỘI DUNG TIN ĐĂNG</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Tiêu đề <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtTieuDe" name="form[password]" placeholder="Tiêu đề" class="form-control" runat="server" />
                                    <div id="dvTieuDe" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="height: auto">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="vertical-align: baseline;">Nội dung <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <%--  <CKEditor:CKEditorControl  Height="300" runat="server"></CKEditor:CKEditorControl> 
                                         <CKFinder:FileBrowser ID="FileBrowser1"   Width    ="0" Height="0" runat="server" OnLoad="FileBrowser1_Load"></CKFinder:FileBrowser>--%>

                                    <textarea runat="server" id="txtNoiDung" style="height: 200px; resize: none;"> </textarea>
                                    <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvConHinhAnh" class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Thêm hình ảnh:
                                </td>
                                <td>
                                    <div class="BgUploadAnh">
                                        <div>
                                            <a class="btn btn-info btn-sm" onclick="javascript:document.getElementById('fileHinhAnh').click();">THÊM HÌNH</a>
                                            <asp:FileUpload ID="fileHinhAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" AllowMultiple="true" Style="display: none;" />
                                        </div>
                                        <div style="padding: 0px 0px; height: auto; overflow: hidden;" id="dvHinhAnh" runat="server">
                                            <%--<div class="imgupload">
                                                   <p style="margin:0px"><img id="imgAnhCuaBan" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                   <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                               </div>
                                               <div class="imgupload">
                                                   <p style="margin:0px"><img id="img1" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                   <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                               </div>--%>
                                        </div>
                                    </div>
                                    <span style="color: red">(*) Hình đầu tiên sẽ là hình đại diện của hàng hoá và bắt buộc đăng ít nhất 3 hình</span>
                                    <div id="dvMessageHinhAnh" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- Buttons-->
                <div class="form-group">
                    <div style="text-align: center">
                        <asp:Button ID="btDangTin" runat="server" Text="ĐĂNG TIN" class="btn btn-primary btn-block" Style="width: 110px; padding: 10px; margin: auto;" OnClick="btDangTin_Click" />
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" id="hdHinhAnh" runat="server" />
    </section>
    <script type="text/javascript">
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
</asp:Content>

