﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;
using System.IO;

public partial class TaoNganHang : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string name = inputName.Text.Trim();
        string code = inputCode.Text.Trim();
        string accountName = inputAccountName.Text.Trim();
        string accountNumber = inputAccountNumber.Text.Trim();
        bool active = inputActive.Checked;
        if(string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(accountName) || string.IsNullOrWhiteSpace(accountNumber))
        {
            return;
        }

        string image = this.getImage(code);
        if(string.IsNullOrWhiteSpace(image))
        {
            return;
        }

        string sql = @"
            INSERT INTO Banks (
                Name,
                Code,
                Active, 
                Image,
                CreatedAt,
                UpdatedAt,
                AccountName,
                AccountNumber
            ) OUTPUT INSERTED.Id VALUES (
	            '" + name + @"',
	            '" + code + @"',
	            " + (active ? "1"  : "0") + @",
	            '" + image + @"',
	            '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
	            '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
                '" + accountName + @"',
                '" + accountNumber + @"'
            );
        ";
        DataTable table = Connect.GetTable(sql);

        string id = table.Rows[0]["Id"].ToString();

        Response.Redirect("/Admin/QuanLyTaiChinh/SuaNganHang.aspx?nganHang=" + id);
    }

    protected string getImage(string mainName)
    {
        if (inputImage.PostedFile != null)
        {
            for (int j = 0; j < Request.Files.Count; j++)
            {
                    string extension = Path.GetExtension(inputImage.PostedFile.FileName);
                    if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".PNG")
                    {
                        if (inputImage.HasFile)
                        {
                            string Ngay = DateTime.Now.Day.ToString();
                            string Thang = DateTime.Now.Month.ToString();
                            string Nam = DateTime.Now.Year.ToString();
                            string Gio = DateTime.Now.Hour.ToString();
                            string Phut = DateTime.Now.Minute.ToString();
                            string Giay = DateTime.Now.Second.ToString();
                            string Khac = DateTime.Now.Ticks.ToString();
                            string fExtension = Path.GetExtension(inputImage.PostedFile.FileName);

                            string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + mainName + fExtension;
                            string FilePath = "/Images/banks/" + FileName;
                            inputImage.PostedFile.SaveAs(Server.MapPath("~"+ FilePath));
                            return FilePath;
                        }
                    }
                }
        }
        return null;
    }
}

