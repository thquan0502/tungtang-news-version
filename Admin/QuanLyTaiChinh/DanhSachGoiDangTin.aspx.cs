﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class DanhSachGoiDangTin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }
        if (!IsPostBack)
        {
            this.loadMainTable();
        }
    }

    protected void loadMainTable()
    {
        string html = "";
        DataTable table = Services.PostFees.getAll();
        html = @"
            <table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                <thead >
                    <tr>
                        <th class='th'>Mã gói</th>
                        <th class='th'>Tên gói</th>
                        <th class='th'>Giá 1 ngày</th>
                        <th class='th'>Giá 7 ngày</th>
                        <th class='th'>Giá 14 ngày</th>
                        <th class='th'>Giá 30 ngày</th>
                        <th class='th '>Hành động</th>
                    </tr>
                </thead>
                <tbody>
        ";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += @"<tr>";
            // Code
            html += "<td class='td'>" + table.Rows[i]["Code"].ToString() + "</td>";
            // Name
            html += "<td class='td'>" + table.Rows[i]["Name"].ToString() + "</td>";
            // PricePer1Days
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["PricePer1Days"].ToString()) + "</td>";
            // PricePer1Days
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["PricePer7Days"].ToString()) + "</td>";
            // PricePer1Days
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["PricePer14Days"].ToString()) + "</td>";
            // PricePer1Days
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["PricePer30Days"].ToString()) + "</td>";
            // Action
            html += @"
                <td class='td'>
                    <a href='/Admin/QuanLyTaiChinh/SuaGoiDangTin.aspx?packageId=" + table.Rows[i]["Id"].ToString() + @"' class='btn btn-primary'>Sửa</a>
                </td>
            ";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
    }
}