﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;
using System.IO;

public partial class SuaGoiNap : System.Web.UI.Page
{
    protected DataRow pack = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }

        string packId = Request.QueryString["packageId"];
        DataRow pack = Services.PostFees.find(packId);

        if (pack == null)
        {
            Utilities.Redirect.notFound();
            return;
        }
        else
        {
            this.pack = pack;
        }

        if (!IsPostBack)
        {
            inputName.Text = this.pack["Name"].ToString();
            inputCode.Text = this.pack["Code"].ToString();
            inputPricePer1Days.Text = this.pack["PricePer1Days"].ToString();
            inputPricePer7Days.Text = this.pack["PricePer7Days"].ToString();
            inputPricePer14Days.Text = this.pack["PricePer14Days"].ToString();
            inputPricePer30Days.Text = this.pack["PricePer30Days"].ToString();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if(this.pack == null)
        {
            Utilities.Redirect.notFound();
            return;
        }

        string id = this.pack["Id"].ToString();

        string name = inputName.Text.Trim();
        string pricePer1DaysString = inputPricePer1Days.Text.Trim();
        long pricePer1Days = long.Parse(pricePer1DaysString);
        string pricePer7DaysString = inputPricePer7Days.Text.Trim();
        long pricePer7Days = long.Parse(pricePer7DaysString);
        string pricePer14DaysString = inputPricePer14Days.Text.Trim();
        long pricePer14Days = long.Parse(pricePer14DaysString);
        string pricePer30DaysString = inputPricePer30Days.Text.Trim();
        long pricePer30Days = long.Parse(pricePer30DaysString);

        if (string.IsNullOrWhiteSpace(name))
        {
            return;
        }

        Services.PostFees.update(id, name, pricePer1Days, pricePer7Days, pricePer14Days, pricePer30Days);

        Response.Redirect("/Admin/QuanLyTaiChinh/SuaGoiDangTin.aspx?packageId=" + id);
    }
}

