﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;
using System.IO;

public partial class SuaNganHang : System.Web.UI.Page
{
    protected DataRow bank = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }

        string bankId = Request.QueryString["nganHang"];
        DataRow bank = Services.Banks.find(bankId);

        if (bank == null)
        {
            Utilities.Redirect.notFound();
            return;
        }
        else
        {
            this.bank = bank;
        }

        if (!IsPostBack)
        {
            inputName.Text = this.bank["Name"].ToString();
            inputCode.Text = this.bank["Code"].ToString();
            inputAccountName.Text = this.bank["AccountName"].ToString();
            inputAccountNumber.Text = this.bank["AccountNumber"].ToString();
            inputActive.Checked = this.bank["Active"].ToString() == "True";
            oldImage.Src = this.bank["Image"].ToString();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if(this.bank == null)
        {
            Utilities.Redirect.notFound();
            return;
        }

        string id = this.bank["Id"].ToString();

        string name = inputName.Text.Trim();
        string code = inputCode.Text.Trim();
        string accountName = inputAccountName.Text.Trim();
        string accountNumber = inputAccountNumber.Text.Trim();
        bool active = inputActive.Checked;
        if(string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(accountName) || string.IsNullOrWhiteSpace(accountNumber))
        {
            return;
        }

        string image = this.getImage(code);

        string currentImage = this.bank["Image"].ToString();

        string sql = @"
            UPDATE Banks SET
               Name = '" + name + @"',
               Code = '" + code + @"',
               Active = " + (active ? "1" : "0") + @",
               Image = '" + (string.IsNullOrWhiteSpace(image) ? currentImage : image) + @"',
               UpdatedAt = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + @"',
               AccountName = '" +accountName + @"',
               AccountNumber = '" + accountNumber + @"'
            WHERE 
                Id = '" + id +@"'
        ";
        Connect.Exec(sql);


        Utilities.IO.removeFile(currentImage);

        Response.Redirect("/Admin/QuanLyTaiChinh/SuaNganHang.aspx?nganHang=" + id);
    }

    protected string getImage(string mainName)
    {
        if (inputImage.PostedFile != null)
        {
            for (int j = 0; j < Request.Files.Count; j++)
            {
                string extension = Path.GetExtension(inputImage.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".PNG")
                {
                    if (inputImage.HasFile)
                    {
                        string Ngay = DateTime.Now.Day.ToString();
                        string Thang = DateTime.Now.Month.ToString();
                        string Nam = DateTime.Now.Year.ToString();
                        string Gio = DateTime.Now.Hour.ToString();
                        string Phut = DateTime.Now.Minute.ToString();
                        string Giay = DateTime.Now.Second.ToString();
                        string Khac = DateTime.Now.Ticks.ToString();
                        string fExtension = Path.GetExtension(inputImage.PostedFile.FileName);

                        string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + mainName + fExtension;
                        string FilePath = "/Images/banks/" + FileName;
                        inputImage.PostedFile.SaveAs(Server.MapPath("~"+ FilePath));
                        return FilePath;
                    }
                }
            }
        }
        return null;
    }
}

