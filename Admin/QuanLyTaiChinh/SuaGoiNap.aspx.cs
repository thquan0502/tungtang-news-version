﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;
using System.IO;

public partial class SuaGoiNap : System.Web.UI.Page
{
    protected DataRow pack = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }

        string packId = Request.QueryString["packageId"];
        DataRow pack = Services.RechargePackages.find(packId);

        if (pack == null)
        {
            Utilities.Redirect.notFound();
            return;
        }
        else
        {
            this.pack = pack;
        }

        if (!IsPostBack)
        {
            inputName.Text = this.pack["Name"].ToString();
            inputCode.Text = this.pack["Code"].ToString();
            inputRechargePrice.Text = this.pack["RechargePrice"].ToString();
            inputActualAmount.Text = this.pack["ActualAmount"].ToString();
            inputBackgroundColor.Text = this.pack["BackgroundColor"].ToString();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if(this.pack == null)
        {
            Utilities.Redirect.notFound();
            return;
        }

        string id = this.pack["Id"].ToString();

        string name = inputName.Text.Trim();
        string rechargePrice = inputRechargePrice.Text.Trim();
        string actualAmount = inputActualAmount.Text.Trim();
        string backgroundColor = inputBackgroundColor.Text.Trim();
        if(string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(rechargePrice) || string.IsNullOrWhiteSpace(actualAmount) || string.IsNullOrWhiteSpace(backgroundColor))
        {
            return;
        }

        Services.RechargePackages.update(id, name, rechargePrice, actualAmount, backgroundColor);

        Response.Redirect("/Admin/QuanLyTaiChinh/SuaGoiNap.aspx?packageId=" + id);
    }
}

