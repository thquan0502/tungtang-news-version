﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DanhSachNganHang.aspx.cs" Inherits="DanhSachNganHang" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Danh sách ngân hàng</span>
                </h1>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="/Admin/QuanLyTaiChinh/TaoNganHang.aspx" class="btn btn-primary">Tạo mới</a>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                </div>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
                <div id="divPagination1" class="app-table-pagination" style="text-align:right;" runat="server"></div>
            </section>
        </div>
        <asp:TextBox ID="inputDeleteId" runat="server" type="hidden" />
        <asp:Button ID="btnDelete" runat="server" style="display: none;" OnClick="btnDelete_Click" />
    </form>
    <script>
        $(document).ready(function () {
            $(".btn-delete").on("click", function () {
                var id = $(this).data("id");
                if (!id) {
                    return;
                }
                $("#ContentPlaceHolder1_inputDeleteId").val(id);
                $("#ContentPlaceHolder1_btnDelete").trigger("click");
            });
        });
    </script>
</asp:Content>
