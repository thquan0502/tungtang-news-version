﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DanhSachGoiDangTin.aspx.cs" Inherits="DanhSachGoiDangTin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1 class="title">
                    <span>Danh sách gói đăng tin</span>
                </h1>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
            </section>
        </div>
    </form>
</asp:Content>
