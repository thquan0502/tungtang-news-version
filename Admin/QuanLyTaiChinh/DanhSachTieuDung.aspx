﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DanhSachTieuDung.aspx.cs" Inherits="DanhSachTieuDung" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Danh sách tiêu dùng</span>
                </h1>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
                <div id="divPagination1" class="app-table-pagination" style="text-align:right;" runat="server"></div>
            </section>
        </div>
    </form>
</asp:Content>
