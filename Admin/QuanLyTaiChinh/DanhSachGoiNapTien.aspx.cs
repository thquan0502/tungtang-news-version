﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class DanhSachGoiNapTien : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }
        if (!IsPostBack)
        {
            this.loadMainTable();
        }
    }

    protected void loadMainTable()
    {
        string html = "";
        DataTable table = Services.RechargePackages.getAll();
        html = @"
            <table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                <thead >
                    <tr>
                        <th class='th'>Mã gói</th>
                        <th class='th'>Tên gói</th>
                        <th class='th'>Số tiền nạp</th>
                        <th class='th'>Số tiền thực nhận</th>
                        <th class='th'>Màu nền</th>
                        <th class='th '>Hành động</th>
                    </tr>
                </thead>
                <tbody>
        ";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            string backgroundColor = table.Rows[i]["BackgroundColor"].ToString();
            html += @"<tr>";
            // code
            html += "<td class='td'>" + table.Rows[i]["Code"].ToString() + "</td>";
            // name
            html += "<td class='td'>" + table.Rows[i]["Name"].ToString() + "</td>";
            // RechargePrice
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["RechargePrice"].ToString()) + "</td>";
            // ActualAmount
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["ActualAmount"].ToString()) + "</td>";
            // BackgroundColor
            html += "<td class='td'>" + backgroundColor + "<div class='preview-background-color' style='background-color: " + backgroundColor + "'></div></td>";
            // action
            html += @"
                <td class='td'>
                    <a href='/Admin/QuanLyTaiChinh/SuaGoiNap.aspx?packageId=" + table.Rows[i]["Id"].ToString() + @"' class='btn btn-primary'>Sửa</a>
                </td>
            ";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
    }
}