﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class DanhSachThanhVien : System.Web.UI.Page
{
    protected string sSoDienThoai = null;
    //  string sSoDienThoai = "";
    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;

    protected string templateFilterPostOwner = "";
    protected string filterPostOwner = null;

    protected string templateJsonOwners = "";
    protected string templateJsonCode4 = "";

    protected string rechargeSuccessMessage = "";

    protected void checkRechargeSuccess()
    {
        HttpCookie c = Request.Cookies.Get("recharge_transfer_success");
        if (c != null)
        {
            string idHistory = c.Value;
            long id;
            if (long.TryParse(idHistory, out id))
            {
                DataRow row = Services.BalanceHistory.find(id);
                long amount = row.Field<long>("Amount");

                DataRow acc = Services.Account.getAccount("" + row.Field<long>("AccountId"));

                if(acc == null)
                {
                    Utilities.Redirect.notFound();
                }

                this.rechargeSuccessMessage = "Dã nạp " + Utilities.Formatter.toCurrencyString(amount) + " cho " + acc.Field<string>("SoDienThoai") + " thành công !";
            }
            Response.Cookies["recharge_transfer_success"].Expires = DateTime.Now.AddSeconds(-1);
        }
        else
        {
            this.rechargeSuccessMessage = "";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }
        if (!IsPostBack)
        {
            this.checkRechargeSuccess();
            //Load_SelectSDT("select 'TT' +(CAST(idThanhVien as varchar(100)) + ' - ' + SoDienThoai + ' - ' + TenCuaHang) as IdTV,SoDienThoai,idThanhVien from tb_ThanhVien where TenCuaHang != '' order by NgayDangKy desc", "IdTV", "idThanhVien", true, "-- Tất cả --", SlSdt);

            try
            {
                sSoDienThoai = Request.QueryString["SoDienThoai"].Trim();
                // SlSdt.Value = sSoDienThoai;
            }
            catch { }
            generateFilterInputs();
            this.loadMainTable();
            this.loadOwners();
           
        }
    }


    protected void loadOwners()
    {
        string sql1 = @"
          select 'TT' +(CAST(idThanhVien as varchar(100))) as IdTV,SoDienThoai,idThanhVien,TenCuaHang from tb_ThanhVien where TenCuaHang != '' order by NgayDangKy desc
         ";
        DataTable table1 = Connect.GetTable(sql1);

        this.templateJsonOwners = "[";
        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            this.templateJsonOwners += @"{id:'" + table1.Rows[i]["idThanhVien"].ToString() + @"', code:'" + table1.Rows[i]["IdTV"].ToString() + @"', phone:'" + table1.Rows[i]["SoDienThoai"].ToString() + @"', fullname: '" + table1.Rows[i]["TenCuaHang"].ToString() + @"'},";
        }
        this.templateJsonOwners += "]";
    }

    protected void loadMainTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 15;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = new Dictionary<string, string>();
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();
        if (Request.QueryString["Page"] != null)
        {


            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 += @"
         FROM (
                SELECT 
                    ROW_NUMBER() OVER ( 
                        ORDER BY tb_ThanhVien.idThanhVien DESC
                    ) AS RowNumber,
	                ISNULL(tb_ThanhVien.idThanhVien, '') AS AccountID,
					tb_ThanhVien.TenCuaHang as AccountName,
	                ISNULL(tb_ThanhVien.SoDienThoai, '') AS AccountPhone,
	                ISNULL(tb_ThanhVien.BalanceRecharge,'') AS BalanceRecharge,
	                ISNULL(tb_ThanhVien.BalanceSpent, 0) AS BalanceSpent,
					ISNULL(tb_ThanhVien.BalanceAmount,0) AS BalanceAmount
                FROM tb_ThanhVien
                WHERE 1 = 1
        ";
        if (!string.IsNullOrWhiteSpace(filterPostOwner))
        {
            sql1 += " AND tb_ThanhVien.idThanhVien = " + filterPostOwner + " ";
            postParams.Add("FilterPostOwner", filterPostOwner);
        }
        sql1 += @"
            ) TBWrap
        ";

        sql2 += " SELECT COUNT(*) AS TOTALROWS " + sql1;
        sql3 += @" SELECT * " + sql1 + @" WHERE RowNumber BETWEEN " + fromIndex + @" AND " + toIndex + @" ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TOTALROWS"].ToString());

        DataTable table3 = Connect.GetTable(sql3);

        html = @"
            <table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                <thead >
                    <tr>
                        <th class='th'>Mã TV</th>
                        <th class='th'>Tên Thành Viên</th>
                        <th class='th'>Tên Tài khoản</th>
                        <th class='th'>Tổng Nạp</th>
                        <th class='th'>Đã Chi Tiêu</th>
                        <th class='th'>Số Dư</th>
                        <th class='th'>Hành Động</th>
                    </tr>
                </thead>
                <tbody>
        ";
        if (table3.Rows.Count == 0)
        {
            html += @"
                <tr>
                    <td colspan='4'><p class='text-center'><strong>Không có kết quả !</strong></p></td>
                </tr>
            ";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            html += @"<tr>";
            // Mã TV
            html += "<td class='td'>" + "TT" + table3.Rows[i]["AccountID"].ToString() + " </td>";
            // Tên Thành Viên
            html += "<td class='td'>" + table3.Rows[i]["AccountName"].ToString() + " </td>";
            //Tên Đăng Nhập
            html += "<td class='td'>" + table3.Rows[i]["AccountPhone"].ToString() + " </td>";
            // Tổng Nạp
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString((long)table3.Rows[i]["BalanceRecharge"]) + "</td>";
            // Đã Chi Tiêu
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString((long)table3.Rows[i]["BalanceSpent"]) + "</td>";
            // Số Dư
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString((long)table3.Rows[i]["BalanceAmount"]) + "</td>";
            // action
            html += @"
                <td class='td'>
                    <a href='/Admin/QuanLyTaiChinh/CongTien.aspx?userid=" + table3.Rows[i]["AccountID"].ToString() + @"' class='btn btn-primary'>Cộng Tiền</a>
                </td>
            ";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
        if (totalItems > perPage)
        {
            htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "/Admin/QuanLyTaiChinh/DanhSachThanhVien.aspx", postParams);
            divPagination1.InnerHtml = htmlPagination;
        }
        else
        {
            divPagination1.Visible = false;
        }
    }
    protected void generateFilterInputs()
    {
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostOwner")))
        {
            string filterPostOwner = Request.QueryString.Get("FilterPostOwner").Trim();
            this.filterPostOwner = filterPostOwner;
            this.templateFilterPostOwner = filterPostOwner;
        }
    }
}