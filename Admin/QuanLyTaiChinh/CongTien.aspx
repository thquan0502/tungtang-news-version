﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="CongTien.aspx.cs" Inherits="CongTien" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .active-label {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .active-label input {
            display: inline-block;
            margin: 0;
            margin-right: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Cộng tiền thủ công</span>
                </h1>
                <div class="container mt-5">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-lg-6">
                            <label>Mã Thành Viên</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputCode" runat="server" cssClass="form-control" ReadOnly />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Tên Tài Khoản</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputName" runat="server" cssClass="form-control" ReadOnly />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Số Điện Thoại</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputAccountPhone" runat="server" cssClass="form-control" ReadOnly />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Số tiền cộng</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputBalance" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                         <div class="col-xs-12 col-lg-6">
                            <label>Ghi Chú</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputNote" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-5">
                             <div class="form-group">
                            <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSubmit_Click" />
                                 </div>
                        </div>
                        <div class="col-xs-12 col-lg-5">
                             <div class="form-group">
                            <asp:Button ID="btnCancel" runat="server" class="btn btn-primary" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</asp:Content>
