﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class DanhSachNganHang : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }
        if (IsPostBack)
        {
            
        }
        else
        {
            this.loadMainTable();
        }
    }

    protected void loadMainTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 15;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = new Dictionary<string, string>();

        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 += @"
            FROM (
	            SELECT 
                   ROW_NUMBER() OVER ( 
                        ORDER BY Banks.CreatedAt DESC
                    ) AS RowNumber,
	                ISNULL(Banks.Id, '') AS Id,
	                ISNULL(Banks.Name, '') AS Name,
	                ISNULL(Banks.Code, '') AS Code,
	                ISNULL(Banks.AccountName, '') AS AccountName,
	                ISNULL(Banks.AccountNumber, '') AS AccountNumber,
	                ISNULL(Banks.Active, 0) AS Active,
	                ISNULL(Banks.Image, '') AS Image
                FROM Banks
                WHERE 1 = 1
        ";
        sql1 += @"
            ) TBWrap
        ";
        sql2 += " SELECT COUNT(*) AS TOTALROWS " + sql1;
        sql3 += @" SELECT * " + sql1 + @" WHERE RowNumber BETWEEN " + fromIndex + @" AND " + toIndex + @" ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TOTALROWS"].ToString());

        DataTable table3 = Connect.GetTable(sql3);

        html = @"
            <table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                <thead >
                    <tr>
                        <th class='th'>Tên ngân hàng</th>
                        <th class='th'>Mã ngân hàng</th>
                        <th class='th'>Tên chủ tài khoản</th>
                        <th class='th'>Số tài khoản</th>
                        <th class='th'>Trạng thái</th>
                        <th class='th'>Logo</th>
                        <th class='th '>Hành động</th>
                    </tr>
                </thead>
                <tbody>
        ";
        if (table3.Rows.Count == 0)
        {
            html += @"
                <tr>
                    <td colspan='7'><p class='text-center'><strong>Không có kết quả !</strong></p></td>
                </tr>
            ";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            html += @"<tr>";
            // name
            html += "<td class='td'>" + table3.Rows[i]["Name"].ToString() + "</td>";
            // code
            html += "<td class='td'>" + table3.Rows[i]["Code"].ToString() + "</td>";
            // account name
            html += "<td class='td'>" + table3.Rows[i]["AccountName"].ToString() + "</td>";
            // account number
            html += "<td class='td'>" + table3.Rows[i]["AccountNumber"].ToString() + "</td>";
            // active
            html += "<td class='td'>" + (table3.Rows[i]["Active"].ToString() == "True" ? "Bật" : "Tắt" ) + "</td>";
            // image
            html += "<td class='td'><img src='"+ Utilities.Banks.createLogoUrl(table3.Rows[i]["Image"].ToString()) + "' /></td>";
            // action
            html += @"
                <td class='td'>
                    <a href='/Admin/QuanLyTaiChinh/SuaNganHang.aspx?nganHang=" + table3.Rows[i]["Id"].ToString() + @"' class='btn btn-primary'>Sửa</a>
                    <a href='javascript:void(0);' class='btn btn-danger btn-delete' data-id='"+ table3.Rows[i]["Id"].ToString() +@"'>Xóa</a>
                </td>
            ";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
        if (totalItems > perPage)
        {
            htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "/Admin/QuanLyTaiChinh/DanhSachNganHang.aspx", postParams);
            divPagination1.InnerHtml = htmlPagination;
        }
        else
        {
            divPagination1.Visible = false;
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        string id = inputDeleteId.Text.Trim();
        DataRow bank = Services.Banks.find(id);
        if(bank == null)
        {
            Utilities.Redirect.notFound();
            return;
        }
        string currentImage = bank["Image"].ToString();
        Services.Banks.remove(bank["Id"].ToString());
        Utilities.IO.removeFile(currentImage);
        Response.Redirect("/Admin/QuanLyTaiChinh/DanhSachNganHang.aspx");
    }
}