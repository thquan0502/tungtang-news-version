﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="GiaUpTin.aspx.cs" Inherits="GiaUpTin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Giá up tin</span>
                </h1>
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-xs-12 col-lg-6">
                            <label>Giá</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputPrice" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-lg-6"></div>
                        <div class="col-xs-12 col-lg-6">
                            <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSubmit_Click" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</asp:Content>
