﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DanhSachGoiNapTien.aspx.cs" Inherits="DanhSachGoiNapTien" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .preview-background-color {
            width: 50px;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1 class="title">
                    <span>Danh sách gói nạp tiền</span>
                </h1>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
            </section>
        </div>
    </form>
</asp:Content>
