﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="SuaGoiNap.aspx.cs" Inherits="SuaGoiNap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .active-label {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .active-label input {
            display: inline-block;
            margin: 0;
            margin-right: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Sửa gói nạp</span>
                </h1>
                <div class="container mt-5">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-lg-6">
                            <label>Mã gói nạp</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputCode" runat="server" cssClass="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Tên gói nạp</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputName" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Số tiền nạp</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputRechargePrice" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Số tiền thực tế nhận</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputActualAmount" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Màu nền</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputBackgroundColor" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6"></div>
                        <div class="col-xs-12 col-lg-6"></div>
                        <div class="col-xs-12 col-lg-6">
                            <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSubmit_Click" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</asp:Content>
