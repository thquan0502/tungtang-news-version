﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DanhSachThanhVien.aspx.cs" Inherits="DanhSachThanhVien" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="/asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>
	
	<script src="../Js/Page/TinDang.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>

    <link href="../Css/Page/ThongTinCaNhan.css" rel="stylesheet" />
     <link href="../Css/Page/AccountPost.css" rel="stylesheet" />
	<link href="/asset/css/zalo/style.css" rel="stylesheet" />
    <script src="../Js/Page/ThongTinCaNhan.min.js"></script>

      <link href="../plugins/select2/select2.min.css" rel="stylesheet" />
      <script src="../plugins/select2/select2.full.min.js"></script>    
    <script src="../../Js/jquery.modal.min.js"></script> 

        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
                    <% if (!string.IsNullOrWhiteSpace(rechargeSuccessMessage))
                { %>
            <script>
                Swal.fire('Thành công', '<%= rechargeSuccessMessage %>', 'success');
            </script>
            <% } %>
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>DANH SÁCH THÀNH VIÊN</span>
                </h1>
                 <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            
                            <div class="col-md-8">
                                 <div id="filterPostOwnerWrap" class="filter-row">
                                                        <div class="col-md-2 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Thành Viên:</label>
                                                        </div>
                                                        <div class="col col-md-10">
                                                            <select id="filterPostOwnerInput" class="form-control"></select>
                                                        </div>
                                                </div>
                            </div>
                            <div class="col-md-4">
                             <button id="btnFilterSubmit" type="button" class="btn btn-primary text-sm">Tìm Kiếm</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
                <div id="divPagination1" class="app-table-pagination" style="text-align:right;" runat="server"></div>
            </section>
        </div>
    </form>

     <script>                
                  (function (){
            //var checkTaba = document.getElementById('ipkt').value;
            var url_string = window.location;
                var url = new URL(url_string);
              //  var name = url.searchParams.get("Tab");
                //alert(name);
                //alert('hi' +checkTaba);
                //if (checkTaba == "")
                //{
                //    checkTaba = "1";
                //}
              
                  var templateDefaultOption = "<option value=''>Chọn</option>";
                       var templateJsonOwners = <%= templateJsonOwners %>;
                       var selectHtml2 = "";
                       templateJsonOwners.forEach(function (ele1, pos1, arr1) {
                           selectHtml2 += "<option value='" + ele1.id + "'>" + ele1.code + " - " + ele1.phone + " - " + ele1.fullname + "</option>";
                       });
                       selectHtml2 = templateDefaultOption + selectHtml2;
                       $('#filterPostOwnerInput').html(selectHtml2);
        })();
       
        function $_GET(q,s) {
            s = (s) ? s : window.location.search;
            var re = new RegExp('&amp;'+q+'=([^&amp;]*)','i');
            return (s=s.replace(/^\?/,'&amp;').match(re)) ?s=s[1] :s='';
        }
        (function () {
            var fnSlashToDash = function (sSlash) {
                var rs = "";
                if (sSlash != undefined && sSlash != null && sSlash != "") {
                    sSlash = sSlash.trim();
                    var parts = sSlash.split("/");
                    if (parts.length == 3) {
                        return parts[2] + "-" + parts[1] + "-" + parts[0];
                    }
                }
                return rs;
            }

            var baseUrl = "/Admin/QuanLyTaiChinh/DanhSachThanhVien.aspx?";
            $("#btnFilterSubmit").click(function () {                
                var filterPostOwnerInput = $('#filterPostOwnerInput').val();
             

                var url = baseUrl;
                var counterParams = 0;
                if (filterPostOwnerInput != undefined && filterPostOwnerInput != null && filterPostOwnerInput != "") {
                    counterParams++;                    
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "FilterPostOwner=" + filterPostOwnerInput;                    
                }
                        
                if (counterParams > 0) {
                    window.location.href = url;                    
                }                             
            });

        })();   
          (function () {
              var initFilterPostOwner = "<%= templateFilterPostOwner %>";
              $('#filterPostOwnerInput').val(initFilterPostOwner)
              if (initFilterPostOwner != "") {
                  $('#filterPostOwnerWrap').show();
                  
              }
              $('#filterPostOwnerInput').select2();
          })();
         </script>
</asp:Content>
