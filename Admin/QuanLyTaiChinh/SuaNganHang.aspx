﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="SuaNganHang.aspx.cs" Inherits="SuaNganHang" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .active-label {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .active-label input {
            display: inline-block;
            margin: 0;
            margin-right: 8px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Sửa ngân hàng</span>
                </h1>
                <div class="container mt-5">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-lg-6">
                            <label>Tên ngân hàng</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputName" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Mã ngân hàng</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputCode" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Tên chủ tài khoản</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputAccountName" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Số tài khoản</label>
                            <div class="form-group">
                                <asp:TextBox ID="inputAccountNumber" runat="server" cssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>&nbsp;</label>
                            <label class="active-label">
                                <asp:CheckBox ID="inputActive" runat="server" /><span>Hiển thị</span>
                            </label>
                        </div>
                        <div class="col-xs-12 col-lg-6">
                            <label>Logo ngân hàng</label>
                            <div class="form-group">
                                <asp:FileUpload ID="inputImage" runat="server" cssClass="form-control" AllowMultiple="false" />
                                <img ID="oldImage" runat="server" src="" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-6"></div>
                        <div class="col-xs-12 col-lg-6">
                            <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSubmit_Click" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
</asp:Content>
