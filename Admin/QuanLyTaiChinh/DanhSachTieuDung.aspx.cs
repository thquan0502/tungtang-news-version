﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class DanhSachTieuDung : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }
        if (!IsPostBack){
            this.loadMainTable();
        }
    }

    protected void loadMainTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 15;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = new Dictionary<string, string>();

        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 += @"
            FROM (
                SELECT 
                    ROW_NUMBER() OVER ( 
                        ORDER BY BalanceHistory.CreatedAt DESC
                    ) AS RowNumber,
	                ISNULL(BalanceHistory.Id, '') AS Id,
	                ISNULL(TBAccounts.SoDienThoai, '') AS AccountPhone,
	                ISNULL(BalanceHistory.SpentType, '') AS SpentType,
	                ISNULL(BalanceHistory.Amount, 0) AS Amount,
	                BalanceHistory.FeeCode AS FeeCode,
	                ISNULL(BalanceHistory.CreatedAt, '') AS CreatedAt
                FROM BalanceHistory
                INNER JOIN tb_ThanhVien TBAccounts
	                ON TBAccounts.idThanhVien = BalanceHistory.AccountId
                WHERE BalanceHistory.Type = '" + Utilities.BalanceHistory.TYPE_SPENT +@"'
        ";
        sql1 += @"
            ) TBWrap
        ";
        sql2 += " SELECT COUNT(*) AS TOTALROWS " + sql1;
        sql3 += @" SELECT * " + sql1 + @" WHERE RowNumber BETWEEN " + fromIndex + @" AND " + toIndex + @" ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TOTALROWS"].ToString());

        DataTable table3 = Connect.GetTable(sql3);

        html = @"
            <table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                <thead >
                    <tr>
                        <th class='th'>Sđt tài khoản</th>
                        <th class='th'>Mục đích</th>
                        <th class='th'>Số tiền</th>
                        <th class='th'>Thời gian</th>
                    </tr>
                </thead>
                <tbody>
        ";
        if (table3.Rows.Count == 0)
        {
            html += @"
                <tr>
                    <td colspan='4'><p class='text-center'><strong>Không có kết quả !</strong></p></td>
                </tr>
            ";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string detail = "";
            string spentType = table3.Rows[i]["SpentType"].ToString();
            string feeCode = table3.Rows[i]["FeeCode"].ToString();
            if (spentType == Utilities.BalanceHistory.SPENT_TYPE_ADD_POST)
            {
                detail += "Đăng tin";
                if (!string.IsNullOrWhiteSpace(feeCode))
                {
                    detail += ", " + Utilities.PostFee.getPostCodeHuman(feeCode);
                }
            }
            else if (spentType == Utilities.BalanceHistory.SPENT_TYPE_UPGRADE_POST)
            {
                detail += "Nâng cấp tin";
                if (!string.IsNullOrWhiteSpace(feeCode))
                {
                    detail += ", " + Utilities.PostFee.getPostCodeHuman(feeCode);
                }
            }
            else if (spentType == Utilities.BalanceHistory.SPENT_TYPE_EXTEND_POST)
            {
                detail += "Gia hạn tin";
                if (!string.IsNullOrWhiteSpace(feeCode))
                {
                    detail += ", " + Utilities.PostFee.getPostCodeHuman(feeCode);
                }
            }
            else if (spentType == Utilities.BalanceHistory.SPENT_TYPE_TO_TOP)
            {
                detail += "Lên TOP tin đăng";
            }

            html += @"<tr>";
            // account phone
            html += "<td class='td'>" + table3.Rows[i]["AccountPhone"].ToString() + "</td>";
            // method
            html += "<td class='td'>" + detail + "</td>";
            // amount
            html += "<td class='td'>" + Utilities.Formatter.toCurrencyString((long)table3.Rows[i]["Amount"]) + "</td>";
            // created at
            html += "<td class='td'>" + Utilities.Formatter.toDateTimeString(table3.Rows[i]["CreatedAt"].ToString()) + "</td>";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
        if (totalItems > perPage)
        {
            htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "/Admin/QuanLyTaiChinh/DanhSachTieuDung.aspx", postParams);
            divPagination1.InnerHtml = htmlPagination;
        }
        else
        {
            divPagination1.Visible = false;
        }
    }
}