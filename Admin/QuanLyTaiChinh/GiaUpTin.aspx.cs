﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;
using System.IO;

public partial class GiaUpTin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }

        if (!IsPostBack)
        {
            inputPrice.Text = "" + Services.RenovateFees.getPrice();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        long price = long.Parse(inputPrice.Text.Trim());

        Services.RenovateFees.updatePrice(price);

        Response.Redirect("/Admin/QuanLyTaiChinh/GiaUpTin.aspx");
    }
}

