﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;
using System.IO;

public partial class CongTien : System.Web.UI.Page
{
    protected DataRow userbalance = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Services.AdminPermissions.isAuthorize(9))
        {
            Utilities.Redirect.notFound();
            return;
        }

        string userId = Request.QueryString["userid"];
        DataRow userbalance = Services.UserBalance.finds(userId);

        if (userbalance == null)
        {
            Utilities.Redirect.notFound();
            return;
        }
        else
        {
            this.userbalance = userbalance;
        }

        if (!IsPostBack)
        {
            inputCode.Text = this.userbalance["idThanhVien"].ToString();
            inputName.Text = this.userbalance["TenCuaHang"].ToString();
            inputAccountPhone.Text = this.userbalance["TenDangNhap"].ToString();
           // inputBalance.Text = this.userbalance["BalanceAmount"].ToString();
           // inputNote.Text = this.userbalance["Note"].ToString();
            //  inputActive.Checked = this.bank["Active"].ToString() == "True";
            // oldImage.Src = this.bank["Image"].ToString();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if(this.userbalance == null)
        {
            Utilities.Redirect.notFound();
            return;
        }


        string id = this.userbalance["idThanhVien"].ToString();

        string name = inputName.Text.Trim();
        string code = inputCode.Text.Trim();
        string accountPhone = inputAccountPhone.Text.Trim();
        string accountBalance = inputBalance.Text.Trim();
        string note= inputNote.Text.Trim();


        //  bool active = inputActive.Checked;
        if (string.IsNullOrWhiteSpace(accountBalance) || string.IsNullOrWhiteSpace(note))
        {
            return;
        }

        long amount = long.Parse(accountBalance);

        long idHistory = Services.BalanceHistory.createRechargeTransfer(id, amount, amount, Utilities.BalanceHistory.RECHARGE_STATUS_ENTERED, note);
        Services.Account.rechargeBalance(id, amount);

        HttpCookie cookieData = new HttpCookie("recharge_transfer_success");
        cookieData.Value = "" + idHistory;
        cookieData.Expires = DateTime.Now.AddSeconds(10);

        Response.Cookies.Add(cookieData);
        Response.Redirect("/Admin/QuanLyTaiChinh/DanhSachThanhVien.aspx");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Admin/QuanLyTaiChinh/DanhSachThanhVien.aspx");
    }
}

