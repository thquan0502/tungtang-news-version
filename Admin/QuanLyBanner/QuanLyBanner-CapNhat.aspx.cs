﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLyBanner_QuanLyBanner_CapNhat : System.Web.UI.Page
{
    string sIdBanner = "";
    string Page = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            sIdBanner = StaticData.ValidParameter(Request.QueryString["idBanner"].Trim());
        }
        catch { }
        try
        {
            Page = StaticData.ValidParameter(Request.QueryString["Page"].Trim());
        }
        catch { }
        if (!IsPostBack)
        {
            LoadThongTinBanner();
        }
        if (IsPostBack && fileLinkAnh.PostedFile != null)
        {
            if (fileLinkAnh.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnh.HasFile)
                    {
                        string Ngay = DateTime.Now.Day.ToString();
                        string Thang = DateTime.Now.Month.ToString();
                        string Nam = DateTime.Now.Year.ToString();
                        string Gio = DateTime.Now.Hour.ToString();
                        string Phut = DateTime.Now.Minute.ToString();
                        string Giay = DateTime.Now.Second.ToString();
                        string Khac = DateTime.Now.Ticks.ToString();
                        string fExtension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                        string FileName = "banner" + Ngay + Thang + Nam + Gio + Phut + Giay + Khac + fExtension;
                        string FilePath = "../../Images/Banner/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnh.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnh.Src = FilePath;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }
    }
    private void LoadThongTinBanner()
    {
        if (sIdBanner != "")
        {
            string sql = "select * from tb_Banner where idBanner='" + sIdBanner + "'";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                dvTitle.InnerHtml = "SỬA THÔNG TIN BANNER";
                btLuu.Text = "SỬA";
                txtTieuDe.Value = table.Rows[0]["TieuDe"].ToString();
                imgLinkAnh.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
                txtUrl.Value = table.Rows[0]["Url"].ToString();
            }
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        string TieuDe = "";
        string LinkAnh = imgLinkAnh.Src.Replace("../../", "");
        string Url = txtUrl.Value;
        //Tiêu đề
        if (txtTieuDe.Value.Trim() != "")
        {
            TieuDe = txtTieuDe.Value.Trim();
        }
        else
        {
            Response.Write("<script>alert('Bạn chưa nhập tiêu đề!')</script>");
            return;
        }
        //////////
        if (sIdBanner == "")
        {
            string sqlInsertBanner = "insert into tb_Banner(TieuDe,LinkAnh,Url)";
            sqlInsertBanner += " values(N'" + TieuDe + "',N'" + LinkAnh + "',N'"+ Url +"')";
            bool ktInsertBanner = Connect.Exec(sqlInsertBanner);
            if (ktInsertBanner)
            {
                Response.Redirect("QuanLyBanner.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm banner!')</script>");
            }
        }
        else
        {
            string sqlUpdateBanner = "update tb_Banner set TieuDe=N'" + TieuDe + "'";
            sqlUpdateBanner += ",LinkAnh=N'" + LinkAnh + "'";
            sqlUpdateBanner += ",Url=N'" + Url + "'";
            sqlUpdateBanner += " where idBanner='" + sIdBanner + "'";
            bool ktUpdateBanner = Connect.Exec(sqlUpdateBanner);
            if (ktUpdateBanner)
            {
                if (Page != "")
                    Response.Redirect("QuanLyBanner.aspx?Page=" + Page);
                else
                    Response.Redirect("QuanLyBanner.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("QuanLyBanner.aspx");
    }
}