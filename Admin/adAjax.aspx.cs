﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_adAjax : System.Web.UI.Page
{
    string sTenDangNhap = "";
    string mQuyen = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["QuanLyHoaDongDifoco_Login"] != null && Session["QuanLyHoaDongDifoco_Login"].ToString() != "")
        //{
        //    sTenDangNhap = Session["QuanLyHoaDongDifoco_Login"].ToString();
        //    mQuyen = StaticData.getField("tb_Admin", "MaNhomAdmin", "TenDangNhap", sTenDangNhap);
        //}
        //else
        //{
        //    //Response.Redirect("Home/DangNhap.aspx"); 
        //}
        string action = Request.QueryString["Action"].Trim();
        switch (action)
        {
            case "DangXuat":
                DangXuat(); break;
            case "DuyetTinDang":
                DuyetTinDang(); break;
			case "DuyetUserPro":
                DuyetUserPro(); break;
            case "KhoaThanhVien":
                KhoaThanhVien(); break;
            case "DeleteDanhMucCap1":
                DeleteDanhMucCap1(); break;
            case "DeleteDanhMucCap2":
                DeleteDanhMucCap2(); break;
            case "CapNhatHetHan":
                CapNhatHetHan(); break;
            case "MoXemChiTietTinDang":
                MoXemChiTietTinDang(); break;
            case "DeleteTinTuc":
                DeleteTinTuc(); break;
			case "DeleteTinDang":
                DeleteTinDang(); break;
            case "DeleteTags":
                DeleteTags(); break;
            case "DeleteTop":
                DeleteTop(); break;
            case "XoaAdmin":
                XoaAdmin(); break;
            case "LenTop":
                LenTop(); break;
            case "LuuQuyen":
                LuuQuyen(); break;
            case "GetHTMLCities":
                GetHTMLCities(); break;
            case "GetHTMLDistricts":
                GetHTMLDistricts(); break;
            case "GetHTMLWards":
                GetHTMLWards(); break;
			case "ThemLyDo":
                ThemLyDo(); break;
            case "LoadDSLydo":
                LoadDSLydo(); break;
            case "SuaLyDoKD":
                SuaLyDoKD(); break;
            case "DeleteLyDo":
                DeleteLyDo(); break;
			case "GetHTMLDMC1":
                GetHTMLDMC1(); break;
            case "GetHTMLDMC2":
                GetHTMLDMC2(); break;
			case "BieuDo":	
                BieuDo(); break;
        }
    }
    private void LuuQuyen()
    {
        string idNhomQuyen = Request.QueryString["idNhomQuyen"].Trim();
        string check = Request.QueryString["check"].Trim();
        
        if (Request.Cookies["cookie_PhanQuyen"] != null)
        {

            string a = HttpContext.Current.Request.Cookies["cookie_PhanQuyen"].Value;

            if (check == "LUU")
                a += idNhomQuyen + ",";
            else
            {
                a = a.Replace(idNhomQuyen+",","");
            }

                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", a);
            cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie_PhanQuyen);
        }
    }
	
	private void BieuDo()	
    {	
        string TuNgay = Request.QueryString["TuNgay"].Trim();	
        string DenNgay = Request.QueryString["DenNgay"].Trim();	
        string sqlBieuDo1 = "select Count(idThanhVien) as SoTV from (select Count(idThanhVien) as SoLan, idThanhVien from (select COUNT(NgayDangBai) as TongTinNgay,idThanhVien, NgayDangBai from (select ROW_NUMBER() OVER(PARTITION BY idThanhVien ORDER BY NgayDang asc) as RowNumber, idThanhVien, CONVERT(VARCHAR(10), NgayDang, 103) as NgayDangBai from tb_TinDang where NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59' ) as tb1 group by idThanhVien, NgayDangBai) as tb2 where TongTinNgay = 1 group by idThanhVien, NgayDangBai) as tb3";	
        string sqlBieuDo23 = "select Count(idThanhVien) as SoTV from (select Count(idThanhVien) as SoLan, idThanhVien from (select COUNT(NgayDangBai) as TongTinNgay,idThanhVien, NgayDangBai from (select ROW_NUMBER() OVER(PARTITION BY idThanhVien ORDER BY NgayDang asc) as RowNumber, idThanhVien, CONVERT(VARCHAR(10), NgayDang, 103) as NgayDangBai from tb_TinDang where NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59' ) as tb1 group by idThanhVien, NgayDangBai) as tb2 where TongTinNgay >= 2 and TongTinNgay <= 3 group by idThanhVien, NgayDangBai) as tb3";	
        string sqlBieuDo45 = "select Count(idThanhVien) as SoTV from (select Count(idThanhVien) as SoLan, idThanhVien from (select COUNT(NgayDangBai) as TongTinNgay,idThanhVien, NgayDangBai from (select ROW_NUMBER() OVER(PARTITION BY idThanhVien ORDER BY NgayDang asc) as RowNumber, idThanhVien, CONVERT(VARCHAR(10), NgayDang, 103) as NgayDangBai from tb_TinDang where NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59' ) as tb1 group by idThanhVien, NgayDangBai) as tb2 where TongTinNgay >= 4 and TongTinNgay <= 5 group by idThanhVien, NgayDangBai) as tb3";	
        string sqlBieuDo610 = "select Count(idThanhVien) as SoTV from (select Count(idThanhVien) as SoLan, idThanhVien from (select COUNT(NgayDangBai) as TongTinNgay,idThanhVien, NgayDangBai from (select ROW_NUMBER() OVER(PARTITION BY idThanhVien ORDER BY NgayDang asc) as RowNumber, idThanhVien, CONVERT(VARCHAR(10), NgayDang, 103) as NgayDangBai from tb_TinDang where NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59' ) as tb1 group by idThanhVien, NgayDangBai) as tb2 where TongTinNgay >= 6 and TongTinNgay <= 10 group by idThanhVien, NgayDangBai) as tb3";	
        string sqlBieuDo1120 = "select Count(idThanhVien) as SoTV from (select Count(idThanhVien) as SoLan, idThanhVien from (select COUNT(NgayDangBai) as TongTinNgay,idThanhVien, NgayDangBai from (select ROW_NUMBER() OVER(PARTITION BY idThanhVien ORDER BY NgayDang asc) as RowNumber, idThanhVien, CONVERT(VARCHAR(10), NgayDang, 103) as NgayDangBai from tb_TinDang where NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59' ) as tb1 group by idThanhVien, NgayDangBai) as tb2 where TongTinNgay >= 11 and TongTinNgay <= 20 group by idThanhVien, NgayDangBai) as tb3";	
        string sqlBieuDo20 = "select Count(idThanhVien) as SoTV from (select Count(idThanhVien) as SoLan, idThanhVien from (select COUNT(NgayDangBai) as TongTinNgay,idThanhVien, NgayDangBai from (select ROW_NUMBER() OVER(PARTITION BY idThanhVien ORDER BY NgayDang asc) as RowNumber, idThanhVien, CONVERT(VARCHAR(10), NgayDang, 103) as NgayDangBai from tb_TinDang where NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59' ) as tb1 group by idThanhVien, NgayDangBai) as tb2 where TongTinNgay > 20 group by idThanhVien, NgayDangBai) as tb3";	
        DataTable bieuDo1 = Connect.GetTable(sqlBieuDo1);	
        DataTable bieuDo23 = Connect.GetTable(sqlBieuDo23);	
        DataTable bieuDo45 = Connect.GetTable(sqlBieuDo45);	
        DataTable bieuDo610 = Connect.GetTable(sqlBieuDo610);	
        DataTable bieuDo1120 = Connect.GetTable(sqlBieuDo1120);	
        DataTable bieuDo20 = Connect.GetTable(sqlBieuDo20);	
        string sbieuDo1 = bieuDo1.Rows[0]["SoTV"].ToString();	
        string sbieuDo23 = bieuDo23.Rows[0]["SoTV"].ToString();	
        string sbieuDo45 = bieuDo45.Rows[0]["SoTV"].ToString();	
        string sbieuDo610 = bieuDo610.Rows[0]["SoTV"].ToString();	
        string sbieuDo1120 = bieuDo1120.Rows[0]["SoTV"].ToString();	
        string sbieuDo20 = bieuDo20.Rows[0]["SoTV"].ToString();	
        string bd = sbieuDo1 + "," + sbieuDo23 + "," + sbieuDo45 + "," + sbieuDo610 + "," + sbieuDo1120 + "," + sbieuDo20;	
        string html = "";	
        if (sbieuDo1 != "" || sbieuDo23 != "" || sbieuDo45 != "" || sbieuDo610 != "" || sbieuDo1120 != "" || sbieuDo20 != "")	
        {	
            html += "<input id='txtTest' value='" + bd + "' />";	
            Response.Write(html);	
        }        	
    }
	
    private void DangXuat()
    {
        HttpCookie cookie_AdminTungTang_Login = new HttpCookie("AdminTungTang_Login", "");
        // Gán thời gian sống của Cookie là thời gian hiện tại 
        cookie_AdminTungTang_Login.Expires = DateTime.Now;
        // Thêm Cookie 
        Response.Cookies.Add(cookie_AdminTungTang_Login);
        Response.Write("True");
    }
    
    private string updateDuyet(string postId)
    {
        string sql = "";
        
        DataRow row = Services.Post.find(postId);
        if(row == null)
        {
            return sql;
        }

        object[] feeInfo = Utilities.Post.getFeeInfo(row);

        string code = (string) feeInfo[1];
        if(code == "default_pack")
        {
            return sql;
        }
        else
        {
            DateTime startTime = (DateTime)feeInfo[2];
            DateTime endTime = (DateTime)feeInfo[3];

            TimeSpan diff = endTime - startTime;
            endTime.Add(diff);

            sql += " FeeStartTime= '" + startTime.ToString("yyyy-MM-dd HH:mm:ss:fff") + "', ";
            sql += " FeeEndTime= '" + endTime.ToString("yyyy-MM-dd HH:mm:ss:fff") + "', ";
        }
        return sql;
    }

    private void DuyetTinDang()
    {
        string idNguoiDuyet = "";
        if (Request.Cookies["AdminTungTang_Login"] != null && Request.Cookies["AdminTungTang_Login"].Value.Trim() != "")
        {
            string mTenDangNhap = Request.Cookies["AdminTungTang_Login"].Value.Trim();
            idNguoiDuyet = StaticData.getField("tb_Admin", "idAdmin", "tenDangNhap", mTenDangNhap);
        }

        string idTinDang = Request.QueryString["idTinDang"].Trim();
        string Duyet = Request.QueryString["Duyet"].Trim();
        string LyDoKhongDuyet = Request.QueryString["LyDo"].Trim();

        string sqlUpdateDuyet = "update tb_TinDang";
       
        if (Duyet.ToUpper() == "DUYET")
        {
            sqlUpdateDuyet += " set isDuyet='true', LyDo_KhongDuyet=NULL,";
            sqlUpdateDuyet += " " + updateDuyet(idTinDang) + " ";
        }
        if (Duyet.ToUpper() == "KHONGDUYET")
        {
            sqlUpdateDuyet += " set isDuyet='false', LyDo_KhongDuyet= N'" + LyDoKhongDuyet + "',";
        }
		if (Duyet.ToUpper() == "DANGCHO")
        {
            sqlUpdateDuyet += " set isDuyet=null, LyDo_KhongDuyet=NULL,";
        }
        if (Duyet.ToUpper() == "")
        {
            sqlUpdateDuyet += " set isDuyet=null, LyDo_KhongDuyet=NULL,";
        }
        sqlUpdateDuyet += "idAdmin_Duyet='" + idNguoiDuyet + "',";

        if (Duyet.ToUpper() == "DUYET" )
        {
            string sqlLayNgay = "select NgayDang, NgayGuiDuyet from tb_TinDang where idtindang='" + idTinDang + "' ";
            DataTable tbLayNgay = Connect.GetTable(sqlLayNgay);
            string NgayDang = "";
            string NgayGuiDuyet = "";
			string NgayAdminDuyet = "";
            string DayNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                NgayDang = DateTime.Parse(tbLayNgay.Rows[0]["NgayDang"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch
            { }

            try
            {
                NgayGuiDuyet = DateTime.Parse(tbLayNgay.Rows[0]["NgayGuiDuyet"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch
            { }

			try
            {
                NgayAdminDuyet = DateTime.Parse(tbLayNgay.Rows[0]["NgayAdminDuyet"].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch {}

            if(NgayDang != "" )
            {
                sqlUpdateDuyet += " NgayDayLenTop='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',";
				sqlUpdateDuyet += "NgayAdminDuyet='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' ";
            }
        }
		if (Duyet.ToUpper() == "KHONGDUYET")
        {
            sqlUpdateDuyet += "NgayAdminDuyet='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }
        sqlUpdateDuyet += " where idTinDang='" + idTinDang + "'";
        sqlUpdateDuyet = sqlUpdateDuyet.Replace(", where", " where ");
        bool ktUpdateDuyet = Connect.Exec(sqlUpdateDuyet);
        if (ktUpdateDuyet)
        {
            Response.Write("True");
        }
        else
        {
            Response.Write("False");
        }
    }
	  private void DuyetUserPro()
    {
        string idNguoiDuyet = "";
        if (Request.Cookies["AdminTungTang_Login"] != null && Request.Cookies["AdminTungTang_Login"].Value.Trim() != "")
        {
            string mTenDangNhap = Request.Cookies["AdminTungTang_Login"].Value.Trim();
            idNguoiDuyet = StaticData.getField("tb_Admin", "idAdmin", "tenDangNhap", mTenDangNhap);
        }

        string idThanhVien = Request.QueryString["idThanhVien"].Trim();
        string Duyet = Request.QueryString["Duyet"].Trim();
        string LyDoKhongDuyet = Request.QueryString["LyDo"].Trim();
        string sqlUpdateDuyet = "update tb_ThanhVien";
        string sql = "select * from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
        DataTable table = Connect.GetTable(sql);
        if (Duyet.ToUpper() == "DUYET")
        {
            if(table.Rows[0]["HeadCMND"].ToString() == "" && table.Rows[0]["FooterCMND"].ToString() =="")
            {
                Response.Write("Falses");
                return;
            }
            else
            sqlUpdateDuyet += " set isDuyet='true',LyDo_KhongDuyet=NULL, StsPro=1, AdminAt ='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
            sqlUpdateDuyet += "idAdmin_Duyet='" + idNguoiDuyet + "',";
        }
        if (Duyet.ToUpper() == "DANGCHO")
        {
            if (table.Rows[0]["HeadCMND"].ToString() == "" && table.Rows[0]["FooterCMND"].ToString() == "")
            {
                Response.Write("Falses");
                return;
            }
            else
                sqlUpdateDuyet += " set idAdmin_Duyet='-1',isDuyet='false',LyDo_KhongDuyet=NULL, StsPro=0, AdminAt ='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
        }

        if (Duyet.ToUpper() == "KHONGDUYET")
        {
            if (table.Rows[0]["HeadCMND"].ToString() == "" && table.Rows[0]["FooterCMND"].ToString() == "")
            {
                Response.Write("Falses");
                return;
            }
            else
                sqlUpdateDuyet += " set isDuyet='false',LyDo_KhongDuyet= N'" + LyDoKhongDuyet + "',";
            sqlUpdateDuyet += "idAdmin_Duyet='" + idNguoiDuyet + "',";
        }
        if (Duyet.ToUpper() == "")
        {
            if (table.Rows[0]["HeadCMND"].ToString() == "" && table.Rows[0]["FooterCMND"].ToString() == "")
            {
                Response.Write("Falses");
                return;
            }
            else
                sqlUpdateDuyet += " set isDuyet=null,LyDo_KhongDuyet=NULL,";
            sqlUpdateDuyet += "idAdmin_Duyet='" + idNguoiDuyet + "',";
        }
      


        sqlUpdateDuyet += " where idThanhVien='" + idThanhVien + "'";
        sqlUpdateDuyet = sqlUpdateDuyet.Replace(", where", " where ");
        bool ktUpdateDuyet = Connect.Exec(sqlUpdateDuyet);
        if (ktUpdateDuyet)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void KhoaThanhVien()
    {
        string idThanhVien = Request.QueryString["idThanhVien"].Trim();
        string Khoa = Request.QueryString["Khoa"].Trim();
        string sqlUpdateDuyet = "update tb_ThanhVien";
        sqlUpdateDuyet += " set isKhoa='" + Khoa + "'";
        sqlUpdateDuyet += " where idThanhVien='" + idThanhVien + "'";
        bool ktUpdateDuyet = Connect.Exec(sqlUpdateDuyet);
        if (ktUpdateDuyet)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void DeleteDanhMucCap1()
    {
        string idDanhMucCap1 = StaticData.ValidParameter(Request.QueryString["id"].Trim());
        string LinkIcon = StaticData.getField("tb_DanhMucCap1", "LinkIcon", "idDanhMucCap1", idDanhMucCap1);
        string LinkHinhAnh = StaticData.getField("tb_DanhMucCap1", "LinkAnh", "idDanhMucCap1", idDanhMucCap1);

        {
            string sql = "delete from tb_DanhMucCap1 where idDanhMucCap1='" + idDanhMucCap1 + "'";
            bool ktDelete = Connect.Exec(sql);
            if (ktDelete)
            {
                try
                {
                    File.Delete(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + LinkIcon);
                }
                catch { }
                try
                {
                    File.Delete(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + LinkHinhAnh);
                }
                catch { }

                Response.Write("True");
            }
            else
                Response.Write("False");
        }

    }
    private void DeleteDanhMucCap2()
    {
        string idDanhMucCap2 = StaticData.ValidParameter(Request.QueryString["id"].Trim());
        string LinkIcon = StaticData.getField("tb_DanhMucCap2", "LinkIcon", "idDanhMucCap2", idDanhMucCap2);
        {
            string sql = "delete from tb_DanhMucCap2 where idDanhMucCap2='" + idDanhMucCap2 + "'";
            bool ktDelete = Connect.Exec(sql);
            if (ktDelete)
            {
                try
                {
                    File.Delete(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + LinkIcon);
                }
                catch { }

                Response.Write("True");
            }
            else
                Response.Write("False");
        }

    }
    private void CapNhatHetHan()
    {
        string idTinDang = Request.QueryString["idTinDang"].Trim();
        string HetHan = Request.QueryString["HetHan"].Trim();
        string sqlUpdateTD = "update tb_TinDang";
        if (HetHan.ToUpper().Trim() == "HETHAN")
            sqlUpdateTD += " set isHetHan='True'";
        else
            sqlUpdateTD += " set isHetHan='False'";
        sqlUpdateTD += " where idTinDang='" + idTinDang + "'";
        bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
        if (ktUpdateTD)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void MoXemChiTietTinDang()
    {
        string idTinDang = StaticData.ValidParameter(Request.QueryString["idTinDang"].Trim());

        string sql = "select * from tb_TinDang where idTinDang ='" + idTinDang + "'";
        DataTable table = Connect.GetTable(sql);
        string html = "<div class='titlePouple'>" + table.Rows[0]["TieuDe"].ToString() + "</div>";

        html += "<table class='table table-bordered table-striped' style='font-size:15px'>";


        html += "<tr><td><b>Loại thuê:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + StaticData.getField("tb_LoaiThue", "TenLoaiThue", "MaLoaiThue", table.Rows[0]["MaLoaiThue"].ToString()) + "</td>";
        html += "<td><b>Giá:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["Gia"].ToString() + "</td></tr>";

        html += "<tr><td><b>Người đăng:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + StaticData.getField("tb_ThanhVien", "HoTen", "idThanhVien", table.Rows[0]["idThanhVien"].ToString()) + "</td>";
        html += "<td><b>Ngày đăng:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + StaticData.ConvertMMDDYYtoDDMMYY(table.Rows[0]["NgayDang"].ToString()) + "</td></tr>";

        html += "<tr><td colspan='4'><i><b>Thông tin liên hệ</b><i></td></tr>";

        html += "<tr><td><b>Họ tên:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["HoTen"].ToString() + "</td>";
        html += "<td><b>Số điện thoại:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["SoDienTHoai"].ToString() + "</td></tr>";

        html += "<tr><td><b>Email:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["Email"].ToString() + "</td>";
        html += "<td><b>Địa chỉ:</b></td>";
        html += "<td style='text-align:center; color:#234f84'>" + table.Rows[0]["DiaChi"].ToString() + "</td></tr>";

        html += "<tr><td style='width:150px'><b>Nội dung:</b></td>";
        html += "<td colspan='3' style='text-align:justify; color:#234f84'>" + table.Rows[0]["NoiDung"].ToString() + "</td></tr>";

        html += "</table>";
        Response.Write(html);
    }
    private void DeleteTinTuc()
    {
        string idTinTuc = StaticData.ValidParameter(Request.QueryString["idTinTuc"].Trim());
        string sql = "delete from tb_TinTuc where idTinTuc='" + idTinTuc + "'";
        bool ktDelete = Connect.Exec(sql);
        if (ktDelete)
            Response.Write("True");
        else
            Response.Write("False");
    }
	 private void DeleteTinDang()
    {
        string idTinDang = StaticData.ValidParameter(Request.QueryString["idTinDang"].Trim());
        string sql = "delete from tb_TinDang where idTinDang='" + idTinDang + "'";
        bool ktDelete = Connect.Exec(sql);
        if (ktDelete)
            Response.Write("True");
        else
            Response.Write("False");
    }
   private void DeleteTags()
    {
        string id_tags = StaticData.ValidParameter(Request.QueryString["id_tags"].Trim());
        string sql2 = "delete from Tags where id_tags='" + id_tags + "'";
        bool ktDelete = Connect.Exec(sql2);
        if (ktDelete)
            Response.Write("True");
        else
            Response.Write("False");
    }
    private void DeleteTop()
    {
        string id_toptrend = StaticData.ValidParameter(Request.QueryString["id_toptrend"].Trim());
        string sql2 = "delete from  Toptrend where id_toptrend='" + id_toptrend + "'";
        bool ktDelete = Connect.Exec(sql2);
        if (ktDelete)
            Response.Write("True");
        else
            Response.Write("False");
    }
    void XoaAdmin()
    {
        string admin = StaticData.ValidParameter(Request.QueryString["admin"].Trim());

        string sql1 = "delete from [tb_ChiTietQuyenAdmin] where idadmin='" + admin + "' ";
        bool rs = Connect.Exec(sql1);
        string sql = "delete from tb_ADmin where idadmin='" + admin + "'";

        if (Connect.Exec(sql))
            Response.Write("True");
        else
            Response.Write("False");
    }
    void LenTop()
    {
        string TD = StaticData.ValidParameter(Request.QueryString["TD"].Trim());
        if (Connect.Exec("Update tb_TinDang set NgayDayLenTop ='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where idTinDang='" + TD + "' "))
            Response.Write("True");
    }
    void GetHTMLCities()
    {
        string res = "<option value=''>Chọn</option>";
        string sql1 = @"
            SELECT
	            TBCities.id AS CityId,
	            TBCities.Ten AS CityName
            FROM
	            City TBCities
        ";
        DataTable table1 = Connect.GetTable(sql1);
        for(int i=0; i< table1.Rows.Count; i++)
        {
            res += "<option value='" + table1.Rows[i]["CityId"].ToString() + "'>" + table1.Rows[i]["CityName"].ToString() + "</option>";
        }
        Response.Write(res);
    }
    void GetHTMLDistricts()
    {
        string cityId = Request.QueryString["CityId"];
        string res = "<option value=''>Chọn</option>";
        if (!string.IsNullOrWhiteSpace(cityId))
        {
            string sql1 = @"
                SELECT
	                TBDistricts.id AS DistrictId,
	                TBDistricts.Ten AS DistrictName
                FROM
	                District TBDistricts
                WHERE TBDistricts.idTinhTP = '"+ cityId + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            for (int i = 0; i < table1.Rows.Count; i++)
            {
                res += "<option value='" + table1.Rows[i]["DistrictId"].ToString() + "'>" + table1.Rows[i]["DistrictName"].ToString() + "</option>";
            }
        }
        Response.Write(res);
    }
    void GetHTMLWards()
    {
        string districtId = Request.QueryString["DistrictId"];
        string res = "<option value=''>Chọn</option>";
        if (!string.IsNullOrWhiteSpace(districtId))
        {
            string sql1 = @"
                SELECT
	                TBWards.id AS WardId,
	                TBWards.Ten AS WardName
                FROM
	                tb_PhuongXa TBWards
                WHERE TBWards.idQuanHuyen = '" + districtId + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            for (int i = 0; i < table1.Rows.Count; i++)
            {
                res += "<option value='" + table1.Rows[i]["WardId"].ToString() + "'>" + table1.Rows[i]["WardName"].ToString() + "</option>";
            }
        }
        Response.Write(res);
    }
	void GetHTMLDMC1()
    {
        string res = "<option value=''>Chọn</option>";
        string sql1 = @"	
            SELECT	
	            TBDMC1.idDanhMucCap1 AS DMC1Id,	
	            TBDMC1.TenDanhMucCap1 AS Name	
            FROM	
	            tb_DanhMucCap1 TBDMC1	
            order by sothutu	
        ";
        DataTable table1 = Connect.GetTable(sql1);
        for (int i = 0; i < table1.Rows.Count; i++)
        {
            res += "<option value='" + table1.Rows[i]["DMC1Id"].ToString() + "'>" + table1.Rows[i]["Name"].ToString() + "</option>";
        }
        Response.Write(res);
    }
    void GetHTMLDMC2()
    {
        string DMC1Id = Request.QueryString["DMC1Id"];
        string res = "<option value=''>Chọn</option>";
        if (!string.IsNullOrWhiteSpace(DMC1Id))
        {
            string sql1 = @"	
                SELECT	
	                TBDMC2.idDanhMucCap2 AS DMC2Id,	
	                TBDMC2.TenDanhMucCap2 AS Name	
                FROM	                	
	            tb_DanhMucCap2 TBDMC2	
                WHERE TBDMC2.idDanhMucCap1 = '" + DMC1Id + @"'	
            ";
            DataTable table1 = Connect.GetTable(sql1);
            for (int i = 0; i < table1.Rows.Count; i++)
            {
                res += "<option value='" + table1.Rows[i]["DMC2Id"].ToString() + "'>" + table1.Rows[i]["Name"].ToString() + "</option>";
            }
        }
        Response.Write(res);
    }
	 private void ThemLyDo()
    {
        string txtLydo = StaticData.ValidParameter(Request.QueryString["txtLydo"].Trim());
        string sql = "INSERT INTO tb_NoiDung (NoiDung) VALUES (N'" + txtLydo + "')";
        bool ktThem = Connect.Exec(sql);
        if (ktThem)
            Response.Write("true");
        else
            Response.Write("False");
    }

    private void LoadDSLydo()
    {
        string html = "";
        string sql = "select * from tb_NoiDung";
        DataTable tb = Connect.GetTable(sql);
        for (int i = 0; i < tb.Rows.Count; i++)
        {
            html += @"
            <li><label><input type='checkbox' name='kd' value='" + tb.Rows[i]["NoiDung"].ToString() + @"'/> " + tb.Rows[i]["NoiDung"].ToString() + @"</label><a href='#' style='float:right'>Edit</a></li>
            <hr />";
        }
        Response.Write(html);
    }

    private void SuaLyDoKD()
    {
        string idLydo = StaticData.ValidParameter(Request.QueryString["idLydo"].Trim());
        string txtLydo = StaticData.ValidParameter(Request.QueryString["txtLydo"].Trim());
        string sql = "UPDATE tb_NoiDung SET NoiDung = N'" + txtLydo + "' WHERE id = '" + idLydo + "'";
        bool ktSua = Connect.Exec(sql);
        if (ktSua)
            Response.Write("true");
        else
            Response.Write("False");
    }
    private void DeleteLyDo()
    {
        string idLyDo = StaticData.ValidParameter(Request.QueryString["id"].Trim());
        string sql = "delete from tb_NoiDung where id='" + idLyDo + "'";
        bool ktDelete = Connect.Exec(sql);
        if (ktDelete)
            Response.Write("True");
        else
            Response.Write("False");
    }
}