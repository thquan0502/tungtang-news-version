﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Admin_QuanTriSeo_DanhSachTitle_CapNhat : System.Web.UI.Page
{
    string idTitle = "";
    string Page = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            idTitle = StaticData.ValidParameter(Request.QueryString["id_title"].Trim());
        }
        catch { }
        try
        {
            Page = StaticData.ValidParameter(Request.QueryString["Page"].Trim());
        }
        catch { }
        if (!IsPostBack)
        {
            LoadDanhSach();
        }
        //if (IsPostBack && fileLinkAnh.PostedFile != null)
        //{
        //    if (fileLinkAnh.PostedFile.FileName.Length > 0)
        //    {
        //        string extension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
        //        if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
        //        {
        //            if (fileLinkAnh.HasFile)
        //            {
        //                string fExtension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
        //                string FileName = "DM" + DateTime.Now.ToString("ddMMyyyyHHmmssms") + fExtension;
        //                string FilePath = "../../Images/Category/" + FileName;
        //                //LinkAnh = "Images/SanPham/" + FileName;
        //                fileLinkAnh.SaveAs(Server.MapPath(FilePath));
        //                imgLinkAnh.Src = FilePath;
        //            }
        //        }
        //        else
        //        {
        //            Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
        //            return;
        //        }
        //    }
        //}

        //if (IsPostBack && fileLinkAnhIcon.PostedFile != null)
        //{
        //    if (fileLinkAnhIcon.PostedFile.FileName.Length > 0)
        //    {
        //        string extension = Path.GetExtension(fileLinkAnhIcon.PostedFile.FileName);
        //        if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
        //        {
        //            if (fileLinkAnhIcon.HasFile)
        //            {
        //                string fExtension = Path.GetExtension(fileLinkAnhIcon.PostedFile.FileName);
        //                string FileName = "DM" + DateTime.Now.ToString("ddMMyyyyHHmmssms") + fExtension;
        //                string FilePath = "../../Images/Category/" + FileName;
        //                //LinkAnh = "Images/SanPham/" + FileName;
        //                fileLinkAnhIcon.SaveAs(Server.MapPath(FilePath));
        //                imgLinkAnhIcon.Src = FilePath;
        //            }
        //        }
        //        else
        //        {
        //            Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
        //            return;
        //        }
        //    }
        //}
    }
    private void LoadDanhSach()
    {
        if (idTitle != "")
        {
            string sql = "select * from Seo where id_title='" + idTitle + "'";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                dvTitle.InnerHtml = "Sửa HomePage";
                btLuu.Text = "SỬA";
                txtTitle.Value = table.Rows[0]["Title"].ToString();
                txtDescription.Value = table.Rows[0]["Desciption"].ToString();
                //txtSoThuTu.Value = table.Rows[0]["SoThuTu"].ToString();
                //txtMucGiaBoLoc.Value = decimal.Parse(table.Rows[0]["MucGia_BOLoc"].ToString().Trim().Replace(" ","0")).ToString("N0").Replace(" ", "0");
                //imgLinkAnh.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
                //imgLinkAnhIcon.Src = "../../" + table.Rows[0]["LinkIcon"].ToString();

                //txtMoTa.Value = table.Rows[0]["MoTa"].ToString();
                //txtTitlte.Value = table.Rows[0]["Titlte"].ToString();
                //txtmetaCanonical.Value = table.Rows[0]["metaCanonical"].ToString();
                //txtmetarobots.Value = table.Rows[0]["metarobots"].ToString();
                //txtmetaOpenGraph.Value = table.Rows[0]["metaOpenGraph"].ToString();
                //txtmetatwitter.Value = table.Rows[0]["metatwitter"].ToString(); 
            }
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        //string LinkIcon = imgLinkAnhIcon.Src.Replace("../../", "");
        //string LinkAnh = imgLinkAnh.Src.Replace("../../", "");
        string Title = txtTitle.Value.Trim();
        string Description = txtDescription.Value.Trim();
        //string SoThuTu = txtSoThuTu.Value.Trim();
        //string MucGia_BoLoc = txtMucGiaBoLoc.Value.Trim().Replace(",","");

        //string Titlte = txtTitlte.Value.Trim();
        //string MoTa = txtMoTa.Value.Trim();
        //string metaCanonical = txtmetaCanonical.Value.Trim();
        //string metarobots = txtmetarobots.Value.Trim();
        //string metaOpenGraph = txtmetaOpenGraph.Value.Trim();
        //string metatwitter = txtmetatwitter.Value.Trim();

        if (Title == "")
        {
            Response.Write("<script>alert('Tiêu đề không được trống !')</script>");
            txtTitle.Focus();
            return;
        }

        if (Description == "")
        {
            Response.Write("<script>alert('Nội dung không được trống !')</script>");
            txtDescription.Focus();
            return;
        }
        //////////
        if (idTitle == "")
        {
            string sqlInsertTitle = "insert into Seo(Title, Desciption";
            //            sqlInsertLoaiKH += @",[Titlte]
            //              ,[MoTa]
            //              ,[metaCanonical]
            //              ,[metarobots]
            //              ,[metaOpenGraph]
            //              ,[metatwitter]";
            sqlInsertTitle += ")";
            sqlInsertTitle += " values(N'" + Title + "'";
            //            sqlInsertLoaiKH += @",N'"+Titlte+@"'
            //              ,N'"+MoTa+@"'
            //              ,N'"+metaCanonical+@"'
            //              ,N'"+metarobots+@"'
            //              ,N'"+metaOpenGraph+@"'
            //              ,N'"+metatwitter+@"'";
            sqlInsertTitle += ")";


            bool ktInsertTitle = Connect.Exec(sqlInsertTitle);
            if (ktInsertTitle)
            {
                Response.Redirect("DanhSachTitle.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm !')</script>");
            }

        }
        else
        {
            string sqlUpdateTitle = "update Seo set Title=N'" + Title + "'";
            sqlUpdateTitle += ",Desciption=N'" + Description + "'";
            //sqlUpdateSanPham += ",MucGia_BoLoc='" + MucGia_BoLoc + "'";
            //sqlUpdateSanPham += ",StepMucGia_BoLoc='" + MucGia_BoLoc.Replace("00", "") + "'";
            //sqlUpdateSanPham += ",LinkAnh=N'" + LinkAnh + "'";
            //sqlUpdateSanPham += ",LinkIcon=N'" + LinkIcon + "'";


            //            sqlUpdateSanPham += @",[Titlte]=N'" + Titlte + @"'
            //              ,[MoTa]=N'" + MoTa + @"'
            //              ,[metaCanonical]=N'" + metaCanonical + @"'
            //              ,[metarobots]=N'" + metarobots + @"'
            //              ,[metaOpenGraph]=N'" + metaOpenGraph + @"'
            //              ,[metatwitter]=N'" + metatwitter + @"'";



            sqlUpdateTitle += " where id_title='" + idTitle + "'";
            bool ktUpdateTitle = Connect.Exec(sqlUpdateTitle);
            if (ktUpdateTitle)
            {
                if (Page != "")
                    Response.Redirect("DanhSachTitle.aspx?Page=" + Page);
                else
                    Response.Redirect("DanhSachTitle.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("DanhSachTitle.aspx");
    }
}