﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Admin_QuanTriSeo_DanhSachTags_CapNhat : System.Web.UI.Page
{
    string idTags = "";
    string Page = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            idTags = StaticData.ValidParameter(Request.QueryString["id_tags"].Trim());
        }
        catch { }
        try
        {
            Page = StaticData.ValidParameter(Request.QueryString["Page"].Trim());
        }
        catch { }
        if (!IsPostBack)
        {
            LoadDanhSach();
        }
        
    }
    private void LoadDanhSach()
    {
        if (idTags != "")
        {
            string sql = "select * from Tags where id_tags='" + idTags + "'";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
              // Response.Redirect("https://google.com");
                dvTitle.InnerHtml = "Sửa Tags";
                btLuu.Text = "SỬA";
                txtTitle.Value = table.Rows[0]["Title"].ToString();
                txtDescription.Value = table.Rows[0]["Desciption"].ToString();

                txtDuongDan.Value = table.Rows[0]["DuongDan"].ToString();

                if (table.Rows[0]["DuongDan"].ToString() == "")
                {
                    txtDuongDan.Value = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[0]["Title"].ToString().Trim()));
                }
                else
                {
                    txtDuongDan.Value = table.Rows[0]["DuongDan"].ToString();
                }

                //txtSoThuTu.Value = table.Rows[0]["SoThuTu"].ToString();
                //txtMucGiaBoLoc.Value = decimal.Parse(table.Rows[0]["MucGia_BOLoc"].ToString().Trim().Replace(" ","0")).ToString("N0").Replace(" ", "0");
                //imgLinkAnh.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
                //imgLinkAnhIcon.Src = "../../" + table.Rows[0]["LinkIcon"].ToString();

                //txtMoTa.Value = table.Rows[0]["MoTa"].ToString();
                //txtTitlte.Value = table.Rows[0]["Titlte"].ToString();
                //txtmetaCanonical.Value = table.Rows[0]["metaCanonical"].ToString();
                //txtmetarobots.Value = table.Rows[0]["metarobots"].ToString();
                //txtmetaOpenGraph.Value = table.Rows[0]["metaOpenGraph"].ToString();
                //txtmetatwitter.Value = table.Rows[0]["metatwitter"].ToString(); 
            }
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        //string LinkIcon = imgLinkAnhIcon.Src.Replace("../../", "");
        //string LinkAnh = imgLinkAnh.Src.Replace("../../", "");
        string Title = txtTitle.Value.Trim();
        string Description = txtDescription.Value.Trim();
        string DuongDan = "";
        //string SoThuTu = txtSoThuTu.Value.Trim();
        //string MucGia_BoLoc = txtMucGiaBoLoc.Value.Trim().Replace(",","");

        //string Titlte = txtTitlte.Value.Trim();
        //string MoTa = txtMoTa.Value.Trim();
        //string metaCanonical = txtmetaCanonical.Value.Trim();
        //string metarobots = txtmetarobots.Value.Trim();
        //string metaOpenGraph = txtmetaOpenGraph.Value.Trim();
        //string metatwitter = txtmetatwitter.Value.Trim();

        if (Title == "")
        {
            Response.Write("<script>alert('Tiêu đề không được trống !')</script>");
            txtTitle.Focus();
            return;
        }

        if (Description == "")
        {
            Response.Write("<script>alert('Nội dung không được trống !')</script>");
            txtDescription.Focus();
            return;
        }
        if (txtDuongDan.Value.Trim() != "")
        {
            DuongDan = txtDuongDan.Value.Trim();
            dvDuongDan.InnerHtml = "";
        }
        else
        {
            DuongDan = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(Title.Trim()));
            return;
        }

        //////////
        if (idTags == "")
        {
            string sqlInsertTitle = "insert into Tags(Title, Desciption,DuongDan";
            //            sqlInsertLoaiKH += @",[Titlte]
            //              ,[MoTa]
            //              ,[metaCanonical]
            //              ,[metarobots]
            //              ,[metaOpenGraph]
            //              ,[metatwitter]";
            sqlInsertTitle += ")";
            sqlInsertTitle += " values(N'" + Title + "'";
            sqlInsertTitle += @",N'" + Description + "'";
            sqlInsertTitle += @",N'" + DuongDan + "'";
            //              ,N'"+MoTa+@"'
            //              ,N'"+metaCanonical+@"'
            //              ,N'"+metarobots+@"'
            //              ,N'"+metaOpenGraph+@"'
            //              ,N'"+metatwitter+@"'";
            sqlInsertTitle += ")";


            bool ktInsertTitle = Connect.Exec(sqlInsertTitle);
            if (ktInsertTitle)
            {
                Response.Redirect("DanhSachTags.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm !')</script>");
            }

        }
        else
        {
            string sqlUpdateTitle = "update Tags set Title=N'" + Title + "'";
            sqlUpdateTitle += ",Desciption=N'" + Description + "'";
            sqlUpdateTitle += ",DuongDan=N'" + DuongDan + "'";
            //sqlUpdateSanPham += ",MucGia_BoLoc='" + MucGia_BoLoc + "'";
            //sqlUpdateSanPham += ",StepMucGia_BoLoc='" + MucGia_BoLoc.Replace("00", "") + "'";
            //sqlUpdateSanPham += ",LinkAnh=N'" + LinkAnh + "'";
            //sqlUpdateSanPham += ",LinkIcon=N'" + LinkIcon + "'";


            //            sqlUpdateSanPham += @",[Titlte]=N'" + Titlte + @"'
            //              ,[MoTa]=N'" + MoTa + @"'
            //              ,[metaCanonical]=N'" + metaCanonical + @"'
            //              ,[metarobots]=N'" + metarobots + @"'
            //              ,[metaOpenGraph]=N'" + metaOpenGraph + @"'
            //              ,[metatwitter]=N'" + metatwitter + @"'";



            sqlUpdateTitle += " where id_tags='" + idTags + "'";
            bool ktUpdateTitle = Connect.Exec(sqlUpdateTitle);
            if (ktUpdateTitle)
            {
                if (Page != "")
                    Response.Redirect("DanhSachTags.aspx?Page=" + Page);
                else
                    Response.Redirect("DanhSachTags.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("DanhSachTags.aspx");
    }
}