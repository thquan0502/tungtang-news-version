﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" CodeFile="DanhSachSeoDM-CapNhat.aspx.cs" Inherits="Admin_QuanTriSeo_DanhSachSeoDM_CapNhat" %>

<%@ Register Namespace="CKEditor.NET" Assembly="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Namespace="CKFinder" Assembly="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        window.onload = function () {
            if (document.getElementById('ContentPlaceHolder1_hdHinhAnh').value != "")
                window.scrollTo(0, document.body.scrollHeight);
            document.getElementById("search-widget-wrapper").style.display = "none";
        }
        $(function () {
            CKEDITOR.config.extraPlugins = 'justify';
        });
        function format_curency(a) {
            var money = a.value.replace(/\./g, "");
            a.value = money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
        function XoaHinhAnh(id, TenAnh) {
            document.getElementById(id).style.display = "none";
            document.getElementById("ContentPlaceHolder1_hdHinhAnh").value = document.getElementById("ContentPlaceHolder1_hdHinhAnh").value.replace(TenAnh + "|~~~~|", "");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content-wrapper">
    <!-- Main content -->
        <div class="title" id="dvTitle" runat="server">CẬP NHẬT SEO DANH MỤC</div>
    <div class="title1"><a href="DanhSachSeoDM.aspx"><i class="fa fa-step-backward"></i>TRỞ VỀ</a></div>
    <section class="content">
        <div class="box">
      <div class="row">
         <!--Top Jobs-->
         <div class="col-md-12">
            <div class="top-job">
               <div class="panel jobs-board-listing with-mc no-padding no-border">
                  <div class="panel-content" id="tabChoThue" style="padding:10px">
                      <div class="job-list scrollbar m-t-lg">
                        
                          
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12">
                                                             Text :
                                                         </td>
                                                         <td>
                                                             <input id="txtMoTaNgan" placeholder="" class="form-control" runat="server" />
                                                             <div id="dvMoTaNgan" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         
                                                         <td>
                                                             <CKEditor:CKEditorControl ID="txtNoiDung" Height="300" runat="server"></CKEditor:CKEditorControl> 
                                                             <CKFinder:FileBrowser ID="FileBrowser1"   Width="0" Height="0" runat="server" OnLoad="FileBrowser1_Load"></CKFinder:FileBrowser>
                                                             <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>

                         
                      
                          <!-- Buttons-->
                          <div class="form-group">
                                             <div style="text-align:center">
                                                <asp:Button ID="btDangTin" runat="server" Text="CẬP NHẬT" class="btn btn-primary btn-block" Style="width: 110px;padding: 10px;margin: auto;" OnClick="btDangTin_Click" />
                                             </div>
                                         </div>
                      </div>
                  </div>
                   
               </div>
            </div>
         </div>
      </div>
        <input type="hidden" id="hdHinhAnh" runat="server" />
            </div>
   </section>
        </div>
    </form>
</asp:Content>

