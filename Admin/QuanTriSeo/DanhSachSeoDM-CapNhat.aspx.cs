﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriSeo_DanhSachSeoDM_CapNhat : System.Web.UI.Page
{
    string idTinTuc = "";
    protected void FileBrowser1_Load(object sender, EventArgs e)
    {
        FileBrowser1 = new CKFinder.FileBrowser();
        FileBrowser1.BasePath = "/ckfinder/";
        FileBrowser1.SetupCKEditor(txtNoiDung);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {
            //Đăng tin không cần đăng nhập
            Response.Redirect("../Home/DangNhap.aspx");
        }
        else
        {
            //idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        try
        {
            idTinTuc = Request.QueryString["idTinTuc"].Trim();
        }
        catch { }
        if (!IsPostBack)
        {
            LoadTinTuc();
          

        }
      

    }
    private void LoadTinTuc()
    {
        
            btDangTin.Text = "CẬP NHẬT";
            string sqlTinDang = "select TenDanhMuc,NoiDung from [DanhMucSeo] WHERE [IdDanhMuc]='" + idTinTuc + "' ";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            if (tbTinDang.Rows.Count > 0)
            {
                txtMoTaNgan.Value = tbTinDang.Rows[0]["TenDanhMuc"].ToString();
                txtNoiDung.Text = tbTinDang.Rows[0]["NoiDung"].ToString();  
        }
    }
    protected void btDangTin_Click(object sender, EventArgs e)
    {
       
        //    //Sửa tin đăng
        string sqlUpdateTD = "update DanhMucSeo set TenDanhMuc=N'" + txtMoTaNgan.Value.Replace("'", "") + "',NoiDung =N'" + txtNoiDung.Text.Replace("'", "") + "' where [IdDanhMuc]='" + idTinTuc + "' ";
       
        bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
        if (ktUpdateTD)
        {
            Response.Redirect("DanhSachSeoDM.aspx");
        }
        else
        {
            Response.Write("<script>alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!');</script>");
        }
        //}
    }
}