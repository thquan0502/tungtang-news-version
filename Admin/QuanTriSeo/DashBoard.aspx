﻿ <%@ Page Language="C#" EnableViewState="false" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DashBoard.aspx.cs" Inherits="Admin_QuanTriSeo_DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- bootstrap-datepicker --%>
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
   <script src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js" type="text/javascript"></script>	
    <script src="https://code.highcharts.com/highcharts.js"></script>     	
 <script>     	
     function BieuDo()	
     {	
         var TuNgay = $("#chartDayFrombd").val();	
         var DenNgay = $("#chartDayTobd").val();	
         if (TuNgay == "" && DenNgay == "") {	
             alert("Vui lòng chọn khoảng thời gian lọc!");	
         }	
         else {	
             var xmlhttp;	
             if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari	
                 xmlhttp = new XMLHttpRequest();	
             }	
             else {// code for IE6, IE5	
                 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");	
             }	
             xmlhttp.onreadystatechange = function () {	
                 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
                     if (xmlhttp.responseText == "True")	
                         alert('xin chao');	
                     document.getElementById('dvTest').innerHTML = xmlhttp.responseText;	
                     document.getElementById('chartBtnFilterbd').onclick();	
                 }	
             }	
             xmlhttp.open("GET", "../adAjax.aspx?Action=BieuDo&TuNgay=" + TuNgay + "&DenNgay=" + DenNgay, true);	
             xmlhttp.send();	
         }	
     }
	 
     function DeleteTop(idEmail) {
         var xmlhttp;
         if (confirm("Bạn có muốn xóa không ?")) {
             //alert(idSlide);
             var xmlhttp;
             if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                 xmlhttp = new XMLHttpRequest();
             }
             else {// code for IE6, IE5
                 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
             }
             xmlhttp.onreadystatechange = function () {
                 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                     if (xmlhttp.responseText == "True")
                         window.location.reload();
                 }
             }
             xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteEmail&idEmail=" + idEmail, true);
             xmlhttp.send();
         }
     }
    </script>
    <style>
       .highcharts-credits{	
            display:none;	
        }	
        .highcharts-figure,	
        .highcharts-data-table table {	
          min-width: 310px;	
          max-width: 800px;	
          margin: 1em auto;	
        }	
        #container {	
          height: 400px;	
        }	
        .highcharts-data-table table {	
          font-family: Verdana, sans-serif;	
          border-collapse: collapse;	
          border: 1px solid #ebebeb;	
          margin: 10px auto;	
          text-align: center;	
          width: 100%;	
          max-width: 500px;	
        }	
        .highcharts-data-table caption {	
          padding: 1em 0;	
          font-size: 1.2em;	
          color: #555;	
        }	
        .highcharts-data-table th {	
          font-weight: 600;	
          padding: 0.5em;	
        }	
        .highcharts-data-table td,	
        .highcharts-data-table th,	
        .highcharts-data-table caption {	
          padding: 0.5em;	
        }	
        .highcharts-data-table thead tr,	
        .highcharts-data-table tr:nth-child(even) {	
          background: #f8f8f8;	
        }	
        .highcharts-data-table tr:hover {	
          background: #f1f7ff;	
        }	
       @media (max-width:600px) {	
           .slpick-date{    	
                text-align:center;    	
                margin-left:110px;     	
                margin-top:-20px;   	
            }	
           .time-pick{	
                text-align:center;                	
                margin-right:-80px;	
            }	
           .btn{	
           }	
            .chon{	
            margin-left:90px;	
            margin-bottom: 10px;	
        }	
        .time{	
            margin-bottom:10px;	
        }	
        }	
        @media (min-width:992px) {	
            .slpick-date{	
                margin-top:-20px;	
                margin-left:30px;                	
            }	
            .time-pick{	
                text-align:right;	
                margin-left:180px;	
                margin-right:-90px;	
                margin-top:-40px;	
            }
            .button {
                background-color: #3c8dbc;
                border: none;
                color: white;
                padding: 12px 20px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                font-weight: bold;
            }
            .btn-dash{
                margin-left:5px; 
                margin-top:5px
            }
        }
    </style>
    </asp:Content>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function DeleteLink(id) {
            if (confirm("Bạn có muốn xóa không ?")) {
                //alert(idSlide);
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Không thể xóa link này !")
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteLink&id=" + id, true);
                xmlhttp.send();
            }
        }
    </script>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server" autocomplete="off">
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="title">DASHBOARD</div>
    <div class="box">
        <div class="box-body">
           <div class="form-group"> 
                        <div class="row">
                        <div class="dvnull">&nbsp;</div>
                     <div class="coninput1">                 
                            </div>
                           </div>                            
                          <div class="row">
                              <div class="col-sm-3 chon">
                                  <select id="chartSelectRange" onchange="val()" class="form-control" style="width: 150px;padding: 5px 10px;">
                                    <option value="">Tùy chỉnh</option>
                                    <option value="TODAY">Hôm nay</option>
                                    <option value="YESTERDAY">Hôm qua</option>
                                    <option value="WEEK">7 Ngày trước</option>
                                    <option value="1_MONTH">1 Tháng trước</option>
                                    <option value="3_MONTH">3 Tháng trước</option>
                                    <option value="6_MONTH">6 Tháng trước</option>
                                  </select>    
                                  </div>
                            <div class="col-sm-6 time">
                               <div id="chartRangeDays" class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                              <input id="chartDayFrom" type="hidden" class="form-control" readonly style="background-color: inherit;margin-top:5px" />                                   
                                   <input id="txtTuNgay" type="text" runat="server" readonly class="form-control"  style="background-color: inherit;" />                                                                
                              <span class="input-group-addon">&nbsp;&nbsp;đến&nbsp;&nbsp;</span>
                              <input id="chartDayTo" type="hidden" class="form-control" readonly style="background-color: inherit;visibility:visible" />
                                   <input id="txtDenNgay" type="text" runat="server" readonly class="form-control"  style="background-color: inherit;" />                                                                
                        </div>    
                                <input id="txtsl" runat="server" style="display:none" />                                                       
                            </div>                              
                            <div class="col-sm-3">
                                <div style="text-align: center;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click"/>
                                </div>
</div>      
                              </div> 
                </div>     
            </div>           
        <script>    
            function val() {
                d = document.getElementById("chartSelectRange").value;
                document.getElementById('<%= txtsl.ClientID %>').value = d;                
            }           
        </script>      
        <script>
            (function () {
                var plot_statistics;
                var dataset = [];
                var rawDataset = null;
                var fnDateToDash = function (date) {
                    return "" + date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).substr(-2) + "-" + ("0" + date.getDate()).substr(-2);
                }
                var fnDateToSlash = function (date) {
                    return "" + ("0" + date.getDate()).substr(-2) + "/" + ("0" + (date.getMonth() + 1)).substr(-2) + "/" + date.getFullYear();
                }
                var fnDashToSlash = function (sDash) {
                    var date = new Date(sDash);
                    if (!isNaN(date)) {
                        return fnDateToSlash(date);
                    } else {
                        return "";
                    }
                }
                var fnSlashToDash = function (sSlash) {
                    var rs = "";
                    if (sSlash != undefined && sSlash != null && sSlash != "") {
                        sSlash = sSlash.trim();
                        var parts = sSlash.split("/");
                        if (parts.length == 3) {
                            return parts[2] + "-" + parts[1] + "-" + parts[0];
                        }
                    }
                    return rs;
                }
                var fnDateToStringFormat2 = function (date) { // MM/YYYY
                    return "" + ("0" + (date.getMonth() + 1)).substr(-2) + "/" + date.getFullYear();
                }
                var fnDateToStringFormat3 = function (date1) {// YYYY
                    return "" + date1.getFullYear();
                }
                var fnDateToStringFormat4 = function (date) { // DD/MM
                    return "" + ("0" + date.getDate()).substr(-2) + "/" + ("0" + (date.getMonth() + 1)).substr(-2);
                }
                var fnDateToStringFormat5 = function (date1, date2) { // MM/YYYY -  MM/YYYY
                    return "" + ("0" + (date1.getMonth() + 1)).substr(-2) + "/" + date1.getFullYear() + "-" + ("0" + (date2.getMonth() + 1)).substr(-2) + "/" + date2.getFullYear();
                }
                var fnDateToStringFormat6 = function (date) { // MM
                    return "" + ("0" + (date.getMonth() + 1)).substr(-2);
                }
                var fnDateToStringFormat7 = function (date1, date2) { // DD/MM - DD/MM
                    return "" + fnDateToStringFormat4(date1) + "-" + fnDateToStringFormat4(date2);
                }
                var fnDateToStringFormat8 = function (date1) { // DD/MM/YYYY
                    return "" + ("0" + date1.getDate()).substr(-2) + "/" + ("0" + (date1.getMonth() + 1)).substr(-2) + "/" + date1.getFullYear();
                }
                var showChartTooltip = function (x, y, xValue, yValue, item) {
                    var percent = null;
                    if (item.dataIndex > 0) {
                        if (dataset[0][1] > 0) {
                            percent = (dataset[item.dataIndex][1] * 100) / dataset[0][1];
                            percent = - (100 - percent);
                        }
                    }
                    html = '';
                    var heightBlockText = 22;
                    var totalHeight = 40 - heightBlockText;
                    html += `<div id="tooltip" class="chart-tooltip">`;
                    html += `<div>Thời gian: ${dataset[item.dataIndex][0]}</div>`;
                    totalHeight += heightBlockText;
                    html += `<div>Lượt xem: <span class="highlight">${yValue}</span></div>`;
                    totalHeight += heightBlockText;
                    if (percent != null) {
                        var iconHtml = "";
                        if (percent >= 0) {
                            iconHtml += `Tăng:&nbsp;`;
                        } else {
                            iconHtml += `Giảm:&nbsp;`;
                        }
                        html += `<div>` + iconHtml + `<span class='highlight'>${parseFloat(percent).toFixed(2)}</span> %</div>`;
                        totalHeight += heightBlockText;
                    }
                    html += `</div>`;
                    $(html).css({
                        position: 'absolute',
                        display: 'none',
                        top: y - totalHeight,
                        left: x - 40,
                        border: '0px solid #ccc',
                        'background-color': '#fff'
                    }).appendTo("body").fadeIn(200);
                };
                var showChart = function (isFirst) {
                    $('#site_statistics_content').hide();
                    $('#site_statistics_loading').show();
                    var funFlot = function () {
                        $('#site_statistics').html('');
                        $('#site_statistics_loading').hide();
                        $('#site_statistics_content').show();
                        plot_statistics = $.plot(
                            $("#site_statistics"),
                            [
                                {
                                    data: dataset,
                                    lines: {
                                        fill: 0.6,
                                        lineWidth: 0
                                    },
                                    color: ['#f89f9f']
                                },
                                {
                                    data: dataset,
                                    points: {
                                        show: true,
                                        fill: true,
                                        radius: 5,
                                        fillColor: "#f89f9f",
                                        lineWidth: 3
                                    },
                                    color: '#fff',
                                    shadowSize: 0
                                },
                            ],
                            {
                                xaxis: {
                                    tickLength: 0,
                                    tickDecimals: 0,
                                    mode: "categories",
                                    min: 0,
                                    font: {
                                        lineHeight: 14,
                                        style: "normal",
                                        variant: "small-caps",
                                        color: "#6F7B8A"
                                    }
                                },
                                yaxis: {
                                    ticks: 5,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                    font: {
                                        lineHeight: 14,
                                        style: "normal",
                                        variant: "small-caps",
                                        color: "#6F7B8A"
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: "#eee",
                                    borderColor: "#eee",
                                    borderWidth: 1
                                }
                            }
                        );
                        var previousPoint = null;
                        $("#site_statistics").bind("plothover", function (event, pos, item) {
                            $("#x").text(pos.x.toFixed(2));
                            $("#y").text(pos.y.toFixed(2));
                            if (item) {
                                if (previousPoint != item.dataIndex) {
                                    previousPoint = item.dataIndex;

                                    $("#tooltip").remove();
                                    var x = item.datapoint[0].toFixed(2),
                                        y = item.datapoint[1].toFixed(2);

                                    showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1], item);
                                }
                            } else {
                                $("#tooltip").remove();
                                previousPoint = null;
                            }
                        });
                    }
                    var inputFrom, inputTo;
                    if (isFirst) {
                        inputTo = new Date();
                        inputFrom = new Date(inputTo);
                        inputFrom.setDate(inputFrom.getDate() - 7);
                        inputTo = fnDateToDash(inputTo);
                        <%--//(document.getElementById('<%= txtDenNgay.ClientID %>').value).fnDateToStringFormat8 = inputTo;--%>
                        inputFrom = fnDateToDash(inputFrom);
                        <%--//(document.getElementById('<%= txtTuNgay.ClientID %>').value).fnDateToStringFormat8 = inputFrom;--%>
                    } else {
                        inputTo = $('#chartDayTo').val();
                        inputTo = fnSlashToDash(inputTo);
                        <%--//(document.getElementById('<%= txtDenNgay.ClientID %>').value).fnDateToStringFormat8 = inputTo;--%>
                        inputFrom = $('#chartDayFrom').val();
                        inputFrom = fnSlashToDash(inputFrom);
                        <%--//(document.getElementById('<%= txtTuNgay.ClientID %>').value).fnDateToStringFormat8 = inputFrom;--%>
                    }
                    $.ajax({
                        type: 'GET',
                        url: "/AccountAjax.aspx?Action=ChartData",
                        data: {
                            from: inputFrom,
                            to: inputTo,
                        },
                        success: function success(res) {
                            $('#chartWidgetReferralLink').attr("href", "/tai-khoan/cong-tac?FilterOwnerApprovedAtFrom=" + res.sBeforeDate + "&FilterOwnerApprovedAtTo=" + res.sAfterDate);
                            $('#chartWidgetCommissionRealLink').attr("href", "/tai-khoan/cong-tac?FilterReferralStatus=FEE_COMMISSION&FilterOwnerApprovedAtFrom=" + res.sBeforeDate + "&FilterOwnerApprovedAtTo=" + res.sAfterDate);

                            var chartWidgetViewNum = 0;
                            // $('#chartSelectRange').val("");
                            dataset = [];
                            rawDataset = res;
                            var firstDate = new Date(res.sBeforeDate);
                            firstDate.setHours(0, 0, 0, 0);
                            var lastDate = new Date(res.sAfterDate);
                            lastDate.setHours(0, 0, 0, 0);

                            fnSetRangeDays(firstDate, lastDate);

                            var type = null;

                            var miliDiff = lastDate - firstDate;
                            var miliPerDay = 1000 * 60 * 60 * 24;

                            if (fnDateToDash(firstDate) == fnDateToDash(lastDate)) {
                                type = 'PER_HOUR';
                            } else if ((miliDiff / miliPerDay) <= 10) {
                                type = 'PER_DAY';
                            } else if ((miliDiff / miliPerDay) <= (30 * 3)) {
                                type = 'PER_WEEK';
                            } else if ((miliDiff / miliPerDay) <= (30 * 12)) {
                                type = 'PER_MONTH';
                            } else if ((miliDiff / miliPerDay) <= (30 * 12 * 3)) {
                                type = 'PER_QUARTER';
                            } else {
                                type = 'PER_YEAR';
                            }

                            var indexDate = null;
                            var index = 0;

                            if (index < res.data.length) {
                                indexDate = new Date(res.data[index]["date"]);
                            } else {
                                indexDate = null;
                            }

                            var sumVal = 0;
                            var weekMockTo = null;
                            var weekMockFrom = null;
                            var monthMockTo = null;
                            var monthMockFrom = null;
                            var quarterMockTo = null;
                            var quarterMockFrom = null;
                            var yearMockTo = null;
                            var yearMockFrom = null;
                            if (type == 'PER_HOUR') {
                                var tmpTotal = 0;
                                res.data.forEach(function (ele, position) {
                                    chartWidgetViewNum += parseInt(ele.value);
                                    tmpTotal += parseInt(ele.value);
                                    if (position % 2 == 1) {
                                        dataset.push([
                                            (ele.date - 1) + "-" + (ele.date + 1) + "H",
                                            tmpTotal
                                        ]);
                                        tmpTotal = 0;
                                    }
                                });
                                funFlot();
                                $('#chartWidgetViewNum').html(chartWidgetViewNum);
                                $('#chartWidgetReferralNum').html(res.estimateReferral);
                                $('#chartWidgetCommissionRealNum').html(res.estimateCommissionReal);
                                return;
                            } else if (type == 'PER_DAY') {

                            } else if (type == 'PER_WEEK') {
                                sumVal = 0;
                                weekMockTo = new Date(firstDate);
                                weekMockFrom = new Date(firstDate);
                                weekMockTo.setDate(weekMockTo.getDate() + 7);
                                if (fnDateToDash(weekMockTo) > fnDateToDash(lastDate)) {
                                    weekMockTo = new Date(lastDate);
                                }
                            } else if (type == 'PER_MONTH') {
                                monthMockFrom = new Date(firstDate);
                                monthMockTo = new Date(monthMockFrom.getFullYear(), monthMockFrom.getMonth() + 1, 0);
                                if (fnDateToDash(monthMockTo) > fnDateToDash(lastDate)) {
                                    monthMockTo = new Date(lastDate);
                                }
                            } else if (type == 'PER_QUARTER') {
                                quarterMockFrom = new Date(firstDate);
                                quarterMockTo = new Date(quarterMockFrom);
                                quarterMockTo.setDate(1);
                                while ([2, 5, 8, 11].indexOf(quarterMockTo.getMonth()) < 0) {
                                    quarterMockTo.setMonth(quarterMockTo.getMonth() + 1);
                                }
                                quarterMockTo = new Date(quarterMockTo.getFullYear(), quarterMockTo.getMonth() + 1, 0);
                                if (fnDateToDash(quarterMockTo) > fnDateToDash(lastDate)) {
                                    quarterMockTo = new Date(lastDate);
                                }
                            } else {
                                yearMockFrom = new Date(firstDate);
                                yearMockTo = new Date(yearMockFrom.getFullYear(), 11, 31);
                                if (fnDateToDash(yearMockTo) > fnDateToDash(lastDate)) {
                                    yearMockTo = new Date(lastDate);
                                }
                            }
                            while (fnDateToDash(firstDate) <= fnDateToDash(lastDate)) {
                                var valDate = "", valValue = 0;

                                if (indexDate != null && fnDateToDash(indexDate) == fnDateToDash(firstDate)) {
                                    valValue = parseInt(res.data[index]["value"]);

                                    index = index + 1;
                                    if (index < res.data.length) {
                                        indexDate = new Date(res.data[index]["date"]);
                                    } else {
                                        indexDate = null;
                                    }
                                } else {
                                    valValue = 0;
                                }

                                chartWidgetViewNum += valValue;

                                if (type == 'PER_DAY') {
                                    dataset.push([
                                        fnDateToStringFormat4(firstDate),
                                        valValue
                                    ]);
                                } else if (type == 'PER_WEEK') {
                                    sumVal += valValue;
                                    if (fnDateToDash(weekMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat7(weekMockFrom, weekMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        weekMockFrom = new Date(firstDate);
                                        weekMockFrom.setDate(weekMockFrom.getDate() + 1);
                                        weekMockTo.setDate(weekMockTo.getDate() + 7);
                                        if (fnDateToDash(weekMockTo) > fnDateToDash(lastDate)) {
                                            weekMockTo = new Date(lastDate);
                                        }
                                    } else {

                                    }
                                } else if (type == 'PER_MONTH') {
                                    sumVal += valValue;
                                    if (fnDateToDash(monthMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat2(monthMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        monthMockFrom = new Date(firstDate);
                                        monthMockFrom.setDate(monthMockFrom.getDate() + 1);
                                        monthMockTo = new Date(monthMockFrom.getFullYear(), monthMockFrom.getMonth() + 1, 0);
                                        if (fnDateToDash(monthMockTo) > fnDateToDash(lastDate)) {
                                            monthMockTo = new Date(lastDate);
                                        }
                                    }
                                } else if (type == 'PER_QUARTER') {
                                    sumVal += valValue;
                                    if (fnDateToDash(quarterMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat5(quarterMockFrom, quarterMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        quarterMockFrom = new Date(firstDate);
                                        quarterMockFrom.setDate(quarterMockFrom.getDate() + 1);
                                        quarterMockTo = new Date(quarterMockFrom);
                                        quarterMockTo.setDate(1);
                                        while ([2, 5, 8, 11].indexOf(quarterMockTo.getMonth()) < 0) {
                                            quarterMockTo.setMonth(quarterMockTo.getMonth() + 1);
                                        }
                                        quarterMockTo = new Date(quarterMockTo.getFullYear(), quarterMockTo.getMonth() + 1, 0);
                                        if (fnDateToDash(quarterMockTo) > fnDateToDash(lastDate)) {
                                            quarterMockTo = new Date(lastDate);
                                        }
                                    }
                                } else {
                                    sumVal += valValue;
                                    if (fnDateToDash(yearMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat3(yearMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        yearMockFrom = new Date(firstDate);
                                        yearMockFrom.setDate(yearMockFrom.getDate() + 1);
                                        yearMockTo = new Date(yearMockFrom.getFullYear(), 11, 31);
                                        if (fnDateToDash(yearMockTo) > fnDateToDash(lastDate)) {
                                            yearMockTo = new Date(lastDate);
                                        }
                                    }
                                }

                                firstDate.setDate(firstDate.getDate() + 1);
                            }
                            funFlot();
                            $('#chartWidgetViewNum').html(chartWidgetViewNum);
                            $('#chartWidgetReferralNum').html(res.estimateReferral);
                            $('#chartWidgetCommissionRealNum').html(res.estimateCommissionReal);
                            fnCheckRangeToSelect();
                        },
                        error: function error(data) {

                        }
                    });

                }
                var fnSetRangeDays = function (dayx, dayy) {
                    dayx.setHours(0, 0, 0, 0);
                    dayy.setHours(0, 0, 0, 0);

                    $('#chartDayFrom').datepicker('setDate', dayx);
                    $('#chartDayTo').datepicker('setDate', dayy);

                    fnCheckRangeToSelect();
                }
                //window.CustomModalJQuery.onOpens.push(function (obj) {
                //    if(obj.anchor[0].id == "modalChart"){
                //        showChart(true);
                //    }
                //});
                //window.CustomModalJQuery.onCloses.push(function (obj) {
                //    if(obj.anchor[0].id == "modalChart"){
                //        plot_statistics.destroy();
                //    }
                //});
                $('#chartRangeDays').datepicker({
                    language: 'vi',
                    orientation: "left",
                    autoclose: true
                });
                $('#btTimKiem').click(function () {
                    showChart(false);
                });
                $('#chartSelectRange').on("change", function () {
                    var val = $('#chartSelectRange').val();
                    var from = null, to = null;
                    switch (val) {
                        case '':
                            to = '';
                            from = '';
                            break;
                        case 'TODAY':
                            to = new Date();
                            from = new Date(to);
                            break;
                        case 'YESTERDAY':
                            to = new Date();
                            from = new Date(to);
                            to.setDate(to.getDate() - 1);
                            from.setDate(from.getDate() - 1);
                            break;
                        case 'WEEK':
                            to = new Date();
                            from = new Date(to);
                            from.setDate(from.getDate() - 7);
                            break;
                        case '1_MONTH':
                            to = new Date();
                            from = new Date(to);
                            from.setMonth(from.getMonth() - 1);
                            break;
                        case '3_MONTH':
                            to = new Date();
                            from = new Date(to);
                            from.setMonth(from.getMonth() - 3);
                            break;
                        case '6_MONTH':
                            to = new Date();
                            from = new Date(to);
                            from.setMonth(from.getMonth() - 6);
                            break;

                        default:
                            break;
                    }
                    if (from != null && to != null) {
                        fnSetRangeDays(from, to);
                        $('#btTimKiem').click();
                    }
                    fnCheckRangeToSelect();
                });
                var fnPatternToday = function () {
                    var to = new Date();
                    var from = new Date(to);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPatternYesterday = function () {
                    var to = new Date();
                    var from = new Date(to);
                    to.setDate(to.getDate() - 1);
                    from.setDate(from.getDate() - 1);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPatternWeek = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setDate(from.getDate() - 7);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPattern1Month = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setMonth(from.getMonth() - 1);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPattern3Month = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setMonth(from.getMonth() - 3);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPattern6Month = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setMonth(from.getMonth() - 6);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnCheckRangeToSelect = function () {
                    var value = "";
                    var rangeFrom = $('#chartDayFrom').val();
                    rangeFrom = fnSlashToDash(rangeFrom);
                    rangeFrom = new Date(rangeFrom);
                   <%--// (document.getElementById('<%= txtTuNgay.ClientID %>').value).fnDateToStringFormat8 = rangeFrom;--%>
                    var rangeTo = $('#chartDayTo').val();
                    rangeTo = fnSlashToDash(rangeTo);
                    rangeTo = new Date(rangeTo);
                   <%--// (document.getElementById('<%= txtDenNgay.ClientID %>').value).fnDateToStringFormat8 = rangeTo;--%>
                    var pattern = fnDateToDash(rangeFrom) + ":" + fnDateToDash(rangeTo);

                    if (pattern == fnPatternToday()) {
                        value = 'TODAY';
                    } else if (pattern == fnPatternYesterday()) {
                        value = 'YESTERDAY';
                    } else if (pattern == fnPatternWeek()) {
                        value = 'WEEK';
                    } else if (pattern == fnPattern1Month()) {
                        value = '1_MONTH';
                    } else if (pattern == fnPattern3Month()) {
                        value = '3_MONTH';
                    } else if (pattern == fnPattern6Month()) {
                        value = '6_MONTH';
                    } else {
                        value = '';
                    }
                    $('#chartSelectRange').val(value);
                    document.getElementById('<%= txtTuNgay.ClientID %>').value = document.getElementById('chartDayFrom').value
                    document.getElementById('<%= txtDenNgay.ClientID %>').value = document.getElementById('chartDayTo').value                
                }                                                

            })();
        </script>
            <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                                   <div class="col-md-12" style="margin-top: 10px">
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                      <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalBieuDo6"><img src="../../Images/chart1.png" width="50px" /> Biểu đồ</button>
                                        </div>
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                      <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><img src="../../Images/city.png" width="50px" />Tin đăng</button>
                                        </div>
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                       <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter1"><img src="../../Images/user-dk.png" width="50px" /> User đăng ký</button></li>
                                      </div>
                                       <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                       <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter4"><img src="../../Images/time.png" width="50px" /> Giờ đăng bài</button></li>
                                      </div>
                                       <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                       <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter2"><img src="../../Images/danhmuc2.png" width="50px" /> Danh mục cấp 1</button></li>
                                       </div>
                                       <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                       <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter3"><img src="../../Images/danhmuc2.png" width="50px" /> Danh mục cấp 2</button></li>
                                       </div>
                                       <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                       <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter5"><img src="../../Images/baidang1.png" width="50px" /> Bài đăng 1 ngày</button></li>
                                       </div>
                                       <div class="col-lg-3 col-md-3 col-sm-6 col-12" style="margin-top:10px;">
                                       <button type="button" style="width:100%;" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter6"><img src="../../Images/userpost.png" width="50px" /> Số người đăng 1 ngày</button></li>                                                                        
                                       </div>
                              </div>                                                  
              </div>                           
                <label id="lblsolan1" runat="server" style="display:none"></label>
                <label id="lblsolan23" runat="server" style="display:none"></label>
                <label id="lblsolan45" runat="server" style="display:none"></label>
                <label id="lblsolan610" runat="server" style="display:none"></label>
                <label id="lblsolan1120" runat="server" style="display:none"></label>
                <label id="lblsolan20" runat="server" style="display:none"></label>
                <!-- Top 10 tỉnh thành đăng tin nhiều -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle"><img src="../../Images/city.png" width="30px" /> Top 10 tỉnh thành có tin đăng nhiều</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dvTop10TinDang" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div> 
            <!-- Top 10 tỉnh thành mà user đăng ký nhiều -->
                <div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle1"><img src="../../Images/user-dk.png" width="30px" /> Top 10 tỉnh thành có user đăng ký nhiều</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dvTop10user" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>       
            <%--Top 10 danh mục được chọn đăng nhiều nhất(danh mục cấp 1)--%>
            <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle2"><img src="../../Images/danhmuc2.png" width="30px" /> Top 10 danh mục cấp 1 được chọn đăng nhiều</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dvTop10DMC1" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>  
            <%--Top 10 danh mục được chọn đăng nhiều nhất(danh mục cấp 2)--%>
            <div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle3"><img src="../../Images/danhmuc2.png" width="30px" /> Top 10 danh mục cấp 2 được chọn đăng nhiều</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dvTop10DMC2" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>    
            <%--Giờ đăng bài nhiều--%>
            <div class="modal fade" id="exampleModalCenter4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle4"><img src="../../Images/time.png" width="30px" /> Giờ đăng bài nhiều trong ngày</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dvGioDbNhieu" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>  
            <%--Một ngày bao nhiêu bài đăng--%>
            <div class="modal fade" id="exampleModalCenter5" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle5"><img src="../../Images/baidang1.png" width="30px" /> Số bài đăng một ngày</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dv1nbnb" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>    
            <%--Một ngày bao nhiêu số người đăng--%>
            <div class="modal fade" id="exampleModalCenter6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongTitle6"><img src="../../Images/userpost.png" width="30px" /> Số người đăng bài một ngày</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="dv1nbndb" runat="server">

                       </div> 
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>                               
                <%--Biểu đồ--%>
            <div class="modal fade" id="exampleModalBieuDo6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" style="font-size:18px;font-weight:bold;font-family:inherit;color:#444" id="exampleModalLongBieuDo"><img src="../../Images/chart1.png" width="30px" /> Biểu Đồ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" style="margin-top:-20px">                        
                        <figure class="highcharts-figure">
                          <div id="container"></div>
                          
                        </figure>
                      </div>
                        <div class="chart-foot row" style="margin: 9px 0">
                    <div class="col-mobile">
                      <select id="chartSelectRangebd" class="form-control slpick-date" style="width: 150px;padding: 5px 10px;margin-bottom: 6px;">
                        <option value="">Tùy chỉnh</option>
                        <option value="TODAY">Hôm nay</option>
                        <option value="YESTERDAY">Hôm qua</option>
                        <option value="WEEK">7 Ngày trước</option>
                        <option value="1_MONTH">1 Tháng trước</option>
                        <option value="3_MONTH">3 Tháng trước</option>
                        <option value="6_MONTH">6 Tháng trước</option>
                      </select>
                    </div>
                    <div class ="col-mobile time-pick">
                    <div class="col-xs-10 col-mb-chart">
                        <div id="chartRangeDaysbd" class="input-group input-large date-picker input-daterange col-mb-day" data-date-format="dd/mm/yyyy">
                          <input id="chartDayFrombd" type="text" class="form-control" readonly style="background-color: inherit;" />
                          <span class="input-group-addon">&nbsp;&nbsp;đến&nbsp;&nbsp;</span>
                          <input id="chartDayTobd" type="text" class="form-control" readonly style="background-color: inherit;" />
                        </div>
                    </div>                    
                    </div>
                </div>
                        <div id="dvTest" style="display:none"></div>    
                      <div class="modal-footer">
                          <input id="chartBtnFilterbd1" onclick="BieuDo()" type="button" class="btn btn-primary chart-filter-btn" value="Lọc" />
                        <input id="chartBtnFilterbd" type="button" style="display:none" class="btn btn-primary chart-filter-btn" value="Lọc" />
                        <button type="button" class="btn btn-secondary" style="border:1px solid black" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
               
         </div>
        </div>        
        </div>
        <script>
            (function () {
                var plot_statistics;
                var dataset = [];
                var rawDataset = null;
                var fnDateToDash = function (date) {
                    return "" + date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).substr(-2) + "-" + ("0" + date.getDate()).substr(-2);
                }
                var fnDateToSlash = function (date) {
                    return "" + ("0" + date.getDate()).substr(-2) + "/" + ("0" + (date.getMonth() + 1)).substr(-2) + "/" + date.getFullYear();
                }
                var fnDashToSlash = function (sDash) {
                    var date = new Date(sDash);
                    if (!isNaN(date)) {
                        return fnDateToSlash(date);
                    } else {
                        return "";
                    }
                }
                var fnSlashToDash = function (sSlash) {
                    var rs = "";
                    if (sSlash != undefined && sSlash != null && sSlash != "") {
                        sSlash = sSlash.trim();
                        var parts = sSlash.split("/");
                        if (parts.length == 3) {
                            return parts[2] + "-" + parts[1] + "-" + parts[0];
                        }
                    }
                    return rs;
                }
                var fnDateToStringFormat2 = function (date) { // MM/YYYY
                    return "" + ("0" + (date.getMonth() + 1)).substr(-2) + "/" + date.getFullYear();
                }
                var fnDateToStringFormat3 = function (date1) {// YYYY
                    return "" + date1.getFullYear();
                }
                var fnDateToStringFormat4 = function (date) { // DD/MM
                    return "" + ("0" + date.getDate()).substr(-2) + "/" + ("0" + (date.getMonth() + 1)).substr(-2);
                }
                var fnDateToStringFormat5 = function (date1, date2) { // MM/YYYY -  MM/YYYY
                    return "" + ("0" + (date1.getMonth() + 1)).substr(-2) + "/" + date1.getFullYear() + "-" + ("0" + (date2.getMonth() + 1)).substr(-2) + "/" + date2.getFullYear();
                }
                var fnDateToStringFormat6 = function (date) { // MM
                    return "" + ("0" + (date.getMonth() + 1)).substr(-2);
                }
                var fnDateToStringFormat7 = function (date1, date2) { // DD/MM - DD/MM
                    return "" + fnDateToStringFormat4(date1) + "-" + fnDateToStringFormat4(date2);
                }
                var fnDateToStringFormat8 = function (date1) { // DD/MM/YYYY
                    return "" + ("0" + date1.getDate()).substr(-2) + "/" + ("0" + (date1.getMonth() + 1)).substr(-2) + "/" + date1.getFullYear();
                }
                var showChartTooltip = function (x, y, xValue, yValue, item) {
                    var percent = null;
                    if (item.dataIndex > 0) {
                        if (dataset[0][1] > 0) {
                            percent = (dataset[item.dataIndex][1] * 100) / dataset[0][1];
                            percent = - (100 - percent);
                        }
                    }                    
                    var inputFrom, inputTo;
                    if (isFirst) {
                        inputTo = new Date();
                        inputFrom = new Date(inputTo);
                        inputFrom.setDate(inputFrom.getDate() - 7);
                        inputTo = fnDateToDash(inputTo);
                        inputFrom = fnDateToDash(inputFrom);
                    } else {
                        inputTo = $('#chartDayTobd').val();
                        inputTo = fnSlashToDash(inputTo);
                        inputFrom = $('#chartDayFrombd').val();
                        inputFrom = fnSlashToDash(inputFrom);
                    }
                    $.ajax({
                        type: 'GET',
                        url: "/AccountAjax.aspx?Action=ChartData",
                        data: {
                            from: inputFrom,
                            to: inputTo,
                        },
                        success: function success(res) {
                            $('#chartWidgetReferralLink').attr("href", "/tai-khoan/cong-tac?FilterOwnerApprovedAtFrom=" + res.sBeforeDate + "&FilterOwnerApprovedAtTo=" + res.sAfterDate);
                            $('#chartWidgetCommissionRealLink').attr("href", "/tai-khoan/cong-tac?FilterReferralStatus=FEE_COMMISSION&FilterOwnerApprovedAtFrom=" + res.sBeforeDate + "&FilterOwnerApprovedAtTo=" + res.sAfterDate);

                            var chartWidgetViewNum = 0;
                            // $('#chartSelectRange').val("");
                            dataset = [];
                            rawDataset = res;
                            var firstDate = new Date(res.sBeforeDate);
                            firstDate.setHours(0, 0, 0, 0);
                            var lastDate = new Date(res.sAfterDate);
                            lastDate.setHours(0, 0, 0, 0);

                            fnSetRangeDays(firstDate, lastDate);

                            var type = null;

                            var miliDiff = lastDate - firstDate;
                            var miliPerDay = 1000 * 60 * 60 * 24;

                            if (fnDateToDash(firstDate) == fnDateToDash(lastDate)) {
                                type = 'PER_HOUR';
                            } else if ((miliDiff / miliPerDay) <= 10) {
                                type = 'PER_DAY';
                            } else if ((miliDiff / miliPerDay) <= (30 * 3)) {
                                type = 'PER_WEEK';
                            } else if ((miliDiff / miliPerDay) <= (30 * 12)) {
                                type = 'PER_MONTH';
                            } else if ((miliDiff / miliPerDay) <= (30 * 12 * 3)) {
                                type = 'PER_QUARTER';
                            } else {
                                type = 'PER_YEAR';
                            }

                            var indexDate = null;
                            var index = 0;

                            if (index < res.data.length) {
                                indexDate = new Date(res.data[index]["date"]);
                            } else {
                                indexDate = null;
                            }

                            var sumVal = 0;
                            var weekMockTo = null;
                            var weekMockFrom = null;
                            var monthMockTo = null;
                            var monthMockFrom = null;
                            var quarterMockTo = null;
                            var quarterMockFrom = null;
                            var yearMockTo = null;
                            var yearMockFrom = null;
                            if (type == 'PER_HOUR') {
                                var tmpTotal = 0;
                                res.data.forEach(function (ele, position) {
                                    chartWidgetViewNum += parseInt(ele.value);
                                    tmpTotal += parseInt(ele.value);
                                    if (position % 2 == 1) {
                                        dataset.push([
                                            (ele.date - 1) + "-" + (ele.date + 1) + "H",
                                            tmpTotal
                                        ]);
                                        tmpTotal = 0;
                                    }
                                });
                                funFlot();
                                $('#chartWidgetViewNum').html(chartWidgetViewNum);
                                $('#chartWidgetReferralNum').html(res.estimateReferral);
                                $('#chartWidgetCommissionRealNum').html(res.estimateCommissionReal);
                                return;
                            } else if (type == 'PER_DAY') {

                            } else if (type == 'PER_WEEK') {
                                sumVal = 0;
                                weekMockTo = new Date(firstDate);
                                weekMockFrom = new Date(firstDate);
                                weekMockTo.setDate(weekMockTo.getDate() + 7);
                                if (fnDateToDash(weekMockTo) > fnDateToDash(lastDate)) {
                                    weekMockTo = new Date(lastDate);
                                }
                            } else if (type == 'PER_MONTH') {
                                monthMockFrom = new Date(firstDate);
                                monthMockTo = new Date(monthMockFrom.getFullYear(), monthMockFrom.getMonth() + 1, 0);
                                if (fnDateToDash(monthMockTo) > fnDateToDash(lastDate)) {
                                    monthMockTo = new Date(lastDate);
                                }
                            } else if (type == 'PER_QUARTER') {
                                quarterMockFrom = new Date(firstDate);
                                quarterMockTo = new Date(quarterMockFrom);
                                quarterMockTo.setDate(1);
                                while ([2, 5, 8, 11].indexOf(quarterMockTo.getMonth()) < 0) {
                                    quarterMockTo.setMonth(quarterMockTo.getMonth() + 1);
                                }
                                quarterMockTo = new Date(quarterMockTo.getFullYear(), quarterMockTo.getMonth() + 1, 0);
                                if (fnDateToDash(quarterMockTo) > fnDateToDash(lastDate)) {
                                    quarterMockTo = new Date(lastDate);
                                }
                            } else {
                                yearMockFrom = new Date(firstDate);
                                yearMockTo = new Date(yearMockFrom.getFullYear(), 11, 31);
                                if (fnDateToDash(yearMockTo) > fnDateToDash(lastDate)) {
                                    yearMockTo = new Date(lastDate);
                                }
                            }
                            while (fnDateToDash(firstDate) <= fnDateToDash(lastDate)) {
                                var valDate = "", valValue = 0;

                                if (indexDate != null && fnDateToDash(indexDate) == fnDateToDash(firstDate)) {
                                    valValue = parseInt(res.data[index]["value"]);

                                    index = index + 1;
                                    if (index < res.data.length) {
                                        indexDate = new Date(res.data[index]["date"]);
                                    } else {
                                        indexDate = null;
                                    }
                                } else {
                                    valValue = 0;
                                }

                                chartWidgetViewNum += valValue;

                                if (type == 'PER_DAY') {
                                    dataset.push([
                                        fnDateToStringFormat4(firstDate),
                                        valValue
                                    ]);
                                } else if (type == 'PER_WEEK') {
                                    sumVal += valValue;
                                    if (fnDateToDash(weekMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat7(weekMockFrom, weekMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        weekMockFrom = new Date(firstDate);
                                        weekMockFrom.setDate(weekMockFrom.getDate() + 1);
                                        weekMockTo.setDate(weekMockTo.getDate() + 7);
                                        if (fnDateToDash(weekMockTo) > fnDateToDash(lastDate)) {
                                            weekMockTo = new Date(lastDate);
                                        }
                                    } else {

                                    }
                                } else if (type == 'PER_MONTH') {
                                    sumVal += valValue;
                                    if (fnDateToDash(monthMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat2(monthMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        monthMockFrom = new Date(firstDate);
                                        monthMockFrom.setDate(monthMockFrom.getDate() + 1);
                                        monthMockTo = new Date(monthMockFrom.getFullYear(), monthMockFrom.getMonth() + 1, 0);
                                        if (fnDateToDash(monthMockTo) > fnDateToDash(lastDate)) {
                                            monthMockTo = new Date(lastDate);
                                        }
                                    }
                                } else if (type == 'PER_QUARTER') {
                                    sumVal += valValue;
                                    if (fnDateToDash(quarterMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat5(quarterMockFrom, quarterMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        quarterMockFrom = new Date(firstDate);
                                        quarterMockFrom.setDate(quarterMockFrom.getDate() + 1);
                                        quarterMockTo = new Date(quarterMockFrom);
                                        quarterMockTo.setDate(1);
                                        while ([2, 5, 8, 11].indexOf(quarterMockTo.getMonth()) < 0) {
                                            quarterMockTo.setMonth(quarterMockTo.getMonth() + 1);
                                        }
                                        quarterMockTo = new Date(quarterMockTo.getFullYear(), quarterMockTo.getMonth() + 1, 0);
                                        if (fnDateToDash(quarterMockTo) > fnDateToDash(lastDate)) {
                                            quarterMockTo = new Date(lastDate);
                                        }
                                    }
                                } else {
                                    sumVal += valValue;
                                    if (fnDateToDash(yearMockTo) == fnDateToDash(firstDate)) {
                                        dataset.push([
                                            fnDateToStringFormat3(yearMockTo),
                                            sumVal
                                        ]);
                                        sumVal = 0;
                                        yearMockFrom = new Date(firstDate);
                                        yearMockFrom.setDate(yearMockFrom.getDate() + 1);
                                        yearMockTo = new Date(yearMockFrom.getFullYear(), 11, 31);
                                        if (fnDateToDash(yearMockTo) > fnDateToDash(lastDate)) {
                                            yearMockTo = new Date(lastDate);
                                        }
                                    }
                                }

                                firstDate.setDate(firstDate.getDate() + 1);
                            }
                            funFlot();
                            $('#chartWidgetViewNum').html(chartWidgetViewNum);
                            $('#chartWidgetReferralNum').html(res.estimateReferral);
                            $('#chartWidgetCommissionRealNum').html(res.estimateCommissionReal);
                            fnCheckRangeToSelect();
                        },
                        error: function error(data) {

                        }
                    });

                }
                var fnSetRangeDays = function (dayx, dayy) {
                    dayx.setHours(0, 0, 0, 0);
                    dayy.setHours(0, 0, 0, 0);

                    $('#chartDayFrombd').datepicker('setDate', dayx);
                    $('#chartDayTobd').datepicker('setDate', dayy);

                    fnCheckRangeToSelect();
                }                
                $('#chartRangeDaysbd').datepicker({
                    language: 'vi',
                    orientation: "left",
                    autoclose: true
                });
                $('#chartBtnFilterbd').click(function () {
                    //showChart(false);
                });
                $('#chartSelectRangebd').on("change", function () {
                    var val = $('#chartSelectRangebd').val();
                    var from = null, to = null;chartDayFrombd                    
                    switch (val) {
                        case '':
                            to = '';
                            from = '';
                            break;
                        case 'TODAY':
                            to = new Date();
                            from = new Date(to);
                            break;
                        case 'YESTERDAY':
                            to = new Date();
                            from = new Date(to);
                            to.setDate(to.getDate() - 1);
                            from.setDate(from.getDate() - 1);
                            break;
                        case 'WEEK':
                            to = new Date();
                            from = new Date(to);
                            from.setDate(from.getDate() - 7);
                            break;
                        case '1_MONTH':
                            to = new Date();
                            from = new Date(to);
                            from.setMonth(from.getMonth() - 1);
                            break;
                        case '3_MONTH':
                            to = new Date();
                            from = new Date(to);
                            from.setMonth(from.getMonth() - 3);
                            break;
                        case '6_MONTH':
                            to = new Date();
                            from = new Date(to);
                            from.setMonth(from.getMonth() - 6);
                            break;

                        default:
                            break;
                    }
                    if (from != null && to != null) {
                        fnSetRangeDays(from, to);
                        $('#chartBtnFilterbd').click();
                    }
                    fnCheckRangeToSelect();
                });
                var fnPatternToday = function () {
                    var to = new Date();
                    var from = new Date(to);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPatternYesterday = function () {
                    var to = new Date();
                    var from = new Date(to);
                    to.setDate(to.getDate() - 1);
                    from.setDate(from.getDate() - 1);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPatternWeek = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setDate(from.getDate() - 7);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPattern1Month = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setMonth(from.getMonth() - 1);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPattern3Month = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setMonth(from.getMonth() - 3);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnPattern6Month = function () {
                    var to = new Date();
                    var from = new Date(to);
                    from.setMonth(from.getMonth() - 6);
                    return fnDateToDash(from) + ":" + fnDateToDash(to);
                }
                var fnCheckRangeToSelect = function () {
                    var value = "";
                    var rangeFrom = $('#chartDayFrombd').val();
                    rangeFrom = fnSlashToDash(rangeFrom);
                    rangeFrom = new Date(rangeFrom);
                    var rangeTo = $('#chartDayTobd').val();
                    rangeTo = fnSlashToDash(rangeTo);
                    rangeTo = new Date(rangeTo);
                    var pattern = fnDateToDash(rangeFrom) + ":" + fnDateToDash(rangeTo);

                    if (pattern == fnPatternToday()) {
                        value = 'TODAY';
                    } else if (pattern == fnPatternYesterday()) {
                        value = 'YESTERDAY';
                    } else if (pattern == fnPatternWeek()) {
                        value = 'WEEK';
                    } else if (pattern == fnPattern1Month()) {
                        value = '1_MONTH';
                    } else if (pattern == fnPattern3Month()) {
                        value = '3_MONTH';
                    } else if (pattern == fnPattern6Month()) {
                        value = '6_MONTH';
                    } else {
                        value = '';
                    }
                    $('#chartSelectRangebd').val(value);
                }
            })();
        </script>
        <script>
            document.getElementById('chartBtnFilterbd').onclick = function () {
                var bd = document.getElementById('txtTest').value;
                var arr = bd.split(",");
                var sotv1 = arr[0];
                var sotv23 = arr[1];
                var sotv45 = arr[2];
                var sotv610 = arr[3];
                var sotv1120 = arr[4];
                var sotv20 = arr[5];
                // Create the chart
                Highcharts.chart('container', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: ''
                    },
                    accessibility: {
                        announceNewData: {
                            enabled: true
                        }
                    },
                    xAxis: {
                        type: 'category'
                    },
                    yAxis: {
                        title: {
                            text: 'Số thành viên'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> thành viên<br/>'
                    },

                    series: [
                      {
                          name: "Số lần:",
                          colorByPoint: true,
                          data: [
                            {
                                name: "1 lần",
                                y: +sotv1,
                                drilldown: "1 lần"
                            },
                            {
                                name: "2 - 3 lần",
                                y: +sotv23,
                                drilldown: "2 - 3 lần"
                            },
                            {
                                name: "4 - 5 lần",
                                y: +sotv45,
                                drilldown: "4 - 5 lần"
                            },
                            {
                                name: "6 - 10 lần",
                                y: +sotv610,
                                drilldown: "6 - 10 lần"
                            },
                            {
                                name: "11 - 20 lần",
                                y: +sotv1120,
                                drilldown: "11 - 20 lần"
                            },
                            {
                                name: "trên 20 lần",
                                y: +sotv20,
                                drilldown: "trên 20 lần"
                            }
                          ]
                      }
                    ],
                    drilldown: {
                        series: [
                          {
                              name: "1 lần",
                              id: "1 lần",
                              data: [
                                [
                                  "v65.0",
                                  0.1
                                ],
                                [
                                  "v64.0",
                                  1.3
                                ],
                                [
                                  "v63.0",
                                  53.02
                                ],
                                [
                                  "v62.0",
                                  1.4
                                ],
                                [
                                  "v61.0",
                                  0.88
                                ],
                                [
                                  "v60.0",
                                  0.56
                                ],
                                [
                                  "v59.0",
                                  0.45
                                ],
                                [
                                  "v58.0",
                                  0.49
                                ],
                                [
                                  "v57.0",
                                  0.32
                                ],
                                [
                                  "v56.0",
                                  0.29
                                ],
                                [
                                  "v55.0",
                                  0.79
                                ],
                                [
                                  "v54.0",
                                  0.18
                                ],
                                [
                                  "v51.0",
                                  0.13
                                ],
                                [
                                  "v49.0",
                                  2.16
                                ],
                                [
                                  "v48.0",
                                  0.13
                                ],
                                [
                                  "v47.0",
                                  0.11
                                ],
                                [
                                  "v43.0",
                                  0.17
                                ],
                                [
                                  "v29.0",
                                  0.26
                                ]
                              ]
                          },
                          {
                              name: "2 - 3 lần",
                              id: "2 - 3 lần",
                              data: [
                                [
                                  "v58.0",
                                  1.02
                                ],
                                [
                                  "v57.0",
                                  7.36
                                ],
                                [
                                  "v56.0",
                                  0.35
                                ],
                                [
                                  "v55.0",
                                  0.11
                                ],
                                [
                                  "v54.0",
                                  0.1
                                ],
                                [
                                  "v52.0",
                                  0.95
                                ],
                                [
                                  "v51.0",
                                  0.15
                                ],
                                [
                                  "v50.0",
                                  0.1
                                ],
                                [
                                  "v48.0",
                                  0.31
                                ],
                                [
                                  "v47.0",
                                  0.12
                                ]
                              ]
                          },
                          {
                              name: "4 - 5 lần",
                              id: "4 - 5 lần",
                              data: [
                                [
                                  "v11.0",
                                  6.2
                                ],
                                [
                                  "v10.0",
                                  0.29
                                ],
                                [
                                  "v9.0",
                                  0.27
                                ],
                                [
                                  "v8.0",
                                  0.47
                                ]
                              ]
                          },
                          {
                              name: "6 - 10 lần",
                              id: "6 - 10 lần",
                              data: [
                                [
                                  "v11.0",
                                  3.39
                                ],
                                [
                                  "v10.1",
                                  0.96
                                ],
                                [
                                  "v10.0",
                                  0.36
                                ],
                                [
                                  "v9.1",
                                  0.54
                                ],
                                [
                                  "v9.0",
                                  0.13
                                ],
                                [
                                  "v5.1",
                                  0.2
                                ]
                              ]
                          },
                          {
                              name: "11 - 20 lần",
                              id: "11 - 20 lần",
                              data: [
                                [
                                  "v16",
                                  2.6
                                ],
                                [
                                  "v15",
                                  0.92
                                ],
                                [
                                  "v14",
                                  0.4
                                ],
                                [
                                  "v13",
                                  0.1
                                ]
                              ]
                          },
                          {
                              name: "trên 20 lần",
                              id: "trên 20 lần",
                              data: [
                                [
                                  "v50.0",
                                  0.96
                                ],
                                [
                                  "v49.0",
                                  0.82
                                ],
                                [
                                  "v12.1",
                                  0.14
                                ]
                              ]
                          }
                        ]
                    }
                });
            };
        </script>    
        <script>
            var SL = '<%= Request.QueryString["SL"] %>';
            document.getElementById('chartSelectRange').value = SL;
        </script>                              
        </form>
</asp:Content>