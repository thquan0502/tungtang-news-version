﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriSeo_DanhSachSeoDM : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sTieuDe = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            
            LoadTinDang();
        }
    }
    #region paging
    //private void SetPage()
    //{
    //    string sql = "select count(idfooter) from tb_footer where '1'='1'";
    //    if (sTieuDe != "")
    //        sql += " and TieuDe like N'%" + sTieuDe + "%'";
    //    if (sTuNgay != "")
    //        sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
    //    if (sDenNgay != "")
    //        sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        
    //    DataTable tbTotalRows = Connect.GetTable(sql);
    //    int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
    //    if (TotalRows % PageSize == 0)
    //        MaxPage = TotalRows / PageSize;
    //    else
    //        MaxPage = TotalRows / PageSize + 1;
    //    txtLastPage = MaxPage.ToString();
    //    if (Page == 1)
    //    {
    //        for (int i = 1; i <= MaxPage; i++)
    //        {
    //            if (i <= 5)
    //            {
    //                switch (i)
    //                {
    //                    case 1: txtPage1 = i.ToString(); break;
    //                    case 2: txtPage2 = i.ToString(); break;
    //                    case 3: txtPage3 = i.ToString(); break;
    //                    case 4: txtPage4 = i.ToString(); break;
    //                    case 5: txtPage5 = i.ToString(); break;
    //                }
    //            }
    //            else
    //                return;
    //        }
    //    }
    //    else
    //    {
    //        if (Page == 2)
    //        {
    //            for (int i = 1; i <= MaxPage; i++)
    //            {
    //                if (i == 1)
    //                    txtPage1 = "1";
    //                if (i <= 5)
    //                {
    //                    switch (i)
    //                    {
    //                        case 2: txtPage2 = i.ToString(); break;
    //                        case 3: txtPage3 = i.ToString(); break;
    //                        case 4: txtPage4 = i.ToString(); break;
    //                        case 5: txtPage5 = i.ToString(); break;
    //                    }
    //                }
    //                else
    //                    return;
    //            }
    //        }
    //        else
    //        {
    //            int Cout = 1;
    //            if (Page <= MaxPage)
    //            {
    //                for (int i = Page; i <= MaxPage; i++)
    //                {
    //                    if (i == Page)
    //                    {
    //                        txtPage1 = (Page - 2).ToString();
    //                        txtPage2 = (Page - 1).ToString();
    //                    }
    //                    if (Cout <= 3)
    //                    {
    //                        if (i == Page)
    //                            txtPage3 = i.ToString();
    //                        if (i == (Page + 1))
    //                            txtPage4 = i.ToString();
    //                        if (i == (Page + 2))
    //                            txtPage5 = i.ToString();
    //                        Cout++;
    //                    }
    //                    else
    //                        return;
    //                }
    //            }
    //            else
    //            {
    //                //Page = MaxPage;
    //                SetPage();
    //            }
    //        }
    //    }
    //}
    #endregion
    private void LoadTinDang()
    {
        string sql = "";
        sql += @"
	            SELECT 
	              *
                  FROM tungtang.DanhMucSeo where '1'='1'
            ";
 


        DataTable table = Connect.GetTable(sql);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();
       // SetPage();
        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                               
                             

    <th class='th'>
                                   Tên Danh Mục
                                </th>

  <th class='th'>
                                  Mô tả
                                </th>

                            </tr>";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "       <tr>";
            html += "       <td>" + ( i + 1).ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["TenDanhMuc"].ToString() + "</td>";
            html += "       <td>";
            html += "       <a style='cursor:pointer' onclick='window.location=\"DanhSachSeoDM-CapNhat.aspx?idTinTuc=" + table.Rows[i]["IdDanhMuc"].ToString() + "\"'><img class='imgedit' src='../images/edit.png'/>Xem và Sửa</a>";
            html += "       </td>";
            html += "       </tr>";
        }
       
        html += "     </table>";
        dvTinDang.InnerHtml = html;
    }
}