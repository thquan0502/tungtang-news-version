﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriSeo_DashBoard : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sThoiGian = "";
    protected void Page_Load(object sender, EventArgs e)
     {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }           
        if (!IsPostBack)
        {
            this.initSearchValues();
            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }
            //try
            //{
            //    string option = "4";
            //    if (Request.QueryString["ThoiGian"].Trim() != "")
            //    {
            //        sThoiGian = Request.QueryString["ThoiGian"].Trim();
            //        chartSelectRange.Value = sThoiGian;
            //    }
            //}
            //catch { }
            Load10TinhThanh();
        }              
    }
   
    private void Load10TinhThanh()
    {        
        sTuNgay = txtTuNgay.Value.Trim();
        sDenNgay = txtDenNgay.Value.Trim();
        //sThoiGian = chartSelectRange.Value.Trim();
        #region Top 10 tin đăng        
        string sql = "";
        sql += @"select Top(10)City.Ten, COUNT(tb_TinDang.idTinh) AS 'SoLuong' from tb_TinDang inner join City on tb_TinDang.idTinh = City.id where";
        if (sTuNgay != "")
            sql += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{            
        //    if (sThoiGian == "1")
        //    {
        //        sql += " tb_TinDang.NgayDang BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    if (sThoiGian == "2")
        //    {
        //        sql += " tb_TinDang.NgayDang BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    if (sThoiGian == "3")
        //    {
        //        sql += " DATEPART(month, NgayDang) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDang) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //        sql += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //        sql += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        sql += " Group by City.Ten order by [SoLuong] desc";
        DataTable table = Connect.GetTable(sql);        
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Tỉnh/Thành Phố
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string url1 = "";
        if (sTuNgay != "")
            url1 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url1 += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            url1 += "ThoiGian=" + sThoiGian + "&";
        if (table != null)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                html += "       <tr>";
                html += "       <td>" + ((i + 1).ToString()) + "</td>";
                html += "       <td>" + table.Rows[i]["Ten"] + "</td>";
                html += "       <td>" + table.Rows[i]["SoLuong"] + "</td>";
                html += "       </tr>";

            }
        }   
                
        html += "     </table>";
        dvTop10TinDang.InnerHtml = html;
        #endregion End Top 10 tin đăng

        #region Top 10 user
        string sqluser = "";
        sqluser += @"select Top(10)City.Ten, COUNT(tb_ThanhVien.idTinh) AS 'SoLuong' from tb_ThanhVien inner join City on tb_ThanhVien.idTinh = City.id where";
        if (sTuNgay != "")
            sqluser += " tb_ThanhVien.NgayDangKy between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sqluser += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{
        //    if (sThoiGian == "1")
        //    {
        //        sqluser += " tb_ThanhVien.NgayDangKy BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "2")
        //    {
        //        sqluser += " tb_ThanhVien.NgayDangKy BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "3")
        //    {
        //        sqluser += " DATEPART(month, NgayDangKy) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDangKy) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    else if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //            sqluser += " tb_ThanhVien.NgayDangKy between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //            sqluser += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        //sql += " Group by City.Ten order by [SoLuong] desc";
        sqluser += " Group by City.Ten order by [SoLuong] desc";
        DataTable tableuser = Connect.GetTable(sqluser);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string htmluser = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Tỉnh/Thành Phố
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string urluser1 = "";
        if (sTuNgay != "")
            urluser1 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            urluser1 += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            urluser1 += "ThoiGian=" + sThoiGian + "&";
        if (tableuser != null)
        {
            for (int i = 0; i < tableuser.Rows.Count; i++)
            {
                htmluser += "       <tr>";
                htmluser += "       <td>" + ((i + 1).ToString()) + "</td>";
                htmluser += "       <td>" + tableuser.Rows[i]["Ten"] + "</td>";
                htmluser += "       <td>" + tableuser.Rows[i]["SoLuong"] + "</td>";
                htmluser += "       </tr>";

            }
        }

        htmluser += "     </table>";
        dvTop10user.InnerHtml = htmluser;
        #endregion End Top 10 user  
        #region Top 10 danh mục cấp 1
        string sqldmc1 = "";
        sqldmc1 += @"select Top(10)tb_DanhMucCap1.TenDanhMucCap1, COUNT(tb_tinDang.idDanhMucCap1) AS 'SoLuong' from tb_DanhMucCap1 inner join tb_TinDang on tb_DanhMucCap1.idDanhMucCap1 = tb_TinDang.idDanhMucCap1 where";
        if (sTuNgay != "")
            sqldmc1 += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sqldmc1 += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{
        //    if (sThoiGian == "1")
        //    {
        //        sqldmc1 += " tb_TinDang.NgayDang BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "2")
        //    {
        //        sqldmc1 += " tb_TinDang.NgayDang BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "3")
        //    {
        //        sqldmc1 += " DATEPART(month, NgayDang) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDang) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    else if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //            sqldmc1 += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //            sqldmc1 += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        sqldmc1 += " Group by tb_DanhMucCap1.TenDanhMucCap1 order by [SoLuong] desc";
        DataTable tabledmc1 = Connect.GetTable(sqldmc1);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string htmldmc1 = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Tỉnh/Thành Phố
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string urldmc1 = "";
        if (sTuNgay != "")
            urldmc1 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            urldmc1 += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            urldmc1 += "ThoiGian=" + sThoiGian + "&";
        if (tabledmc1 != null)
        {
            for (int i = 0; i < tabledmc1.Rows.Count; i++)
            {
                htmldmc1 += "       <tr>";
                htmldmc1 += "       <td>" + ((i + 1).ToString()) + "</td>";
                htmldmc1 += "       <td>" + tabledmc1.Rows[i]["TenDanhMucCap1"] + "</td>";
                htmldmc1 += "       <td>" + tabledmc1.Rows[i]["SoLuong"] + "</td>";
                htmldmc1 += "       </tr>";

            }
        }

        htmldmc1 += "     </table>";
        dvTop10DMC1.InnerHtml = htmldmc1;
        #endregion Top 10 danh mục cấp 1
        #region Top 10 danh mục cấp 2
        string sqldmc2 = "";
        sqldmc2 += @"select Top(10)tb_DanhMucCap2.TenDanhMucCap2, COUNT(tb_tinDang.idDanhMucCap2) AS 'SoLuong' from tb_DanhMucCap2 inner join tb_TinDang on tb_DanhMucCap2.idDanhMucCap2 = tb_TinDang.idDanhMucCap2 where";
        if (sTuNgay != "")
            sqldmc2 += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sqldmc2 += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{
        //    if (sThoiGian == "1")
        //    {
        //        sqldmc2 += " tb_TinDang.NgayDang BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "2")
        //    {
        //        sqldmc2 += " tb_TinDang.NgayDang BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "3")
        //    {
        //        sqldmc2 += " DATEPART(month, NgayDang) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDang) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    else if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //            sqldmc2 += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //            sqldmc2 += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        sqldmc2 += " Group by tb_DanhMucCap2.TenDanhMucCap2 order by [SoLuong] desc";
        DataTable tabledmc2 = Connect.GetTable(sqldmc2);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string htmldmc2 = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Tỉnh/Thành Phố
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string urldmc2 = "";
        if (sTuNgay != "")
            urldmc2 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            urldmc2 += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            urldmc2 += "ThoiGian=" + sThoiGian + "&";
        if (tabledmc2 != null)
        {
            for (int i = 0; i < tabledmc2.Rows.Count; i++)
            {
                htmldmc2 += "       <tr>";
                htmldmc2 += "       <td>" + ((i + 1).ToString()) + "</td>";
                htmldmc2 += "       <td>" + tabledmc2.Rows[i]["TenDanhMucCap2"] + "</td>";
                htmldmc2 += "       <td>" + tabledmc2.Rows[i]["SoLuong"] + "</td>";
                htmldmc2 += "       </tr>";

            }
        }

        htmldmc2 += "     </table>";
        dvTop10DMC2.InnerHtml = htmldmc2;
        #endregion Top 10 danh mục cấp 2
        #region Top 10 giờ đăng bài nhiều
        string sqlgiodb = "";
        sqlgiodb += @"select Top(10)DATEPART(hour, NgayDang) AS 'GioDangNhieu', COUNT(idTinDang) AS 'SoTinDang' from tb_TinDang where";
        if (sTuNgay != "")
            sqlgiodb += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sqlgiodb += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{
        //    if (sThoiGian == "1")
        //    {
        //        sqlgiodb += " tb_TinDang.NgayDang BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "2")
        //    {
        //        sqlgiodb += " tb_TinDang.NgayDang BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "3")
        //    {
        //        sqlgiodb += " DATEPART(month, NgayDang) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDang) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    else if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //            sqlgiodb += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //            sqlgiodb += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        sqlgiodb += " group by DATEPART(hour, NgayDang) order by COUNT(idTinDang) desc";
        DataTable tablegiodb = Connect.GetTable(sqlgiodb);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string htmlgiodb = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Giờ Đăng Bài Nhiều
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string urlgiodb = "";
        if (sTuNgay != "")
            urlgiodb += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            urlgiodb += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            urlgiodb += "ThoiGian=" + sThoiGian + "&";
        if (tablegiodb != null)
        {
            for (int i = 0; i < tablegiodb.Rows.Count; i++)
            {
                htmlgiodb += "       <tr>";
                htmlgiodb += "       <td>" + ((i + 1).ToString()) + "</td>";
                htmlgiodb += "       <td>" + tablegiodb.Rows[i]["GioDangNhieu"] + "h"+ "</td>";
                htmlgiodb += "       <td>" + tablegiodb.Rows[i]["SoTinDang"] + "</td>";
                htmlgiodb += "       </tr>";

            }
        }

        htmlgiodb += "     </table>";
        dvGioDbNhieu.InnerHtml = htmlgiodb;
        #endregion Top 10 giờ đăng bài nhiều  
        #region Một ngày bao nhiêu bài
        string sql1nbnb = "";
        sql1nbnb += @"select CONVERT(varchar,NgayDang,103) as NgayDangBai,COUNT(idTinDang) AS 'SoLuong' from tb_TinDang where";
        if (sTuNgay != "")
            sql1nbnb += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql1nbnb += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{
        //    if (sThoiGian == "1")
        //    {
        //        sql1nbnb += " tb_TinDang.NgayDang BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "2")
        //    {
        //        sql1nbnb += " tb_TinDang.NgayDang BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "3")
        //    {
        //        sql1nbnb += " DATEPART(month, NgayDang) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDang) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    else if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //            sql1nbnb += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //            sql1nbnb += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        sql1nbnb += " group by CONVERT(varchar,NgayDang,103) order by COUNT(idTinDang) desc";
        DataTable table1nbnb = Connect.GetTable(sql1nbnb);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string html1nbnb = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Ngày Đăng
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string url1nbnb = "";
        if (sTuNgay != "")
            url1nbnb += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url1nbnb += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            url1nbnb += "ThoiGian=" + sThoiGian + "&";
        if (table1nbnb != null)
        {
            for (int i = 0; i < table1nbnb.Rows.Count; i++)
            {
                html1nbnb += "       <tr>";
                html1nbnb += "       <td>" + ((i + 1).ToString()) + "</td>";
                html1nbnb += "       <td>" + table1nbnb.Rows[i]["NgayDangBai"] + "</td>";
                html1nbnb += "       <td>" + table1nbnb.Rows[i]["SoLuong"] + "</td>";
                html1nbnb += "       </tr>";

            }
        }

        html1nbnb += "     </table>";
        dv1nbnb.InnerHtml = html1nbnb;
        #endregion
        #region Một ngày bao nhiêu người đăng
        string sql1nbndb = "";
        sql1nbndb += @"select CONVERT(varchar,NgayDang,103) as NgayDangBai,COUNT(distinct (idThanhVien)) AS SoLuong from tb_TinDang where";
        if (sTuNgay != "")
            sql1nbndb += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql1nbndb += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (sThoiGian != "")
        //{
        //    if (sThoiGian == "1")
        //    {
        //        sql1nbndb += " tb_TinDang.NgayDang BETWEEN GETDATE()-7 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "2")
        //    {
        //        sql1nbndb += " tb_TinDang.NgayDang BETWEEN GETDATE()-20 AND GETDATE()";
        //    }
        //    else if (sThoiGian == "3")
        //    {
        //        sql1nbndb += " DATEPART(month, NgayDang) = (DATEPART(month, GETDATE()) - 1) and DATEPART(year, NgayDang) = DATEPART(year, DATEADD(m, -1, GETDATE()))";
        //    }
        //    else if (sThoiGian == "4")
        //    {
        //        if (sTuNgay != "")
        //            sql1nbndb += " tb_TinDang.NgayDang between '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        //        if (sDenNgay != "")
        //            sql1nbndb += " and '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //    }
        //}
        sql1nbndb += " group by CONVERT(varchar,NgayDang,103) order by COUNT(idThanhVien) desc";
        DataTable table1nbndb = Connect.GetTable(sql1nbndb);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();       
        string html1nbndb = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                   Ngày Đăng
                                </th>
                                <th class='th'>
                                   Số Lượng
                                </th>                                                              
                            </tr>";
        string url1nbndb = "";
        if (sTuNgay != "")
            url1nbndb += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url1nbndb += "DenNgay=" + sDenNgay + "&";
        if (sThoiGian != "")
            url1nbndb += "ThoiGian=" + sThoiGian + "&";
        if (table1nbndb != null)
        {
            for (int i = 0; i < table1nbndb.Rows.Count; i++)
            {
                html1nbndb += "       <tr>";
                html1nbndb += "       <td>" + ((i + 1).ToString()) + "</td>";
                html1nbndb += "       <td>" + table1nbndb.Rows[i]["NgayDangBai"] + "</td>";
                html1nbndb += "       <td>" + table1nbndb.Rows[i]["SoLuong"] + "</td>";
                html1nbndb += "       </tr>";

            }
        }

        html1nbndb += "     </table>";
        dv1nbndb.InnerHtml = html1nbndb;
        #endregion
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {        
        string TuNgay = txtTuNgay.Value.Trim();        
        string DenNgay = txtDenNgay.Value.Trim();
		string SL = txtsl.Value.Trim();
        //string ThoiGian = chartSelectRange.Value.Trim();
        string url = "/Admin/QuanTriSeo/DashBoard.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";  
		if (SL != "")	
            url += "SL=" + SL + "&";
        Response.Redirect(url);
    }

    private void initSearchValues()
    {        
        try
        {
            if (Request.QueryString["TuNgay"].Trim() != "")
            {
                sTuNgay = Request.QueryString["TuNgay"].Trim();
                txtTuNgay.Value = sTuNgay;
            }
        }
        catch { }

        try
        {
            if (Request.QueryString["DenNgay"].Trim() != "")
            {
                sDenNgay = Request.QueryString["DenNgay"].Trim();
                txtDenNgay.Value = sDenNgay;
            }
        }
        catch { }        
    }
}