﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" CodeFile="DanhSachTags_CapNhat.aspx.cs" Inherits="Admin_QuanTriSeo_DanhSachTags_CapNhat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        } 
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="title" id="dvTitle" runat="server">Sửa Tags</div>
        <div class="title1"><a href="DanhSachTags.aspx"><i class="fa fa-step-backward"></i>Title</a></div>
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <form class="form-horizontal" runat="server">
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1" style="width: 100%;">
                                    <div class="titleinput" style="width: 13%;"><b>Title(*):</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTitle" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                  <div class="coninput1" style="margin-top:10px;width: 100%;">
                                    <div class="titleinput" style="width: 13%;"><b>Description(*):</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDescription" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1" style="margin-top:10px;width: 100%;">
                                    <div class="titleinput" style="width: 13%;"><b>DuongDan(*):</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDuongDan" runat="server" name="Content.ContentName" type="text" value="" />
                                          <div id="dvDuongDan" style="color: red;" runat="server"></div>
                                    </div>
                                </div>
                              <%--  <div class="coninput2">
                                    <div class="titleinput"><b>Số thứ tự:</b></div>
                                    <div class="txtinput">
                                        <input id="txtSoThuTu" class="form-control" runat="server" />
                                    </div>
                                </div>--%>
                            </div>
                            <%--<div class="row"> 
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Mức giá để tìm kiếm:</b></div>
                                    <div class="txtinput">
                                        <input id="txtMucGiaBoLoc" class="form-control" runat="server"  oninput='DinhDangTien(this.id)' onkeypress='onlyNumber(event)'/>
                                    </div>
                                </div>
                            </div>--%>
                        </div>


                        <%--<div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Titlte:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTitlte" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Description:</b></div>
                                    <div class="txtinput">
                                        <input id="txtMoTa" class="form-control" runat="server" type="text"  />
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>meta Canonical:</b></div>
                                    <div class="txtinput">
                                        <input id="txtmetaCanonical" class="form-control" runat="server" type="text"  />
                                    </div>
                                </div>
                            </div>

                            <div class="row"> 
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>meta robots:</b></div>
                                    <div class="txtinput">
                                        <input id="txtmetarobots" class="form-control" runat="server" type="text" />
                                    </div>
                                </div>
                            </div>

                            <div class="row"> 
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>meta OpenGraph:</b></div>
                                    <div class="txtinput">
                                        <input id="txtmetaOpenGraph" class="form-control" runat="server" type="text" />
                                    </div>
                                </div>
                            </div>

                            <div class="row"> 
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>meta twitter:</b></div>
                                    <div class="txtinput">
                                        <input id="txtmetatwitter" class="form-control" runat="server" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>--%>


                        <%--<div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Hình ảnh:</b></div>
                                    <div class="txtinput">
                                        <asp:FileUpload ID="fileLinkAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" />
                                        <img id="imgLinkAnh" runat="server" src="" style="height: 130px" />
                                    </div>
                                </div>
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Hình ảnh icon:</b></div>
                                    <div class="txtinput">
                                        <asp:FileUpload ID="fileLinkAnhIcon" ClientIDMode="Static" onchange="this.form.submit()" runat="server" />
                                        <img id="imgLinkAnhIcon" runat="server" src="" style="height: 130px;background-color:#0000005e;" />
                                    </div>
                                </div>
                            </div>
                        </div>--%>


                        



                        <div class="box-footer">
                            <asp:Button ID="btLuu" runat="server" Text="LƯU" class="btn btn-primary btn-flat" OnClick="btLuu_Click" />
                            <asp:Button ID="btHuy" runat="server" Text="HỦY" class="btn btn-primary btn-flat" OnClick="btHuy_Click" />
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</asp:Content>
