﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLyDanhMuc_DanhMucCap2_CapNhat : System.Web.UI.Page
{
    string idDanhMucCap2 = "";
    string Page = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            idDanhMucCap2 = StaticData.ValidParameter(Request.QueryString["idDanhMucCap2"].Trim());
        }
        catch { }
        try
        {
            Page = StaticData.ValidParameter(Request.QueryString["Page"].Trim());
        }
        catch { }
        if (!IsPostBack)
        {
            LoadDanhMucCap1();
            LoadDanhMucCap2();
        }
        if (IsPostBack && fileLinkAnhIcon.PostedFile != null)
        {
            if (fileLinkAnhIcon.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnhIcon.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnhIcon.HasFile)
                    {
                        string fExtension = Path.GetExtension(fileLinkAnhIcon.PostedFile.FileName);
                        string FileName = "DMCap2" + DateTime.Now.ToString("ddMMyyyyHHmmssms") + fExtension;
                        string FilePath = "../../Images/Category/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnhIcon.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnhIcon.Src = FilePath;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }
    }
    private void LoadDanhMucCap1()
    {
        string strSql = "select * from tb_DanhMucCap1 order by sothutu";
        slDanhMucCap1.DataSource = Connect.GetTable(strSql);
        slDanhMucCap1.DataTextField = "TenDanhMucCap1";
        slDanhMucCap1.DataValueField = "idDanhMucCap1";
        slDanhMucCap1.DataBind();
        slDanhMucCap1.Items.Add(new ListItem("-- Chọn --", "0"));
        slDanhMucCap1.Items.FindByText("-- Chọn --").Selected = true;
    }
    private void LoadDanhMucCap2()
    {
        if (idDanhMucCap2 != "")
        {
            string sql = "select * from tb_DanhMucCap2 where idDanhMucCap2='" + idDanhMucCap2 + "'  order by sothutu";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                dvTitle.InnerHtml = "SỬA THÔNG TIN DANH MỤC CẤP 2";
                btLuu.Text = "SỬA";
                txtTenDanhMuc.Value = table.Rows[0]["TenDanhMucCap2"].ToString();
                txtSoThuTu.Value = table.Rows[0]["sothutu"].ToString();
                txtMucGiaBoLoc.Value = decimal.Parse(table.Rows[0]["MucGia_BOLoc"].ToString().Trim().Replace(" ", "0")).ToString("N0").Replace(" ", "0");
                slDanhMucCap1.Value = table.Rows[0]["idDanhMucCap1"].ToString();
                imgLinkAnhIcon.Src = "../../" + table.Rows[0]["LinkIcon"].ToString();

                txtMoTa.Value = table.Rows[0]["MoTa"].ToString();
                txtTitlte.Value = table.Rows[0]["Titlte"].ToString();
            }
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        string TenDanhMuc = txtTenDanhMuc.Value.Trim();
        string idDanhMucCap1 = slDanhMucCap1.Value.Trim();
        string SoThuTu = txtSoThuTu.Value.Trim();
        string MucGia_BoLoc = txtMucGiaBoLoc.Value.Trim().Replace(",", "");
        string LinkIcon = imgLinkAnhIcon.Src.Replace("../../", "");


        string Titlte = txtTitlte.Value.Trim();
        string MoTa = txtMoTa.Value.Trim();

        if (TenDanhMuc == "")
        {
            Response.Write("<script>alert('Tên danh mục trống !')</script>");
            txtTenDanhMuc.Focus();
            return;
        }
        if (idDanhMucCap1 == "0")
        {
            Response.Write("<script>alert('Bạn chưa chọn danh mục cấp 1 !')</script>");
            slDanhMucCap1.Focus();
            return;
        }
        //////////
        if (idDanhMucCap2 == "")
        {
            string sqlInsertLoaiKH = "insert into tb_DanhMucCap2(TenDanhMucCap2,idDanhMucCap1,SoThuTu, MucGia_BoLoc,StepMucGia_BoLoc,LinkIcon";

            sqlInsertLoaiKH += @",[Titlte]
              ,[MoTa]
             ";
            sqlInsertLoaiKH += ")";

            sqlInsertLoaiKH += " values(N'" + TenDanhMuc + "','" + idDanhMucCap1 + "','" + SoThuTu + "',N'" + MucGia_BoLoc + "',N'" + MucGia_BoLoc.Replace("00", "") + "',N'" + LinkIcon + "'";
            sqlInsertLoaiKH += @",N'" + Titlte + @"'
              ,N'" + MoTa + @"'
              ";
            sqlInsertLoaiKH += ")";



            bool ktInsertLoaiKH = Connect.Exec(sqlInsertLoaiKH);
            if (ktInsertLoaiKH)
            {
                Response.Redirect("DanhMucCap2.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm danh mục cấp 2!')</script>");
            } 
        }
        else
        {
            string sqlUpdateSanPham = "update tb_DanhMucCap2 set TenDanhMucCap2=N'" + TenDanhMuc + "'";
            sqlUpdateSanPham += ",idDanhMucCap1='" + idDanhMucCap1 + "'";
            sqlUpdateSanPham += ",SoThuTu='" + SoThuTu + "'";
            sqlUpdateSanPham += ",MucGia_BoLoc='" + MucGia_BoLoc + "'";
            sqlUpdateSanPham += ",StepMucGia_BoLoc='" + MucGia_BoLoc.Replace("00", "") + "'";
            sqlUpdateSanPham += ",LinkIcon=N'" + LinkIcon + "'";

            sqlUpdateSanPham += @",[Titlte]=N'" + Titlte + @"'
              ,[MoTa]=N'" + MoTa + @"'
              ";


            sqlUpdateSanPham += " where idDanhMucCap2='" + idDanhMucCap2 + "'";
            bool ktUpdateSanPham = Connect.Exec(sqlUpdateSanPham);
            if (ktUpdateSanPham)
            {
                if (Page != "")
                    Response.Redirect("DanhMucCap2.aspx?Page=" + Page);
                else
                    Response.Redirect("DanhMucCap2.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("DanhMucCap2.aspx");
    }
}