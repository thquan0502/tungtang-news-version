﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="DanhMucCap2.aspx.cs" Inherits="Admin_QuanLyDanhMuc_DanhMucCap2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function DeleteDanhMucCap2(id) {
            if (confirm("Bạn có muốn xóa không ?")) {
                //alert(idSlide);
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Không thể xóa vì danh mục này đang có tin đăng !")
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteDanhMucCap2&id=" + id, true);
                xmlhttp.send();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="title">QUẢN LÝ DANH MỤC CẤP 2</div>
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Tên danh mục:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtTenDanhMuc" runat="server" name="Content.ContentName" type="text" value="" />
                          </div>
                        </div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Danh mục cấp 1:</b></div>
                          <div class="txtinput">
                              <select class="form-control" id="slDanhMucCap1" runat="server"></select>
                          </div>
                        </div>
                      </div>
                </div>
            <div class="row">
                <div class="col-sm-9">
                    <a class="btn btn-primary btn-flat" href="DanhMucCap2-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
                </div>
                <div class="col-sm-3">
                        <div style="text-align:right;">
                            <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                        </div>
                    </div>
            </div>
            <div id="dvDanhMucCap2" runat="server">

            </div>
        </div>
    </div>
</section>

    <!-- /.content -->
  </div>
        </form>
</asp:Content>

