﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLyDanhMuc_DanhMucCap1_CapNhat : System.Web.UI.Page
{
    string idDanhMucCap1 = "";
    string Page = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            idDanhMucCap1 = StaticData.ValidParameter(Request.QueryString["idDanhMucCap1"].Trim());
        }
        catch { }
        try
        {
            Page = StaticData.ValidParameter(Request.QueryString["Page"].Trim());
        }
        catch { }
        if (!IsPostBack)
        {
            LoadDanhMucCap1();
        }
        if (IsPostBack && fileLinkAnh.PostedFile != null)
        {
            if (fileLinkAnh.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnh.HasFile)
                    {
                        string fExtension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                        string FileName = "DM" + DateTime.Now.ToString("ddMMyyyyHHmmssms") + fExtension;
                        string FilePath = "../../Images/Category/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnh.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnh.Src = FilePath;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }

        if (IsPostBack && fileLinkAnhIcon.PostedFile != null)
        {
            if (fileLinkAnhIcon.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnhIcon.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnhIcon.HasFile)
                    {
                        string fExtension = Path.GetExtension(fileLinkAnhIcon.PostedFile.FileName);
                        string FileName = "DM" + DateTime.Now.ToString("ddMMyyyyHHmmssms") + fExtension;
                        string FilePath = "../../Images/Category/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnhIcon.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnhIcon.Src = FilePath;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }
    }
    private void LoadDanhMucCap1()
    {
        if (idDanhMucCap1 != "")
        {
            string sql = "select * from tb_DanhMucCap1 where idDanhMucCap1='" + idDanhMucCap1 + "'";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                dvTitle.InnerHtml = "SỬA THÔNG TIN DANH MỤC CẤP";
                btLuu.Text = "SỬA";
                txtTenDanhMuc.Value = table.Rows[0]["TenDanhMucCap1"].ToString(); 
                txtSoThuTu.Value = table.Rows[0]["SoThuTu"].ToString();
                txtMucGiaBoLoc.Value = decimal.Parse(table.Rows[0]["MucGia_BOLoc"].ToString().Trim().Replace(" ","0")).ToString("N0").Replace(" ", "0");
                imgLinkAnh.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
                imgLinkAnhIcon.Src = "../../" + table.Rows[0]["LinkIcon"].ToString();

                txtMoTa.Value = table.Rows[0]["MoTa"].ToString();
                txtTitlte.Value = table.Rows[0]["Titlte"].ToString();
            }
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        string LinkIcon = imgLinkAnhIcon.Src.Replace("../../", "");
        string LinkAnh = imgLinkAnh.Src.Replace("../../", "");
        string TenDanhMuc = txtTenDanhMuc.Value.Trim(); 
        string SoThuTu = txtSoThuTu.Value.Trim();
        string MucGia_BoLoc = txtMucGiaBoLoc.Value.Trim().Replace(",","");

        string Titlte = txtTitlte.Value.Trim();
        string MoTa = txtMoTa.Value.Trim();

        if (TenDanhMuc == "")
        {
            Response.Write("<script>alert('Tên danh mục trống !')</script>");
            txtTenDanhMuc.Focus();
            return;
        }
        //////////
        if (idDanhMucCap1 == "")
        {
            string sqlInsertLoaiKH = "insert into tb_DanhMucCap1(TenDanhMucCap1,SoThuTu,LinkAnh,linkIcon,MucGia_BoLoc,StepMucGia_BoLoc";
            sqlInsertLoaiKH += @",[Titlte]
              ,[MoTa]
             ";
            sqlInsertLoaiKH += ")";
            sqlInsertLoaiKH += " values(N'" + TenDanhMuc + "','" + SoThuTu + "',N'"+ LinkAnh + "',N'" + LinkIcon + "','"+ MucGia_BoLoc + "','"+ MucGia_BoLoc.Replace("00","") + "'";
            sqlInsertLoaiKH += @",N'" + Titlte + @"'
              ,N'" + MoTa + @"'
              ";
            sqlInsertLoaiKH += ")";
            bool ktInsertLoaiKH = Connect.Exec(sqlInsertLoaiKH);
            if (ktInsertLoaiKH)
            {
                Response.Redirect("DanhMucCap1.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm danh mục cấp 1!')</script>");
            }

        }
        else
        {
            string sqlUpdateSanPham = "update tb_DanhMucCap1 set TenDanhMucCap1=N'" + TenDanhMuc + "'";
            sqlUpdateSanPham += ",SoThuTu='" + SoThuTu + "'";
            sqlUpdateSanPham += ",MucGia_BoLoc='" + MucGia_BoLoc + "'";
            sqlUpdateSanPham += ",StepMucGia_BoLoc='" + MucGia_BoLoc.Replace("00", "") + "'";
            sqlUpdateSanPham += ",LinkAnh=N'" + LinkAnh + "'";
            sqlUpdateSanPham += ",LinkIcon=N'" + LinkIcon + "'";
            sqlUpdateSanPham += @",[Titlte]=N'" + Titlte + @"'
              ,[MoTa]=N'" + MoTa + @"'
             ";

            sqlUpdateSanPham += " where idDanhMucCap1='" + idDanhMucCap1 + "'";

            bool ktUpdateSanPham = Connect.Exec(sqlUpdateSanPham);
            if (ktUpdateSanPham)
            {
                if (Page != "")
                    Response.Redirect("DanhMucCap1.aspx?Page=" + Page);
                else
                    Response.Redirect("DanhMucCap1.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("DanhMucCap1.aspx");
    }
}