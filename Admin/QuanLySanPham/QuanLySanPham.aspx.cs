﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLySanPham_QuanLySanPham : System.Web.UI.Page
{
    string sMaSanPham = "";
    string sTenSanPham = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString["MaSanPham"].Trim() != "")
                {
                    sMaSanPham = Request.QueryString["MaSanPham"].Trim();
                    txtMaSanPham.Value = sMaSanPham;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["TenSanPham"].Trim() != "")
                {
                    sTenSanPham = Request.QueryString["TenSanPham"].Trim();
                    txtTenSanPham.Value = sTenSanPham;
                }
            }
            catch { }
            LoadDanhSachSanPham();
        }
    }
    #region paging
    private void SetPage()
    {
        string sql = "select count(idSanPham) from tb_SanPham where isnull(isHienThiWeb,'False')='True'";
        if (sMaSanPham != "")
            sql += " and MaSanPham like '%" + sMaSanPham + "%'";
        if (sTenSanPham != "")
            sql += " and TenSanPham like N'%" + sTenSanPham + "%'";
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    #endregion
    private void LoadDanhSachSanPham()
    {
        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY idSanPham desc
                  )AS RowNumber
	              ,*
                  FROM tb_SanPham where isnull(isHienThiWeb,'False')='True'
            ";
        if (sMaSanPham != "")
            sql += " and MaSanPham like '%" + sMaSanPham + "%'";
        if (sTenSanPham != "")
            sql += " and TenSanPham like N'%" + sTenSanPham + "%'";
        sql += ") as tb1 WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";


        DataTable table = Connect.GetTable(sql);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();
        SetPage();
        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                    Mã sản phẩm
                                </th>
                                <th class='th'>
                                    Tên sản phẩm
                                </th>
                                <th class='th'>
                                    Loại sản phẩm
                                </th>
                                <th class='th'>
                                    Ảnh
                                </th>
                                <th class='th'>
                                    Bán chạy
                                </th>
                                <th class='th'>
                                    Được yêu thích
                                </th>
                                <th class='th'>
                                    Ghi chú
                                </th>
                                <th class='th' style='width:110px'></th>
                            </tr>";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "       <tr>";
            html += "       <td>" + (((Page - 1) * PageSize) + i + 1).ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["MaSanPham"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["TenSanPham"].ToString() + "</td>";
            html += "       <td>" + StaticData.getField("tb_LoaiSanPham","TenLoaiSanPham","idLoaiSanPham",table.Rows[i]["idLoaiSanPham"].ToString()) + "</td>";
            html += "       <td><img src='../../" + table.Rows[i]["LinkAnh"].ToString() + "' style='width:150px' /></td>";
            html += "       <td>" + table.Rows[i]["isBanChay"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["isDuocYeuThich"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["GhiChu"].ToString() + "</td>";
            html += "       <td>";
            html += "       <a href='#' onclick='window.location=\"QuanLySanPham-CapNhat.aspx?Page=" + Page.ToString() + "&idSanPham=" + table.Rows[i]["idSanPham"].ToString() + "\"'><img class='imgedit' src='../images/edit.png'/>Sửa</a>";
            //html += "       |<a href='#'  onclick='DeleteLoaiKhachHang(\"" + table.Rows[i]["idLoaiKhachHang"].ToString() + "\")'> <img class='imgedit' src='../images/delete.png' />Xóa</a>";
            html += "       </td>";
            html += "       </tr>";

        }
        html += "   <tr>";
        html += "       <td colspan='9' class='footertable'>";
        string url = "QuanLySanPham.aspx?";
        if (sMaSanPham != "" && sMaSanPham != "0")
            url += "MaSanPham=" + sMaSanPham + "&";
        if (sTenSanPham != "" && sTenSanPham != "0")
            url += "TenSanPham=" + sTenSanPham + "&";
        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvLoaiKhachHang.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string MaSanPham = txtMaSanPham.Value.Trim();
        string TenSanPham = txtTenSanPham.Value.Trim();
        string url = "QuanLySanPham.aspx?";
        if (MaSanPham != "")
            url += "MaSanPham=" + MaSanPham + "&";
        if (TenSanPham != "")
            url += "TenSanPham=" + TenSanPham + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "QuanLySanPham.aspx";
        Response.Redirect(url);
    }
}