﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLySanPham_QuanLySanPham_CapNhat : System.Web.UI.Page
{
    string sIdSanPham= "";
    //string LinkAnh = "";
    string Page = "";
    protected void FileBrowser1_Load(object sender, EventArgs e)
    {
        FileBrowser1 = new CKFinder.FileBrowser();
        FileBrowser1.BasePath = "/ckfinder/";
        FileBrowser1.SetupCKEditor(txtMoTa);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            sIdSanPham = StaticData.ValidParameter(Request.QueryString["idSanPham"].Trim());
        }
        catch { }
        try
        {
            Page = StaticData.ValidParameter(Request.QueryString["Page"].Trim());
        }
        catch { }
        if (!IsPostBack)
        {
            LoadLoaiSanPham();
            LoadThongTinSanPham();
        }
        if (IsPostBack && fileLinkAnh.PostedFile != null)
        {
            if (fileLinkAnh.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnh.HasFile)
                    {
                        string Ngay = DateTime.Now.Day.ToString();
                        string Thang = DateTime.Now.Month.ToString();
                        string Nam = DateTime.Now.Year.ToString();
                        string Gio = DateTime.Now.Hour.ToString();
                        string Phut = DateTime.Now.Minute.ToString();
                        string Giay = DateTime.Now.Second.ToString();
                        string Khac = DateTime.Now.Ticks.ToString();
                        string fExtension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                        string FileName = "SP" + Ngay + Thang + Nam + Gio + Phut + Giay + Khac + fExtension;
                        string FilePath = "../../Images/SanPham/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnh.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnh.Src = FilePath;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }
    }
    private void LoadLoaiSanPham()
    {
        string strSql = "select * from tb_LoaiSanPham";
        slLoaiSanPham.DataSource = Connect.GetTable(strSql);
        slLoaiSanPham.DataTextField = "TenLoaiSanPham";
        slLoaiSanPham.DataValueField = "idLoaiSanPham";
        slLoaiSanPham.DataBind();
        slLoaiSanPham.Items.Add(new ListItem("-- Chọn --", "0"));
        slLoaiSanPham.Items.FindByText("-- Chọn --").Selected = true;
    }
    private void LoadThongTinSanPham()
    {
        if (sIdSanPham != "")
        {
            string sql = "select * from tb_SanPham where idSanPham='" + sIdSanPham + "'";
            DataTable table = Connect.GetTable(sql);
            if (table.Rows.Count > 0)
            {
                dvTitle.InnerHtml = "SỬA THÔNG TIN LOẠI SẢN PHẨM";
                btLuu.Text = "SỬA";
                txtMaSanPham.Value = table.Rows[0]["MaSanPham"].ToString();
                txtTenSanPham.Value = table.Rows[0]["TenSanPham"].ToString();
                slLoaiSanPham.Value = table.Rows[0]["idLoaiSanPham"].ToString();
                if (table.Rows[0]["GiaBan"].ToString() != "")
                    txtGiaBan.Value = double.Parse(table.Rows[0]["GiaBan"].ToString()).ToString("#,##").Replace(",",".");
                txtGhiChu.Value = table.Rows[0]["GhiChu"].ToString();
                imgLinkAnh.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
                txtMoTa.Text = table.Rows[0]["MoTa"].ToString();
                if (table.Rows[0]["isBanChay"].ToString() == "True")
                    ckbBanChay.Checked = true;
                else
                    ckbBanChay.Checked = false;
                if (table.Rows[0]["isDuocYeuThich"].ToString() == "True")
                    ckbDuocYeuThich.Checked = true;
                else
                    ckbDuocYeuThich.Checked = false;
                txtTuKhoa.Value = table.Rows[0]["TuKhoa"].ToString();
            }
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        string LinkAnh = imgLinkAnh.Src.Replace("../../", "");
        string MoTa = txtMoTa.Text;
        string isBanChay = ckbBanChay.Checked.ToString();
        string isDuocYeuThich = ckbDuocYeuThich.Checked.ToString();
        string TuKhoa = txtTuKhoa.Value.Trim();
        //////////
        if (sIdSanPham == "")
        {
            /*string sqlInsertLoaiKH = "insert into tb_LoaiKhachHang(TenLoaiKhachHang,GhiChu)";
            sqlInsertLoaiKH += " values(N'" + TenLoaiKhachHang + "',N'" + GhiChu + "')";
            bool ktInsertLoaiKH = Connect.Exec(sqlInsertLoaiKH);
            if (ktInsertLoaiKH)
            {
                Response.Redirect("DanhMucLoaiKhachHang.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm loại khách hàng!')</script>");
            }*/

        }
        else
        {
            string sqlUpdateSanPham = "update tb_SanPham set LinkAnh=N'"+ LinkAnh +"'";
            sqlUpdateSanPham += ",MoTa=N'" + MoTa + "'";
            sqlUpdateSanPham += ",isBanChay='" + isBanChay + "'";
            sqlUpdateSanPham += ",isDuocYeuThich='" + isDuocYeuThich + "'";
            sqlUpdateSanPham += ",TuKhoa=N'" + TuKhoa + "'";
            sqlUpdateSanPham += " where idSanPham='" + sIdSanPham + "'";
            bool ktUpdateSanPham = Connect.Exec(sqlUpdateSanPham);
            if (ktUpdateSanPham)
            {
                if (Page != "")
                    Response.Redirect("QuanLySanPham.aspx?Page=" + Page);
                else
                    Response.Redirect("QuanLySanPham.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("QuanLySanPham.aspx");
    }
}