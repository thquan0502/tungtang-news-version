﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="QuanLySanPham.aspx.cs" Inherits="Admin_QuanLySanPham_QuanLySanPham" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="title">QUẢN LÝ SẢN PHẨM</div>
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Mã sản phẩm:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtMaSanPham" runat="server" name="Content.ContentName" type="text" value="" />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Tên sản phẩm:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtTenSanPham" runat="server" name="Content.ContentName" type="text" value="" />
                          </div>
                        </div>
                      </div>
                </div>
            <div class="row">
                <div class="col-sm-9">
                    <%--<a class="btn btn-primary btn-flat" href="DanhMucLoaiKhachHang-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>--%>
                </div>
                <div class="col-sm-3">
                        <div style="text-align:right;">
                            <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                        </div>
                    </div>
            </div>
            <div id="dvLoaiKhachHang" runat="server">

            </div>
            <%--<table class="table table-bordered table-striped">
                <tr>
                    <th class="th">
                        STT
                    </th>
                    <th class="th">
                        Tên loại khách hàng
                    </th>
                    <th class="th">
                        Ghi chú
                    </th>
                    <th id="command" class="th"></th>
                </tr>
                <tr>
                        <td>
                            1
                        </td>
                        <td>
                            Loại khách hàng 1
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <a href="#"><img class="imgedit" src="../images/edit.png" />Sửa</a>|
                            <a href="#"><img class="imgedit" src="../images/delete.png" />Xóa</a>
                        </td>
                    </tr>
            </table>
            <br />
            <div class="pagination-container" style="text-align:center">
                <ul class="pagination">
                    <li class="active"><a>1</a></li>
                    <li class=""><a>2</a></li>
                    <li class=""><a>3</a></li>
                    <li class=""><a>4</a></li>
                    <li class=""><a>5</a></li>
                </ul>
            </div>--%>
        </div>
    </div>
</section>


    <!-- /.content -->
  </div>
        </form>
</asp:Content>

