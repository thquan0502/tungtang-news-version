﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="QuanLySanPham-CapNhat.aspx.cs" Inherits="Admin_QuanLySanPham_QuanLySanPham_CapNhat" %>
<%@ Register Namespace="CKEditor.NET" Assembly="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Namespace="CKFinder" Assembly="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
    <!-- Main content -->
    <div class="title" id="dvTitle" runat="server">THÊM THÔNG TIN SẢN PHẨM</div>
    <div class="title1"><a href="QuanLySanPham.aspx"><i class="fa fa-step-backward"></i> Danh sách sản phẩm</a></div>
    <section class="content">
    <div class="box">
        <div class="box-body">
            <form class="form-horizontal" runat="server">
               <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Mã sản phẩm(*):</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtMaSanPham" runat="server" name="Content.ContentName" type="text" value="" disabled />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Loại sản phẩm(*):</b></div>
                          <div class="txtinput">
                              <select id="slLoaiSanPham" runat="server" class="form-control" disabled>
                                  <option>--Chọn--</option>
                              </select>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Tên sản phẩm(*):</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtTenSanPham" runat="server" name="Content.ContentName" type="text" value="" disabled />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Giá bán:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtGiaBan" runat="server" name="Content.ContentName" type="text" value="" disabled/>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="Content_ContentName">Ghi chú:</label>
                    <div class="col-md-10">
                        <textarea id="txtGhiChu" class="form-control" runat="server" disabled></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Hình ảnh:</b></div>
                          <div class="txtinput">
                              <asp:FileUpload ID="fileLinkAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" />
                              <img id="imgLinkAnh" runat="server" src="" style="width:100%; height:180px" />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Bán chạy:</b></div>
                          <div class="txtinput" style="padding-top: 4px;">
                              <input type="checkbox" id="ckbBanChay" runat="server" />
                          </div>
                        </div>
                      </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Được yêu thích:</b></div>
                          <div class="txtinput">
                              <input type="checkbox" id="ckbDuocYeuThich" runat="server" />
                          </div>
                        </div>
                        <%--<div class="coninput2">
                          <div class="titleinput"><b>Bán chạy:</b></div>
                          <div class="txtinput" style="padding-top: 4px;">
                              <input type="checkbox" id="Checkbox1" runat="server" />
                          </div>
                        </div>--%>
                      </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="Content_ContentName">Từ khóa:</label>
                    <div class="col-md-10">
                        <textarea id="txtTuKhoa" class="form-control" runat="server"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="Content_ContentName">Mô tả:</label>
                    <div class="col-md-10">
                        <%--<textarea id="txtMoTa" class="form-control" runat="server"></textarea>--%>
                        <CKEditor:CKEditorControl ID="txtMoTa" Height="500" runat="server"></CKEditor:CKEditorControl> 
                        <CKFinder:FileBrowser ID="FileBrowser1"   Width="0" runat="server" OnLoad="FileBrowser1_Load"></CKFinder:FileBrowser>
                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btLuu" runat="server" Text="LƯU" class="btn btn-primary btn-flat" OnClick="btLuu_Click" />
                    <asp:Button ID="btHuy" runat="server" Text="HỦY" class="btn btn-primary btn-flat" OnClick="btHuy_Click" />
                </div>
            </form>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
</asp:Content>

