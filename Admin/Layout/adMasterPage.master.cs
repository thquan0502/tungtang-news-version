﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Layout_adMasterPage : System.Web.UI.MasterPage
{
    string mTenDangNhap = "";
    string mQuyen = "";
    string pidAdmin = "";
    string pXetQuyen = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string urlCheck = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper();
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = mTenDangNhap = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiAdmin = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            pidAdmin = StaticData.getField("tb_admin", "idAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            mQuyen = idLoaiAdmin;// StaticData.getField("tb_LoaiAdmin", "TenLoaiAdmin", "idLoaiAdmin", idLoaiAdmin);


            if (idLoaiAdmin.Trim().ToUpper() == "1")
            {

            }
            else
            {
                if (urlCheck.Contains("DEFAULT.ASPX") || urlCheck.Contains("DOIMATKHAU.ASPX"))
                {
                    string idAdmin = StaticData.getField("tb_admin", "idAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
                    int flagError = 0;
                    DataTable tbChiTietNhomQuyen
                        = Connect.GetTable(@"SELECT [idChiTietQuyenAdmin]
                      ,[idAdmin] ,[idNhomQuyen] FROM [tb_ChiTietQuyenAdmin] where idAdmin='" + idAdmin + "' ");
                    if (tbChiTietNhomQuyen.Rows.Count > 0)
                    {
                        for (int i = 0; i < tbChiTietNhomQuyen.Rows.Count; i++)
                        {
                            string idNhomQuyen = tbChiTietNhomQuyen.Rows[i]["idNhomQuyen"].ToString();
                            pXetQuyen += idNhomQuyen + ",";


                        }
                    }
                }
                else
                {
                    string idAdmin = StaticData.getField("tb_admin", "idAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
                    int flagError = 0;
                    DataTable tbChiTietNhomQuyen
                        = Connect.GetTable(@"SELECT [idChiTietQuyenAdmin]
                      ,[idAdmin] ,[idNhomQuyen] FROM [tb_ChiTietQuyenAdmin] where idAdmin='" + idAdmin + "' ");
                    if (tbChiTietNhomQuyen.Rows.Count > 0)
                    {
                        for (int i = 0; i < tbChiTietNhomQuyen.Rows.Count; i++)
                        {
                            string idNhomQuyen = tbChiTietNhomQuyen.Rows[i]["idNhomQuyen"].ToString();
                            pXetQuyen += idNhomQuyen + ",";
                            bool checkResult = MyStaticData.CheckNhomQuyen(idNhomQuyen, urlCheck);

                            if (checkResult)
                            {
                                flagError = 1;
                                // break;
                                //
                            }
                        }
                        if (flagError == 0)
                        {
                            //Response.Redirect("../Home/Default.aspx");
                            if (urlCheck.Contains("DEFAULT.ASPX"))
                            {

                            }
                            else
                                Response.Redirect("../Home/Default.aspx");
                        }
                    }
                    else
                    {
                        if (urlCheck.Contains("DEFAULT.ASPX"))
                        {

                        }
                        else
                            Response.Redirect("../Home/Default.aspx");
                    }
                }
            }
        }
        else
        {
            Response.Redirect("../Home/DangNhap.aspx");
        }
        if (!IsPostBack)
        {
            LoadThongTinNguoiDung();
            LoadMenu();
        }
    }
    private void LoadThongTinNguoiDung()
    {
        string html = "";
        string sqlTTND = "select * from tb_Admin where TenDangNhap='" + mTenDangNhap + "'";
        DataTable tbTTND = Connect.GetTable(sqlTTND);
        if (tbTTND.Rows.Count > 0)
        {
            //mQuyen = tbTTND.Rows[0]["MaQuyen"].ToString();
            lbTenDangNhap.InnerHtml = tbTTND.Rows[0]["HoTen"].ToString();
            html += "<img src='../dist/img/user2-160x160.jpg' class='img-circle' alt='User Image'>";
            html += "<p>" + tbTTND.Rows[0]["HoTen"].ToString() + "";
            //html += "Quyền: " + mQuyen + "</small></p>";
        }
        dvTTND.InnerHtml = html;
    }



    private void LoadMenu()
    {
        string URL = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper();
        string html = "";



        html += @"<ul class='sidebar-menu'>
        <li class='header' style='color:white; background-color: rgb(60, 141, 188);font-size: 14px;font-family: -webkit-body; font-weight:bold;text-shadow: rgba(0,0,0,0.25) 0 -1px 0;box-shadow: rgba(0,0,0,0.25) 0 1px 0,inset rgba(255,255,255,0.16) 0 1px 0;'><img src='../images/management.png' class='imgcategorymenu'/>DANH MỤC QUẢN LÝ</li>
        <!-- Optionally, you can add icons to the links -->";

        /////////Danh mục
        /*if (URL.Contains("/DANHMUC/"))
            html += "  <li id='dvDanhMuc' class='treeview active'>";
        else
            html += "  <li id='dvDanhMuc' class='treeview'>";
        html += @"<a href='#'><i class='fa fa-user'></i> <span>Danh mục</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../DanhMuc/DanhMucLoaiSanPham.aspx'><i class='fa fa-edit'></i>Danh mục loại sản phẩm</a></li>
                <li><a href='../DanhMuc/DanhMucSanPham.aspx'><i class='fa fa-edit'></i>Danh mục sản phẩm</a></li>
                <li><a href='../DanhMuc/DuyetDaiLy.aspx'><i class='fa fa-edit'></i>Duyệt đại lý</a></li>
              </ul>
           </li>";*/
        /////////
        ///////Danh mục




        // if (mQuyen != "Người kiểm duyệt")
        // {
        if (pXetQuyen.Contains("1,") || mQuyen == "1")
        {
            if (URL.Contains("/QUANLYDANHMUC/"))
                html += "  <li id='dvQuanLyDanhMuc' class='treeview active'>";
            else
                html += "  <li id='dvQuanLyDanhMuc' class='treeview'>";
            html += @"<a href='#'><i class='fa fa-bars'></i> <span>Quản lý Danh mục</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../QuanLyDanhMuc/DanhMucCap1.aspx'><i class='fa fa-edit'></i>Danh mục cấp 1</a></li>
                <li><a href='../QuanLyDanhMuc/DanhMucCap2.aspx'><i class='fa fa-edit'></i>Danh mục cấp 2</a></li>
              </ul>
           </li>";
        }
        //  }

        ///////// Quản lý thành viên
        // if (mQuyen != "Người kiểm duyệt")
        // { 
        if (pXetQuyen.Contains("2,") || mQuyen == "1")
        {
            //if (URL.Contains("/QUANLYTHANHVIENADMIN/"))
            //    html += "  <li id='dvQuanLyThanhVienAdmin' class='treeview active'>";
            //else
            //    html += "  <li id='dvQuanLyThanhVienAdmin' class='treeview'>";
            //html += "  <a href='../QuanLyThanhVienAdmin/QuanLyThanhVien.aspx'>";
            //html += "    <i class='fa fa-user'></i> <span>Quản lý Admin</span>";
            //html += "  </a>";
            //html += "</li>";
            if (URL.Contains("/QUANLYDANHMUC/"))
                html += "  <li id='dvQuanLyDanhMuc' class='treeview active'>";
            else
                html += "  <li id='dvQuanLyDanhMuc' class='treeview'>";
            html += @"<a href='#'><i class='fa fa-bars'></i> <span>Quản trị Hệ thống</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../QuanTriHeThong/QuanLyThanhVien.aspx'><i class='fa fa-user'></i>Quản Lý Admin</a></li>
                <li><a href='../System/AccountTable.aspx'><i class='fa fa-user'></i>Quản Lý Thành Viên</a></li>
              </ul>
           </li>";


            if (URL.Contains("/QUANLYDANHMUC/"))
                html += "  <li id='dvQuanLyDanhMuc' class='treeview active'>";
            else
                html += "  <li id='dvQuanLyDanhMuc' class='treeview'>";
            html += @"<a href='#'><i class='fa fa-bars'></i> <span>Quản trị Seo</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../QuanTriSeo/DanhSachTitle.aspx'><i class='fa fa-newspaper-o'></i>Seo Home</a></li>
                <li><a href='../QuanTriSeo/DanhSachTop.aspx'><i class='fa fa-newspaper-o'></i>Quản Lý Keyword</a></li>
                <li><a href='../QuanTriSeo/DanhSachTags.aspx'><i class='fa fa-newspaper-o'></i>Quản Lý Tags</a></li>
                <li><a href='../QuanTriSeo/DanhSachfooter.aspx'><i class='fa fa-newspaper-o'></i>Quản Lý Giới thiệu</a></li>
              </ul>
           </li>";
        }
        // }
        /////////
        ///////// Quản lý thành viên
        // if (mQuyen != "Người kiểm duyệt")
        // {
        if (pXetQuyen.Contains("3,") || mQuyen == "1")
        {
            //if (URL.Contains("/QUANLYTHANHVIEN/"))
            //    html += "  <li id='dvQuanLyThanhVien' class='treeview active'>";
            //else
            //    html += "  <li id='dvQuanLyThanhVien' class='treeview'>";
            //html += "  <a href='../QuanLyThanhVien/QuanLyThanhVien.aspx'>";
            //html += "    <i class='fa fa-user'></i> <span>Quản lý thành viên</span>";
            //html += "  </a>";
            //html += "</li>";

            //        if (URL.Contains("/QuanLyTitle/"))
            //            html += "  <li  class='treeview active'>";
            //        else
            //            html += "  <li  class='treeview'>";
            //        html += "  <a href='../QuanLyTitle/DanhSach.aspx'>";
            //        html += "    <i class='fa fa-newspaper-o'></i> <span>Seo Home</span>";
            //        html += "  </a>";
            //        html += "</li>";

            //        if (URL.Contains("/QuanLyTags/"))
            //            html += "  <li  class='treeview active'>";
            //        else
            //            html += "  <li  class='treeview'>";
            //        html += "  <a href='../QuanLyTags/DanhSach.aspx'>";
            //        html += "    <i class='fa fa-newspaper-o'></i> <span>Quản Lý Tags</span>";
            //        html += "  </a>";
            //        html += "</li>";

            //if (URL.Contains("/QuanLyTop/"))
            //            html += "  <li  class='treeview active'>";
            //        else
            //            html += "  <li  class='treeview'>";
            //        html += "  <a href='../QuanLyTop/DanhSach.aspx'>";
            //        html += "    <i class='fa fa-newspaper-o'></i> <span>Quản Lý Keyword</span>";
            //        html += "  </a>";
            //        html += "</li>";
        }
        //  }
        /////////
        ///////// Quản lý tin đăng
        if (pXetQuyen.Contains("4,") || mQuyen == "1")
        {
            //    if (URL.Contains("/QUANLYTINDANG/"))
            //        html += "  <li id='dvQuanLyTinDang' class='treeview active'>";
            //    else
            //        html += "  <li id='dvQuanLyTinDang' class='treeview'>";
            //    html += "  <a href='../QuanLyTinDang/QuanLyTinDang.aspx'>";
            //    html += "    <i class='fa fa-barcode'></i> <span>Quản lý tin đăng</span>";
            //    html += "  </a>";
            //    html += "</li>";
        }
        /////////  




        if (pXetQuyen.Contains("4,") || mQuyen == "1")
        {
            string sql = @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY idTinDang desc
                  )AS RowNumber
	              ,td.*,TenThanhVien=tv.TenCuaHang,tv.TenDangNhap
                  FROM tb_TinDang td left join tb_ThanhVien tv on td.idThanhVien=tv.idThanhVien where '1'='1'
                    and  td.isDuyet is null 
            ";

            sql += ") as tb1 ";
            string SoTinDang = "0";
            DataTable tableSoTinDangCho = Connect.GetTable("select count(*) from (" + sql + ") as tb1");
            try
            {
                SoTinDang = "" + tableSoTinDangCho.Rows[0][0].ToString();
            }
            catch
            {

            }

            ///////// Quản lý tin đăng
            //if (URL.Contains("/TINCHODUYET/"))
            //    html += "  <li id='dvTinChoDuyet' class='treeview active'>";
            //else
            //    html += "  <li id='dvTinChoDuyet' class='treeview'>";
            //html += "  <a href='../TinChoDuyet/TinChoDuyet.aspx'>";
            //html += "    <i class='fa fa-barcode'></i> <span>Tin chờ duyệt</span>&nbsp;<span style='background-color: red; padding: 5px; border-radius: 13px;color:white;'>" + SoTinDang + "</span>";
            //html += "  </a>";
            //html += "</li>";

            string sql1 = @"
                SELECT COUNT(*) AS TOTALROWS FROM Referrals INNER JOIN tb_TinDang TBPost
                        ON Referrals.TinDangId = TBPost.idTinDang WHERE ISNUll(Referrals.StsOwnerApproved, 0) = 1 AND ISNUll(Referrals.StsCollabApproved, 0) = 1 AND ISNUll(Referrals.StsAdminApproved, 0) = 0
            ";
            DataTable table1 = Connect.GetTable(sql1);
            int totalReferrals = Int32.Parse(table1.Rows[0]["TOTALROWS"].ToString());

            if (URL.Contains("/QUANLYDANHMUC/"))
                html += "  <li id='dvQuanLyDanhMuc' class='treeview active'>";
            else
                html += "  <li id='dvQuanLyDanhMuc' class='treeview'>";
            html += @"<a href='#'><i class='fa fa-bars'></i> <span>Quản trị Tin Đăng</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../Posts/PostTable.aspx'><i class='fa fa-newspaper-o'></i>Quản lý Tin đăng</a></li>
                <li><a href='../QuanTriTinDang/TinChoDuyet.aspx'>";
            html += "<i class='fa fa-barcode'></i> <span>Tin chờ duyệt</span>&nbsp;<span style='background-color: red; padding: 5px; border-radius: 13px;color:white;'>" + SoTinDang + @"</span></a></li>
                <li><a href='/Admin/Posts/PostReferralTable.aspx?FilterReferralStatus=" + Models.Referral.STATUS_COLLAB_APPROVED + @"'><i class='fa fa-barcode'></i> <span>Tin cộng tác</span>&nbsp;<span style='background-color: red; padding: 5px; border-radius: 13px;color:white;'>" + totalReferrals + @"</span></a></li>
            </ul>";



        }
        /////////  

        // if (mQuyen != "Người kiểm duyệt")
        // {
        if (pXetQuyen.Contains("5,") || mQuyen == "1")
        {
            //if (URL.Contains("/QUANLYGIOITHIEU/"))
            //    html += "  <li id='dvQuanLyTinTuc' class='treeview active'>";
            //else
            //    html += "  <li id='dvQuanLyTinTuc' class='treeview'>";
            //html += "  <a href='../QuanLyGioiThieu/DanhSachTinTuc.aspx'>";
            //html += "    <i class='fa fa-newspaper-o'></i> <span>Quản lý giới thiệu</span>";
            //html += "  </a>";
            //html += "</li>";

            //if (URL.Contains("/QUANLYFOOTER/"))
            //    html += "  <li id='dvQuanLyTinTuc' class='treeview active'>";
            //else
            //    html += "  <li id='dvQuanLyTinTuc' class='treeview'>";
            //html += "  <a href='../QuanLyfooter/DanhSachfooter.aspx'>";
            //html += "    <i class='fa fa-newspaper-o'></i> <span>Quản lý giới thiệu</span>";
            //html += "  </a>";
            //html += "</li>";


        }
        // }
        /////////
        // if (mQuyen != "Người kiểm duyệt")
        // {
        if (pXetQuyen.Contains("6,") || mQuyen == "1")
        {
            //        if (URL.Contains("/BANNERTRANGCHU/"))
            //            html += "  <li id='dvBannerTrangChu' class='treeview active'>";
            //        else
            //            html += "  <li id='dvBannerTrangChu' class='treeview'>";
            //        html += "  <a href='../BannerTrangChu/BannerTrangChu.aspx'>";
            //        html += "    <i class='fa fa-newspaper-o'></i> <span>Banner trang chủ</span>";
            //        html += "  </a>";
            //        html += "</li>";
            //if (URL.Contains("/BANNERTINDANG/"))
            //        html += "  <li  class='treeview active'>";
            //    else
            //        html += "  <li  class='treeview'>";
            //    html += "  <a href='../BannerTinDang/BannerTinDang.aspx'>";
            //    html += "    <i class='fa fa-newspaper-o'></i> <span>Banner danh mục sản phẩm</span>";
            //    html += "  </a>";
            //    html += "</li>";


            //    if (URL.Contains("/BANNERSANPHAM/"))
            //        html += "  <li  class='treeview active'>";
            //    else
            //        html += "  <li  class='treeview'>";
            //    html += "  <a href='../BannerSanPham/BannerSanPham.aspx'>";
            //    html += "    <i class='fa fa-newspaper-o'></i> <span>Banner trang sản phẩm</span>";
            //    html += "  </a>";
            //    html += "</li>";

            //if (URL.Contains("/BANNERBLOG/"))
            //            html += "  <li  class='treeview active'>";
            //        else
            //            html += "  <li  class='treeview'>";
            //        html += "  <a href='../BannerBlog/BannerBlog.aspx'>";
            //        html += "    <i class='fa fa-newspaper-o'></i> <span>Banner Blog</span>";
            //        html += "  </a>";
            //        html += "</li>";


            if (URL.Contains("/QUANLYDANHMUC/"))
                html += "  <li id='dvQuanLyDanhMuc' class='treeview active'>";
            else
                html += "  <li id='dvQuanLyDanhMuc' class='treeview'>";
            html += @"<a href='#'><i class='fa fa-bars'></i> <span>Quản trị Banner</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../QuanTriBanner/BannerTrangChu.aspx'><i class='fa fa-newspaper-o'></i>Banner Trang Chủ</a></li>
                <li><a href='../QuanTriBanner/BannerTinDang.aspx'><i class='fa fa-newspaper-o'></i>Banner danh mục sản phẩm</a></li>
                <li><a href='../QuanTriBanner/BannerSanPham.aspx'><i class='fa fa-newspaper-o'></i>Banner trang sản phẩm</a></li>
                <li><a href='../QuanTriBanner/BannerBlog.aspx'><i class='fa fa-newspaper-o'></i>Banner Blog</a></li>
              </ul>
           </li>";

        }

        if (pXetQuyen.Contains("7,") || mQuyen == "1")
        {
            //if (URL.Contains("/BLOG/"))
            //    html += "  <li id='dvBlog' class='treeview active'>";
            //else
            //    html += "  <li id='dvBlog' class='treeview'>";
            //html += "  <a href='../QuanLyTinTuc/DanhSachTinTuc.aspx'>";
            //html += "    <i class='fa fa-barcode'></i> <span>Blog</span>&nbsp";
            //html += "  </a>";
            //html += "</li>";
            if (URL.Contains("/QUANLYDANHMUC/"))
                html += "  <li id='dvQuanLyDanhMuc' class='treeview active'>";
            else
                html += "  <li id='dvQuanLyDanhMuc' class='treeview'>";
            html += @"<a href='#'><i class='fa fa-bars'></i> <span>Quản trị Blog</span> <i class='fa fa-angle-left pull-right'></i></a>
              <ul class='treeview-menu'>
                <li><a href='../QuanTriBlog/LoaiTinTuc.aspx'><i class='fa fa-barcode'></i>Loại Tin tức</a></li>
                <li><a href='../QuanTriBlog/DanhSachTinTuc.aspx'><i class='fa fa-barcode'></i>Blog</a></li>";
        }
            if (pXetQuyen.Contains("8,") || mQuyen == "1")
            {
                html += "  <li id='dvBlog' class='treeview'>";
                html += "  <a href='../QuanTriBlog/DanhSachCTV.aspx'>";
                html += "    <i class='fa fa-barcode'></i> <span>BlogCTV</span>&nbsp";
                html += "  </a>";
                html += "</li>";
            }
            html += "</ul>";
             html +="</li>";
        
        // if (pXetQuyen.Contains("8,") || mQuyen == "1")
        //{
        //    if (URL.Contains("/BLOGCTV/"))
        //        html += "  <li id='dvBlog' class='treeview active'>";
        //    else
        //        html += "  <li id='dvBlog' class='treeview'>";
        //    html += "  <a href='../QuanLyCTV/DanhSachCTV.aspx'>";
        //    html += "    <i class='fa fa-barcode'></i> <span>BlogCTV</span>&nbsp";
        //    html += "  </a>";
        //    html += "</li>";
        //}		
       

        // }
        ///////// Mật khẩu
        if (URL.Contains("/DOIMATKHAU/"))
            html += "  <li id='dvDoiMatKhau' class='treeview active'>";
        else
            html += "  <li id='dvDoiMatKhau' class='treeview'>";
        html += "  <a href='../DoiMatKhau/DoiMatKhau.aspx'>";
        html += "    <i class='fa fa-barcode'></i> <span>Đổi mật khẩu</span>";
        html += "  </a>";
        html += "</li>";
        /////////
        html += "</ul>";
        dvMenu.InnerHtml = html;
    }
}
