﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QuanLyNhapKho_NhapKho : System.Web.UI.Page
{
    string pChuoiTimKiem = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 1;
    int MaxPage = 0;
    int PageSize = 12;
    string mQuyen;
    string tenDangNhap_HienTai = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = tenDangNhap_HienTai = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiTaiKhoan = StaticData.getField("tb_TaiKhoan", "idLoaiTaiKhoan", "TenDangNhap", TenDangNhap_Cookie.ToString());

            mQuyen = StaticData.getField("tb_LoaiTaiKhoan", "TenLoaiTaiKhoan", "idLoaiTaiKhoan", idLoaiTaiKhoan);
            //if (mQuyen == "Giao nhận" || mQuyen == "Thủ kho")
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/DangNhap.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            try
            {
                pChuoiTimKiem = Request.QueryString["ChuoiTimKiem"].ToString();
                txtTenNguoiDung.Value = pChuoiTimKiem;
            }
            catch
            {
            }
            LoadBanner();
        }
    }
    #region paging
    private void SetPage()
    {
        string sql = @"select count(idBannerTrangChu) from tb_BannerTrangChu where '1'='1' ";
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    #endregion
    private void LoadBanner()
    {
        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY idBanner desc
                  )AS RowNumber
	              ,*
                  FROM tb_BannerTrangChu where '1'='1'  ";
        sql += ") as tb1 ";
        sql += "WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        DataTable table = Connect.GetTable(sql);
        SetPage();
        string html = @"<table class='ui table-hover unstackable table'>
                            <thead>
                                <th style='text-align: center; width: 50px'>
                                 STT
                              </th>
                              <th>
                                 Hình ảnh
                              </th>
                              <th style='min-width:150px;'>
                                Đường dẫn
                              </th> 
                              <th>
                              </th> 
                            </thead>
                    <tbody>";
        if (table != null)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                

                html += " <tr>";
                html += "       <td style='text-align:center;'>" + (((Page - 1) * PageSize) + i + 1) + "</td>";
                if (System.IO.File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "Images\\Banner\\" + table.Rows[i]["LinkAnh"]))
                    html += "       <td style='text-align:center;'><img src='/Images/Banner/" + table.Rows[i]["LinkAnh"] + "' style='width:150px;height:100px;'></td>";
                else
                    html += "       <td style='text-align:center;'></td>";

                html += "       <td style='text-align:center;'>" + table.Rows[i]["Url"] + "</td>";
   
                html += "       <td style='text-align: center; white-space: nowrap;'>";
                //{
                html += "           <a class='ui blue horizontal label' href='ThemSuaBanner.aspx?idBanner=" + table.Rows[i]["idBannerTrangChu"] + "'  >Sửa</a>";
                html += "           <a class='ui red horizontal label' onclick='XoaBanner(" + table.Rows[i]["idBannerTrangChu"] + ")' >Xóa</a> ";
                //}

                html += "       </td>";
                html += " </tr>";

            }

            html += "   <tr>";
            html += "       <td colspan='10' class='footertable'>";
            string url = "DanhMucBanner.aspx?";
            if (pChuoiTimKiem != "")
                url += "ChuoiTimKiem=" + pChuoiTimKiem + "&";
            url += "Page=";
            html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
            //Page 1
            if (txtPage1 != "")
            {
                if (Page.ToString() == txtPage1)
                    html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
                else
                    html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            }
            else
            {
                html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            }
            //Page 2
            if (txtPage2 != "")
            {
                if (Page.ToString() == txtPage2)
                    html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
                else
                    html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            }
            else
            {
                html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            }
            //Page 3
            if (txtPage3 != "")
            {
                if (Page.ToString() == txtPage3)
                    html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
                else
                    html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            }
            else
            {
                html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            }
            //Page 4
            if (txtPage4 != "")
            {
                if (Page.ToString() == txtPage4)
                    html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
                else
                    html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            }
            else
            {
                html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            }
            //Page 5
            if (txtPage5 != "")
            {
                if (Page.ToString() == txtPage5)
                    html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
                else
                    html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            }
            else
            {
                html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            }

            html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
            html += "   </td></tr>";
        }
        html += "   </tbody>";
        html += "     </table>";
        dvDanhSach.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string HoTen = txtTenNguoiDung.Value.Trim();
        string url = "DanhMucBanner.aspx?";
        if (HoTen != "")
            url += "ChuoiTimKiem=" + HoTen + "&";
        Response.Redirect(url);
    }
    protected void btn_Luu_Click(object sender, EventArgs e)
    {



    }
}