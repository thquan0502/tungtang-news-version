﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="BannerSanPham.aspx.cs" Inherits="Admin_QuanTriBanner_BannerTrangChu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        window.onload = function ()
        {
            $('#AnHinh1').html('');
            $('#AnLuu1').html('');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content-wrapper">
    <!-- Main content -->
    <div class="title" id="dvTitle" runat="server">BANNER SẢN PHẨM</div>
    <%--<div class="title1"><a href="QuanLyBanner.aspx"><i class="fa fa-step-backward"></i> Danh sách banner</a></div>--%>
    <section class="content">
    <div class="box">
        <div class="box-body">
            <form class="form-horizontal" runat="server">
                <div class="form-group" id="AnHinh1" style="display:none">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Hình ảnh:</b></div>
                          <div class="txtinput">
                              <asp:FileUpload ID="fileLinkAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" />
                              <img id="imgLinkAnh" runat="server" src="" style="width:100%; height:180px" />
                          </div>
                        </div>
                        <%--<div class="coninput2">
                          <div class="titleinput"><b>Url:</b></div>
                          <div class="txtinput" style="padding-top: 4px;">
                              <input class="form-control" data-val="true" data-val-required="" id="txtUrl" runat="server" name="Content.ContentName" type="text" value=""/>
                          </div>
                        </div>--%>
                      </div>
                </div>
                <div class="box-footer" style="display:none" id="AnLuu1" >
                    <asp:Button ID="btLuu" runat="server" Text="LƯU" class="btn btn-primary btn-flat" OnClick="btLuu_Click" />
                    <asp:Button ID="btHuy" runat="server" Text="HỦY" class="btn btn-primary btn-flat" OnClick="btHuy_Click" />
                </div>
               <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Hình ảnh:</b></div>
                          <div class="txtinput">
                              <asp:FileUpload ID="fileLinkAnh1" ClientIDMode="Static" onchange="this.form.submit()" runat="server" />
                              <img id="imgLinkAnh1" runat="server" src="" style="width:100%; height:180px" />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Url:</b></div>
                          <div class="txtinput" style="padding-top: 4px;">
                              <input class="form-control" data-val="true" data-val-required="" id="txtUrl1" runat="server" name="Content.ContentName" type="text" value=""/>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btLuu1" runat="server" Text="LƯU" class="btn btn-primary btn-flat" OnClick="btLuu1_Click" />
                    <asp:Button ID="btHuy1" runat="server" Text="HỦY" class="btn btn-primary btn-flat" OnClick="btHuy1_Click" />
                </div>
                <%--<div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Hình ảnh:</b></div>
                          <div class="txtinput">
                              <asp:FileUpload ID="fileLinkAnh2" ClientIDMode="Static" onchange="this.form.submit()" runat="server" />
                              <img id="imgLinkAnh2" runat="server" src="" style="width:100%; height:180px" />
                          </div>
                        </div>
                        <div class="coninput2">
                          <div class="titleinput"><b>Url:</b></div>
                          <div class="txtinput" style="padding-top: 4px;">
                              <input class="form-control" data-val="true" data-val-required="" id="txtUrl2" runat="server" name="Content.ContentName" type="text" value=""/>
                          </div>
                        </div>
                      </div>
                </div>--%>
                <%--<div class="box-footer">
                    <asp:Button ID="btLuu2" runat="server" Text="LƯU" class="btn btn-primary btn-flat" OnClick="btLuu2_Click" />
                    <asp:Button ID="btHuy2" runat="server" Text="HỦY" class="btn btn-primary btn-flat" OnClick="btHuy2_Click" />
                </div>--%>
            </form>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
</asp:Content>

