﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriBanner_BannerTrangChu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        if (!IsPostBack)
        {
            LoadThongTinBanner();
        }
        if (IsPostBack && fileLinkAnh.PostedFile != null)
        {
            if (fileLinkAnh.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnh.HasFile)
                    {
                        string Ngay = DateTime.Now.Day.ToString();
                        string Thang = DateTime.Now.Month.ToString();
                        string Nam = DateTime.Now.Year.ToString();
                        string Gio = DateTime.Now.Hour.ToString();
                        string Phut = DateTime.Now.Minute.ToString();
                        string Giay = DateTime.Now.Second.ToString();
                        string Khac = DateTime.Now.Ticks.ToString();
                        string fExtension = Path.GetExtension(fileLinkAnh.PostedFile.FileName);
                        string FileName = "BNTC" + Ngay + Thang + Nam + Gio + Phut + Giay + Khac + fExtension;
                        string FilePath = "../../Images/Advertisement/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnh.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnh.Src = FilePath;
                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }
        if (IsPostBack && fileLinkAnh1.PostedFile != null)
        {

            if (fileLinkAnh1.PostedFile.FileName.Length > 0)
            {
                string extension = Path.GetExtension(fileLinkAnh1.PostedFile.FileName);
                if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                {
                    if (fileLinkAnh1.HasFile)
                    {
                        string Ngay = DateTime.Now.Day.ToString();
                        string Thang = DateTime.Now.Month.ToString();
                        string Nam = DateTime.Now.Year.ToString();
                        string Gio = DateTime.Now.Hour.ToString();
                        string Phut = DateTime.Now.Minute.ToString();
                        string Giay = DateTime.Now.Second.ToString();
                        string Khac = DateTime.Now.Ticks.ToString();
                        string fExtension = Path.GetExtension(fileLinkAnh1.PostedFile.FileName);
                        string FileName = "BNTC" + Ngay + Thang + Nam + Gio + Phut + Giay + Khac + fExtension;
                        string FilePath = "../../Images/Advertisement/" + FileName;
                        //LinkAnh = "Images/SanPham/" + FileName;
                        fileLinkAnh1.SaveAs(Server.MapPath(FilePath));
                        imgLinkAnh1.Src = FilePath;

                    }
                }
                else
                {
                    Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                    return;
                }
            }
        }
        //if (IsPostBack && fileLinkAnh2.PostedFile != null)
        //{

        //    if (fileLinkAnh2.PostedFile.FileName.Length > 0)
        //    {
        //        string extension = Path.GetExtension(fileLinkAnh2.PostedFile.FileName);
        //        if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
        //        {
        //            if (fileLinkAnh2.HasFile)
        //            {
        //                string Ngay = DateTime.Now.Day.ToString();
        //                string Thang = DateTime.Now.Month.ToString();
        //                string Nam = DateTime.Now.Year.ToString();
        //                string Gio = DateTime.Now.Hour.ToString();
        //                string Phut = DateTime.Now.Minute.ToString();
        //                string Giay = DateTime.Now.Second.ToString();
        //                string Khac = DateTime.Now.Ticks.ToString();
        //                string fExtension = Path.GetExtension(fileLinkAnh2.PostedFile.FileName);
        //                string FileName = "BNTC" + Ngay + Thang + Nam + Gio + Phut + Giay + Khac + fExtension;
        //                string FilePath = "../../Images/Banner/" + FileName;
        //                //LinkAnh = "Images/SanPham/" + FileName;
        //                fileLinkAnh2.SaveAs(Server.MapPath(FilePath));
        //                imgLinkAnh2.Src = FilePath;

        //            }
        //        }
        //        else
        //        {
        //            Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
        //            return;
        //        }
        //    }
        //}
    }
    private void LoadThongTinBanner()
    {
        string sql = "select * from tb_BannerTinDang where [idBannerTinDang]=3";
        DataTable table = Connect.GetTable(sql);
        if (table.Rows.Count > 0)
        {
            //dvTitle.InnerHtml = "BANNER TRANG CHỦ";
            btLuu.Text = "LƯU";
            // imgLinkAnh.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
            // txtUrl.Value = table.Rows[0]["Url"].ToString();
            imgLinkAnh1.Src = "../../" + table.Rows[0]["LinkAnh"].ToString();
             txtUrl1.Value = table.Rows[0]["Url"].ToString();
            //  imgLinkAnh2.Src = "../../" + table.Rows[2]["LinkAnh"].ToString();
            //  txtUrl2.Value = table.Rows[2]["Url"].ToString();
        }
    }
    protected void btLuu_Click(object sender, EventArgs e)
    {
        //string sql = "select * from tb_BannerTrangChu where idBannerTrangChu=1";
        //DataTable table = Connect.GetTable(sql);
        string LinkAnh = imgLinkAnh.Src.Replace("../../", "");


        string sqlUpdateBanner = "update tb_BannerTinDang set ";
        sqlUpdateBanner += "LinkAnh=N'" + LinkAnh + "'";
        sqlUpdateBanner += " where idBannerTinDang=3 ";
        bool ktUpdateBanner = Connect.Exec(sqlUpdateBanner);
        if (ktUpdateBanner)
        {
            //Response.Redirect("BannerTrangChu.aspx");
            Response.Write("<script>alert('Lưu thành công!')</script>");
        }
        else
        {
            Response.Write("<script>alert('Lỗi !')</script>");
        }


        //string Url = txtUrl.Value;
        //if (table.Rows.Count == 0)
        //{
        //    string sqlInsertBanner = "insert into tb_BannerTrangChu(LinkAnh,Url)";
        //    sqlInsertBanner += " values(N'" + LinkAnh + "',N'" + Url + "')";
        //    bool ktInsertBanner = Connect.Exec(sqlInsertBanner);
        //    if (ktInsertBanner)
        //    {
        //        //Response.Redirect("BannerTrangChu.aspx");
        //        Response.Write("<script>alert('Lưu thành công!')</script>");
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('Lỗi thêm banner trang chủ!')</script>");
        //    }
        //}
        //else
        //{
        //    string sqlUpdateBanner = "update tb_BannerTrangChu set ";
        //    sqlUpdateBanner += "LinkAnh=N'" + LinkAnh + "'";
        //    sqlUpdateBanner += ",Url=N'" + Url + "'";
        //    bool ktUpdateBanner = Connect.Exec(sqlUpdateBanner);
        //    if (ktUpdateBanner)
        //    {
        //        //Response.Redirect("BannerTrangChu.aspx");
        //        Response.Write("<script>alert('Lưu thành công!')</script>");
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('Lỗi !')</script>");
        //    }
        //}
    }
    protected void btLuu1_Click(object sender, EventArgs e)
    {
        string sql = "select * from tb_BannerTinDang where idBannerTinDang=3";
        DataTable table = Connect.GetTable(sql);
        string LinkAnh = imgLinkAnh1.Src.Replace("../../", "");
        string Url1 = txtUrl1.Value;
        if (table.Rows.Count == 0)
        {
            string sqlInsertBanner = "insert into tb_BannerTinDang(LinkAnh,Url)";
            sqlInsertBanner += " values(N'" + LinkAnh + "',N'" + Url1 + "')";
            bool ktInsertBanner = Connect.Exec(sqlInsertBanner);
            if (ktInsertBanner)
            {
                //Response.Redirect("BannerTrangChu.aspx");
                Response.Write("<script>alert('Lưu thành công!')</script>");
            }
            else
            {
                Response.Write("<script>alert('Lỗi thêm banner trang chủ!')</script>");
            }
        }
        else
        {
            string sqlUpdateBanner = "update tb_BannerTinDang set ";
            sqlUpdateBanner += "LinkAnh=N'" + LinkAnh + "'";
            sqlUpdateBanner += ",Url=N'" + Url1 + "' where idBannerTinDang=3";
            bool ktUpdateBanner = Connect.Exec(sqlUpdateBanner);
            if (ktUpdateBanner)
            {
                //Response.Redirect("BannerTrangChu.aspx");
                Response.Write("<script>alert('Lưu thành công!')</script>");
            }
            else
            {
                Response.Write("<script>alert('Lỗi !')</script>");
            }
        }
    }
    protected void btLuu2_Click(object sender, EventArgs e)
    {
        //  string sql = "select * from tb_BannerTrangChu where idBannerTrangChu=3";
        //  DataTable table = Connect.GetTable(sql);
        //string LinkAnh = imgLinkAnh2.Src.Replace("../../", "");
        //string Url = txtUrl2.Value;
        //if (table.Rows.Count == 0)
        //{
        //    string sqlInsertBanner = "insert into tb_BannerTrangChu(LinkAnh,Url)";
        //    sqlInsertBanner += " values(N'" + LinkAnh + "',N'" + Url + "')";
        //    bool ktInsertBanner = Connect.Exec(sqlInsertBanner);
        //    if (ktInsertBanner)
        //    {
        //        //Response.Redirect("BannerTrangChu.aspx");
        //        Response.Write("<script>alert('Lưu thành công!')</script>");
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('Lỗi thêm banner trang chủ!')</script>");
        //    }
        //}
        //else
        //{
        //    string sqlUpdateBanner = "update tb_BannerTrangChu set ";
        //    sqlUpdateBanner += "LinkAnh=N'" + LinkAnh + "'";
        //    sqlUpdateBanner += ",Url=N'" + Url + "' where idBannerTrangChu=3";
        //    bool ktUpdateBanner = Connect.Exec(sqlUpdateBanner);
        //    if (ktUpdateBanner)
        //    {
        //        //Response.Redirect("BannerTrangChu.aspx");
        //        Response.Write("<script>alert('Lưu thành công!')</script>");
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('Lỗi !')</script>");
        //    }
        //}
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("BannerSanPham.aspx");
    }
    protected void btHuy1_Click(object sender, EventArgs e)
    {
        Response.Redirect("BannerSanPham.aspx");
    }
    protected void btHuy2_Click(object sender, EventArgs e)
    {
        Response.Redirect("BannerSanPham.aspx");
    }
}