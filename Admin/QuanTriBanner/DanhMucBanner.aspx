﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="DanhMucBanner.aspx.cs" Inherits="QuanLyNhapKho_NhapKho" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server" class="ui form">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="box">
                    <input type="hidden" name="name" value="" runat="server" id="txtidTK" />
                    <div class="ui small breadcrumb">
                        <a class="section"><i class="large home icon" style="margin-top: -1px;"></i></a>
                        <i class="right chevron icon divider"></i>
                        <div class="active section">DANH MỤC BANNER</div>
                    </div>
                    <div class="box-body">
                        <div class="three fields">
                            <div class="field" style="display: none;">
                                <div class="ui left icon input">
                                    <input id="txtTenNguoiDung" type="text" placeholder="Tiêu Đề" runat="server" />
                                    <i class="search icon"></i>
                                </div>
                            </div>
                            <div class="field">
                                <div class="fields">
                                    <div class="field" style="display: none;">
                                        <asp:LinkButton ID="btTimKiem" class="ui waves-effect waves-light primary button" Width="130px" runat="server">  <i class="search icon"></i> Tìm kiếm </asp:LinkButton>
                                    </div>
                                    <div class="field" style="display: none;">
                                        <a href="QuanLyBanner.aspx" class="ui icon primary button"><i class="refresh icon"></i></a>
                                    </div>
                                    <div class="field">
                                        <a href="ThemSuaBanner.aspx" class="ui waves-effect waves-light primary labeled icon button" style="width: 147px" id="btnThemMoi" runat="server"><i class="plus icon"></i>Thêm mới </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dvDanhSach" runat="server" style='overflow: auto; margin-bottom: 12px;'>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>

        <!--Popup cập nhật nhập kho-->
        <div class="ui modal scrolling transition hidden" id="dvPopup" style="padding: 10px; top: 10px; display: block !important;">
            <div style="font-weight: bold; font-size: 18px;" id="title_Popup">TITLE</div>
            <div class="box-body">
                <div class="content">
                    <div class="ui form">
                        <div class="ui grid">
                            <input type="text" name="name" value="" id="txtidBanner" runat="server" style="display: none;" />
                            <div class="eight wide column">
                                <div class="ui fluid image" style="width: 100%; margin-bottom: 1rem; cursor: pointer;" onclick="$('#ContentPlaceHolder1_fuHinhDaiDien').click();">
                                    <a class="ui red right corner label">
                                        <i class="save icon"></i>
                                    </a>
                                    <img src="../images/wireframe/image_corner%20.png" id="imgHinhAnh" runat="server" />
                                </div>
                                <asp:FileUpload ID="fuHinhDaiDien" accept=".png,.jpg,.jpeg,.gif" runat="server" onchange="UploadHinhAnh_Onchange(this,'imgHinhAnh');" Style="display: none" />
                            </div>

                            <div class="eight wide column">
                                <label>Đường dẫn : </label>
                                <input placeholder="" type="text" id="txtDuongDan_Popup" runat="server" />
                                <div class="field">
                                    <label>Nội dung: </label>
                                    <textarea
                                        style="width: 100%; height: 50px;"
                                        name="Text1"
                                        id="txtNoiDung_Popup" runat="server" />
                                    </textarea>
                                </div>
                                <div class="ui toggle checkbox">
                                    <input type="checkbox" name="public" id="cb_TrangThai" runat="server" />
                                    <label for="ContentPlaceHolder1_cb_TrangThai">Ẩn hiện bài viết</label>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
            <div class='actions'>
                <a class='waves-effect waves-light negative ui cancel right labeled icon button' style=""><i class='remove icon'></i>Đóng </a>
                <asp:LinkButton runat="server" ID="btn_Luu" OnClick="btn_Luu_Click" class="'waves-effect waves-light ui green right labeled icon button"><i class='checkmark icon'></i>
                                                      Lưu
                </asp:LinkButton> 
            </div>
        </div>

    </form>

    <div class="ui modal" id="dvBanner2">
        <div id="dvTieuDeCapNhat" class="header" style="margin-top: -40px;"></div>

        <div class="content">
            <div class="ui form">
                <div id="ShowBanner">
                </div>
            </div>
        </div>
        <div class="actions">
            <a class='waves-effect waves-light negative ui cancel right labeled icon button'><i class='remove icon'></i>Đóng </a>
        </div>
    </div>
    <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
    <link href="../plugins/datetimePicker1/build/jquery.datetimepicker.min.css" rel="stylesheet" />
    <script>
function ShowPopup_ThemMoi() {
            //$("#ContentPlaceHolder1_txtMaBanner_Popup").val('');
            $("#ContentPlaceHolder1_txtNgay_Popup").val('');
            $("#txtTieuDe_Popup").val('');
            $("#txtNoiDung_Popup").val('');
            $("#txtDiaDiem_Popup").val('');

            $("#title_Popup").html("THÊM MỚI BANNER");
            $("#btnAction").attr("onclick", "ThemBanner()");
            $("#dvPopup").modal('show');
        }
        function ShowPopup_ChinhSua(idBanner) {
            // alert('hjtgtrgrgtdgtrhsdgtrh')
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText != "") {
                        var arr = xmlhttp.responseText.split("@_@");
                        $("#ContentPlaceHolder1_txtNgay_Popup").val(arr[1].trim());
                        $("#txtTieuDe_Popup").val(arr[2].trim());
                        $("#txtNoiDung_Popup").val(arr[3].trim());
                        $("#txtDiaDiem_Popup").val(arr[4].trim());

                        $("#title_Popup").html("SỬA Banner");
                        $("#btnAction").attr("onclick", "SuaBanner(" + idBanner + ")");
                        $("#dvPopup").modal('show')

                    }
                    else {
                        alert("Lỗi load thông tin Banner !")
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=LoadBanner&idBanner=" + idBanner, true);
            xmlhttp.send();
        }

        function SuaBanner(idBanner) {
            var Ngay = $("#ContentPlaceHolder1_txtNgay_Popup").val().trim();
            var TieuDe = $("#txtTieuDe_Popup").val().trim();
            var NoiDung = $("#txtNoiDung_Popup").val().trim();
            var DiaDiem = $("#txtDiaDiem_Popup").val().trim();

            if (Ngay == "")
                alert("Vui lòng nhập ngày !");
            else if (TieuDe == "")
                alert("Vui lòng nhập tiêu đề !");
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "CapNhatThanhCong") {
                            window.location.reload();
                        }
                        else if (xmlhttp.responseText == "TrungMa") {
                            alert("Trùng mã khách hàng !");
                        }
                        else {
                            alert("Lỗi thêm khách hàng !");
                        }

                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=XacNhanSuaBanner&idBanner=" + idBanner + "&Ngay=" + Ngay + "&TieuDe=" + TieuDe + "&NoiDung=" + NoiDung + "&DiaDiem=" + DiaDiem, true);
                xmlhttp.send();
            }
        }
        function XoaBanner(idBanner) {
            swal({
                title: "Bạn có muốn xóa Banner ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
         .then((willDelete) => {
             if (willDelete)
             {
                 var xmlhttp;
                 if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                     xmlhttp = new XMLHttpRequest();
                 }
                 else {// code for IE6, IE5
                     xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                 }
                 xmlhttp.onreadystatechange = function () {
                     if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                         if (xmlhttp.responseText == "XoaThanhCong") {
                             swal("Xóa thành công !", {
                                 icon: "success",
                             });
                             setTimeout(location.reload.bind(location), 1000);

                         }
                     }
                 }
                 xmlhttp.open("GET", "../Ajax.aspx?Action=XoaBanner&idBanner=" + idBanner, true);
                 xmlhttp.send();
             }
         });
        }
        function ThemBanner() {
            var Ngay = $("#ContentPlaceHolder1_txtNgay_Popup").val().trim();
            var TieuDe = $("#txtTieuDe_Popup").val().trim();
            var NoiDung = $("#txtNoiDung_Popup").val().trim();
            var DiaDiem = $("#txtDiaDiem_Popup").val().trim();


            if (Ngay == "")
                alert("Vui lòng nhập ngày !");
            else if (TieuDe == "")
                alert("Vui lòng nhập tiêu đề !");
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "ThemThanhCong") {
                            window.location.reload();
                        }
                        else if (xmlhttp.responseText == "TrungMa") {
                            alert("Trùng  !");
                        }
                        else {
                            alert("Lỗi thêm Banner !");
                        }

                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=ThemBanner&Ngay=" + Ngay + "&TieuDe=" + TieuDe + "&NoiDung=" + NoiDung + "&DiaDiem=" + DiaDiem, true);
                xmlhttp.send();
            }
        }
        function TrangThaiChuyenHuong_Banner_onchange(HH, value, i) {

            if (!document.getElementById(value).checked)
                value = "False";
            else
                value = "True";

            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "OK") {
                        $("#spBanner_" + i).attr("style", "color:green;display:block;");
                        $("#spBanner_" + i).html("Đã cập nhật");
                        setTimeout(function () {
                            $("#spChiTiet_" + i).attr("style", "display:none;");
                        }, 3000);
                    }
                    else {
                        $("#spBanner_" + i).attr("style", "color:red;display:block;");
                        $("#spBanner_" + i).html("Lỗi");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=TrangThaiChuyenHuong_Banner_onchange&HH=" + HH + "&value=" + value, true);
            xmlhttp.send();
        }
        function TrangThaiHienThi_Banner_onchange(HH, value, i) {

            if (!document.getElementById(value).checked)
                value = "False";
            else
                value = "True";

            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "OK") {
                        $("#spBanner_HienThi_" + i).attr("style", "color:green;display:block;");
                        $("#spBanner_HienThi_" + i).html("Đã cập nhật");
                        setTimeout(function () {
                            $("#spChiTiet_" + i).attr("style", "display:none;");
                        }, 3000);
                    }
                    else {
                        $("#spBanner_HienThi_" + i).attr("style", "color:red;display:block;");
                        $("#spBanner_HienThi_" + i).html("Lỗi");
                    }
                }
            }
            xmlhttp.open("GET", "../Ajax.aspx?Action=TrangThaiHienThi_Banner_onchange&HH=" + HH + "&value=" + value, true);
            xmlhttp.send();
        }
        $('#').datetimepicker({
            //dayOfWeekStart : 1,
            //todayBtn: "linked",
            language: "it",
            autoclose: true,
            todayHighlight: true,
            timepicker: false,
            dateFormat: 'dd/mm/yyyy',
            format: 'd/m/Y',
            formatDate: 'Y/m/d',
            //value: 'today'
        }); 

        function UploadHinhAnh_Onchange(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</asp:Content>

