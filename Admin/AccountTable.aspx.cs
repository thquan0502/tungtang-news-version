﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.Caching;

public partial class Admin_QuanLyThanhVien_QuanLyThanhVienA : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sHoTen = "";
    string sSoDienThoai = "";
    string TinhThanh = "";
	string LoaiThanhVien = "";
    string slistLabel = "";
    string sruler = "";
    string sRangeLuotXem = "";

    string slistLabelhh = "";
    string srulerhh = "";

    string slistLabelchh = "";
    string srulerchh = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
              //  Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            Load_SelectHTML("select  * from City order by type asc,Ten asc", "Ten", "id", true, "-- Tất cả --", slTinhThanh);
			 Load_SelectSDT("select  (SoDienThoai + ' - ' + TenCuaHang) as IdTV,SoDienThoai from tb_ThanhVien order by NgayDangKy desc", "IdTV", "SoDienThoai", true, "-- Tất cả --", SlSdt);
            
            try
            {
                if (Request.QueryString["HoTen"].Trim() != "")
                {
                    sHoTen = Request.QueryString["HoTen"].Trim();
                    txtHoTen.Value = sHoTen;
                }
            }
            catch { }
            try
            {
                sSoDienThoai = Request.QueryString["SoDienThoai"].Trim();
                SlSdt.Value = sSoDienThoai;
            }
            catch { }
            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }
			 try
            {
                LoaiThanhVien = Request.QueryString["LoaiThanhVien"].Trim();
                slTV.Value = LoaiThanhVien;
            }
            catch { }
            try
            {
                if (Request.QueryString["Luotxem"].Trim() != "")
                {
                    slistLabel = Request.QueryString["Luotxem"].Trim();
                    txtRangeLuotXem.Value = slistLabel;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterViewed"].Trim() != "")
                {
                    sruler = Request.QueryString["RangeCounterViewed"].Trim();
                    ruler.InnerHtml = sruler;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["HoaHong"].Trim() != "")
                {
                    slistLabelhh = Request.QueryString["HoaHong"].Trim();
                    txtRangeLuotXemhh.Value = slistLabelhh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterCommission"].Trim() != "")
                {
                    srulerhh = Request.QueryString["RangeCounterCommission"].Trim();
                    rulerhh.InnerHtml = srulerhh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["ChiHoaHong"].Trim() != "")
                {
                    slistLabelchh = Request.QueryString["ChiHoaHong"].Trim();
                    txtRangeLuotXemchh.Value = slistLabelchh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterFee"].Trim() != "")
                {
                    srulerchh = Request.QueryString["RangeCounterFee"].Trim();
                    rulerchh.InnerHtml = srulerchh;
                }
            }
            catch { }

            try
            {
                TinhThanh = Request.QueryString["TT"].Trim();
                slTinhThanh.Value = TinhThanh;
            }
            catch { }

            LoadTinDang();                      
        }
    }

    protected override void SavePageStateToPersistenceMedium(Object viewState)
    {
        string _vskey;
        _vskey = "VIEWSTATE_" + base.Session.SessionID + "_" + Request.RawUrl +
        "_" + System.DateTime.Now.Ticks.ToString();

        Cache.Add(_vskey, viewState, null,
        System.DateTime.Now.AddMinutes(Session.Timeout), Cache.NoSlidingExpiration,
        CacheItemPriority.Default, null);

        RegisterHiddenField("_VIEWSTATE_KEY", _vskey);
    }

    protected override object LoadPageStateFromPersistenceMedium()
    {
        string _vskey = Request.Form["_VIEWSTATE_KEY"];

        if (_vskey == null)
        {
            return null;
        }
        else
        {
            return Cache[_vskey];
        }
    }

    void Load_SelectHTML(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        slTinhThanh.DataSource = table;
        slTinhThanh.DataTextField = TextField;
        slTinhThanh.DataValueField = ValueField;
        slTinhThanh.DataBind();

        if (AddANewItem)
        {
            slTinhThanh.Items.Insert(0, (new ListItem(ItemName, "")));
            slTinhThanh.Items.FindByText(ItemName).Selected = true;
        }
    }
	  void Load_SelectSDT(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        SlSdt.DataSource = table;
        SlSdt.DataTextField = TextField;
        SlSdt.DataValueField = ValueField;
        SlSdt.DataBind();

        if (AddANewItem)
        {
            SlSdt.Items.Insert(0, (new ListItem(ItemName, "")));
            SlSdt.Items.FindByText(ItemName).Selected = true;
        }
    }
    #region paging
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string paramsLinkCommission = "&FilterReferralStatus="+Models.Referral.STATUS_FEE_COMMISSION;
        string paramsLinkFee = "&FilterReferralStatus=" + Models.Referral.STATUS_FEE_COMMISSION;

        string colViewedCounterName, colViewedCounterRealName;
        string colCommissionCounterName, colCommissionCounterRealName;
        string colFeeCounterName, colFeeCounterRealName;

        string filterRangeViewedCounter = null;
        int filterRangeFromViewedCounter = -1, filterRangeToViewedCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterViewed")))
        {
            filterRangeViewedCounter = Request.QueryString.Get("RangeCounterViewed").Trim();
            string[] filterRangeViewedCounterVals = { };
            filterRangeViewedCounterVals = filterRangeViewedCounter.Split(';');
            if (filterRangeViewedCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeViewedCounterVals[0], out filterRangeFromViewedCounter);
                Int32.TryParse(filterRangeViewedCounterVals[1], out filterRangeToViewedCounter);
                txtViewedCounterRange.Text = filterRangeViewedCounter;
            }
        }

        string filterRangeCommissionCounter = null;
        int filterRangeFromCommissionCounter = -1, filterRangeToCommissionCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterCommission")))
        {
            filterRangeCommissionCounter = Request.QueryString.Get("RangeCounterCommission").Trim();
            string[] filterRangeCommissionCounterVals = { };
            filterRangeCommissionCounterVals = filterRangeCommissionCounter.Split(';');
            if (filterRangeCommissionCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeCommissionCounterVals[0], out filterRangeFromCommissionCounter);
                Int32.TryParse(filterRangeCommissionCounterVals[1], out filterRangeToCommissionCounter);
                txtCommissionCounterRange.Text = filterRangeCommissionCounter;
            }
        }

        string filterRangeFeeCounter = null;
        int filterRangeFromFeeCounter = -1, filterRangeToFeeCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterFee")))
        {
            filterRangeFeeCounter = Request.QueryString.Get("RangeCounterFee").Trim();
            string[] filterRangeFeeCounterVals = { };
            filterRangeFeeCounterVals = filterRangeFeeCounter.Split(';');
            if (filterRangeFeeCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeFeeCounterVals[0], out filterRangeFromFeeCounter);
                Int32.TryParse(filterRangeFeeCounterVals[1], out filterRangeToFeeCounter);
                txtFeeCounterRange.Text = filterRangeFeeCounter;
            }
        }

        string filterRangeFromReferralDate = null, filterRangeFromReferralDateAlt = null;
        DateTime filterRangeFromReferralDate_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeFromReferralDate")))
        {
            filterRangeFromReferralDate = Request.QueryString.Get("RangeFromReferralDate").Trim();
            this.txtRangeReferralFrom.Text = filterRangeFromReferralDate;
            paramsLinkCommission += "FilterFromApprovedAt="+ filterRangeFromReferralDate + "&";
            paramsLinkFee += "FilterFromApprovedAt=" + filterRangeFromReferralDate + "&";
            if (DateTime.TryParseExact(filterRangeFromReferralDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterRangeFromReferralDate_DateTime))
            {
                filterRangeFromReferralDateAlt = filterRangeFromReferralDate_DateTime.ToString("yyyy-MM-dd") + " 00:00:00:000";
            }
        }
        string filterRangeToReferralDate = null, filterRangeToReferralDateAlt = null;
        DateTime filterRangeToReferralDate_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeToReferralDate")))
        {
            filterRangeToReferralDate = Request.QueryString.Get("RangeToReferralDate").Trim();
            this.txtRangeReferralTo.Text = filterRangeToReferralDate;
            paramsLinkCommission += "FilterToApprovedAt=" + filterRangeToReferralDate + "&";
            paramsLinkFee += "FilterToApprovedAt=" + filterRangeToReferralDate + "&";
            if (DateTime.TryParseExact(filterRangeToReferralDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterRangeToReferralDate_DateTime))
            {
                filterRangeToReferralDateAlt = filterRangeToReferralDate_DateTime.ToString("yyyy-MM-dd") + " 23:59:59:999";
            }
        }

        if(!string.IsNullOrWhiteSpace(filterRangeFromReferralDateAlt) || !string.IsNullOrWhiteSpace(filterRangeToReferralDateAlt))
        {
            Utilities.ViewedManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colViewedCounterName = "ViewedCounter";
            colViewedCounterRealName = "ViewedManager.FilterCurrentValue";

            Utilities.CommissionManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colCommissionCounterName = "CommissionCounter";
            colCommissionCounterRealName = "CommissionManager.FilterCurrentValue";

            Utilities.FeeManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colFeeCounterName = "FeeCounter";
            colFeeCounterRealName = "FeeManager.FilterCurrentValue";
        }
        else
        {
            colViewedCounterName = "ViewedCounter";
            colViewedCounterRealName = "ViewedManager.TotalViewed";

            colCommissionCounterName = "CommissionCounter";
            colCommissionCounterRealName = "CommissionManager.TotalCommission";
            colFeeCounterName = "FeeCounter";
            colFeeCounterRealName = "FeeManager.TotalFee";
        }

        string sql = "";
        sql += @"select tb1.* from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY tb_ThanhVien.NgayDangKy desc, "+ colViewedCounterRealName + @" desc, "+ colCommissionCounterRealName + @" DESC, " + colFeeCounterRealName + @" DESC
                  ) AS RowNumber, 

                      City.Ten as CityName,
                      District.Ten as DistrictName,
                      tb_PhuongXa.Ten as WardName,
                      tb_ThanhVien.*,
                      ISNULL(" + colViewedCounterRealName + @", 0) AS ViewedCounter,
                      ISNULL("+ colCommissionCounterRealName + @", 0) AS CommissionCounter,
                      ISNULL(" + colFeeCounterRealName + @", 0) AS FeeCounter
                  from tb_ThanhVien 
                	left join City on tb_ThanhVien.idTinh = City.id
	                left join District on tb_ThanhVien.idHuyen = District.id
	                left join tb_PhuongXa on tb_ThanhVien.idPhuongXa = tb_PhuongXa.id
                    INNER JOIN ViewedManager ON tb_ThanhVien.idThanhVien = ViewedManager.AccountId
                    INNER JOIN CommissionManager ON tb_ThanhVien.idThanhVien = CommissionManager.AccountId
                    INNER JOIN FeeManager ON tb_ThanhVien.idThanhVien = FeeManager.AccountId
where 1 = 1
            ";
        if (sHoTen != "")
            sql += " and TenCuaHang like N'%" + sHoTen + "%'";
		if (LoaiThanhVien != "")
        {
            if (LoaiThanhVien == "0")
                sql += " and StsPro !='1'";
            else if (LoaiThanhVien == "2")
                sql += "";
            else
                sql += " and StsPro ='1'";
        }
        if (TinhThanh != "")
            sql += " and idTinh=" + TinhThanh;
         if (sTuNgay != "")
        {
            if(LoaiThanhVien == "1")
            {
                sql += " and AdminAt >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
            }
            else
            sql += " and NgayDangKy >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        }    
        if (sDenNgay != "")
        {
            if (LoaiThanhVien == "1")
            {
                sql += " and AdminAt <= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
            }
            else
            sql += " and NgayDangKy <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";

        }       
        //Lọc Lượt xem
        if (slistLabel == "100500")
            sql += " and TotalViewed between 100 and 500";
        if (slistLabel == "5001000")
            sql += " and TotalViewed between 500 and 1000";
        if (slistLabel == "1000")
            sql += " and TotalViewed >= 1000";
        //if(checklx1.Checked)
        //{
        //    sql += " and TotalViewed between 100 and 500";
        //}

        if (slistLabel == "1005001000")
            sql += " and TotalViewed between 100 and 500 or TotalViewed >= 1000";
        if (slistLabel == "50010001000")
            sql += " and TotalViewed between 500 and 1000 or TotalViewed >= 1000";
        if (slistLabel == "1005005001000")
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000";
        if (slistLabel == "10050050010001000")
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000 or TotalViewed >= 1000";
        //Lọc Hoa hồng
     if (slistLabelhh == "10100")
            sql += " and (TotalCommission between 10000000 and 100000000)";
        if (slistLabelhh == "100500")
            sql += " and (TotalCommission between 100000000 and 500000000)";
        if (slistLabelhh == "500")
            sql += " and (TotalCommission >= 500000000)";
        if (slistLabelhh == "10100500")
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission >= 500000000)";
        if (slistLabelhh == "100500500")
            sql += " and (TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000)";
        if (slistLabelhh == "10100100500")
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000)";
        if (slistLabelhh == "10100100500500")
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000)";
        //Lọc Chi Hoa hồng
        if (slistLabelchh == "10100")
            sql += " and TotalFee between 10000000 and 100000000";
        if (slistLabelchh == "100500")
            sql += " and TotalFee between 100000000 and 500000000";
        if (slistLabelchh == "500")
            sql += " and TotalFee >= 500000000";
        if (slistLabelchh == "10100500")
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee >= 500000000";
        if (slistLabelchh == "100500500")
            sql += " and TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
        if (slistLabelchh == "10100100500")
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000";
        if (slistLabelchh == "10100100500500")
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
        if (sSoDienThoai != "")
            sql += " and SoDienThoai like N'%" + sSoDienThoai + "%'";
        //Lọc Lượt xem với thước
        if (sruler != "" && slistLabel == "0")
        {
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "100500")
        {
            sql += " and TotalViewed between 100 and 500";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "5001000")
        {
            sql += " and TotalViewed between 500 and 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "1000")
        {
            sql += " and TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "1005001000")
        {
            sql += " and TotalViewed between 100 and 500 or TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "50010001000")
        {
            sql += " and TotalViewed between 500 and 1000 or TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "1005005001000")
        {
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "10050050010001000")
        {
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000 or TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        //Lọc Hoa hồng với thước
              if (srulerhh != "" && slistLabelhh == "0")
        {
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100")
        {
            sql += " and TotalCommission between 10000000 and 100000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter  + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500")
        {
            sql += " and TotalCommission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter  + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "500")
        {
            sql += " and TotalCommission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100500")
        {
            sql += " and TotalCommission between 10000000 and 100000000 or TotalCommission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter  + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500500")
        {
            sql += " and TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter  + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500")
        {
            sql += " and TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter  + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500500")
        {
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000)";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter  + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter  + " ";
            }
        }
        //Lọc Chi hoa hồng với thước       
         if (srulerchh != "" && slistLabelchh == "0")
        {
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter  + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter  + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100")
        {
            sql += " and TotalFee between 10000000 and 100000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter  + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500")
        {
            sql += " and TotalFee between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter  + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "500")
        {
            sql += " and TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100500")
        {
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500500")
        {
            sql += " and TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500")
        {
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500500")
        {
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        sql += ") as tb1";

        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += " WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";

        
        DataTable table = Connect.GetTable(sql);

        txtTongThanhVien.Value = AllRowNumber.ToString();
        SetPage(AllRowNumber);

        if(paramsLinkCommission != "&")
        {
            paramsLinkCommission += "&FilterTypeApprovedAt=ReferralCollabApprovedAt";
        }
        else
        {
            paramsLinkCommission = "";
        }

        if (paramsLinkFee != "&")
        {
            paramsLinkFee += "&FilterTypeApprovedAt=ReferralOwnerApprovedAt";
        }
        else
        {
            paramsLinkFee = "";
        }

        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                               	<th class='th' colspan='9' style='text-align: center;'>Thông tin chi tiết</th>
                                <th class='th' colspan='8'  style='text-align: center;'>Tài khoản Pro</th>
                                <th class='th' colspan='6'  style='text-align: center;'></th>
                            </tr>
                            <tr>
                                <th class='th'>Mã thành viên</th>
                                <th class='th'>Tên thành viên</th>
                                <th class='th'>Ảnh đại diện</th>
                                <th class='th'>Địa chỉ</th>
                                <th class='th'>Số điên thoại</th>
                                <th class='th'>Email</th>
                               
                                <th class='th'>Tên đăng nhập</th>
                                <th class='th'>Mật khẩu</th>
                                <th class='th'>Ngày đăng ký</th>

                                <th class='th'>Số CMND/CCCD</th>
                                <th class='th'>Ảnh CMND Mặt trước</th>
                                <th class='th'>Ảnh CMND Mặt sau</th>
                                <th class='th'>Ngày gửi duyệt</th>
                                <th class='th'>Admin Duyệt</th>
                                <th class='th'>Ngày Admin duyệt</th>
                                <th class='th'>Lý Do Không Duyệt</th>
                                <th class='th'>Trạng thái</th>
								
							    <th class='th'> Tiền hoa hồng</th>
                                <th class='th'> Chi hoa hồng</th>
                                <th class='th'> Lượt xem</th>
                                <th class='th'>isKhoa</th>
                                <th class='th'></th>
                                <th class='th'></th>
                            </tr>";
         for (int i = 0; i < table.Rows.Count; i++)
        {
            html += " <tr>";
            if(table.Rows[i]["StsPro"].ToString().Trim() == "1")
            {
                html += " <td>" + table.Rows[i]["Code"] + "-PRO</td>";
            }
            else
                html += " <td>" + table.Rows[i]["Code"] + "</td>";

            html += "       <td>" + table.Rows[i]["TenCuaHang"] + "</td>";
            html += "       <td><img src='../../Images/User/" + table.Rows[i]["LinkAnh"] + "' style='width:115px'/></td>";
            
            html += "       <td class='social'>" + table.Rows[i]["DiaChi"]+ ", " + table.Rows[i]["WardName"].ToString() + ", " + table.Rows[i]["DistrictName"].ToString() + ", " + table.Rows[i]["CityName"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["SoDienThoai"] + "</td>";
            html += "       <td>" + table.Rows[i]["Email"] + "</td>";
            
            html += "       <td>" + table.Rows[i]["TenDangNhap"] + "</td>";
            html += "       <td>" + table.Rows[i]["MatKhau"] + "</td>";
            try
            {
                html += "       <td>" + DateTime.Parse(table.Rows[i]["NgayDangKy"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            }
            catch
            {
                html += "       <td></td>";
            }

            //pro
            html += "       <td>" + table.Rows[i]["SoCMND"] + "</td>";
            html += "       <td><img src='../../Images/User/" + table.Rows[i]["HeadCMND"] + "' style='width:115px'/></td>";
            html += "       <td><img src='../../Images/User/" + table.Rows[i]["FooterCMND"] + "' style='width:115px'/></td>";
            //new
            if (table.Rows[i]["ProAt"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["ProAt"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";
            if (table.Rows[i]["idAdmin_Duyet"].ToString() != "")
                html += "       <td>" + StaticData.getField("tb_Admin", "TenDangNhap", "idAdmin", table.Rows[i]["idAdmin_Duyet"].ToString()) + "</td>";
            else
                html += "   <td></td>";
            if (table.Rows[i]["AdminAt"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["AdminAt"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";
            if (table.Rows[i]["LyDo_KhongDuyet"].ToString() != "")
                html += "   <td>" + table.Rows[i]["LyDo_KhongDuyet"].ToString() + "</td>";
            else
                html += "   <td></td>";
			 //Trạng thái
            if (table.Rows[i]["HeadCMND"].ToString().Trim() == "" && table.Rows[i]["FooterCMND"].ToString().Trim() == "")
            {
                html += "<td> Chưa đăng ký </td>";
            }
            else
            {
            html += "       <td><select id='slDuyet_" + table.Rows[i]["idThanhVien"] + "' disabled='disabled' class='form-control' style='width:122px'>";
             if (table.Rows[i]["idAdmin_Duyet"].ToString().Trim() == "-1")
                html += "<option value='' selected='selected'>Đang chờ...</option>";
             else if(table.Rows[i]["StsPro"].ToString().Trim() == "1" || table.Rows[i]["LyDo_KhongDuyet"].ToString().Trim() != "")
                {
                html += "";
                }
            else
                html += "<option value=''>Đang chờ...</option>";
            if (table.Rows[i]["StsPro"].ToString().Trim() == "1")
                html += "<option value='Duyet' selected='selected'>Duyệt</option>";
            else
                html += "<option value='Duyet'>Duyệt</option>";
            if (table.Rows[i]["LyDo_KhongDuyet"].ToString().Trim() != "")
                html += "<option value='KhongDuyet' selected='selected'>Không duyệt</option>";
			  else
                html += "<option value='KhongDuyet'>Không duyệt</option>";
            html += "       </select></td>";
			}
            //end new 
            //end pro  
			html += "       <td><a href='/Admin/Referral/ReferralTableByCollab.aspx?CollabId=" + table.Rows[i]["idThanhVien"].ToString() + (paramsLinkCommission != "" ? paramsLinkCommission : "") + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["CommissionCounter"].ToString()) + "</a></td>";
            html += "       <td><a href='/Admin/Referral/ReferralTableByOwner.aspx?OwnerId=" + table.Rows[i]["idThanhVien"].ToString() + (paramsLinkFee != "" ? paramsLinkFee : "") + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["FeeCounter"].ToString()) + "</a></td>";
            html += "       <td>" + table.Rows[i]["ViewedCounter"].ToString() + "</td>";
            if (table.Rows[i]["isKhoa"].ToString() == "True")
                html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled checked/></td>";
            else
                html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled/></td>";
           
            html += "       <td><a id='btKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' style='cursor:pointer' onclick='DuyetUserPro(\"" + table.Rows[i]["idThanhVien"].ToString() + "\")'><img class='imgedit' src='../images/edit.png'/>Sửa</a></td>";
            html += "       <td><a href='/Admin/Posts/PostTable.aspx?TenDangNhap=" + table.Rows[i]["TenDangNhap"].ToString() + "'>Xem tin đăng</a></td>";
            html += "       </tr>";
        }
        html += "   <tr>";
        html += "       <td colspan='17' class='footertable'>";
        string url = "AccountTable.aspx?";
        if (sHoTen != "")
            url += "HoTen=" + sHoTen + "&";
		if (LoaiThanhVien != "")
            url += "LoaiThanhVien=" + LoaiThanhVien + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";      
        //Lọc lượt xem
        if (txtRangeLuotXem.Value == "Từ 100 đến 500")
            url += "Luotxem=100500&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000")
            url += "Luotxem=5001000&";
        if (txtRangeLuotXem.Value == "Từ 1000 trở lên")
            url += "Luotxem=1000&";       
        //if(checklx1.Checked)
        //{
        //    url += "Luotxem=100500&";
        //}
           // url += "Luotxem=1000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 1000 trở lên")
            url += "Luotxem=1005001000&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=50010001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000")
            url += "Luotxem=1005005001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=10050050010001000&";
        //Lọc hoa hồng
         if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
            url += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
            url += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
            url += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=10100100500500&";
        //Lọc hoa hồng       
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
            url += "ChiHoaHong=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
            url += "ChiHoaHong=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
            url += "ChiHoaHong=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "ChiHoaHong=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "ChiHoaHong=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=10100100500500&";
        if (sSoDienThoai != "")
            url += "SoDienThoai=" + sSoDienThoai + "&";
        if (TinhThanh != "")
            url += "TT=" + TinhThanh + "&";
        
        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvThanhVien.InnerHtml = html;
    }    
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string HoTen = txtHoTen.Value.Trim();
         string SoDienThoai = SlSdt.Value.Trim();
        string idTinhThanh = slTinhThanh.Value.Trim();
        string rangeCounterViewed = txtViewedCounterRange.Text.Trim();
        string rangeCounterCommission = txtCommissionCounterRange.Text.Trim();
        string rangeCounterFee = txtFeeCounterRange.Text.Trim();
        string rangeFromReferralDate = txtRangeReferralFrom.Text.Trim();
        string rangeToReferralDate = txtRangeReferralTo.Text.Trim();
		string LoaiThanhVien = slTV.Value.Trim();
                
        string url = "AccountTable.aspx?";
        if (HoTen != "")
            url += "HoTen=" + HoTen + "&";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (SoDienThoai != "")
            url += "SoDienThoai=" + SoDienThoai + "&";
        if (idTinhThanh != "")
            url += "TT=" + idTinhThanh + "&";
		 if(LoaiThanhVien !="")
            url += "LoaiThanhVien=" + LoaiThanhVien + "&";
        //Lọc Lượt xem
        if (txtRangeLuotXem.Value == "Chọn")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=0&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=100500&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 500 đến 1000")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=5001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=1000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500Từ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=1005001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 500 đến 1000Từ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=50010001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500Từ 500 đến 1000")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=1005005001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=10050050010001000&";
        }
        if (txtRangeLuotXem.Value == "Từ 100 đến 500")
            url += "Luotxem=100500&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000")
            url += "Luotxem=5001000&";
        if (txtRangeLuotXem.Value == "Từ 1000 trở lên")
            url += "Luotxem=1000&";

        //if(checklx1.Checked)
        //{
        //    url += "Luotxem=100500&";
        //}
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 1000 trở lên")
            url += "Luotxem=1005001000&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=50010001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000")
            url += "Luotxem=1005005001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=10050050010001000&";

        //Lọc Hoa hồng
       if (txtRangeLuotXemhh.Value == "Chọn")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=0&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=100500500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100100500500&";
        }
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
            url += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
            url += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
            url += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=10100100500500&";
        //Lọc Chi Hoa hồng
        if (txtRangeLuotXemchh.Value == "Chọn")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=0&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=100500500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100100500500&";
        }
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
            url += "ChiHoaHong=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
            url += "ChiHoaHong=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
            url += "ChiHoaHong=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "ChiHoaHong=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "ChiHoaHong=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=10100100500500&";
        if (rangeFromReferralDate != "")
            url += "RangeFromReferralDate=" + rangeFromReferralDate + "&";
        if (rangeToReferralDate != "")
            url += "RangeToReferralDate=" + rangeToReferralDate + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "AccountTable.aspx";
        Response.Redirect(url);
    }

    protected void showList_Click(object sender, EventArgs e)
    {
        //listLabel.Text = "";
        ////loop through ArrayList to print selected list item
        //for (int i = 0; i < myCheckBoxes.Count; i++)
        //{
        //    listLabel.Text += myCheckBoxes[i].ToString() + " ";
        //}
    }    
}