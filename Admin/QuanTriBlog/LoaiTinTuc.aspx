﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="LoaiTinTuc.aspx.cs" Inherits="Admin_QuanTriBlog_DanhMucCap1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function DeleteDanhMucCap1(id) {
            if (confirm("Bạn có muốn xóa không ?")) {
                //alert(idSlide);
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Không thể xóa vì !")
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteLoaiTinTuc&id=" + id, true);
                xmlhttp.send();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
    <div class="title">QUẢN LÝ LOẠI BLOG</div>
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                    <div class="row">
                        <div class="dvnull">&nbsp;</div>
                        <div class="coninput1">
                          <div class="titleinput"><b>Tên loại:</b></div>
                          <div class="txtinput">
                              <input class="form-control" data-val="true" data-val-required="" id="txtTenDanhMuc" runat="server" name="Content.ContentName" type="text" value="" />
                          </div>
                        </div>
                      </div>
                </div>
            <div class="row">
                <div class="col-sm-9">  <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                   
                </div>
                <div class="col-sm-3">
                   
                        <div style="text-align:right;">
                            <a class="btn btn-primary btn-flat" href="LoaiTinTuc.aspx" >Tất cả</a>
                           <a class="btn btn-primary btn-flat" href="LoaiTinTuc-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>
                        </div>
                    </div>
            </div>
            <div id="dvDanhMucCap1" runat="server">

            </div>
        </div>
    </div>
</section>


    <!-- /.content -->
  </div>
        </form>
</asp:Content>
