﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="DanhSachCTV.aspx.cs" Inherits="Admin_QuanTriBlog_DanhSachCTV" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function DeleteTinTuc(idTinTuc) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "True")
                        window.location.reload();
                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteTinTuc&idTinTuc=" + idTinTuc, true);
            xmlhttp.send();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="title">QUẢN LÝ TIN TỨC</div>
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Từ ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTuNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Đến ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDenNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tiêu đề:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTenDangNhap" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Lĩnh vực:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slLinhVuc" runat="server">
                                        </select>
                                    </div>
                                </div>
                               <%-- <div class="coninput2">
                                    <div class="titleinput"><b>Người đăng:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slNguoiDuyet" runat="server">
                                        </select>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <a class="btn btn-primary btn-flat" href="QuanLyCapNhat-CTV.aspx"><i class="glyphicon glyphicon-plus"></i>Thêm mới</a>
                            </div>
                            <div class="col-sm-3">
                                <div style="text-align: right;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                                </div>
                            </div>
                        </div>
                        <div id="dvTinDang" runat="server">
                        </div>
                    </div>
                </div>
            </section>

            <!--Popup xem chi tiết-->
            <div id="lightXemChiTiet" class="white_content" style="top: 10%; width: 70%; left: 15%; height: 80%;">
                <div class="box">
                    <div class="box-body">

                        <div id="dvXemChiTiet" style="padding: 10px; font-family: time new roman;">
                        </div>
                    </div>
                </div>
            </div>
            <div id="fadeXemChiTiet" onclick="DongXemChiTiet()" class="black_overlay"></div>
            <!--End popup--->
            <!-- /.content -->
            <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
            <script type="text/javascript">
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

            </script>
        </div>
    </form>
</asp:Content>
