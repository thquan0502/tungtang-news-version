﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriBlog_QuanLyCapNhat_CTV : System.Web.UI.Page
{
    string idTinTuc = "";
    string idadmin = "";
    string NguoiDuyet = "";
    protected void FileBrowser1_Load(object sender, EventArgs e)
    {
        FileBrowser1 = new CKFinder.FileBrowser();
        FileBrowser1.BasePath = "/ckfinder/";
        FileBrowser1.SetupCKEditor(txtNoiDung);

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {

            //Đăng tin không cần đăng nhập
            Response.Redirect("../Home/DangNhap.aspx");
        }
        else
        {
            string x = Request.Cookies["AdminTungTang_Login"].Value.Trim().ToString();
            idadmin = StaticData.getField("tb_admin", "idAdmin", "TenDangNhap", x);
            //idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        try
        {
            idTinTuc = Request.QueryString["idTinTuc"].Trim();
        }
        catch { }
        if (!IsPostBack)
        {
            LoadTags();
            LoadLoai();
            LoadTinTuc();
        }
        if (IsPostBack && fileHinhAnh.PostedFile != null)
        {
            if (Request.Files.Count > 10)
            {
                Response.Write("<script>alert('Bạn vui lòng chọn tối đa 10 ảnh!')</script>");
                return;
            }
            for (int j = 0; j < Request.Files.Count; j++)
            {
                HttpPostedFile file = Request.Files[j];
                if (file.ContentLength > 0)
                {
                    //if (fileHinhAnh.PostedFile.FileName.Length > 0)
                    //{
                    string extension = Path.GetExtension(file.FileName);
                    if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                    {
                        if (fileHinhAnh.HasFile)
                        {
                            string Ngay = DateTime.Now.Day.ToString();
                            string Thang = DateTime.Now.Month.ToString();
                            string Nam = DateTime.Now.Year.ToString();
                            string Gio = DateTime.Now.Hour.ToString();
                            string Phut = DateTime.Now.Minute.ToString();
                            string Giay = DateTime.Now.Second.ToString();
                            string Khac = DateTime.Now.Ticks.ToString();
                            string fExtension = Path.GetExtension(file.FileName);

                            string sqlIdTinDang = "select top 1 idTinTuc from tb_TinTuc order by idTinTuc desc";
                            DataTable tbIdTinDang = Connect.GetTable(sqlIdTinDang);
                            string idTinDang = "0";
                            if (tbIdTinDang.Rows.Count > 0)
                                idTinDang = (float.Parse(tbIdTinDang.Rows[0]["idTinTuc"].ToString()) + 1).ToString();
                            string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + idTinDang + fExtension;
                            string FilePath = "/Images/" + FileName;

                            //if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("iPhone")))
                            //    StaticData.LoadWaterMark(file, Server.MapPath(FilePath), "IOS");
                            //else
                            //    StaticData.LoadWaterMark(file, Server.MapPath(FilePath), ""); 
                            //file.SaveAs(Server.MapPath(FilePath));

                            hdHinhAnh.Value = hdHinhAnh.Value + FileName;
                            string htmlHinhAnh = "<div id='dvHinhAnh_" + hdHinhAnh.Value.Trim() + "' class='imgupload' style='width:150px'>";
                            htmlHinhAnh += "<p style='margin:0px'><img src='/Images/news/" + hdHinhAnh.Value.Trim() + "' style='width: 150px;height: 110px;' /></p>";
                            htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + hdHinhAnh.Value.Trim() + "\",\"" + hdHinhAnh.Value.Trim() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                            htmlHinhAnh += "</div>";
                            dvHinhAnh.InnerHtml = htmlHinhAnh;
                            //imgAnhCuaBan.Src = FilePath;
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                        return;
                    }
                }
            }
            //Response.Write("<script>window.scrollTo(0, document.body.scrollHeight);</script>");
        }

    }

    private void LoadLoai()
    {
        string strSql = "select * from tb_LoaiTinTuc";
        slLoaiBlog.DataSource = Connect.GetTable(strSql);
        slLoaiBlog.DataTextField = "Loai";
        slLoaiBlog.DataValueField = "idLoaiTinTuc";
        slLoaiBlog.DataBind();
    }
    private void LoadTags()
    {
        string strSql = "select * from Tags";
        sTags.DataSource = Connect.GetTable(strSql);
        sTags.DataTextField = "Title";
        sTags.DataValueField = "id_tags";
        sTags.DataBind();
    }

    private void LoadTinTuc()
    {
        if (idTinTuc != "")
        {
            btDangTin.Text = "SỬA";
            string sqlTinDang = "select * from tb_TinTuc where idTinTuc='" + idTinTuc + "'";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            if (tbTinDang.Rows.Count > 0)
            {
                txtTieuDe.Value = tbTinDang.Rows[0]["TieuDe"].ToString();
                txtDuongDan.Value = tbTinDang.Rows[0]["DuongDan"].ToString();
                txtTieuDeSeo.Value = tbTinDang.Rows[0]["TieuDeSeo"].ToString();
                txtTieuDeSeo.Disabled = true;
                txtMoTaNgan.Value = tbTinDang.Rows[0]["MoTaNgan"].ToString();
                txtNoiDung.Text = tbTinDang.Rows[0]["MoTa"].ToString();
                if (tbTinDang.Rows[0]["isHot"].ToString() == "" || tbTinDang.Rows[0]["isHot"].ToString() == "False")
                    ckisHot.Checked = false;
                else
                    ckisHot.Checked = true;
                if (tbTinDang.Rows[0]["KichHoat"].ToString() == "" || tbTinDang.Rows[0]["KichHoat"].ToString() == "False")
                    ckKichHoat.Checked = false;
                else
                    ckKichHoat.Checked = true;

				txtKeyword.Value = tbTinDang.Rows[0]["Keyword"].ToString();
                txtMoTa.Value = tbTinDang.Rows[0]["Desciption"].ToString();
                txtTitlte.Value = tbTinDang.Rows[0]["Titlte"].ToString();
                //txtmetaCanonical.Value = tbTinDang.Rows[0]["metaCanonical"].ToString();
                //txtmetarobots.Value = tbTinDang.Rows[0]["metarobots"].ToString();
                //txtmetaOpenGraph.Value = tbTinDang.Rows[0]["metaOpenGraph"].ToString();
                //txtmetatwitter.Value = tbTinDang.Rows[0]["metatwitter"].ToString();

                imgHinhAnh.Src = tbTinDang.Rows[0]["AnhDaiDien"].ToString();


                slLoaiBlog.Value = tbTinDang.Rows[0]["idLoaiTinTuc"].ToString();
                sTags.Value = tbTinDang.Rows[0]["id_tags"].ToString();
                //string htmlHinhAnh = "";
                //htmlHinhAnh += "<div id='dvHinhAnh_" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "' class='imgupload'>";
                //htmlHinhAnh += "<p style='margin:0px'><img src='/Images/news/" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "' style='width: 150px;height: 110px;' /></p>";
                //htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "\",\"" + tbTinDang.Rows[0]["AnhDaiDien"].ToString() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                //htmlHinhAnh += "</div>";
                //dvHinhAnh.InnerHtml = htmlHinhAnh;
            }
        }
    }
    protected void btDangTin_Click(object sender, EventArgs e)
    {
        string TieuDe = "";
        string DuongDan = "";
        string TieuDeSeo = "";
        string MoTaNgan = "";
        string NoiDung = "";
		string Keyword = "";
        string isHot = "False";
        string KichHoat = "False";
        string HinhAnh = hdHinhAnh.Value.Trim();
        HinhAnh = "";

        if (fileHinhAnh.HasFile)
        {
            string Ngay = DateTime.Now.Day.ToString();
            string Thang = DateTime.Now.Month.ToString();
            string Nam = DateTime.Now.Year.ToString();
            string Gio = DateTime.Now.Hour.ToString();
            string Phut = DateTime.Now.Minute.ToString();
            string Giay = DateTime.Now.Second.ToString();
            string Khac = DateTime.Now.Ticks.ToString();
            string fExtension = Path.GetExtension(fileHinhAnh.FileName);

            string sqlIdTinDang = "select top 1 idTinTuc from tb_TinTuc order by idTinTuc desc";
            DataTable tbIdTinDang = Connect.GetTable(sqlIdTinDang);
            string idTinDang = "0";
            if (tbIdTinDang.Rows.Count > 0)
                idTinDang = (float.Parse(tbIdTinDang.Rows[0]["idTinTuc"].ToString()) + 1).ToString();
            string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + idTinDang + fExtension;
            string FilePath = "/images/" + FileName;

            //if ((Request.ServerVariables["HTTP_USER_AGENT"].Contains("iPhone")))
            //    StaticData.LoadWaterMark(file, Server.MapPath(FilePath), "IOS");
            //else
            //    StaticData.LoadWaterMark(file, Server.MapPath(FilePath), ""); 
            //file.SaveAs(Server.MapPath(FilePath));
            fileHinhAnh.SaveAs(Server.MapPath(FilePath));
            hdHinhAnh.Value = hdHinhAnh.Value + FileName;
            HinhAnh = FilePath;
            //string htmlHinhAnh = "<div id='dvHinhAnh_" + hdHinhAnh.Value.Trim() + "' class='imgupload' style='width:150px'>";
            //htmlHinhAnh += "<p style='margin:0px'><img src='/Images/news/" + hdHinhAnh.Value.Trim() + "' style='width: 150px;height: 110px;' /></p>";
            //htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + hdHinhAnh.Value.Trim() + "\",\"" + hdHinhAnh.Value.Trim() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
            //htmlHinhAnh += "</div>";
            //dvHinhAnh.InnerHtml = htmlHinhAnh;
            //imgAnhCuaBan.Src = FilePath;
        }


        //Tiêu đề
        if (txtTieuDe.Value.Trim() != "")
        {
            TieuDe = txtTieuDe.Value.Trim();
            dvTieuDe.InnerHtml = "";
        }
        else
        {
            Response.Write("<script>alert('Bạn chưa nhập tiêu đề!');</script>");
            dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề!";
            return;
        }
		 if (txtKeyword.Value.Trim() != "")
        {
            Keyword = txtKeyword.Value.Trim();
            dvTuKhoa.InnerHtml = "";
        }
        else
        {
            Response.Write("<script>alert('Bạn chưa nhập từ khóa!');</script>");
            dvTuKhoa.InnerHtml = "Bạn chưa nhập từ khóa!";
            return;
        }
        if (idTinTuc != "" && txtDuongDan.Value.Trim() != "")
        {
            DuongDan = txtDuongDan.Value.Trim();
        }
        else
        {
            DuongDan = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TieuDe.Trim()));
        }
        //Tiêu đề seo
        //if (txtTieuDeSeo.Value.Trim() != "")
        //{
        //    TieuDeSeo = txtTieuDeSeo.Value.Trim();
        //    dvTieuDeSeo.InnerHtml = "";
        //}
        //else
        //{
        //    Response.Write("<script>alert('Bạn chưa nhập tiêu đề seo!');</script>");
        //    dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề seo!";
        //    return;
        //}



        string Titlte = txtTitlte.Value.Trim();
        string Desciption = txtMoTa.Value.Trim();
        //string metacanonical = txtmetacanonical.value.trim();
        //string metarobots = txtmetarobots.value.trim();
        //string metaopengraph = txtmetaopengraph.value.trim();
        //string metatwitter = txtmetatwitter.value.trim();
        string idLoaiTinTuc = slLoaiBlog.Value.Trim();
        if (idLoaiTinTuc == "")
        {
            idLoaiTinTuc = "NULL";
        }
        else
        {
            idLoaiTinTuc = "'" + idLoaiTinTuc + "'";
        }

        string idTags = sTags.Value.Trim();
        if (idTags == "")
        {
            idTags = "NULL";
        }
        else
        {
            idTags = "'" + idTags + "'";
        }

        MoTaNgan = txtMoTaNgan.Value.Trim();
        if (ckisHot.Checked)
            isHot = "True";
        if (ckKichHoat.Checked)
            KichHoat = "True";
        //Nội dung
        if (txtNoiDung.Text.Trim() != "")
        {
            NoiDung = txtNoiDung.Text.Trim();
            dvNoiDung.InnerHtml = "";
        }
        else
        {
            Response.Write("<script>alert('Bạn chưa nhập nội dung!');</script>");
            dvNoiDung.InnerHtml = "Bạn chưa nhập nội dung!";
            return;
        }
        if (idTinTuc == "")
        {
            //Insert tin đăng
            string sqlInsertTD = "insert into tb_TinTuc(TieuDe,TieuDeSeo,MoTaNgan,MoTa,AnhDaiDien,isHot,KichHoat,NgayDang,[Titlte],[Desciption],[idLoaiTinTuc],[id_tags],DuongDan,[idAdmin],Keyword)";
            sqlInsertTD += " values(N'" + TieuDe + "'";
            sqlInsertTD += @",N'" + TieuDeSeo + "',N'" + MoTaNgan + "',N'" + NoiDung + "','" + HinhAnh + "','" + isHot + "','" + KichHoat + "','" + DateTime.Now + "' ";
            sqlInsertTD += @",N'" + Titlte + @"',N'" + Desciption + @"'," + idLoaiTinTuc + @"," + idTags + @",N'" + DuongDan + "'," + idadmin + @",N'" + Keyword + "'";
            sqlInsertTD += ")";
            bool ktInsertTD = Connect.Exec(sqlInsertTD);
            if (ktInsertTD)
            {
                Response.Redirect("DanhSachCTV.aspx");
            }
            else
            {
                Response.Write("<script>console.log(`" + sqlInsertTD + "`);</script>");
            }
        }
        else
        {
            //Sửa tin đăng
            string sqlUpdateTD = "update tb_TinTuc set TieuDe=N'" + TieuDe + "'";
            sqlUpdateTD += ",DuongDan=N'" + DuongDan + "'";
            sqlUpdateTD += ",TieuDeSeo=N'" + TieuDeSeo + "'";
            sqlUpdateTD += ",MoTaNgan=N'" + MoTaNgan + "'";
            sqlUpdateTD += ",KichHoat='" + KichHoat + "'";
           // sqlUpdateTD += ",NgayDang='" + DateTime.Now + "'";
            sqlUpdateTD += ",MoTa=N'" + NoiDung + "'";
            sqlUpdateTD += ",idAdmin='" + idadmin + "'";
            sqlUpdateTD += ",isHot='" + isHot + "'";
            if (HinhAnh != "")
                sqlUpdateTD += ",AnhDaiDien='" + HinhAnh + "'";


            sqlUpdateTD += @",[Titlte]=N'" + Titlte + @"'
              ,[Desciption]=N'" + Desciption + @"'
				,[Keyword]=N'" + Keyword + @"'
             ,idLoaiTinTuc =" + idLoaiTinTuc + "";
            sqlUpdateTD += ",id_tags=" + idTags + "";
            sqlUpdateTD += " where idTinTuc='" + idTinTuc + "'";
            bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
            if (ktUpdateTD)
            {
                Response.Redirect("DanhSachCTV.aspx");
            }
            else
            {
                Response.Write("<script>alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!');</script>");
            }
        }
    }
}