﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Layout/adMasterPage.master" CodeFile="QuanLyCapNhat-CTV.aspx.cs" Inherits="Admin_QuanTriBlog_QuanLyCapNhat_CTV" %>

<%@ Register Namespace="CKEditor.NET" Assembly="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Namespace="CKFinder" Assembly="CKFinder" TagPrefix="CKFinder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        window.onload = function () {
            if (document.getElementById('ContentPlaceHolder1_hdHinhAnh').value != "")
                window.scrollTo(0, document.body.scrollHeight);
            document.getElementById("search-widget-wrapper").style.display = "none";
        }
        $(function () {
            CKEDITOR.config.extraPlugins = 'justify';
        });
        function format_curency(a) {
            var money = a.value.replace(/\./g, "");
            a.value = money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
        function XoaHinhAnh(id, TenAnh) {
            document.getElementById(id).style.display = "none";
            document.getElementById("ContentPlaceHolder1_hdHinhAnh").value = document.getElementById("ContentPlaceHolder1_hdHinhAnh").value.replace(TenAnh + "|~~~~|", "");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content-wrapper">
    <!-- Main content -->
        <div class="title" id="dvTitle" runat="server">SỬA TIN TỨC</div>
    <div class="title1"><a href="DanhSachCTV.aspx"><i class="fa fa-step-backward"></i> Danh sách tin tức</a></div>
    <section class="content">
        <div class="box">
      <div class="row">
         <!--Top Jobs-->
         <div class="col-md-12">
            <div class="top-job">
               <div class="panel jobs-board-listing with-mc no-padding no-border">
                  <div class="panel-content" id="tabChoThue" style="padding:10px">
                      <div class="job-list scrollbar m-t-lg">
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12">
                                                             Tiêu đề <span style="color:red">(*)</span>:
                                                         </td>
                                                         <td>
                                                             <input type="text" id="txtTieuDe" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                             <div id="dvTieuDe" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                           <div class="col-md-12">
                              <div class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12">
                                                             Loại :
                                                         </td>
                                                         <td>
                                                             <select  id="slLoaiBlog"  class="form-control" runat="server" ></select> 
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                              
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12">
                                                             Tags :
                                                         </td>
                                                         <td>
                                                             <select  id="sTags"  class="form-control" runat="server" ></select> 
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                              
                          </div>
                          <div class="col-md-12" >
                              <div class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12">
                                                             Tiêu đề seo <span style="color:red">(*)</span>:
                                                         </td>
                                                         <td>
                                                             <input type="text" id="txtTieuDeSeo" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" readonly />
                                                             <div id="dvTieuDeSeo" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12">
                                                             Mô tả ngắn :
                                                         </td>
                                                         <td>
                                                             <textarea id="txtMoTaNgan" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                             <div id="dvMoTaNgan" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                             Nội dung:
                                                         </td>
                                                         <td>
                                                             <CKEditor:CKEditorControl ID="txtNoiDung" Height="300" runat="server"></CKEditor:CKEditorControl> 
                                                             <CKFinder:FileBrowser ID="FileBrowser1"   Width="0" Height="0" runat="server" OnLoad="FileBrowser1_Load"></CKFinder:FileBrowser>
                                                             <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                             Title tag
                                                         </td>
                                                         <td>
                                                             <input type="text" id="txtTitlte" runat="server" class="form-control"/>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>

                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                             Desciption tag
                                                         </td>
                                                         <td>
                                                             <input type="text" id="txtMoTa" runat="server" class="form-control"/>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
						      <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                               Từ khóa <span style="color:red">(*)</span>:
                                                         </td>
                                                         <td>
                                                             <input type="text" id="txtKeyword" runat="server" class="form-control"/>
                                                               <div id="dvTuKhoa" style="color: red;" runat="server"></div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                             Duong dan
                                                         </td>
                                                         <td>
                                                             <input type="text" id="txtDuongDan" runat="server" class="form-control" readonly />
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                         <div class="col-md-12" >
                              <div id="" class="form-group">
                                             <div>
                                                  <table style="width: 100%;">
                                                      <tr>
                                                      <td>
                                                          <a onclick="fileuploadclick();" class="btn btn-primary btn-block" style="width: 10%;">Chọn hình</a>
                                                      </td>
                                                        </tr>
                                                      <tr>
                                                          <td>
                                                          <img  id="imgHinhAnh" runat="server" style="width:140px;height:140px;" />
                                                      </td>
                                                      </tr>
                                                  </table>
                                              </div>
                                  </div>
                               </div>
                          <asp:FileUpload ID="fileHinhAnh"
                                                                         ClientIDMode="Static" onchange="UploadHinhAnh_Onchange(this,'imgHinhAnh');"
                                                                        accept=".png,.jpg,.jpeg,.gif"
                                                                         runat="server" style="display:none;" />
                          <div class="col-md-12" style="display:none;">
                              <div id="dvConHinhAnh" class="form-group">
                                             <div>
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="display:none;">
                                                             Thêm hình ảnh:
                                                         </td>
                                                         <td>
                                                             <div class="BgUploadAnh">
                                                                <div>
                                                                    
                                                                </div>
                                                                <div style="display:none;padding:10px 0px;height: auto;overflow: hidden;display: -webkit-inline-box;" id="dvHinhAnh" runat="server">
                                                                    <%--<div class="imgupload">
                                                                        <p style="margin:0px"><img id="imgAnhCuaBan" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                                        <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                                                    </div>
                                                                    <div class="imgupload">
                                                                        <p style="margin:0px"><img id="img1" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                                        <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                                                    </div>--%>
                                                                </div>
                                                            </div>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                             isHot
                                                         </td>
                                                         <td>
                                                             <input type="checkbox" id="ckisHot" runat="server"/>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                             <div style="height:auto">
                                                 <table style="width: 100%;">
                                                     <tr>
                                                         <td class="nametdadmin12" style="vertical-align: baseline;">
                                                             Kích hoạt
                                                         </td>
                                                         <td>
                                                             <input type="checkbox" id="ckKichHoat" runat="server"/>
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                         </div>
                          </div>
                          <!-- Buttons-->
                          <div class="form-group">
                                             <div style="text-align:center">
                                                <asp:Button ID="btDangTin" runat="server" Text="LƯU TIN" class="btn btn-primary btn-block" Style="width: 110px;padding: 10px;margin: auto;" OnClick="btDangTin_Click" />
                                             </div>
                                         </div>\
                      </div>
                  </div>
                   
               </div>
            </div>
         </div>
      </div>
        <input type="hidden" id="hdHinhAnh" runat="server" />
            </div>
   </section>
        </div>
    </form>

     <script>
        function fileuploadclick()
        {
            document.getElementById('fileHinhAnh').click();
        }
        function UploadHinhAnh_Onchange(input, iddd) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_' + iddd)
                        .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</asp:Content>

