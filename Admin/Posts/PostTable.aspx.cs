﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections;
using System.Web.Caching;

public partial class Admin_QuanTriTinDang_QuanLyTinDang : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sThoiHan = "";
    string sTinhTrang = "";
    string sTenDangNhap = "";
    string sisHot = "";
    string sHoaHong = "";
    string LinhVuc = "";
    string LinhVucC2 = "";
    string City = "";
    string District = "";
    string Ward = "";
    string NguoiDuyet = "";
    string MT = "";
    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;



    protected string templateFilterDMC1 = "";
    protected string filterDMC1 = null;

    protected string templateFilterDMC2 = "";
    protected string filterDMC2 = null;

    protected string templateJsonCities = "";
    protected string templateJsonDistricts = "";
    protected string templateJsonWards = "";

    protected string templateFilterCity = ""; // gia tri mac dinh can truyen cho client
    protected string filterCity = null; // gia tri mà request gui len

    protected string templateFilterDistrict = "";
    protected string filterDistrict = null;

    protected string templateFilterWard = "";
    protected string filterWard = null;


    protected string templateFilterPostPrice = "";
    protected string filterPostPrice = null;
    protected double filterFromPostPrice = 0;
    protected double filterToPostPrice = 0;

    protected string templateFilterPostCommission = "";
    protected string filterPostCommission = null;
    protected double filterFromPostCommission = 0;
    protected double filterToPostCommission = 0;

    string slistLabelhh = "";
    string srulerhh = "";

    string slistLabelchh = "";
    string srulerchh = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {
            Response.Redirect("../Home/DangNhap.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }
        if (!IsPostBack)
        {
            this.loadMaTin("select 'TT' +(CAST(idTinDang as varchar(100)) + ' - ' + Code) as IdTD,Code from tb_TinDang order by NgayDang desc", "Code", "Code", true, "-- Tất cả --", slMaTin);
            // this.loadTenDangNhap("select 'TT' +(CAST(idThanhVien as varchar(100)) + ' - ' + TenDangNhap + ' - ' + TenCuaHang) as IdTV,TenDangNhap from tb_ThanhVien where TenCuaHang != '' order by NgayDangKy desc", "IdTV", "TenDangNhap", true, "-- Tất cả --", slTenDangNhap);
            // this.loadLinhVuc();
            //this.loadKhuVuc();
            this.initCollabDropdownList();
            this.initSearchValues();
            loadNguoiDuyet();
            try
            {
                if (Request.QueryString["ND"].Trim() != "")
                {
                    NguoiDuyet = Request.QueryString["ND"].Trim();
                    slNguoiDuyet.Value = NguoiDuyet;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["HoaHong"].Trim() != "")
                {
                    slistLabelhh = Request.QueryString["HoaHong"].Trim();
                    txtRangeLuotXemhh.Value = slistLabelhh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterCommission"].Trim() != "")
                {
                    srulerhh = Request.QueryString["RangeCounterCommission"].Trim();
                    rulerhh.InnerHtml = srulerhh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["Price"].Trim() != "")
                {
                    slistLabelchh = Request.QueryString["Price"].Trim();
                    txtRangeLuotXemchh.Value = slistLabelchh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterFee"].Trim() != "")
                {
                    srulerchh = Request.QueryString["RangeCounterFee"].Trim();
                    rulerchh.InnerHtml = srulerchh;
                }
            }
            catch { }

            this.loadPost();
        }
        this.LoadLiDo();
    }

    protected void initCollabDropdownList()
    {
        string sql1 = @"
           SELECT 
	            TBOwners.idThanhVien AS OwnerId,
	            TBOwners.Code AS OwnerCode,
	            TBOwners.TenCuaHang AS OwnerFullname,
                TBOwners.SoDienThoai AS OwnerPhone, 
				COUNT(TBPost.idTinDang)
            FROM tb_ThanhVien TBOwners , tb_TinDang TBPost
			WHERE TBOwners.idThanhVien = TBPost.idThanhVien 
			GROUP BY TBOwners.idThanhVien,
	            TBOwners.Code,
	            TBOwners.TenCuaHang,
                TBOwners.SoDienThoai,
				NgayDangKy
            ORDER BY TBOwners.NgayDangKy DESC
        ";
        DataTable table1 = Connect.GetTable(sql1);

        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Value", typeof(String)));
        dt.Columns.Add(new DataColumn("Text", typeof(String)));

        DataRow dr0 = dt.NewRow();
        dr0[0] = "";
        dr0[1] = "---Chọn---";
        dt.Rows.Add(dr0);

        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            DataRow dr1 = dt.NewRow();
            dr1[0] = table1.Rows[i]["OwnerId"].ToString();
            dr1[1] = (table1.Rows[i]["OwnerCode"].ToString() != "" ? (table1.Rows[i]["OwnerCode"].ToString()) + " - " + (table1.Rows[i]["OwnerPhone"].ToString() + " - ") : "") + table1.Rows[i]["OwnerFullname"].ToString();
            dt.Rows.Add(dr1);
        }

        DataView dv = new DataView(dt);
        drdlCollab.DataSource = dv;
        drdlCollab.DataTextField = "Text";
        drdlCollab.DataValueField = "Value";
        drdlCollab.DataBind();
    }

    #region paging
    private void loadPagination()
    {
        string filterRangeCommissionCounter = null;
        int filterRangeFromCommissionCounter = -1, filterRangeToCommissionCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterCommission")))
        {
            filterRangeCommissionCounter = Request.QueryString.Get("RangeCounterCommission").Trim();
            string[] filterRangeCommissionCounterVals = { };
            filterRangeCommissionCounterVals = filterRangeCommissionCounter.Split(';');
            if (filterRangeCommissionCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeCommissionCounterVals[0], out filterRangeFromCommissionCounter);
                Int32.TryParse(filterRangeCommissionCounterVals[1], out filterRangeToCommissionCounter);
                txtCommissionCounterRange.Text = filterRangeCommissionCounter;
            }
        }

        string filterRangeFeeCounter = null;
        int filterRangeFromFeeCounter = -1, filterRangeToFeeCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterFee")))
        {
            filterRangeFeeCounter = Request.QueryString.Get("RangeCounterFee").Trim();
            string[] filterRangeFeeCounterVals = { };
            filterRangeFeeCounterVals = filterRangeFeeCounter.Split(';');
            if (filterRangeFeeCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeFeeCounterVals[0], out filterRangeFromFeeCounter);
                Int32.TryParse(filterRangeFeeCounterVals[1], out filterRangeToFeeCounter);
                txtFeeCounterRange.Text = filterRangeFeeCounter;
            }
        }

        string sql = "select count(idTinDang) from tb_TinDang td left join tb_ThanhVien tv on td.idThanhVien=tv.idThanhVien where '1'='1'";
        if (sTenDangNhap != "")
            sql += " and td.idThanhVien = '" + sTenDangNhap + "'";
        if (MT != "")
            sql += " and td.Code like '%" + MT + "%'";
        if (NguoiDuyet != "")
            sql += " and td.idAdmin_Duyet = '" + NguoiDuyet + "'";
        if (sisHot != "")
        {
            if (sisHot == "False")
                sql += " and (isHot = '" + sisHot + "' or isHot is Null)";
            else
                sql += " and isHot = '" + sisHot + "'";
        }

        if (filterDMC1 != "")
            sql += " and idDanhMucCap1= '" + filterDMC1 + "'";
        if (filterDMC2 != "")
            sql += " and idDanhMucCap2= '" + filterDMC2 + "'";
        if (filterCity != "")
            sql += " and td.idTinh= '" + filterCity + "'";
        if (filterDistrict != "")
            sql += " and td.idHuyen= '" + filterDistrict + "'";
        if (filterWard != "")
            sql += " and td.idPhuongXa= '" + filterWard + "'";
        if (sTuNgay != "")
            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (slistLabelhh == "10100")
            sql += " and (td.Commission between 10000000 and 100000000)";
        if (slistLabelhh == "100500")
            sql += " and (td.Commission between 100000000 and 500000000)";
        if (slistLabelhh == "500")
            sql += " and (td.Commission >= 500000000)";
        if (slistLabelhh == "10100500")
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission >= 500000000)";
        if (slistLabelhh == "100500500")
            sql += " and (td.Commission between 100000000 and 500000000 or td.Commission >= 500000000)";
        if (slistLabelhh == "10100100500")
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000)";
        if (slistLabelhh == "10100100500500")
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000 or td.Commission >= 500000000)";
        //Lọc Giá
        if (slistLabelchh == "10100")
            sql += " and (td.TuGia between 10000000 and 100000000)";
        if (slistLabelchh == "100500")
            sql += " and (td.TuGia between 100000000 and 500000000)";
        if (slistLabelchh == "500")
            sql += " and (td.TuGia >= 500000000)";
        if (slistLabelchh == "10100500")
            sql += " and (td.TuGia between 10000000 and 100000000 or td.TuGia >= 500000000)";
        if (slistLabelchh == "100500500")
            sql += " and (td.TuGia between 100000000 and 500000000 or td.TuGia >= 500000000)";
        if (slistLabelchh == "10100100500")
            sql += " and (td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000)";
        if (slistLabelchh == "10100100500500")
            sql += " and (td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000 or td.TuGia >= 500000000)";

        //Lọc Hoa hồng với thước
        if (srulerhh != "" && slistLabelhh == "0")
        {
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " and td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission  <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100")
        {
            sql += " and ( td.Commission between 10000000 and 100000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500")
        {
            sql += " and ( td.Commission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + "  ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "500")
        {
            sql += " and ( td.Commission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100500")
        {
            sql += " and ( td.Commission between 10000000 and 100000000 or td.Commission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500500")
        {
            sql += " and ( td.Commission between 100000000 and 500000000 or td.Commission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500")
        {
            sql += " and ( td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500500")
        {
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000 or td.Commission >= 500000000 ";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        //Lọc Giá với thước       
        if (srulerchh != "" && slistLabelchh == "0")
        {
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " and td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500")
        {
            sql += " and ( td.TuGia between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "500")
        {
            sql += " and ( td.TuGia >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100500")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000 or td.TuGia >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500500")
        {
            sql += " and ( td.TuGia between 100000000 and 500000000 or td.TuGia >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500500")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000 or td.TuGia>= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (sThoiHan != "")
        {
            if (sThoiHan == "2")
                sql += " and td.isHetHan is Null";
            else
                sql += " and td.isHetHan = '" + sThoiHan + "'";
        }
        if (sTinhTrang != "")
        {
            if (sTinhTrang == "2")
                sql += " and td.isDuyet is Null";
            else
                sql += " and td.isDuyet = '" + sTinhTrang + "'";
        }
        if (sHoaHong != "")
        {
            if (sHoaHong == "2")
                sql += " and Commission = 0";
            else
                sql += " and Commission !=0";
        }
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    this.loadPagination();
                }
            }
        }
    }


    private void loadMaTin(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        //string sql = "SELECT * FROM tb_TinDang WHERE NgayDang >= DATEADD(MONTH, -6, GETDATE()) AND isDuyet = 1 order by NgayDang desc";
        DataTable table = Connect.GetTable(sql);
        slMaTin.DataSource = table;
        slMaTin.DataTextField = TextField;
        slMaTin.DataValueField = ValueField;
        slMaTin.DataBind();
        if (AddANewItem)
        {
            slMaTin.Items.Insert(0, (new ListItem(ItemName, "")));
            slMaTin.Items.FindByText(ItemName).Selected = true;
        }
    }
    //private void loadTenDangNhap(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    //{
    //    //string sql = "SELECT DISTINCT tv.idThanhVien,tv.TenDangNhap FROM tb_ThanhVien tv, tb_TinDang td where tv.idThanhVien = td.idThanhVien AND td.isDuyet = 1";
    //    DataTable table = Connect.GetTable(sql);
    //    slTenDangNhap.DataSource = table;
    //    slTenDangNhap.DataTextField = TextField;
    //    slTenDangNhap.DataValueField = ValueField;
    //    slTenDangNhap.DataBind();
    //    if (AddANewItem)
    //    {
    //        slTenDangNhap.Items.Insert(0, (new ListItem(ItemName, "")));
    //        slTenDangNhap.Items.FindByText(ItemName).Selected = true;
    //    }
    //}
    #endregion
    //private void loadLinhVuc()
    //{
    //    string sql = "select * from tb_DanhMucCap1 order by sothutu";
    //    DataTable table = Connect.GetTable(sql);
    //    slLinhVuc.DataSource = table;
    //    slLinhVuc.DataTextField = "TenDanhMucCap1";
    //    slLinhVuc.DataValueField = "idDanhMucCap1";
    //    slLinhVuc.DataBind();

    //    slLinhVuc.Items.Add(new ListItem("-- Chọn --", ""));
    //    slLinhVuc.Items.FindByText("-- Chọn --").Selected = true;
    //}
    //private void loadKhuVuc()
    //{
    //    string sql = "select * from City order by id";
    //    DataTable table = Connect.GetTable(sql);
    //    slKhuvuc.DataSource = table;
    //    slKhuvuc.DataTextField = "Ten";
    //    slKhuvuc.DataValueField = "id";
    //    slKhuvuc.DataBind();

    //    slKhuvuc.Items.Add(new ListItem("-- Chọn --", ""));
    //    slKhuvuc.Items.FindByText("-- Chọn --").Selected = true;
    //}
    private void loadNguoiDuyet()
    {
        string sql = "select * from tb_Admin where '1'='1' order by HoTen";
        DataTable table = Connect.GetTable(sql);
        slNguoiDuyet.DataSource = table;
        slNguoiDuyet.DataTextField = "TenDangNhap";
        slNguoiDuyet.DataValueField = "idAdmin";
        slNguoiDuyet.DataBind(); 
        slNguoiDuyet.Items.Add(new ListItem("-- Tất cả --", ""));
        slNguoiDuyet.Items.FindByText("-- Tất cả --").Selected = true;
    }

    #region paging	
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;	
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void loadPost()
    {


        string filterRangeCommissionCounter = null;
        int filterRangeFromCommissionCounter = -1, filterRangeToCommissionCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterCommission")))
        {
            filterRangeCommissionCounter = Request.QueryString.Get("RangeCounterCommission").Trim();
            string[] filterRangeCommissionCounterVals = { };
            filterRangeCommissionCounterVals = filterRangeCommissionCounter.Split(';');
            if (filterRangeCommissionCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeCommissionCounterVals[0], out filterRangeFromCommissionCounter);
                Int32.TryParse(filterRangeCommissionCounterVals[1], out filterRangeToCommissionCounter);
                txtCommissionCounterRange.Text = filterRangeCommissionCounter;
            }
        }

        string filterRangeFeeCounter = null;
        int filterRangeFromFeeCounter = -1, filterRangeToFeeCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterFee")))
        {
            filterRangeFeeCounter = Request.QueryString.Get("RangeCounterFee").Trim();
            string[] filterRangeFeeCounterVals = { };
            filterRangeFeeCounterVals = filterRangeFeeCounter.Split(';');
            if (filterRangeFeeCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeFeeCounterVals[0], out filterRangeFromFeeCounter);
                Int32.TryParse(filterRangeFeeCounterVals[1], out filterRangeToFeeCounter);
                txtFeeCounterRange.Text = filterRangeFeeCounter;
            }
        }

        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();
        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY NgayGuiDuyet desc
                  )AS RowNumber 
	              ,td.*,
                   ISNULL(tv.Email, '') AS TVEmail,
                   ISNULL(tv.SoCMND, '') AS TVCMND,
                   ISNULL(tv.DiaChi, '') AS Address,
                   ISNULL(td.ReferralRate, 0) as ReferralRate2,
                   ISNULL(td.FeeCode, '') AS FeeCodeS,
                   ISNULL(tv.LinkAnh, '') AS AnhDaiDien,
                   ISNULL(tv.SoDienThoai, '') AS SDT,
                   ISNULL(tv.StsPro, '') AS StsPro,
                   ISNULL(TPCity.Ten, '') AS CityName,
	               ISNULL(TPDistrict.Ten, '') AS DistrictName,
				   ISNULL(TPPhuongXa.Ten, '') AS WardName,
                   ISNULL(ct.Ten, '') AS City,
	               ISNULL(dt.Ten, '') AS District,
				   ISNULL(px.Ten, '') AS Ward,
					td.TuGia as GiaTien,
                    tv.NgayDangKy as NgayDangKy,
                    tv.MST as MST,
                    tv.NgayCap as NgayCap,
                    tv.NoiCap as NoiCap,
                    tv.TenCuaHang as TenThanhVien,
			        ISNULL(td.ReferralAmount, 0) as ReferralAmount2,
			   ISNULL(td.Commission, 0) as Commission2,
                 tv.TenDangNhap as TenDangNhap,  
                (
					SELECT count(*)
					FROM Referrals
					WHERE Referrals.TinDangId = td.idTinDang
				) as SelledDetails
                  FROM tb_TinDang td 
                  left join tb_ThanhVien tv on td.idThanhVien=tv.idThanhVien 
                  left join City TPCity on td.idTinh = TPCity.id
				  left join District TPDistrict on td.idHuyen = TPDistrict.id
				  left join tb_PhuongXa TPPhuongXa  on td.idPhuongXa = TPPhuongXa.id
                  left join City ct on tv.idTinh = ct.id
				  left join District dt on tv.idHuyen = dt.id
				  left join tb_PhuongXa px on tv.idPhuongXa = px.id

                  where '1' = '1'
            ";
        if (sTenDangNhap != "")
            sql += " and td.idThanhVien = '" + sTenDangNhap + "'";
        if (MT != "")
            sql += " and td.Code like '%" + MT + "%'";
        if (NguoiDuyet != "")
            sql += " and td.idAdmin_Duyet = '" + NguoiDuyet + "'";
        if (sisHot != "")
        {
            if (sisHot == "False")
                sql += " and (isHot = '" + sisHot + "' or isHot is Null)";
            else
                sql += " and isHot = '" + sisHot + "'";
        }
        if (sTuNgay != "")
            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (!string.IsNullOrWhiteSpace(filterDMC1))
        {
            sql += "       AND idDanhMucCap1 = '" + filterDMC1 + "' ";
            paginationParams.Add("LinhVuc", filterDMC1);
        }
        if (!string.IsNullOrWhiteSpace(filterDMC2))
        {
            sql += "       AND idDanhMucCap2 = '" + filterDMC2 + "' ";
            paginationParams.Add("LinhVucC2", filterDMC2);
        }
        if (!string.IsNullOrWhiteSpace(filterCity))
        {
            sql += "       AND td.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict))
        {
            sql += "       AND td.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard))
        {
            sql += "       AND td.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        if (slistLabelhh == "10100")
            sql += " and (td.Commission between 10000000 and 100000000)";
        if (slistLabelhh == "100500")
            sql += " and (td.Commission between 100000000 and 500000000)";
        if (slistLabelhh == "500")
            sql += " and (td.Commission >= 500000000)";
        if (slistLabelhh == "10100500")
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission >= 500000000)";
        if (slistLabelhh == "100500500")
            sql += " and (td.Commission between 100000000 and 500000000 or td.Commission >= 500000000)";
        if (slistLabelhh == "10100100500")
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000)";
        if (slistLabelhh == "10100100500500")
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000 or td.Commission >= 500000000)";
        //Lọc Giá
        if (slistLabelchh == "10100")
            sql += " and (td.TuGia between 10000000 and 100000000)";
        if (slistLabelchh == "100500")
            sql += " and (td.TuGia between 100000000 and 500000000)";
        if (slistLabelchh == "500")
            sql += " and (td.TuGia >= 500000000)";
        if (slistLabelchh == "10100500")
            sql += " and (td.TuGia between 10000000 and 100000000 or td.TuGia >= 500000000)";
        if (slistLabelchh == "100500500")
            sql += " and (td.TuGia between 100000000 and 500000000 or td.TuGia >= 500000000)";
        if (slistLabelchh == "10100100500")
            sql += " and (td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000)";
        if (slistLabelchh == "10100100500500")
            sql += " and (td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000 or td.TuGia >= 500000000)";

        //Lọc Hoa hồng với thước
        if (srulerhh != "" && slistLabelhh == "0")
        {
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " and td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission  <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100")
        {
            sql += " and ( td.Commission between 10000000 and 100000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500")
        {
            sql += " and ( td.Commission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + "  ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "500")
        {
            sql += " and ( td.Commission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100500")
        {
            sql += " and ( td.Commission between 10000000 and 100000000 or td.Commission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500500")
        {
            sql += " and ( td.Commission between 100000000 and 500000000 or td.Commission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500")
        {
            sql += " and ( td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500500")
        {
            sql += " and (td.Commission between 10000000 and 100000000 or td.Commission between 100000000 and 500000000 or td.Commission >= 500000000 ";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or td.Commission >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and td.Commission <= " + filterRangeToCommissionCounter + " ) ";
            }
        }
        //Lọc Giá với thước       
        if (srulerchh != "" && slistLabelchh == "0")
        {
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " and td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500")
        {
            sql += " and ( td.TuGia between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "500")
        {
            sql += " and ( td.TuGia >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100500")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000 or td.TuGia >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500500")
        {
            sql += " and ( td.TuGia between 100000000 and 500000000 or td.TuGia >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500500")
        {
            sql += " and ( td.TuGia between 10000000 and 100000000 or td.TuGia between 100000000 and 500000000 or td.TuGia>= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or td.TuGia >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and td.TuGia <= " + filterRangeToFeeCounter + " ) ";
            }
        }
        if (sThoiHan != "")
        {
            if (sThoiHan == "2")
                sql += " and td.isHetHan is Null";
            else
                sql += " and td.isHetHan = '" + sThoiHan + "'";
        }
        if (sTinhTrang != "")
        {
            if (sTinhTrang == "2")
                sql += " and td.isDuyet is Null";
            else
                sql += " and td.isDuyet = '" + sTinhTrang + "'";
        }
        if (sHoaHong != "")
        {
            if (sHoaHong == "2")
                sql += " and Commission = 0";
            else
                sql += " and Commission !=0";
        }

         sql += ") as tb1";
        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += " WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";

        DataTable table = Connect.GetTable(sql);
		txtTongTinDang.Value = AllRowNumber.ToString();
        // string sqlLoadTT = "select City.Ten as CityName, District.Ten as DistrictName, tb_PhuongXa.Ten as WardName from tb_ThanhVien tv left join City on tv.idTinh = City.id ";
        //        sqlLoadTT += " left join District on tv.idHuyen = District.id ";
        //        sqlLoadTT += " left join tb_PhuongXa on tv.idPhuongXa = tb_PhuongXa.id ";
        //DataTable tableTT = Connect.GetTable(sqlLoadTT);
        //this.loadPagination();
        SetPage(AllRowNumber);
        string html = @"<table class='table table-bordered table-striped' id='myTable' style='width: 1300px'>
                            <tr>
                                <th class='th' colspan='17' style='text-align: center;'>Thông tin chi tiết tin đăng</th>
                                <th class='th' colspan='7'  style='text-align: center;'>Admin</th>
                            </tr>
                            <tr>
                                <th class='th' style='white-space: nowrap;'>STT</th>
                                <th class='th' >Lên TOP</th>
                                <th class='th' >Ngày đăng</th>
                                <th class='th' >Ngày gửi duyệt</th>
                                <th class='th' >Ngày lên TOP</th>
                                <th class='th' >Mã tin</th>
                                <th class='th' >Người đăng bài</th>
                                <th class='th' >Lĩnh vực</th>
                                <th class='th' >Khu vực</th>
                                <th class='th' style='white-space: nowrap;'>Tiêu đề</th>
								<th class='th'>Giá</th>
                                <th class='th'>Loại Tin</th>
                                <th class='th'>Tỉ lệ hoa hồng</th>
                                <th class='th'>Hoa hồng cố định</th>
                                <th class='th'>Hoa hồng thực tế</th>
                                <th class='th'>CTV đã cộng tác</th>
                                <th class='th' style='white-space: nowrap;'>isHot</th>
                                <th class='th' >Tình Trạng</th>
                                <th class='th' >Người duyệt</th>
                                <th class='th' >Ngày Admin duyệt</th>
                                <th class='th' style='white-space: nowrap;'>Lý do ko duyệt</th>
                                <th class='th' style='width:110px' ></th>
                                <th class='th' style='width:70px' ></th>
								<th class='th' style='width:60px;'></th>
                            </tr>
                            ";
        string url1 = "";
        if (filterDMC1 != "")
            url1 += "LinhVuc=" + filterDMC1 + "&";
        if (filterDMC2 != "")
            url1 += "LinhVucC2=" + filterDMC1 + "&";
        if (filterCity != "")
            url1 += "FilterCity=" + filterCity + "&";
        if (filterDistrict != "")
            url1 += "FilterDistrict=" + filterDistrict + "&";
        if (filterWard != "")
            url1 += "FilterWard=" + filterWard + "&";
        if (sTuNgay != "")
            url1 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url1 += "DenNgay=" + sDenNgay + "&";
        if (sThoiHan != "")
            url1 += "ThoiHan=" + sThoiHan + "&";
        if (sTinhTrang != "")
            url1 += "TinhTrang=" + sTinhTrang + "&";
        //Lọc hoa hồng
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
           url1 += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
           url1 += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
           url1 += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
           url1 += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
           url1 += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
           url1 += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
           url1 += "HoaHong=10100100500500&";
        //Lọc hoa hồng       
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
           url1 += "Price=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
           url1 += "Price=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
           url1 += "Price=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
           url1 += "Price=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
           url1 += "Price=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
           url1 += "Price=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
           url1 += "Price=10100100500500&";

        if (sTenDangNhap != "")
            url1 += "TenDangNhap=" + sTenDangNhap + "&";
        if (NguoiDuyet != "")
            url1 += "ND=" + NguoiDuyet + "&";
        if (sisHot != "")
            url1 += "isHot=" + sisHot + "&";
        if (MT != "")
            url1 += "MT=" + MT + "&";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            string accountInfoText = "";
            accountInfoText += " data-thumbnail='" + Utilities.Account.getLinkThumbWithPath(table.Rows[i]["AnhDaiDien"].ToString()) + "' ";
            accountInfoText += " data-fullname='" + table.Rows[i]["TenThanhVien"].ToString() + "' ";
            accountInfoText += " data-code='" + "TT" + table.Rows[i]["idThanhVien"].ToString() + "' ";
            if(table.Rows[i]["StsPro"].ToString() == "1")
            {
                accountInfoText += " data-pro=' Thành Viên Pro' ";
            }
            else
            {
                accountInfoText += " data-pro=' Thành Viên Thường' ";
            }
            accountInfoText += " data-phone='" + table.Rows[i]["SDT"].ToString() + "' ";
            accountInfoText += " data-email='" + table.Rows[i]["TVEmail"].ToString() + "' ";
            accountInfoText += " data-addr='" + table.Rows[i]["Address"].ToString() + ", " + table.Rows[i]["Ward"].ToString() + ", " + table.Rows[i]["District"].ToString() + ", " + table.Rows[i]["City"].ToString() + "' ";
            accountInfoText += " data-register-at='" + ((DateTime)table.Rows[i]["NgayDangKy"]).ToString("dd-MM-yyyy HH:mm:ss") + "' ";
            accountInfoText += " data-tax-number='" + table.Rows[i]["TVCMND"].ToString() + "' ";

            html += " <tr>";
            html += "       <td>" + (((Page - 1) * PageSize) + i + 1).ToString() + "</td>";
            html += "       <td><a href='javascript:LenTop(" + table.Rows[i]["idTinDang"] + ");' title='Lên TOP'><i class='fa fa-arrow-circle-up'></i> TOP</a></td>";
            if (table.Rows[i]["NgayDang"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";
            //gui duyet
            if (table.Rows[i]["NgayGuiDuyet"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayGuiDuyet"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";



            if (table.Rows[i]["NgayDayLenTop"].ToString().ToString() != "")
            {
                if (table.Rows[i]["isDuyet"].ToString() == "True")
                {
                    html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayDayLenTop"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                }
                else
                {
                    html += "   <td></td>";
                }
            }
            else
                html += "   <td></td>";
            
            
            html += "       <td>" + table.Rows[i]["Code"] + "</td>";
            // title
            html += @"<td class='td data-col-account social'>";
            html += @"<p> Mã TV: <a href='javascript:void(0);' class='user-info-btn' data-account-id='" + table.Rows[i]["idThanhVien"].ToString() + "' " + accountInfoText + " >" + "TT" + table.Rows[i]["idThanhVien"].ToString() + "</a></p>";
            html += @"<p> Tên TV: <a href='javascript:void(0);' class='user-info-btn' data-account-id='" + table.Rows[i]["idThanhVien"].ToString() + "' " + accountInfoText + " >" + table.Rows[i]["TenThanhVien"].ToString() + "</a></p>";
            html += @"<p> SĐT: <a href='tel:" + table.Rows[i]["SDT"].ToString() + "'>" + table.Rows[i]["SDT"].ToString() + @" </a></p>";
            html += @"</td>";
            if (table.Rows[i]["idDanhMucCap2"].ToString() != "")
            {
                html += "       <td  class='social'>" + StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", table.Rows[i]["idDanhMucCap1"].ToString()) + ", "+ StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", table.Rows[i]["idDanhMucCap2"].ToString()) + "</td>";
            }
            else
            {
                html += "       <td  class='social'>" + StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", table.Rows[i]["idDanhMucCap1"].ToString()) + "</td>";
            }
            //khu vực
            if(table.Rows[i]["WardName"].ToString() =="" && table.Rows[i]["DistrictName"].ToString() == "" && table.Rows[i]["CityName"].ToString() =="")
            {
                html += "       <td class='social'> Toàn Quốc </td>";
            }
            else if(table.Rows[i]["CityName"].ToString() != "" && table.Rows[i]["DistrictName"].ToString() == "" && table.Rows[i]["WardName"].ToString() == "")
            {
                html += " <td class='social'>" + table.Rows[i]["CityName"].ToString()+ "</td>";
            }
            else if(table.Rows[i]["CityName"].ToString() != "" && table.Rows[i]["DistrictName"].ToString() !="" && table.Rows[i]["WardName"].ToString() == "")
            {
                html += "<td class='social'>" + table.Rows[i]["DistrictName"].ToString() + ", " + table.Rows[i]["CityName"].ToString() + "</td>";
            }
            else
            {
                html += "       <td class='social'>" + table.Rows[i]["WardName"].ToString() + ", " + table.Rows[i]["DistrictName"].ToString() + ", " + table.Rows[i]["CityName"].ToString() + "</td>";
            }
              
            html += "       <td class='social' style='width: 400px'><a style='cursor:pointer;' href='/Admin/QuanTriTinDang/PendingPostForm.aspx?idTinDang=" + table.Rows[i]["idTinDang"].ToString() + "&" + url1 + "'>" + table.Rows[i]["TieuDe"].ToString() + "</a></td>";
           
            if (table.Rows[i]["GiaTien"].ToString() != "")
                html += "<td>" + double.Parse(table.Rows[i]["GiaTien"].ToString()).ToString("#,##").Replace(",", ".") + "</td>";

            //Loai Tin
            html += "  <td style='min-width:100px'>";
            if (table.Rows[i]["FeeCodeS"].ToString() == "" || table.Rows[i]["FeeCodeS"].ToString() == "default_pack")
            {
                html += "<strong> Tin Thường</strong>";
            }
            else if (table.Rows[i]["FeeCodeS"].ToString() == "vip1_pack")
            {
                html += "<strong> Tin Vip 1</strong>";
            }
            else if (table.Rows[i]["FeeCodeS"].ToString() == "vip2_pack")
            {
                html += "<strong> Tin Vip 2</strong>";
            }
            else if (table.Rows[i]["FeeCodeS"].ToString() == "vip3_pack")
            {
                html += "<strong> Tin Vip 3</strong>";
            }
            html += "</td>";

            // column
            html += "<td>" + Utilities.Formatter.toPercentString(table.Rows[i]["ReferralRate2"].ToString()) + "</td>";
            // column
            html += "<td>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["ReferralAmount2"].ToString()) + "</td>";
            // column
            html += "<td>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["Commission2"].ToString()) + "</td>";
            // column
            html += "<td><a href='/Admin/Referral/ReferralTableByPost.aspx?PostId="+ table.Rows[i]["idTinDang"].ToString() + "'>"+ table.Rows[i]["SelledDetails"].ToString() + "</a></td>";
            // column
           
            // column
            html += "   <td style='display:none;'><a id='btHetHan_" + table.Rows[i]["idTinDang"] + "' style='cursor:pointer' onclick='CapNhatHetHan(\"" + table.Rows[i]["idTinDang"] + "\")'><img class='imgedit' src='../images/edit.png' style='width:25px; cursor:pointer'/>Cập nhật hạn</a></td>";
            if (table.Rows[i]["isHot"].ToString() == "True")
                html += "<td><input type='checkbox' disabled checked/></td>";
            else
                html += "<td><input type='checkbox' disabled/></td>";
            if(table.Rows[i]["isHetHan"].ToString() == "True")
            {
                html += "<td> Hết hạn </td>";
            }
            else
            {
                html += "<td> Còn hạn </td>";
            }
            html += "       <td>" + StaticData.getField("tb_Admin", "TenDangNhap", "idAdmin", table.Rows[i]["idAdmin_Duyet"].ToString()) + "</td>";
            if (table.Rows[i]["isDuyet"].ToString() == "True" || table.Rows[i]["isDuyet"].ToString() == "False")
            {
                if (table.Rows[i]["NgayAdminDuyet"].ToString().ToString() != "")
                {
                    html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayAdminDuyet"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                }
                else if (table.Rows[i]["NgayGuiDuyet"].ToString().ToString() != "")
                {
                    html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayGuiDuyet"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                }
            }
            else
                html += "   <td></td>";
            // column
            // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script> var LyDo = ("+table.Rows[i]["LyDo_KhongDuyet"].ToString()+ ").replaceAll('|', '<br />');</script>");
            string LyDo = table.Rows[i]["LyDo_KhongDuyet"].ToString();
            string LyDos = LyDo.Replace("|", "<br/>");
            html += "       <td class='social' style='width: 400px'>" + LyDos + "</td>";
            // column
            // column
            html += "       <td><select id='slDuyet_" + table.Rows[i]["idTinDang"] + "' disabled='disabled' class='form-control' style='width:122px'>";
            if (table.Rows[i]["isDuyet"].ToString() == "")
                html += "<option value='DangCho' selected='selected'>Đang chờ...</option>";
            else
                html += "<option value='DangCho'>Đang chờ...</option>";
            if (table.Rows[i]["isDuyet"].ToString() == "True")
                html += "<option value='Duyet' selected='selected'>Duyệt</option>";
            else
                html += "<option value='Duyet'>Duyệt</option>";
            if (table.Rows[i]["isDuyet"].ToString() == "False")
                html += "<option value='KhongDuyet' selected='selected'>Không duyệt</option>";
            else
                html += "<option value='KhongDuyet'>Không duyệt</option>";
            html += "       </select></td>";
            // column
            html += "   <td><a id='btDuyet_" + table.Rows[i]["idTinDang"].ToString() + "' style='cursor:pointer' onclick='DuyetTinDang(\"" + table.Rows[i]["idTinDang"].ToString() + "\")'><img class='imgedit' src='../images/edit.png' style='width:25px; cursor:pointer'/>Sửa</a></td>";
            // column
            html += "   <td><a style='cursor:pointer'  onclick='DeleteTinDang(\"" + table.Rows[i]["idTinDang"].ToString() + "\")'> <img class='imgedit' src='../images/delete.png' />Xóa</a></td>";
			html += "       </tr>";
        }
        html += "   <tr>";
        html += "       <td colspan='20' class='footertable'>";
        string url = "PostTable.aspx?";
        if (filterDMC1 != "")
            url += "LinhVuc=" + filterDMC1 + "&";
        if (filterDMC2 != "")
            url += "LinhVucC2=" + filterDMC2 + "&";
        if (filterCity != "")
            url += "FilterCity=" + filterCity + "&";
        if (filterDistrict != "")
            url += "FilterDistrict=" + filterDistrict + "&";
        if (filterWard != "")
            url += "FilterWard=" + filterWard + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";
        if (sThoiHan != "")
            url += "ThoiHan=" + sThoiHan + "&";
        if (sTinhTrang != "")
            url += "TinhTrang=" + sTinhTrang + "&";
        if (sTenDangNhap != "")
            url += "TenDangNhap=" + sTenDangNhap + "&";
         //Lọc hoa hồng
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
           url += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
           url += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
           url += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
           url += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
           url += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
           url += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
           url += "HoaHong=10100100500500&";
        //Lọc hoa hồng       
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
           url += "Price=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
           url += "Price=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
           url += "Price=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
           url += "Price=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
           url += "Price=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
           url += "Price=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
           url += "Price=10100100500500&";
        if (NguoiDuyet != "")
            url += "ND=" + NguoiDuyet + "&";
        if (sisHot != "")
            url += "isHot=" + sisHot + "&";
        if (sHoaHong != "")
            url += "Commission=" + sHoaHong + "&";
        if (MT != "")
            url += "MT=" + MT + "&";

        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvTinDang.InnerHtml = html;
    }
    private void initSearchValues()
    {
        try
        {
            sTenDangNhap = Request.QueryString["TenDangNhap"].Trim();
            drdlCollab.SelectedValue = sTenDangNhap;
        }
        catch { }

        try
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("LinhVuc")))
            {
                string filterDMC1 = Request.QueryString.Get("LinhVuc").Trim();
                this.filterDMC1 = filterDMC1;
                this.templateFilterDMC1 = filterDMC1;
            }
        }
        catch { }

        try
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("LinhVucC2")))
            {
                string filterDMC2 = Request.QueryString.Get("LinhVucC2").Trim();
                this.filterDMC2 = filterDMC2;
                this.templateFilterDMC2 = filterDMC2;
            }
        }
        catch { }

        try
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCity")))
            {
                string filterCity = Request.QueryString.Get("FilterCity").Trim();
                this.filterCity = filterCity;
                this.templateFilterCity = filterCity;
            }
        }
        catch { }

        try
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterDistrict")))
            {
                string filterDistrict = Request.QueryString.Get("FilterDistrict").Trim();
                this.filterDistrict = filterDistrict;
                this.templateFilterDistrict = filterDistrict;
            }
        }
        catch { }

        try
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterWard")))
            {
                string filterWard = Request.QueryString.Get("FilterWard").Trim();
                this.filterWard = filterWard;
                this.templateFilterWard = filterWard;
            }
        }
        catch { }


        try
        {
            if (Request.QueryString["TuNgay"].Trim() != "")
            {
                sTuNgay = Request.QueryString["TuNgay"].Trim();
                txtTuNgay.Value = sTuNgay;
            }
        }
        catch { }

        try
        {
            if (Request.QueryString["DenNgay"].Trim() != "")
            {
                sDenNgay = Request.QueryString["DenNgay"].Trim();
                txtDenNgay.Value = sDenNgay;
            }
        }
        catch { }

        try
        {
            if (Request.QueryString["ThoiHan"].Trim() != "")
            {
                sThoiHan = Request.QueryString["ThoiHan"].Trim();
                slHan.Value = sThoiHan;
            }
        }
        catch { }

        try
        {
            if (Request.QueryString["TinhTrang"].Trim() != "")
            {
                sTinhTrang = Request.QueryString["TinhTrang"].Trim();
                slTinhTrang.Value = sTinhTrang;
            }
        }
        catch { }

        try
        {
            if (Request.QueryString["Commission"].Trim() != "")
            {
                sHoaHong = Request.QueryString["Commission"].Trim();
                slHoaHong.Value = sHoaHong;
            }
        }
        catch { }


        try
        {
            if (Request.QueryString["isHot"].Trim() != "")
            {
                sisHot = Request.QueryString["isHot"].Trim();
                slisHot.Value = sisHot;
            }
        }
        catch { }
        try
        {
            if (Request.QueryString["MT"].Trim() != "")
            {
                MT = Request.QueryString["MT"].Trim();
                slMaTin.Value = MT;
            }
        }
        catch { }
        //try
        //{
        //    if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostPrice")))
        //    {
        //        string filterPostPrice = Request.QueryString.Get("FilterPostPrice").Trim();
        //        this.filterPostPrice = filterPostPrice;
        //        this.templateFilterPostPrice = filterPostPrice;
        //        string[] temp = filterPostPrice.Split(';');
        //        if (temp.Length == 2)
        //        {
        //            Double.TryParse(temp[0], out this.filterFromPostPrice);
        //            Double.TryParse(temp[1], out this.filterToPostPrice);
        //        }
        //    }
        //}
        //catch { }
        //try
        //{
        //    if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostCommission")))
        //    {
        //        string filterPostCommission = Request.QueryString.Get("FilterPostCommission").Trim();
        //        this.filterPostCommission = filterPostCommission;
        //        this.templateFilterPostCommission = filterPostCommission;
        //        string[] temp = filterPostCommission.Split(';');
        //        if (temp.Length == 2)
        //        {
        //            Double.TryParse(temp[0], out this.filterFromPostCommission);
        //            Double.TryParse(temp[1], out this.filterToPostCommission);
        //        }
        //    }
        //}
        //catch { }

    }
    private void LoadLiDo()
    {
        string html = "";
        string sql = "select * from tb_NoiDung";
        DataTable tb = Connect.GetTable(sql);
        for (int i = 0; i < tb.Rows.Count; i++)
        {
            html += @"
            <li><label><input type='checkbox' name='kd' value='" + tb.Rows[i]["NoiDung"].ToString() + @"'/> " + tb.Rows[i]["NoiDung"].ToString() + @"</label><span style='float:right' ><a id='idLyDo_" + tb.Rows[i]["id"].ToString() + @"' onclick='SuaLyDo(" + tb.Rows[i]["id"].ToString() + @")'>Sửa</a>&ensp;<a id='idLyDo_" + tb.Rows[i]["id"].ToString() + @"' onclick='DeleteLyDo(" + tb.Rows[i]["id"].ToString() + @")' style='color: red;'>Xóa</a></li>
            <div id='hienlydo_" + tb.Rows[i]["id"].ToString() + @"' style='display:none'><input id='txtSuaLydo_" + tb.Rows[i]["id"].ToString() + @"' data-val='true' data-val-required='' name='sualydo' type='text' class='inputLydo' style='font - family:Arial,sans - serif;margin-top:0px;' value='" + tb.Rows[i]["NoiDung"].ToString() + @"'/><button id='btnSuaLyDo_" + tb.Rows[i]["id"].ToString() + @"' onclick='SuaLyDoKD(" + tb.Rows[i]["id"].ToString() + @")' class='btn btn-info btn - flat' style='float:left; margin - top:5px;'><i class='fa fa-pencil - square - o' aria-hidden='true'></i> Sửa</button></div><br />            
            <hr style='margin-top:5px;margin-bottom:5px'/>";
        }
        htmlLydo.InnerHtml = html;
    }

    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string DMC1 = ipDmc1.Value.Trim();
        string DMC2 = ipDmc2.Value.Trim();
        string City = ipCity.Value.Trim();
        string DisTricts = ipDistrict.Value.Trim();
        string Ward = ipWard.Value.Trim();
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string ThoiHan = slHan.Value.Trim();
        string TinhTrang = slTinhTrang.Value.Trim();
        string HoaHong = slHoaHong.Value.Trim();
        string TenDangNhap = drdlCollab.SelectedValue;
        string isHot = slisHot.Value.Trim();
        string NguoiDuyet = slNguoiDuyet.Value.Trim();
        string MT = slMaTin.Value.Trim();
        string rangeCounterCommission = txtCommissionCounterRange.Text.Trim();
        string rangeCounterFee = txtFeeCounterRange.Text.Trim();

        string url = "/Admin/Posts/PostTable.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (ThoiHan != "0")
            url += "ThoiHan=" + ThoiHan + "&";
        if (TinhTrang != "-1")
            url += "TinhTrang=" + TinhTrang + "&";
        if (TenDangNhap != "")
            url += "TenDangNhap=" + TenDangNhap + "&";
        if (MT != "")
            url += "MT=" + MT + "&";
        if (NguoiDuyet != "")
            url += "ND=" + NguoiDuyet + "&";
        if (DMC1 != "")
            url += "LinhVuc=" + DMC1 + "&";
        if (DMC2 != "")
            url += "LinhVucC2=" + DMC2 + "&";
        if (City != "")
            url += "FilterCity=" + City + "&";
        if (DisTricts != "")
            url += "FilterDisTrict=" + DisTricts + "&";
        if (Ward != "")
            url += "FilterWard=" + Ward + "&";
        if (isHot != "-1")
            url += "isHot=" + isHot + "&";
        if (HoaHong != "0")
            url += "Commission=" + HoaHong + "&";
        //Lọc Hoa hồng
        if (txtRangeLuotXemhh.Value == "Chọn")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=0&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=100500500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100100500500&";
        }
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
            url += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
            url += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
            url += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=10100100500500&";
        //Lọc Chi Hoa hồng
        if (txtRangeLuotXemchh.Value == "Chọn")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=0&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=10100&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=10100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=100500500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=10100100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "Price=10100100500500&";
        }
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
            url += "Price=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
            url += "Price=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
            url += "Price=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "Price=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "Price=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "Price=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "Price=10100100500500&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "PostTable.aspx";
        Response.Redirect(url);
    }
}