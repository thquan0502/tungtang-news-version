﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="PostReferralTable.aspx.cs" Inherits="Admin_Referral_ReferralTable" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- range --%>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="/asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    <%-- datepicker --%>
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>
    <%-- select2 --%>
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-select/bootstrap-select.min.css" />
    <link rel="stylesheet" type="text/css" href="/asset/vendors/select2/select2.css" />
    <script type="text/javascript" src="/asset/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/select2/select2.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title">
                    <span>Các cộng tác tin đăng</span>
                </h1>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Bài đăng</label>
                                    <asp:TextBox ID="txtPostTitle" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Người đăng</label>
                                    <asp:DropDownList ID="drdlOwner" runat="server"  CssClass="form-control" autocomplete="off">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Cộng tác viên</label>
                                    <asp:DropDownList ID="drdlCollab" runat="server"  CssClass="form-control" autocomplete="off">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Trạng thái</label>
                                    <asp:DropDownList ID="drdlReferralStatus" runat="server"  CssClass="form-control" autocomplete="off">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Thời gian duyệt</label>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                <asp:TextBox ID="txtApprovedAtFrom" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
										        <span class="input-group-addon"> đến </span>
										        <asp:TextBox ID="txtApprovedAtTo" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
									        </div>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="drdlTypeAprrovedAt" runat="server"  CssClass="form-control" autocomplete="off">
                                                <asp:ListItem Value="All" Text="Tất cả" />
                                                <asp:ListItem Value="ReferralOwnerApprovedAt" Text="Người đăng bài" />
                                                <asp:ListItem Value="ReferralAdminApprovedAt" Text="Tung Tăng" />
                                                <asp:ListItem Value="ReferralCollabApprovedAt" Text="CTV" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label">Hoa hồng</label>
                                <asp:TextBox ID="txtReferralCommission" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Tỉ lệ hoa hồng</label>
                                <asp:TextBox ID="txtReferralRate" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label class="control-label">Hoa hồng cố định</label>
                                <asp:TextBox ID="txtReferralAmount" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <asp:Button ID="btnFilter" runat="server" Text="Lọc kết quả" CssClass="btn btn-primary" style="margin-top: 10px;" OnClick="btnFilter_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divPagination2" class="app-table-pagination" style="text-align:right;" runat="server"></div>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
                <div id="divPagination1" class="app-table-pagination" style="text-align:right;" runat="server"></div>
            </section>
        </div>
        <%-- modal account info --%>
        <div class="modal fade account-info" id="modalAccountInfo" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					    <div class="row">
                            <div class="col-md-3 profile-col">
                                <p>
                                    <img class="profile-img" id="modalAccountInfo_imgThumbnail" runat="server" src="" />
                                </p>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><label>Họ tên: </label> <span id="modalAccountInfo_spanFullName" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><label><span class="code-text"></span>: </label> <span id="modalAccountInfo_spanCode" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Số điện thoại: </label> <a id="modalAccountInfo_linkPhone" runat="server" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Email: </label> <a id="modalAccountInfo_linkEmail" runat="server" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Địa chỉ: </label> <span id="modalAccountInfo_spanAddr" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Đăng ký lúc: </label> <span id="modalAccountInfo_spanRegisterAt" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Mã số thuế: </label> <span id="modalAccountInfo_spanTaxNumber">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Ngày cấp: </label> <span id="modalAccountInfo_spanTaxCreatedDate">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Nơi cấp: </label> <span id="modalAccountInfo_spanTaxCreatedPlace">(Chưa có thông tin)</span></p>
                                    </div>
					            </div>
                            </div>
					    </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Đóng</button>
					</div>
				</div>
			</div>
		</div>
        <%-- fake submit approve referral --%>
        <div class="modal fade referral-info" id="modalReferralInfo" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Thông tin cộng tác</h4>
					</div>
					<div class="modal-body">
					    <div class="row">
                            <div class="col-md-4 widget-info">
                                <p class="app-label-1">Người đăng bài:</p>
                                <div id="modalReferralInfo_ownerDetails"></div>
                            </div>
                            <div class="col-md-4 widget-info">
                                <p class="app-label-1">Bài viết cộng tác:</p>
                                <div id="modalReferralInfo_postDetails"></div>
                            </div>
                            <div class="col-md-4 widget-info">
                                <p class="app-label-1">Cộng tác viên:</p>
                                <div id="modalReferralInfo_collabDetails"></div>
                            </div>
					    </div>
					</div>
					<div class="modal-footer">
                        <button id="modalReferralInfo_btnApprove" type="button" class="btn btn-primary">Xác nhận cộng tác</button>
						<button type="button" class="btn default" data-dismiss="modal">Đóng</button>
					</div>
				</div>
			</div>
		</div>
        <asp:Button ID="PFake_ApproveReferral_Submit" runat="server" style="display: none;" OnClick="PFake_ApproveReferral_Submit_Click"/>
        <asp:TextBox ID="PFake_ApproveReferral_ReferrralId" type="hidden" runat="server"></asp:TextBox>
        <asp:TextBox ID="PFake_ApproveReferral_RedirectUrl" type="hidden" runat="server"></asp:TextBox>
    </form>
    <script>
        $(document).ready(function () {
            $("#ContentPlaceHolder1_drdlCollab").select2({
            });
        });
        $(document).ready(function () {
            $("#ContentPlaceHolder1_drdlOwner").select2({
            });
        });
        $('#ContentPlaceHolder1_txtReferralCommission').ionRangeSlider({
            min: 0,
            max: 50,
            type: 'double',
            step: 1,
            postfix: " Triệu",
            hasGrid: true,
        });
        $('#ContentPlaceHolder1_txtReferralRate').ionRangeSlider({
            min: 0,
            max: 100,
            type: 'double',
            step: 1,
            postfix: "%",
            hasGrid: true,
        });
        $('#ContentPlaceHolder1_txtReferralAmount').ionRangeSlider({
            min: 0,
            max: 50,
            type: 'double',
            step: 1,
            postfix: " Triệu",
            hasGrid: true,
        });
        $('.date-picker').datepicker({
            language: 'vi',
            orientation: "left",
            autoclose: true
        });
    </script>
    <%-- process modal account info --%>
    <script>
        $(document).ready(function () {
            $('.user-info-btn').click(function () {
                var accountType = $(this).data("account-type");
                var dataAccountId = $(this).data("account-id");
                var dataThumbnail = $(this).data("thumbnail");
                var dataFullname = $(this).data("fullname");
                var dataCode = $(this).data("code");
                var dataPhone = $(this).data("phone");
                var dataEmail = $(this).data("email");
                var dataAddr = $(this).data("addr");
                var dataRegisterAt = $(this).data("register-at");
                var dataTaxNumber = $(this).data("tax-number");
                var dataTaxCreatedDate = $(this).data("tax-created-date");
                var dataTaxCreatedPlace = $(this).data("tax-created-place");
                if (dataAccountId == undefined || dataAccountId == null || dataAccountId == "") {
                    return;
                }
                if (accountType == 'OWNER') {
                    $('#modalAccountInfo .modal-title').text("Thông tin Người đăng bài");
                    $('#modalAccountInfo .code-text').text("Mã NĐB");
                } else if (accountType == 'COLLAB') {
                    $('#modalAccountInfo .modal-title').text("Thông tin CTV");
                    $('#modalAccountInfo .code-text').text("Mã CTV");
                } else {
                    return;
                }
                $('#ContentPlaceHolder1_modalAccountInfo_imgThumbnail').attr("src", (dataThumbnail != "" ? dataThumbnail : "#"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanFullName').text((dataFullname != "" ? dataFullname : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanCode').text((dataCode != "" ? dataCode : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkPhone').text((dataPhone != "" ? dataPhone : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkPhone').attr("href", (dataPhone != "" ? ("tel:" + dataPhone) : "javascript:void(0);"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkEmail').text((dataEmail != "" ? dataEmail : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkEmail').attr("href", (dataEmail != "" ? "mailto:" + dataEmail : "javascript:void(0);"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanAddr').text((dataAddr != "" ? dataAddr : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanRegisterAt').text((dataRegisterAt != "" ? dataRegisterAt : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxNumber').text((dataTaxNumber != "" ? dataTaxNumber : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxCreatedDate').text((dataTaxCreatedDate != "" ? dataTaxCreatedDate : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxCreatedPlace').text((dataTaxCreatedPlace != "" ? dataTaxCreatedPlace : "(Chưa có thông tin)"));
                $('#modalAccountInfo').modal();
            });
        });
    </script>
    <%-- process approve referral --%>
    <script>
        (function () {
            var redirectUrl = window.location.href.substr(window.location.href.indexOf(window.location.origin) + window.location.origin.length);
            $('#ContentPlaceHolder1_PFake_ApproveReferral_RedirectUrl').val(redirectUrl);
            $('.btn-approve-referral').click(function () {
                var postDetails = $('#modalReferralInfo_postDetails');
                postDetails.html('');
                var ownerDetails = $('#modalReferralInfo_ownerDetails');
                ownerDetails.html('');
                var collabDetails = $('#modalReferralInfo_collabDetails');
                collabDetails.html('');

                var referralId = $(this).data('referral-id');

                var postUrl = $(this).data('post-url');
                if (postUrl == undefined || postUrl == null || postUrl == "") {
                    postUrl = "javascript:voif(0);";
                }

                var postTitle = $(this).data('post-title');
                if (postTitle != undefined && postTitle != null && postTitle != "") {
                    postDetails.append('<p><label>Tiêu đề:</label> <a href="' + postUrl + '">' + postTitle + '</a></p>');
                }

                var referralAdminApprovedAt = $(this).data('referral-admin-approved-at');
                if (referralAdminApprovedAt != undefined && referralAdminApprovedAt != null && referralAdminApprovedAt != "") {
                    postDetails.append('<p><label>Tung Tăng duyệt:</label> <span>' + referralAdminApprovedAt + '</span></p>');
                }

                var referralCommissionFormat = $(this).data('referral-commission-format');
                if (referralCommissionFormat != undefined && referralCommissionFormat != null && referralCommissionFormat != "") {
                    postDetails.append('<p class="bg-success"><label>Hoa hồng:</label> <span>' + referralCommissionFormat + '</span></p>');
                }

                var postCode = $(this).data('post-code');
                if (postCode != undefined && postCode != null && postCode != "") {
                    postDetails.append('<p><label>Mã bài viết:</label> <span>' + postCode + '</span></p>');
                }

                var postCreatedAt = $(this).data('post-created-at');
                if (postCreatedAt != undefined && postCreatedAt != null && postCreatedAt != "") {
                    postDetails.append('<p><label>Ngày đăng:</label> <span>' + postCreatedAt + '</span></p>');
                }

                var collabFullname = $(this).data('collab-fullname');
                if (collabFullname != undefined && collabFullname != null && collabFullname != "") {
                    collabDetails.append('<p><label>Họ tên:</label> <span>' + collabFullname + '</span></p>');
                }

                var referralCollabApprovedAt = $(this).data('referral-collab-approved-at');
                if (referralCollabApprovedAt != undefined && referralCollabApprovedAt != null && referralCollabApprovedAt != "") {
                    collabDetails.append('<p><label>Đã duyệt:</label> <span>' + referralCollabApprovedAt + '</span></p>');
                }

                var referralCollabDeniedAt = $(this).data('referral-collab-denied-at');
                if (referralCollabDeniedAt != undefined && referralCollabDeniedAt != null && referralCollabDeniedAt != "") {
                    collabDetails.append('<p><label>Đã từ chối:</label> <span>' + referralCollabDeniedAt + '</span></p>');
                }

                var collabCode = $(this).data('collab-code');
                if (collabCode != undefined && collabCode != null && collabCode != "") {
                    collabDetails.append('<p><label>Mã CTV:</label> <span>' + collabCode + '</span></p>');
                }

                var collabPhone = $(this).data('collab-phone');
                if (collabPhone != undefined && collabPhone != null && collabPhone != "") {
                    collabDetails.append('<p><label>Số điện thoại CTV:</label> <a href="tel:' + collabPhone + '">' + collabPhone + '</a></p>');
                }

                var collabEmail = $(this).data('collab-email');
                if (collabEmail != undefined && collabEmail != null && collabEmail != "") {
                    collabDetails.append('<p><label>Email CTV:</label> <a href="mailto:' + collabEmail + '">' + collabEmail + '</a></p>');
                }

                var ownerFullname = $(this).data('owner-fullname');
                if (ownerFullname != undefined && ownerFullname != null && ownerFullname != "") {
                    ownerDetails.append('<p><label>Họ tên:</label> <span>' + ownerFullname + '</span></p>');
                }

                var referralOwnerApprovedAt = $(this).data('referral-owner-approved-at');
                if (referralOwnerApprovedAt != undefined && referralOwnerApprovedAt != null && referralOwnerApprovedAt != "") {
                    ownerDetails.append('<p><label>Đã duyệt:</label> <span>' + referralOwnerApprovedAt + '</span></p>');
                }

                var referralOwnerCanceledAt = $(this).data('referral-owner-canceled-at');
                if (referralOwnerCanceledAt != undefined && referralOwnerCanceledAt != null && referralOwnerCanceledAt != "") {
                    ownerDetails.append('<p><label>Đã hủy trích hoa hồng:</label> <span>' + referralOwnerCanceledAt + '</span></p>');
                }

                var ownerCode = $(this).data('owner-code');
                if (ownerCode != undefined && ownerCode != null && ownerCode != "") {
                    ownerDetails.append('<p><label>Mã NĐB:</label> <span>' + ownerCode + '</span></p>');
                }

                var ownerPhone = $(this).data('owner-phone');
                if (ownerPhone != undefined && ownerPhone != null && ownerPhone != "") {
                    ownerDetails.append('<p><label>Số điện thoại NĐB:</label> <a href="tel:' + ownerPhone + '">' + ownerPhone + '</a></p>');
                }

                var ownerEmail = $(this).data('owner-email');
                if (ownerEmail != undefined && ownerEmail != null && ownerEmail != "") {
                    ownerDetails.append('<p><label>Email NĐB:</label> <a href="mailto:' + ownerEmail + '">' + ownerEmail + '</a></p>');
                }

                var approveButton = $(this).data('approve-button');
                if (approveButton == 1) {
                    $('#modalReferralInfo_btnApprove').show();
                    var referralId = $(this).data('referral-id');
                    $('#modalReferralInfo_btnApprove').data('referral-id', referralId);
                } else {
                    $('#modalReferralInfo_btnApprove').hide();
                }

                $('#modalReferralInfo').modal();
            });
            $('#modalReferralInfo_btnApprove').click(function () {
                var referralId = $(this).data('referral-id');
                if (referralId == undefined || referralId == null || referralId == "") {
                    return;
                } else {
                    $('#ContentPlaceHolder1_PFake_ApproveReferral_ReferrralId').val(referralId);
                    $('#ContentPlaceHolder1_PFake_ApproveReferral_Submit').click();
                }
            });
        }) ();
    </script>
</asp:Content>
