﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class Admin_Referral_ReferralTable : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {

        }
        else
        {
            this.initStatusDropdownList();
            this.initAccountDropdownList();
            this.loadMainTable();
        }
    }
    protected void loadMainTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 15;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> postParams = new Dictionary<string, string>();

        string filterPostTitle = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostTitle")))
        {
            filterPostTitle = Request.QueryString.Get("FilterPostTitle").Trim();
            postParams.Add("FilterPostTitle", filterPostTitle);
            txtPostTitle.Text = filterPostTitle;
        }

        string filterCollab = null;
        int filterCollab_int = 0;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCollab")))
        {
            filterCollab = Request.QueryString.Get("FilterCollab").Trim();
            if(Int32.TryParse(filterCollab, out filterCollab_int))
            {
                postParams.Add("FilterCollab", filterCollab);
                Utilities.Control.setSelectedValue(this.drdlCollab, filterCollab);
            }
        }

        string filterOwner = null;
        int filterOwner_int = 0;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterOwner")))
        {
            filterOwner = Request.QueryString.Get("FilterOwner").Trim();
            if (Int32.TryParse(filterOwner, out filterOwner_int))
            {
                postParams.Add("FilterOwner", filterOwner);
                Utilities.Control.setSelectedValue(this.drdlOwner, filterOwner);
            }
        }

        string filterReferralStatus = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterReferralStatus")))
        {
            filterReferralStatus = Request.QueryString.Get("FilterReferralStatus").Trim();
            postParams.Add("FilterReferralStatus", filterReferralStatus);
            Utilities.Control.setSelectedValue(this.drdlReferralStatus, filterReferralStatus);
        }

        string filterFromApprovedAt = null, filterFromApprovedAtAlt = null;
        DateTime filterFromApprovedAt_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterFromApprovedAt")))
        {
            filterFromApprovedAt = Request.QueryString.Get("FilterFromApprovedAt").Trim();
            postParams.Add("FilterFromApprovedAt", filterFromApprovedAt);
            this.txtApprovedAtFrom.Text = filterFromApprovedAt;
            if (DateTime.TryParseExact(filterFromApprovedAt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterFromApprovedAt_DateTime))
            {
                filterFromApprovedAtAlt = filterFromApprovedAt_DateTime.ToString("yyyy-MM-dd") + " 00:00:00:000";
            }
        }
        string filterToApprovedAt = null, filterToApprovedAtAlt = null;
        DateTime filterToApprovedAt_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterToApprovedAt")))
        {
            filterToApprovedAt = Request.QueryString.Get("FilterToApprovedAt").Trim();
            postParams.Add("FilterToApprovedAt", filterToApprovedAt);
            this.txtApprovedAtTo.Text = filterToApprovedAt;
            if (DateTime.TryParseExact(filterToApprovedAt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterToApprovedAt_DateTime))
            {
                filterToApprovedAtAlt = filterToApprovedAt_DateTime.ToString("yyyy-MM-dd") + " 23:59:59:999";
            }
        }
        string filterTypeApprovedAt = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterTypeApprovedAt")))
        {
            filterTypeApprovedAt = Request.QueryString.Get("FilterTypeApprovedAt").Trim();
            postParams.Add("FilterTypeApprovedAt", filterTypeApprovedAt);
            Utilities.Control.setSelectedValue(this.drdlTypeAprrovedAt, filterTypeApprovedAt);
        }

        string filterRangeReferralCommission = null; 
        int filterRangeFromReferralCommission = -1, filterRangeToReferralCommission = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterRangeReferralCommission")))
        {
            filterRangeReferralCommission = Request.QueryString.Get("FilterRangeReferralCommission").Trim();
            string[] filterRangeReferralCommissionVals = {};
            filterRangeReferralCommissionVals = filterRangeReferralCommission.Split(';');
            if (filterRangeReferralCommissionVals.Length == 2)
            {
                Int32.TryParse(filterRangeReferralCommissionVals[0], out filterRangeFromReferralCommission);
                Int32.TryParse(filterRangeReferralCommissionVals[1], out filterRangeToReferralCommission);
                postParams.Add("FilterRangeReferralCommission", filterRangeReferralCommission);
                txtReferralCommission.Text = filterRangeReferralCommission;
            }
        }

        string filterRangeReferralRate = null;
        int filterRangeFromReferralRate = -1, filterRangeToReferralRate = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterRangeReferralRate")))
        {
            filterRangeReferralRate = Request.QueryString.Get("FilterRangeReferralRate").Trim();
            string[] filterRangeReferralRateVals = { };
            filterRangeReferralRateVals = filterRangeReferralRate.Split(';');
            if (filterRangeReferralRateVals.Length == 2)
            {
                Int32.TryParse(filterRangeReferralRateVals[0], out filterRangeFromReferralRate);
                Int32.TryParse(filterRangeReferralRateVals[1], out filterRangeToReferralRate);
                postParams.Add("FilterRangeReferralRate", filterRangeReferralRate);
                txtReferralRate.Text = filterRangeReferralRate;
            }
        }

        string filterRangeReferralAmount = null;
        int filterRangeFromReferralAmount = -1, filterRangeToReferralAmount = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterRangeReferralAmount")))
        {
            filterRangeReferralAmount = Request.QueryString.Get("FilterRangeReferralAmount").Trim();
            string[] filterRangeReferralAmountVals = { };
            filterRangeReferralAmountVals = filterRangeReferralAmount.Split(';');
            if (filterRangeReferralAmountVals.Length == 2)
            {
                Int32.TryParse(filterRangeReferralAmountVals[0], out filterRangeFromReferralAmount);
                Int32.TryParse(filterRangeReferralAmountVals[1], out filterRangeToReferralAmount);
                postParams.Add("FilterRangeReferralAmount", filterRangeReferralAmount);
                txtReferralAmount.Text = filterRangeReferralAmount;
            }
        }

        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 += @"
            FROM (
	            SELECT 
                    ROW_NUMBER() OVER ( 
                        ORDER BY Referrals.Id DESC
                    ) AS RowNumber,
                    Referrals.Id AS ReferralId, 
                    Referrals.TinDangId AS TinDangId, 
                    Referrals.CollabId AS ReferralCollabId, 
                    Referrals.OwnerId AS ReferralOwnerId, 
                    ISNULL(Referrals.Rate, 0) AS ReferralRate, 
                    ISNULL(Referrals.Amount, 0) AS ReferralAmount, 
                    ISNULL(Referrals.Commission, 0) AS ReferralCommission, 
                    Referrals.Status AS ReferralStatus, 
                    ISNULL(Referrals.StsOwnerApproved, 0) AS ReferralStsOwnerApproved, 
                    ISNULL(Referrals.StsCollabApproved, 0) AS ReferralStsCollabApproved, 
                    ISNULL(Referrals.StsAdminApproved, 0) AS ReferralStsAdminApproved, 
                    ISNULL(Referrals.StsOwnerCanceled, 0) AS ReferralStsOwnerCanceled, 
                    ISNULL(Referrals.StsCollabDenied, 0) AS ReferralStsCollabDenied, 
                    Referrals.OwnerApprovedAt AS ReferralOwnerApprovedAt, 
                    Referrals.AdminApprovedAt AS ReferralAdminApprovedAt, 
                    Referrals.CollabApprovedAt AS ReferralCollabApprovedAt, 
                    Referrals.OwnerCanceledAt AS ReferralOwnerCanceledAt, 
                    Referrals.CollabDeniedAt AS ReferralCollabDeniedAt, 
                    TBPost.idTinDang AS PostId, 
                    ISNULL(TBPost.TieuDe, '') AS PostTitle, 
                    ISNULL(TBPost.DuongDan, '') AS PostSlug,
                    TBPost.isHetHan AS PostIsHetHan, 
                    TBPost.isDraft AS PostIsDraft, 
                    TBPost.isDuyet AS PostIsDuyet, 
                    TBPost.NgayDang AS PostCreatedAt, 
					ISNULL(TBPost.Code, '') AS PostCode,
                    ISNULL(TBOwner.Code, '') AS OwnerCode,
                    ISNULL(TBOwner.TenCuaHang, '') AS OwnerFullName,
                    ISNULL(TBOwner.LinkAnh, '') AS OwnerThumbnail,
                    ISNULL(TBOwner.SoDienThoai, '') AS OwnerPhone,
                    ISNULL(TBOwner.Email, '') AS OwnerEmail,
                    ISNULL(TBOwner.TenDangNhap, '') AS OwnerUsername,
                    ISNULL(TBOwner.NgayDangKy, '') AS OwnerRegisterAt,
                    ISNULL(TBOwner.DiaChi, '') AS OwnerAddress,
                    ISNULL(TBOwner.MST, '') AS OwnerTaxNumber, 
                    TBOwner.NgayCap AS OwnerTaxCreatedDate, 
                    ISNULL(TBOwner.NoiCap, '') AS OwnerTaxCreatedPlace, 
		            ISNULL(TBOwnerCities.Ten, '') AS OwnerCity,
		            ISNULL(TBOwnerDistricts.Ten, '') AS OwnerDistrict,
		            ISNULL(TBOwnerWards.Ten, '') AS OwnerWard,
		            ISNULL(TBCollab.Code, '') AS CollabCode,
		            ISNULL(TBCollab.TenCuaHang, '') AS CollabFullName,
		            ISNULL(TBCollab.LinkAnh, '') AS CollabThumbnail,
		            ISNULL(TBCollab.SoDienThoai, '') AS CollabPhone,
		            ISNULL(TBCollab.Email, '') AS CollabEmail,
		            ISNULL(TBCollab.TenDangNhap, '') AS CollabUsername,
		            ISNULL(TBCollab.NgayDangKy, '') AS CollabRegisterAt,
                    ISNULL(TBCollab.DiaChi, '') AS CollabAddress,
                    ISNULL(TBCollab.MST, '') AS CollabTaxNumber, 
                    TBCollab.NgayCap AS CollabTaxCreatedDate, 
                    ISNULL(TBCollab.NoiCap, '') AS CollabTaxCreatedPlace, 
                    ISNULL(TBCollabCities.Ten, '') AS CollabCity,
                    ISNULL(TBCollabDistricts.Ten, '') AS CollabDistrict,
                    ISNULL(TBCollabWards.Ten, '') AS CollabWard
                FROM Referrals  
                    LEFT JOIN tb_ThanhVien TBOwner
                        ON Referrals.OwnerId = TBOwner.idThanhVien 
		            LEFT JOIN City TBOwnerCities
                        ON TBOwner.idTinh = TBOwnerCities.id
		            LEFT JOIN District TBOwnerDistricts
                        ON TBOwner.idHuyen = TBOwnerDistricts.id
		            LEFT JOIN tb_PhuongXa TBOwnerWards
                        ON TBOwner.idPhuongXa = TBOwnerWards.id
                    LEFT JOIN tb_ThanhVien TBCollab
                        ON Referrals.CollabId = TBCollab.idThanhVien 
                    LEFT JOIN City TBCollabCities
                        ON TBCollab.idTinh = TBCollabCities.id 
                    LEFT JOIN District TBCollabDistricts
                        ON TBCollab.idHuyen = TBCollabDistricts.id 
                    LEFT JOIN tb_PhuongXa TBCollabWards
                        ON TBCollab.idPhuongXa = TBCollabWards.id 
                    INNER JOIN tb_TinDang TBPost
                        ON Referrals.TinDangId = TBPost.idTinDang 
                WHERE 1 = 1
         ";
        if (!string.IsNullOrWhiteSpace(filterPostTitle))
        {
            sql1 += @" AND TBPost.TieuDe LIKE N'%" + filterPostTitle + @"%' ";
        }
        if (filterCollab_int > 0)
        {
            sql1 += @" AND Referrals.CollabId = '" + filterCollab_int + @"' ";
        }
        if (filterOwner_int > 0)
        {
            sql1 += @" AND Referrals.OwnerId = '" + filterOwner_int + @"' ";
        }
        if (!string.IsNullOrWhiteSpace(filterReferralStatus))
        {
            if (filterReferralStatus == Models.Referral.STATUS_OWNER_APPROVED)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsAdminApproved, 0) = 0 ";
            }
            else if (filterReferralStatus == Models.Referral.STATUS_COLLAB_APPROVED)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsAdminApproved, 0) = 0 ";
            }
            else if (filterReferralStatus == Models.Referral.STATUS_ADMIN_APPROVED)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsAdminApproved, 0) = 1 ";
            }
        }
        if (!string.IsNullOrWhiteSpace(filterTypeApprovedAt))
        {
            switch (filterTypeApprovedAt)
            {
                case "ReferralOwnerApprovedAt":
                    if (!string.IsNullOrWhiteSpace(filterFromApprovedAtAlt))
                    {
                        sql1 += @" AND Referrals.OwnerApprovedAt >= '" + filterFromApprovedAtAlt + @"' ";
                    }
                    if (!string.IsNullOrWhiteSpace(filterToApprovedAtAlt))
                    {
                        sql1 += @" AND Referrals.OwnerApprovedAt <= '" + filterToApprovedAtAlt + @"' ";
                    }
                    break;
                case "ReferralAdminApprovedAt":
                    if (!string.IsNullOrWhiteSpace(filterFromApprovedAtAlt))
                    {
                        sql1 += @" AND Referrals.AdminApprovedAt >= '" + filterFromApprovedAtAlt + @"' ";
                    }
                    if (!string.IsNullOrWhiteSpace(filterToApprovedAtAlt))
                    {
                        sql1 += @" AND Referrals.AdminApprovedAt <= '" + filterToApprovedAtAlt + @"' ";
                    }
                    break;
                case "ReferralCollabApprovedAt":
                    if (!string.IsNullOrWhiteSpace(filterFromApprovedAtAlt))
                    {
                        sql1 += @" AND Referrals.CollabApprovedAt >= '" + filterFromApprovedAtAlt + @"' ";
                    }
                    if (!string.IsNullOrWhiteSpace(filterToApprovedAtAlt))
                    {
                        sql1 += @" AND Referrals.CollabApprovedAt <= '" + filterToApprovedAtAlt + @"' ";
                    }
                    break;
                default:
                    if (!string.IsNullOrWhiteSpace(filterFromApprovedAtAlt) && !string.IsNullOrWhiteSpace(filterToApprovedAtAlt))
                    {
                        sql1 += @" AND ( ";
                        sql1 += @" ( Referrals.OwnerApprovedAt >= '" + filterFromApprovedAtAlt + @"' ";
                        sql1 += @" AND Referrals.OwnerApprovedAt <= '" + filterToApprovedAtAlt + @"' ) ";
                        sql1 += @" OR ";
                        sql1 += @" ( Referrals.AdminApprovedAt >= '" + filterFromApprovedAtAlt + @"' ";
                        sql1 += @" AND Referrals.AdminApprovedAt <= '" + filterToApprovedAtAlt + @"' ) ";
                        sql1 += @" OR ";
                        sql1 += @" ( Referrals.CollabApprovedAt >= '" + filterFromApprovedAtAlt + @"' ";
                        sql1 += @" AND Referrals.CollabApprovedAt <= '" + filterToApprovedAtAlt + @"' ) ";
                        sql1 += @" ) ";
                    }
                    break;
            }
        }
        if (filterRangeFromReferralRate >= 0)
        {
            sql1 += @" AND Referrals.Rate >= "+ filterRangeFromReferralRate + " ";
        }
        if (filterRangeToReferralRate >= 0)
        {
            sql1 += @" AND Referrals.Rate <= " + filterRangeToReferralRate + " ";
        }
        if (filterRangeFromReferralAmount >= 0)
        {
            sql1 += @" AND Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        }
        if (filterRangeToReferralAmount >= 0)
        {
            sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        }
        if (filterRangeFromReferralCommission >= 0)
        {
            sql1 += @" AND Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
        }
        if (filterRangeToReferralCommission >= 0)
        {
            sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
        }
        sql1 += @"
            ) TBWrap
        ";

        sql2 += " SELECT COUNT(*) AS TOTALROWS " + sql1;

        sql3 += @" SELECT * " + sql1 + @" WHERE RowNumber BETWEEN " + fromIndex + @" AND " + toIndex + @" ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TOTALROWS"].ToString());

        DataTable table3 = Connect.GetTable(sql3);

        html = @"<table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                            <thead >
                                <tr>
                                    <th class='th '>Tiêu đề</th>
                                    <th class='th '>Ngày đăng</th>
                                    <th class='th data-col-account'>Người đăng bài</th>
                                    <th class='th data-col-account'>Cộng tác viên</th>
                                    <th class='th data-col-ref-commission'>Tiền hoa hồng</th>
                                    <th class='th data-col-ref-status'>Trạng thái</th>
                                    <th class='th data-col-datetime'>Duyệt lúc</th>
                                </tr>
                            </thead>
                            <tbody>";
        if (table3.Rows.Count == 0)
        {
            html += @"
                <tr>
                    <td colspan='7'><p class='text-center'><strong>Không có kết quả !</strong></p></td>
                </tr>
            ";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string tinDangStatus = Models.TinDang.getStatus(table3.Rows[i]["PostIsHetHan"].ToString(), table3.Rows[i]["PostIsDraft"].ToString(), table3.Rows[i]["PostIsDuyet"].ToString());
            string postTitle = table3.Rows[i]["PostTitle"].ToString();
            string postUrl = Utilities.Post.getUrl(table3.Rows[i]["PostSlug"].ToString());
            string postCode = table3.Rows[i]["PostCode"].ToString();
            string postCreatedAt = ((DateTime)table3.Rows[i]["PostCreatedAt"]).ToString("dd/MM/yyyy");
            string collabThumb = Utilities.Account.getLinkThumbWithPath(table3.Rows[i]["CollabThumbnail"].ToString());
            string collabFullname = table3.Rows[i]["CollabFullName"].ToString();
            string collabCode = table3.Rows[i]["CollabCode"].ToString();
            string collabPhone = table3.Rows[i]["CollabPhone"].ToString();
            string collabEmail = table3.Rows[i]["CollabEmail"].ToString();
            string collabAddressLine = table3.Rows[i]["CollabAddress"].ToString() + ", " + table3.Rows[i]["CollabWard"].ToString() + ", " + table3.Rows[i]["CollabDistrict"].ToString() + ", " + table3.Rows[i]["CollabCity"].ToString();
            string collabCreatedAt = ((DateTime)table3.Rows[i]["CollabRegisterAt"]).ToString("dd-MM-yyyy HH:mm:ss");
            string collabTaxNumber = table3.Rows[i]["CollabTaxNumber"].ToString();
            string collabTaxCreatedDate = ((table3.Rows[i]["CollabTaxCreatedDate"].ToString() != "") ? ((DateTime)table3.Rows[i]["CollabTaxCreatedDate"]).ToString("dd-MM-yyyy") : "");
            string collabTaxCreatedPlace = table3.Rows[i]["CollabTaxCreatedPlace"].ToString();
            string ownerThumb = Utilities.Account.getLinkThumbWithPath(table3.Rows[i]["OwnerThumbnail"].ToString());
            string ownerFullname = table3.Rows[i]["OwnerFullName"].ToString();
            string ownerCode = table3.Rows[i]["OwnerCode"].ToString();
            string ownerPhone = table3.Rows[i]["OwnerPhone"].ToString();
            string ownerEmail = table3.Rows[i]["OwnerEmail"].ToString();
            string ownerAddressLine = table3.Rows[i]["OwnerAddress"].ToString() + ", " + table3.Rows[i]["OwnerWard"].ToString() + ", " + table3.Rows[i]["OwnerDistrict"].ToString() + ", " + table3.Rows[i]["OwnerCity"].ToString();
            string ownerCreatedAt = ((DateTime)table3.Rows[i]["OwnerRegisterAt"]).ToString("dd-MM-yyyy HH:mm:ss");
            string ownerTaxNumber = table3.Rows[i]["OwnerTaxNumber"].ToString();
            string ownerTaxCreatedDate = ((table3.Rows[i]["OwnerTaxCreatedDate"].ToString() != "") ? ((DateTime)table3.Rows[i]["OwnerTaxCreatedDate"]).ToString("dd-MM-yyyy") : "");
            string ownerTaxCreatedPlace = table3.Rows[i]["OwnerTaxCreatedPlace"].ToString();
            string referralStsOwnerApproved = table3.Rows[i]["ReferralStsOwnerApproved"].ToString();
            string referralStsOwnerCanceled = table3.Rows[i]["ReferralStsOwnerCanceled"].ToString();
            string referralStsCollabApproved = table3.Rows[i]["ReferralStsCollabApproved"].ToString();
            string referralStsCollabDenied = table3.Rows[i]["ReferralStsCollabDenied"].ToString();
            string referralStsAdminApproved = table3.Rows[i]["ReferralStsAdminApproved"].ToString();
            string referralOwnerApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralOwnerCanceledAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerCanceledAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerCanceledAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabDeniedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabDeniedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabDeniedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralAdminApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralAdminApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralAdminApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCommissionFormat = Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralCommission"].ToString());

            string collabInfoText = "";
            collabInfoText += " data-thumbnail='" + collabThumb + "' ";
            collabInfoText += " data-fullname='"+ collabFullname + "' ";
            collabInfoText += " data-code='" + collabCode + "' ";
            collabInfoText += " data-phone='" + collabPhone + "' ";
            collabInfoText += " data-email='" + collabEmail + "' ";
            collabInfoText += " data-addr='" + collabAddressLine + "' ";
            collabInfoText += " data-register-at='" + collabCreatedAt + "' ";
            collabInfoText += " data-tax-number='" + collabTaxNumber + "' ";
            collabInfoText += " data-tax-created-date='" + collabTaxCreatedDate + "' ";
            collabInfoText += " data-tax-created-place='" + collabTaxCreatedPlace + "' ";

            string ownerInfoText = "";
            ownerInfoText += " data-thumbnail='" + ownerThumb + "' ";
            ownerInfoText += " data-fullname='" + ownerFullname + "' ";
            ownerInfoText += " data-code='" + ownerCode + "' ";
            ownerInfoText += " data-phone='" + ownerPhone + "' ";
            ownerInfoText += " data-email='" + ownerEmail + "' ";
            ownerInfoText += " data-addr='" + ownerAddressLine + "' ";
            ownerInfoText += " data-register-at='" + ownerCreatedAt + "' ";
            ownerInfoText += " data-tax-number='" + ownerTaxNumber + "' ";
            ownerInfoText += " data-tax-created-date='" + ownerTaxCreatedDate + "' ";
            ownerInfoText += " data-tax-created-place='" + ownerTaxCreatedPlace + "' ";

            string referralInfoText = "";
            referralInfoText += " data-referral-id='" + table3.Rows[i]["ReferralId"].ToString() + "' ";
            referralInfoText += " data-post-title='" + postTitle + "' ";
            referralInfoText += " data-post-url='"+ postUrl + "' ";
            referralInfoText += " data-post-code='" + postCode + "' ";
            referralInfoText += " data-post-created-at='"+ postCreatedAt + "' ";
            referralInfoText += " data-collab-thumbnail='" + collabThumb + "' ";
            referralInfoText += " data-collab-fullname='" + collabFullname + "' ";
            referralInfoText += " data-collab-code='" + collabCode + "' ";
            referralInfoText += " data-collab-phone='" + collabPhone + "' ";
            referralInfoText += " data-collab-email='" + collabEmail + "' ";
            referralInfoText += " data-owner-thumbnail='" + ownerThumb + "' ";
            referralInfoText += " data-owner-fullname='" + ownerFullname + "' ";
            referralInfoText += " data-owner-code='" + ownerCode + "' ";
            referralInfoText += " data-owner-phone='" + ownerPhone + "' ";
            referralInfoText += " data-owner-email='" + ownerEmail + "' ";
            referralInfoText += " data-referral-owner-approved-at='" + referralOwnerApprovedAtFormatDatetime + "' ";
            referralInfoText += " data-referral-owner-canceled-at='" + referralOwnerCanceledAtFormatDatetime + "' ";
            referralInfoText += " data-referral-collab-approved-at='" + referralCollabApprovedAtFormatDatetime + "' ";
            referralInfoText += " data-referral-collab-denied-at='" + referralCollabDeniedAtFormatDatetime + "' ";
            referralInfoText += " data-referral-admin-approved-at='" + referralAdminApprovedAtFormatDatetime + "' ";
            referralInfoText += " data-referral-commission-format='"+ referralCommissionFormat + "' ";

            html += @"<tr>";
            // post
            html += @"<td class='td data-col-account'>";
            html += @"<p><a href='/Admin/QuanTriTinDang/PostUpdate.aspx?idTinDang=" + table3.Rows[i]["PostId"].ToString() + "' class='' >" + table3.Rows[i]["PostTitle"].ToString() + "</a></p>";
            html += @"</td>";
            // post created at
            html += @"<td class='td data-col-account'>";
            html += "<p>Ngày đăng: " + ((DateTime)table3.Rows[i]["PostCreatedAt"]).ToString("dd/MM/yyyy") + "</p>";
            html += @"</td>";
            // owner
            html += @"<td class='td data-col-account'>";
            html += @"<a href='javascript:void(0);' class='user-info-btn' data-account-type='OWNER' data-account-id='" + table3.Rows[i]["ReferralOwnerId"].ToString() + "' " + ownerInfoText + " >" + (table3.Rows[i]["OwnerFullName"].ToString() != "" ? table3.Rows[i]["OwnerFullName"].ToString() : table3.Rows[i]["OwnerCode"].ToString()) + "</a>";
            html += @"<br />";
            html += @"<a href='tel:" + table3.Rows[i]["OwnerPhone"].ToString() + "'>(" + table3.Rows[i]["OwnerPhone"].ToString() + @")</a>";
            html += @"</td>";
            // collab
            html += @"<td class='td data-col-account'>";
            html += @"<a href='javascript:void(0);' class='user-info-btn' data-account-type='COLLAB' data-account-id='"+ table3.Rows[i]["ReferralCollabId"].ToString() + "' "+ collabInfoText + " >" + (table3.Rows[i]["CollabFullName"].ToString() != "" ? table3.Rows[i]["CollabFullName"].ToString() : table3.Rows[i]["CollabCode"].ToString()) + "</a>";
            html += @"<br />";
            html += @"<a href='tel:"+ table3.Rows[i]["CollabPhone"].ToString() + "'>("+ table3.Rows[i]["CollabPhone"].ToString() + @")</a>";
            html += @"</td>";
            // commission
            html += @"<td class='td data-col-ref-commission'>";
            html += @"<span>Tổng: "+ referralCommissionFormat + "</span>";
            html += @"<br />";
            html += @"<span>Tỉ lệ: " + Utilities.Formatter.toPercentString(table3.Rows[i]["ReferralRate"].ToString()) + "</span>";
            html += @"<br />";
            html += @"<span>Cố định: " + Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralAmount"].ToString()) + "</span>";
            html += @"</td>";
            // referral status
            html += @"<td class='td data-col-ref-status'>";
            string htmlMoreDetail = @"<p><a href='javascript:void(0);' class='btn-approve-referral' " + referralInfoText + " ><i class='fa fa-angle-double-right'></i> Thông tin thêm <i class='fa fa-angle-double-left'></i></a></p>";
            if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "1")
            {
                html += @"<p><label class='label label-danger text-label'>NĐB đã hủy trích hoa hồng</label></p>";
                html += htmlMoreDetail;
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<p><label class='label label-primary text-label'>NĐB đã xác nhận</label></p>";
                html += htmlMoreDetail;
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<p><label class='label label-info text-label'>CTV đã xác nhận</label></p>";
                html += @"<p><a href='javascript:void(0);' class='btn btn-success btn-sm btn-approve-referral' data-approve-button='1' " + referralInfoText + " ><i class='fa fa-angle-double-right'></i> Xác nhận cộng tác <i class='fa fa-angle-double-left'></i></a></p>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "1" && referralStsAdminApproved == "0")
            {
                html += @"<p><label class='label label-danger text-label'>CTV đã từ chối</label></p>";
                html += htmlMoreDetail;
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "1")
            {
                html += @"<p><label class='label label-success text-label'>Tung Tăng đã xác nhận</label></p>";
                html += htmlMoreDetail;
            }
            html += "</td>";
            // approve at
            html += "<td class='td data-col-datetime'>";
            if (!string.IsNullOrWhiteSpace(referralOwnerApprovedAtFormatDatetime))
            {
                html += "<div>NĐB xác nhận: " + referralOwnerApprovedAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabApprovedAtFormatDatetime))
            {
                html += "<div>CTV xác nhận: " + referralCollabApprovedAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralAdminApprovedAtFormatDatetime))
            {
                html += "<div>Tung Tăng xác nhận: " + referralAdminApprovedAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralOwnerCanceledAtFormatDatetime))
            {
                html += "<div>NĐB hủy trích hoa hồng: " + referralOwnerCanceledAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabDeniedAtFormatDatetime))
            {
                html += "<div>CTV đã từ chối: " + referralCollabDeniedAtFormatDatetime + "</div>";
            }
            html += "</td>";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
        if (totalItems > perPage)
        {
            htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "ReferralTableByPost.aspx", postParams);
            divPagination1.InnerHtml = htmlPagination;
            divPagination2.InnerHtml = htmlPagination;
        }
        else
        {
            divPagination1.Visible = false;
            divPagination2.Visible = false;
        }
    }
    protected void initStatusDropdownList()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Value", typeof(String)));
        dt.Columns.Add(new DataColumn("Text", typeof(String)));

        DataRow dr0 = dt.NewRow();
        dr0[0] = "";
        dr0[1] = "Tất cả";
        dt.Rows.Add(dr0);

        DataRow dr1 = dt.NewRow();
        dr1[0] = Referral.STATUS_OWNER_APPROVED;
        dr1[1] = "Người đăng bài đã trích hoa hồng";
        dt.Rows.Add(dr1);

        DataRow dr2 = dt.NewRow();
        dr2[0] = Referral.STATUS_COLLAB_APPROVED;
        dr2[1] = "Cộng tác viên đã xác nhận";
        dt.Rows.Add(dr2);

        DataRow dr3 = dt.NewRow();
        dr3[0] = Referral.STATUS_ADMIN_APPROVED;
        dr3[1] = "Tung Tăng đã xác nhận";
        dt.Rows.Add(dr3);

        DataView dv = new DataView(dt);
        drdlReferralStatus.DataSource = dv;
        drdlReferralStatus.DataTextField = "Text";
        drdlReferralStatus.DataValueField = "Value";
        drdlReferralStatus.DataBind();
    }
    protected void initAccountDropdownList()
    {
        string sql1 = @"
            SELECT
	            tb_ThanhVien.idThanhVien AS AccountId,
	            ISNULL(tb_ThanhVien.TenCuaHang, '') AS FullName,
	            tb_ThanhVien.TenDangNhap AS Username
            FROM tb_ThanhVien
        ";
        DataTable table1 = Connect.GetTable(sql1);

        DataTable dtCollab = new DataTable();
        DataTable dtOwner = new DataTable();
        dtCollab.Columns.Add(new DataColumn("Value", typeof(String)));
        dtCollab.Columns.Add(new DataColumn("Text", typeof(String)));
        dtOwner.Columns.Add(new DataColumn("Value", typeof(String)));
        dtOwner.Columns.Add(new DataColumn("Text", typeof(String)));

        DataRow dr0Collab = dtCollab.NewRow();
        dr0Collab[0] = "";
        dr0Collab[1] = "---Chọn---";
        dtCollab.Rows.Add(dr0Collab);
        DataRow dr0Owner = dtOwner.NewRow();
        dr0Owner[0] = "";
        dr0Owner[1] = "---Chọn---";
        dtOwner.Rows.Add(dr0Owner);

        for (int i = 0; i < table1.Rows.Count; i = i + 1)
        {
            DataRow dr1Collab = dtCollab.NewRow();
            dr1Collab[0] = table1.Rows[i]["AccountId"].ToString();
            dr1Collab[1] = (table1.Rows[i]["FullName"].ToString() != "" ? (table1.Rows[i]["FullName"].ToString() + " - ") : "" ) + "("+ table1.Rows[i]["Username"].ToString()+")";
            dtCollab.Rows.Add(dr1Collab);
            DataRow dr1Owner = dtOwner.NewRow();
            dr1Owner[0] = table1.Rows[i]["AccountId"].ToString();
            dr1Owner[1] = (table1.Rows[i]["FullName"].ToString() != "" ? (table1.Rows[i]["FullName"].ToString() + " - ") : "") + "(" + table1.Rows[i]["Username"].ToString() + ")";
            dtOwner.Rows.Add(dr1Owner);
        }

        DataView dvCollab = new DataView(dtCollab);
        drdlCollab.DataSource = dvCollab;
        drdlCollab.DataTextField = "Text";
        drdlCollab.DataValueField = "Value";
        drdlCollab.DataBind();
        DataView dvOwner = new DataView(dtOwner);
        drdlOwner.DataSource = dvOwner;
        drdlOwner.DataTextField = "Text";
        drdlOwner.DataValueField = "Value";
        drdlOwner.DataBind();
    }
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        string url = "/Admin/Posts/PostReferralTable.aspx?";
        url += "FilterPostTitle=" + this.txtPostTitle.Text;
        url += "&FilterCollab=" + this.drdlCollab.SelectedValue;
        url += "&FilterOwner=" + this.drdlOwner.SelectedValue;
        url += "&FilterReferralStatus=" + this.drdlReferralStatus.SelectedValue;
        url += "&FilterFromApprovedAt=" + this.txtApprovedAtFrom.Text;
        url += "&FilterToApprovedAt=" + this.txtApprovedAtTo.Text;
        url += "&FilterTypeApprovedAt=" + this.drdlTypeAprrovedAt.SelectedValue;
        url += "&FilterRangeReferralRate=" + txtReferralRate.Text;
        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text;
        url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text;
        Response.Redirect(url);
    }
    protected void PFake_ApproveReferral_Submit_Click(object sender, EventArgs e)
    {
        string referralId = PFake_ApproveReferral_ReferrralId.Text;
        string redirectUrl = PFake_ApproveReferral_RedirectUrl.Text;
        if (!string.IsNullOrWhiteSpace(referralId))
        {
            referralId = referralId.Trim();
        }
        if (!string.IsNullOrWhiteSpace(redirectUrl))
        {
            redirectUrl = redirectUrl.Trim();
        }
        Utilities.Referral.adminApproveReferral(referralId);
        Response.Redirect(redirectUrl);
    }
}