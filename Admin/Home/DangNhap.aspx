﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeFile="DangNhap.aspx.cs" Inherits="Home_DangNhap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin: Website Tung Tăng</title>
    <link rel="shortcut icon" type="../image/x-icon" href="../Images/Logo.png" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.2.0 -->
    <script src="../plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
    <script type="text/javascript">
        $(document).ready(function () {
            //alert('dfdsf');
            //$("#dvHeader").load("../header.html");
            //$("#dvMenu").load("../menu.html");
            $("#dvFooter").load("../footer.html");
        });
        function loadPage(href) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", href, false);
            xmlhttp.send();
            return xmlhttp.responseText;
        }
    </script>
    <style>
        #BigDiv {
            padding: 11% 34%;
        }
        @media (max-width:768px)
        {
        #BigDiv {
            padding: 0;
        }
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-image: url('../images/bg3.jpg'); background-size: 100%;">
    <form runat="server">
        <div>
            <!-- Content Wrapper. Contains page content -->
            <div id="BigDiv">

                <!-- Horizontal Form -->
                <div class="box box-info" style="background-color: #f1f1f1;">
                    <div class="box-header with-border">
                        <div style="color: #00c0ef; font-size: 25PX; font-weight: BOLD; text-align: CENTER; padding-bottom: 15PX;">ADMIN WEBSITE TUNG TĂNG</div>
                        <!--<h3 class="box-title" style="text-align:center">Đăng nhập hệ thống</h3>-->
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label" style="width: 130px;">Tên đăng nhập</label>

                            <div class="col-sm-10" style="width: 100%;">
                                <input type="text" class="form-control" id="txtTenDangNhap" runat="server" placeholder="Tên đăng nhập">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label" style="width: 130px;">Mật khẩu</label>

                            <div class="col-sm-10" style="width: 100%;">
                                <input type="password" class="form-control" id="txtMatKhau" runat="server" placeholder="Mật khẩu">
                            </div>
                        </div>
                        <%--<div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Nhớ mật khẩu
                      </label>
                    </div>
                  </div>
                </div>--%>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align: center; padding-right: 20px; background-color: #f1f1f1;">
                        <%--<button type="submit" class="btn btn-default">Hủy</button>--%>
                        <%--<button type="submit" class="btn btn-info pull-right">Đăng nhập</button>--%>
                        <asp:Button ID="btDangNhap" runat="server" class="btn btn-info pull-right" Text="Đăng nhập" OnClick="btDangNhap_Click" />
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <p style="color: #00c0ef; padding-left: 20PX; font-size: 20PX;display:none;">
                @ Được phát triển bởi <i><b><a href="https://xep.vn/">Công ty TNHH Phần Mềm XEP</a></b></i>
            </p>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane active" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript::;">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript::;">
                                    <h4 class="control-sidebar-subheading">Custom Template Design
                <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
              <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </form>
</body>
</html>

