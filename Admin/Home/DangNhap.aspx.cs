﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home_DangNhap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btDangNhap_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Default.aspx");
        string Username = txtTenDangNhap.Value.Trim();
        string Password = txtMatKhau.Value.Trim();
        if (Username == "")
        {
            Response.Write("<script>alert('Bạn chưa nhập tên đăng nhập !')</script>");
            return;
        }
        if (Password == "")
        {
            Response.Write("<script>alert('Bạn chưa nhập mật khẩu !')</script>");
            return;
        }
        string sqlCheckUsername = "select top 1 * from tb_Admin where TenDangNhap='" + StaticData.ValidParameter(Username) + "'";
        DataTable tbCheckUsername = Connect.GetTable(sqlCheckUsername);
        if (tbCheckUsername.Rows.Count > 0)
        {
            DataTable tbCheckPassword = Connect.GetTable("select top 1 * from tb_Admin where TenDangNhap='" + StaticData.ValidParameter(Username) + "' and MatKhau='" + StaticData.ValidParameter(Password) + "'");
            if (tbCheckPassword.Rows.Count > 0)
            {
                HttpCookie cookie_AdminTungTang_Login = new HttpCookie("AdminTungTang_Login", Username);
                cookie_AdminTungTang_Login.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_AdminTungTang_Login);
                Response.Redirect("../Home/Default.aspx");
            }
            else
            {
                Response.Write("<script>alert('Mật khẩu chưa đúng !')</script>");
                return;
            }
        }
        else
        {
            Response.Write("<script>alert('Tên đăng nhập chưa đúng !')</script>");
            return;
        }
    }
}