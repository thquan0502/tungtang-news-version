﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriTinDang_QuanLyTinDang : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sThoiHan = "";
    string sTinhTrang = "";
    string sTenDangNhap = "";
    string sisHot = "";
    string LinhVuc = "";
    string NguoiDuyet = "";
    string MT = "";
    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {

        //string sql = "select * from tb_TinDang";
        ////string sql = "select * from tb_TinDang where DuongDan = NULL"; 

        //DataTable table = Connect.GetTable(sql);

        //for (int i = 0; i < table.Rows.Count; i++)
        //{

        //    string sqlUpdateTD = "update tb_TinDang set DuongDan=N'" + StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(table.Rows[i]["TieuDe"].ToString().Trim())) + "' where idTinDang='" + table.Rows[i]["idTinDang"] + "'";
        //    bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
        //}


        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {

            //Đăng tin không cần đăng nhập
            Response.Redirect("../Home/DangNhap.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            LoadLinhVuc();
           
            try
            {
                if (Request.QueryString["TenDangNhap"].Trim() != "")
                {
                    sTenDangNhap = Request.QueryString["TenDangNhap"].Trim();
                    txtTenDangNhap.Value = sTenDangNhap;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["LinhVuc"].Trim() != "")
                {
                    LinhVuc = Request.QueryString["LinhVuc"].Trim();
                    slLinhVuc.Value = LinhVuc;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["ThoiHan"].Trim() != "")
                {
                    sThoiHan = Request.QueryString["ThoiHan"].Trim();
                    slHan.Value = sThoiHan;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["TinhTrang"].Trim() != "")
                {
                    sTinhTrang = Request.QueryString["TinhTrang"].Trim();
                    slTinhTrang.Value = sTinhTrang;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["isHot"].Trim() != "")
                {
                    sisHot = Request.QueryString["isHot"].Trim();
                    slisHot.Value = sisHot;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["MT"].Trim() != "")
                {
                    MT = Request.QueryString["MT"].Trim();
                    txtMaTin.Value = MT;
                }
            }
            catch { }

            LoadNguoiDuyet();
            try
            {
                if (Request.QueryString["ND"].Trim() != "")
                {
                    NguoiDuyet = Request.QueryString["ND"].Trim();
                    slNguoiDuyet.Value = NguoiDuyet;
                }
            }
            catch { }
            LoadTinDang();
        }
    }
    #region paging
    private void SetPage()
    {
        string sql = "select count(idTinDang) from tb_TinDang td left join tb_ThanhVien tv on td.idThanhVien=tv.idThanhVien where '1'='1'";
        if (sTenDangNhap != "")
            sql += " and TenDangNhap = '" + sTenDangNhap + "'";
        if (MT != "")
            sql += " and MaTinDang like '%" + MT + "%'";
        if (NguoiDuyet != "")
            sql += " and idAdmin_Duyet = '" + NguoiDuyet + "'";
        if (sisHot != "")
        {
            if (sisHot == "False")
                sql += " and (isHot = '" + sisHot + "' or isHot is Null)";
            else
                sql += " and isHot = '" + sisHot + "'";
        }

        if (LinhVuc != "")
            sql += " and idDanhMucCap1= '" + LinhVuc + "'";
        if (sTuNgay != "")
            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (sThoiHan != "")
        {
            if (sThoiHan == "2")
                sql += " and isHetHan is Null";
            else
                sql += " and isHetHan = '" + sThoiHan + "'";
        }
        if (sTinhTrang != "")
        {
            if (sTinhTrang == "2")
                sql += " and isDuyet is Null";
            else
                sql += " and isDuyet = '" + sTinhTrang + "'";
        }
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    #endregion
    void LoadLinhVuc()
    {
        string sql = "select * from tb_DanhMucCap1 order by sothutu";
        DataTable table = Connect.GetTable(sql);
        slLinhVuc.DataSource = table;
        slLinhVuc.DataTextField = "TenDanhMucCap1";
        slLinhVuc.DataValueField = "idDanhMucCap1";
        slLinhVuc.DataBind();

        slLinhVuc.Items.Add(new ListItem("-- Chọn --", ""));
        slLinhVuc.Items.FindByText("-- Chọn --").Selected = true;
    }
    void LoadNguoiDuyet()
    {
        string sql = "select * from tb_Admin where '1'='1' order by HoTen";
        DataTable table = Connect.GetTable(sql);
        slNguoiDuyet.DataSource = table;
        slNguoiDuyet.DataTextField = "TenDangNhap";
        slNguoiDuyet.DataValueField = "idAdmin";
        slNguoiDuyet.DataBind(); 
        slNguoiDuyet.Items.Add(new ListItem("-- Tất cả --", ""));
        slNguoiDuyet.Items.FindByText("-- Tất cả --").Selected = true;
    }
    private void LoadTinDang()
    {
        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY idTinDang desc
                  )AS RowNumber
	              ,td.*,TenThanhVien=tv.TenCuaHang,tv.TenDangNhap
                  FROM tb_TinDang td left join tb_ThanhVien tv on td.idThanhVien=tv.idThanhVien where '1'='1'
            ";
        if (sTenDangNhap != "")
            sql += " and TenDangNhap = '" + sTenDangNhap + "'";
        if (MT != "")
            sql += " and MaTinDang like '%" + MT + "%'";
        if (NguoiDuyet != "")
            sql += " and idAdmin_Duyet = '" + NguoiDuyet + "'";
        if (sisHot != "")
        {
            if (sisHot == "False")
                sql += " and (isHot = '" + sisHot + "' or isHot is Null)";
            else
                sql += " and isHot = '" + sisHot + "'";
        }
        if (sTuNgay != "")
            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (LinhVuc != "")
            sql += " and idDanhMucCap1= '" + LinhVuc + "'";
        if (sThoiHan != "")
        {
            if (sThoiHan == "2")
                sql += " and isHetHan is Null";
            else
                sql += " and isHetHan = '" + sThoiHan + "'";
        }
        if (sTinhTrang != "")
        {
            if (sTinhTrang == "2")
                sql += " and isDuyet is Null";
            else
                sql += " and isDuyet = '" + sTinhTrang + "'";
        }

        sql += ") as tb1 WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";


        DataTable table = Connect.GetTable(sql);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();
        SetPage();
        string html = @"<table class='table table-bordered table-striped' id='myTable' style='width: 1300px'>
                            <tr>
                                <th class='th' style='white-space: nowrap;'>
                                    STT
                                </th>
                                <th class='th' >
                                    Lên TOP
                                </th>
                                <th class='th' >
                                    Ngày đăng
                                </th>
                                <th class='th' >
                                    Ngày lên TOP
                                </th>
                                <th class='th' >
                                   Mã tin
                                </th>
                                <th class='th' >
                                    Tên thành viên
                                </th>
                                <th class='th' >
                                    Tên đăng nhập
                                </th>
                                <th class='th' >
                                    Lĩnh vực
                                </th>
                                <th class='th' style='white-space: nowrap;'>Tiêu đề</th>
                                <th class='th' style='white-space: nowrap;'>
                                    isHot
                                </th>
                                <th class='th' >
                                    Người duyệt
                                </th>
                                <th class='th' style='width:80px;display:none;'></th>
                                <th class='th' style='width:110px;display:none;'></th>
                                <th class='th' style='white-space: nowrap;'>
                                    Lý do ko duyệt
                                </th>
                                <th class='th' style='width:110px' ></th>
                                <th class='th' style='width:70px' ></th>
								<th class='th' style='width:60px;'></th>
                            </tr>";
        string url1 = "";
        if (LinhVuc != "")
            url1 += "LinhVuc=" + LinhVuc + "&";
        if (sTuNgay != "")
            url1 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url1 += "DenNgay=" + sDenNgay + "&";
        if (sThoiHan != "")
            url1 += "ThoiHan=" + sThoiHan + "&";
        if (sTinhTrang != "")
            url1 += "TinhTrang=" + sTinhTrang + "&";
        if (sTenDangNhap != "")
            url1 += "TenDangNhap=" + sTenDangNhap + "&";
        if (NguoiDuyet != "")
            url1 += "ND=" + NguoiDuyet + "&";
        if (sisHot != "")
            url1 += "isHot=" + sisHot + "&";
        if (MT != "")
            url1 += "MT=" + MT + "&";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += " <tr>";
            html += "       <td>" + (((Page - 1) * PageSize) + i + 1).ToString() + "</td>";
            html += "       <td><a href='javascript:LenTop(" + table.Rows[i]["idTinDang"] + ");' title='Lên TOP'><i class='fa fa-arrow-circle-up'></i> TOP</a></td>";
            if (table.Rows[i]["NgayDang"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";

        

            if (table.Rows[i]["NgayDayLenTop"].ToString().ToString() != "")
            {
                if (table.Rows[i]["isDuyet"].ToString() == "True")
                {
                    html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayDayLenTop"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                }
                else
                {
                    html += "   <td></td>";
                }
            }
            else
                html += "   <td></td>";
            
            
            html += "       <td>" + table.Rows[i]["MaTinDang"] + "</td>";
            html += "       <td>" + table.Rows[i]["TenThanhVien"] + "</td>";
            html += "       <td>" + table.Rows[i]["TenDangNhap"] + "</td>";
            html += "       <td>" + StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", table.Rows[i]["idDanhMucCap1"].ToString()) + "</td>";
            html += "       <td style='width: 400px'><a style='cursor:pointer;' href='QuanLyTinDang-CapNhat.aspx?idTinDang=" + table.Rows[i]["idTinDang"].ToString() + "&" + url1 + "'>" + table.Rows[i]["TieuDe"].ToString() + "</a></td>";
            if (table.Rows[i]["isHot"].ToString() == "True")
                html += "<td><input type='checkbox' disabled checked/></td>";
            else
                html += "<td><input type='checkbox' disabled/></td>";
            html += "       <td>" + StaticData.getField("tb_Admin", "TenDangNhap", "idAdmin", table.Rows[i]["idAdmin_Duyet"].ToString()) + "</td>";

            html += "       <td style='width: 400px'>" + table.Rows[i]["LyDo_KhongDuyet"] + "</td>";
            html += "       <td style='display:none;'>";
            html += "           <select id='slHetHan_" + table.Rows[i]["idTinDang"] + "' disabled='disabled' class='form-control' style='width:122px'>";
            if (table.Rows[i]["isHetHan"].ToString() != "True")
            {
                html += "           <option value='ConHieuLuc' selected='selected'>Còn hạn</option>";
                html += "           <option value='HetHan'>Hết hạn</option>";
            }
            else
            {
                html += "           <option value='ConHieuLuc'>Còn hạn</option>";
                html += "           <option value='HetHan' selected='selected'>Hết hạn</option>";
            }
            html += "           </select>";
            html += "         </td>";
            html += "   <td style='display:none;'><a id='btHetHan_" + table.Rows[i]["idTinDang"] + "' style='cursor:pointer' onclick='CapNhatHetHan(\"" + table.Rows[i]["idTinDang"] + "\")'><img class='imgedit' src='../images/edit.png' style='width:25px; cursor:pointer'/>Cập nhật hạn</a></td>";

            html += "       <td><select id='slDuyet_" + table.Rows[i]["idTinDang"] + "' disabled='disabled' class='form-control' style='width:122px'>";
            if (table.Rows[i]["isDuyet"].ToString() == "")
                html += "<option value='' selected='selected'>Đang chờ...</option>";
            else
                html += "<option value=''>Đang chờ...</option>";
            if (table.Rows[i]["isDuyet"].ToString() == "True")
                html += "<option value='Duyet' selected='selected'>Duyệt</option>";
            else
                html += "<option value='Duyet'>Duyệt</option>";
            if (table.Rows[i]["isDuyet"].ToString() == "False")
                html += "<option value='KhongDuyet' selected='selected'>Không duyệt</option>";
            else
                html += "<option value='KhongDuyet'>Không duyệt</option>";
            html += "       </select></td>";
            html += "   <td><a id='btDuyet_" + table.Rows[i]["idTinDang"].ToString() + "' style='cursor:pointer' onclick='DuyetTinDang(\"" + table.Rows[i]["idTinDang"].ToString() + "\")'><img class='imgedit' src='../images/edit.png' style='width:25px; cursor:pointer'/>Sửa</a></td>";
            html += "   <td><a style='cursor:pointer'  onclick='DeleteTinDang(\"" + table.Rows[i]["idTinDang"].ToString() + "\")'> <img class='imgedit' src='../images/delete.png' />Xóa</a>";
			html += "       </tr>";
        }
        html += "   <tr>";
        html += "       <td colspan='20' class='footertable'>";
        string url = "QuanLyTinDang.aspx?";
        if (LinhVuc != "")
            url += "LinhVuc=" + LinhVuc + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";
        if (sThoiHan != "")
            url += "ThoiHan=" + sThoiHan + "&";
        if (sTinhTrang != "")
            url += "TinhTrang=" + sTinhTrang + "&";
        if (sTenDangNhap != "")
            url += "TenDangNhap=" + sTenDangNhap + "&";
        if (NguoiDuyet != "")
            url += "ND=" + NguoiDuyet + "&";
        if (sisHot != "")
            url += "isHot=" + sisHot + "&";
        if (MT != "")
            url += "MT=" + MT + "&";

        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvTinDang.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string LinhVuc = slLinhVuc.Value.Trim();
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string ThoiHan = slHan.Value.Trim();
        string TinhTrang = slTinhTrang.Value.Trim();
        string TenDangNhap = txtTenDangNhap.Value.Trim();
        string isHot = slisHot.Value.Trim();
        string NguoiDuyet = slNguoiDuyet.Value.Trim();
        string MT = txtMaTin.Value.Trim();
        string url = "QuanLyTinDang.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (ThoiHan != "0")
            url += "ThoiHan=" + ThoiHan + "&";
        if (TinhTrang != "-1")
            url += "TinhTrang=" + TinhTrang + "&";
        if (TenDangNhap != "")
            url += "TenDangNhap=" + TenDangNhap + "&";
        if (MT != "")
            url += "MT=" + MT + "&";
        if (NguoiDuyet != "")
            url += "ND=" + NguoiDuyet + "&";
        if (LinhVuc != "")
            url += "LinhVuc=" + LinhVuc + "&";
        if (isHot != "-1")
            url += "isHot=" + isHot + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "QuanLyTinDang.aspx";
        Response.Redirect(url);
    }
}