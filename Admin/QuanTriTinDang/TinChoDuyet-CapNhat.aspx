﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="TinChoDuyet-CapNhat.aspx.cs" Inherits="Admin_QuanTriTinDang_QuanLyTinDang_CapNhat" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        
        window.onload = function () {
            if (document.getElementById('ContentPlaceHolder1_hdHinhAnh').value != "")
                window.scrollTo(0, document.body.scrollHeight);
            document.getElementById("search-widget-wrapper").style.display = "none";
            
        }
        $(function () {
            CKEDITOR.config.extraPlugins = 'justify';
        });
        function format_curency(a) {
            var money = a.value.replace(/\./g, "");
            a.value = money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
        function XoaHinhAnh(id, TenAnh) {
            document.getElementById(id).style.display = "none";
            document.getElementById("ContentPlaceHolder1_hdHinhAnh").value = document.getElementById("ContentPlaceHolder1_hdHinhAnh").value.replace(TenAnh + "|~~~~|", "");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <div class="title" id="dvTitle" runat="server">XEM CHI TIẾT TIN ĐĂNG</div>
            <div class="title1"><div id="linkback" runat="server"></div> <a href="TinChoDuyet.aspx"><i class="fa fa-step-backward"></i>Danh sách tin đăng</a></div>
            <!-- Main content -->
            <section class="content">
                <div class="box">
                    <div class="row">
                        <!--Top Jobs-->
                        <div class="col-md-12">
                            <div class="top-job">
                                <div class="panel jobs-board-listing with-mc no-padding no-border">
                                    <div class="panel-content" id="tabChoThue" style="padding: 10px">
                                        <div class="job-list scrollbar m-t-lg">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Lĩnh vực <span style="color: red">(*)</span>:
                                                                </td>
                                                                <td>
                                                                    <%--<select onchange="ClickDanhMuc();" required="required" data-search-input-placeholder="Tìm kiếm danh mục" data-placeholder="Danh mục" class="form-control" id="slDanhMucCap1" name="industry[]" runat="server">
                                                             </select>--%>
                                                                    <asp:DropDownList ID="ddlLoaiDanhMuc" class="form-control" runat="server"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Tỉnh/ thành:
                                                                </td>
                                                                <td>
                                                                    <%--<select onchange="ClickTinhThanh()" data-search-input-placeholder="Tìm kiếm tỉnh" data-placeholder="" class="form-control" id="slTinh" name="industry[]" runat="server">
                                                            </select>--%>
                                                                    <asp:DropDownList ID="ddlTinh" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTinh_SelectedIndexChanged"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Giá:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtTuGia" oninput="format_curency(this);" style="width: 150px" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                </td>
                                                                <td style="width: 10px;">
                                                                </td>
                                                                <td style="display: none;">
                                                                    <input type="text" id="txtDenGia" oninput="format_curency(this);" style="width: 150px" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Quận/ huyện:
                                                                </td>
                                                                <td>
                                                                    <select data-search-input-placeholder="Tìm kiếm huyện" data-placeholder="Tất cả" class="form-control" id="slHuyen" name="industry[]" runat="server" disabled="disabled">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Mã tin đăng:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtMaTinDang" style="width: 150px" class="form-control" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Duyệt tin:
                                                                </td>
                                                                <td>
                                                                    <select onchange="thaydoi()" type="text" id="slDuyet" style="width: 150px" class="form-control" runat="server">
                                                                        <option value="">Đang chờ...</option>
                                                                        <option value="Duyet">Duyệt</option>
                                                                        <option value="KhongDuyet">Không duyệt</option>
                                                                    </select>
                                                                        
                                                                    <script>
                                                                        function thaydoi(){
                                                                            var id = $("#ContentPlaceHolder1_slDuyet").val();
                                                                            if (id == "KhongDuyet") {
                                                                                $("#anc").css("display", "");
                                                                            }
                                                                            else {
                                                                                $("#anc").css("display", "none");
                                                                            }
                                                                        }
                                                                    </script>
                                                                </td>
                                                                <td id="anc">
                                                                    <textarea id="txtLyDoKhongDuyet" placeholder="Lý do không duyệt" rows="3" style="width: 100%;background-color: #f3e3e3;color: #000;" runat="server"></textarea>
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="display: none;">
                                                <div class="col-md-10">
                                                    <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">THÔNG TIN LIÊN HỆ</div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="display: none;">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Họ tên <span style="color: red">(*)</span>:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtHoTen" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                    <div id="dvHoTen" style="color: red;" runat="server"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Email:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtEmail" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                    <div id="dvEmail" style="color: red;" runat="server"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="display: none;">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Số điện thoại <span style="color: red">(*)</span>:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtSoDienThoai" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                    <div id="dvSoDienThoai" style="color: red;" runat="server"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="display: none;">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin">Địa chỉ:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtDiaChi" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">NỘI DUNG TIN ĐĂNG</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin12">Tiêu đề <span style="color: red">(*)</span>:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtTieuDe" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                    <div id="dvTieuDe" style="color: red;" runat="server"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
											
											  <div class="col-md-12">
                                                <div class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin12">Đường dẫn <span style="color: red">(*)</span>:
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="txtDuongDan" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                    <div id="dvDuongDan" style="color: red;" runat="server"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
											
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div style="height: auto">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin12" style="vertical-align: baseline;">Nội dung <span style="color: red">(*)</span>:
                                                                </td>
                                                                <td>

                                                                    <textarea id="txtNoiDung" runat="server" style="width: 100%; height: 300px;"></textarea>
                                                                    <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div id="dvConHinhAnh" class="form-group">
                                                    <div>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin12">Thêm hình ảnh:
                                                                </td>
                                                                <td>
                                                                    <div class="BgUploadAnh">
                                                                        <div>
                                                                            <asp:FileUpload ID="fileHinhAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" AllowMultiple="true" Style="display: none;" />
                                                                        </div>
                                                                        <div style="padding: 10px 0px; height: auto; overflow: hidden; display: -webkit-inline-box;" id="dvHinhAnh" runat="server">
                                                                            <%--<div class="imgupload">
                                                                        <p style="margin:0px"><img id="imgAnhCuaBan" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                                        <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                                                    </div>
                                                                    <div class="imgupload">
                                                                        <p style="margin:0px"><img id="img1" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                                        <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                                                    </div>--%>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group" style="">
                                                    <div style="height: auto">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin12" style="vertical-align: baseline;">
                                                                    <label for="ContentPlaceHolder1_ckisHot">isHot</label>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" id="ckisHot" runat="server" />
                                                                    <label for="ContentPlaceHolder1_ckisHot">isHot</label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group" style="">
                                                    <div style="height: auto">
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td class="nametdadmin12" style="vertical-align: baseline;">
                                                                     <a class="btn btn-primary btn-block" onclick="isDuyet1()">Sửa tin</a>
                                                                </td>
                                                                <td>
                                                                   
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Buttons-->
                                            <div class="form-group">
                                                                                   
                                                    <asp:Button ID="btDangTin" runat="server" Text="SỬA TIN" class="btn btn-primary btn-block" Style="display:none; width: 110px; padding: 10px; margin: auto;" OnClick="btDangTin_Click" />
                                                    
                                                <script>
                                                    
                                                    function isDuyet1() {
                                                        debugger
                                                        var id = $("#ContentPlaceHolder1_slDuyet").val();
                                                        if (id == "KhongDuyet") {
                                                            if ($("#ContentPlaceHolder1_txtLyDoKhongDuyet").val() == "") {
                                                                alert("Vui lòng nhập lý do");
                                                            }
                                                            else {
                                                                document.getElementById("ContentPlaceHolder1_btDangTin").click();
                                                            }
                                                        }
                                                        else {
                                                            document.getElementById("ContentPlaceHolder1_btDangTin").click();
                                                        }
                                                    }
                                                </script>
                                            </div>
                                             
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>
                           
                        </div>
                    </div>
                    <input type="hidden" id="hdHinhAnh" runat="server" />
                </div>
        
            </section>
            <div class="modal-ThongKe-cls">
                <div id="modalThongKe" class="modal" style="overflow: hidden;">
                    <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">Lý do không duyệt bài đăng</div>
                    
                    <a id="btnLuuLyDo" class="btn btn-primary btn-flat" style="float:right;">Lưu</a>
                </div>
            </div>
        </div>
    </form>
    <script>
        var id = $("#ContentPlaceHolder1_slDuyet").val();
        if (id == "KhongDuyet") {
            $("#anc").css("display", "");
        }
        else {
            $("#anc").css("display", "none");
        }
    </script>
</asp:Content>
