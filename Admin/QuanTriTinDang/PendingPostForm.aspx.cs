﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class Admin_QuanTriTinDang_QuanLyTinDang_CapNhat : System.Web.UI.Page
{
    string idTinDang = "";
    string idadmin = "";
    string page = "";
    string sTenDangNhap = "";
    string MT = "";
    string NguoiDuyet = "";
    string sTuNgay = "";
    string sDenNgay = "";
    string LinhVuc = "";
    string sTinhTrang = "";
    protected Models.Referral referralHelper = new Models.Referral();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {
            
            //Đăng tin không cần đăng nhập
            Response.Redirect("../Home/DangNhap.aspx");
        }
        else
        {
            string x = Request.Cookies["AdminTungTang_Login"].Value.Trim().ToString();
            idadmin = StaticData.getField("tb_admin", "idAdmin", "TenDangNhap", x);
            //idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        try
        {
            idTinDang = Request.QueryString["idTinDang"].Trim();
        }
        catch { }
        if (!IsPostBack)
        {
            
            LoadDanhMucCap1();
			// LoadDanhMucCap2();
            LoadTinh();
            LoadTinDang();
			LoadLiDo();
            this.loadReferral(idTinDang);
        }
        if (IsPostBack && fileHinhAnh.PostedFile != null)
        {
			//if (slTinhTrang_.Value == "1")
            //{
            //    slTinhTrang_.Value = "1";
            //}
            //else
            //{
               
            //    if (txtLyDoKD.Value.ToString() != "")
            //    {
            //        slTinhTrang_.Value = "0";
            //    }
            //    else
            //        slTinhTrang_.Value = "2";
            //}
            LoadLiDo();
            if (Request.Files.Count > 10)
            {
                Response.Write("<script>alert('Bạn vui lòng chọn tối đa 10 ảnh!')</script>");
                return;
            }
            for (int j = 0; j < Request.Files.Count; j++)
            {
                HttpPostedFile file = Request.Files[j];
                if (file.ContentLength > 0)
                {
                    //if (fileHinhAnh.PostedFile.FileName.Length > 0)
                    //{
                    string extension = Path.GetExtension(file.FileName);
                    if (extension.ToUpper() == ".JPG" || extension.ToUpper() == ".JPEG" || extension.ToUpper() == ".BMP" || extension.ToUpper() == ".GIF" || extension.ToUpper() == ".PNG")
                    {
                        if (fileHinhAnh.HasFile)
                        {
                            string Ngay = DateTime.Now.Day.ToString();
                            string Thang = DateTime.Now.Month.ToString();
                            string Nam = DateTime.Now.Year.ToString();
                            string Gio = DateTime.Now.Hour.ToString();
                            string Phut = DateTime.Now.Minute.ToString();
                            string Giay = DateTime.Now.Second.ToString();
                            string Khac = DateTime.Now.Ticks.ToString();
                            string fExtension = Path.GetExtension(file.FileName);

                            string sqlIdTinDang = "select top 1 idTinDang from tb_TinDang order by idTinDang desc";
                            DataTable tbIdTinDang = Connect.GetTable(sqlIdTinDang);
                            string idTinDang = "0";
                            if (tbIdTinDang.Rows.Count > 0)
                                idTinDang = (float.Parse(tbIdTinDang.Rows[0]["idTinDang"].ToString()) + 1).ToString();
                            string FileName = Ngay + Thang + Nam + Gio + Phut + Giay + Khac + idTinDang + fExtension;
                            string FilePath = "/Images/td/slides/" + FileName;
                            file.SaveAs(Server.MapPath(FilePath));

                            hdHinhAnh.Value = hdHinhAnh.Value + FileName + "|~~~~|";
                            string htmlHinhAnh = "";
                            string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                            for (int i = 0; i < arrHinhAnh.Length; i++)
                            {
                                if (arrHinhAnh[i].Trim() != "")
                                {
                                    htmlHinhAnh += "<div id='dvHinhAnh_" + arrHinhAnh[i].Trim() + "' class='imgupload' style='width:150px'>";
                                    htmlHinhAnh += "<p style='margin:0px'><img src='/Images/td/slides/" + arrHinhAnh[i].Trim() + "' style='width: 150px;height: 110px;' /></p>";
                                    //htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + arrHinhAnh[i].Trim() + "\",\"" + arrHinhAnh[i].Trim() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                                    htmlHinhAnh += "</div>";
                                }
                            }
                            dvHinhAnh.InnerHtml = htmlHinhAnh;
                            //imgAnhCuaBan.Src = FilePath;
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Bạn vui lòng chọn file là hình ảnh!')</script>");
                        return;
                    }
                }
            }
            //Response.Write("<script>window.scrollTo(0, document.body.scrollHeight);</script>");
        }

    }
    private void LoadTinDang()
    {
        if (idTinDang != "")
        {
            btDangTin.Text = "SỬA TIN";
            string sqlTinDang = @"
                SELECT 
	                TBPosts.MaTinDang AS MaTinDang,
	                TBPosts.idDanhMucCap1 AS idDanhMucCap1,
					TBPosts.idDanhMucCap2 AS idDanhMucCap2,
	                TBPosts.idTinh AS idTinh,
	                TBPosts.isDuyet AS isDuyet,
	                TBPosts.LyDo_KhongDuyet AS LyDo_KhongDuyet,
	                TBPosts.idHuyen AS idHuyen,
					TBPosts.idPhuongXa AS idPhuongXa,
	                TBPosts.TuGia AS TuGia,
	                TBPosts.DenGia AS DenGia,
	                TBPosts.HoTen AS HoTen,
	                TBPosts.SoDienThoai AS SoDienThoai,
	                TBPosts.Email AS Email,
	                TBPosts.DiaChi AS DiaChi,
	                TBPosts.TieuDe AS TieuDe,
	                TBPosts.NoiDung AS NoiDung,
	                TBPosts.DuongDan AS DuongDan,
	                TBPosts.isHot AS isHot,
					TBOwners.StsPro AS StsPro,
                    ISNULL(TBPosts.Commission, 0) AS PostCommission,
                    TBPosts.Code AS PostCode,
	                ISNULL(TBOwners.TenCuaHang, '') AS OwnerFullname,
	                ISNULL(TBOwners.Code, '') AS OwnerCode,
	                ISNULL(TBOwners.SoDienThoai, '') AS OwnerPhone,
	                ISNULL(TBOwners.Email, '') AS OwnerEmail,
	                TBOwners.NgayDangKy OwnerCreatedAt,
	                ISNULL(TBOwners.DiaChi, '') AS OwnerAddress,
	                ISNULL(TBOwnerCities.Ten, '') AS OwnerCityName,
	                ISNULL(TBOwnerDistricts.Ten, '') AS OwnerDistrictName,
	                ISNULL(TBOwnerWards.Ten, '') AS OwnerWardName
                FROM tb_TinDang TBPosts 
	                LEFT JOIN tb_DanhMucCap1 TBCategories1 
		                ON TBPosts.idDanhMucCap1 = TBCategories1.idDanhMucCap1 
					LEFT JOIN tb_DanhMucCap2 TBCategories2 
		                ON TBPosts.idDanhMucCap2 = TBCategories2.idDanhMucCap2 
	                INNER JOIN tb_ThanhVien TBOwners
		                ON TBPosts.idThanhVien = TBOwners.idThanhVien
	                LEFT JOIN City TBOwnerCities
		                ON TBOwners.idTinh = TBOwnerCities.id
	                LEFT JOIN District TBOwnerDistricts
		                ON TBOwners.idHuyen = TBOwnerDistricts.id
	                LEFT JOIN tb_PhuongXa TBOwnerWards
		                ON TBOwners.idPhuongXa = TBOwnerWards.id
                WHERE TBPosts.idTinDang=" + idTinDang + @"
            ";
            DataTable tbTinDang = Connect.GetTable(sqlTinDang);
            if (tbTinDang.Rows.Count > 0)
            {
                txtMaTinDang.Value = tbTinDang.Rows[0]["PostCode"].ToString();
                ddlLoaiDanhMuc.SelectedValue = tbTinDang.Rows[0]["idDanhMucCap1"].ToString();
				// ddlLoaiDanhMuc2.SelectedValue = tbTinDang.Rows[0]["idDanhMucCap2"].ToString();
                ddlTinh.SelectedValue = tbTinDang.Rows[0]["idTinh"].ToString();

                if (tbTinDang.Rows[0]["isDuyet"].ToString().Trim() == "True")
                {
                    slTinhTrang_.Value = "1";
                }
                   
                else if (tbTinDang.Rows[0]["isDuyet"].ToString().Trim() == "False")
                {
                    slTinhTrang_.Value = "0";
                    if (tbTinDang.Rows[0]["LyDo_KhongDuyet"].ToString() != "")
                    {
                        string LyDo = tbTinDang.Rows[0]["LyDo_KhongDuyet"].ToString();
                    string LyDos = LyDo.Replace("|", "<br />");
                    ListLyDo.InnerHtml = LyDos;
                    ListLyDo.Style.Add("display", "");
                    }

                }
                else
                {
                    slTinhTrang_.Value = "2";

                }
				 if (tbTinDang.Rows[0]["idDanhMucCap2"].ToString().Trim() != "")
                {
                    SlDanhMuc2.Disabled = false;
                    string sqlDM = "select * from tb_DanhMucCap2 where idDanhMucCap1='" + tbTinDang.Rows[0]["idDanhMucCap1"].ToString() + "'  order by SoThuTu";
                    SlDanhMuc2.DataSource = Connect.GetTable(sqlDM);
                    SlDanhMuc2.DataTextField = "TenDanhMucCap2";
                    SlDanhMuc2.DataValueField = "idDanhMucCap2";
                    SlDanhMuc2.DataBind();
                    SlDanhMuc2.Items.Add(new ListItem("Tất cả", "0"));
                    SlDanhMuc2.Value = tbTinDang.Rows[0]["idDanhMucCap2"].ToString();
                }
				
                if (tbTinDang.Rows[0]["idHUyen"].ToString().Trim() != "")
                {
                    slHuyen.Disabled = false;
                    string sqlHuyen = "select * from District where idTinhTP='" + tbTinDang.Rows[0]["idTinh"].ToString() + "'  order by ten asc";
                    slHuyen.DataSource = Connect.GetTable(sqlHuyen);
                    slHuyen.DataTextField = "ten";
                    slHuyen.DataValueField = "id";
                    slHuyen.DataBind();
                    slHuyen.Items.Add(new ListItem("Tất cả", "0"));
                    slHuyen.Value = tbTinDang.Rows[0]["idHuyen"].ToString();
                 }
				
				 if (tbTinDang.Rows[0]["idPhuongXa"].ToString().Trim() != "")
                {
                    slXa.Disabled = false;
                    string sqlXa = "select * from tb_PhuongXa where idQuanHuyen='" + tbTinDang.Rows[0]["idHuyen"].ToString() + "'  order by ten asc";
                    slXa.DataSource = Connect.GetTable(sqlXa);
                    slXa.DataTextField = "ten";
                    slXa.DataValueField = "id";
                    slXa.DataBind();
                    slXa.Items.Add(new ListItem("Tất cả", "0"));
                    slXa.Value = tbTinDang.Rows[0]["idPhuongXa"].ToString();
                }

                if (tbTinDang.Rows[0]["TuGia"].ToString() != "")
                    txtTuGia.Value = double.Parse(tbTinDang.Rows[0]["TuGia"].ToString()).ToString("#,##").Replace(",", ".");
                if (tbTinDang.Rows[0]["DenGia"].ToString() != "")
                    txtDenGia.Value = double.Parse(tbTinDang.Rows[0]["DenGia"].ToString()).ToString("#,##").Replace(",", ".");

                txtHoTen.Value = tbTinDang.Rows[0]["HoTen"].ToString();
                txtSoDienThoai.Value = tbTinDang.Rows[0]["SoDienThoai"].ToString();
                txtEmail.Value = tbTinDang.Rows[0]["Email"].ToString();
                txtDiaChi.Value = tbTinDang.Rows[0]["DiaChi"].ToString();
                txtTieuDe.Value = tbTinDang.Rows[0]["TieuDe"].ToString();
                txtNoiDung.Value = tbTinDang.Rows[0]["NoiDung"].ToString().Trim();
				txtDuongDan.Value = tbTinDang.Rows[0]["DuongDan"].ToString();
                txtCommission.Value = tbTinDang.Rows[0]["PostCommission"].ToString();

                spanOwnerFullname.InnerText = tbTinDang.Rows[0]["OwnerFullname"].ToString() == "" ? "(Chưa cung cấp)" : tbTinDang.Rows[0]["OwnerFullname"].ToString();
                spanOwnerCode.InnerText = tbTinDang.Rows[0]["OwnerCode"].ToString();
                spanOwnerCreatedAt.InnerText = ((DateTime)tbTinDang.Rows[0]["OwnerCreatedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
                spanOwnerPhone.InnerText = tbTinDang.Rows[0]["OwnerPhone"].ToString();
                spanOwnerPhone.HRef = "tel:" + tbTinDang.Rows[0]["OwnerPhone"].ToString();
                spanOwnerEmail.InnerText = tbTinDang.Rows[0]["OwnerEmail"].ToString() == "" ? "(Chưa cung cấp)" : tbTinDang.Rows[0]["OwnerEmail"].ToString();
                spanOwnerEmail.HRef = tbTinDang.Rows[0]["OwnerEmail"].ToString() == "javascript:void(0);" ? "(Chưa cung cấp)" : ("mailto:"+ tbTinDang.Rows[0]["OwnerEmail"].ToString());

                string sAddress = "";
                string colAddress = tbTinDang.Rows[0]["OwnerAddress"].ToString();
                string colCity = tbTinDang.Rows[0]["OwnerCityName"].ToString();
                string colDistrict = tbTinDang.Rows[0]["OwnerDistrictName"].ToString();
                string colWard = tbTinDang.Rows[0]["OwnerWardName"].ToString();
				
				 if (tbTinDang.Rows[0]["StsPro"].ToString() == "1")
                {
                    spanOwnerPro.InnerText = "Thành Viên Pro";
                }
                else
                    spanOwnerPro.InnerText = "Thành Viên Thường";

               if (colCity == "" && colDistrict == "" && colWard == "" && colAddress == "")
                {
                   
                }
                else if (colCity != "" && colDistrict == "" && colWard == "" && colAddress == "")
                {
                    sAddress += colCity;
                }
                else if (colCity != "" && colDistrict != "" && colWard == "" && colAddress == "")
                {
                    sAddress += colDistrict + ", " +  colCity ;
                }
                else if(colCity != "" && colDistrict != "" && colWard != "" && colAddress == "")
                {
                    sAddress += colWard +", " + colDistrict + ", " + colCity;
                }
                else
                {
                    sAddress += colAddress + ", " + colWard + ", " + colDistrict + ", " + colCity;
                }
                spanOwnerAddress.InnerText = sAddress == "" ? "(Chưa cung cấp)" : sAddress;

                if (tbTinDang.Rows[0]["isHot"].ToString() == "" || tbTinDang.Rows[0]["isHot"].ToString() == "False")
                    ckisHot.Checked = false;
                else
                    ckisHot.Checked = true;

                string sqlHinhAnh = "select * from tb_HinhAnh where idTinDang='" + idTinDang + "'";
                DataTable tbHinhAnh = Connect.GetTable(sqlHinhAnh);
                string sHinhAnh = "";
                for (int i = 0; i < tbHinhAnh.Rows.Count; i++)
                {
                    sHinhAnh += tbHinhAnh.Rows[i]["UrlHinhAnh"].ToString().Trim() + "|~~~~|";
                }
                hdHinhAnh.Value = sHinhAnh;
                string htmlHinhAnh = "";
                string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                for (int i = 0; i < arrHinhAnh.Length; i++)
                {
                    if (arrHinhAnh[i].Trim() != "")
                    {
                        htmlHinhAnh += "<div id='dvHinhAnh_" + arrHinhAnh[i].Trim() + "' class='imgupload'>";
                        htmlHinhAnh += "<p style='margin:0px'><img src='/Images/td/slides/" + arrHinhAnh[i].Trim() + "' style='width: 150px;height: 110px;' /></p>";
                        htmlHinhAnh += "<p style='text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;'><img onclick='XoaHinhAnh(\"dvHinhAnh_" + arrHinhAnh[i].Trim() + "\",\"" + arrHinhAnh[i].Trim() + "\")' src='/images/icons/delete.png' style='width:25px; height:25px; cursor:pointer' /></p>";
                        htmlHinhAnh += "</div>";
                    }
                }
                dvHinhAnh.InnerHtml = htmlHinhAnh;
            }
        }
    }
    private void LoadDanhMucCap1()
    {
        string strSql = "select * from tb_DanhMucCap1 order by SoThuTu";
        ddlLoaiDanhMuc.DataSource = Connect.GetTable(strSql);
        ddlLoaiDanhMuc.DataTextField = "TenDanhMucCap1";
        ddlLoaiDanhMuc.DataValueField = "idDanhMucCap1";
        ddlLoaiDanhMuc.DataBind();
        ddlLoaiDanhMuc.Items.Add(new ListItem("", "0"));
        ddlLoaiDanhMuc.Items.FindByText("").Selected = true;
    }
	 //   private void LoadDanhMucCap2()
 //   {
    //    string strSql = "select * from tb_DanhMucCap2 order by SoThuTu";
    //    ddlLoaiDanhMuc2.DataSource = Connect.GetTable(strSql);
   //     ddlLoaiDanhMuc2.DataTextField = "TenDanhMucCap2";
   //     ddlLoaiDanhMuc2.DataValueField = "idDanhMucCap2";
   //     ddlLoaiDanhMuc2.DataBind();
   //     ddlLoaiDanhMuc2.Items.Add(new ListItem("", "0"));
   //     ddlLoaiDanhMuc2.Items.FindByText("").Selected = true;
   // }
	 private void LoadLiDo()
    {
        string html = "";
        string sql = "select * from tb_NoiDung";
        DataTable tb = Connect.GetTable(sql);
        if (tb != null)
        {
            html += @"
            <li><label><input type='checkbox' name ='kd' value='" + tb.Rows[0]["NoiDung"].ToString() + @"'/> " + tb.Rows[0]["NoiDung"].ToString() + @"</label><span style='float:right' ><a id='idLyDo_" + tb.Rows[0]["id"].ToString() + @"'  onclick='SuaLyDo(" + tb.Rows[0]["id"].ToString() + @")'>Sửa</a>&ensp;<a id='idLyDos_" + tb.Rows[0]["id"].ToString() + @"' onclick='DeleteLyDo(" + tb.Rows[0]["id"].ToString() + @")' style='color: red;'>Xóa</a></span></li>
            <div id='hienlydo_" + tb.Rows[0]["id"].ToString() + @"' style='display:none'><input id='txtSuaLydo_" + tb.Rows[0]["id"].ToString() + @"' data-val='true' data-val-required='' name='sualydo' type='text' class='inputLydo' style='font - family:Arial,sans - serif;margin-top:0px;' value='" + tb.Rows[0]["NoiDung"].ToString() + @"'/><button id='btnSuaLyDo_" + tb.Rows[0]["id"].ToString() + @"' onclick='SuaLyDoKD(" + tb.Rows[0]["id"].ToString() + @")' class='btn btn-info btn - flat' style='float:left; margin - top:5px;'><i class='fa fa-pencil - square - o' aria-hidden='true'></i> Sửa</button></div><br />            
            <hr style='margin-top:5px;margin-bottom:5px'/>";
            for (int i = 1; i < tb.Rows.Count; i++)

            {
                html += @"
            <li><label><input type='checkbox' name ='kd' value='" + tb.Rows[i]["NoiDung"].ToString() + @"'/> " + tb.Rows[i]["NoiDung"].ToString() + @"</label><span style='float:right' ><a id='idLyDo_" + tb.Rows[i]["id"].ToString() + @"'  onclick='SuaLyDo(" + tb.Rows[i]["id"].ToString() + @")'>Sửa</a>&ensp;<a id='idLyDos_" + tb.Rows[i]["id"].ToString() + @"' onclick='DeleteLyDo(" + tb.Rows[i]["id"].ToString() + @")' style='color: red;'>Xóa</a></span></li>
            <div id='hienlydo_" + tb.Rows[i]["id"].ToString() + @"' style='display:none'><input id='txtSuaLydo_" + tb.Rows[i]["id"].ToString() + @"' data-val='true' data-val-required='' name='sualydo' type='text' class='inputLydo' style='font - family:Arial,sans - serif;margin-top:0px;' value='" + tb.Rows[i]["NoiDung"].ToString() + @"'/><button id='btnSuaLyDo_" + tb.Rows[i]["id"].ToString() + @"' onclick='SuaLyDoKD(" + tb.Rows[i]["id"].ToString() + @")' class='btn btn-info btn - flat' style='float:left; margin - top:5px;'><i class='fa fa-pencil - square - o' aria-hidden='true'></i> Sửa</button></div><br />            
            <hr style='margin-top:5px;margin-bottom:5px'/>";
            }
        }
        htmlLydo.InnerHtml = html;

    }

    private void LoadTinh()
    {
        string strSql = "select * from City  order by type asc, ten asc";
        ddlTinh.DataSource = Connect.GetTable(strSql);
        ddlTinh.DataTextField = "Ten";
        ddlTinh.DataValueField = "id";
        ddlTinh.DataBind();
        ddlTinh.Items.Add(new ListItem("Toàn quốc", "0"));
        ddlTinh.Items.FindByText("Toàn quốc").Selected = true;
    }
	
	protected void ddLoaiDanhMuc_SelectedIndexChanged(object sender, EventArgs e)
    {
        string DM1 = ddlLoaiDanhMuc.SelectedValue.ToString();
        if (DM1 != "" && DM1 != "0")
        {
            SlDanhMuc2.Disabled = false;
            string sqlDM = "select * from tb_DanhMucCap2 where idDanhMucCap1='" + DM1 + "'  order by SoThuTu";
            SlDanhMuc2.DataSource = Connect.GetTable(sqlDM);
            SlDanhMuc2.DataTextField = "TenDanhMucCap2";
            SlDanhMuc2.DataValueField = "idDanhMucCap2";
            SlDanhMuc2.DataBind();
            SlDanhMuc2.Items.Add(new ListItem("Tất cả", "0"));
            SlDanhMuc2.Items.FindByText("Tất cả").Selected = true;
        }
        else
        {
            SlDanhMuc2.Disabled = true;
            string sqlDM = "select * from tb_DanhMucCap2 where isnull(idDanhMucCap1,'')=''  order by SoThuTu";
            SlDanhMuc2.DataSource = Connect.GetTable(sqlDM);
            SlDanhMuc2.DataTextField = "TenDanhMucCap2";
            SlDanhMuc2.DataValueField = "idDanhMucCap2";
            SlDanhMuc2.DataBind();
            SlDanhMuc2.Items.Add(new ListItem("", "0"));
            SlDanhMuc2.Items.FindByText("").Selected = true;
        }

    }
    protected void ddlTinh_SelectedIndexChanged(object sender, EventArgs e)
    {
        string CityId = ddlTinh.SelectedValue.ToString();
        if (CityId != "" && CityId != "0")
        {
            slHuyen.Disabled = false;
            string sqlHuyen = "select * from District where idTinhTP='" + CityId + "'  order by ten asc";
            slHuyen.DataSource = Connect.GetTable(sqlHuyen);
            slHuyen.DataTextField = "ten";
            slHuyen.DataValueField = "id";
            slHuyen.DataBind();
            slHuyen.Items.Add(new ListItem("Tất cả", "0"));
            slHuyen.Items.FindByText("Tất cả").Selected = true;
        }
        else
        {
            slHuyen.Disabled = true;
            string sqlHuyen = "select * from District where isnull(idTinhTP,'')=''  order by ten asc";
            slHuyen.DataSource = Connect.GetTable(sqlHuyen);
            slHuyen.DataTextField = "ten";
            slHuyen.DataValueField = "id";
            slHuyen.DataBind();
            slHuyen.Items.Add(new ListItem("", "0"));
            slHuyen.Items.FindByText("").Selected = true;
        }
		
		 string PhuongXaId = slHuyen.Value.Trim();
        if (PhuongXaId != "" && PhuongXaId != "0")
        {
            slXa.Disabled = false;
            string sqlXa = "select * from tb_PhuongXa where idQuanHuyen='" + PhuongXaId + "'  order by ten asc";
            slXa.DataSource = Connect.GetTable(sqlXa);
            slXa.DataTextField = "ten";
            slXa.DataValueField = "id";
            slXa.DataBind();
            slXa.Items.Add(new ListItem("Tất cả", "0"));
            slXa.Items.FindByText("Tất cả").Selected = true;
        }
        else
        {
            slXa.Disabled = true;
            string sqlXa = "select * from tb_PhuongXa where isnull(idQuanHuyen,'')=''  order by ten asc";
            slXa.DataSource = Connect.GetTable(sqlXa);
            slXa.DataTextField = "ten";
            slXa.DataValueField = "id";
            slXa.DataBind();
            slXa.Items.Add(new ListItem("", "0"));
            slXa.Items.FindByText("").Selected = true;
        }
    }
    protected void btDangTin_Click(object sender, EventArgs e)
    {
        string idDanhMucCap1 = "";
		 string idDanhMucCap2 = "";
        string idTinh = "";
        string idHuyen = "";
		string idXa = "";
        string TuGia = "";
        string DenGia = "";
        string SoLuongTheoLoaiGia = "1";
        string MaLoaiGia = "";
        string HoTen = "";
        string SoDienThoai = "";
        string Email = "";
        string DiaChi = "";
        string TieuDe = "";
        string NoiDung = "";
        string isHot = "False";
        string isDuyet = "NULL";
		string DuongDan="";
        //Danh mục cấp 1
		 string LyDo = "";
        //Danh mục cấp 1
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>document.getElementById('txtLyDo1').innerHTML = document.getElementById('txtLyDo').innerHTML </script>");

        idDanhMucCap1 = ddlLoaiDanhMuc.SelectedValue.Trim();
		idDanhMucCap2 = SlDanhMuc2.Value.Trim();
        //Tỉnh
        idTinh = ddlTinh.SelectedValue.Trim();
        //Huyện
        idHuyen = slHuyen.Value.Trim();
		 //Phường Xã
        idXa = slXa.Value.Trim();
        //Từ Giá
        TuGia = txtTuGia.Value.Trim().Replace(",", "").Replace(".", "");
        //Đến Giá
        DenGia = txtDenGia.Value.Trim().Replace(",", "").Replace(".", "");

        //Email
        Email = txtEmail.Value.Trim();
        //Địa chỉ
        DiaChi = txtDiaChi.Value.Trim();
        //Tiêu đề
        if (txtTieuDe.Value.Trim() != "")
        {
            TieuDe = txtTieuDe.Value.Trim();
            dvTieuDe.InnerHtml = "";
        }
        else
        {
            Response.Write("<script>alert('Bạn chưa nhập tiêu đề!');</script>");
            dvTieuDe.InnerHtml = "Bạn chưa nhập tiêu đề!";
            return;
        }
		if (txtDuongDan.Value.Trim() != "")
        {
            DuongDan = txtDuongDan.Value.Trim();
            dvDuongDan.InnerHtml = "";
        }
        else
        {
             DuongDan = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TieuDe.Trim()));
            do
            {
                string sql4 = @"
                select DuongDan from tb_TinDang
                where DuongDan = '" + DuongDan + @"' and (isHetHan = '0' or isHetHan is null ) 
            ";
                if (idTinDang != "")
                {
                    sql4 += " AND idTinDang != '" + idTinDang + "' ";
                }
                DataTable tb4 = Connect.GetTable(sql4);
                if (tb4.Rows.Count == 0)
                {
                    break;
                }

                DuongDan += "s";
            } while (true);
        }
		//if (slTinhTrang_.Value.Trim() == "1")
      //  {
       //     DuongDan = StaticData.ReplaceTieuDe(StaticData.BoDauTiengViet(TieuDe.Trim()));   
       // }
      //  else
       // {
       //     DuongDan = "";
       //     return;
       // }

        if (ckisHot.Checked)
            isHot = "True";

          if (slTinhTrang_.Value.Trim() == "1")
        {
            isDuyet = "1";
        }
        else if (slTinhTrang_.Value.Trim() == "0")
        {
            isDuyet = "0";
            string sql = "select LyDo_KhongDuyet from tb_TinDang where idTinDang='" +idTinDang+ "'";
            DataTable table1 = Connect.GetTable(sql);
            if (table1.Rows.Count > 0)
            {
                if (table1.Rows[0]["LyDo_KhongDuyet"].ToString() != "")
                {
                    LyDo = table1.Rows[0]["LyDo_KhongDuyet"].ToString();   
                }
                else
                {
                    if (txtLyDoKD.Value.Trim() != "")
                    {
                        LyDo = txtLyDoKD.Value.Trim();
                    }
                }
            }
            if(LyDo =="")
            {
                Response.Write("<script>alert('Bạn hãy chọn lý do từ chối tin đăng!');</script>");
                return;
            }
        }
        else
        {
            Response.Write("<script>alert('Bạn hãy chọn trạng thái duyệt của tin đăng!');</script>");
            ListLyDo.Visible = false;
            MessTinhTrang.Visible = true;
            return;
        }
            
        //Nội dung
        if (txtNoiDung.Value.Trim() != "")
        {
            NoiDung = txtNoiDung.Value.Trim();
            dvNoiDung.InnerHtml = "";
        }
        else
        {
            Response.Write("<script>alert('Bạn chưa nhập nội dung!');</script>");
            dvNoiDung.InnerHtml = "Bạn chưa nhập nội dung!";
            return;
        }
        if (idTinDang == "")
        {
            //Insert tin đăng
            //string sqlInsertTD = "insert into tb_TinDang(MaLoaiThue,idDanhMucCap2,idTinh,idHuyen,Gia,SoLuongTheoLoaiGia,MaLoaiGia,HoTen,SoDienThoai,Email,DiaChi,TieuDe,NoiDung,idThanhVien,NgayDang)";
            //sqlInsertTD += " values('" + MaLoaiThue + "'";
            //sqlInsertTD += ",'" + idDanhMucCap2 + "'";
            //if (idTinh != "" && idTinh != "0")
            //    sqlInsertTD += ",'" + idTinh + "'";
            //else
            //    sqlInsertTD += ",null";
            //if (idHuyen != "" && idHuyen != "0")
            //    sqlInsertTD += ",'" + idHuyen + "'";
            //else
            //    sqlInsertTD += ",null";
            //if (Gia != "")
            //    sqlInsertTD += ",'" + Gia + "'";
            //else
            //    sqlInsertTD += ",null";
            //if (SoLuongTheoLoaiGia != "")
            //    sqlInsertTD += ",'" + SoLuongTheoLoaiGia + "'";
            //else
            //    sqlInsertTD += ",null";
            //if (MaLoaiGia != "")
            //    sqlInsertTD += ",'" + MaLoaiGia + "'";
            //else
            //    sqlInsertTD += ",null";
            //sqlInsertTD += ",N'" + HoTen + "','" + SoDienThoai + "','" + Email + "',N'" + DiaChi + "',N'" + TieuDe + "',N'" + NoiDung + "'";
            //if (idThanhVien != "")
            //    sqlInsertTD += ",'" + idThanhVien + "'";
            //else
            //    sqlInsertTD += ",null";
            //sqlInsertTD += ",'" + DateTime.Now.ToString() + "')";
            //bool ktInsertTD = Connect.Exec(sqlInsertTD);
            //if (ktInsertTD)
            //{
            //    string idTinDangMoi = "";
            //    string sqlTinDangMoi = "select top 1 idTinDang from tb_TinDang where '1'='1'";
            //    if (idThanhVien != "")
            //        sqlTinDangMoi += " and idThanhVien='" + idThanhVien + "'";
            //    sqlTinDangMoi += " order by idTinDang desc";
            //    DataTable tbTinDangMoi = Connect.GetTable(sqlTinDangMoi);
            //    if (tbTinDangMoi.Rows.Count > 0)
            //        idTinDangMoi = tbTinDangMoi.Rows[0]["idTinDang"].ToString();
            //    string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
            //    for (int i = 0; i < arrHinhAnh.Length; i++)
            //    {
            //        if (arrHinhAnh[i].Trim() != "")
            //        {
            //            string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTinDang,UrlHinhAnh) values('" + idTinDangMoi + "','" + arrHinhAnh[i].Trim() + "')";
            //            bool ktInsertHinhAnh = Connect.Exec(sqlInsertHinhAnh);
            //        }
            //    }
            //    if (idThanhVien != "")
            //        Response.Redirect("/thong-tin-ca-nhan/ttcn");
            //    else
            //        Response.Redirect(Domain);
            //}
            //else
            //{
            //    Response.Write("<script>alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!');</script>");
            //}
        }
        else
        {
            //Sửa tin đăng
            try
            {
                if (Request.QueryString["TenDangNhap"].Trim() != "")
                {
                    sTenDangNhap = Request.QueryString["TenDangNhap"].Trim();
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["LinhVuc"].Trim() != "")
                {
                    LinhVuc = Request.QueryString["LinhVuc"].Trim();
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["TinhTrang"].Trim() != "")
                {
                    sTinhTrang = Request.QueryString["TinhTrang"].Trim();
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["MT"].Trim() != "")
                {
                    MT = Request.QueryString["MT"].Trim();
                }
            }

            catch { }
            string url1 = "";
            if (LinhVuc != "")
                url1 += "LinhVuc=" + LinhVuc + "&";
            if (sTuNgay != "")
                url1 += "TuNgay=" + sTuNgay + "&";
            if (sDenNgay != "")
                url1 += "DenNgay=" + sDenNgay + "&";
            if (sTinhTrang != "")
                url1 += "TinhTrang=" + sTinhTrang + "&";
            if (sTenDangNhap != "")
                url1 += "TenDangNhap=" + sTenDangNhap + "&";
            if (NguoiDuyet != "")
                url1 += "ND=" + NguoiDuyet + "&";
            if (MT != "")
                url1 += "MT=" + MT + "&";

            string sqlUpdateTD = "update tb_TinDang set ";

            if (idDanhMucCap1 != "" && idDanhMucCap1 != "0")
                sqlUpdateTD += " idDanhMucCap1='" + idDanhMucCap1 + "'";
            else
                sqlUpdateTD += "idDanhMucCap1=null";
			
			 if (idDanhMucCap2 != "" && idDanhMucCap2 != "0")
                sqlUpdateTD += ",idDanhMucCap2='" + idDanhMucCap2 + "'";
            else
                sqlUpdateTD += ",idDanhMucCap2=null";
            if (idTinh != "" && idTinh != "0")
                sqlUpdateTD += ",idTinh='" + idTinh + "'";
            else
                sqlUpdateTD += ",idTinh=null";
            if (idHuyen != "" && idHuyen != "0")
                sqlUpdateTD += ",idHuyen='" + idHuyen + "'";
            else
                sqlUpdateTD += ",idHuyen=null";
			
			if (idXa != "" && idXa != "0")
                sqlUpdateTD += ",idPhuongXa='" + idXa + "'";
            else
                sqlUpdateTD += ",idPhuongXa=null";
			
            if (TuGia != "")
                sqlUpdateTD += ",TuGia='" + TuGia + "'";
            else
                sqlUpdateTD += ",TuGia=null";
            if (DenGia != "")
                sqlUpdateTD += ",DenGia='" + DenGia + "'";
            else
                sqlUpdateTD += ",DenGia=null";
            if (SoLuongTheoLoaiGia != "")
                sqlUpdateTD += ",SoLuongTheoLoaiGia='" + SoLuongTheoLoaiGia + "'";
            else
                sqlUpdateTD += ",SoLuongTheoLoaiGia=null";
            if (MaLoaiGia != "" && MaLoaiGia != "0")
                sqlUpdateTD += ",MaLoaiGia='" + MaLoaiGia + "'";
            else
                sqlUpdateTD += ",MaLoaiGia=null";
            sqlUpdateTD+=",idAdmin_Duyet='" + idadmin + "'";

         

            sqlUpdateTD += ",HoTen=N'" + HoTen + "'";
            sqlUpdateTD += ",SoDienThoai='" + SoDienThoai + "'";
            sqlUpdateTD += ",Email='" + Email + "'";
            sqlUpdateTD += ",DiaChi=N'" + DiaChi + "'";
            sqlUpdateTD += ",TieuDe=N'" + TieuDe + "'";
			sqlUpdateTD += ",DuongDan=N'" + DuongDan + "'";
            sqlUpdateTD += ",NoiDung=N'" + NoiDung + "'";
            sqlUpdateTD += ",isHot=N'" + isHot + "'";

            sqlUpdateTD += ",isDuyet=" + isDuyet + "";

            if(isDuyet == "1")
            {
                string sqlLayNgay
                = "		select NgayDang,NgayGuiDuyet,NgayAdminDuyet from tb_TinDang where idtindang='" + idTinDang + "' ";
                DataTable tbLayNgay = Connect.GetTable(sqlLayNgay);

                string NgayDang = "";
                string NgayGuiDuyet = "";
				string NgayAdminDuyet = "";
                string DayNow = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                try
                {
                    NgayDang = DateTime.Parse(tbLayNgay.Rows[0]["NgayDang"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch
                {

                }

                try
                {
                    NgayGuiDuyet = DateTime.Parse(tbLayNgay.Rows[0]["NgayGuiDuyet"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch
                {

                }
				try
                {
                    NgayAdminDuyet = DateTime.Parse(tbLayNgay.Rows[0]["NgayAdminDuyet"].ToString()).ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch
                {

                }
                if (NgayDang != "")
                {
                       
                        sqlUpdateTD += ",NgayDayLenTop='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "'";
						//sqlUpdateTD += ",NgayAdminDuyet='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' ";
                        
                }
                else
                {

                }
            }
        

            sqlUpdateTD += ",LyDo_KhongDuyet=" + (isDuyet == "0" ? ("N'" + LyDo + "'") : ("NULL")) + "";
			sqlUpdateTD += ",NgayAdminDuyet='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' ";
            sqlUpdateTD += "where idTinDang='" + idTinDang + "'";
            bool ktUpdateTD = Connect.Exec(sqlUpdateTD);
            if (ktUpdateTD)
            {
                string sqlDeleteHinhAnh = "delete from tb_HinhAnh where idTinDang='" + idTinDang + "'";
                bool ktDeleteHinhAnh = Connect.Exec(sqlDeleteHinhAnh);
                if (ktDeleteHinhAnh)
                {
                    string[] arrHinhAnh = hdHinhAnh.Value.Split(new string[] { "|~~~~|" }, StringSplitOptions.None);
                    for (int i = 0; i < arrHinhAnh.Length; i++)
                    {
                        if (arrHinhAnh[i].Trim() != "")
                        {
                            string sqlInsertHinhAnh = "insert into tb_HinhAnh(idTinDang,UrlHinhAnh) values('" + idTinDang + "','" + arrHinhAnh[i].Trim() + "')";
                            bool ktInsertHinhAnh = Connect.Exec(sqlInsertHinhAnh);
                        }
                    }
                }
                Response.Redirect("TinChoDuyet.aspx?" + url1);
            }
            else
            {
                Response.Write("<script>alert('Lỗi, bạn vui lòng kiểm tra lại dữ liệu nhập!');</script>");
            }
        }
        
    }
    protected void loadReferral(string tinDangId)
    {
        string sql = "";
        sql += @" SELECT ";
        sql += @"   tb_ThanhVien.idThanhVien AS AccountId, ";
        sql += @"   tb_ThanhVien.TenDangNhap AS Username, ";
        sql += @"   Referrals.Id AS ReferralId, ";
        sql += @"   Referrals.Status AS Status, ";
        sql += @"   ISNULL(Referrals.StsOwnerApproved, 0) AS ReferralStsOwnerApproved, ";
        sql += @"   ISNULL(Referrals.StsCollabApproved, 0) AS ReferralStsCollabApproved, ";
        sql += @"   ISNULL(Referrals.StsAdminApproved, 0) AS ReferralStsAdminApproved ";
        sql += @" FROM Referrals ";
        sql += @" INNER JOIN tb_ThanhVien ";
        sql += @"   ON Referrals.CollabId = tb_ThanhVien.idThanhVien ";
        sql += @" WHERE Referrals.TinDangId = "+ tinDangId + @" ";

        DataTable dataTable = Connect.GetTable(sql);
        if (dataTable != null)
        {
            List<string> items = new List<string>();
            for (int i = 0; i < dataTable.Rows.Count; i = i + 1)
            {
                string accountId = dataTable.Rows[i]["AccountId"].ToString();
                string username = dataTable.Rows[i]["Username"].ToString();
                string referralId = dataTable.Rows[i]["ReferralId"].ToString();
                string status = dataTable.Rows[i]["Status"].ToString();
                string typeCss = "";
                string statusText = "";
                string template = "";
                string bgCss = "";
                if (dataTable.Rows[i]["ReferralStsOwnerApproved"].ToString() == "1" && dataTable.Rows[i]["ReferralStsCollabApproved"].ToString() == "0" && dataTable.Rows[i]["ReferralStsAdminApproved"].ToString() == "0")
                {
                    statusText = "Người đăng bài đã xác nhận";
                    typeCss = "warning";
                    bgCss = "bg-warning";
                }
                else if (dataTable.Rows[i]["ReferralStsOwnerApproved"].ToString() == "1" && dataTable.Rows[i]["ReferralStsCollabApproved"].ToString() == "1" && dataTable.Rows[i]["ReferralStsAdminApproved"].ToString() == "0")
                {
                    statusText = "Cộng tác viên đã xác nhận";
                    typeCss = "primary";
                }
                else if (dataTable.Rows[i]["ReferralStsOwnerApproved"].ToString() == "1" && dataTable.Rows[i]["ReferralStsCollabApproved"].ToString() == "1" && dataTable.Rows[i]["ReferralStsAdminApproved"].ToString() == "1")
                {
                    statusText = "Tung Tăng đã xác nhận";
                    typeCss = "success";
                }
                template += "<li class='list-group-item "+ bgCss + "'>";
                template += "   <span>"+ username + "</span>";
                if(status == Models.Referral.STATUS_OWNER_APPROVED)
                {
                    template += "   <button type='button' class='btn btn-primary btn-xs page-item-attribute' onclick='confirmReferral(\""+ referralId + "\")'>Xác nhận</button>";
                }
                else
                {
                    template += "   <span class='label label-"+ typeCss + " page-item-sucees page-item-attribute'>" + statusText+"</span>";
                }
                template += "</li>";
                items.Add(template);
            }
            repeaterReferrals.DataSource = items;
            repeaterReferrals.DataBind();
        }
    }
    protected void fakeButtonReferralAprove_click(object sender, EventArgs e)
    {
        string referralId = fakeTextBoxReferralForAccountId.Text.Trim();
        if (referralId == "")
        {
            return;
        }
        Utilities.Referral.adminApproveReferral(referralId);
        this.loadReferral(idTinDang);
    }

    protected void Unnamed_ServerClick(object sender, EventArgs e)
    {
        string selected = Request.Form["Fruit"];
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + selected + "');", true);
    }
}