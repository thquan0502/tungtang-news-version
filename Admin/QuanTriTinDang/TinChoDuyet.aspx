﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="TinChoDuyet.aspx.cs" Inherits="Admin_QuanTriTinDang_QuanLyTinDang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />--%>
     <script src="../plugins/select2/select2.full.min.js"></script>
    <link href="../plugins/select2/select2.min.css" rel="stylesheet" />
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>  

       <%-- range --%>
    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="../../asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>
    <%-- datepicker --%>
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>
    <%-- other --%>

    <script>
         function DuyetTinDang(idTinDang, flag) {
            var Duyet = document.getElementById("slDuyet_" + idTinDang);
            if (Duyet.disabled == true) {
                document.getElementById("slDuyet_" + idTinDang).disabled = false;
                document.getElementById("btDuyet_" + idTinDang).innerHTML = "<img class='imgedit' src='../images/save.png'/>Lưu</a>";
            }
            else {
                if (Duyet.value == "KhongDuyet" && flag == undefined) {
                    $("#modalAccountInfoLydo").modal({
                        fadeDuration: 200,
                        showClose: false
                    });
                    $("#btnLuuLyDo").attr("onclick", "DuyetTinDang(" + idTinDang + ",1)");
                }
                else {
                    var LyDo = "";
                    var checkbox = document.getElementsByName('kd');
                    var thongbao = '';
                    var j = 1;
                    if (Duyet.value == "KhongDuyet") {
                        for (var i = 0; i < checkbox.length; i++) {
                            if (checkbox[i].checked === true) {
                                LyDo += j + '.' + checkbox[i].value + '|';
                                j++;
                            }
                        }
                        if (LyDo == '') {
                            thongbao += "Hãy chọn lý do";
                        }
                    }
                    if (thongbao == '') {
                        var xmlhttp;
                        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        }
                        else {// code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "True")
                                    window.location.reload();
                                else
                                    alert("Lỗi !");
                            }
                        }
                        xmlhttp.open("GET", "../adAjax.aspx?Action=DuyetTinDang&idTinDang=" + idTinDang + "&Duyet=" + Duyet.value + "&LyDo=" + LyDo, true);
                        xmlhttp.send();
                    }
                    else {
                        alert(thongbao);
                    }
                }
            }
        }
        function MoXemChiTiet(idTinDang) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById('lightXemChiTiet').style.display = 'block';
                    document.getElementById('fadeXemChiTiet').style.display = 'block';
                    document.getElementById('dvXemChiTiet').innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=MoXemChiTietTinDang&idTinDang=" + idTinDang, true);
            xmlhttp.send();
        }
        function DongXemChiTiet() {
            document.getElementById('lightXemChiTiet').style.display = 'none';
            document.getElementById('fadeXemChiTiet').style.display = 'none';
        }
        function LenTop(TD) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "True") {
                        alert('Bài viết đã lên TOP');
                    }

                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=LenTop&TD=" + TD, true);
            xmlhttp.send();
        }
    </script>
     <script>
        function ThemLyDo() {
            var txtLydo = $("#txtLydo").val();
            if (txtLydo == "") {
                alert("Vui lòng điền lý do không duyệt!");
            }
            else {                
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {                    
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")   
                         window.location.reload();
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=ThemLyDo&txtLydo=" + txtLydo, true);
                xmlhttp.send();                
            }
        }
    </script>
    <script>
        function SuaLyDo(idLydo)
        {            
            document.getElementById("hienlydo_" + idLydo).style.display = "";
        }
        function SuaLyDoKD(idLydo, txtLydo) {
            var id = document.getElementById("idLyDo_" + idLydo);
            var txtLydo = $("#txtSuaLydo_" + idLydo).val();
            if (txtLydo == "") {
                alert("Vui lòng điền lý do không duyệt!");
            }
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            alert("Sửa lý do thành công!");
                        window.location.reload();
                        //$('#modalThongKe').load('PostTable.aspx #modalThongKe');
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=SuaLyDoKD&idLydo=" + idLydo + "&txtLydo=" + txtLydo, true);
                xmlhttp.send();
            }
        }
    </script>
    <script>
       function DeleteLyDo(idLyDo) {
           if (confirm("Bạn có muốn xóa không ?")) {
               var xmlhttp;
               if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                   xmlhttp = new XMLHttpRequest();
               }
               else {// code for IE6, IE5
                   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
               }
               xmlhttp.onreadystatechange = function () {
                   if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                       if (xmlhttp.responseText == "True")
                           window.location.reload();
                   }
               }
               xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteLyDo&id=" + idLyDo, true);
               xmlhttp.send();
           }
       }
    </script>
    <style>
        #myTable .social {
            min-width: 150px;
        }
      @media (min-width:992px) {
            .btn-lydo{
                 float:left;
                 margin-top:5px;
                 margin-left:20px;
            }
            .modal-lydo {
                overflow: hidden;
                background-color: white;
                width: 600px;
                margin-left: 400px;
                margin-top: 30px;
            }

            .Lydokd {
                list-style-type: none;
                margin-left: -20px;
            }
            .head-lydo{
                background-color: #000012; 
                color:#ffffff; 
                text-align: center; 
                font-weight:bold;
                font-size:16px;
                padding: 10px;
                width:600px;
            }
            .inputLydo[type=text] {
            margin-left:3px;
            margin-top:5px;
          width: 130px;
          box-sizing: border-box;
          border: 2px solid #ccc;
          border-radius: 4px;
          font-size: 14px;
          background-color: white;
          background-position: 10px 10px; 
          background-repeat: no-repeat;
          padding:6px 10px 8px 8px;
          -webkit-transition: width 0.4s ease-in-out;
          transition: width 0.4s ease-in-out;
        }
        .inputLydo[type=text]:focus{
            width:80%;
            border: 2px solid #ff9900;
        }
        }
        @media (max-width:600px) {
            .btn-lydo{
                 float:left;
                 margin-top:5px;
                 margin-left:10px;
            }
            .modal-lydo {
                overflow: hidden;
                background-color: white;
                width: 338px;
                margin-left: 20px;
                margin-top: 30px;
            }

            .Lydokd {
                list-style-type: none;
                margin-left: -20px;
            }
            .head-lydo{
                background-color: #000012; 
                color:#ffffff; 
                font-weight: bold; 
                text-align: center; 
                padding: 10px;
                width:355px;
                font-size:14px;
            }
            .inputLydo[type=text] {
            margin-left:3px;
            margin-top:5px;
          width: 130px;
          box-sizing: border-box;
          border: 2px solid #ccc;
          border-radius: 4px;
          font-size: 14px;
          background-color: white;
          background-position: 10px 10px; 
          background-repeat: no-repeat;
          padding:6px 10px 8px 8px;
          -webkit-transition: width 0.4s ease-in-out;
          transition: width 0.4s ease-in-out;
        }
        .inputLydo[type=text]:focus{
            width:65%;
            border: 2px solid #ff9900;
        }
        }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server" autocomplete="off">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="title">TIN CHỜ DUYỆT</div>
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Từ ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTuNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Đến ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDenNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Thời hạn:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slHan" runat="server">
                                            <option value="0">-- Chọn --</option>
                                            <option value="1">Hết hạn</option>
                                            <option value="2">Còn hạn</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Tình trạng:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slTinhTrang" runat="server">
                                            <option value="-1">-- Chọn --</option>
                                            <option value="1">Đã duyệt</option>
                                            <option value="0">Không duyệt</option>
                                            <option value="2">Đang chờ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tên đăng nhập:</b></div>
                                    <div class="txtinput">
                                       <asp:DropDownList ID="drdlCollab" runat="server" CssClass="form-control" autocomplete="off">
                                    </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="coninput3" style="display:none">
                                    <div class="titleinput"><b>isHot:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slisHot" runat="server">
                                            <option value="-1">-- Tất cả --</option>
                                            <option value="True">Tin Hot</option>
                                            <option value="False">Tin không Hot</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Mã tin:</b></div>
                                    <div class="txtinput">
                                       <select class="form-control" id="slMaTin" runat="server">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Hoa Hồng:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slHoaHong" runat="server">
                                            <option value="0">-- Tất cả --</option>
                                            <option value="1">Có hoa hồng</option>
                                            <option value="2">Không có hoa hồng</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Người duyệt:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slNguoiDuyet" runat="server">
                                        </select>
                                    </div>
                                </div>
                                </div>
                            </div>

                          <div class="form-group">
                                 <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                 <div class="coninput1">                                    
                                     <div class="titleinput"><b>Danh mục cấp 1:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" style="margin-bottom:15px" id="slLinhVuc">
                                        </select>
                                    </div>
                                     <div style="display:none" id="dvDMC2">
                                     <div class="titleinput"><b>Danh mục Cấp 2:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slLinhVuc2">
                                        </select>
                                    </div>
                                    </div>
                                      </div>
                              <div class="coninput2">
                                    <div class="titleinput"><b>Khu vực:</b></div>
                                    <div class="txtinput">
                                        <select id="filterCity" style="margin-bottom:15px" class="form-control"></select>                                        
                                    </div>
                                     <div style="display:none" id="dvqh">
                                    <div class="titleinput"><b>Quận/Huyện:</b></div>
                                    <div class="txtinput">                                        
                                        <select id="filterDistrict" style="margin-bottom:15px" class="form-control"></select>                                        
                                    </div>
                                    </div>
                                    <div style="display:none" id="dvPX">
                                        <div class="titleinput"><b>Phường/Xã:</b></div>
                                        <div class="txtinput">
                                            <select id="filterWard" class="form-control"></select>
                                        </div>
                                    </div>
                                      </div>
                                </div>
                            </div> 
                          <%--Bộ lọc chi tiết--%>    
                        <div class="form-group"> 
                            <div class="row">
                                  <div class="dvnull">&nbsp;</div>
                              
                                <div class="coninput1">
                                     <div id="checkbox-container2">                                      
                                        <div>
                                    <div class="titleinput"><b>Giá:</b>
                                        
                                    </div>
                                    <div class="txtinput" runat="server">                                 
                                        <input type="hidden" id="txttestchh" ></input>
                                        <input type="hidden" id="txtRangeLuotXemchh" runat="server"></input>
                                        <p id="rulerchh" runat="server" visible="false"></p>
                                        <div style="margin-bottom:9px;" >
                                        <label style="font-weight:bold"><input type="checkbox" id="chonrulerchh" name="checkchh" value="Chọn"/> Chọn Range lọc</label>
                                        <asp:TextBox ID="txtFeeCounterRange" runat="server" type="text" CssClass="form-control"></asp:TextBox>
                                        </div>
                                         <a id="textCHH" style="color:blue">>>Thêm bộ lọc<<</a>
                                        <a id="textCHHAN" style="color:blue;display:none">>>Ẩn<<</a>
                                     <div id="BoLocCHH" style="display:none;">
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb10100chh" name="checkchh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu
                                         </div>
                                        <div class="col-12" style="font-weight:bold">
                                          <input type="checkbox" id="ckb100500chh" name="checkchh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb500chh" name="checkchh" value="Từ 500 trở lên" /> 500 triệu trở lên
                                        </div>
                                     </div>
                                        <div style="margin-top:9px;">               
                                        <input type="button" id="btnchh" value="Áp dụng Giá" class="btn btn-success btn-flat"/>
                                         <p id="messagexx" style="color:#ffba00"></p>
                                            </div>
                                        </div>
                                            </div>
				
                                            <script>
                                                document.getElementById('btnchh').onclick = function () {
                                                    document.getElementById('messagexx').innerText = "Đã áp dụng";
                                                        // Khai báo tham số
                                                        var checkboxchh = document.getElementsByName('checkchh');
                                                        var resultchh = "";

                                                        // Lặp qua từng checkbox để lấy giá trị
                                                        for (var i = 0; i < checkboxchh.length; i++) {
                                                            if (checkboxchh[i].checked === true) {
                                                                resultchh += '' + checkboxchh[i].value + '';
                                                            }
                                                        }
                                                        // In ra kết quả
                                                        //alert("Sở thích là: " + result);
                                                        //document.getElementById('show_message').innerHTML = result;
                                                        document.getElementById('txttestchh').value = resultchh;
                                                        document.getElementById('<%= txtRangeLuotXemchh.ClientID %>').value = document.getElementById('txttestchh').value;
                                                    };
                                                function chonLuotXemchh(obj) {

                                                    window.onload = function () {
                                                        //Lượt xem
                                                        if (sessionStorage.getItem('select') == "select") {
                                                            return;
                                                        }
                                                        if (sessionStorage.getItem('luotxem') == "luotxem") {
                                                            return;
                                                        }                                                        

                                                        var name = sessionStorage.getItem('select');
                                                        if (name !== null) $('#chartSelectRange').val(name);

                                                        var range = sessionStorage.getItem('luotxem');
                                                        if (range !== null) $('#txttest').val(range);
                                                        //Hoa Hồng
                                                        if (sessionStorage.getItem('selecthh') == "selecthh") {
                                                            return;
                                                        }
                                                        if (sessionStorage.getItem('luotxemhh') == "luotxemhh") {
                                                            return;
                                                        }
                                                        var namehh = sessionStorage.getItem('selecthh');
                                                        if (namehh !== null) $('#chartSelectRangehh').val(namehh);

                                                        var rangehh = sessionStorage.getItem('luotxemhh');
                                                        if (rangehh !== null) $('#txttesthh').val(rangehh);
                                                        //Chi hoa hồng
                                                        if (sessionStorage.getItem('selectchh') == "selectchh") {
                                                            return;
                                                        }
                                                        if (sessionStorage.getItem('luotxemchh') == "luotxemchh") {
                                                            return;
                                                        }
                                                        var namechh = sessionStorage.getItem('selectchh');
                                                        if (namechh !== null) $('#chartSelectRangechh').val(namechh);

                                                        var rangechh = sessionStorage.getItem('luotxemchh');
                                                        if (rangechh !== null) $('#txttestchh').val(rangechh);

                                                    }
                                                    window.onbeforeunload = function () {
                                                        //Lượt xem
                                                        sessionStorage.setItem("select", $('#chartSelectRange').val());
                                                        sessionStorage.setItem("luotxem", $('#txttest').val());     
                                                        //Hoa Hồng
                                                        sessionStorage.setItem("selecthh", $('#chartSelectRangehh').val());
                                                        sessionStorage.setItem("luotxemhh", $('#txttesthh').val());
                                                        //Chi hoa hồng
                                                        sessionStorage.setItem("selectchh", $('#chartSelectRangechh').val());
                                                        sessionStorage.setItem("luotxemchh", $('#txttestchh').val());
                                                    }
                                                }                                                
                                               
                                        </script>     
                                         <script>
                                               document.getElementById('textCHH').onclick = function () {
                                                   document.getElementById("BoLocCHH").style.display = "";
                                                   document.getElementById("textCHH").style.display = "none";
                                                   document.getElementById("textCHHAN").style.display = "";
                                               }
                                               document.getElementById("textCHHAN").onclick = function () {
                                                   document.getElementById("textCHH").style.display = "";
                                                   document.getElementById("BoLocCHH").style.display = "none";
                                                   document.getElementById("textCHHAN").style.display = "none";
                                               };
                                               </script>        
                                        <script>                                            
                                            var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},
                                            $checkboxes = $("#checkbox-container :checkbox");

                                            $checkboxes.on("change", function () {
                                                $checkboxes.each(function () {
                                                    checkboxValues[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
                                            });

                                            // On page load
                                            $.each(checkboxValues, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>   
                                        <script>                                            
                                            var checkboxValues1 = JSON.parse(sessionStorage.getItem('checkboxValues1')) || {},
                                            $checkboxes1 = $("#checkbox-container1 :checkbox");

                                            $checkboxes1.on("change", function () {
                                                $checkboxes1.each(function () {
                                                    checkboxValues1[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues1", JSON.stringify(checkboxValues1));
                                            });

                                            // On page load
                                            $.each(checkboxValues1, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>      
                                        <script>                                            
                                            var checkboxValues2 = JSON.parse(sessionStorage.getItem('checkboxValues2')) || {},
                                            $checkboxes2 = $("#checkbox-container2 :checkbox");

                                            $checkboxes2.on("change", function () {
                                                $checkboxes2.each(function () {
                                                    checkboxValues2[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues2", JSON.stringify(checkboxValues2));
                                            });

                                            // On page load
                                            $.each(checkboxValues2, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>                                                             
                                    </div>
                                </div>
                                  <div class="coninput2">
                                    <div id="checkbox-container1">                                      
                                        <div>
                                    <div class="titleinput"><b>Hoa hồng:</b>                                       
                                    </div>
                                        <div class="txtinput" runat="server">
                                        <div style="margin-bottom:9px;" >
                                        <label style="font-weight:bold"><input type="checkbox" id="chonrulerhh" name="checkhh" value="Chọn"/> Chọn Range lọc</label>                                    
                                        <asp:TextBox ID="txtCommissionCounterRange" runat="server" type="text" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <a id="textHH" style="color:blue">>>Thêm bộ lọc<<</a>
                                        <a id="textHHAN" style="color:blue;display:none">>>Ẩn<<</a>
                                    <div id="BoLocHH" style="display:none;">
                                        <div class="col-12" style="font-weight:bold">
                                           <input type="checkbox" id="ckb10100hh" name="checkhh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb100500hh" name="checkhh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb500hh" name="checkhh" value="Từ 500 trở lên" /> 500 triệu trở lên
                                        </div>
                                    </div>
                                            <div style="margin-top:9px;">
                                                 <input type="button" id="btnhh" value="Áp dụng hoa hồng" class="btn btn-success btn-flat"/>
                                                <p id="messagex" style="color:#ffba00"></p>
                                            </div>
                                       
                                            </div>
                                            </div>
                                        
                                        <%--<p id="show_message"></p>--%>                                  
                                        <input type="hidden" id="txttesthh" />
                                        <input type="hidden" id="txtRangeLuotXemhh" runat="server" />
                                        <p id="rulerhh" runat="server" visible="false"></p>
                                         <script>                                            
                                            document.getElementById('btnhh').onclick = function()
                                            {
                                                document.getElementById('messagex').innerText = "Đã áp dụng";
                                                // Khai báo tham số
                                                var checkboxhh = document.getElementsByName('checkhh');
                                                var resulthh = "";
                                                
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkboxhh.length; i++){
                                                    if (checkboxhh[i].checked === true){
                                                        resulthh += '' + checkboxhh[i].value + '';
                                                    }
                                                }                                                                                              
                                                document.getElementById('txttesthh').value = resulthh;
                                                document.getElementById('<%= txtRangeLuotXemhh.ClientID %>').value = document.getElementById('txttesthh').value;
                                            };
                                        </script>
                                         <script>
                                               document.getElementById('textHH').onclick = function () {
                                                   document.getElementById("BoLocHH").style.display = "";
                                                   document.getElementById("textHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "";
                                               }
                                               document.getElementById("textHHAN").onclick = function () {
                                                   document.getElementById("textHH").style.display = "";
                                                   document.getElementById("BoLocHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "none";
                                               };
                                               </script>       
                                              <script>                                            
                                            var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},
                                            $checkboxes = $("#checkbox-container :checkbox");

                                            $checkboxes.on("change", function () {
                                                $checkboxes.each(function () {
                                                    checkboxValues[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
                                            });

                                            // On page load
                                            $.each(checkboxValues, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>   
                                        <script>                                            
                                            var checkboxValues1 = JSON.parse(sessionStorage.getItem('checkboxValues1')) || {},
                                            $checkboxes1 = $("#checkbox-container1 :checkbox");

                                            $checkboxes1.on("change", function () {
                                                $checkboxes1.each(function () {
                                                    checkboxValues1[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues1", JSON.stringify(checkboxValues1));
                                            });

                                            // On page load
                                            $.each(checkboxValues1, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>      
                                        <script>                                            
                                            var checkboxValues2 = JSON.parse(sessionStorage.getItem('checkboxValues2')) || {},
                                            $checkboxes2 = $("#checkbox-container2 :checkbox");

                                            $checkboxes2.on("change", function () {
                                                $checkboxes2.each(function () {
                                                    checkboxValues2[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues2", JSON.stringify(checkboxValues2));
                                            });

                                            // On page load
                                            $.each(checkboxValues2, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>                                    
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                                 <div class="row">
                                  <div class="dvnull">&nbsp;</div>
                                     <div class="coninput1">                                                                                              
                                        <div class="titleinput"><b>Tổng tin đăng:</b></div>
                                        <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTongTinDang" runat="server"  readonly type="text" value="" />
                                        </div>                      
                                </div>  
                                     </div>
                            </div>
                                 
                        </div>

                        <div class="row">
                            <div class="col-sm-9" id="SoTinDangCho" runat="server" style="font-weight:bold;font-size:14px;">
                                <%--<a class="btn btn-primary btn-flat" href="DanhMucLoaiKhachHang-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>--%>
                           
                                
                                
                                 </div>
                            <div class="col-sm-3">
                                <div style="text-align: right;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                                    <button id="btnTimKiem" class="btn btn-primary btn-flat" style="display:none">Tìm kiếm</button>                                    
                                    <input id="ipDmc1" runat="server" style="display:none"/>
                                    <input id="ipDmc2" runat="server" style="display:none"/>
                                    <input id="ipCity" runat="server" style="display:none"/>
                                    <input id="ipDistrict" runat="server" style="display:none"/>
                                    <input id="ipWard" runat="server" style="display:none"/>
                                </div>
                            </div>
                        </div>
                        <div id="dvTinDang" style="overflow: auto" runat="server">
                        </div>
                        <%--<table class="table table-bordered table-striped">
                <tr>
                    <th class="th">
                        STT
                    </th>
                    <th class="th">
                        Tên loại khách hàng
                    </th>
                    <th class="th">
                        Ghi chú
                    </th>
                    <th id="command" class="th"></th>
                </tr>
                <tr>
                        <td>
                            1
                        </td>
                        <td>
                            Loại khách hàng 1
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <a href="#"><img class="imgedit" src="../images/edit.png" />Sửa</a>|
                            <a href="#"><img class="imgedit" src="../images/delete.png" />Xóa</a>
                        </td>
                    </tr>
            </table>
            <br />
            <div class="pagination-container" style="text-align:center">
                <ul class="pagination">
                    <li class="active"><a>1</a></li>
                    <li class=""><a>2</a></li>
                    <li class=""><a>3</a></li>
                    <li class=""><a>4</a></li>
                    <li class=""><a>5</a></li>
                </ul>
            </div>--%>
                    </div>
                </div>

         <div class="modal fade account-info" id="modalAccountInfo" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Thông tin TV</h4>
					</div>
					<div class="modal-body">
					    <div class="row">
                            <div class="col-md-3 profile-col">
                                <p>
                                    <img class="profile-img" id="modalAccountInfo_imgThumbnail" runat="server" src="" />
                                </p>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><label>Họ tên: </label> <span id="modalAccountInfo_spanFullName" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><label>Mã TV: </label> <span id="modalAccountInfo_spanCode" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><label>Loại TV: </label> <span id="modalAccountInfo_spanPro" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Số điện thoại: </label> <a id="modalAccountInfo_linkPhone" runat="server" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Email: </label> <a id="modalAccountInfo_linkEmail" runat="server" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Địa chỉ: </label> <span id="modalAccountInfo_spanAddr" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Đăng ký lúc: </label> <span id="modalAccountInfo_spanRegisterAt" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label> CMND/CCCD: </label> <span id="modalAccountInfo_spanTaxNumber" runat="server" >(Chưa có thông tin)</span></p>
                                    </div>
                                  <%--  <div class="col-md-12">
                                        <p><label>Ngày cấp: </label> <span id="modalAccountInfo_spanTaxCreatedDate">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Nơi cấp: </label> <span id="modalAccountInfo_spanTaxCreatedPlace">(Chưa có thông tin)</span></p>
                                    </div>--%>
					            </div>
                            </div>
					    </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Đóng</button>
					</div>
				</div>
			</div>
		</div>
            </section>


            <div class="modal fade account-info" id="modalAccountInfoLydo" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
						<h4 class="head-lydo">Lý do không duyệt bài đăng</h4>
					<div class="modal-body">
					    <div class="row">
                     <input id="txtLydo" data-val="true" data-val-required="" name="lydo" type="text" class="inputLydo" style="font-family:Arial,sans-serif;" placeholder="Lý do ..."/><button id="btnThemLiDo" onclick="ThemLyDo()" class="btn btn-success btn-flat btn-lydo"> + Thêm</button><br />                      
                    <p style="margin-top:5px;font-weight:bold;margin-left:20px">Chọn lý do:</p>
                    <ul class="Lydokd" id="htmlLydo" runat="server">
                        
                    </ul>
                    <a id="btnLuuLyDo" class="btn btn-primary btn-flat" style="float:right;margin-right:15px">Lưu</a>
                </div>
            </div>
            </div>    
            </div>  
            </div>    

            <!-- /.content -->
            <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
          <script type="text/javascript">
              $("#ContentPlaceHolder1_drdlCollab").select2();
                $("#ContentPlaceHolder1_slMaTin").select2();

            
              (function () {
            var defaultCity = "<%= templateFilterCity %>"; // defaultCity: tỉnh ban đàu defaultCity= dà nẵng
            var defaultDistrict = "<%= templateFilterDistrict %>"; // defaultDistrict = qhair châyy
            var defaultWard = "<%= templateFilterWard %>"; // mặc định xã


            // function render(vẽ) tỉnh thành phố
            var fnInitCities = function () {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLCities";
                var defValue = defaultCity;
                defaultCity = "";

                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () { 
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { // khi lấy về xong
                        $('#filterCity').html(xmlhttp.responseText);
                        $('#filterCity').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };

            // function vẽ huyện quận của tỉnh thành phố nào
            var fnInitDistricts = function (cityId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLDistricts";
                var defValue = defaultDistrict;
                defaultDistrict = "";
                if (cityId != undefined && cityId != null && cityId != "") {
                    url += "&CityId=" + cityId;
                    document.getElementById('dvqh').style.display = "";
                } else {
                    cityId = "";
                    document.getElementById('dvqh').style.display = "none";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterDistrict').html(xmlhttp.responseText);
                        $('#filterDistrict').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };

            // function vẽ xã phường
            var fnInitWards = function (districtId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLWards";
                var defValue = defaultWard;
                defaultWard = "";
                if (districtId != undefined && districtId != null && districtId != "") {
                    url += "&DistrictId=" + districtId;
                    document.getElementById('dvPX').style.display = "";
                } else {
                    districtId = "";
                    document.getElementById('dvPX').style.display = "none";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterWard').html(xmlhttp.responseText);
                        $('#filterWard').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };

            // khi thay đổi city
            $('#filterCity').on("change", function () {
                var value = $(this).val();
                fnInitDistricts(value);
            });

            // khi thay đổi district 
            $('#filterDistrict').on("change", function () {
                var value = $(this).val();
                fnInitWards(value);
            });

            // lần dầu tiên gọi function vẽ tỉnh thành phố
            fnInitCities();
        })();

            (function () {
            var defaultDMC1 = "<%= templateFilterDMC1 %>"; // defaultDMC1
            var defaultDMC2 = "<%= templateFilterDMC2 %>"; // defaultDMC2


            // function render(vẽ) DMC1
                var fnInitDMC1 = function () {                
                var url = "/Admin/adAjax.aspx?Action=GetHTMLDMC1";
                var defValue = defaultDMC1;
                defaultDMC1 = "";

                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () { 
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { // khi lấy về xong                        
                        $('#slLinhVuc').html(xmlhttp.responseText);
                        $('#slLinhVuc').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
          
                var fnInitDMC2 = function (DMC1Id) {                    
                var url = "/Admin/adAjax.aspx?Action=GetHTMLDMC2";
                var defValue = defaultDMC2;
                defaultDMC2 = "";
                if (DMC1Id != undefined && DMC1Id != null && DMC1Id != "") {
                    url += "&DMC1Id=" + DMC1Id;
                    document.getElementById('dvDMC2').style.display = "";
                } else {
                    DMC1Id = "";
                    document.getElementById('dvDMC2').style.display = "none";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#slLinhVuc2').html(xmlhttp.responseText);
                        $('#slLinhVuc2').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };            

            // khi thay đổi DMC1
            $('#slLinhVuc').on("change", function () {
                var value = $(this).val();
                fnInitDMC2(value);
            });
         

            fnInitDMC1();
        })();

                //------------------------------         

                var baseUrl = "/Admin/Posts/PostTable.aspx";
                $("#btnTimKiem").click(function () {
                    var filterDMC1 = $('#slLinhVuc').val();
                    var filterDMC2 = $('#slLinhVuc2').val();
                    var filterCity = $('#filterCity').val();
                    var filterDistrict = $('#filterDistrict').val();
                    var filterWard = $('#filterWard').val();
                     document.getElementById('<%= ipDmc1.ClientID %>').value = $('#slLinhVuc').val();
                    document.getElementById('<%= ipDmc2.ClientID %>').value = $('#slLinhVuc2').val();
                    document.getElementById('<%= ipCity.ClientID %>').value = $('#filterCity').val();
                    document.getElementById('<%= ipDistrict.ClientID %>').value = $('#filterDistrict').val();
                    document.getElementById('<%= ipWard.ClientID %>').value = $('#filterWard').val();                                                                                           
                });
                $("#<%= btTimKiem.ClientID%>").click(function () {
                    $("#btnTimKiem").click();
                });

                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });
                $('#ContentPlaceHolder1_txtCommissionCounterRange').ionRangeSlider({
                    min: 0,
                    max: 10000000,
                    type: 'double',
                    step: 5000,
                    postfix: "",
                    hasGrid: true,
                    prettifyFunc: function (num) {
                        //var milions = parseInt(num / 1000);
                        var le = parseInt(num % 1000);
                        var chan = parseInt(num / 1000);
                        var trieu = parseInt(chan / 1000);
                        var tram = chan % 1000;
                        var text = "";
                        if (chan > 1000 && le < 1000 && trieu % 1000) {
                            if (tram == 0) {
                                text += " " + trieu + " Triệu ";
                            }
                            else {
                                text += " " + trieu + " Triệu " + tram + " Nghìn";
                            }
                        }
                        else if (trieu / 1000) {
                            text += " " + trieu + " Triệu";
                        }
                        else if (chan < 1000) {
                            text += " " + chan + " Nghìn";
                        }
                        text = text.trim();
                        if (text == "") {
                            text = "0 Nghìn";
                        }
                        return text;
                    },
                });
                $('#ContentPlaceHolder1_txtFeeCounterRange').ionRangeSlider({
                    min: 0,
                    max: 10000000,
                    type: 'double',
                    step: 5000,
                    postfix: "",
                    hasGrid: true,
                    prettifyFunc: function (num) {
                        var le = parseInt(num % 1000);
                        var chan = parseInt(num / 1000);
                        var trieu = parseInt(chan / 1000);
                        var tram = chan % 1000;
                        var text = "";
                        if (chan > 1000 && le < 1000 && trieu % 1000) {
                            if (tram == 0) {
                                text += " " + trieu + " Triệu ";
                            }
                            else {
                                text += " " + trieu + " Triệu " + tram + " Nghìn";
                            }
                        }
                        else if (trieu / 1000) {
                            text += " " + trieu + " Triệu";
                        }
                        else if (chan < 1000) {
                            text += " " + chan + " Nghìn";
                        }
                        text = text.trim();
                        if (text == "") {
                            text = "0 Nghìn";
                        }
                        return text;
                    },
                });
                $('.date-picker').datepicker({
                    language: 'vi',
                    orientation: "left",
                    autoclose: true
                });
            </script>


           <script>
        $(document).ready(function () {
            $('.user-info-btn').click(function () {
                var accountType = $(this).data("account-type");
                var dataAccountId = $(this).data("account-id");
                var dataThumbnail = $(this).data("thumbnail");
                var dataFullname = $(this).data("fullname");
                var dataCode = $(this).data("code");
                var dataPro = $(this).data("pro");
                var dataPhone = $(this).data("phone");  //modalAccountInfo_spanPro
                var dataEmail = $(this).data("email");
                var dataAddr = $(this).data("addr");
                var dataRegisterAt = $(this).data("register-at");
                var dataTaxNumber = $(this).data("tax-number");
                var dataTaxCreatedDate = $(this).data("tax-created-date");
                var dataTaxCreatedPlace = $(this).data("tax-created-place");
                if (dataAccountId == undefined || dataAccountId == null || dataAccountId == "") {
                    return;
                }
                $('#ContentPlaceHolder1_modalAccountInfo_imgThumbnail').attr("src", (dataThumbnail != "" ? dataThumbnail : "#"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanFullName').text((dataFullname != "" ? dataFullname : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanCode').text((dataCode != "" ? dataCode : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanPro').text((dataPro != "" ? dataPro : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkPhone').text((dataPhone != "" ? dataPhone : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkPhone').attr("href", (dataPhone != "" ? ("tel:" + dataPhone) : "javascript:void(0);"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkEmail').text((dataEmail != "" ? dataEmail : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkEmail').attr("href", (dataEmail != "" ? "mailto:" + dataEmail : "javascript:void(0);"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanAddr').text((dataAddr != "" ? dataAddr : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanRegisterAt').text((dataRegisterAt != "" ? dataRegisterAt : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxNumber').text((dataTaxNumber != "" ? dataTaxNumber : "(Chưa có thông tin)"));
                //$('#ContentPlaceHolder1_modalAccountInfo_spanTaxCreatedDate').text((dataTaxCreatedDate != "" ? dataTaxCreatedDate : "(Chưa có thông tin)"));
                //$('#ContentPlaceHolder1_modalAccountInfo_spanTaxCreatedPlace').text((dataTaxCreatedPlace != "" ? dataTaxCreatedPlace : "(Chưa có thông tin)"));
                $('#modalAccountInfo').modal();
            });
        });
    </script>
        </div>
    </form>
</asp:Content>

