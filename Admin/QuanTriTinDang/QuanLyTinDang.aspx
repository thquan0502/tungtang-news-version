﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/adMasterPage.master" AutoEventWireup="true" CodeFile="QuanLyTinDang.aspx.cs" Inherits="Admin_QuanTriTinDang_QuanLyTinDang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script>
        function DuyetTinDang(idTinDang ,flag) {
            var Duyet = document.getElementById("slDuyet_" + idTinDang);
            if (Duyet.disabled == true) {
                document.getElementById("slDuyet_" + idTinDang).disabled = false;
                document.getElementById("btDuyet_" + idTinDang).innerHTML = "<img class='imgedit' src='../images/save.png'/>Lưu</a>";
            }
            else {
                if (Duyet.value == "KhongDuyet" && flag == undefined) {
                    $("#modalThongKe").modal({
                        fadeDuration: 200,
                        showClose: false
                    });
                    $("#btnLuuLyDo").attr("onclick", "DuyetTinDang(" + idTinDang + ",1)");
                }
                else {
                    var LyDo = $("#txtLyDoKhongDuyet").val();

                    var thongbao = '';
                    if (Duyet.value == "KhongDuyet") {
                        try {
                            LyDo = LyDo.trim();
                        }
                        catch (e) {

                        }
                        if (LyDo == '') {
                            thongbao += "Hãy nhập lý do";
                        }
                    }
                    if (thongbao == '') {
                        var xmlhttp;
                        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        }
                        else {// code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "True")
                                    window.location.reload();
                                else
                                    alert("Lỗi !");
                            }
                        }
                        xmlhttp.open("GET", "../adAjax.aspx?Action=DuyetTinDang&idTinDang=" + idTinDang + "&Duyet=" + Duyet.value + "&LyDo=" + LyDo, true);
                        xmlhttp.send();
                    }
                    else {
                        alert(thongbao);
                    }
                }
            }
        }
        function CapNhatHetHan(idTinDang) {
            var HetHan = document.getElementById("slHetHan_" + idTinDang);
            if (HetHan.disabled == true) {
                document.getElementById("slHetHan_" + idTinDang).disabled = false;
                document.getElementById("btHetHan_" + idTinDang).innerHTML = "<img class='imgedit' src='../images/save.png' style='width:25px; cursor:pointer'/>Lưu</a>";
            }
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Lỗi !")
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=CapNhatHetHan&idTinDang=" + idTinDang + "&HetHan=" + HetHan.value, true);
                xmlhttp.send();
            }
        }

        function MoXemChiTiet(idTinDang) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById('lightXemChiTiet').style.display = 'block';
                    document.getElementById('fadeXemChiTiet').style.display = 'block';
                    document.getElementById('dvXemChiTiet').innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=MoXemChiTietTinDang&idTinDang=" + idTinDang, true);
            xmlhttp.send();
        }
        function DongXemChiTiet() {
            document.getElementById('lightXemChiTiet').style.display = 'none';
            document.getElementById('fadeXemChiTiet').style.display = 'none';
        }
        function LenTop(TD) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "True") {
                        alert('Bài viết đã lên TOP');
                    }

                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=LenTop&TD=" + TD, true);
            xmlhttp.send();
        }
    </script>
	 <script>
       function DeleteTinDang(idTinDang) {
           if (confirm("Bạn có muốn xóa không ?")) {
               var xmlhttp;
               if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                   xmlhttp = new XMLHttpRequest();
               }
               else {// code for IE6, IE5
                   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
               }
               xmlhttp.onreadystatechange = function () {
                   if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                       if (xmlhttp.responseText == "True")
                           window.location.reload();
                   }
               }
               xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteTinDang&idTinDang=" + idTinDang, true);
               xmlhttp.send();
           }
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server" autocomplete="off">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="title">QUẢN LÝ TIN ĐĂNG</div>
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Từ ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTuNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Đến ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDenNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Thời hạn:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slHan" runat="server">
                                            <option value="0">-- Chọn --</option>
                                            <option value="1">Hết hạn</option>
                                            <option value="2">Còn hạn</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Tình trạng:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slTinhTrang" runat="server">
                                            <option value="-1">-- Chọn --</option>
                                            <option value="1">Đã duyệt</option>
                                            <option value="0">Không duyệt</option>
                                            <option value="2">Đang chờ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tên đăng nhập:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTenDangNhap" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput3" style="display:none">
                                    <div class="titleinput"><b>isHot:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slisHot" runat="server">
                                            <option value="-1">-- Tất cả --</option>
                                            <option value="True">Tin Hot</option>
                                            <option value="False">Tin không Hot</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Mã tin:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtMaTin" runat="server" name="Content.ContentName1" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Lĩnh vực:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slLinhVuc" runat="server">
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Người duyệt:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slNguoiDuyet" runat="server">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-9">
                                <%--<a class="btn btn-primary btn-flat" href="DanhMucLoaiKhachHang-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>--%>
                            </div>
                            <div class="col-sm-3">
                                <div style="text-align: right;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                                </div>
                            </div>
                        </div>
                        <div id="dvTinDang" style="overflow: auto" runat="server">
                        </div>
                        <%--<table class="table table-bordered table-striped">
                <tr>
                    <th class="th">
                        STT
                    </th>
                    <th class="th">
                        Tên loại khách hàng
                    </th>
                    <th class="th">
                        Ghi chú
                    </th>
                    <th id="command" class="th"></th>
                </tr>
                <tr>
                        <td>
                            1
                        </td>
                        <td>
                            Loại khách hàng 1
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <a href="#"><img class="imgedit" src="../images/edit.png" />Sửa</a>|
                            <a href="#"><img class="imgedit" src="../images/delete.png" />Xóa</a>
                        </td>
                    </tr>
            </table>
            <br />
            <div class="pagination-container" style="text-align:center">
                <ul class="pagination">
                    <li class="active"><a>1</a></li>
                    <li class=""><a>2</a></li>
                    <li class=""><a>3</a></li>
                    <li class=""><a>4</a></li>
                    <li class=""><a>5</a></li>
                </ul>
            </div>--%>
                    </div>
                </div>
            </section>


            <div class="modal-ThongKe-cls">
                <div id="modalThongKe" class="modal" style="overflow: hidden;">
                    <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">Lý do không duyệt bài đăng</div>
                    <textarea id="txtLyDoKhongDuyet" rows="3" style="width: 100%;"></textarea>
                    <a id="btnLuuLyDo" class="btn btn-primary btn-flat" style="float:right;">Lưu</a>
                </div>
            </div>

            <!-- /.content -->
            <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
            <script type="text/javascript">
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

            </script>
        </div>
    </form>
</asp:Content>

