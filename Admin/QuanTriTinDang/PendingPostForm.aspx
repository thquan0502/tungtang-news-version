﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="PendingPostForm.aspx.cs" Inherits="Admin_QuanTriTinDang_QuanLyTinDang_CapNhat" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="/Css/Page/Admin/QuanTriTinDang/TinChoDuyet-CapNhat.css" rel="stylesheet" />
    <script>
        window.onload = function () {
            if (document.getElementById('ContentPlaceHolder1_hdHinhAnh').value != "")
                window.scrollTo(0, document.body.scrollHeight);
            document.getElementById("search-widget-wrapper").style.display = "none";
            
        }
        $(function () {
            CKEDITOR.config.extraPlugins = 'justify';
        });
        function format_curency(a) {
            var money = a.value.replace(/\./g, "");
            a.value = money.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
        function XoaHinhAnh(id, TenAnh) {
            document.getElementById(id).style.display = "none";
            document.getElementById("ContentPlaceHolder1_hdHinhAnh").value = document.getElementById("ContentPlaceHolder1_hdHinhAnh").value.replace(TenAnh + "|~~~~|", "");
        }
    </script>
	<script>
        function ThemLyDo() {
            var txtLydo = $("#txtLydo").val();
            if (txtLydo == "") {
                alert("Vui lòng điền lý do không duyệt!");
            }
            else {                
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {                    
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                         alert("Thêm lý do thành công!");    
                         window.location.reload();
                        //$('#modalThongKe').load('PostTable.aspx #modalThongKe');
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=ThemLyDo&txtLydo=" + txtLydo, true);
                xmlhttp.send();                
            }
        }
    </script>
    <script>
        function SuaLyDo(idLydo)
        {            
            document.getElementById("hienlydo_" + idLydo).style.display = "";
        }
        function SuaLyDoKD(idLydo, txtLydo) {
            var id = document.getElementById("idLyDo_" + idLydo);
            var txtLydo = $("#txtSuaLydo_" + idLydo).val();
            if (txtLydo == "") {
                alert("Vui lòng điền lý do không duyệt!");
				 window.location.reload();
            }
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            alert("Sửa lý do thành công!");
                        window.location.reload();
                        //$('#modalThongKe').load('PostTable.aspx #modalThongKe');
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=SuaLyDoKD&idLydo=" + idLydo + "&txtLydo=" + txtLydo, true);
                xmlhttp.send();
            }
        }
    </script>
    <script>
       function DeleteLyDo(idLyDo) {
           if (confirm("Bạn có muốn xóa không ?")) {
               var xmlhttp;
               if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                   xmlhttp = new XMLHttpRequest();
               }
               else {// code for IE6, IE5
                   xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
               }
               xmlhttp.onreadystatechange = function () {
                   if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                       if (xmlhttp.responseText == "True")
                           window.location.reload();
                   }
               }
               xmlhttp.open("GET", "../adAjax.aspx?Action=DeleteLyDo&id=" + idLyDo, true);
               xmlhttp.send();
           }
       }
    </script>
    <style>
		.form-group{
            margin-bottom: 0px;
        }
        .Lydokd{
            list-style-type:none;
            margin-left:-40px;
        }
        .inputLydo[type=text] {
            margin-left:3px;
            margin-top:5px;
          width: 130px;
          box-sizing: border-box;
          border: 2px solid #ccc;
          border-radius: 4px;
          font-size: 14px;
          background-color: white;
          background-position: 10px 10px; 
          background-repeat: no-repeat;
          padding:6px 10px 8px 8px;
          -webkit-transition: width 0.4s ease-in-out;
          transition: width 0.4s ease-in-out;
        }
        .inputLydo[type=text]:focus{
            width:80%;
            border: 2px solid #ff9900;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        
    <form  runat="server">
        <div class="content-wrapper">
            <div class="title" id="dvTitle" runat="server">XEM CHI TIẾT TIN ĐĂNG</div>
            <div class="title1"><div id="linkback" runat="server"></div> <a href="TinChoDuyet.aspx"><i class="fa fa-step-backward"></i>Danh sách tin đăng</a></div>
            <!-- Main content -->
            <section class="content">
                <div class="box">
                    <div class="row">
                        <!--Top Jobs-->
                        <div class="col-md-12">
                            <div class="top-job">
                                <div class="panel-content" id="tabChoThue" style="padding: 10px">
                                    <div class="job-list scrollbar m-t-lg">
                                        <div class="form-group">
                                               <div class="row">
                                                    <div class="dvnull">&nbsp;</div>
                                                    <div class="coninput1">
                                                            <div class="titleinput">Danh Mục Cấp 1 <span style="color: red">(*)</span>:</div>
                                                        <div class="txtinput">                                                           
                                                                <asp:DropDownList ID="ddlLoaiDanhMuc" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddLoaiDanhMuc_SelectedIndexChanged"></asp:DropDownList>
                                                           </div>                                                       
                                                </div>
                                                   <div class="coninput2">
                                                            <div class="titleinput">Danh Mục Cấp 2 <span style="color: red">(*)</span>:</div>
                                                        <div class="txtinput">                                                           
                                                                <select data-search-input-placeholder="Tìm kiếm danh mục" data-placeholder="Tất cả" class="form-control" id="SlDanhMuc2" name="industry[]" runat="server" disabled="disabled"></select>
                                                           </div>                                                       
                                                </div>
                                                   
                                            </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="dvnull">&nbsp;</div>
                                                    <div class="coninput1">
                                                            <div class="titleinput">Tỉnh/ thành:</div>
                                                        <div class="txtinput">                                                           
                                                                <asp:DropDownList ID="ddlTinh" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTinh_SelectedIndexChanged"></asp:DropDownList>
                                                           </div>                                                       
                                                    </div>  
                                                    <div class="coninput2">
                                                    <div class="titleinput">Quận/ huyện: </div>
                                                    <div class="txtinput">
                                                        <select data-search-input-placeholder="Tìm kiếm huyện" data-placeholder="Tất cả" class="form-control" id="slHuyen" name="industry[]" runat="server" disabled="disabled"></select>
                                                    </div>
                                                </div>
                                                                                                
                                                </div>
                                            </div>  
                                               <div class="form-group">
                                                <div class="row">
                                                    <div class="dvnull">&nbsp;</div>
                                                    <div class="coninput2">
                                                    <div class="titleinput">Phường/ Xã: </div>
                                                    <div class="txtinput">
                                                        <select data-search-input-placeholder="Tìm kiếm Phường, xã" data-placeholder="Tất cả" class="form-control" id="slXa" name="industry[]" runat="server" disabled="disabled"></select>
                                                    </div>
                                                </div>   
                                                    </div>
                                            </div>          
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="dvnull">&nbsp;</div>
                                                    <div class="coninput1">
                                                            <div class="titleinput">Mã tin đăng:</div>
                                                        <div class="txtinput">                                                           
                                                                <input type="text" id="txtMaTinDang" style="width: 150px" class="form-control" runat="server" readonly />
                                                           </div>                                                       
                                                    </div> 
                                                    <div class="coninput2">
                                                    <div class="titleinput">Giá:</div>
                                                    <div class="txtinput">
                                                        <input type="text" id="txtTuGia" oninput="format_curency(this);" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                    </div>
                                                </div> 
                                                    
                                                </div>
                                            </div>  
                                         <div class="form-group">
                                                <div class="row">
                                                    <div class="dvnull">&nbsp;</div>
                                                      <div class="coninput1">
                                                    <div class="titleinput">Hoa hồng: </div>
                                                    <div class="txtinput">
                                                        <input type="text" id="txtCommission" oninput="format_curency(this);" style="width: 150px;" name="form[password]" placeholder="" tabindex="2" class="form-control hh" runat="server" readonly />
                                                        <input type="hidden" id="txtDenGia" oninput="format_curency(this);" style="width: 150px" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                    </div>
                                                </div>    
                                                 </div>
                                            </div>                                                        
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="dvnull">&nbsp;</div>
                                                    <div class="coninput1">
                                                            <div class="titleinput" style="color:red">Duyệt tin:</div>
                                                        <div class="txtinput">                                                           
                                                                <select class="form-control" id="slTinhTrang_" name="slTinhTrang" type="text"  onchange="thaydoi()" style='width:150px' runat="server">
                                                                    <option value="2">Đang chờ</option>
                                                                    <option value="1">Đã duyệt</option>
                                                                    <option value="0">Không duyệt</option>
                                                                </select>
                                                            <div id="txtLyDo_a" style="margin-top: 5px;border: 1px solid;border-radius: 5px;line-height: 22px;padding: 0 10px;background: #4cb050;color: white;display:none;"></div>
															 <div id="ListLyDo" style="margin-top: 5px;border: 1px solid;border-radius: 5px;line-height: 22px;padding: 0 10px;background: #4cb050;color: white;display:none;" runat="server"></div>
															 <p id="MessTinhTrang" runat="server" style="color:red;" visible="false" >Hãy chọn trạng thái duyệt tin đăng</p>
                                                                 <input type="hidden" id="txtldtc" />
                                                                 <input id="txtLyDoKD" type="hidden" runat="server" />
                                                                <script>
                                                                    function thaydoi(){
                                                                        var id = $("#ContentPlaceHolder1_slTinhTrang_").val();
                                                                        if (id == "0") {
                                                                            $("#anc").css("display", "");
                                                                            //$("#modalThongKe").modal({
                                                                            //    fadeDuration: 200,
                                                                            //    showClose: false
                                                                            //});
                                                                        }
                                                                        else {
																			 document.getElementById('<%= txtLyDoKD.ClientID %>').value = "";
                                                                            $("#anc").css("display", "none");
																		    $("#txtLyDo_a").css("display", "none");
																			 $("#ContentPlaceHolder1_ListLyDo").hide();																	
                                                                        }
                                                                        document.getElementById('btnLuuLyDo').onclick = function()
                                                                        {
                                                                            //$("#anc").css("display", "none");          
                                                                            var checkbox = document.getElementsByName('kd');
                                                                            var result = "";
                                                                            var thongbao = "";
                                                                            //var n = "";
                                                                            //  var arr = Array[i];

                                                                            var j = 1;
                                                                            for (var i = 0; i < checkbox.length; i++) {
                                                                                if (checkbox[i].checked === true) {    
                                                                                    result += j + '.' + checkbox[i].value + '|';
                                                                                    j++;
                                                                                }
                                                                                
                                                                            }
                                                                            if (result == '') {
                                                                               result += +j+ '.' + checkbox[0].value + '';
                                                                                document.getElementById('txtldtc').value = result;

                                                                                document.getElementById('<%= txtLyDoKD.ClientID %>').value = document.getElementById('txtldtc').value;
                                                                            }
                                                                            else {
                                                                               // console.log(arr);
                                                                                document.getElementById('txtldtc').value = result;

                                                                                document.getElementById('<%= txtLyDoKD.ClientID %>').value = document.getElementById('txtldtc').value;
                                                                                //txtLyDo_a.innerText = result;
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                        
                                                                    }
                                                                    if (document.getElementById('<%= txtLyDoKD.ClientID %>').value != "")
                                                                    {
                                                                        txtLyDo_a.innerHTML = (document.getElementById('<%= txtLyDoKD.ClientID %>').value).replaceAll('|', '<br />');
                                                                        $("#txtLyDo_a").css("display", "");
																		  $("#ContentPlaceHolder1_ListLyDo").hide();
                                                                    }

                                                                </script>
                                                           </div>                                                       
                                                    </div> 
                                                     <div class="coninput2" id="anc" style="border: 2px solid #555">                                                    
                                                       <div style="background-color: #000012; color:#ffffff; font-weight: bold; text-align: center; padding: 10px">Lý do không duyệt bài đăng</div>
                                                        <input id="txtLydo" data-val="true" data-val-required="" name="lydo" type="text" class="inputLydo" style="font-family:Arial,sans-serif;" placeholder="Lý do ..."/><button id="btnThemLiDo" onclick="ThemLyDo()" class="btn btn-success btn-flat" style="float:left;margin-top:5px"> + Thêm</button><br />                      
                                                        <p style="margin-top:5px;font-weight:bold">Chọn lý do:</p>
                                                        <ul class="Lydokd" id="htmlLydo" runat="server">
                        
                                                        </ul>                                                             
                                                    <button id="btnLuuLyDo" class="btn btn-primary btn-flat" style="float:right;">Chọn</button>                                                     
                                                </div>                                       
                                                </div>
                                            </div>                                                   
                                            </div>
                                        <div class="form-group" style="display: none;">
                                            <div class="col-md-10">
                                                <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">THÔNG TIN LIÊN HỆ</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="display: none;">
                                            <div class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin">Họ tên <span style="color: red">(*)</span>:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtHoTen" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                <div id="dvHoTen" style="color: red;" runat="server"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin">Email:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtEmail" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                <div id="dvEmail" style="color: red;" runat="server"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="display: none;">
                                            <div class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin">Số điện thoại <span style="color: red">(*)</span>:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtSoDienThoai" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                <div id="dvSoDienThoai" style="color: red;" runat="server"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="form-group" style="display: none;">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin">Địa chỉ:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtDiaChi" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">NGƯỜI ĐĂNG TIN</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12">
                                                            </td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <p>Họ tên: <strong id="spanOwnerFullname" runat="server"></strong></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p>Mã thành viên: <strong id="spanOwnerCode" runat="server"></strong></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p>Loại Thành Viên: <strong id="spanOwnerPro" runat="server"></strong></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p>Thời gian đăng ký <strong id="spanOwnerCreatedAt" runat="server"></strong></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <p>Số điện thoại: <strong><a href="" id="spanOwnerPhone" runat="server"></a></strong></p>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <p>Email: <strong><a href="" id="spanOwnerEmail" runat="server"></a></strong></p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                        <p>Địa chỉ: <strong id="spanOwnerAddress" runat="server"></strong></p>
                                                                    </div>
                                                                </div>
																
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">NỘI DUNG TIN ĐĂNG</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12">Tiêu đề <span style="color: red">(*)</span>:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtTieuDe" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                <div id="dvTieuDe" style="color: red;" runat="server"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-12">
                                            <div class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12">Đường dẫn <span style="color: red">(*)</span>:
                                                            </td>
                                                            <td>
                                                                <input type="text" id="txtDuongDan" name="form[password]" placeholder="" tabindex="2" class="form-control" runat="server" />
                                                                <div id="dvDuongDan" style="color: red;" runat="server"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div style="height: auto">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12" style="vertical-align: baseline;">Nội dung <span style="color: red">(*)</span>:
                                                            </td>
                                                            <td>

                                                                <textarea id="txtNoiDung" runat="server" style="width: 100%; height: 300px;"></textarea>
                                                                <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="dvConHinhAnh" class="form-group">
                                                <div>
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12">Thêm hình ảnh:
                                                            </td>
                                                            <td>
                                                                <div class="BgUploadAnh">
                                                                    <div>
                                                                        <asp:FileUpload ID="fileHinhAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" AllowMultiple="true" Style="display: none;" />
                                                                    </div>
                                                                    <div style="padding: 10px 0px; height: auto; overflow: hidden; display: -webkit-inline-box;" id="dvHinhAnh" runat="server">
                                                                        <%--<div class="imgupload">
                                                                    <p style="margin:0px"><img id="imgAnhCuaBan" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                                    <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                                                </div>
                                                                <div class="imgupload">
                                                                    <p style="margin:0px"><img id="img1" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                                    <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                                                </div>--%>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group" style="">
                                                <div style="height: auto">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12" style="vertical-align: baseline;">
                                                                <label for="ContentPlaceHolder1_ckisHot">isHot</label>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" id="ckisHot" runat="server" />
                                                                <label for="ContentPlaceHolder1_ckisHot">isHot</label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group" style="">
                                                <div style="height: auto">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12" style="vertical-align: baseline;">
                                                                <label for="ContentPlaceHolder1_ckisHot">Cộng tác viên</label>
                                                            </td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="page-referrals-list">
                                                                            <ul class="list-group">
                                                                                <asp:Repeater runat="server" ID="repeaterReferrals">
                                                                                    <ItemTemplate>
                                                                                        <%# Container.DataItem %>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group" style="">
                                                <div style="height: auto">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametdadmin12" style="vertical-align: baseline;">
                                                                    <a class="btn btn-primary btn-block" onclick="isDuyet1()">Sửa tin</a>
                                                            </td>
                                                            <td>
                                                                   
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                                                   
                                                <asp:Button ID="btDangTin" runat="server" Text="SỬA TIN" class="btn btn-primary btn-block" Style="display:none; width: 110px; padding: 10px; margin: auto;" OnClick="btDangTin_Click" />
                                                    
                                             <script>
                                                
                                                function isDuyet1() {
                                                    var id = $("#ContentPlaceHolder1_slTinhTrang_").val();
                                                    if (id == "0") {                                                        
                                                            document.getElementById("ContentPlaceHolder1_btDangTin").click();
                                                        
                                                    }
                                                    else {
                                                        document.getElementById("ContentPlaceHolder1_btDangTin").click();
                                                    }
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="hdHinhAnh" runat="server" />
                </div>
        
            </section>
        </div>
        <asp:TextBox ID="fakeTextBoxReferralForAccountId" style="display:none;" runat="server"></asp:TextBox>
        <asp:Button ID="fakeButtonReferralAprove" runat="server" OnClick="fakeButtonReferralAprove_click" />
    </form>

    <script>
		 $('input[type="checkbox"]').on('change', function () {
            $(this).siblings('input[type="checkbox"]').prop('checked', false);
            });
        var id = $("#ContentPlaceHolder1_slDuyet").val();
        if (id == "KhongDuyet") {
            $("#anc").css("display", "");
        }
        else {
            $("#anc").css("display", "none");
        }
        function confirmReferral(referralId) {
            $("#ContentPlaceHolder1_fakeTextBoxReferralForAccountId").val(referralId);
            $("#ContentPlaceHolder1_fakeButtonReferralAprove").click();
        }
        //$(document).ready(function () {
        //    CKEDITOR.replace('ContentPlaceHolder1_txtNoiDung', {
        //        htmlEncodeOutput: true,
        //    });
        //});
        (function () {
            $('#ContentPlaceHolder1_txtCommission').trigger('input');
        })();
    </script>
</asp:Content>
