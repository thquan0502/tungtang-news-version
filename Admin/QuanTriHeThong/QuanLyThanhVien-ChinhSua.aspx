﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="QuanLyThanhVien-ChinhSua.aspx.cs" Inherits="Admin_QuanTriHeThong_QuanLyThanhVien_ChinhSua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        function LuuQuyen(idNhomQuyen)
        {
            var check = document.getElementById('chk_' + idNhomQuyen).checked;
            if (check == true)
            {
                check = "LUU";
            }
            else {
                check = "BO";
            }

            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                  
                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=LuuQuyen&idNhomQuyen=" + idNhomQuyen + "&check=" + check, true);
            xmlhttp.send();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="title" id="dvTitle" runat="server">THÊM DANH MỤC CẤP</div>
        <section class="content">
            <div class="box">
                <div class="box-body">
                    <form class="form-horizontal" runat="server">
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Họ tên(*):</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtHoTen" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Số điện thoại:</b></div>
                                    <div class="txtinput">
                                        <input id="txtSDT" class="form-control" runat="server" onkeypress="onlyNumber(event);" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Email:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" id="txtEmail" runat="server"  type="text"  />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Địa chỉ: </b></div>
                                    <div class="txtinput">
                                        <input id="txtDiaChi" class="form-control" runat="server" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Chọn quyền</b></div>
                                   
                                </div>
                             </div>
                        </div>

                        <left><b></b></left>
                         <div id="divQuyen" runat="server">

                         </div>
                        <br />
                        <div class="form-group" style="display:none;">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Quyền:</b></div>
                                    <div class="txtinput"> 


                                       



                                        <select class="form-control" id="slQuyen" runat="server" style="display:none;"></select>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tên đăng nhập(*):</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTenDangNhap" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Mật khẩu(*): </b></div>
                                    <div class="txtinput">
                                        <input id="txtMatKhau" class="form-control" runat="server"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btLuu" runat="server" Text="LƯU" class="btn btn-primary btn-flat" OnClick="btLuu_Click" />
                            <asp:Button ID="btHuy" runat="server" Text="HỦY" class="btn btn-primary btn-flat" OnClick="btHuy_Click" />
                        </div>
                        <div id="ShowLoi" runat="server" style="color:red;font-weight:bold;">

                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
</asp:Content>
