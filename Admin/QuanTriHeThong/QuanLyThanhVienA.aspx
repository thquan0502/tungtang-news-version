﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="QuanLyThanhVienA.aspx.cs" Inherits="Admin_QuanLyThanhVien_QuanLyThanhVienA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function DuyetTinDang(idThanhVien) {
            var Duyet = document.getElementById("ckKhoa_" + idThanhVien);
            if (Duyet.disabled == true) {
                document.getElementById("ckKhoa_" + idThanhVien).disabled = false;
                document.getElementById("btKhoa_" + idThanhVien).innerHTML = "<img class='imgedit' src='../images/save.png'/>Lưu</a>";
            }
            else {
                var xmlhttp;
                if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Lỗi !");
                    }
                }
                xmlhttp.open("GET", "../adAjax.aspx?Action=KhoaThanhVien&idThanhVien=" + idThanhVien + "&Khoa=" + Duyet.checked, true);
                xmlhttp.send();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="title">QUẢN LÝ THÀNH VIÊN</div>
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div class="titleinput"><b>Họ tên:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtHoTen" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Số điện thoại:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtSoDienThoai" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div class="titleinput"><b>Từ ngày đăng ký:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTuNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Đến ngày đăng ký:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDenNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tổng thành viên</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTongThanhVien" runat="server"  readonly type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Tỉnh/Thành:</b></div>
                                    <div class="txtinput">
                                        <select id="slTinhThanh" runat="server"  class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9">
                                <%--<a class="btn btn-primary btn-flat" href="DanhMucLoaiKhachHang-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>--%>
                            </div>
                            <div class="col-sm-3">
                                <div style="text-align: right;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                                </div>
                            </div>
                        </div>
                        <div id="dvThanhVien" style="overflow: auto" runat="server">
                        </div>
                    </div>
                </div>
            </section>


            <!-- /.content -->
            <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
            <script src="../plugins/select2/select2.full.min.js"></script>
            <link href="../plugins/select2/select2.min.css" rel="stylesheet" />
            <script>
                $("#ContentPlaceHolder1_slTinhThanh").select2();
                $('#ContentPlaceHolder1_txtTuNgay,#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                });
            </script>
        </div>
    </form>
</asp:Content>
