﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLyThanhVien_QuanLyThanhVienA : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sHoTen = "";
    string sSoDienThoai = "";
    string TinhThanh = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
              //  Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            Load_SelectHTML("select  * from City order by type asc,Ten asc", "Ten", "id", true, "-- Tất cả --", slTinhThanh);
            
            try
            {
                if (Request.QueryString["HoTen"].Trim() != "")
                {
                    sHoTen = Request.QueryString["HoTen"].Trim();
                    txtHoTen.Value = sHoTen;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["SoDienThoai"].Trim() != "")
                {
                    sSoDienThoai = Request.QueryString["SoDienThoai"].Trim();
                    txtSoDienThoai.Value = sSoDienThoai;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }

            try
            {
                TinhThanh = Request.QueryString["TT"].Trim();
                slTinhThanh.Value = TinhThanh;
            }
            catch { }
           
            
            LoadTinDang();
        }
    }

    void Load_SelectHTML(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        slTinhThanh.DataSource = table;
        slTinhThanh.DataTextField = TextField;
        slTinhThanh.DataValueField = ValueField;
        slTinhThanh.DataBind();

        if (AddANewItem)
        {
            slTinhThanh.Items.Insert(0, (new ListItem(ItemName, "")));
            slTinhThanh.Items.FindByText(ItemName).Selected = true;
        }
    }
    #region paging
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string sql = "";
        sql += @"select tb1.* from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY TotalCommission desc
                  ) AS RowNumber, 

                      City.Ten as CityName,
                      District.Ten as DistrictName,
                      tb_PhuongXa.Ten as WardName,
                      ISNULL(TbReferrals.totalcommission, 0)     AS TotalCommission,

                    tb_ThanhVien.*
                  from tb_ThanhVien 
                	inner join City on tb_ThanhVien.idTinh = City.id
	                inner join District on tb_ThanhVien.idHuyen = District.id
	                inner join tb_PhuongXa on tb_ThanhVien.idPhuongXa = tb_PhuongXa.id
                    LEFT JOIN (SELECT referrals.collabid        AS CollabId,
                                 Sum(referrals.commission) AS TotalCommission
                          FROM   referrals
                          GROUP  BY referrals.collabid) AS TbReferrals
                      ON tb_thanhvien.idthanhvien = TbReferrals.collabid
where 1 = 1 
            ";
        if (sHoTen != "")
            sql += " and TenCuaHang like N'%" + sHoTen + "%'";
        if (TinhThanh != "")
            sql += " and idTinh=" + TinhThanh;
        if (sTuNgay != "")
            sql += " and NgayDangKy >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDangKy <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (sSoDienThoai != "")
            sql += " and SoDienThoai like '%" + sSoDienThoai + "%'";
        sql += ") as tb1";

        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += " WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";


        DataTable table = Connect.GetTable(sql);
        txtTongThanhVien.Value = AllRowNumber.ToString();
        SetPage(AllRowNumber);
        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>STT</th>
                                <th class='th'>Mã thành viên</th>
                                <th class='th'>Tên thành viên</th>
                                <th class='th'>Ảnh đại diện</th>
                                <th class='th'>Địa chỉ</th>
                                <th class='th'>Số điên thoại</th>
                                <th class='th'>Email</th>
                                <th class='th'> Tiền hoa hồng</th>
                                <th class='th'>Tên đăng nhập</th>
                                <th class='th'>Mật khẩu</th>
                                <th class='th'>isKhoa</th>
                                <th class='th'>Ngày đăng ký</th>
                                <th class='th'></th>
                                <th class='th'></th>
                            </tr>";

        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += " <tr>";
            html += "       <td>" + (((Page - 1) * PageSize) + i + 1).ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["MaThanhVien"] + "</td>";
            html += "       <td>" + table.Rows[i]["TenCuaHang"] + "</td>";
            html += "       <td><img src='../../Images/User/" + table.Rows[i]["LinkAnh"] + "' style='width:70px'/></td>";
            html += "       <td>"+ table.Rows[i]["DiaChi"]+ ", " + table.Rows[i]["WardName"].ToString() + ", " + table.Rows[i]["DistrictName"].ToString() + ", " + table.Rows[i]["CityName"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["SoDienThoai"] + "</td>";
            html += "       <td>" + table.Rows[i]["Email"] + "</td>";
            html += "       <td><a href='/Admin/Referral/ReferralTableByCollab.aspx?AccountId="+ table.Rows[i]["idThanhVien"].ToString() + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["TotalCommission"].ToString()) + "</a></td>";
            html += "       <td>" + table.Rows[i]["TenDangNhap"] + "</td>";
            html += "       <td>" + table.Rows[i]["MatKhau"] + "</td>";
            if (table.Rows[i]["isKhoa"].ToString() == "True")
                html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled checked/></td>";
            else
                html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled/></td>";
            try
            {
                html += "       <td>" + StaticData.ConvertMMDDYYtoDDMMYY(table.Rows[i]["NgayDangKy"].ToString()) + "</td>";
            }
            catch
            {
                html += "       <td></td>";
            }
            html += "       <td><a id='btKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' style='cursor:pointer' onclick='DuyetTinDang(\"" + table.Rows[i]["idThanhVien"].ToString() + "\")'><img class='imgedit' src='../images/edit.png'/>Sửa</a></td>";
            html += "       <td><a href='/Admin/Posts/PostTable.aspx?TenDangNhap=" + table.Rows[i]["TenDangNhap"].ToString() + "'>Xem tin đăng</a></td>";
            html += "       </tr>";
        }
        html += "   <tr>";
        html += "       <td colspan='17' class='footertable'>";
        string url = "QuanLyThanhVienA.aspx?";
        if (sHoTen != "")
            url += "HoTen=" + sHoTen + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";
        if (sSoDienThoai != "")
            url += "SoDienThoai=" + sSoDienThoai + "&";
        if (TinhThanh != "")
            url += "TT=" + TinhThanh + "&";
        
        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvThanhVien.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string HoTen = txtHoTen.Value.Trim();
        string SoDienThoai = txtSoDienThoai.Value.Trim();
        string idTinhThanh = slTinhThanh.Value.Trim();

        string url = "QuanLyThanhVienA.aspx?";
        if (HoTen != "")
            url += "HoTen=" + HoTen + "&";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (SoDienThoai != "")
            url += "SoDienThoai=" + SoDienThoai + "&";
        if (idTinhThanh != "")
            url += "TT=" + idTinhThanh + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "QuanLyThanhVienA.aspx";
        Response.Redirect(url);
    }
}