﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriHeThong_QuanLyThanhVien_ChinhSua : System.Web.UI.Page
{
    string idAdmin = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
             //   Response.Redirect("../Home/Default.aspx");




        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            idAdmin = Request.QueryString["iAM"];
        }
        catch { }

        if (!IsPostBack)
        {
            txtMatKhau.Attributes["type"] = "password";
            //txtMatKhau.
            Load_SelectHTML("select * from tb_LoaiAdmin", "TenLoaiAdmin", "idLoaiAdmin", true, "-- Chọn --", slQuyen);
            LoadThongTinAdmin();
        }
    }
    void Load_SelectHTML(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        select.DataSource = table;
        select.DataTextField = TextField;
        select.DataValueField = ValueField;
        select.DataBind();

        if (AddANewItem)
        {
            select.Items.Add(new ListItem(ItemName, ""));
            select.Items.FindByText(ItemName).Selected = true;
        }
    }
    public void LoadQuyen()
    {
        DataTable table = Connect.GetTable("select * from tb_NhomQuyen");
        string html = "";
        if(table.Rows.Count > 0)
        {
             html += "<center><table style='width:70%;'>";
            for(int i=0;i<table.Rows.Count ;i++)
            {
                html += "<tr>";
                html += "<td><input type='checkbox' id='chk_" + table.Rows[i]["idNhomQuyen"].ToString() + "' onchange='LuuQuyen(" + table.Rows[i]["idNhomQuyen"].ToString() + ")' /> " + table.Rows[i]["TenNhomQuyen"].ToString() + "</td>";
                html += "</tr>";
            }

            html += "</table></center>";
        }

        divQuyen.InnerHtml = html;
    }
    void LoadThongTinAdmin()
    {
        if (Request.Cookies["cookie_PhanQuyen"] == null)
        {
            HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
            cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie_PhanQuyen);
        }
        if (idAdmin == "")
        {
            dvTitle.InnerHtml = "THÊM MỚI ADMIN";

            LoadQuyen();


        }
        else
        {
            DataTable table = Connect.GetTable("select * from tb_Admin where idAdmin='" + idAdmin + "'");
            if (table.Rows.Count > 0)
            {
                dvTitle.InnerHtml = "CẬP NHẬT ADMIN";
                txtHoTen.Value = table.Rows[0]["HOTen"].ToString();
                txtSDT.Value = table.Rows[0]["SDT"].ToString();
                txtEmail.Value = table.Rows[0]["Email"].ToString();
                txtDiaChi.Value = table.Rows[0]["DiaChi"].ToString();
                slQuyen.Value = table.Rows[0]["idLoaiAdmin"].ToString();
                txtTenDangNhap.Value = table.Rows[0]["TenDangNhap"].ToString();
                txtMatKhau.Value = table.Rows[0]["MatKhau"].ToString();

                DataTable tableQuyen = Connect.GetTable(@"
                SELECT [idChiTietQuyenAdmin]
                      ,[idAdmin]
                      ,[idNhomQuyen]
                  FROM [tb_ChiTietQuyenAdmin] where idAdmin='" + idAdmin + "' ");
                string cong = "";
                if(tableQuyen.Rows.Count > 0)
                {
                    for(int j=0;j < tableQuyen.Rows.Count;j++)
                    {
                        cong += tableQuyen.Rows[j]["idNhomQuyen"].ToString()+"," ;
                    }
                }

                DataTable tableNhomQuyen = Connect.GetTable("select * from tb_NhomQuyen");
                string html = "";
                if (tableNhomQuyen.Rows.Count > 0)
                {
                    html += "<center><table style='width:70%;'>";
                    for (int i = 0; i < tableNhomQuyen.Rows.Count; i++)
                    {
                        html += "<tr>";
                        string sqlCheckQuyenTonTai
                            = @"select * from [tb_ChiTietQuyenAdmin] where idAdmin='" + idAdmin + "' and idNhomQuyen= '" + tableNhomQuyen.Rows[i]["idNhomQuyen"].ToString() + "' ";
                        DataTable tbCheckThuNhomQuyen
                            = Connect.GetTable(sqlCheckQuyenTonTai);
                        if (tbCheckThuNhomQuyen.Rows.Count > 0)
                        {
                            html += "<td><input checked='checked' type='checkbox' id='chk_" + tableNhomQuyen.Rows[i]["idNhomQuyen"].ToString() + "' onchange='LuuQuyen(" + tableNhomQuyen.Rows[i]["idNhomQuyen"].ToString() + ")' /> " + tableNhomQuyen.Rows[i]["TenNhomQuyen"].ToString() + "</td>";
                        }
                        else
                        html += "<td><input type='checkbox' id='chk_" + tableNhomQuyen.Rows[i]["idNhomQuyen"].ToString() + "' onchange='LuuQuyen(" + tableNhomQuyen.Rows[i]["idNhomQuyen"].ToString() + ")' /> " + tableNhomQuyen.Rows[i]["TenNhomQuyen"].ToString() + "</td>";


                        html += "</tr>";
                    
                    
                    }
                    html += "</table></center>";
                }

                divQuyen.InnerHtml = html;

                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", cong);
                cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_PhanQuyen);

            }
            else
            {
                dvTitle.InnerHtml = "THÊM MỚI ADMIN";
                LoadQuyen();
            }
        }
    }
    protected void btHuy_Click(object sender, EventArgs e)
    {
        Response.Redirect("QuanLyThanhVien.aspx");
    }

    protected void btLuu_Click(object sender, EventArgs e)
    {
        string HoTen = txtHoTen.Value.Trim();
        string SDT = txtSDT.Value.Trim();
        string Email = txtEmail.Value.Trim();
        string DiaChi = txtDiaChi.Value.Trim();
        string TenDangNhap = txtTenDangNhap.Value.Trim();
        string MatKhau = txtMatKhau.Value.Trim();
        string idLoaiAdmin = slQuyen.Value.Trim();
        string thongbao = "";
        if (HoTen == "")
        {
            thongbao += "Vui lòng nhập Họ tên<br />";
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng nhập Họ tên !')", true);
            //return;
        }
        if (TenDangNhap == "")
        {
            thongbao += "Vui lòng nhập Tên đăng nhập<br />";
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng nhập Tên đăng nhập !')", true);
           // return;
        }
        if (MatKhau == "")
        {
            thongbao += "Vui lòng nhập Mật khẩu<br />";
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng nhập Mật khẩu !')", true);
            //return;
        }
        //if (idLoaiAdmin == "")
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Vui lòng chọn quyền !')", true);
        //    return;
        //}
        if(thongbao != "")
        {
            ShowLoi.InnerHtml = thongbao;
            HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
            cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie_PhanQuyen);

           // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + thongbao + " !')", true);
            return;
        }

        if (idAdmin == "" || idAdmin == null)//THEM MOI
        {
            string TenDangNhap_DB = StaticData.getField("tb_ADmin", "TenDangNhap", "TenDangNhap", TenDangNhap).Trim();
            if (TenDangNhap_DB != "")
            {
                ShowLoi.InnerHtml = "Tên đăng nhập đã tồn tại !";
                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
                cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_PhanQuyen);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Tên đăng nhập đã tồn tại !')", true);
                return;
            }

            if (Connect.Exec("insert into tb_Admin(HoTen,SDT,Email,DiaChi,TenDangNhap,MatKhau,idLoaiAdmin) values(N'" + HoTen + "',N'" + SDT + "',N'" + Email + "',N'" + DiaChi + "',N'" + TenDangNhap + "',N'" + MatKhau + "',NULL)"))
            {

                DataTable tableGetidAdmin = Connect.GetTable("select top 1 idAdmin from tb_Admin order by idAdmin desc");
                string newidAdmin = tableGetidAdmin.Rows[0]["idAdmin"].ToString();
                if (Request.Cookies["cookie_PhanQuyen"] != null)
                {

                    string a = HttpContext.Current.Request.Cookies["cookie_PhanQuyen"].Value;

                    string[] array = a.Split(',');
                    string query = "";
                    for (int k = 0; k < array.Length - 1; k++)
                    {
                        string idNhomQuyen = array[k];


                        DataTable tbKiemTra = Connect.GetTable(@"SELECT  [idChiTietQuyenAdmin]
                          ,[idAdmin]
                          ,[idNhomQuyen]
                      FROM [tb_ChiTietQuyenAdmin] where idAdmin='" + newidAdmin + "' and idNhomQuyen='" + idNhomQuyen + "' ");

                        if (tbKiemTra.Rows.Count > 0)
                        {

                        }
                        else
                        {
                            query = @" insert into tb_ChiTietQuyenAdmin([idAdmin],[idNhomQuyen]) values('" + newidAdmin + "','" + idNhomQuyen + "') ";


                            if (query != "")
                            {
                                bool rs = Connect.Exec(query);
                            }
                        }
                    }


                    HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
                    cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                    Response.Cookies.Add(cookie_PhanQuyen);
                }
                Response.Redirect("QuanLyThanhVien.aspx");
            }
            else
            {
                ShowLoi.InnerHtml = "Lỗi sửa admin !";
                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
                cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_PhanQuyen);
               // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi thêm admin !')", true);
            }
        }
        else//CAP NHAT
        {
            string TenDangNhap_OLD = StaticData.getField("tb_ADmin", "TenDangNhap", "idAdmin", idAdmin).Trim();
            string TenDangNhap_DB = StaticData.getField("tb_ADmin", "TenDangNhap", "TenDangNhap", TenDangNhap).Trim();
            if (TenDangNhap_DB != "" && TenDangNhap_DB != TenDangNhap_OLD)
            {
                ShowLoi.InnerHtml = "Tên đăng nhập đã tồn tại !";
                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
                cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_PhanQuyen);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Tên đăng nhập đã tồn tại !')", true);
                return;
            }
            if (Connect.Exec("update tb_Admin SET   HoTen = N'" + HoTen + "',SDT = N'" + SDT + "',Email = N'" + Email + "',DiaChi = N'" + DiaChi + "',TenDangNhap = N'" + TenDangNhap + "',MatKhau = N'" + MatKhau + "' where idAdmin=" + idAdmin))
            {

                if (Request.Cookies["cookie_PhanQuyen"] != null)
                {
                    string sqlXoaQuyen = "delete from tb_ChiTietQuyenAdmin where idAdmin = '" + idAdmin + "' ";
                    bool rsXoaQuyen = Connect.Exec(sqlXoaQuyen);
                    string a = HttpContext.Current.Request.Cookies["cookie_PhanQuyen"].Value;

                    string[] array = a.Split(',');
                    string query = "";
                    for (int k = 0; k < array.Length - 1; k++)
                    {
                        string idNhomQuyen = array[k];
                        query = @" insert into tb_ChiTietQuyenAdmin([idAdmin],[idNhomQuyen]) values('" + idAdmin + "','" + idNhomQuyen + "') ";
                        DataTable tbKiemTra = Connect.GetTable(@"SELECT  [idChiTietQuyenAdmin]
                          ,[idAdmin]
                          ,[idNhomQuyen]
                      FROM [tb_ChiTietQuyenAdmin] where idAdmin='" + idAdmin + "' and idNhomQuyen='" + idNhomQuyen + "' ");

                        if (tbKiemTra.Rows.Count > 0)
                        {

                        }
                        else
                        {
                            query = @" insert into tb_ChiTietQuyenAdmin([idAdmin],[idNhomQuyen]) values('" + idAdmin + "','" + idNhomQuyen + "') ";


                            if (query != "")
                            {
                                bool rs = Connect.Exec(query);
                            }
                        }
                    }


                    HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
                    cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                    Response.Cookies.Add(cookie_PhanQuyen);
                }

                Response.Redirect("QuanLyThanhVien.aspx");
            }
            else
            {
                ShowLoi.InnerHtml = "Lỗi sửa admin !!";
                HttpCookie cookie_PhanQuyen = new HttpCookie("cookie_PhanQuyen", "");
                cookie_PhanQuyen.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(cookie_PhanQuyen);



              //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lỗi sửa admin !')", true);

            }

        }
    }
}