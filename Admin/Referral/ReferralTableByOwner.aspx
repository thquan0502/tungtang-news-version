﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="ReferralTableByOwner.aspx.cs" Inherits="Admin_Referral_ReferralTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
      <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="../../asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>
    <style>	
        @media (max-width:600px) {	
        }	
        @media (min-width:992px) {	
            .rangetl{	
                margin-bottom:32px	
            }	
        }	
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content-wrapper">
            <section class="content">
                <h1  class="title" id="spanTitleUsername" runat="server"></h1>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
							<div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mã TĐ</label>
                                     <select id="SlTD" runat="server"  class="form-control"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Bài đăng</label>
                                    <asp:TextBox ID="txtPostTitle" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Trạng thái</label>
                                    <asp:DropDownList ID="drdlReferralStatus" runat="server"  CssClass="form-control" autocomplete="off">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Thời gian duyệt</label>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                <asp:TextBox ID="txtApprovedAtFrom" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
										        <span class="input-group-addon"> đến </span>
										        <asp:TextBox ID="txtApprovedAtTo" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
									        </div>
                                        </div>
                                        <%--<div class="col-md-4">
                                            <asp:DropDownList ID="drdlTypeAprrovedAt" runat="server"  CssClass="form-control" autocomplete="off">
                                                <asp:ListItem Value="All" Text="Tất cả" />
                                                <asp:ListItem Value="ReferralOwnerApprovedAt" Text="Người đăng bài" />
                                                <asp:ListItem Value="ReferralAdminApprovedAt" Text="Tung Tăng" />
                                                <asp:ListItem Value="ReferralCollabApprovedAt" Text="CTV" />
                                            </asp:DropDownList>
                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-md-4">
                                <div id="checkbox-container">                                      	
                                        <div>	
                                <label class="control-label">Hoa hồng</label><br />	
                                <label style="font-weight:bold"><input type="checkbox" id="chonruler" name="check" value="Chọn"/> Chọn Range lọc</label>
                                <asp:TextBox ID="txtReferralCommission" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
                                            <a id="textLX" style="color:blue">>>Thêm bộ lọc<<</a>	
                                        <a id="textLXAN" style="color:blue;display:none">>>Ẩn<<</a>	
                                        <div id="BoLocLX" style="display:none;">	
                                       <div class="col-12" style="font-weight:bold">	
                                           <input type="checkbox" id="ckb10100" name="check" value="Từ 10 đến 100"/> Từ 10 - 100 triệu	
                                       </div>	
                                        <div class="col-12" style="font-weight:bold">	
                                        <input type="checkbox" id="ckb100500" name="check" value="Từ 100 đến 500" /> Từ 100 - 500 triệu	
                                        </div>	
                                        <div class="col-12" style="font-weight:bold">	
                                        <input type="checkbox" id="ckb500" name="check" value="Từ 500 trở lên" /> Trên 500 triệu	
                                        </div>	
                                        </div>      	
                                        <div style="margin-top:9px;">	
                                            <input type="button" id="btn" value="Áp dụng Hoa Hồng" class="btn btn-success btn-flat"/>	
                                             <p id="message" style="color:#ffba00"></p>	
                                        </div>	
                                        </div>	
                                        	
                                                                              	
                                                                       	
                                        <input type="hidden" id="txttest" />	
                                        <input type="hidden" id="txtRangeHoaHong" runat="server" />	
                                        <p id="ruler" runat="server" visible="false"></p>	
                                        	
                                         <script>	
                                            document.getElementById('btn').onclick = function()	
                                            {	
                                                document.getElementById('message').innerText = "Đã áp dụng";	
                                                // Khai báo tham số	
                                                var checkbox = document.getElementsByName('check');	
                                                var result = "";	
                 	
                                                // Lặp qua từng checkbox để lấy giá trị	
                                                for (var i = 0; i < checkbox.length; i++){	
                                                    if (checkbox[i].checked === true){	
                                                        result += '' + checkbox[i].value + '';	
                                                    }	
                                                }                                              	
                                                document.getElementById('txttest').value = result;	
                                                document.getElementById('<%= txtRangeHoaHong.ClientID %>').value = document.getElementById('txttest').value;	
                                            };	
                                        </script>      	
                                           <script>	
                                               document.getElementById('textLX').onclick = function () {	
                                                   document.getElementById("BoLocLX").style.display = "";	
                                                   document.getElementById("textLX").style.display = "none";	
                                                   document.getElementById("textLXAN").style.display = "";	
                                               }	
                                               document.getElementById("textLXAN").onclick = function () {	
                                                   document.getElementById("textLX").style.display = "";	
                                                   document.getElementById("BoLocLX").style.display = "none";	
                                                   document.getElementById("textLXAN").style.display = "none";	
                                               };	
                                               </script>   	
                                                     </div>
                            </div>
                        </div>   	
                        <script>                                            	
                                            var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},	
                                            $checkboxes = $("#checkbox-container :checkbox");	
                                            $checkboxes.on("change", function () {	
                                                $checkboxes.each(function () {	
                                                    checkboxValues[this.id] = this.checked;	
                                                });	
                                                sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));	
                                            });	
                                            // On page load	
                                            $.each(checkboxValues, function (key, value) {	
                                                $("#" + key).prop('checked', value);	
                                            });	
                                        </script>  	
                                        <script>                                            	
                                            var checkboxValues1 = JSON.parse(sessionStorage.getItem('checkboxValues1')) || {},	
                                            $checkboxes1 = $("#checkbox-container1 :checkbox");	
                                            $checkboxes1.on("change", function () {	
                                                $checkboxes1.each(function () {	
                                                    checkboxValues1[this.id] = this.checked;	
                                                });	
                                                sessionStorage.setItem("checkboxValues1", JSON.stringify(checkboxValues1));	
                                            });	
                                            // On page load	
                                            $.each(checkboxValues1, function (key, value) {	
                                                $("#" + key).prop('checked', value);	
                                            });	
                                        </script>  
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <asp:Button ID="btnFilter" runat="server" Text="Lọc kết quả" CssClass="btn btn-primary" style="margin-top: 10px;" OnClick="btnFilter_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divPagination2" class="app-table-pagination" style="text-align:right;" runat="server"></div>
                <div id="divMainTable" style="overflow: auto" runat="server"></div>
                <div id="divPagination1" class="app-table-pagination" style="text-align:right;" runat="server"></div>
            </section>
        </div>
        <%-- modal account info --%>
        <div class="modal fade account-info" id="modalAccountInfo" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Thông tin CTV</h4>
					</div>
					<div class="modal-body">
					    <div class="row">
                            <div class="col-md-3 profile-col">
                                <p>
                                    <img class="profile-img" id="modalAccountInfo_imgThumbnail" runat="server" src="" />
                                </p>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><label>Họ tên: </label> <span id="modalAccountInfo_spanFullName" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><label>Mã TV: </label> <span id="modalAccountInfo_spanCode" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Số điện thoại: </label> <a id="modalAccountInfo_linkPhone" runat="server" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Email: </label> <a id="modalAccountInfo_linkEmail" runat="server" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Địa chỉ: </label> <span id="modalAccountInfo_spanAddr" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Đăng ký lúc: </label> <span id="modalAccountInfo_spanRegisterAt" runat="server">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Mã số thuế: </label> <span id="modalAccountInfo_spanTaxNumber">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Ngày cấp: </label> <span id="modalAccountInfo_spanTaxCreatedDate">(Chưa có thông tin)</span></p>
                                    </div>
                                    <div class="col-md-12">
                                        <p><label>Nơi cấp: </label> <span id="modalAccountInfo_spanTaxCreatedPlace">(Chưa có thông tin)</span></p>
                                    </div>
					            </div>
                            </div>
					    </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn default" data-dismiss="modal">Đóng</button>
					</div>
				</div>
			</div>
		</div>
    </form>
    <script>
        $('#ContentPlaceHolder1_txtReferralCommission').ionRangeSlider({
            min: 0,
            max: 10000000,
            type: 'double',
            step: 100000,
            postfix: "",
            hasGrid: true,
            prettifyFunc: function (num) {
                var milions = parseInt(num / 1000);
                var thousand = parseInt(num / 1000);
                var text = "";
                if (milions >= 1000) {
                    text += " " + milions / 1000 + " Triệu";
                }
                if (thousand < 1000) {
                    text += " " + thousand + " Nghìn";
                }
                text = text.trim();
                if (text == "") {
                    text = "0 Nghìn"
                }
                return text;
            },
        });
        $('#ContentPlaceHolder1_txtReferralRate').ionRangeSlider({
            min: 0,
            max: 100,
            type: 'double',
            step: 1,
            postfix: "%",
            hasGrid: true,
        });
        $('#ContentPlaceHolder1_txtReferralAmount').ionRangeSlider({
            min: 0,
            max: 10000000,
            type: 'double',
            step: 100000,
            postfix: "",
            hasGrid: true,
            prettifyFunc: function (num) {
                var milions = parseInt(num / 1000);
                var thousand = parseInt(num / 1000);
                var text = "";
                if (milions >= 1000) {
                    text += " " + milions / 1000 + " Triệu";
                }
                if (thousand < 1000) {
                    text += " " + thousand + " Nghìn";
                }
                text = text.trim();
                if (text == "") {
                    text = "0 Nghìn"
                }
                return text;
            },
        });
        $('.date-picker').datepicker({
            language: 'vi',
            orientation: "left",
            autoclose: true
        });
    </script>
    <%-- process modal account info --%>
    <script>
        $(document).ready(function () {
            $('.user-info-btn').click(function () {
                var dataAccountId = $(this).data("account-id");
                var dataThumbnail = $(this).data("thumbnail");
                var dataFullname = $(this).data("fullname");
                var dataCode = $(this).data("code");
                var dataPhone = $(this).data("phone");
                var dataEmail = $(this).data("email");
                var dataAddr = $(this).data("addr");
                var dataRegisterAt = $(this).data("register-at");
                var dataTaxNumber = $(this).data("tax-number");
                var dataTaxCreatedDate = $(this).data("tax-created-date");
                var dataTaxCreatedPlace = $(this).data("tax-created-place");
                if (dataAccountId == undefined || dataAccountId == null || dataAccountId == "") {
                    return;
                }

                $('#ContentPlaceHolder1_modalAccountInfo_imgThumbnail').attr("src", (dataThumbnail != "" ? dataThumbnail : "#"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanFullName').text((dataFullname != "" ? dataFullname : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanCode').text((dataCode != "" ? dataCode : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkPhone').text((dataPhone != "" ? dataPhone : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkPhone').attr("href", (dataPhone != "" ? ("tel:" + dataPhone) : "javascript:void(0);"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkEmail').text((dataEmail != "" ? dataEmail : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_linkEmail').attr("href", (dataEmail != "" ? "mailto:" + dataEmail : "javascript:void(0);"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanAddr').text((dataAddr != "" ? dataAddr : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanRegisterAt').text((dataRegisterAt != "" ? dataRegisterAt : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxNumber').text((dataTaxNumber != "" ? dataTaxNumber : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxCreatedDate').text((dataTaxCreatedDate != "" ? dataTaxCreatedDate : "(Chưa có thông tin)"));
                $('#ContentPlaceHolder1_modalAccountInfo_spanTaxCreatedPlace').text((dataTaxCreatedPlace != "" ? dataTaxCreatedPlace : "(Chưa có thông tin)"));

                $('#modalAccountInfo').modal();
            });
        });
    </script>
</asp:Content>
