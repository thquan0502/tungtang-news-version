﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Admin_QuanLyThanhVien_QuanLyThanhVienA : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sHoTen = "";
    string sSoDienThoai = "";
    string TinhThanh = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
              //  Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            Load_SelectHTML("select  * from City order by type asc,Ten asc", "Ten", "id", true, "-- Tất cả --", slTinhThanh);
            
            try
            {
                if (Request.QueryString["HoTen"].Trim() != "")
                {
                    sHoTen = Request.QueryString["HoTen"].Trim();
                    txtHoTen.Value = sHoTen;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["SoDienThoai"].Trim() != "")
                {
                    sSoDienThoai = Request.QueryString["SoDienThoai"].Trim();
                    txtSoDienThoai.Value = sSoDienThoai;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }

            try
            {
                TinhThanh = Request.QueryString["TT"].Trim();
                slTinhThanh.Value = TinhThanh;
            }
            catch { }
           
            
            LoadTinDang();
        }
    }

    void Load_SelectHTML(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        slTinhThanh.DataSource = table;
        slTinhThanh.DataTextField = TextField;
        slTinhThanh.DataValueField = ValueField;
        slTinhThanh.DataBind();

        if (AddANewItem)
        {
            slTinhThanh.Items.Insert(0, (new ListItem(ItemName, "")));
            slTinhThanh.Items.FindByText(ItemName).Selected = true;
        }
    }
    #region paging
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string paramsLinkCommission = "&FilterReferralStatus="+Models.Referral.STATUS_FEE_COMMISSION;
        string paramsLinkFee = "&FilterReferralStatus=" + Models.Referral.STATUS_FEE_COMMISSION;

        string colViewedCounterName, colViewedCounterRealName;
        string colCommissionCounterName, colCommissionCounterRealName;
        string colFeeCounterName, colFeeCounterRealName;

        string filterRangeViewedCounter = null;
        int filterRangeFromViewedCounter = -1, filterRangeToViewedCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterViewed")))
        {
            filterRangeViewedCounter = Request.QueryString.Get("RangeCounterViewed").Trim();
            string[] filterRangeViewedCounterVals = { };
            filterRangeViewedCounterVals = filterRangeViewedCounter.Split(';');
            if (filterRangeViewedCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeViewedCounterVals[0], out filterRangeFromViewedCounter);
                Int32.TryParse(filterRangeViewedCounterVals[1], out filterRangeToViewedCounter);
                txtViewedCounterRange.Text = filterRangeViewedCounter;
            }
        }

        string filterRangeCommissionCounter = null;
        int filterRangeFromCommissionCounter = -1, filterRangeToCommissionCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterCommission")))
        {
            filterRangeCommissionCounter = Request.QueryString.Get("RangeCounterCommission").Trim();
            string[] filterRangeCommissionCounterVals = { };
            filterRangeCommissionCounterVals = filterRangeCommissionCounter.Split(';');
            if (filterRangeCommissionCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeCommissionCounterVals[0], out filterRangeFromCommissionCounter);
                Int32.TryParse(filterRangeCommissionCounterVals[1], out filterRangeToCommissionCounter);
                txtCommissionCounterRange.Text = filterRangeCommissionCounter;
            }
        }

        string filterRangeFeeCounter = null;
        int filterRangeFromFeeCounter = -1, filterRangeToFeeCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterFee")))
        {
            filterRangeFeeCounter = Request.QueryString.Get("RangeCounterFee").Trim();
            string[] filterRangeFeeCounterVals = { };
            filterRangeFeeCounterVals = filterRangeFeeCounter.Split(';');
            if (filterRangeFeeCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeFeeCounterVals[0], out filterRangeFromFeeCounter);
                Int32.TryParse(filterRangeFeeCounterVals[1], out filterRangeToFeeCounter);
                txtFeeCounterRange.Text = filterRangeFeeCounter;
            }
        }

        string filterRangeFromReferralDate = null, filterRangeFromReferralDateAlt = null;
        DateTime filterRangeFromReferralDate_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeFromReferralDate")))
        {
            filterRangeFromReferralDate = Request.QueryString.Get("RangeFromReferralDate").Trim();
            this.txtRangeReferralFrom.Text = filterRangeFromReferralDate;
            paramsLinkCommission += "FilterFromApprovedAt="+ filterRangeFromReferralDate + "&";
            paramsLinkFee += "FilterFromApprovedAt=" + filterRangeFromReferralDate + "&";
            if (DateTime.TryParseExact(filterRangeFromReferralDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterRangeFromReferralDate_DateTime))
            {
                filterRangeFromReferralDateAlt = filterRangeFromReferralDate_DateTime.ToString("yyyy-MM-dd") + " 00:00:00:000";
            }
        }
        string filterRangeToReferralDate = null, filterRangeToReferralDateAlt = null;
        DateTime filterRangeToReferralDate_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeToReferralDate")))
        {
            filterRangeToReferralDate = Request.QueryString.Get("RangeToReferralDate").Trim();
            this.txtRangeReferralTo.Text = filterRangeToReferralDate;
            paramsLinkCommission += "FilterToApprovedAt=" + filterRangeToReferralDate + "&";
            paramsLinkFee += "FilterToApprovedAt=" + filterRangeToReferralDate + "&";
            if (DateTime.TryParseExact(filterRangeToReferralDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterRangeToReferralDate_DateTime))
            {
                filterRangeToReferralDateAlt = filterRangeToReferralDate_DateTime.ToString("yyyy-MM-dd") + " 23:59:59:999";
            }
        }

        if(!string.IsNullOrWhiteSpace(filterRangeFromReferralDateAlt) || !string.IsNullOrWhiteSpace(filterRangeToReferralDateAlt))
        {
            Utilities.ViewedManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colViewedCounterName = "ViewedCounter";
            colViewedCounterRealName = "ViewedManager.FilterCurrentValue";

            Utilities.CommissionManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colCommissionCounterName = "CommissionCounter";
            colCommissionCounterRealName = "CommissionManager.FilterCurrentValue";

            Utilities.FeeManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colFeeCounterName = "FeeCounter";
            colFeeCounterRealName = "FeeManager.FilterCurrentValue";
        }
        else
        {
            colViewedCounterName = "ViewedCounter";
            colViewedCounterRealName = "ViewedManager.TotalViewed";

            colCommissionCounterName = "CommissionCounter";
            colCommissionCounterRealName = "CommissionManager.TotalCommission";
            colFeeCounterName = "FeeCounter";
            colFeeCounterRealName = "FeeManager.TotalFee";
        }

        string sql = "";
        sql += @"select tb1.* from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY tb_ThanhVien.NgayDangKy desc, "+ colViewedCounterRealName + @" desc, "+ colCommissionCounterRealName + @" DESC, " + colFeeCounterRealName + @" DESC
                  ) AS RowNumber, 

                      City.Ten as CityName,
                      District.Ten as DistrictName,
                      tb_PhuongXa.Ten as WardName,
                      tb_ThanhVien.*,
                      ISNULL(" + colViewedCounterRealName + @", 0) AS ViewedCounter,
                      ISNULL("+ colCommissionCounterRealName + @", 0) AS CommissionCounter,
                      ISNULL(" + colFeeCounterRealName + @", 0) AS FeeCounter
                  from tb_ThanhVien 
                	left join City on tb_ThanhVien.idTinh = City.id
	                left join District on tb_ThanhVien.idHuyen = District.id
	                left join tb_PhuongXa on tb_ThanhVien.idPhuongXa = tb_PhuongXa.id
                    INNER JOIN ViewedManager ON tb_ThanhVien.idThanhVien = ViewedManager.AccountId
                    INNER JOIN CommissionManager ON tb_ThanhVien.idThanhVien = CommissionManager.AccountId
                    INNER JOIN FeeManager ON tb_ThanhVien.idThanhVien = FeeManager.AccountId
where 1 = 1

            ";
        if (sHoTen != "")
            sql += " and TenCuaHang like N'%" + sHoTen + "%'";
        if (TinhThanh != "")
            sql += " and idTinh=" + TinhThanh;
        if (sTuNgay != "")
            sql += " and NgayDangKy >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDangKy <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (sSoDienThoai != "")
            sql += " and SoDienThoai like '%" + sSoDienThoai + "%'";

        if (filterRangeFromViewedCounter >= 0)
        {
            sql += " and "+ colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
        }
        if (filterRangeToViewedCounter >= 0)
        {
            sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
        }

        if (filterRangeFromCommissionCounter >= 0)
        {
            sql += " and " + colCommissionCounterRealName + " >= " + (filterRangeFromCommissionCounter * 1000000) + " ";
        }
        if (filterRangeToCommissionCounter >= 0)
        {
            sql += " and " + colCommissionCounterRealName + " <= " + (filterRangeToCommissionCounter * 1000000) + " ";
        }

        if (filterRangeFromFeeCounter >= 0)
        {
            sql += " and " + colFeeCounterRealName + " >= " + (filterRangeFromFeeCounter * 1000000) + " ";
        }
        if (filterRangeToFeeCounter >= 0)
        {
            sql += " and " + colFeeCounterRealName + " <= " + (filterRangeToFeeCounter * 1000000) + " ";
        }

        sql += ") as tb1";

        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        sql += " WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";

        
        DataTable table = Connect.GetTable(sql);

        txtTongThanhVien.Value = AllRowNumber.ToString();
        SetPage(AllRowNumber);

        if(paramsLinkCommission != "&")
        {
            paramsLinkCommission += "&FilterTypeApprovedAt=ReferralCollabApprovedAt";
        }
        else
        {
            paramsLinkCommission = "";
        }

        if (paramsLinkFee != "&")
        {
            paramsLinkFee += "&FilterTypeApprovedAt=ReferralOwnerApprovedAt";
        }
        else
        {
            paramsLinkFee = "";
        }

        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>Mã thành viên</th>
                                <th class='th'>Tên thành viên</th>
                                <th class='th'>Ảnh đại diện</th>
                                <th class='th'>Địa chỉ</th>
                                <th class='th'>Số điên thoại</th>
                                <th class='th'>Email</th>
                                <th class='th'> Tiền hoa hồng</th>
                                <th class='th'> Chi hoa hồng</th>
                                <th class='th'> Lượt xem</th>
                                <th class='th'>Tên đăng nhập</th>
                                <th class='th'>Mật khẩu</th>
                                <th class='th'>isKhoa</th>
                                <th class='th'>Ngày đăng ký</th>
                                <th class='th'></th>
                                <th class='th'></th>
                            </tr>";

        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += " <tr>";
            html += "       <td>" + table.Rows[i]["Code"] + "</td>";
            html += "       <td>" + table.Rows[i]["TenCuaHang"] + "</td>";
            html += "       <td><img src='../../Images/User/" + table.Rows[i]["LinkAnh"] + "' style='width:70px'/></td>";
            html += "       <td>"+ table.Rows[i]["DiaChi"]+ ", " + table.Rows[i]["WardName"].ToString() + ", " + table.Rows[i]["DistrictName"].ToString() + ", " + table.Rows[i]["CityName"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["SoDienThoai"] + "</td>";
            html += "       <td>" + table.Rows[i]["Email"] + "</td>";
            html += "       <td><a href='/Admin/Referral/ReferralTableByCollab.aspx?CollabId=" + table.Rows[i]["idThanhVien"].ToString() + (paramsLinkCommission != "" ? paramsLinkCommission : "") + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["CommissionCounter"].ToString()) + "</a></td>";
            html += "       <td><a href='/Admin/Referral/ReferralTableByOwner.aspx?OwnerId=" + table.Rows[i]["idThanhVien"].ToString() + (paramsLinkFee != "" ? paramsLinkFee : "") + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["FeeCounter"].ToString()) + "</a></td>";
            html += "       <td>" + table.Rows[i]["ViewedCounter"].ToString() + "</td>";
            html += "       <td>" + table.Rows[i]["TenDangNhap"] + "</td>";
            html += "       <td>" + table.Rows[i]["MatKhau"] + "</td>";
            if (table.Rows[i]["isKhoa"].ToString() == "True")
                html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled checked/></td>";
            else
                html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled/></td>";
            try
            {
                html += "       <td>" + StaticData.ConvertMMDDYYtoDDMMYY(table.Rows[i]["NgayDangKy"].ToString()) + "</td>";
            }
            catch
            {
                html += "       <td></td>";
            }
            html += "       <td><a id='btKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' style='cursor:pointer' onclick='DuyetTinDang(\"" + table.Rows[i]["idThanhVien"].ToString() + "\")'><img class='imgedit' src='../images/edit.png'/>Sửa</a></td>";
            html += "       <td><a href='/Admin/Posts/PostTable.aspx?TenDangNhap=" + table.Rows[i]["TenDangNhap"].ToString() + "'>Xem tin đăng</a></td>";
            html += "       </tr>";
        }
        html += "   <tr>";
        html += "       <td colspan='17' class='footertable'>";
        string url = "AccountTable.aspx?";
        if (sHoTen != "")
            url += "HoTen=" + sHoTen + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";
        if (sSoDienThoai != "")
            url += "SoDienThoai=" + sSoDienThoai + "&";
        if (TinhThanh != "")
            url += "TT=" + TinhThanh + "&";
        
        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvThanhVien.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string HoTen = txtHoTen.Value.Trim();
        string SoDienThoai = txtSoDienThoai.Value.Trim();
        string idTinhThanh = slTinhThanh.Value.Trim();
        string rangeCounterViewed = txtViewedCounterRange.Text.Trim();
        string rangeCounterCommission = txtCommissionCounterRange.Text.Trim();
        string rangeCounterFee = txtFeeCounterRange.Text.Trim();
        string rangeFromReferralDate = txtRangeReferralFrom.Text.Trim();
        string rangeToReferralDate = txtRangeReferralTo.Text.Trim();

        string url = "AccountTable.aspx?";
        if (HoTen != "")
            url += "HoTen=" + HoTen + "&";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (SoDienThoai != "")
            url += "SoDienThoai=" + SoDienThoai + "&";
        if (idTinhThanh != "")
            url += "TT=" + idTinhThanh + "&";
        if (rangeCounterViewed != "")
            url += "RangeCounterViewed=" + rangeCounterViewed + "&";
        if (rangeCounterCommission != "")
            url += "RangeCounterCommission=" + rangeCounterCommission + "&";
        if (rangeCounterFee != "")
            url += "RangeCounterFee=" + rangeCounterFee + "&";
        if (rangeFromReferralDate != "")
            url += "RangeFromReferralDate=" + rangeFromReferralDate + "&";
        if (rangeToReferralDate != "")
            url += "RangeToReferralDate=" + rangeToReferralDate + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "AccountTable.aspx";
        Response.Redirect(url);
    }
}