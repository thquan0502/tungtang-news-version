﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Models;
using System.Globalization;

public partial class Admin_Referral_ReferralTable : System.Web.UI.Page
{
    protected string ownerId = null;
	string sruler = "";
    string srulercd = "";
    string shoahong = "";
    string shoahongcd = "";
	string MaTD = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ownerId = Request.QueryString["OwnerId"];
        if(this.ownerId == null || this.ownerId == "")
        {
            Response.Redirect("~/Error404.aspx");
        }
        else
        {
            int accountIdNum = 0;
            if(!Int32.TryParse(this.ownerId, out accountIdNum))
            {
                Response.Redirect("~/Error404.aspx");
            }
            string sql1 = @"
                SELECT TOP 1 
	                ISNULL(TBAccounts.TenDangNhap, '') AS AccountUsername,
	                ISNULL(TBAccounts.TenCuaHang, '') AS AccountFullname,
                    ISNULL(TBAccounts.SoDienThoai, '') AS AccountPhone
                FROM tb_ThanhVien TBAccounts
                WHERE TBAccounts.idThanhVien = '" + this.ownerId + @"'
            ";
            DataTable table1 = Connect.GetTable(sql1);
            if(table1.Rows.Count > 0)
            {
                spanTitleUsername.InnerHtml = "Hoa hồng đã chi của " + (table1.Rows[0]["AccountFullname"].ToString() != "" ? (table1.Rows[0]["AccountFullname"].ToString() + " ") : "") + "(<a href='tel:"+table1.Rows[0]["AccountPhone"].ToString() +"'>"+ table1.Rows[0]["AccountUsername"].ToString()+"</a>)";
                if (IsPostBack)
                {

                }
                else
                {
                    this.initStatusDropdownList();
                    this.loadMainTable();
                }
            }
            else
            {
                Response.Redirect("~/Error404.aspx");
            }
        }
		if (!IsPostBack)
        {
			 Load_SelectHTML("select distinct TinDangId from Referrals where OwnerId ='" + this.ownerId + "' order by TinDangId desc", "TinDangId", "TinDangId", true, "-- Tất cả --", SlTD);
            try
            {
                if (Request.QueryString["HoaHong"].Trim() != "")
                {
                    shoahong = Request.QueryString["HoaHong"].Trim();
                    txtRangeHoaHong.Value = shoahong;
                }
            }
            catch { }
			
			 try
            {
                if (Request.QueryString["MaTD"].Trim() != "")
                {
                    MaTD = Request.QueryString["MaTD"].Trim();
                    SlTD.Value = MaTD;
                }
            }
            catch { }
            //try
            //{
            //    if (Request.QueryString["HoaHongCoDinh"].Trim() != "")
            //    {
            //        shoahongcd = Request.QueryString["HoaHongCoDinh"].Trim();
            //        txtRangeHoaHongcd.Value = shoahongcd;
            //    }
            //}
            //catch { }
            try
            {
                if (Request.QueryString["FilterRangeReferralCommission"].Trim() != "")
                {
                    sruler = Request.QueryString["FilterRangeReferralCommission"].Trim();
                    ruler.InnerHtml = sruler;
                }
            }
            catch { }
           //try
            //{
            //    if (Request.QueryString["FilterRangeReferralAmount"].Trim() != "")
            //    {
            //        srulercd = Request.QueryString["FilterRangeReferralAmount"].Trim();
            //        rulerhh.InnerHtml = srulercd;
            //    }
            //}
            //catch { }
        }
    }
	    void Load_SelectHTML(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        SlTD.DataSource = table;
        SlTD.DataTextField = TextField;
        SlTD.DataValueField = ValueField;
        SlTD.DataBind();

        if (AddANewItem)
        {
            SlTD.Items.Insert(0, (new ListItem(ItemName, "")));
            SlTD.Items.FindByText(ItemName).Selected = true;
        }
    }
    protected void loadMainTable()
    {
        string sql1 = "";
        string sql2 = "";
        string sql3 = "";
        string html = "";
        string htmlPagination = "";
        int perPage = 15;
        int fromIndex;
        int toIndex;
        int totalItems = 0;
        int page = 1;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();
        paginationParams.Add("OwnerId", this.ownerId);

        string filterPostTitle = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterPostTitle")))
        {
            filterPostTitle = Request.QueryString.Get("FilterPostTitle").Trim();
            paginationParams.Add("FilterPostTitle", filterPostTitle);
            txtPostTitle.Text = filterPostTitle;
        }
		
		 string MaTinDang = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("MaTD")))
        {
            MaTinDang = Request.QueryString.Get("MaTD").Trim();
            paginationParams.Add("MaTD", filterPostTitle);
            SlTD.Value = MaTinDang;
        }

        string filterReferralStatus = null;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterReferralStatus")))
        {
            filterReferralStatus = Request.QueryString.Get("FilterReferralStatus").Trim();
            paginationParams.Add("FilterReferralStatus", filterReferralStatus);
            Utilities.Control.setSelectedValue(this.drdlReferralStatus, filterReferralStatus);
        }

        string filterFromApprovedAt = null, filterFromApprovedAtAlt = null;
        DateTime filterFromApprovedAt_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterFromApprovedAt")))
        {
            filterFromApprovedAt = Request.QueryString.Get("FilterFromApprovedAt").Trim();
            paginationParams.Add("FilterFromApprovedAt", filterFromApprovedAt);
            this.txtApprovedAtFrom.Text = filterFromApprovedAt;
            if (DateTime.TryParseExact(filterFromApprovedAt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterFromApprovedAt_DateTime))
            {
                filterFromApprovedAtAlt = filterFromApprovedAt_DateTime.ToString("yyyy-MM-dd") + " 00:00:00:000";
            }
        }
        string filterToApprovedAt = null, filterToApprovedAtAlt = null;
        DateTime filterToApprovedAt_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterToApprovedAt")))
        {
            filterToApprovedAt = Request.QueryString.Get("FilterToApprovedAt").Trim();
            paginationParams.Add("FilterToApprovedAt", filterToApprovedAt);
            this.txtApprovedAtTo.Text = filterToApprovedAt;
            if (DateTime.TryParseExact(filterToApprovedAt, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterToApprovedAt_DateTime))
            {
                filterToApprovedAtAlt = filterToApprovedAt_DateTime.ToString("yyyy-MM-dd") + " 23:59:59:999";
            }
        }
        //string filterTypeApprovedAt = null;
        //if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterTypeApprovedAt")))
        //{
        //    filterTypeApprovedAt = Request.QueryString.Get("FilterTypeApprovedAt").Trim();
        //    paginationParams.Add("FilterTypeApprovedAt", filterTypeApprovedAt);
        //    Utilities.Control.setSelectedValue(this.drdlTypeAprrovedAt, filterTypeApprovedAt);
        //}

        string filterRangeReferralCommission = null;
        int filterRangeFromReferralCommission = -1, filterRangeToReferralCommission = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterRangeReferralCommission")))
        {
            filterRangeReferralCommission = Request.QueryString.Get("FilterRangeReferralCommission").Trim();
            string[] filterRangeReferralCommissionVals = { };
            filterRangeReferralCommissionVals = filterRangeReferralCommission.Split(';');
            if (filterRangeReferralCommissionVals.Length == 2)
            {
                Int32.TryParse(filterRangeReferralCommissionVals[0], out filterRangeFromReferralCommission);
                Int32.TryParse(filterRangeReferralCommissionVals[1], out filterRangeToReferralCommission);
                paginationParams.Add("FilterRangeReferralCommission", filterRangeReferralCommission);
                txtReferralCommission.Text = filterRangeReferralCommission;
            }
        }

        //string filterRangeReferralRate = null;
        //int filterRangeFromReferralRate = -1, filterRangeToReferralRate = -1;
        //if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterRangeReferralRate")))
        //{
        //    filterRangeReferralRate = Request.QueryString.Get("FilterRangeReferralRate").Trim();
        //    string[] filterRangeReferralRateVals = { };
        //    filterRangeReferralRateVals = filterRangeReferralRate.Split(';');
        //    if (filterRangeReferralRateVals.Length == 2)
        //    {
        //        Int32.TryParse(filterRangeReferralRateVals[0], out filterRangeFromReferralRate);
        //        Int32.TryParse(filterRangeReferralRateVals[1], out filterRangeToReferralRate);
        //        paginationParams.Add("FilterRangeReferralRate", filterRangeReferralRate);
        //        txtReferralRate.Text = filterRangeReferralRate;
        //    }
        //}

        //string filterRangeReferralAmount = null;
        //int filterRangeFromReferralAmount = -1, filterRangeToReferralAmount = -1;
        //if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterRangeReferralAmount")))
        //{
        //    filterRangeReferralAmount = Request.QueryString.Get("FilterRangeReferralAmount").Trim();
        //    string[] filterRangeReferralAmountVals = { };
        //    filterRangeReferralAmountVals = filterRangeReferralAmount.Split(';');
        //    if (filterRangeReferralAmountVals.Length == 2)
        //    {
        //        Int32.TryParse(filterRangeReferralAmountVals[0], out filterRangeFromReferralAmount);
        //        Int32.TryParse(filterRangeReferralAmountVals[1], out filterRangeToReferralAmount);
        //        paginationParams.Add("FilterRangeReferralAmount", filterRangeReferralAmount);
        //        txtReferralAmount.Text = filterRangeReferralAmount;
        //    }
        //}
		
        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        fromIndex = ((page - 1) * perPage) + 1;
        toIndex = fromIndex + perPage - 1;

        sql1 += @"
            FROM (
	            SELECT 
                    ROW_NUMBER() OVER ( 
                        ORDER BY Referrals.Id DESC
                    ) AS RowNumber,
                    Referrals.Id as ReferralId, 
                    Referrals.TinDangId as TinDangId, 
                    Referrals.CollabId as ReferralCollabId, 
                    Referrals.OwnerId as ReferralOwnerId, 
                    ISNULL(Referrals.Rate, 0) as ReferralRate, 
                    ISNULL(Referrals.Amount, 0) as ReferralAmount, 
                    ISNULL(Referrals.Commission, 0) as ReferralCommission, 
                    Referrals.Status as ReferralStatus, 
                    ISNULL(Referrals.StsOwnerApproved, 0) AS ReferralStsOwnerApproved, 
                    ISNULL(Referrals.StsCollabApproved, 0) AS ReferralStsCollabApproved, 
                    ISNULL(Referrals.StsAdminApproved, 0) AS ReferralStsAdminApproved, 
                    ISNULL(Referrals.StsOwnerCanceled, 0) AS ReferralStsOwnerCanceled, 
                    ISNULL(Referrals.StsCollabDenied, 0) AS ReferralStsCollabDenied, 
                    Referrals.OwnerApprovedAt as ReferralOwnerApprovedAt,
                    Referrals.AdminApprovedAt AS ReferralAdminApprovedAt, 
                    Referrals.CollabApprovedAt AS ReferralCollabApprovedAt, 
                    Referrals.OwnerCanceledAt AS ReferralOwnerCanceledAt, 
                    Referrals.CollabDeniedAt AS ReferralCollabDeniedAt, 
                    TBPosts.TieuDe as PostTitle, 
                    TBPosts.DuongDan as PostSlug, 
                    TBPosts.isHetHan as PostIsHetHan, 
                    TBPosts.isDraft as PostIsDraft, 
                    TBPosts.isDuyet as PostIsDuyet,
		            ISNULL(TBCollabs.Code, '') AS CollabCode,
                    ISNULL(TBCollabs.TenCuaHang, '') AS CollabFullname,
		            ISNULL(TBCollabs.LinkAnh, '') AS CollabThumbnail,
		            ISNULL(TBCollabs.SoDienThoai, '') AS CollabPhone,
		            ISNULL(TBCollabs.Email, '') AS CollabEmail,
                    ISNULL(TBCollabs.NgayDangKy, '') AS CollabRegisterAt,
                    ISNULL(TBCollabs.DiaChi, '') AS CollabAddress,
                    ISNULL(TBCollabs.MST, '') AS CollabTaxNumber, 
                    TBCollabs.NgayCap AS CollabTaxCreatedDate, 
                    ISNULL(TBCollabs.NoiCap, '') AS CollabTaxCreatedPlace, 
		            ISNULL(TBCollabCities.Ten, '') AS CollabCity,
		            ISNULL(TBCollabDistricts.Ten, '') AS CollabDistrict,
		            ISNULL(TBCollabWards.Ten, '') AS CollabWard
                FROM Referrals  
                    INNER JOIN tb_ThanhVien 
                        on Referrals.CollabId = tb_ThanhVien.idThanhVien 
                    INNER JOIN tb_TinDang TBPosts
                        on Referrals.TinDangId = TBPosts.idTinDang 
                    LEFT JOIN tb_ThanhVien TBCollabs
                        ON Referrals.CollabId = TBCollabs.idThanhVien 
		            LEFT JOIN City TBCollabCities
                        ON TBCollabs.idTinh = TBCollabCities.id
		            LEFT JOIN District TBCollabDistricts
                        ON TBCollabs.idHuyen = TBCollabDistricts.id
		            LEFT JOIN tb_PhuongXa TBCollabWards
                        ON TBCollabs.idPhuongXa = TBCollabWards.id
                WHERE Referrals.OwnerId = '" + this.ownerId + @"' 
        ";
			if (!string.IsNullOrWhiteSpace(MaTinDang))
        {
            sql1 += @" AND TBPosts.idTinDang ='" + MaTinDang + "'";
        }
        if (!string.IsNullOrWhiteSpace(filterPostTitle))
        {
            sql1 += @" AND TBPosts.TieuDe LIKE N'%"+ filterPostTitle + @"%' ";
        }
        if (!string.IsNullOrWhiteSpace(filterReferralStatus))
        {
			if (filterReferralStatus == Models.Referral.STATUS_ALL)
            {
                sql1 += @" AND ( ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                // sql1 += @" OR ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" OR ISNULL(Referrals.StsCollabApproved, 0) = 1 ";
                //sql1 += @" OR ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
                sql1 += @" OR ISNULL(Referrals.StsAdminApproved, 0) = 1 )";
            }
            else if (filterReferralStatus == Models.Referral.STATUS_OWNER_APPROVED)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                //sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
                // sql1 += @" AND ISNULL(Referrals.StsAdminApproved, 0) = 0 ";
            }
            else if (filterReferralStatus == Models.Referral.STATUS_COLLAB_APPROVED)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
              //  sql1 += @" AND ISNULL(Referrals.StsAdminApproved, 0) = 0 ";
            }
            else if (filterReferralStatus == Models.Referral.STATUS_ADMIN_APPROVED)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsAdminApproved, 0) = 1 ";
            }
            else if (filterReferralStatus == Models.Referral.STATUS_FEE_COMMISSION)
            {
                sql1 += @" AND ISNULL(Referrals.StsOwnerApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsOwnerCanceled, 0) = 0 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabApproved, 0) = 1 ";
                sql1 += @" AND ISNULL(Referrals.StsCollabDenied, 0) = 0 ";
            }
        }
         if (!string.IsNullOrWhiteSpace(filterFromApprovedAt))
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterFromApprovedAtAlt + "') OR (Referrals.CollabApprovedAt >= '" + filterFromApprovedAtAlt + "')  OR (Referrals.AdminApprovedAt >= '" + filterFromApprovedAtAlt + "')) ";
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt >= '" + filterFromApprovedAtAlt + "' ";
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt >= '" + filterFromApprovedAtAlt + "' ";
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt >= '" + filterFromApprovedAtAlt + "' ";
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterFromApprovedAtAlt + "') OR (Referrals.CollabApprovedAt >= '" + filterFromApprovedAtAlt + "')) ";
                }
            }
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt >= '" + filterFromApprovedAtAlt + "') OR (Referrals.CollabApprovedAt >= '" + filterFromApprovedAtAlt + "')  OR (Referrals.AdminApprovedAt >= '" + filterFromApprovedAtAlt + "')) ";
            }
        }
        if (!string.IsNullOrWhiteSpace(filterToApprovedAt))
        {
            if (!string.IsNullOrWhiteSpace(filterReferralStatus))
            {
                if (filterReferralStatus == "ALL")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterToApprovedAtAlt + "') OR (Referrals.CollabApprovedAt <= '" + filterToApprovedAtAlt + "') OR (Referrals.AdminApprovedAt <= '" + filterToApprovedAtAlt + "')) ";
                }
                else if (filterReferralStatus == "OWNER_APPROVED")
                {
                    sql1 += "       AND Referrals.OwnerApprovedAt <= '" + filterToApprovedAtAlt + "' ";
                }
                else if (filterReferralStatus == "COLLAB_APPROVED")
                {
                    sql1 += "       AND Referrals.CollabApprovedAt <= '" + filterToApprovedAtAlt + "' ";
                }
                else if (filterReferralStatus == "ADMIN_APPROVED")
                {
                    sql1 += "       AND Referrals.AdminApprovedAt <= '" + filterToApprovedAtAlt + "' ";
                }
                else if (filterReferralStatus == "FEE_COMMISSION")
                {
                    sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterToApprovedAtAlt + "') OR (Referrals.CollabApprovedAt <= '" + filterToApprovedAtAlt + "')) ";
                }
            }
            else
            {
                sql1 += "       AND ((Referrals.OwnerApprovedAt <= '" + filterToApprovedAtAlt + "') OR (Referrals.CollabApprovedAt <= '" + filterToApprovedAtAlt + "') OR (Referrals.AdminApprovedAt <= '" + filterToApprovedAtAlt + "')) ";
            }
        }
       //if (filterRangeFromReferralRate >= 0)
        //{
        //    sql1 += @" AND Referrals.Rate >= " + filterRangeFromReferralRate + " ";
        //}
        //if (filterRangeToReferralRate >= 0)
        //{
        //    sql1 += @" AND Referrals.Rate <= " + filterRangeToReferralRate + " ";
        //}
        //Lọc hoa hồng cố định với thước	
        //if (srulercd != "" && shoahongcd == "0")
        //{
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "10100")
        //{
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "100500")
        //{
        //    sql1 += @" AND Referrals.Amount BETWEEN 100 AND 500";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "500")
        //{
        //    sql1 += @" AND Referrals.Amount >= 500";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "10100100500")
        //{
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100 OR Referrals.Amount BETWEEN 100 AND 500";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "10100500")
        //{
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100 OR Referrals.Amount >= 500";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "100500500")
        //{
        //    sql1 += @" AND Referrals.Amount BETWEEN 100 AND 500 OR Referrals.Amount >= 500";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (srulercd != "" && shoahongcd == "10100100500500")
        //{
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100 OR Referrals.Amount BETWEEN 100 AND 500 OR Referrals.Amount >= 500";
        //    if (filterRangeFromReferralAmount >= 0)
        //    {
        //        sql1 += @" OR Referrals.Amount >= " + (filterRangeFromReferralAmount * 1000000) + " ";
        //    }
        //    if (filterRangeToReferralAmount >= 0)
        //    {
        //        sql1 += @" AND Referrals.Amount <= " + (filterRangeToReferralAmount * 1000000) + " ";
        //    }
        //}
        //if (shoahongcd == "10100")
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100";
        //if (shoahongcd == "100500")
        //    sql1 += @" AND Referrals.Amount BETWEEN 100 AND 500";
        //if (shoahongcd == "500")
        //    sql1 += @" AND Referrals.Amount >= 500";
        //if (shoahongcd == "10100100500")
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100 OR Referrals.Amount BETWEEN 100 AND 500";
        //if (shoahongcd == "100500500")
        //    sql1 += @" AND Referrals.Amount BETWEEN 100 AND 500 OR Referrals.Amount >= 500";
        //if (shoahongcd == "10100500")
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100 OR Referrals.Amount >= 500";
        //if (shoahongcd == "10100100500")
        //    sql1 += @" AND Referrals.Amount BETWEEN 10 AND 100 OR Referrals.Amount BETWEEN 100 AND 500 OR Referrals.Amount >= 500";
        //Lọc hoa hồng với thước	
        if (sruler != "" && shoahong == "0")
        {
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "10100")
        {
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "100500")
        {
            sql1 += @" AND Referrals.Commission BETWEEN 100 AND 500";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "500")
        {
            sql1 += @" AND Referrals.Commission >= 500";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "10100100500")
        {
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100 OR Referrals.Commission BETWEEN 100 AND 500";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "10100500")
        {
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100 OR Referrals.Commission >= 500";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "100500500")
        {
            sql1 += @" AND Referrals.Commission BETWEEN 100 AND 500 OR Referrals.Commission >= 500";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (sruler != "" && shoahong == "10100100500500")
        {
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100 OR Referrals.Commission BETWEEN 100 AND 500 OR Referrals.Commission >= 500";
            if (filterRangeFromReferralCommission >= 0)
            {
                sql1 += @" OR Referrals.Commission >= " + (filterRangeFromReferralCommission * 1000000) + " ";
            }
            if (filterRangeToReferralCommission >= 0)
            {
                sql1 += @" AND Referrals.Commission <= " + (filterRangeToReferralCommission * 1000000) + " ";
            }
        }
        if (shoahong == "10100")
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100";
        if (shoahong == "100500")
            sql1 += @" AND Referrals.Commission BETWEEN 100 AND 500";
        if (shoahong == "500")
            sql1 += @" AND Referrals.Commission >= 500";
        if (shoahong == "10100100500")
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100 OR Referrals.Commission BETWEEN 100 AND 500";
        if (shoahong == "100500500")
            sql1 += @" AND Referrals.Commission BETWEEN 100 AND 500 OR Referrals.Commission >= 500";
        if (shoahong == "10100500")
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100 OR Referrals.Commission >= 500";
        if (shoahong == "10100100500")
            sql1 += @" AND Referrals.Commission BETWEEN 10 AND 100 OR Referrals.Commission BETWEEN 100 AND 500 OR Referrals.Commission >= 500";
        sql1 += @"
            ) TBWrap
        ";

        sql2 += " SELECT COUNT(*) AS TOTALROWS " + sql1;

        sql3 += @" SELECT * " + sql1 + @" WHERE RowNumber BETWEEN " + fromIndex + @" AND " + toIndex + @" ";

        DataTable table2 = Connect.GetTable(sql2);
        totalItems = Int32.Parse(table2.Rows[0]["TOTALROWS"].ToString());

        DataTable table3 = Connect.GetTable(sql3);

        html = @"<table id='referralTable' class='table table-bordered app-table dataTable no-footer'>
                            <thead >
                                <tr>
									<th class='th data-col-title'>Mã TĐ</th>
                                    <th class='th data-col-title'>Tiêu đề</th>
                                    <th class='th data-col-account'>Cộng tác viên</th>
                                    <th class='th data-col-ref-commission'>Tiền hoa hồng</th>
                                    <th class='th data-col-ref-status'>Trạng thái</th>
                                    <th class='th data-col-datetime'>Duyệt lúc</th>
                                </tr>
                            </thead>
                            <tbody>";
        if(table3.Rows.Count == 0)
        {
            html += @"
                <tr>
                    <td colspan='6'><p class='text-center'><strong>Không có kết quả !</strong></p></td>
                </tr>
            ";
        }
        for (int i = 0; i < table3.Rows.Count; i++)
        {
            string tinDangStatus = Models.TinDang.getStatus(table3.Rows[i]["PostIsHetHan"].ToString(), table3.Rows[i]["PostIsDraft"].ToString(), table3.Rows[i]["PostIsDuyet"].ToString());

            string referralStsOwnerApproved = table3.Rows[i]["ReferralStsOwnerApproved"].ToString();
            string referralStsOwnerCanceled = table3.Rows[i]["ReferralStsOwnerCanceled"].ToString();
            string referralStsCollabApproved = table3.Rows[i]["ReferralStsCollabApproved"].ToString();
            string referralStsCollabDenied = table3.Rows[i]["ReferralStsCollabDenied"].ToString();
            string referralStsAdminApproved = table3.Rows[i]["ReferralStsAdminApproved"].ToString();
            string referralOwnerApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralOwnerCanceledAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralOwnerCanceledAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralOwnerCanceledAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralCollabDeniedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralCollabDeniedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralCollabDeniedAt"]).ToString("dd/MM/yyyy HH:mm:ss");
            string referralAdminApprovedAtFormatDatetime = (string.IsNullOrWhiteSpace(table3.Rows[i]["ReferralAdminApprovedAt"].ToString())) ? "" : ((DateTime)table3.Rows[i]["ReferralAdminApprovedAt"]).ToString("dd/MM/yyyy HH:mm:ss");

            string accountInfoText = "";
            accountInfoText += " data-thumbnail='" + Utilities.Account.getLinkThumbWithPath(table3.Rows[i]["CollabThumbnail"].ToString()) + "' ";
            accountInfoText += " data-fullname='" + table3.Rows[i]["CollabFullName"].ToString() + "' ";
            accountInfoText += " data-code='" + table3.Rows[i]["CollabCode"].ToString() + "' ";
            accountInfoText += " data-phone='" + table3.Rows[i]["CollabPhone"].ToString() + "' ";
            accountInfoText += " data-email='" + table3.Rows[i]["CollabEmail"].ToString() + "' ";
            accountInfoText += " data-addr='" + table3.Rows[i]["CollabAddress"].ToString() + ", " + table3.Rows[i]["CollabWard"].ToString() + ", " + table3.Rows[i]["CollabDistrict"].ToString() + ", " + table3.Rows[i]["CollabCity"].ToString() + "' ";
            accountInfoText += " data-register-at='" + ((DateTime)table3.Rows[i]["CollabRegisterAt"]).ToString("dd-MM-yyyy HH:mm:ss") + "' ";
            accountInfoText += " data-tax-number='" + table3.Rows[i]["CollabTaxNumber"].ToString() + "' ";
            accountInfoText += " data-tax-created-date='" + ((table3.Rows[i]["CollabTaxCreatedDate"].ToString() != "") ? ((DateTime)table3.Rows[i]["CollabTaxCreatedDate"]).ToString("dd-MM-yyyy") : "") + "' ";
            accountInfoText += " data-tax-created-place='" + table3.Rows[i]["CollabTaxCreatedPlace"].ToString() + "' ";
            html += @"<tr class='td data-col-title'>";
			  // Mã tin đăng
            html += @"<td>";
            html += @"  <a href='" + Utilities.Post.getUrl(table3.Rows[i]["PostSlug"].ToString()) + "'>" + table3.Rows[i]["TinDangId"].ToString() + "</a>";
            html += @"</td>";
            // title
            html += @"<td>";
            html += @"  <a href='" + Utilities.Post.getUrl(table3.Rows[i]["PostSlug"].ToString()) + "'>" + table3.Rows[i]["PostTitle"].ToString() + "</a>";
            html += @"</td>";
            // account
            html += @"<td>";
            html += @"  <a href='javascript:void(0);'  class='user-info-btn' data-account-id='" + table3.Rows[i]["ReferralCollabId"].ToString() + "' " + accountInfoText + ">" + ((!string.IsNullOrWhiteSpace(table3.Rows[i]["CollabFullname"].ToString())) ? table3.Rows[i]["CollabFullname"].ToString() : table3.Rows[i]["CollabCode"].ToString()) + "</a>";
            html += @"</td>";
            // commission
            html += @"<td class='td data-col-ref-commission'>";
            html += @"<span>" + Utilities.Formatter.toCurrencyString(table3.Rows[i]["ReferralCommission"].ToString()) + "</span>";
            html += @"<br />";
            html += @"</td>";
            // referral status
            html += @"<td class='td data-col-ref-status'>";
            if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "1")
            {
                html += @"<p><label class='label label-danger text-label'>NĐB đã hủy trích hoa hồng</label></p>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<p><label class='label label-primary text-label'>NĐB đã xác nhận</label></p>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "0")
            {
                html += @"<p><label class='label label-info text-label'>CTV đã xác nhận</label></p>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "0" && referralStsCollabDenied == "1" && referralStsAdminApproved == "0")
            {
                html += @"<p><label class='label label-danger text-label'>CTV đã từ chối</label></p>";
            }
            else if (referralStsOwnerApproved == "1" && referralStsOwnerCanceled == "0" && referralStsCollabApproved == "1" && referralStsCollabDenied == "0" && referralStsAdminApproved == "1")
            {
                html += @"<p><label class='label label-success text-label'>Tung Tăng đã xác nhận</label></p>";
            }
            html += "</td>";
            // approve at
            html += "<td class='td data-col-datetime'>";
            if (!string.IsNullOrWhiteSpace(referralOwnerApprovedAtFormatDatetime))
            {
                html += "<div>NĐB xác nhận: " + referralOwnerApprovedAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabApprovedAtFormatDatetime))
            {
                html += "<div>CTV xác nhận: " + referralCollabApprovedAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralAdminApprovedAtFormatDatetime))
            {
                html += "<div>Tung Tăng xác nhận: " + referralAdminApprovedAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralOwnerCanceledAtFormatDatetime))
            {
                html += "<div>NĐB hủy trích hoa hồng: " + referralOwnerCanceledAtFormatDatetime + "</div>";
            }
            if (!string.IsNullOrWhiteSpace(referralCollabDeniedAtFormatDatetime))
            {
                html += "<div>CTV từ chối: " + referralCollabDeniedAtFormatDatetime + "</div>";
            }
            html += "</td>";
            html += @"</tr>";
        }
        html += @"</tbody></table>";
        divMainTable.InnerHtml = html;
        if(totalItems > perPage)
        {
            htmlPagination = Utilities.Table.renderPagination(totalItems, perPage, page, "ReferralTableByOwner.aspx", paginationParams);
            divPagination1.InnerHtml = htmlPagination;
            divPagination2.InnerHtml = htmlPagination;
        }
        else
        {
            divPagination1.Visible = false;
            divPagination2.Visible = false;
        }
    }
     protected void initStatusDropdownList()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Value", typeof(String)));
        dt.Columns.Add(new DataColumn("Text", typeof(String)));

        DataRow dr0 = dt.NewRow();
        dr0[0] = "";
        dr0[1] = "Chọn";
        dt.Rows.Add(dr0);

        DataRow dr1 = dt.NewRow();
        dr1[0] = Referral.STATUS_ALL;
        dr1[1] = "Tất cả";
        dt.Rows.Add(dr1);

        DataRow dr2 = dt.NewRow();
        dr2[0] = Referral.STATUS_OWNER_APPROVED;
        dr2[1] = "Người đăng bài đã trích hoa hồng";
        dt.Rows.Add(dr2);

        DataRow dr3 = dt.NewRow();
        dr3[0] = Referral.STATUS_COLLAB_APPROVED;
        dr3[1] = "Cộng tác viên đã xác nhận";
        dt.Rows.Add(dr3);

        DataRow dr4 = dt.NewRow();
        dr4[0] = Referral.STATUS_ADMIN_APPROVED;
        dr4[1] = "Tung tăng đã xác nhận";
        dt.Rows.Add(dr4);

        DataView dv = new DataView(dt);
        drdlReferralStatus.DataSource = dv;
        drdlReferralStatus.DataTextField = "Text";
        drdlReferralStatus.DataValueField = "Value";
        drdlReferralStatus.DataBind();
    }
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        string url = "ReferralTableByOwner.aspx?OwnerId=" + this.ownerId;
		url += "&MaTD=" + this.SlTD.Value;
        url += "&FilterPostTitle=" + this.txtPostTitle.Text;
        url += "&FilterReferralStatus=" + this.drdlReferralStatus.SelectedValue;
        url += "&FilterFromApprovedAt=" + this.txtApprovedAtFrom.Text;
        url += "&FilterToApprovedAt=" + this.txtApprovedAtTo.Text;
       //  url += "&FilterTypeApprovedAt=" + this.drdlTypeAprrovedAt.SelectedValue;
       // url += "&FilterRangeReferralRate=" + txtReferralRate.Text;
         //Hoa hồng cố định
        //if (txtRangeHoaHongcd.Value == "Chọn")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=0";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 10 đến 100")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=10100";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 100 đến 500")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=100500";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 500 trở lên")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=500";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=10100100500";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=10100500";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=100500500";
        //}
        //if (txtRangeHoaHongcd.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        //{
        //    if (txtReferralAmount.Text != "")
        //        url += "&FilterRangeReferralAmount=" + txtReferralAmount.Text + "&";
        //    url += "HoaHongCoDinh=10100100500500";
        //}
        //if (txtRangeHoaHongcd.Value == "Từ 10 đến 100")
        //    url += "&HoaHongCoDinh=10100&";
        //if (txtRangeHoaHongcd.Value == "Từ 100 đến 500")
        //    url += "&HoaHongCoDinh=100500&";
        //if (txtRangeHoaHongcd.Value == "Từ 500 trở lên")
        //    url += "&HoaHongCoDinh=500&";
        //if (txtRangeHoaHongcd.Value == "Từ 10 đến 100Từ 100 đến 500")
        //    url += "&HoaHongCoDinh=10100100500&";
        //if (txtRangeHoaHongcd.Value == "Từ 10 đến 100Từ 500 trở lên")
        //    url += "&HoaHongCoDinh=10100500&";
        //if (txtRangeHoaHongcd.Value == "Từ 100 đến 500Từ 500 trở lên")
        //    url += "&HoaHongCoDinh=100500500&";
        //if (txtRangeHoaHongcd.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        //    url += "&HoaHongCoDinh=10100100500500&";
        //Hoa hồng
        if (txtRangeHoaHong.Value == "Chọn")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=0";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 10 đến 100")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=10100";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 100 đến 500")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=100500";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 500 trở lên")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=500";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=10100500";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=100500500";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=10100100500";
        }
        if (txtRangeHoaHong.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (txtReferralCommission.Text != "")
                url += "&FilterRangeReferralCommission=" + txtReferralCommission.Text + "&";
            url += "HoaHong=10100100500500";
        }
        if (txtRangeHoaHong.Value == "Từ 10 đến 100")
            url += "HoaHong=10100";
        if (txtRangeHoaHong.Value == "Từ 100 đến 500")
            url += "HoaHong=100500";
        if (txtRangeHoaHong.Value == "Từ 500 trở lên")
            url += "HoaHong=500";
        if (txtRangeHoaHong.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "HoaHong=10100100500";
        if (txtRangeHoaHong.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "HoaHong=10100500";
        if (txtRangeHoaHong.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=100500500";
        if (txtRangeHoaHong.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=10100100500500";
        Response.Redirect(url);
    }
}