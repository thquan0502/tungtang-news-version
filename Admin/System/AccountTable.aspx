﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="AccountTable.aspx.cs" Inherits="Admin_QuanLyThanhVien_QuanLyThanhVienA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- range --%>
    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>   
    <%-- datepicker --%>
    <link rel="stylesheet" href="../../Css/jquery.modal.min.css" />
    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/> 
    <link href="../plugins/select2/select2.min.css" rel="stylesheet" />
    
    <style>        
        @media (max-width:600px) {
            .CheckDay{
                margin-top:5px;
            }
            #filterDistrict{
                width:295px;
            }
            #filterWard{
                width:295px;
            }
        } 
        }
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        }
        @media (min-width:1100px) {
             #filterDistrict{
                width:344.41px;
            }
             #filterWard{
                width:344.41px;
            }
            .CheckDay{
                margin-top:0px;
            }
            }
        @media (max-width: 991px){            
            .tp{
                width: 139%;
            }
             .qh{
                width: 127%;
            }

        }
            
        .pd{
            margin-top:5px;
            margin-bottom:5px;
        }
        #checkbox-container{
          margin: 10px 5px;
        }

        #checkbox-container div{
          margin-bottom: 0px;
        }

        #checkbox-container button{
          margin-top: 5px;
        }
        #checkbox-container1{
          margin: 10px 5px;
        }

        #checkbox-container1 div{
          margin-bottom: 0px;
        }

        #checkbox-container1 button{
          margin-top: 5px;
        }
        #checkbox-container2{
          margin: 10px 5px;
        }

        #checkbox-container2 div{
          margin-bottom: 0px;
        }

        #checkbox-container2 button{
          margin-top: 5px;
        }
        #checkbox-container3{
          margin: 10px 5px;
        }

        #checkbox-container3 div{
          margin-bottom: 0px;
        }

        #checkbox-container3 button{
          margin-top: 5px;
        }
        #myTable .social{
            min-width:200px;
        }
    </style>                 
    
      <!-- jQuery Modal -->   
    <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>    
    <script src="../../Js/jquery.modal.min.js"></script> 
    <script src="../../asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>   
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> 
            
    <script>
        function DuyetUserPro(idThanhVien, flag) {
            var Duyet = document.getElementById("slDuyet_" + idThanhVien);
            var Duyets = document.getElementById("ckKhoa_" + idThanhVien);
            if (Duyet.disabled == true || Duyets.disabled == true) {
                document.getElementById("ckKhoa_" + idThanhVien).disabled = false;
                document.getElementById("slDuyet_" + idThanhVien).disabled = false;
                document.getElementById("btKhoa_" + idThanhVien).innerHTML = "<img class='imgedit' src='../images/save.png'/>Lưu</a>";
            }
            else {
                if (Duyet.value == "KhongDuyet" && flag == undefined) {
                    $("#modalThongKe").modal({
                        fadeDuration: 200,
                        showClose: false
                    });
                    $("#btnLuuLyDo").attr("onclick", "DuyetUserPro(" + idThanhVien + ",1)");
                }
                else {
                    var LyDo = "";
                    var checkbox = document.getElementsByName('kd');
                    var thongbao = '';
                    if (Duyet.value == "KhongDuyet") {
                        if (checkbox[0].checked === true) {
                            LyDo = checkbox[0].value;
                        }
                        if (LyDo == '') {
                            thongbao += "Hãy chọn lý do";
                        }
                    }
                    if (thongbao == '') {
                        var xmlhttp;
                        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        }
                        else {// code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "True")
                                    window.location.reload();
                                else
                                    alert("Lỗi !");
                            }
                        }
                        xmlhttp.open("GET", "../adAjax.aspx?Action=DuyetUserPro&idThanhVien=" + idThanhVien + "&Duyet=" + Duyet.value + "&LyDo=" + LyDo, true);
                        xmlhttp.send();
                    }
                    else {
                        alert(thongbao);
                    }

                    var xmlhttp;
                    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    }
                    else {// code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (xmlhttp.responseText == "True")
                                window.location.reload();
                            else
                                alert("Lỗi !");
                        }
                    }
                    xmlhttp.open("GET", "../adAjax.aspx?Action=KhoaThanhVien&idThanhVien=" + idThanhVien + "&Khoa=" + Duyets.checked, true);
                    xmlhttp.send();
                }
                }
            }
    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">    
    <form runat="server">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="title">QUẢN LÝ THÀNH VIÊN</div>
                <div class="box">
                    <div class="box-body">                        
                        <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div class="titleinput"><b>Thành Viên:</b></div>
                                    <div class="txtinput">
                                        <select id="SlSdt" runat="server"  class="form-control"></select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Loại Thành Viên:</b></div>
                                    <div class="txtinput">
                                       <select class="form-control" id="slTV" runat="server" onchange="thaydoiTrangThai()">
                                            <option value="">-- Chọn --</option>
                                            <option value="2">Tất cả</option>
                                            <option value="0">Thành Viên Thường</option>
                                            <option value="1">Thành Viên Pro</option>
                                            <option value="-1">Thành Viên Pro - Không Duyệt</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row"> 
                                 <div class="coninput1">
                                    <div class="titleinput"><b>Admin duyệt Pro:</b></div>
                                    <div class="txtinput">
                                       <select class="form-control" id="SlAdminDuyet" runat="server">
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Tỉnh/Thành:</b></div>
                                    <div class="txtinput">
                                        <select id="filterCity" style="margin-bottom:15px" class="form-control"></select>
                                    </div>
                                    <div style="display:none" id="dvqh">
                                    <div class="titleinput"><b>Quận/ Huyện: </b></div>
                                         <div class="txtinput tp">
                                         <select id="filterDistrict" style="margin-bottom:15px;" class="form-control"></select>
                                        </div>
                                     </div>
                                    <div style="display:none" id="dvPX">
                                    <div class="titleinput"><b>Phường/ Xã: </b></div>
                                        <div class="txtinput qh">
                                             <select id="filterWard" class="form-control"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tổng Thành Viên:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTongThanhVien" runat="server"  readonly type="text" value="" />
                                    </div>
                                </div>                                
                            </div>
                        </div>                           
                        </div>
                    <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div class="titleinput"><b>Đăng Ký Từ Ngày:</b></div>
                                    <div class="txtinput">
                                        <div class="CheckDay" >
                                            <input class="form-control" data-val="true" data-val-required="" id="txtTuNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                        </div>
                                </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Đăng Ký Đến Ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDenNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>   

                     <%--Bộ lọc chi tiết--%>    
                        <div class="form-group"> 
                            <div class="row">
                                <div class="coninput1">
                                    <div id="checkbox-container1">                                      
                                        <div>
                                    <div class="titleinput"><b>Hoa hồng:</b>                                       
                                    </div>
                                        <div class="txtinput" runat="server">
                                        <div style="margin-bottom:9px;" >
                                        <label style="font-weight:bold"><input type="checkbox" id="chonrulerhh" name="checkhh" value="Chọn"/> Chọn Range lọc</label>                                    
                                        <asp:TextBox ID="txtCommissionCounterRange" runat="server" type="text" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <a id="textHH" style="color:blue">>>Thêm bộ lọc<<</a>
                                        <a id="textHHAN" style="color:blue;display:none">>>Ẩn<<</a>
                                    <div id="BoLocHH" style="display:none;">
                                        <div class="col-12" style="font-weight:bold">
                                           <input type="checkbox" id="ckb10100hh" name="checkhh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb100500hh" name="checkhh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb500hh" name="checkhh" value="Từ 500 trở lên" /> 500 triệu trở lên
                                        </div>
                                    </div>
                                            <div style="margin-top:9px;">
                                                 <input type="button" id="btnhh" value="Áp dụng hoa hồng" class="btn btn-success btn-flat"/>
                                                <p id="messagex" style="color:#ffba00"></p>
                                            </div>
                                       
                                            </div>
                                            </div>
                                        
                                        <%--<p id="show_message"></p>--%>                                  
                                        <input type="hidden" id="txttesthh" />
                                        <input type="hidden" id="txtRangeLuotXemhh" runat="server" />
                                        <p id="rulerhh" runat="server" visible="false"></p>
                                         <script>                                            
                                            document.getElementById('btnhh').onclick = function()
                                            {
                                                document.getElementById('messagex').innerText = "Đã áp dụng";
                                                // Khai báo tham số
                                                var checkboxhh = document.getElementsByName('checkhh');
                                                var resulthh = "";
                                                
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkboxhh.length; i++){
                                                    if (checkboxhh[i].checked === true){
                                                        resulthh += '' + checkboxhh[i].value + '';
                                                    }
                                                }                                                                                              
                                                document.getElementById('txttesthh').value = resulthh;
                                                document.getElementById('<%= txtRangeLuotXemhh.ClientID %>').value = document.getElementById('txttesthh').value;
                                            };
                                        </script>
                                         <script>
                                               document.getElementById('textHH').onclick = function () {
                                                   document.getElementById("BoLocHH").style.display = "";
                                                   document.getElementById("textHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "";
                                               }
                                               document.getElementById("textHHAN").onclick = function () {
                                                   document.getElementById("textHH").style.display = "";
                                                   document.getElementById("BoLocHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "none";
                                               };
                                               </script>                                     
                                    </div>
                                </div>
                                <div class="coninput2">
                                     <div id="checkbox-container2">                                      
                                        <div>
                                    <div class="titleinput"><b>Chi hoa hồng:</b>
                                        
                                    </div>
                                    <div class="txtinput" runat="server">                                 
                                        <input type="hidden" id="txttestchh" ></input>
                                        <input type="hidden" id="txtRangeLuotXemchh" runat="server"></input>
                                        <p id="rulerchh" runat="server" visible="false"></p>
                                        <div style="margin-bottom:9px;" >
                                        <label style="font-weight:bold"><input type="checkbox" id="chonrulerchh" name="checkchh" value="Chọn"/> Chọn Range lọc</label>
                                        <asp:TextBox ID="txtFeeCounterRange" runat="server" type="text" CssClass="form-control"></asp:TextBox>
                                        </div>
                                         <a id="textCHH" style="color:blue">>>Thêm bộ lọc<<</a>                                        
                                        <a id="textCHHAN" style="color:blue;display:none">>>Ẩn<<</a>
                                     <div id="BoLocCHH" style="display:none;">
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb10100chh" name="checkchh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu
                                         </div>
                                        <div class="col-12" style="font-weight:bold">
                                          <input type="checkbox" id="ckb100500chh" name="checkchh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                            <input type="checkbox" id="ckb500chh" name="checkchh" value="Từ 500 trở lên" /> 500 triệu trở lên
                                        </div>
                                     </div>
                                        <div style="margin-top:9px;">               
                                        <input type="button" id="btnchh" value="Áp dụng Chi hoa hồng" class="btn btn-success btn-flat"/>
                                         <p id="messagexx" style="color:#ffba00"></p>
                                            </div>
                                        </div>
                                            </div>
				
                                            <script>
                                                document.getElementById('btnchh').onclick = function () {
                                                    document.getElementById('messagexx').innerText = "Đã áp dụng";
                                                        // Khai báo tham số
                                                        var checkboxchh = document.getElementsByName('checkchh');
                                                        var resultchh = "";

                                                        // Lặp qua từng checkbox để lấy giá trị
                                                        for (var i = 0; i < checkboxchh.length; i++) {
                                                            if (checkboxchh[i].checked === true) {
                                                                resultchh += '' + checkboxchh[i].value + '';
                                                            }
                                                        }
                                                        // In ra kết quả
                                                        document.getElementById('txttestchh').value = resultchh;
                                                        document.getElementById('<%= txtRangeLuotXemchh.ClientID %>').value = document.getElementById('txttestchh').value;
                                                    };
                                                function chonLuotXemchh(obj) {

                                                    window.onload = function () {
                                                        //Lượt xem
                                                        if (sessionStorage.getItem('select') == "select") {
                                                            return;
                                                        }
                                                        if (sessionStorage.getItem('luotxem') == "luotxem") {
                                                            return;
                                                        }                                                        

                                                        var name = sessionStorage.getItem('select');
                                                        if (name !== null) $('#chartSelectRange').val(name);

                                                        var range = sessionStorage.getItem('luotxem');
                                                        if (range !== null) $('#txttest').val(range);
                                                        //Hoa Hồng
                                                        if (sessionStorage.getItem('selecthh') == "selecthh") {
                                                            return;
                                                        }
                                                        if (sessionStorage.getItem('luotxemhh') == "luotxemhh") {
                                                            return;
                                                        }
                                                        var namehh = sessionStorage.getItem('selecthh');
                                                        if (namehh !== null) $('#chartSelectRangehh').val(namehh);

                                                        var rangehh = sessionStorage.getItem('luotxemhh');
                                                        if (rangehh !== null) $('#txttesthh').val(rangehh);
                                                        //Chi hoa hồng
                                                        if (sessionStorage.getItem('selectchh') == "selectchh") {
                                                            return;
                                                        }
                                                        if (sessionStorage.getItem('luotxemchh') == "luotxemchh") {
                                                            return;
                                                        }
                                                        var namechh = sessionStorage.getItem('selectchh');
                                                        if (namechh !== null) $('#chartSelectRangechh').val(namechh);

                                                        var rangechh = sessionStorage.getItem('luotxemchh');
                                                        if (rangechh !== null) $('#txttestchh').val(rangechh);

                                                    }
                                                    window.onbeforeunload = function () {
                                                        //Lượt xem
                                                        sessionStorage.setItem("select", $('#chartSelectRange').val());
                                                        sessionStorage.setItem("luotxem", $('#txttest').val());     
                                                        //Hoa Hồng
                                                        sessionStorage.setItem("selecthh", $('#chartSelectRangehh').val());
                                                        sessionStorage.setItem("luotxemhh", $('#txttesthh').val());
                                                        //Chi hoa hồng
                                                        sessionStorage.setItem("selectchh", $('#chartSelectRangechh').val());
                                                        sessionStorage.setItem("luotxemchh", $('#txttestchh').val());
                                                    }
                                                }                                                
                                               
                                        </script>     
                                         <script>
                                               document.getElementById('textCHH').onclick = function () {
                                                   document.getElementById("BoLocCHH").style.display = "";
                                                   document.getElementById("textCHH").style.display = "none";
                                                   document.getElementById("textCHHAN").style.display = "";
                                               }
                                               document.getElementById("textCHHAN").onclick = function () {
                                                   document.getElementById("textCHH").style.display = "";
                                                   document.getElementById("BoLocCHH").style.display = "none";
                                                   document.getElementById("textCHHAN").style.display = "none";
                                               };
                                               </script>        
                                        <script>                                            
                                            var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},
                                            $checkboxes = $("#checkbox-container :checkbox");

                                            $checkboxes.on("change", function () {
                                                $checkboxes.each(function () {
                                                    checkboxValues[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
                                            });

                                            // On page load
                                            $.each(checkboxValues, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>   
                                        <script>                                            
                                            var checkboxValues1 = JSON.parse(sessionStorage.getItem('checkboxValues1')) || {},
                                            $checkboxes1 = $("#checkbox-container1 :checkbox");

                                            $checkboxes1.on("change", function () {
                                                $checkboxes1.each(function () {
                                                    checkboxValues1[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues1", JSON.stringify(checkboxValues1));
                                            });

                                            // On page load
                                            $.each(checkboxValues1, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>      
                                        <script>                                            
                                            var checkboxValues2 = JSON.parse(sessionStorage.getItem('checkboxValues2')) || {},
                                            $checkboxes2 = $("#checkbox-container2 :checkbox");

                                            $checkboxes2.on("change", function () {
                                                $checkboxes2.each(function () {
                                                    checkboxValues2[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues2", JSON.stringify(checkboxValues2));
                                            });

                                            // On page load
                                            $.each(checkboxValues2, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>                                                                                              
                                    </div>
                                </div>
                            </div>
                        </div>
                     <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    <div id="checkbox-container">                                      
                                        <div>
                                    <div class="titleinput"><b>Lượt xem:</b>                                         
                                        </div>
                                    <div class="txtinput" runat="server">
                                        <div style="margin-bottom:9px;" >
                                            <label style="font-weight:bold; display:inline-block"><input type="checkbox" id="chonruler" name="check" value="Chọn"/> Chọn Range lọc</label>
                                        <asp:TextBox ID="txtViewedCounterRange" runat="server" type="text" CssClass="form-control"></asp:TextBox>
                                        </div>                        
                                        <a id="textLX" style="color:blue">>>Thêm bộ lọc<<</a>
                                        <a id="textLXAN" style="color:blue;display:none">>>Ẩn<<</a>
                                        <div id="BoLocLX" style="display:none;">
                                       <div class="col-12" style="font-weight:bold">
                                           <input type="checkbox" id="ckb100500" name="check" value="Từ 100 đến 500"/> Từ 100 - 500
                                       </div>
                                        <div class="col-12" style="font-weight:bold">
                                        <input type="checkbox" id="ckb5001000" name="check" value="Từ 500 đến 1000" /> Từ 500 - 1000
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                        <input type="checkbox" id="ckb1000" name="check" value="Từ 1000 trở lên" /> Trên 1000
                                        </div>
                                        </div>      
                                        <div style="margin-top:9px;">
                                            <input type="button" id="btn" value="Áp dụng Lượt Xem" class="btn btn-success btn-flat"/>
                                             <p id="message" style="color:#ffba00"></p>
                                        </div>
                                        </div>
                                        
                                            </div>                                        
                                                                       
                                        <input type="hidden" id="txttest" ></input>
                                        <input type="hidden" id="txtRangeLuotXem" runat="server"></input>
                                        <p id="ruler" runat="server" visible="false"></p>
                                         <script>
                                            document.getElementById('btn').onclick = function()
                                            {
                                                document.getElementById('message').innerText = "Đã áp dụng";
                                                // Khai báo tham số
                                                var checkbox = document.getElementsByName('check');
                                                var result = "";
                 
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkbox.length; i++){
                                                    if (checkbox[i].checked === true){
                                                        result += '' + checkbox[i].value + '';
                                                    }
                                                }                                              
                                                document.getElementById('txttest').value = result;
                                                document.getElementById('<%= txtRangeLuotXem.ClientID %>').value = document.getElementById('txttest').value;
                                            };
                                        </script>      
                                           <script>
                                               document.getElementById('textLX').onclick = function () {
                                                   document.getElementById("BoLocLX").style.display = "";
                                                   document.getElementById("textLX").style.display = "none";
                                                   document.getElementById("textLXAN").style.display = "";
                                               }
                                               document.getElementById("textLXAN").onclick = function () {
                                                   document.getElementById("textLX").style.display = "";
                                                   document.getElementById("BoLocLX").style.display = "none";
                                                   document.getElementById("textLXAN").style.display = "none";
                                               };
                                               </script>
                                    </div>
                                </div>
                                <script>                                            
                                            var checkboxValues3 = JSON.parse(sessionStorage.getItem('checkboxValues3')) || {},
                                            $checkboxes3 = $("#checkbox-container :checkbox");

                                            $checkboxes3.on("change", function () {
                                                $checkboxes3.each(function () {
                                                    checkboxValues3[this.id] = this.checked;
                                                });

                                                sessionStorage.setItem("checkboxValues3", JSON.stringify(checkboxValues3));
                                            });

                                            // On page load
                                            $.each(checkboxValues3, function (key, value) {
                                                $("#" + key).prop('checked', value);
                                            });

                                        </script>  
                                <div class="coninput2">
                                    <div id="checkbox-container4">                                      
                                        <div>
                                    <div class="titleinput"><b>Tổng tin đăng:</b>
                                    </div>
                                    <div class="txtinput">
                                         <div style="margin-bottom:9px;" >
                                            <label style="font-weight:bold; display:inline-block"><input type="checkbox" id="chonrulerSum" name="checkSum" value="Chọn"/> Chọn Range lọc</label>
                                       <asp:TextBox ID="txtSumPostCounterRange" runat="server" type="text" CssClass="form-control"></asp:TextBox>
                                    </div>
                                        <a id="textSum" style="color:blue">>>Thêm bộ lọc<<</a>
                                        <a id="textSumAN" style="color:blue;display:none">>>Ẩn<<</a>
                                        <div id="BoLocSum" style="display:none;">
                                       <div class="col-12" style="font-weight:bold">
                                           <input type="checkbox" id="ckb100500Sum" name="checkSum" value="Từ 100 đến 500"/> Từ 100 - 500
                                       </div>
                                        <div class="col-12" style="font-weight:bold">
                                        <input type="checkbox" id="ckb5001000Sum" name="checkSum" value="Từ 500 đến 1000" /> Từ 500 - 1000
                                        </div>
                                        <div class="col-12" style="font-weight:bold">
                                        <input type="checkbox" id="ckb1000Sum" name="checkSum" value="Từ 1000 trở lên" /> Trên 1000
                                        </div>
                                        </div>      
                                        <div style="margin-top:9px;">
                                            <input type="button" id="btnSum" value="Áp dụng Tổng tin đăng" class="btn btn-success btn-flat"/>
                                             <p id="messageSum" style="color:#ffba00"></p>
                                        </div>
                                        </div>
                                        
                                            </div>                                        
                                                                       
                                        <input type="hidden" id="txttestSum" />
                                        <input type="hidden" id="txtRangeSum" runat="server" />
                                        <p id="rulerSum" runat="server" visible="false"></p>
                                         <script>
                                             document.getElementById('btnSum').onclick = function ()
                                            {
                                                 document.getElementById('messageSum').innerText = "Đã áp dụng";
                                                // Khai báo tham số
                                                 var checkbox = document.getElementsByName('checkSum');
                                                var result = "";
                 
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkbox.length; i++){
                                                    if (checkbox[i].checked === true){
                                                        result += '' + checkbox[i].value + '';
                                                    }
                                                }                                              
                                                document.getElementById('txttestSum').value = result;
                                                document.getElementById('<%= txtRangeSum.ClientID %>').value = document.getElementById('txttestSum').value;
                                            };
                                        </script>      
                                           <script>
                                               document.getElementById('textSum').onclick = function () {
                                                   document.getElementById("BoLocSum").style.display = "";
                                                   document.getElementById("textSum").style.display = "none";
                                                   document.getElementById("textSumAN").style.display = "";
                                               }
                                               document.getElementById("textSumAN").onclick = function () {
                                                   document.getElementById("textSum").style.display = "";
                                                   document.getElementById("BoLocSum").style.display = "none";
                                                   document.getElementById("textSumAN").style.display = "none";
                                               };
                                               </script>
                                </div>
                                </div>
                         <div class="form-group">
                            <div class="row">
                                <div class="coninput1">
                                    </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Từ ngày - Đến ngày(Lọc):</b></div>
                                    <div class="txtinput">
                                        <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                            <asp:TextBox ID="txtRangeReferralFrom" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
										    <span class="input-group-addon"> đến </span>
										    <asp:TextBox ID="txtRangeReferralTo" runat="server" type="text" CssClass="form-control" autocomplete="off"></asp:TextBox>
									    </div>
                                    </div>
                                </div>
                            </div>
                             </div>                           
                   <%-- End bộ lọc chi tiết--%>
                        <div class="row">
                            <div class="col-sm-9">                                
                            </div>
                            <div class="col-sm-3">
                                <div style="text-align: right;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                                    <button id="btnTimKiem" class="btn btn-primary btn-flat" style="display:none">Tìm kiếm</button>                                                                        
                                    <input id="ipCity" runat="server" style="display:none"/>
                                    <input id="ipDistrict" runat="server" style="display:none"/>
                                    <input id="ipWard" runat="server" style="display:none"/>
                                </div>
                            </div>
                        </div>
                        <div class="table-scrollable app-table-wrap mt-5" id="dvThanhVien" style="overflow: auto" runat="server">
                        </div>
                    </div>
                </div>
            </section>                                  
             <div class="modal-ThongKe-cls">
                <div id="modalThongKe" class="modal" style="overflow: hidden;">
                    <div style="background-color: #000012; color:#ffffff; font-weight: bold; text-align: center; padding: 10px">Lý do không duyệt bài đăng</div>
                    <input type="checkbox" name="kd" value="Hình ảnh CMND/CCCD không hợp lệ!"/> <label>Hình ảnh CMND/CCCD không hợp lệ!</label> <br/>  
                    <a id="btnLuuLyDo" class="btn btn-primary btn-flat" style="float:right;">Lưu</a>
                </div>
            </div>                      
            <script>                
                $("#ContentPlaceHolder1_slTinhThanh").select2();
                $("#ContentPlaceHolder1_SlSdt").select2();
                $('#ContentPlaceHolder1_txtTuNgay,#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                });
                $('input[type="checkbox"]').on('change', function () {
                    $(this).siblings('input[type="checkbox"]').prop('checked', false);
                });
                function chonLuotXemchh(obj) {

                    window.onload = function () {
                        if (sessionStorage.getItem('select') == "select") {
                            return;
                        }
                        var name = sessionStorage.getItem('select');
                        if (name !== null) $('#filterPostPriceInput').val(name);
                    }
                    window.onbeforeunload = function () {
                        sessionStorage.setItem("select", $('#filterPostPriceInput').val());
                    }
                }
            </script>
             <script>                                            
            var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},
            $checkboxes = $("#checkbox-container4 :checkbox");

            $checkboxes.on("change", function () {
                $checkboxes.each(function () {
                    checkboxValues[this.id] = this.checked;
                });

                sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
            });

            // On page load
            $.each(checkboxValues, function (key, value) {
                $("#" + key).prop('checked', value);
            });

    </script>
        </div>
    </form>
    <script>
        function chonKiemtrangay(obj) {

            window.onload = function () {               
                if (sessionStorage.getItem('dangky') == "dangky") {
                    return;
                }                
                var dk = sessionStorage.getItem('dangky');
                if (dk !== null) $('#ipcheck').val(dk);
                
            }
            window.onbeforeunload = function () {             
                sessionStorage.setItem("dangky", $('#ipcheck').val());                
            }
        }
        (function () {
            var defaultCity = "<%= templateFilterCity %>"; // defaultCity: tỉnh ban đàu defaultCity= dà nẵng
            var defaultDistrict = "<%= templateFilterDistrict %>"; // defaultDistrict = qhair châyy
            var defaultWard = "<%= templateFilterWard %>"; // mặc định xã


            // function render(vẽ) tỉnh thành phố
            var fnInitCities = function () {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLCities";
                var defValue = defaultCity;
                defaultCity = "";

                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () { 
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) { // khi lấy về xong
                        $('#filterCity').html(xmlhttp.responseText);
                        $('#filterCity').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };

            // function vẽ huyện quận của tỉnh thành phố nào
            var fnInitDistricts = function (cityId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLDistricts";
                var defValue = defaultDistrict;
                defaultDistrict = "";
                if (cityId != undefined && cityId != null && cityId != "") {
                    url += "&CityId=" + cityId;
                    document.getElementById('dvqh').style.display = "";
                } else {
                    cityId = "";
                    document.getElementById('dvqh').style.display = "none";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterDistrict').html(xmlhttp.responseText);
                        $('#filterDistrict').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };

            // function vẽ xã phường
            var fnInitWards = function (districtId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLWards";
                var defValue = defaultWard;
                defaultWard = "";
                if (districtId != undefined && districtId != null && districtId != "") {
                    url += "&DistrictId=" + districtId;
                    document.getElementById('dvPX').style.display = "";
                } else {
                    districtId = "";
                    document.getElementById('dvPX').style.display = "none";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterWard').html(xmlhttp.responseText);
                        $('#filterWard').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };

            // khi thay đổi city
            $('#filterCity').on("change", function () {
                var value = $(this).val();
                fnInitDistricts(value);
            });

            // khi thay đổi district 
            $('#filterDistrict').on("change", function () {
                var value = $(this).val();
                fnInitWards(value);
            });

            // lần dầu tiên gọi function vẽ tỉnh thành phố
            fnInitCities();
        })();

        //------------------------------
        $("#btnTimKiem").click(function () {
                    var filterCity = $('#filterCity').val();
                    var filterDistrict = $('#filterDistrict').val();
                    var filterWard = $('#filterWard').val();
                    var check = document.getElementById('cbkDangKy');
                    var checkhh = document.getElementById('cbkHoaHong');
                    document.getElementById('<%= ipCity.ClientID %>').value = $('#filterCity').val();
                    document.getElementById('<%= ipDistrict.ClientID %>').value = $('#filterDistrict').val();
                    document.getElementById('<%= ipWard.ClientID %>').value = $('#filterWard').val();          
                });
        $("#<%= btTimKiem.ClientID%>").click(function () {
            $("#btnTimKiem").click();
        });

        $('#ContentPlaceHolder1_txtViewedCounterRange').ionRangeSlider({
            min: 0,
            max: 100,
            type: 'double',
            step: 1,
            postfix: " Lượt",
            hasGrid: true,
        });
        $('#ContentPlaceHolder1_txtSumPostCounterRange').ionRangeSlider({
            min: 0,
            max: 100,
            type: 'double',
            step: 1,
            postfix: " Tin",
            hasGrid: true,
        });
         $('#ContentPlaceHolder1_txtCommissionCounterRange').ionRangeSlider({
            min: 0,
            max: 10000000,
            type: 'double',
            step: 100000,
            postfix: "",
            hasGrid: true,
            prettifyFunc: function(num) {
                //var milions = parseInt(num / 1000);
                var le = parseInt(num % 1000);
                var chan = parseInt(num / 1000);
                var trieu = parseInt(chan / 1000);
                var tram = chan % 1000;
                var text = "";
                if (chan > 1000 && le < 1000 && trieu % 1000) {
                    if (tram == 0) {
                        text += " " + trieu + " Triệu ";
                    }
                    else {
                        text += " " + trieu + " Triệu " + tram + " Nghìn";
                    }
                }
                else if (trieu / 1000) {
                    text += " " + trieu + " Triệu";
                }
                else if (chan < 1000) {
                    text += " " + chan + " Nghìn";
                }
                text = text.trim();
                if (text == "") {
                    text = "0 Nghìn";
                }
                return text;
            },
        });
        $('#ContentPlaceHolder1_txtFeeCounterRange').ionRangeSlider({
            min: 0,
            max: 10000000,
            type: 'double',
            step: 100000,
            postfix: "",
            hasGrid: true,
            prettifyFunc: function (num) {
                var le = parseInt(num % 1000);
                var chan = parseInt(num / 1000);
                var trieu = parseInt(chan / 1000);
                var tram = chan % 1000;
                var text = "";
                if (chan > 1000 && le < 1000 && trieu % 1000) {
                    if (tram == 0) {
                        text += " " + trieu + " Triệu ";
                    }
                    else {
                        text += " " + trieu + " Triệu " + tram + " Nghìn";
                    }
                }
                else if (trieu / 1000) {
                    text += " " + trieu + " Triệu";
                }
                else if (chan < 1000) {
                    text += " " + chan + " Nghìn";
                }
                text = text.trim();
                if (text == "") {
                    text = "0 Nghìn";
                }
                return text;
            },
        });
        $('.date-picker').datepicker({
            language: 'vi',
            orientation: "left",
            autoclose: true
        });
    </script>     
    <script>
        var APP_SORT_DIRECTION = '<%= Request.QueryString["sortDirection"] == "1" ? "1" : "0" %>';
        var APP_SORT_COLUMN = '<%= Request.QueryString["sortColumn"] %>';

        //TỔNG TIN ĐĂNG
        // chen icon column dang sort
        if (APP_SORT_COLUMN == 'totalPost') {
            if (APP_SORT_DIRECTION === '1') {
                $('#clnTongTin').append('<i class="fa fa-chevron-up"></i>');
            } else if ( APP_SORT_DIRECTION === '0') {
                $('#clnTongTin').append('<i class="fa fa-chevron-down"></i>');
            }
        }


        // xu ly sort column columnView ---- KHI CLICK VAO
        $('#clnTongTin').on('click', function () {
            var baseUrl = "/Admin/System/AccountTable.aspx?";
            var TuNgay = '<%= Request.QueryString["TuNgay"] %>';
            var DenNgay = '<%= Request.QueryString["DenNgay"] %>';
            var SoDienThoai = '<%= Request.QueryString["SoDienThoai"] %>';
            var FilterCity = '<%= Request.QueryString["FilterCity"] %>';
            var FilterDisTrict = '<%= Request.QueryString["FilterDisTrict"] %>';
            var FilterWard = '<%= Request.QueryString["FilterWard"] %>';
            var LoaiThanhVien = '<%= Request.QueryString["LoaiThanhVien"] %>';
            var AdminDuyet = '<%= Request.QueryString["AdminDuyet"] %>';
            var RangeCounterViewed = '<%= Request.QueryString["RangeCounterViewed"] %>';
			var Luotxem = '<%= Request.QueryString["Luotxem"] %>';
            var RangeCounterCommission = '<%= Request.QueryString["RangeCounterCommission"] %>';
            var HoaHong = '<%= Request.QueryString["HoaHong"] %>';
            var RangeCounterFee = '<%= Request.QueryString["RangeCounterFee"] %>';
            var ChiHoaHong = '<%= Request.QueryString["ChiHoaHong"] %>';
            var RangeCounterSumPost = '<%= Request.QueryString["RangeCounterSumPost"] %>';
            var TongTinDang = '<%= Request.QueryString["TongTinDang"] %>';
			var RangeFromReferralDate = '<%= Request.QueryString["RangeFromReferralDate"] %>';	
            var RangeToReferralDate = '<%= Request.QueryString["RangeToReferralDate"] %>';
            var url = baseUrl;
            if (TuNgay != undefined && TuNgay != null && TuNgay != "")
            {
                url += "TuNgay=" + TuNgay + "&";
            }
            if (DenNgay != undefined && DenNgay != null && DenNgay != "") {
                url += "DenNgay=" + DenNgay + "&";
            }
			if (RangeFromReferralDate != undefined && RangeFromReferralDate != null && RangeFromReferralDate != "") {	
                url += "RangeFromReferralDate=" + RangeFromReferralDate + "&";	
            }	
            if (RangeToReferralDate != undefined && RangeToReferralDate != null && RangeToReferralDate != "") {	
                url += "RangeToReferralDate=" + RangeToReferralDate + "&";	
            }
            if (SoDienThoai != undefined && SoDienThoai != null && SoDienThoai != "") {
                url += "SoDienThoai=" + SoDienThoai + "&";
            }
            if (FilterCity != undefined && FilterCity != null && FilterCity != "") {
                url += "FilterCity=" + FilterCity + "&";
            }
            if (FilterDisTrict != undefined && FilterDisTrict != null && FilterDisTrict != "") {
                url += "FilterDisTrict=" + FilterDisTrict + "&";
            }
            if (FilterWard != undefined && FilterWard != null && FilterWard != "") {
                url += "FilterWard=" + FilterWard + "&";
            }
            if (LoaiThanhVien != undefined && LoaiThanhVien != null && LoaiThanhVien != "") {
                url += "LoaiThanhVien=" + LoaiThanhVien + "&";
            }
            if (AdminDuyet != undefined && AdminDuyet != null && AdminDuyet != "") {
                url += "AdminDuyet=" + AdminDuyet + "&";
            }
            if (RangeCounterViewed != undefined && RangeCounterViewed != null && RangeCounterViewed != "") {
                url += "RangeCounterViewed=" + RangeCounterViewed + "&";
            }
			if (Luotxem != undefined && Luotxem != null && Luotxem != "") {	
                url += "Luotxem=" + Luotxem + "&";	
            }
            if (RangeCounterCommission != undefined && RangeCounterCommission != null && RangeCounterCommission != "") {
                url += "RangeCounterCommission=" + RangeCounterCommission + "&";
            }
            if (HoaHong != undefined && HoaHong != null && HoaHong != "") {
                url += "HoaHong=" + HoaHong + "&";
            }
            if (RangeCounterFee != undefined && RangeCounterFee != null && RangeCounterFee != "") {
                url += "RangeCounterFee=" + RangeCounterFee + "&";
            }
            if (ChiHoaHong != undefined && ChiHoaHong != null && ChiHoaHong != "") {
                url += "ChiHoaHong=" + ChiHoaHong + "&";
            }
            if (RangeCounterSumPost != undefined && RangeCounterSumPost != null && RangeCounterSumPost != "") {
                url += "RangeCounterSumPost=" + RangeCounterSumPost + "&";
            }
            if (TongTinDang != undefined && TongTinDang != null && TongTinDang != "") {
                url += "TongTinDang=" + TongTinDang + "&";
            }
            url += "sortColumn=totalPost&sortDirection=" + (APP_SORT_DIRECTION === '1' ? '0' : '1');
            window.location.href = url;
        });

        //LƯỢT XEM
        // chen icon column dang sort
        if (APP_SORT_COLUMN == 'totalView') {
            if (APP_SORT_DIRECTION === '1') {
                $('#clnLuotXem').append('<i class="fa fa-chevron-up"></i>');
            } else if (APP_SORT_DIRECTION === '0') {
                $('#clnLuotXem').append('<i class="fa fa-chevron-down"></i>');
            }
        }


        // xu ly sort column columnView ---- KHI CLICK VAO
        $('#clnLuotXem').on('click', function () {
            var baseUrl = "/Admin/System/AccountTable.aspx?";
            var TuNgay = '<%= Request.QueryString["TuNgay"] %>';
            var DenNgay = '<%= Request.QueryString["DenNgay"] %>';
            var SoDienThoai = '<%= Request.QueryString["SoDienThoai"] %>';
            var FilterCity = '<%= Request.QueryString["FilterCity"] %>';
            var FilterDisTrict = '<%= Request.QueryString["FilterDisTrict"] %>';
            var FilterWard = '<%= Request.QueryString["FilterWard"] %>';
            var LoaiThanhVien = '<%= Request.QueryString["LoaiThanhVien"] %>';
            var AdminDuyet = '<%= Request.QueryString["AdminDuyet"] %>';
            var RangeCounterViewed = '<%= Request.QueryString["RangeCounterViewed"] %>';
			var Luotxem = '<%= Request.QueryString["Luotxem"] %>';
            var RangeCounterCommission = '<%= Request.QueryString["RangeCounterCommission"] %>';
            var HoaHong = '<%= Request.QueryString["HoaHong"] %>';
            var RangeCounterFee = '<%= Request.QueryString["RangeCounterFee"] %>';
            var ChiHoaHong = '<%= Request.QueryString["ChiHoaHong"] %>';
            var RangeCounterSumPost = '<%= Request.QueryString["RangeCounterSumPost"] %>';
            var TongTinDang = '<%= Request.QueryString["TongTinDang"] %>';
			var RangeFromReferralDate = '<%= Request.QueryString["RangeFromReferralDate"] %>';	
            var RangeToReferralDate = '<%= Request.QueryString["RangeToReferralDate"] %>';
            var url = baseUrl;
            if (TuNgay != undefined && TuNgay != null && TuNgay != "")
            {
                url += "TuNgay=" + TuNgay + "&";
            }
            if (DenNgay != undefined && DenNgay != null && DenNgay != "") {
                url += "DenNgay=" + DenNgay + "&";
            }
			if (RangeFromReferralDate != undefined && RangeFromReferralDate != null && RangeFromReferralDate != "") {	
                url += "RangeFromReferralDate=" + RangeFromReferralDate + "&";	
            }	
            if (RangeToReferralDate != undefined && RangeToReferralDate != null && RangeToReferralDate != "") {	
                url += "RangeToReferralDate=" + RangeToReferralDate + "&";	
            }
            if (SoDienThoai != undefined && SoDienThoai != null && SoDienThoai != "") {
                url += "SoDienThoai=" + SoDienThoai + "&";
            }
            if (FilterCity != undefined && FilterCity != null && FilterCity != "") {
                url += "FilterCity=" + FilterCity + "&";
            }
            if (FilterDisTrict != undefined && FilterDisTrict != null && FilterDisTrict != "") {
                url += "FilterDisTrict=" + FilterDisTrict + "&";
            }
            if (FilterWard != undefined && FilterWard != null && FilterWard != "") {
                url += "FilterWard=" + FilterWard + "&";
            }
            if (LoaiThanhVien != undefined && LoaiThanhVien != null && LoaiThanhVien != "") {
                url += "LoaiThanhVien=" + LoaiThanhVien + "&";
            }
            if (AdminDuyet != undefined && AdminDuyet != null && AdminDuyet != "") {
                url += "AdminDuyet=" + AdminDuyet + "&";
            }
            if (RangeCounterViewed != undefined && RangeCounterViewed != null && RangeCounterViewed != "") {
                url += "RangeCounterViewed=" + RangeCounterViewed + "&";
            }
			if (Luotxem != undefined && Luotxem != null && Luotxem != "") {	
                url += "Luotxem=" + Luotxem + "&";	
            }
            if (RangeCounterCommission != undefined && RangeCounterCommission != null && RangeCounterCommission != "") {
                url += "RangeCounterCommission=" + RangeCounterCommission + "&";
            }
            if (HoaHong != undefined && HoaHong != null && HoaHong != "") {
                url += "HoaHong=" + HoaHong + "&";
            }
            if (RangeCounterFee != undefined && RangeCounterFee != null && RangeCounterFee != "") {
                url += "RangeCounterFee=" + RangeCounterFee + "&";
            }
            if (ChiHoaHong != undefined && ChiHoaHong != null && ChiHoaHong != "") {
                url += "ChiHoaHong=" + ChiHoaHong + "&";
            }
            if (RangeCounterSumPost != undefined && RangeCounterSumPost != null && RangeCounterSumPost != "") {
                url += "RangeCounterSumPost=" + RangeCounterSumPost + "&";
            }
            if (TongTinDang != undefined && TongTinDang != null && TongTinDang != "") {
                url += "TongTinDang=" + TongTinDang + "&";
            }
            url += "sortColumn=totalView&sortDirection=" + (APP_SORT_DIRECTION === '1' ? '0' : '1');
            window.location.href = url;
        });
        //CHI HOA HỒNG
        // chen icon column dang sort
        if (APP_SORT_COLUMN == 'totalFree') {
            if (APP_SORT_DIRECTION === '1') {
                $('#clnChiHH').append('<i class="fa fa-chevron-up"></i>');
            } else if (APP_SORT_DIRECTION === '0') {
                $('#clnChiHH').append('<i class="fa fa-chevron-down"></i>');
            }
        }


        // xu ly sort column columnView ---- KHI CLICK VAO
        $('#clnChiHH').on('click', function () {
            var baseUrl = "/Admin/System/AccountTable.aspx?";
            var TuNgay = '<%= Request.QueryString["TuNgay"] %>';
            var DenNgay = '<%= Request.QueryString["DenNgay"] %>';
            var SoDienThoai = '<%= Request.QueryString["SoDienThoai"] %>';
            var FilterCity = '<%= Request.QueryString["FilterCity"] %>';
            var FilterDisTrict = '<%= Request.QueryString["FilterDisTrict"] %>';
            var FilterWard = '<%= Request.QueryString["FilterWard"] %>';
            var LoaiThanhVien = '<%= Request.QueryString["LoaiThanhVien"] %>';
            var AdminDuyet = '<%= Request.QueryString["AdminDuyet"] %>';
            var RangeCounterViewed = '<%= Request.QueryString["RangeCounterViewed"] %>';
			var Luotxem = '<%= Request.QueryString["Luotxem"] %>';
            var RangeCounterCommission = '<%= Request.QueryString["RangeCounterCommission"] %>';
            var HoaHong = '<%= Request.QueryString["HoaHong"] %>';
            var RangeCounterFee = '<%= Request.QueryString["RangeCounterFee"] %>';
            var ChiHoaHong = '<%= Request.QueryString["ChiHoaHong"] %>';
            var RangeCounterSumPost = '<%= Request.QueryString["RangeCounterSumPost"] %>';
            var TongTinDang = '<%= Request.QueryString["TongTinDang"] %>';
			var RangeFromReferralDate = '<%= Request.QueryString["RangeFromReferralDate"] %>';	
            var RangeToReferralDate = '<%= Request.QueryString["RangeToReferralDate"] %>';
            var url = baseUrl;
            if (TuNgay != undefined && TuNgay != null && TuNgay != "")
            {
                url += "TuNgay=" + TuNgay + "&";
            }
            if (DenNgay != undefined && DenNgay != null && DenNgay != "") {
                url += "DenNgay=" + DenNgay + "&";
            }
			if (RangeFromReferralDate != undefined && RangeFromReferralDate != null && RangeFromReferralDate != "") {	
                url += "RangeFromReferralDate=" + RangeFromReferralDate + "&";	
            }	
            if (RangeToReferralDate != undefined && RangeToReferralDate != null && RangeToReferralDate != "") {	
                url += "RangeToReferralDate=" + RangeToReferralDate + "&";	
            }
            if (SoDienThoai != undefined && SoDienThoai != null && SoDienThoai != "") {
                url += "SoDienThoai=" + SoDienThoai + "&";
            }
            if (FilterCity != undefined && FilterCity != null && FilterCity != "") {
                url += "FilterCity=" + FilterCity + "&";
            }
            if (FilterDisTrict != undefined && FilterDisTrict != null && FilterDisTrict != "") {
                url += "FilterDisTrict=" + FilterDisTrict + "&";
            }
            if (FilterWard != undefined && FilterWard != null && FilterWard != "") {
                url += "FilterWard=" + FilterWard + "&";
            }
            if (LoaiThanhVien != undefined && LoaiThanhVien != null && LoaiThanhVien != "") {
                url += "LoaiThanhVien=" + LoaiThanhVien + "&";
            }
            if (AdminDuyet != undefined && AdminDuyet != null && AdminDuyet != "") {
                url += "AdminDuyet=" + AdminDuyet + "&";
            }
            if (RangeCounterViewed != undefined && RangeCounterViewed != null && RangeCounterViewed != "") {
                url += "RangeCounterViewed=" + RangeCounterViewed + "&";
            }
			if (Luotxem != undefined && Luotxem != null && Luotxem != "") {	
                url += "Luotxem=" + Luotxem + "&";	
            }
            if (RangeCounterCommission != undefined && RangeCounterCommission != null && RangeCounterCommission != "") {
                url += "RangeCounterCommission=" + RangeCounterCommission + "&";
            }
            if (HoaHong != undefined && HoaHong != null && HoaHong != "") {
                url += "HoaHong=" + HoaHong + "&";
            }
            if (RangeCounterFee != undefined && RangeCounterFee != null && RangeCounterFee != "") {
                url += "RangeCounterFee=" + RangeCounterFee + "&";
            }
            if (ChiHoaHong != undefined && ChiHoaHong != null && ChiHoaHong != "") {
                url += "ChiHoaHong=" + ChiHoaHong + "&";
            }
            if (RangeCounterSumPost != undefined && RangeCounterSumPost != null && RangeCounterSumPost != "") {
                url += "RangeCounterSumPost=" + RangeCounterSumPost + "&";
            }
            if (TongTinDang != undefined && TongTinDang != null && TongTinDang != "") {
                url += "TongTinDang=" + TongTinDang + "&";
            }
            url += "sortColumn=totalFree&sortDirection=" + (APP_SORT_DIRECTION === '1' ? '0' : '1');
            window.location.href = url;
        });
        //TIỀN HOA HỒNG
        // chen icon column dang sort
        if (APP_SORT_COLUMN == 'totalCommission') {
            if (APP_SORT_DIRECTION === '1') {
                $('#clnTienHH').append('<i class="fa fa-chevron-up"></i>');
            } else if (APP_SORT_DIRECTION === '0') {
                $('#clnTienHH').append('<i class="fa fa-chevron-down"></i>');
            }
        }


        // xu ly sort column columnView ---- KHI CLICK VAO
        $('#clnTienHH').on('click', function () {
            var baseUrl = "/Admin/System/AccountTable.aspx?";
            var TuNgay = '<%= Request.QueryString["TuNgay"] %>';
            var DenNgay = '<%= Request.QueryString["DenNgay"] %>';
            var SoDienThoai = '<%= Request.QueryString["SoDienThoai"] %>';
            var FilterCity = '<%= Request.QueryString["FilterCity"] %>';
            var FilterDisTrict = '<%= Request.QueryString["FilterDisTrict"] %>';
            var FilterWard = '<%= Request.QueryString["FilterWard"] %>';
            var LoaiThanhVien = '<%= Request.QueryString["LoaiThanhVien"] %>';
            var AdminDuyet = '<%= Request.QueryString["AdminDuyet"] %>';
            var RangeCounterViewed = '<%= Request.QueryString["RangeCounterViewed"] %>';
			var Luotxem = '<%= Request.QueryString["Luotxem"] %>';
            var RangeCounterCommission = '<%= Request.QueryString["RangeCounterCommission"] %>';
            var HoaHong = '<%= Request.QueryString["HoaHong"] %>';
            var RangeCounterFee = '<%= Request.QueryString["RangeCounterFee"] %>';
            var ChiHoaHong = '<%= Request.QueryString["ChiHoaHong"] %>';
            var RangeCounterSumPost = '<%= Request.QueryString["RangeCounterSumPost"] %>';
            var TongTinDang = '<%= Request.QueryString["TongTinDang"] %>';
			var RangeFromReferralDate = '<%= Request.QueryString["RangeFromReferralDate"] %>';	
            var RangeToReferralDate = '<%= Request.QueryString["RangeToReferralDate"] %>';
            var url = baseUrl;
            if (TuNgay != undefined && TuNgay != null && TuNgay != "")
            {
                url += "TuNgay=" + TuNgay + "&";
            }
            if (DenNgay != undefined && DenNgay != null && DenNgay != "") {
                url += "DenNgay=" + DenNgay + "&";
            }
			if (RangeFromReferralDate != undefined && RangeFromReferralDate != null && RangeFromReferralDate != "") {	
                url += "RangeFromReferralDate=" + RangeFromReferralDate + "&";	
            }	
            if (RangeToReferralDate != undefined && RangeToReferralDate != null && RangeToReferralDate != "") {	
                url += "RangeToReferralDate=" + RangeToReferralDate + "&";	
            }
            if (SoDienThoai != undefined && SoDienThoai != null && SoDienThoai != "") {
                url += "SoDienThoai=" + SoDienThoai + "&";
            }
            if (FilterCity != undefined && FilterCity != null && FilterCity != "") {
                url += "FilterCity=" + FilterCity + "&";
            }
            if (FilterDisTrict != undefined && FilterDisTrict != null && FilterDisTrict != "") {
                url += "FilterDisTrict=" + FilterDisTrict + "&";
            }
            if (FilterWard != undefined && FilterWard != null && FilterWard != "") {
                url += "FilterWard=" + FilterWard + "&";
            }
            if (LoaiThanhVien != undefined && LoaiThanhVien != null && LoaiThanhVien != "") {
                url += "LoaiThanhVien=" + LoaiThanhVien + "&";
            }
            if (AdminDuyet != undefined && AdminDuyet != null && AdminDuyet != "") {
                url += "AdminDuyet=" + AdminDuyet + "&";
            }
            if (RangeCounterViewed != undefined && RangeCounterViewed != null && RangeCounterViewed != "") {
                url += "RangeCounterViewed=" + RangeCounterViewed + "&";
            }
			if (Luotxem != undefined && Luotxem != null && Luotxem != "") {	
                url += "Luotxem=" + Luotxem + "&";	
            }
            if (RangeCounterCommission != undefined && RangeCounterCommission != null && RangeCounterCommission != "") {
                url += "RangeCounterCommission=" + RangeCounterCommission + "&";
            }
            if (HoaHong != undefined && HoaHong != null && HoaHong != "") {
                url += "HoaHong=" + HoaHong + "&";
            }
            if (RangeCounterFee != undefined && RangeCounterFee != null && RangeCounterFee != "") {
                url += "RangeCounterFee=" + RangeCounterFee + "&";
            }
            if (ChiHoaHong != undefined && ChiHoaHong != null && ChiHoaHong != "") {
                url += "ChiHoaHong=" + ChiHoaHong + "&";
            }
            if (RangeCounterSumPost != undefined && RangeCounterSumPost != null && RangeCounterSumPost != "") {
                url += "RangeCounterSumPost=" + RangeCounterSumPost + "&";
            }
            if (TongTinDang != undefined && TongTinDang != null && TongTinDang != "") {
                url += "TongTinDang=" + TongTinDang + "&";
            }
            url += "sortColumn=totalCommission&sortDirection=" + (APP_SORT_DIRECTION === '1' ? '0' : '1');
            window.location.href = url;
        });
    </script>  
</asp:Content>
