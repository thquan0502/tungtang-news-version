﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanTriTinDang_QuanLyTinDang : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sThoiHan = "";
    string sTinhTrang = "";
    string sTenDangNhap = "";
    string sisHot = "";
    string sHoaHong = "";
    string LinhVuc = "";
    string NguoiDuyet = "";
    string MT = "";
    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] == null || Request.Cookies["AdminTungTang_Login"].Value.Trim() == "")
        {

            //Đăng tin không cần đăng nhập
            Response.Redirect("../Home/DangNhap.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
           // LoadLinhVuc();
            try
            {
                if (Request.QueryString["TenDangNhap"].Trim() != "")
                {
                    sTenDangNhap = Request.QueryString["TenDangNhap"].Trim();
                    txtTenDangNhap.Value = sTenDangNhap;
                }
            }
            catch { }
            //try
            //{
            //    if (Request.QueryString["LinhVuc"].Trim() != "")
            //    {
            //        LinhVuc = Request.QueryString["LinhVuc"].Trim();
            //        slLinhVuc.Value = LinhVuc;
            //    }
            //}
            //catch { }

            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }

            //try
            //{
            //    if (Request.QueryString["ThoiHan"].Trim() != "")
            //    {
            //        sThoiHan = Request.QueryString["ThoiHan"].Trim();
            //        slHan.Value = sThoiHan;
            //    }
            //}
            //catch { }

            //try
            //{
            //    if (Request.QueryString["TinhTrang"].Trim() != "")
            //    {
            //        sTinhTrang = Request.QueryString["TinhTrang"].Trim();
            //        slTinhTrang.Value = sTinhTrang;
            //    }
            //}
            //catch { }

            try
            {
                if (Request.QueryString["Commission"].Trim() != "")
                {
                    sHoaHong = Request.QueryString["Commission"].Trim();
                }
            }
            catch { }

            //try
            //{
            //    if (Request.QueryString["isHot"].Trim() != "")
            //    {
            //        sisHot = Request.QueryString["isHot"].Trim();
            //        slisHot.Value = sisHot;
            //    }
            //}
            //catch { }
            //try
            //{
            //    if (Request.QueryString["MT"].Trim() != "")
            //    {
            //        MT = Request.QueryString["MT"].Trim();
            //        txtMaTin.Value = MT;
            //    }
            //}
            //catch { }

            LoadNguoiDuyet();
            try
            {
                if (Request.QueryString["ND"].Trim() != "")
                {
                    NguoiDuyet = Request.QueryString["ND"].Trim();
                    slNguoiDuyet.Value = NguoiDuyet;
                }
            }
            catch { }
          
            LoadTinDang();
        }
    }
    #region paging
    private void SetPage()
    {
        string sql = @"select count(idThanhVien) from tb_ThanhVien tv where '1'='1'
                    and  tv.StsPro=1
        ";
        if (sTenDangNhap != "")
            sql += " and TenDangNhap = '" + sTenDangNhap + "'";
       
        if (NguoiDuyet != "")
            sql += " and idAdmin_Duyet = '" + NguoiDuyet + "'";
       
        if (sTuNgay != "")
            sql += " and ProAt >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and ProAt <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        if (sTinhTrang != "")
        {
            if (sTinhTrang == "2")
                sql += " and isDuyet is Null";
            else
                sql += " and isDuyet = '" + sTinhTrang + "'";
        }

        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    #endregion
    //void LoadLinhVuc()
    //{
    //    string sql = "select * from tb_DanhMucCap1 order by sothutu";
    //    DataTable table = Connect.GetTable(sql);
    //    slLinhVuc.DataSource = table;
    //    slLinhVuc.DataTextField = "TenDanhMucCap1";
    //    slLinhVuc.DataValueField = "idDanhMucCap1";
    //    slLinhVuc.DataBind();

    //    slLinhVuc.Items.Add(new ListItem("-- Chọn --", ""));
    //    slLinhVuc.Items.FindByText("-- Chọn --").Selected = true;
    //}
    void LoadNguoiDuyet()
    {
        string sql = "select * from tb_Admin where '1'='1' order by HoTen";
        DataTable table = Connect.GetTable(sql);
        slNguoiDuyet.DataSource = table;
        slNguoiDuyet.DataTextField = "TenDangNhap";
        slNguoiDuyet.DataValueField = "idAdmin";
        slNguoiDuyet.DataBind(); 
        slNguoiDuyet.Items.Add(new ListItem("-- Tất cả --", ""));
        slNguoiDuyet.Items.FindByText("-- Tất cả --").Selected = true;
    }
    private void LoadTinDang()
    {
        string sql = "";
        sql += @"select * from
            (
	            SELECT ROW_NUMBER() OVER
                  (
                        ORDER BY tb_ThanhVien.ProAt desc
                  )AS RowNumber,
                      City.Ten as CityName,
                      District.Ten as DistrictName,
                      tb_PhuongXa.Ten as WardName,
                      tb_ThanhVien.*
                  FROM tb_ThanhVien
                	left join City on tb_ThanhVien.idTinh = City.id
	                left join District on tb_ThanhVien.idHuyen = District.id
	                left join tb_PhuongXa on tb_ThanhVien.idPhuongXa = tb_PhuongXa.id
                    where '1'='1'
                    and tb_ThanhVien.StsPro = 0 and tb_ThanhVien.idAdmin_Duyet='-1'
            ";
        if (sTenDangNhap != "")
            sql += " and TenDangNhap = '" + sTenDangNhap + "'";
        //if (MT != "")
        //    sql += " and td.Code like '%" + MT + "%'";
        if (NguoiDuyet != "")
            sql += " and idAdmin_Duyet = '" + NguoiDuyet + "'";
        //if (sisHot != "")
        //{
        //    if (sisHot == "False")
        //        sql += " and (isHot = '" + sisHot + "' or isHot is Null)";
        //    else
        //        sql += " and isHot = '" + sisHot + "'";
        //}
        if (sTuNgay != "")
            sql += " and ProAt >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and ProAt <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        //if (LinhVuc != "")
        //    sql += " and idDanhMucCap1= '" + LinhVuc + "'";
        //if (sThoiHan != "")
        //{
        //    if (sThoiHan == "2")
        //        sql += " and isHetHan is Null";
        //    else
        //        sql += " and isHetHan = '" + sThoiHan + "'";
        //}
        if (sTinhTrang != "")
        {
            if (sTinhTrang == "2")
                sql += " and isDuyet is Null";
            else
                sql += " and isDuyet = '" + sTinhTrang + "'";
        }
        //if (sHoaHong != "")
        //{
        //    if (sHoaHong == "2")
        //        sql += " and Commission = 0";
        //    else
        //        sql += " and Commission !=0";
        //}

        sql += ") as tb1 ";

        //DataTable tableSoTinDangCho = Connect.GetTable("select count(*) from ("+sql+") as tb1");
        //try {
        //    SoTinDangCho.InnerHtml ="Số tin đăng là: "+ tableSoTinDangCho.Rows[0][0].ToString();
        //}
        //    catch{
        //        SoTinDangCho.InnerHtml = "Số tin đăng là: 0";
        //    }
        sql+=" WHERE RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";


        DataTable table = Connect.GetTable(sql);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();
        SetPage();
        string html = @"<table class='table table-bordered table-striped' id='myTable' style='width: 1300px'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                                <th class='th'>
                                    Mã TV
                                </th>
                                <th class='th'>
                                    Tên thành viên
                                </th>
                                <th class='th'>
                                    Tên đăng nhập
                                </th>
			 					<th class='th'>
                                    Email
                                </th>
                                <th class='th'>
                                    Ngày đăng ký
                                </th>
                                <th class='th'>
                                    Ảnh đại diện
                                </th>
                                 <th class='th'>
                                    Số CMND/CCCD
                                </th>
                                <th class='th'>
                                   Hình Ảnh CMND Mặt trước
                                </th>
                                <th class='th'>
                                     Hình Ảnh CMND Mặt sau
                                </th>
                                <th class='th'>
                                    Địa Chỉ
                                </th>
			 					<th class='th'>
                                    Ngày gửi duyệt
                                </th>
                               
                                <th class='th' style='width:80px;display:none;'></th>
                                <th class='th' style='width:110px;display:none;'></th>                
                                <th class='th' style='width:110px'></th>
                                <th class='th' style='width:70px'></th>
                            </tr>";
        string url1 = "";
        //if (LinhVuc != "")
        //    url1 += "LinhVuc=" + LinhVuc + "&";
        if (sTuNgay != "")
            url1 += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url1 += "DenNgay=" + sDenNgay + "&";
        //if (sThoiHan != "")
        //    url1 += "ThoiHan=" + sThoiHan + "&";
        if (sTinhTrang != "")
            url1 += "TinhTrang=" + sTinhTrang + "&";
        //if (sHoaHong != "")
        //    url1 += "Commission=" + sHoaHong + "&";
        if (sTenDangNhap != "")
            url1 += "TenDangNhap=" + sTenDangNhap + "&";
        if (NguoiDuyet != "")
            url1 += "ND=" + NguoiDuyet + "&";
        //if (sisHot != "")
        //    url1 += "isHot=" + sisHot + "&";
        //if (MT != "")
        //    url1 += "MT=" + MT + "&";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += " <tr>";
            html += "       <td>" + (((Page - 1) * PageSize) + i + 1).ToString() + "</td>";
            html += "       <td>"+ table.Rows[i]["Code"] + "</td>";
            html += "       <td>" + table.Rows[i]["TenCuaHang"] + "</td>";
            html += "       <td>" + table.Rows[i]["TenDangNhap"] + "</td>";
			  html += "       <td>" + table.Rows[i]["Email"] + "</td>";
            if (table.Rows[i]["NgayDangKy"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["NgayDangKy"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";
            html += "       <td style='width: 100px'><img src='../../Images/User/" + table.Rows[i]["LinkAnh"] + "' style='width:149px'/></td>";
            html += "       <td>" + table.Rows[i]["SoCMND"] + "</td>";
            html += "       <td style='width: 100px'><img src='../../Images/User/" + table.Rows[i]["HeadCMND"] + "' style='width:149px'/></td>";
            html += "       <td style='width: 100px'><img src='../../Images/User/" + table.Rows[i]["FooterCMND"] + "' style='width:149px'/></td>";
            html += "       <td class='social'>" + table.Rows[i]["DiaChi"] + ", " + table.Rows[i]["WardName"].ToString() + ", " + table.Rows[i]["DistrictName"].ToString() + ", " + table.Rows[i]["CityName"].ToString() + "</td>";
			 if (table.Rows[i]["ProAt"].ToString().ToString() != "")
                html += "   <td>" + DateTime.Parse(table.Rows[i]["ProAt"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
            else
                html += "   <td></td>";
            
            html += "       <td style='display:none;'>";

            html += "       <td><select id='slDuyet_" + table.Rows[i]["idThanhVien"] + "' disabled='disabled' class='form-control' style='width:122px'>";
            if (table.Rows[i]["idAdmin_Duyet"].ToString().Trim() == "-1")
                html += "<option value='DangCho' selected='selected'>Đang chờ...</option>";
            else
                html += "<option value=''>Đang chờ...</option>";
            if (table.Rows[i]["idAdmin_Duyet"].ToString().Trim() == "1")
                html += "<option value='Duyet' selected='selected'>Duyệt</option>";
            else
                html += "<option value='Duyet'>Duyệt</option>";
            if (table.Rows[i]["idAdmin_Duyet"].ToString().Trim() == "-1" && table.Rows[i]["LyDo_KhongDuyet"].ToString().Trim()!="")
                html += "<option value='KhongDuyet' selected='selected'>Không duyệt</option>";
            else
                html += "<option value='KhongDuyet'>Không duyệt</option>";
            html += "       </select></td>";
            html += "   <td><a id='btDuyet_" + table.Rows[i]["idThanhVien"].ToString() + "' style='cursor:pointer' onclick='DuyetUserPro(\"" + table.Rows[i]["idThanhVien"].ToString() + "\")'><img class='imgedit' src='../images/edit.png' style='width:25px; cursor:pointer'/>Sửa</a></td>";
            html += "       </tr>";
        }
        html += "   <tr>";
        html += "       <td colspan='20' class='footertable'>";
        string url = "AccountPro.aspx?";
        //if (LinhVuc != "")
        //    url += "LinhVuc=" + LinhVuc + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";
        //if (sThoiHan != "")
        //    url += "ThoiHan=" + sThoiHan + "&";
        if (sTinhTrang != "")
            url += "TinhTrang=" + sTinhTrang + "&";
        if (sTenDangNhap != "")
            url += "TenDangNhap=" + sTenDangNhap + "&";
        if (NguoiDuyet != "")
            url += "ND=" + NguoiDuyet + "&";
        //if (sisHot != "")
        //    url += "isHot=" + sisHot + "&";
        //if (sHoaHong != "")
        //    url += "Commission=" + sHoaHong + "&";
        //if (MT != "")
        //    url += "MT=" + MT + "&";

        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvTinDang.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        //string LinhVuc = slLinhVuc.Value.Trim();
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        //string ThoiHan = slHan.Value.Trim();
      //  string TinhTrang = slTinhTrang.Value.Trim();
      //  string HoaHong = slHoaHong.Value.Trim();
        string TenDangNhap = txtTenDangNhap.Value.Trim();
      //  string isHot = slisHot.Value.Trim();
        string NguoiDuyet = slNguoiDuyet.Value.Trim();
       // string MT = txtMaTin.Value.Trim();
        string url = "AccountPro.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        //if (ThoiHan != "0")
        //    url += "ThoiHan=" + ThoiHan + "&";
        //if (TinhTrang != "-1")
        //    url += "TinhTrang=" + TinhTrang + "&";
        if (TenDangNhap != "")
            url += "TenDangNhap=" + TenDangNhap + "&";
        //if (MT != "")
        //    url += "MT=" + MT + "&";
        if (NguoiDuyet != "")
            url += "ND=" + NguoiDuyet + "&";
        //if (LinhVuc != "")
        //    url += "LinhVuc=" + LinhVuc + "&";
        //if (isHot != "-1")
        //    url += "isHot=" + isHot + "&";
        //if (HoaHong != "0")
        //    url += "Commission=" + HoaHong + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "AccountPro.aspx";
        Response.Redirect(url);
    }
}