﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Layout/AdminMasterPageNew.master" AutoEventWireup="true" CodeFile="AccountPro.aspx.cs" Inherits="Admin_QuanTriTinDang_QuanLyTinDang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script>
        function DuyetUserPro(idThanhVien, flag) {
            var Duyet = document.getElementById("slDuyet_" + idThanhVien);
            if (Duyet.disabled == true) {
                document.getElementById("slDuyet_" + idThanhVien).disabled = false;
                document.getElementById("btDuyet_" + idThanhVien).innerHTML = "<img class='imgedit' src='../images/save.png'/>Lưu</a>";
            }
			 else if(Duyet.value == "DangCho" && flag == undefined)
            {
                alert("Hãy chọn trạng thái!");
                return;
            }
            else {
                if (Duyet.value == "KhongDuyet" && flag == undefined) {
                    $("#modalThongKe").modal({
                        fadeDuration: 200,
                        showClose: false
                    });
                    $("#btnLuuLyDo").attr("onclick", "DuyetUserPro(" + idThanhVien + ",1)");
                }
                else {
                    var LyDo = "";
                    var checkbox = document.getElementsByName('kd');
                    var thongbao = '';
                    if (Duyet.value == "KhongDuyet") {
                        if (checkbox[0].checked === true) {
                            LyDo = checkbox[0].value;
                        }
                        if (LyDo == '') {
                            thongbao += "Hãy chọn lý do";
                        }
                    }
                    if (thongbao == '') {
                        var xmlhttp;
                        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        }
                        else {// code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                if (xmlhttp.responseText == "True")
                                    window.location.reload();
                                else
                                    alert("Lỗi !");
                            }
                        }
                        xmlhttp.open("GET", "../adAjax.aspx?Action=DuyetUserPro&idThanhVien=" + idThanhVien + "&Duyet=" + Duyet.value + "&LyDo=" + LyDo, true);
                        xmlhttp.send();
                    }
                    else {
                        alert(thongbao);
                    }
                }
            }
        }
        function MoXemChiTiet(idTinDang) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById('lightXemChiTiet').style.display = 'block';
                    document.getElementById('fadeXemChiTiet').style.display = 'block';
                    document.getElementById('dvXemChiTiet').innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=MoXemChiTietTinDang&idTinDang=" + idTinDang, true);
            xmlhttp.send();
        }
        function DongXemChiTiet() {
            document.getElementById('lightXemChiTiet').style.display = 'none';
            document.getElementById('fadeXemChiTiet').style.display = 'none';
        }
        function LenTop(TD) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (xmlhttp.responseText == "True") {
                        alert('Bài viết đã lên TOP');
                    }

                }
            }
            xmlhttp.open("GET", "../adAjax.aspx?Action=LenTop&TD=" + TD, true);
            xmlhttp.send();
        }
    </script>
	<style>
         #myTable .social{
            min-width:200px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server" autocomplete="off">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="title">QUẢN LÝ USER PRO</div>
                <div class="box">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Từ ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTuNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Đến ngày:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtDenNgay" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tên đăng nhập:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTenDangNhap" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                       <%-- <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Tên đăng nhập:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtTenDangNhap" runat="server" name="Content.ContentName" type="text" value="" />
                                    </div>
                                </div>
                                <div class="coninput3" style="display:none">
                                    <div class="titleinput"><b>isHot:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slisHot" runat="server">
                                            <option value="-1">-- Tất cả --</option>
                                            <option value="True">Tin Hot</option>
                                            <option value="False">Tin không Hot</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="coninput2">
                                    <div class="titleinput"><b>Mã tin:</b></div>
                                    <div class="txtinput">
                                        <input class="form-control" data-val="true" data-val-required="" id="txtMaTin" runat="server" name="Content.ContentName1" type="text" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                        <div class="form-group">
                            <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                <%--<div class="coninput1">
                                    <div class="titleinput"><b>Lĩnh vực:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slLinhVuc" runat="server">
                                        </select>
                                    </div>
                                </div>--%>
                                <div class="coninput1">
                                    <div class="titleinput"><b>Người duyệt:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slNguoiDuyet" runat="server">
                                        </select>
                                    </div>
                                </div>
                                </div>
                            </div>

                         <%-- <div class="form-group">
                                 <div class="row">
                                <div class="dvnull">&nbsp;</div>
                                 <div class="coninput1">
                                    <div class="titleinput"><b>Hoa Hồng:</b></div>
                                    <div class="txtinput">
                                        <select class="form-control" id="slHoaHong" runat="server">
                                            <option value="0">-- Tất cả --</option>
                                            <option value="1">Có hoa hồng</option>
                                            <option value="2">Không có hoa hồng</option>
                                        </select>
                                    </div>
                                      </div>
                                </div>
                            </div>       --%>         
                        </div>

                        <div class="row">
                            <div class="col-sm-9" id="SoTinDangCho" runat="server" style="font-weight:bold;font-size:14px;">
                                <%--<a class="btn btn-primary btn-flat" href="DanhMucLoaiKhachHang-CapNhat.aspx"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a>--%>
                           
                                
                                
                                 </div>
                            <div class="col-sm-3">
                                <div style="text-align: right;">
                                    <asp:Button ID="btTimKiem" class="btn btn-primary btn-flat" runat="server" Text="Tìm kiếm" OnClick="btTimKiem_Click" />
                                </div>
                            </div>
                        </div>
                        <div id="dvTinDang" style="overflow: auto" runat="server">
                        </div>
                        <%--<table class="table table-bordered table-striped">
                <tr>
                    <th class="th">
                        STT
                    </th>
                    <th class="th">
                        Tên loại khách hàng
                    </th>
                    <th class="th">
                        Ghi chú
                    </th>
                    <th id="command" class="th"></th>
                </tr>
                <tr>
                        <td>
                            1
                        </td>
                        <td>
                            Loại khách hàng 1
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <a href="#"><img class="imgedit" src="../images/edit.png" />Sửa</a>|
                            <a href="#"><img class="imgedit" src="../images/delete.png" />Xóa</a>
                        </td>
                    </tr>
            </table>
            <br />
            <div class="pagination-container" style="text-align:center">
                <ul class="pagination">
                    <li class="active"><a>1</a></li>
                    <li class=""><a>2</a></li>
                    <li class=""><a>3</a></li>
                    <li class=""><a>4</a></li>
                    <li class=""><a>5</a></li>
                </ul>
            </div>--%>
                    </div>
                </div>
            </section>


             <div class="modal-ThongKe-cls">
                <div id="modalThongKe" class="modal" style="overflow: hidden;">
                    <div style="background-color: #000012; color:#ffffff; font-weight: bold; text-align: center; padding: 10px">Lý do không duyệt bài đăng</div>
                    <input type="checkbox" name="kd" value="Hình ảnh CMND/CCCD không hợp lệ!"/> <label>Hình ảnh CMND/CCCD không hợp lệ!</label> <br/>  
                    <a id="btnLuuLyDo" class="btn btn-primary btn-flat" style="float:right;">Lưu</a>
                </div>
            </div>

            <!-- /.content -->
            <script src="../plugins/datetimePicker1/build/jquery.datetimepicker.full.js"></script>
            <script type="text/javascript">
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtTuNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //dayOfWeekStart : 1,
                    //todayBtn: "linked",
                    language: "it",
                    autoclose: true,
                    todayHighlight: true,
                    dateFormat: 'dd/mm/yyyy'
                });
                $('#ContentPlaceHolder1_txtDenNgay').datetimepicker({
                    //yearOffset:222,
                    lang: 'ch',
                    timepicker: false,
                    format: 'd/m/Y',
                    formatDate: 'Y/m/d',
                    //value: 'today'
                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
                });

            </script>
        </div>
    </form>
</asp:Content>

