﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections;
using System.Web.Caching;


public partial class Admin_QuanLyThanhVien_QuanLyThanhVienA : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sHoTen = "";
    string sSoDienThoai = "";
    string TinhThanh = "";
    string QuanHuyen = "";
    string PhuongXa = "";
    string AdminDuyet = "";
    string LoaiThanhVien = "";
    string slistLabel = "";
    string sTongTin = "";
    string sRangeTong = "";
    string sruler = "";
    string sRangeLuotXem = "";

    string slistLabelhh = "";
    string srulerhh = "";

    string slistLabelchh = "";
    string srulerchh = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    string TuNgay = "";
    string DenNgay = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;

    protected string templateJsonCities = "";
    protected string templateJsonDistricts = "";
    protected string templateJsonWards = "";

    protected string templateFilterCity = ""; // gia tri mac dinh can truyen cho client
    protected string filterCity = null; // gia tri mà request gui len

    protected string templateFilterDistrict = "";
    protected string filterDistrict = null;

    protected string templateFilterWard = "";
    protected string filterWard = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //  Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            //LoadTinh();
            LoadAdminDuyet();
            Load_SelectSDT("select 'TT' +(CAST(idThanhVien as varchar(100)) + ' - ' + SoDienThoai + ' - ' + TenCuaHang) as IdTV,SoDienThoai,idThanhVien from tb_ThanhVien where TenCuaHang != '' order by NgayDangKy desc", "IdTV", "idThanhVien", true, "-- Tất cả --", SlSdt);
            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["AdminDuyet"].Trim() != "")
                {
                    AdminDuyet = Request.QueryString["AdminDuyet"].Trim();
                    SlAdminDuyet.Value = AdminDuyet;
                    SlAdminDuyet.Style.Add("display", "");
                }
            }
            catch { }
            try
            {
                sSoDienThoai = Request.QueryString["SoDienThoai"].Trim();
                SlSdt.Value = sSoDienThoai;
            }
            catch { }
            try
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterCity")))
                {
                    string filterCity = Request.QueryString.Get("FilterCity").Trim();
                    this.filterCity = filterCity;
                    this.templateFilterCity = filterCity;
                }
            }
            catch { }

            try
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterDistrict")))
                {
                    string filterDistrict = Request.QueryString.Get("FilterDistrict").Trim();
                    this.filterDistrict = filterDistrict;
                    this.templateFilterDistrict = filterDistrict;
                }
            }
            catch { }

            try
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("FilterWard")))
                {
                    string filterWard = Request.QueryString.Get("FilterWard").Trim();
                    this.filterWard = filterWard;
                    this.templateFilterWard = filterWard;
                }
            }
            catch { }

            try
            {
                LoaiThanhVien = Request.QueryString["LoaiThanhVien"].Trim();
                slTV.Value = LoaiThanhVien;
            }
            catch { }
            try
            {
                if (Request.QueryString["Luotxem"].Trim() != "")
                {
                    slistLabel = Request.QueryString["Luotxem"].Trim();
                    txtRangeLuotXem.Value = slistLabel;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterViewed"].Trim() != "")
                {
                    sruler = Request.QueryString["RangeCounterViewed"].Trim();
                    ruler.InnerHtml = sruler;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["TongTinDang"].Trim() != "")
                {
                    sTongTin = Request.QueryString["TongTinDang"].Trim();
                    txtRangeSum.Value = sTongTin;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterSumPost"].Trim() != "")
                {
                    sRangeTong = Request.QueryString["RangeCounterSumPost"].Trim();
                    txtSumPostCounterRange.Text = sRangeTong;
                }
            }
            catch { }
            try
            {
                if (Request.QueryString["HoaHong"].Trim() != "")
                {
                    slistLabelhh = Request.QueryString["HoaHong"].Trim();
                    txtRangeLuotXemhh.Value = slistLabelhh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterCommission"].Trim() != "")
                {
                    srulerhh = Request.QueryString["RangeCounterCommission"].Trim();
                    rulerhh.InnerHtml = srulerhh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["ChiHoaHong"].Trim() != "")
                {
                    slistLabelchh = Request.QueryString["ChiHoaHong"].Trim();
                    txtRangeLuotXemchh.Value = slistLabelchh;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["RangeCounterFee"].Trim() != "")
                {
                    srulerchh = Request.QueryString["RangeCounterFee"].Trim();
                    rulerchh.InnerHtml = srulerchh;
                }
            }
            catch { }
            try
            {
                TuNgay = Request.QueryString["RangeFromReferralDate"].Trim();
            }
            catch { }
            try
            {
                DenNgay = Request.QueryString["RangeToReferralDate"].Trim();
            }
            catch { }

            //string Tinh = ddlTinh.SelectedValue.Trim();
            //  

            LoadTinDang();





            // ddlTinh_SelectedIndexChanged.();
            // ddlHuyen_SelectedIndexChanged(sender, e);
        }
    }
    void LoadAdminDuyet()
    {
        string sql = "select * from tb_Admin where '1'='1' order by HoTen";
        DataTable table = Connect.GetTable(sql);
        SlAdminDuyet.DataSource = table;
        SlAdminDuyet.DataTextField = "TenDangNhap";
        SlAdminDuyet.DataValueField = "idAdmin";
        SlAdminDuyet.DataBind();
        SlAdminDuyet.Items.Add(new ListItem("-- Tất cả --", ""));
        SlAdminDuyet.Items.FindByText("-- Tất cả --").Selected = true;
    }
    //private void LoadTinh()
    //{
    //    string strSql = "select * from City  order by type asc, ten asc";
    //    ddlTinh.DataSource = Connect.GetTable(strSql);
    //    ddlTinh.DataTextField = "Ten";
    //    ddlTinh.DataValueField = "id";
    //    ddlTinh.DataBind();
    //    ddlTinh.Items.Add(new ListItem("Toàn quốc", "0"));
    //    ddlTinh.Items.FindByText("Toàn quốc").Selected = true;
    //}
    void Load_SelectSDT(string sql, string TextField, string ValueField, bool AddANewItem, string ItemName, System.Web.UI.HtmlControls.HtmlSelect select)
    {
        DataTable table = Connect.GetTable(sql);

        SlSdt.DataSource = table;
        SlSdt.DataTextField = TextField;
        SlSdt.DataValueField = ValueField;
        SlSdt.DataBind();

        if (AddANewItem)
        {
            SlSdt.Items.Insert(0, (new ListItem(ItemName, "")));
            SlSdt.Items.FindByText(ItemName).Selected = true;
        }
    }
    
    protected override void SavePageStateToPersistenceMedium(Object viewState)
    {
        string _vskey;
        _vskey = "VIEWSTATE_" + base.Session.SessionID + "_" + Request.RawUrl +
        "_" + System.DateTime.Now.Ticks.ToString();

        Cache.Add(_vskey, viewState, null,
        System.DateTime.Now.AddMinutes(Session.Timeout), Cache.NoSlidingExpiration,
        CacheItemPriority.Default, null);

        RegisterHiddenField("_VIEWSTATE_KEY", _vskey);
    }

    protected override object LoadPageStateFromPersistenceMedium()
    {
        string _vskey = Request.Form["_VIEWSTATE_KEY"];

        if (_vskey == null)
        {
            return null;
        }
        else
        {
            return Cache[_vskey];
        }
    }


    #region paging
    private void SetPage(int AllRowNumber)
    {
        int TotalRows = AllRowNumber;
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage(AllRowNumber);
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string paramsLinkCommission = "&FilterReferralStatus=" + Models.Referral.STATUS_FEE_COMMISSION;
        string paramsLinkFee = "&FilterReferralStatus=" + Models.Referral.STATUS_FEE_COMMISSION;

        string sortColumn = Request.QueryString["sortColumn"];
        string sortDirection = Request.QueryString["sortDirection"]; // 0 giam, 1 tang

        string colViewedCounterName, colViewedCounterRealName;
        string colSumCounterRealName;
        string colSumCounterName;
        string colCommissionCounterName, colCommissionCounterRealName;
        string colFeeCounterName, colFeeCounterRealName;

        string filterRangeViewedCounter = null;
        string filterRangeSumCounter = null;
        int filterRangeFromViewedCounter = -1, filterRangeToViewedCounter = -1;
        int filterRangeFromSumCounter = -1, filterRangeToSumCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterViewed")))
        {
            filterRangeViewedCounter = Request.QueryString.Get("RangeCounterViewed").Trim();
            string[] filterRangeViewedCounterVals = { };
            filterRangeViewedCounterVals = filterRangeViewedCounter.Split(';');
            if (filterRangeViewedCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeViewedCounterVals[0], out filterRangeFromViewedCounter);
                Int32.TryParse(filterRangeViewedCounterVals[1], out filterRangeToViewedCounter);
                txtViewedCounterRange.Text = filterRangeViewedCounter;
            }
        }

        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterSumPost")))
        {
            filterRangeSumCounter = Request.QueryString.Get("RangeCounterSumPost").Trim();
            string[] filterRangeSumCounterVals = { };
            filterRangeSumCounterVals = filterRangeSumCounter.Split(';');
            if (filterRangeSumCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeSumCounterVals[0], out filterRangeFromSumCounter);
                Int32.TryParse(filterRangeSumCounterVals[1], out filterRangeToSumCounter);
                txtSumPostCounterRange.Text = filterRangeSumCounter;
            }
        }

        string filterRangeCommissionCounter = null;
        int filterRangeFromCommissionCounter = -1, filterRangeToCommissionCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterCommission")))
        {
            filterRangeCommissionCounter = Request.QueryString.Get("RangeCounterCommission").Trim();
            string[] filterRangeCommissionCounterVals = { };
            filterRangeCommissionCounterVals = filterRangeCommissionCounter.Split(';');
            if (filterRangeCommissionCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeCommissionCounterVals[0], out filterRangeFromCommissionCounter);
                Int32.TryParse(filterRangeCommissionCounterVals[1], out filterRangeToCommissionCounter);
                txtCommissionCounterRange.Text = filterRangeCommissionCounter;
            }
        }

        string filterRangeFeeCounter = null;
        int filterRangeFromFeeCounter = -1, filterRangeToFeeCounter = -1;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeCounterFee")))
        {
            filterRangeFeeCounter = Request.QueryString.Get("RangeCounterFee").Trim();
            string[] filterRangeFeeCounterVals = { };
            filterRangeFeeCounterVals = filterRangeFeeCounter.Split(';');
            if (filterRangeFeeCounterVals.Length == 2)
            {
                Int32.TryParse(filterRangeFeeCounterVals[0], out filterRangeFromFeeCounter);
                Int32.TryParse(filterRangeFeeCounterVals[1], out filterRangeToFeeCounter);
                txtFeeCounterRange.Text = filterRangeFeeCounter;
            }
        }

        string filterRangeFromReferralDate = null, filterRangeFromReferralDateAlt = null;
        DateTime filterRangeFromReferralDate_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeFromReferralDate")))
        {
            filterRangeFromReferralDate = Request.QueryString.Get("RangeFromReferralDate").Trim();
            this.txtRangeReferralFrom.Text = filterRangeFromReferralDate;
            paramsLinkCommission += "FilterFromApprovedAt=" + filterRangeFromReferralDate + "&";
            paramsLinkFee += "FilterFromApprovedAt=" + filterRangeFromReferralDate + "&";
            if (DateTime.TryParseExact(filterRangeFromReferralDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterRangeFromReferralDate_DateTime))
            {
                filterRangeFromReferralDateAlt = filterRangeFromReferralDate_DateTime.ToString("yyyy-MM-dd") + " 00:00:00:000";
            }
        }
        string filterRangeToReferralDate = null, filterRangeToReferralDateAlt = null;
        DateTime filterRangeToReferralDate_DateTime;
        if (!string.IsNullOrWhiteSpace(Request.QueryString.Get("RangeToReferralDate")))
        {
            filterRangeToReferralDate = Request.QueryString.Get("RangeToReferralDate").Trim();
            this.txtRangeReferralTo.Text = filterRangeToReferralDate;
            paramsLinkCommission += "FilterToApprovedAt=" + filterRangeToReferralDate + "&";
            paramsLinkFee += "FilterToApprovedAt=" + filterRangeToReferralDate + "&";
            if (DateTime.TryParseExact(filterRangeToReferralDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out filterRangeToReferralDate_DateTime))
            {
                filterRangeToReferralDateAlt = filterRangeToReferralDate_DateTime.ToString("yyyy-MM-dd") + " 23:59:59:999";
            }
        }
        string order_totalPost = "";
        if (!string.IsNullOrWhiteSpace(filterRangeFromReferralDateAlt) || !string.IsNullOrWhiteSpace(filterRangeToReferralDateAlt))
        {
            if (TuNgay != "" && DenNgay != "")
            {
                Utilities.ViewedManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
                colSumCounterName = "tb_ThanhVien";
                colSumCounterRealName = "( select count(tb_TinDang.idThanhVien) from tb_TinDang where tb_TinDang.idThanhVien = tb_ThanhVien.idThanhVien and NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59')";
                order_totalPost = colSumCounterRealName + @" desc";
            }
            if (TuNgay == "" && DenNgay == "")
            {
                Utilities.ViewedManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
                colSumCounterName = "tb_ThanhVien";
                colSumCounterRealName = "tb_ThanhVien.TongTinDang";
                order_totalPost = colSumCounterRealName + @" desc";
            }

            Utilities.ViewedManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colViewedCounterName = "ViewedCounter";
            colViewedCounterRealName = "ViewedManager.FilterCurrentValue";

            Utilities.CommissionManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colCommissionCounterName = "CommissionCounter";
            colCommissionCounterRealName = "CommissionManager.FilterCurrentValue";

            Utilities.FeeManager.applyFilter(filterRangeFromReferralDateAlt, filterRangeToReferralDateAlt);
            colFeeCounterName = "FeeCounter";
            colFeeCounterRealName = "FeeManager.FilterCurrentValue";
        }
        else
        {
            colViewedCounterName = "ViewedCounter";
            colViewedCounterRealName = "ViewedManager.TotalViewed";
            if(TuNgay != "" && DenNgay != "")
            {
                colSumCounterName = "tb_ThanhVien";
                colSumCounterRealName = "( select count(tb_TinDang.idThanhVien) from tb_TinDang where tb_TinDang.idThanhVien = tb_ThanhVien.idThanhVien and NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59')";
                order_totalPost = colSumCounterRealName + @" desc";
            }
            if (TuNgay == "" && DenNgay == "")
            {
                colSumCounterName = "tb_ThanhVien";
                colSumCounterRealName = "tb_ThanhVien.TongTinDang";
                order_totalPost = colSumCounterRealName + @" desc";
            }

            colCommissionCounterName = "CommissionCounter";
            colCommissionCounterRealName = "CommissionManager.TotalCommission";

            colFeeCounterName = "FeeCounter";
            colFeeCounterRealName = "FeeManager.TotalFee";
        }

        string sql = "";

        string order_ngayDangKy = "tb_ThanhVien.NgayDangKy desc";
         
        string order_totalView = colViewedCounterRealName + @" desc";
        string order_totalCommission = colCommissionCounterRealName + @" desc";
        string order_totalFee = colFeeCounterRealName + @" desc";

        string order_final = "";

        switch (sortColumn)
        {
            case "totalPost":
                order_totalPost = order_totalPost.Replace("desc", sortDirection == "1" ? "asc" : "desc");
                order_final = order_totalPost + @", " + order_ngayDangKy + @", " + order_totalCommission + @", " + order_totalFee + @", " + order_totalView;
                break;
            case "totalView":
                order_totalView = order_totalView.Replace("desc", sortDirection == "1" ? "asc" : "desc");
                order_final = order_totalView + @", " + order_ngayDangKy + @", " + order_totalCommission + @", " + order_totalFee + @", " + order_totalPost;
                break;
            case "totalFree":
                order_totalFee = order_totalFee.Replace("desc", sortDirection == "1" ? "asc" : "desc");
                order_final = order_totalFee + @", " + order_ngayDangKy + @", " + order_totalCommission + @", " + order_totalView + @", " + order_totalPost;
                break;
            case "totalCommission":
                order_totalCommission = order_totalCommission.Replace("desc", sortDirection == "1" ? "asc" : "desc");
                order_final = order_totalCommission + @", " + order_ngayDangKy + @", " + order_totalFee + @", " + order_totalView + @", " + order_totalPost;
                break;
            default:
                order_final = order_ngayDangKy + @", " + order_totalCommission + @", " + order_totalFee + @", " + order_totalView + @", " + order_totalPost;
                break;
        }
        string sx = "";
        if (sortDirection == "0")
        {
            sx = "desc";
        }
        if (sortDirection == "1")
        {
            sx = "asc";
        }
        Dictionary<string, string> postParams = null;
        Dictionary<string, string> paginationParams = new Dictionary<string, string>();
        if (TuNgay != "" && DenNgay != "")
        {
            sql += @"select tb2.* from(select ROW_NUMBER() OVER                 (                   
            ORDER BY ";
            sql += "( select count(tb_TinDang.idThanhVien) from tb_TinDang where tb_TinDang.idThanhVien = tb1.idThanhVien and NgayDang between '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00' and '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59')" + sx;
            sql += @", tb1.NgayDangKy desc                   ) as STT, tb1.* from
            (";
        }
        if (TuNgay == "" && DenNgay == "")
        {
            sql += @"select  tb1.* from
            (";
        }
	        sql += @"    SELECT ROW_NUMBER() OVER
                  (
                     ORDER BY " + order_final + @"
                    ) as RowNumber,
                      City.Ten as CityName,
                      District.Ten as DistrictName,
                      tb_PhuongXa.Ten as WardName,          
                      ISNULL(" + colViewedCounterRealName + @", 0) AS ViewedCounter,
                      ISNULL(" + colCommissionCounterRealName + @", 0) AS CommissionCounter,
                      ISNULL(" + colFeeCounterRealName + @", 0) AS FeeCounter,
                        ( select count(tb_TinDang.idThanhVien)
						from tb_TinDang 
						where tb_TinDang.idThanhVien = tb_ThanhVien.idThanhVien ";
                        if (TuNgay != "")
                        {
                            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00'";
                        }
                        if (DenNgay != "")
                        {
                            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59'";
                        }
		sql  +=			@"	) as SoLuong,
                        tb_ThanhVien.isDuyet,tb_ThanhVien.LinkAnh, tb_ThanhVien.idThanhVien, tb_ThanhVien.SoDienThoai, 
						tb_ThanhVien.Email, tb_ThanhVien.TenDangNhap, tb_ThanhVien.MatKhau,
						tb_ThanhVien.DiaChi, tb_ThanhVien.idTinh, tb_ThanhVien.idHuyen, tb_ThanhVien.idPhuongXa,
						tb_ThanhVien.NgayDangKy, tb_ThanhVien.SoCMND, tb_ThanhVien.HeadCMND, tb_ThanhVien.FooterCMND, 
						tb_ThanhVien.ProAt, tb_ThanhVien.idAdmin_Duyet, tb_ThanhVien.AdminAt, tb_ThanhVien.LyDo_KhongDuyet, 
						tb_ThanhVien.StsPro, tb_ThanhVien.isKhoa, tb_ThanhVien.TongTinDang
                  from tb_ThanhVien 
                	left join City on tb_ThanhVien.idTinh = City.id
	                left join District on tb_ThanhVien.idHuyen = District.id
	                left join tb_PhuongXa on tb_ThanhVien.idPhuongXa = tb_PhuongXa.id
                    INNER JOIN ViewedManager ON tb_ThanhVien.idThanhVien = ViewedManager.AccountId
                    INNER JOIN CommissionManager ON tb_ThanhVien.idThanhVien = CommissionManager.AccountId
                    INNER JOIN FeeManager ON tb_ThanhVien.idThanhVien = FeeManager.AccountId
                    left Join tb_TinDang ON tb_ThanhVien.idThanhVien = tb_TinDang.idThanhVien
                  where 1 = 1
            ";
        //if (TuNgay != "")
        //{
        //    sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(TuNgay) + " 00:00:00'";
        //}
        //if (DenNgay != "")
        //{
        //    sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(DenNgay) + " 23:59:59'";
        //}
        if (LoaiThanhVien != "")
        {
            if (LoaiThanhVien == "0")
                sql += " and (StsPro is null or StsPro='0' or tb_ThanhVien.LyDo_KhongDuyet !='')";
            else if (LoaiThanhVien == "2")
                sql += "";
            else if (LoaiThanhVien == "-1")
            {
                sql += "and tb_ThanhVien.LyDo_KhongDuyet !=' ' ";
            }
            else
                sql += " and isDuyet ='1'";
        }
        if (AdminDuyet != "")
            sql += " and tb_ThanhVien.idAdmin_Duyet = '" + AdminDuyet + "'";
        if (!string.IsNullOrWhiteSpace(filterCity))
        {
            sql += "       AND tb_ThanhVien.idTinh = '" + filterCity + "' ";
            paginationParams.Add("FilterCity", filterCity);
        }
        if (!string.IsNullOrWhiteSpace(filterDistrict))
        {
            sql += "       AND tb_ThanhVien.idHuyen = '" + filterDistrict + "' ";
            paginationParams.Add("FilterDistrict", filterDistrict);
        }
        if (!string.IsNullOrWhiteSpace(filterWard))
        {
            sql += "       AND tb_ThanhVien.idPhuongXa = '" + filterWard + "' ";
            paginationParams.Add("FilterWard", filterWard);
        }
        if (sSoDienThoai != "")
            sql += " and tb_ThanhVien.idThanhVien ='" + sSoDienThoai + "'";
        if (sTuNgay != "")
        {
            if (LoaiThanhVien == "1")
            {
                sql += " and AdminAt >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
            }
            else
                sql += " and NgayDangKy >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        }
        if (sDenNgay != "")
        {
            if (LoaiThanhVien == "1")
            {
                sql += " and AdminAt <= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 23:59:59'";
            }
            else
                sql += " and NgayDangKy <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";

        }
        //Lọc Lượt xem
        if (slistLabel == "100500")
            sql += " and TotalViewed between 100 and 500";
        if (slistLabel == "5001000")
            sql += " and TotalViewed between 500 and 1000";
        if (slistLabel == "1000")
            sql += " and TotalViewed >= 1000";
        if (slistLabel == "1005001000")
            sql += " and TotalViewed between 100 and 500 or TotalViewed >= 1000";
        if (slistLabel == "50010001000")
            sql += " and TotalViewed between 500 and 1000 or TotalViewed >= 1000";
        if (slistLabel == "1005005001000")
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000";
        if (slistLabel == "10050050010001000")
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000 or TotalViewed >= 1000";
        //Lọc Tổng tin đăng
        if (sTongTin == "100500" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang between 100 and 500";
        if (sTongTin == "5001000" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang between 500 and 1000";
        if (sTongTin == "1000" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang >= 1000";
        if (sTongTin == "1005001000" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang between 100 and 500 or TongTinDang >= 1000";
        if (sTongTin == "50010001000" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang between 500 and 1000 or TongTinDang >= 1000";
        if (sTongTin == "1005005001000" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang between 100 and 500 or TongTinDang between 500 and 1000";
        if (sTongTin == "10050050010001000" && sRangeTong == "" && TuNgay == "" && DenNgay == "")
            sql += " and TongTinDang between 100 and 500 or TongTinDang between 500 and 1000 or TongTinDang >= 1000";
        //Lọc Hoa hồng
        if (slistLabelhh == "10100")
            sql += " and (TotalCommission between 10000000 and 100000000)";
        if (slistLabelhh == "100500")
            sql += " and (TotalCommission between 100000000 and 500000000)";
        if (slistLabelhh == "500")
            sql += " and (TotalCommission >= 500000000)";
        if (slistLabelhh == "10100500")
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission >= 500000000)";
        if (slistLabelhh == "100500500")
            sql += " and (TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000)";
        if (slistLabelhh == "10100100500")
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000)";
        if (slistLabelhh == "10100100500500")
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000)";
        //Lọc Chi Hoa hồng
        if (slistLabelchh == "10100")
            sql += " and TotalFee between 10000000 and 100000000";
        if (slistLabelchh == "100500")
            sql += " and TotalFee between 100000000 and 500000000";
        if (slistLabelchh == "500")
            sql += " and TotalFee >= 500000000";
        if (slistLabelchh == "10100500")
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee >= 500000000";
        if (slistLabelchh == "100500500")
            sql += " and TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
        if (slistLabelchh == "10100100500")
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000";
        if (slistLabelchh == "10100100500500")
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
        //if (sSoDienThoai != "")
        //    sql += " and SoDienThoai like '%" + sSoDienThoai + "%'";
        //Lọc Lượt xem với thước
        if (sruler != "" && slistLabel == "0")
        {
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "100500")
        {
            sql += " and TotalViewed between 100 and 500";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "5001000")
        {
            sql += " and TotalViewed between 500 and 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "1000")
        {
            sql += " and TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "1005001000")
        {
            sql += " and TotalViewed between 100 and 500 or TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "50010001000")
        {
            sql += " and TotalViewed between 500 and 1000 or TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "1005005001000")
        {
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }
        if (sruler != "" && slistLabel == "10050050010001000")
        {
            sql += " and TotalViewed between 100 and 500 or TotalViewed between 500 and 1000 or TotalViewed >= 1000";
            if (filterRangeFromViewedCounter >= 0)
            {
                sql += " or " + colViewedCounterRealName + " >= " + filterRangeFromViewedCounter + " ";
            }
            if (filterRangeToViewedCounter >= 0)
            {
                sql += " and " + colViewedCounterRealName + " <= " + filterRangeToViewedCounter + " ";
            }
        }

        //Lọc Tổng tin đăng với thước
        if (sRangeTong != "" && sTongTin == "0" && TuNgay == "" && DenNgay == "")
        {
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " and TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "100500" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang between 100 and 500";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "5001000" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang between 500 and 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "1000" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "1005001000" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang between 100 and 500 or TongTinDang >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "50010001000" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang between 500 and 1000 or TongTinDang >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "1005005001000" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang between 100 and 500 or TongTinDang between 500 and 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "10050050010001000" && TuNgay == "" && DenNgay == "")
        {
            sql += " and TongTinDang between 100 and 500 or TongTinDang between 500 and 1000 or TongTinDang >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or TongTinDang >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and TongTinDang <= " + filterRangeToSumCounter + " ";
            }
        }

        //Lọc Hoa hồng với thước
        if (srulerhh != "" && slistLabelhh == "0")
        {
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100")
        {
            sql += " and TotalCommission between 10000000 and 100000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500")
        {
            sql += " and TotalCommission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "500")
        {
            sql += " and TotalCommission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100500")
        {
            sql += " and TotalCommission between 10000000 and 100000000 or TotalCommission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "100500500")
        {
            sql += " and TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500")
        {
            sql += " and TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        if (srulerhh != "" && slistLabelhh == "10100100500500")
        {
            sql += " and (TotalCommission between 10000000 and 100000000 or TotalCommission between 100000000 and 500000000 or TotalCommission >= 500000000)";
            if (filterRangeFromCommissionCounter >= 0)
            {
                sql += " or " + colCommissionCounterRealName + " >= " + filterRangeFromCommissionCounter + " ";
            }
            if (filterRangeToCommissionCounter >= 0)
            {
                sql += " and " + colCommissionCounterRealName + " <= " + filterRangeToCommissionCounter + " ";
            }
        }
        //Lọc Chi hoa hồng với thước       
        if (srulerchh != "" && slistLabelchh == "0")
        {
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100")
        {
            sql += " and TotalFee between 10000000 and 100000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500")
        {
            sql += " and TotalFee between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "500")
        {
            sql += " and TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100500")
        {
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "100500500")
        {
            sql += " and TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500")
        {
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        if (srulerchh != "" && slistLabelchh == "10100100500500")
        {
            sql += " and TotalFee between 10000000 and 100000000 or TotalFee between 100000000 and 500000000 or TotalFee >= 500000000";
            if (filterRangeFromFeeCounter >= 0)
            {
                sql += " or " + colFeeCounterRealName + " >= " + filterRangeFromFeeCounter + " ";
            }
            if (filterRangeToFeeCounter >= 0)
            {
                sql += " and " + colFeeCounterRealName + " <= " + filterRangeToFeeCounter + " ";
            }
        }
        sql += @" 
            GROUP BY tb_ThanhVien.NgayDangKy,tb_ThanhVien.LinkAnh, tb_ThanhVien.SoCMND, tb_ThanhVien.HeadCMND, tb_ThanhVien.FooterCMND, tb_ThanhVien.ProAt, tb_ThanhVien.idAdmin_Duyet, tb_ThanhVien.AdminAt, 
            tb_ThanhVien.LyDo_KhongDuyet, tb_ThanhVien.StsPro, tb_ThanhVien.isKhoa,  tb_ThanhVien.DiaChi, 
            tb_ThanhVien.idTinh, tb_ThanhVien.idHuyen, tb_ThanhVien.idPhuongXa, tb_ThanhVien.SoDienThoai, tb_ThanhVien.Email, tb_ThanhVien.TenDangNhap, tb_ThanhVien.MatKhau, 
            tb_ThanhVien.isDuyet,tb_ThanhVien.idThanhVien,ViewedManager.TotalViewed , CommissionManager.TotalCommission , 
            FeeManager.TotalFee, City.Ten,  District.Ten,tb_PhuongXa.Ten, tb_ThanhVien.TongTinDang,CommissionManager.FilterCurrentValue,FeeManager.FilterCurrentValue,ViewedManager.FilterCurrentValue 
            ) as tb1 WHERE '1'='1'";
        
        string sql1 = "";
        //Lọc Tổng tin đăng
        if (sTongTin == "100500" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong between 100 and 500";
        if (sTongTin == "5001000" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong between 500 and 1000";
        if (sTongTin == "1000" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong >= 1000";
        if (sTongTin == "1005001000" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong between 100 and 500 or SoLuong >= 1000";
        if (sTongTin == "50010001000" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong between 500 and 1000 or SoLuong >= 1000";
        if (sTongTin == "1005005001000" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong between 100 and 500 or SoLuong between 500 and 1000";
        if (sTongTin == "10050050010001000" && sRangeTong == "" && TuNgay != "" && DenNgay != "")
            sql += " and SoLuong between 100 and 500 or SoLuong between 500 and 1000 or SoLuong >= 1000";
        //Lọc Tổng tin đăng với thước
        if (sRangeTong != "" && sTongTin == "0" && TuNgay != "" && DenNgay != "")
        {
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " and SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "100500" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong between 100 and 500";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "5001000" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong between 500 and 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "1000" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "1005001000" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong between 100 and 500 or SoLuong >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "50010001000" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong between 500 and 1000 or SoLuong >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "1005005001000" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong between 100 and 500 or SoLuong between 500 and 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (sRangeTong != "" && sTongTin == "10050050010001000" && TuNgay != "" && DenNgay != "")
        {
            sql += " and SoLuong between 100 and 500 or SoLuong between 500 and 1000 or SoLuong >= 1000";
            if (filterRangeFromSumCounter >= 0)
            {
                sql += " or SoLuong >= " + filterRangeFromSumCounter + " ";
            }
            if (filterRangeToSumCounter >= 0)
            {
                sql += " and SoLuong <= " + filterRangeToSumCounter + " ";
            }
        }
        if (TuNgay != "" && DenNgay != "")
        {
            sql += ") as tb2 where  '1'='1'";
        }
        int AllRowNumber = Connect.GetTable(sql).Rows.Count;
        if (TuNgay == "" && DenNgay == "")
        {
            sql += " and RowNumber BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        }
        if (TuNgay != "" && DenNgay != "")
        {
            sql += " and STT BETWEEN (" + Page + " - 1) * " + PageSize + " + 1 AND (((" + Page + " - 1) * " + PageSize + " + 1) + " + PageSize + ") - 1";
        }
        string total = "";
        string sortsx = "";
        try
        {
            total = Request.QueryString["sortColumn"].Trim();
        }
        catch { }
        try
        {
            sortsx = Request.QueryString["sortDirection"].Trim();
        }
        catch { }
        if (total == "totalPost" && sortsx == "0" && TuNgay == "" && DenNgay == "")
        {
            sql1 += " order by SoLuong desc";
        }
        if (total == "totalPost" && sortsx == "1" && TuNgay == "" && DenNgay == "")
        {
            sql1 += " order by SoLuong asc";
        }
        if (total == "totalView" && sortsx == "0")
        {
            sql1 += " order by ViewedCounter desc";
        }
        if (total == "totalView" && sortsx == "1")
        {
            sql1 += " order by ViewedCounter asc";
        }
        if (total == "totalFree" && sortsx == "0")
        {
            sql1 += " order by FeeCounter desc";
        }
        if (total == "totalFree" && sortsx == "1")
        {
            sql1 += " order by FeeCounter asc";
        }
        if (total == "totalCommission" && sortsx == "0")
        {
            sql1 += " order by CommissionCounter desc";
        }
        if (total == "totalCommission" && sortsx == "1")
        {
            sql1 += " order by CommissionCounter asc";
        }
        if (total == "" && sortsx == "" && TuNgay == "" && DenNgay == "")
        {
            sql1 += " order by tb1.NgayDangky desc";
        }

        string truyvan = sql + sql1;
        DataTable table = Connect.GetTable(truyvan);
            txtTongThanhVien.Value = AllRowNumber.ToString();
            SetPage(AllRowNumber);        

        if (paramsLinkCommission != "&")
        {
            paramsLinkCommission += "&FilterTypeApprovedAt=ReferralCollabApprovedAt";
        }
        else
        {
            paramsLinkCommission = "";
        }

        if (paramsLinkFee != "&")
        {
            paramsLinkFee += "&FilterTypeApprovedAt=ReferralOwnerApprovedAt";
        }
        else
        {
            paramsLinkFee = "";
        }

        string html = @"<table id='myTable' class='table table-bordered table-active'>
                            <thead>                          
                            <tr>
                                <th class='th' colspan='9' style='text-align: center;'>Thông tin chi tiết</th>
                                <th class='th' colspan='8'  style='text-align: center;'>Tài khoản Pro</th>
                                <th class='th' colspan='7'  style='text-align: center;'></th>
                            </tr>                                                        
                            <tr>
                                <th class='th' style='white-space: nowrap;'>STT</th>
                                <th class='th'>Mã thành viên</th>
                                <th class='th'>Tên thành viên</th>
                                <th class='th'>Ảnh đại diện</th>
                                <th class='th'>Địa chỉ</th>
                                <th class='th'>Số điên thoại</th>
                                <th class='th'>Email</th>
                                
                                <th class='th'>Tên đăng nhập</th>
                                <th class='th'>Mật khẩu</th>
                                <th class='th'>Ngày đăng ký</th>

                                <th class='th'>Số CMND/CCCD</th>
                                <th class='th'>Ảnh CMND Mặt trước</th>
                                <th class='th'>Ảnh CMND Mặt sau</th>
                                <th class='th'>Ngày gửi duyệt</th>
                                <th class='th'>Admin Duyệt</th>
                                <th class='th'>Ngày Admin duyệt</th>
                                <th class='th'>Lý Do Không Duyệt</th>
                                <th class='th'>Trạng thái</th>
                                
                                <th class='th js-sort-number' href='javascript:void(0);' id='clnTienHH'> Tiền hoa hồng</th>
                                <th class='th js-sort-number' href='javascript:void(0);' id='clnChiHH'> Chi hoa hồng</th>
                                <th class='th js-sort-number' href='javascript:void(0);' id='clnLuotXem'> Lượt xem</th>
                                <th class='th js-sort-number'>isKhoa</th>
                                <th class='th js-sort-number'></th>
                                <th class='th js-sort-number' href='javascript:void(0);' id='clnTongTin'>Tổng tin đăng </th>
                            </tr></thead>";
        string idTV = "";
        int z = 0;
        int idTVien = 0;
        int a = 0;
        int v = 0;
        int x = 0;
        int page = 1;
        if (Request.QueryString["Page"] != null)
        {
            int oPage;
            if (Int32.TryParse(Request.QueryString["Page"], out oPage))
            {
                page = oPage;
            }
        }
        int stt = page * 70 - 69;
        html += "<tbody>";
        for (int i = 0; i < table.Rows.Count; i++)
        {           
                            idTV = table.Rows[i]["isDuyet"].ToString();
                            idTVien = Convert.ToInt32(table.Rows[i]["idThanhVien"]);
                            if (z != idTVien)
                            {
                                z = idTVien;
                                html += "<tr>";
                                html += " <td style='text-align:center'>" + stt + "</td>";
                                stt++;
                                string Code = StaticData.getField("tb_ThanhVien", "Code", "idThanhVien", table.Rows[i]["idThanhVien"].ToString());
                                if (table.Rows[i]["isDuyet"].ToString() == "1")
                                {
                                    html += " <td>" + Code + "-PRO</td>";
                                }
                                else
                                    html += " <td>" + Code + "</td>";
                                string TenCuaHang = StaticData.getField("tb_ThanhVien", "TenCuaHang", "idThanhVien", table.Rows[i]["idThanhVien"].ToString());
                                html += "       <td>" + TenCuaHang + "</td>";
                                string LinkAnh = StaticData.getField("tb_ThanhVien", "LinkAnh", "idThanhVien", table.Rows[i]["idThanhVien"].ToString());
                                  html += "       <td><img src='../../Images/User/" + table.Rows[i]["LinkAnh"] + "' style='width:115px'/></td>";
                            
                                string Tinh = StaticData.getField("City", "Ten", "id", table.Rows[i]["idTinh"].ToString());
                                string Huyen = StaticData.getField("District", "Ten", "id", table.Rows[i]["idHuyen"].ToString());
                                string PhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[i]["idPhuongXa"].ToString());
                                html += "       <td class='social'>" + table.Rows[i]["DiaChi"] + ", " + PhuongXa + ", " + Huyen + ", " + Tinh + "</td>";
                                html += "       <td>" + table.Rows[i]["SoDienThoai"] + "</td>";
                                html += "       <td>" + table.Rows[i]["Email"] + "</td>";

                                html += "       <td>" + table.Rows[i]["TenDangNhap"] + "</td>";
                                html += "       <td>" + table.Rows[i]["MatKhau"] + "</td>";
                                try
                                {
                                    html += "       <td>" + DateTime.Parse(table.Rows[i]["NgayDangKy"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                                }
                                catch
                                {
                                    html += "       <td></td>";
                                }

                                //pro
                                html += "       <td>" + table.Rows[i]["SoCMND"] + "</td>";
                                html += "       <td><img src='../../Images/User/" + table.Rows[i]["HeadCMND"] + "' style='width:115px'/></td>";
                                html += "       <td><img src='../../Images/User/" + table.Rows[i]["FooterCMND"] + "' style='width:115px'/></td>";
                                //new
                                if (table.Rows[i]["ProAt"].ToString().ToString() != "")
                                    html += "   <td>" + DateTime.Parse(table.Rows[i]["ProAt"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                                else
                                    html += "   <td></td>";
                                if (table.Rows[i]["idAdmin_Duyet"].ToString() != "")
                                    html += "       <td>" + StaticData.getField("tb_Admin", "TenDangNhap", "idAdmin", table.Rows[i]["idAdmin_Duyet"].ToString()) + "</td>";
                                else
                                    html += "   <td></td>";
                                if (table.Rows[i]["AdminAt"].ToString().ToString() != "")
                                    html += "   <td>" + DateTime.Parse(table.Rows[i]["AdminAt"].ToString()).ToString("dd/MM/yyyy HH:mm") + "</td>";
                                else
                                    html += "   <td></td>";
                                if (table.Rows[i]["LyDo_KhongDuyet"].ToString() != "")
                                    html += "   <td>" + table.Rows[i]["LyDo_KhongDuyet"].ToString() + "</td>";
                                else
                                    html += "   <td></td>";
                //Trạng thái
                if (table.Rows[i]["HeadCMND"].ToString().Trim() == "" && table.Rows[i]["FooterCMND"].ToString().Trim() == "")
                {
                    html += "<td><p id='slDuyet_" + table.Rows[i]["idThanhVien"] + "'>Chưa đăng ký</p></td>";
                }
                else
                {
                    html += "<td><select id='slDuyet_" + table.Rows[i]["idThanhVien"] + "' disabled='disabled' class='form-control' style='width:122px'>";
                    if (table.Rows[i]["idAdmin_Duyet"].ToString().Trim() == "-1" && table.Rows[i]["StsPro"].ToString().Trim() == "0")
                        html += "<option value='DangCho' selected='selected'>Đang chờ...</option>";
                    else
                        html += "<option value='DangCho'>Đang chờ...</option>";
                    if (table.Rows[i]["StsPro"].ToString().Trim() == "1")
                        html += "<option value='Duyet' selected='selected'>Duyệt</option>";
                    else
                        html += "<option value='Duyet'>Duyệt</option>";
                    if (table.Rows[i]["LyDo_KhongDuyet"].ToString().Trim() != "")
                        html += "<option value='KhongDuyet' selected='selected'>Không duyệt</option>";
                    else
                        html += "<option value='KhongDuyet'>Không duyệt</option>";
                    html += "       </select></td>";
                }
                //end new 
                //end pro  

                html += "       <td><a href='/Admin/Referral/ReferralTableByCollab.aspx?CollabId=" + table.Rows[i]["idThanhVien"].ToString() + (paramsLinkCommission != "" ? paramsLinkCommission : "") + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["CommissionCounter"].ToString()) + "</a></td>";
                                html += "       <td><a href='/Admin/Referral/ReferralTableByOwner.aspx?OwnerId=" + table.Rows[i]["idThanhVien"].ToString() + (paramsLinkFee != "" ? paramsLinkFee : "") + "'>" + Utilities.Formatter.toCurrencyString(table.Rows[i]["FeeCounter"].ToString()) + "</a></td>";
                                html += "       <td>" + table.Rows[i]["ViewedCounter"].ToString() + "</td>";
                                if (table.Rows[i]["isKhoa"].ToString() == "True")
                                    html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled checked/></td>";
                                else
                                    html += "   <td><input id='ckKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' type='checkbox' disabled/></td>";

                                html += "       <td><a id='btKhoa_" + table.Rows[i]["idThanhVien"].ToString() + "' style='cursor:pointer' onclick='DuyetUserPro(\"" + table.Rows[i]["idThanhVien"].ToString() + "\")'><img class='imgedit' src='../images/edit.png'/>Sửa</a></td>";
                                //html += "       <td><a href='/Admin/Posts/PostTable.aspx?TenDangNhap=" + table.Rows[i]["TenDangNhap"].ToString() + "'>Xem tin đăng</a></td>"; 
                                if (TuNgay != "" && DenNgay != "")
                                {                               
                                    html += "       <td style='min-width:80px;text-align:center'><a href='/Admin/Posts/PostTable.aspx?TuNgay=" + TuNgay + "&DenNgay=" + DenNgay + "&TenDangNhap=" + table.Rows[i]["idThanhVien"].ToString() + "'> " + table.Rows[i]["SoLuong"].ToString() + "</a></td>";
                                }
                                if (TuNgay == "" && DenNgay == "")
                                {
                                    html += "       <td style='min-width:80px;text-align:center'><a href='/Admin/Posts/PostTable.aspx?TenDangNhap=" + table.Rows[i]["idThanhVien"].ToString() + "'> " + table.Rows[i]["TongTinDang"].ToString() + "</a></td>";
                                }
                                html += "       </tr>";
                            }
                        }                   
        html += "</tbody>";
        html += "   <tr>";
        html += "       <td colspan='17' class='footertable'>";
        string url = "AccountTable.aspx?";
        if (sHoTen != "")
            url += "HoTen=" + sHoTen + "&";
        if (sTuNgay != "")
            url += "TuNgay=" + sTuNgay + "&";
        if (sDenNgay != "")
            url += "DenNgay=" + sDenNgay + "&";
        if (sSoDienThoai != "")
            url += "SoDienThoai=" + sSoDienThoai + "&";
        //if (sSoDienThoai != "")
        //    url += "SoDienThoai=" + sSoDienThoai + "&";
        if (LoaiThanhVien != "")
            url += "LoaiThanhVien=" + LoaiThanhVien + "&";
        if (AdminDuyet != "")
            url += "AdminDuyet=" + AdminDuyet + "&";
        if (filterCity != null)
            url += "FilterCity=" + filterCity + "&";
        if (filterDistrict != null)
            url += "FilterDistrict=" + filterDistrict + "&";
        if (filterWard != null)
            url += "FilterWard=" + filterWard + "&";
        //Lọc lượt xem
        if (txtRangeLuotXem.Value == "Từ 100 đến 500")
            url += "Luotxem=100500&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000")
            url += "Luotxem=5001000&";
        if (txtRangeLuotXem.Value == "Từ 1000 trở lên")
            url += "Luotxem=1000&";
        //if(checklx1.Checked)
        //{
        //    url += "Luotxem=100500&";
        //}
        // url += "Luotxem=1000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 1000 trở lên")
            url += "Luotxem=1005001000&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=50010001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000")
            url += "Luotxem=1005005001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=10050050010001000&";
        //Lọc Tổng tin đăng
        string TongTinUrl = "";
        string TongTinRangeUrl = "";
        string rangeSumCounter = txtSumPostCounterRange.Text.Trim();
        try
        {
            TongTinUrl = Request.QueryString["TongTinDang"].Trim();
        }
        catch { }
        try
        {
            TongTinRangeUrl = Request.QueryString["RangeCounterSumPost"].Trim();
        }
        catch { }
        if (txtRangeSum.Value == "Từ 100 đến 500" || TongTinUrl == "100500")
            url += "TongTinDang=100500&";
        if (txtRangeSum.Value == "Từ 500 đến 1000" || TongTinUrl == "5001000")
            url += "TongTinDang=5001000&";
        if (txtRangeSum.Value == "Từ 1000 trở lên" || TongTinUrl == "1000")
            url += "TongTinDang=1000&";
        if (txtRangeSum.Value == "Từ 100 đến 500Từ 1000 trở lên" || TongTinUrl == "1005001000")
            url += "TongTinDang=1005001000&";
        if (txtRangeSum.Value == "Từ 500 đến 1000Từ 1000 trở lên" || TongTinUrl == "50010001000")
            url += "TongTinDang=50010001000&";
        if (txtRangeSum.Value == "Từ 100 đến 500Từ 500 đến 1000" || TongTinUrl == "1005005001000")
            url += "TongTinDang=1005005001000&";
        if (txtRangeSum.Value == "Từ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên" || TongTinUrl == "10050050010001000")
            url += "TongTinDang=10050050010001000&";
        if (TongTinRangeUrl != "" && TongTinUrl == "0")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=0&";
        }
        if (TongTinRangeUrl != "" && TongTinUrl == "100500")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=100500&";
        }
        if (TongTinRangeUrl != "" && TongTinUrl == "5001000")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=5001000&";
        }
        if (TongTinUrl == "1000" && TongTinRangeUrl != "")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=1000&";
        }
        if (TongTinRangeUrl != "" && TongTinUrl == "1005001000")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=1005001000&";
        }
        if (TongTinRangeUrl != "" && TongTinUrl == "50010001000")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=50010001000&";
        }
        if (TongTinRangeUrl != "" && TongTinUrl == "1005005001000")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=1005005001000&";
        }
        if (TongTinRangeUrl != "" && TongTinUrl == "10050050010001000")
        {
            url += "RangeCounterSumPost=" + TongTinRangeUrl + "&TongTinDang=10050050010001000&";
        }
        //Lọc hoa hồng
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
            url += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
            url += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
            url += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=10100100500500&";
        //Lọc hoa hồng       
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
            url += "ChiHoaHong=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
            url += "ChiHoaHong=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
            url += "ChiHoaHong=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "ChiHoaHong=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "ChiHoaHong=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=10100100500500&";
        if (TuNgay != "")
        {
            url += "RangeFromReferralDate=" + TuNgay + "&";
        }
        if (DenNgay != "")
        {
            url += "RangeToReferralDate=" + DenNgay + "&";
        }
        if (sortColumn != "" && sortDirection != "")
        {
            url += "sortColumn=" + sortColumn + "&sortDirection=" + sortDirection + "&";
        }

        url += "Page=";
        html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        //Page 1
        if (txtPage1 != "")
        {
            if (Page.ToString() == txtPage1)
                html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
            else
                html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        else
        {
            html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        }
        //Page 2
        if (txtPage2 != "")
        {
            if (Page.ToString() == txtPage2)
                html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
            else
                html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        else
        {
            html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        }
        //Page 3
        if (txtPage3 != "")
        {
            if (Page.ToString() == txtPage3)
                html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
            else
                html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        else
        {
            html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        }
        //Page 4
        if (txtPage4 != "")
        {
            if (Page.ToString() == txtPage4)
                html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
            else
                html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        else
        {
            html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        }
        //Page 5
        if (txtPage5 != "")
        {
            if (Page.ToString() == txtPage5)
                html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
            else
                html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }
        else
        {
            html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        }

        html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        html += "   </td></tr>";
        html += "     </table>";
        dvThanhVien.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string SoDienThoai = SlSdt.Value;
        string City = ipCity.Value.Trim();
        string DisTricts = ipDistrict.Value.Trim();
        string Ward = ipWard.Value.Trim();
        string rangeCounterViewed = txtViewedCounterRange.Text.Trim();
        string rangeSumCounter = txtSumPostCounterRange.Text.Trim();
        string rangeCounterCommission = txtCommissionCounterRange.Text.Trim();
        string rangeCounterFee = txtFeeCounterRange.Text.Trim();
        string rangeFromReferralDate = txtRangeReferralFrom.Text.Trim();
        string rangeToReferralDate = txtRangeReferralTo.Text.Trim();
        string LoaiThanhVien = slTV.Value.Trim();
        string AdminDuyet = SlAdminDuyet.Value.Trim();


        string url = "AccountTable.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (SoDienThoai != "")
            url += "SoDienThoai=" + SoDienThoai + "&";
        if (City != "")
            url += "FilterCity=" + City + "&";
        if (DisTricts != "")
            url += "FilterDisTrict=" + DisTricts + "&";
        if (Ward != "")
            url += "FilterWard=" + Ward + "&";
        if (LoaiThanhVien != "")
            url += "LoaiThanhVien=" + LoaiThanhVien + "&";
        if (AdminDuyet != "")
            url += "AdminDuyet=" + AdminDuyet + "&";
        //Lọc Lượt xem
        if (txtRangeLuotXem.Value == "Chọn")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=0&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=100500&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 500 đến 1000")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=5001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=1000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500Từ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=1005001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 500 đến 1000Từ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=50010001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500Từ 500 đến 1000")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=1005005001000&";
        }
        if (txtRangeLuotXem.Value == "ChọnTừ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
        {
            if (rangeCounterViewed != "")
                url += "RangeCounterViewed=" + rangeCounterViewed + "&";
            url += "Luotxem=10050050010001000&";
        }
        if (txtRangeLuotXem.Value == "Từ 100 đến 500")
            url += "Luotxem=100500&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000")
            url += "Luotxem=5001000&";
        if (txtRangeLuotXem.Value == "Từ 1000 trở lên")
            url += "Luotxem=1000&";

        //if(checklx1.Checked)
        //{
        //    url += "Luotxem=100500&";
        //}
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 1000 trở lên")
            url += "Luotxem=1005001000&";
        if (txtRangeLuotXem.Value == "Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=50010001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000")
            url += "Luotxem=1005005001000&";
        if (txtRangeLuotXem.Value == "Từ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
            url += "Luotxem=10050050010001000&";

        //Lọc Hoa hồng
        if (txtRangeLuotXemhh.Value == "Chọn")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=0&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=100500500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100100500&";
        }
        if (txtRangeLuotXemhh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterCommission != "")
                url += "RangeCounterCommission=" + rangeCounterCommission + "&";
            url += "HoaHong=10100100500500&";
        }
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100")
            url += "HoaHong=10100&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500")
            url += "HoaHong=100500&";
        if (txtRangeLuotXemhh.Value == "Từ 500 trở lên")
            url += "HoaHong=500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "HoaHong=10100500&";
        if (txtRangeLuotXemhh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=100500500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "HoaHong=10100100500&";
        if (txtRangeLuotXemhh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "HoaHong=10100100500500&";
        //Lọc Chi Hoa hồng
        if (txtRangeLuotXemchh.Value == "Chọn")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=0&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=100500500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100100500&";
        }
        if (txtRangeLuotXemchh.Value == "ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
        {
            if (rangeCounterFee != "")
                url += "RangeCounterFee=" + rangeCounterFee + "&";
            url += "ChiHoaHong=10100100500500&";
        }
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100")
            url += "ChiHoaHong=10100&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500")
            url += "ChiHoaHong=100500&";
        if (txtRangeLuotXemchh.Value == "Từ 500 trở lên")
            url += "ChiHoaHong=500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 500 trở lên")
            url += "ChiHoaHong=10100500&";
        if (txtRangeLuotXemchh.Value == "Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=100500500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500")
            url += "ChiHoaHong=10100100500&";
        if (txtRangeLuotXemchh.Value == "Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên")
            url += "ChiHoaHong=10100100500500&";
        if (rangeFromReferralDate != "")
            url += "RangeFromReferralDate=" + rangeFromReferralDate + "&";
        if (rangeToReferralDate != "")
            url += "RangeToReferralDate=" + rangeToReferralDate + "&";
        //Tổng tin đăng
        if (txtRangeSum.Value == "Chọn")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=0&";
        }
        if (txtRangeSum.Value == "ChọnTừ 100 đến 500")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=100500&";
        }
        if (txtRangeSum.Value == "ChọnTừ 500 đến 1000")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=5001000&";
        }
        if (txtRangeSum.Value == "ChọnTừ 1000 trở lên")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=1000&";
        }
        if (txtRangeSum.Value == "ChọnTừ 100 đến 500Từ 1000 trở lên")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=1005001000&";
        }
        if (txtRangeSum.Value == "ChọnTừ 500 đến 1000Từ 1000 trở lên")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=50010001000&";
        }
        if (txtRangeSum.Value == "ChọnTừ 100 đến 500Từ 500 đến 1000")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=1005005001000&";
        }
        if (txtRangeSum.Value == "ChọnTừ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
        {
            if (rangeSumCounter != "")
                url += "RangeCounterSumPost=" + rangeSumCounter + "&";
            url += "TongTinDang=10050050010001000&";
        }
        if (txtRangeSum.Value == "Từ 100 đến 500")
            url += "TongTinDang=100500&";
        if (txtRangeSum.Value == "Từ 500 đến 1000")
            url += "TongTinDang=5001000&";
        if (txtRangeSum.Value == "Từ 1000 trở lên")
            url += "TongTinDang=1000&";
        if (txtRangeSum.Value == "Từ 100 đến 500Từ 1000 trở lên")
            url += "TongTinDang=1005001000&";
        if (txtRangeSum.Value == "Từ 500 đến 1000Từ 1000 trở lên")
            url += "TongTinDang=50010001000&";
        if (txtRangeSum.Value == "Từ 100 đến 500Từ 500 đến 1000")
            url += "TongTinDang=1005005001000&";
        if (txtRangeSum.Value == "Từ 100 đến 500Từ 500 đến 1000Từ 1000 trở lên")
            url += "TongTinDang=10050050010001000&";

        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "AccountTable.aspx";
        Response.Redirect(url);
    }

    //    protected void showList_Click(object sender, EventArgs e)
    //    {
    //        //listLabel.Text = "";
    //        ////loop through ArrayList to print selected list item
    //        //for (int i = 0; i < myCheckBoxes.Count; i++)
    //        //{
    //        //    listLabel.Text += myCheckBoxes[i].ToString() + " ";
    //        //}
    //    }    
}