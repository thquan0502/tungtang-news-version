﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangKy : System.Web.UI.Page
{
    string idThanhVien = "";
    bool kq;
    protected void Page_Load(object sender, EventArgs e)
    {
        //string soDT = txtSoDienThoai.Value.Trim();
        if (Request.Cookies["TungTang_Login"] == null || Request.Cookies["TungTang_Login"].Value.Trim() == "")
        {
            Response.Redirect("/dang-nhap/dn");
        }
        else
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        if (!IsPostBack)
        {
            //LoadDiaDiem();
            LoadThongTinNguoiDung();
            checkSoDT();//Code new
        }
    }
    public void checkSoDT()//Code new
    {
        string result = "";
        string sql = "Select top 1 SoDienThoai from tb_ThanhVien where idThanhVien='" + idThanhVien + "' and isnull(isKhoa,'False')!='True'";
        try
        {
            DataTable tb = Connect.GetTable(sql);
            if (tb.Rows.Count > 0)
                result = tb.Rows[0][0].ToString();
        }
        catch
        { }
        if (result == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Bạn cần cập nhật thông tin số điện thoại!')", true);
        }
    }
    void LoadThongTinNguoiDung()
    {
        DataTable table = Connect.GetTable("select * from tb_ThanhVien where idThanhVien=" + idThanhVien);
        if (table.Rows.Count > 0)
        {
            //Image new code
            //   
			imgLinkAnhavata.Src = table.Rows[0]["LinkAnh"].ToString().Trim();	
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["LinkAnh"].ToString().Trim()))	
            {	
                imgLinkAnhavata.Src = "/images/user/" + table.Rows[0]["LinkAnh"].ToString().Trim();	
            }
            txtTenCuaHang.Value = table.Rows[0]["TenCuaHang"].ToString().Trim();
            txtEmail.Value = table.Rows[0]["Email"].ToString().Trim();
            txtDiaChi.Value = table.Rows[0]["DiaChi"].ToString().Trim();
            txtSoDienThoai.Value = table.Rows[0]["SoDienThoai"].ToString().Trim();//Code new

            txtQuanHuyen.Value = table.Rows[0]["idHuyen"].ToString().Trim();
            txtTinhThanh.Value = table.Rows[0]["idTinh"].ToString().Trim();
            txtPhuongXa.Value = table.Rows[0]["idPhuongXa"].ToString().Trim();

            string DiaChi = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString().Trim()) + ", " + StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString().Trim()) + ", " + StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString().Trim());
            txtDiaDiem.Value = DiaChi;
            txtDiaDiem.Attributes.Add("title", DiaChi);

            if (txtSoDienThoai.Value.Trim() != "")
            {
                txtSoDienThoai.Attributes.Add("readonly", "readonly");
            }


            imgLinkAnh.Src = table.Rows[0]["HeadCMND"].ToString().Trim();
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["HeadCMND"].ToString().Trim()))
            {
                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["HeadCMND"].ToString().Trim();
            }
            imgLinkAnhA.Src = table.Rows[0]["FooterCMND"].ToString().Trim();
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["FooterCMND"].ToString().Trim()))
            {
                imgLinkAnhA.Src = "/images/user/" + table.Rows[0]["FooterCMND"].ToString().Trim();
            }
            txtCMND.Value = table.Rows[0]["SoCMND"].ToString().Trim();
           // txtEmail.Value = table.Rows[0]["Email"].ToString().Trim();
            //txtDiaChi.Value = table.Rows[0]["DiaChi"].ToString().Trim();
            //txtSoDienThoai.Value = table.Rows[0]["SoDienThoai"].ToString().Trim();//Code new
	    
	  //  if(txtSoDienThoai.Value.Trim() !=""){
			//	 txtSoDienThoai.Attributes.Add("readonly", "readonly");
			//}
            
            //txtMST.Value = table.Rows[0]["MST"].ToString().Trim();
            //DateTime ngayCap = DateTime.MinValue;
            //if (table.Rows[0]["NgayCap"].ToString().Trim() != "")
            //{
            //    ngayCap = (DateTime)table.Rows[0]["NgayCap"];
            //}
            //string NgayCap = (table.Rows[0]["NgayCap"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayCap.ToString("MM-dd-yyyy")));
            //if (NgayCap == "01/01/1900")
            //{
            //    txtNgayCap.Value = "";
            //}
            //else
            //{
            //    txtNgayCap.Value = StaticData.ConvertMMDDYYtoDDMMYY(ngayCap.ToString("MM-dd-yyyy"));
            //}
          //  txtNoiCap.Value = table.Rows[0]["NoiCap"].ToString().Trim();

           // txtQuanHuyen.Value = table.Rows[0]["idHuyen"].ToString().Trim();
          //  txtTinhThanh.Value = table.Rows[0]["idTinh"].ToString().Trim();
          //  txtPhuongXa.Value = table.Rows[0]["idPhuongXa"].ToString().Trim();
           
         //   string DiaChi = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString().Trim()) + ", " + StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString().Trim()) + ", " + StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString().Trim());
          //  txtDiaDiem.Value = DiaChi;
           // txtDiaDiem.Attributes.Add("title", DiaChi);
        }
    }
    private void LoadDiaDiem()
    {
        //string strSql = "select * from City";
        //ddlTinh.DataSource = Connect.GetTable(strSql);
        //ddlTinh.DataTextField = "Ten";
        //ddlTinh.DataValueField = "id";
        //ddlTinh.DataBind();
        //ddlTinh.Items.Add(new ListItem("Chọn tỉnh", "0"));
        //ddlTinh.Items.FindByText("Chọn tỉnh").Selected = true;

        //string sqlHuyen = "select * from District";
        //slHuyen.DataSource = Connect.GetTable(sqlHuyen);
        //slHuyen.DataTextField = "Ten";
        //slHuyen.DataValueField = "id";
        //slHuyen.DataBind();
        //slHuyen.Items.Add(new ListItem("Tất cả quận huyện", "0"));
        //slHuyen.Items.FindByText("Tất cả quận huyện").Selected = true;

        //string sqlXA= "select * from tb_PhuongXa";
        //slXa.DataSource = Connect.GetTable(sqlXA);
        //slXa.DataTextField = "Ten";
        //slXa.DataValueField = "id";
        //slXa.DataBind();
        //slXa.Items.Add(new ListItem("Tất cả phường xã", "0"));
        //slXa.Items.FindByText("Tất cả phường xã").Selected = true;

    }

    protected void btnDangKy_Click(object sender, EventArgs e)
    {
        string linkHinhAnh_OLD = StaticData.getField("tb_ThanhVien", "HeadCMND", "idThanhVien", idThanhVien);
        string linkHinhAnh_DB = "";

        string linkHinhAnhA_OLD = StaticData.getField("tb_ThanhVien", "FooterCMND", "idThanhVien", idThanhVien);
        string linkHinhAnhA_DB = "";
		
		string linkHinhAnhavata_OLD = StaticData.getField("tb_ThanhVien", "LinkAnh", "idThanhVien", idThanhVien);	
        string linkHinhAnhavata_DB = "";

        string HoTen = txtCMND.Value.Trim();
        string HoTenCuaHang = txtTenCuaHang.Value.Trim();
        string Email = txtEmail.Value.Trim();
        string DiaChi = txtDiaChi.Value.Trim();
        string TinhThanh = txtTinhThanh.Value.Trim();
        string QuanHuyen = txtQuanHuyen.Value.Trim();
        string PhuongXa = txtPhuongXa.Value.Trim();


       
        MessageHoTen.InnerText = "";
        if (HoTenCuaHang == "")
        {
            MessageHoTen.InnerText = "Vui lòng nhập Họ Tên!";
            return;
        }

        MessageEmail.InnerText = "";
        if(Email =="")
        {
            MessageEmail.InnerText = "Vui lòng nhập Email!";
            return;
        }

        MessageDiaChi.InnerText = "";
        if (DiaChi == "")
        {
            MessageDiaChi.InnerText = "Vui lòng nhập Địa chỉ!";
            return;
        }

        MessageDiaDiem.InnerText = "";
        if (TinhThanh == "" && QuanHuyen =="" && PhuongXa == "")
        {
            MessageDiaDiem.InnerText = "Vui lòng chọn Địa điểm!";
            return;
        }
        MessageCMND.InnerText = "";
        if (HoTen == "")
        {
            MessageCMND.InnerText = "Vui lòng nhập số CMND/CCCD!";
            return;
        }

        // string MST = txtMST.Value.Trim();
        //DateTime NgayCap = DateTime.Parse(txtNgayCap.Value.Trim());
        //  string NgayCap = txtNgayCap.Value.Trim();
        // DateTime NgayCap = txtNgayCap.Value = (table.Rows[0]["NgayCap"].ToString().Trim() == "" ? "Chưa cung cấp" : StaticData.ConvertMMDDYYtoDDMMYY(ngayCap.ToString("MM-dd-yyyy")));
        //DateTime ngayCap_date;
        //if (DateTime.TryParse(NgayCap, out ngayCap_date))
        //{

        //}
        //else
        //{
        //    NgayCap = "";
        //}
        // string NoiCap = txtNoiCap.Value.Trim();
        //  MessMST.InnerHtml = "";
        //MessageSoDienThoai.InnerHtml = "";

        //string sqlDT = "Select SoDienThoai,idThanhVien from tb_ThanhVien where idThanhVien !='"+idThanhVien+"'";
        //      DataTable tbDT = Connect.GetTable(sqlDT);
        //      if (tbDT.Rows.Count > 0)
        //      {
        //          for (int i = 0; i < tbDT.Rows.Count; i++)
        //          {
        //              if (SoDienThoai != "" && SoDienThoai == tbDT.Rows[i]["SoDienThoai"].ToString())
        //              {
        //                  MessageSoDienThoai.InnerHtml = "Số điện thoại đã được sử dụng. Xin vui lòng nhập số điện thoại khác!";
        //                  return;
        //              }
        //          }
        //      }

        //if (SoDienThoai != "")
        //{
        //    float n;
        //    bool rs = float.TryParse(SoDienThoai.Replace(",", ""), out n);
        //    if (!rs)
        //    {
        //        MessageSoDienThoai.InnerHtml = "Số điện thoại phải là số và độ dài bằng 10 số";
        //        return;
        //    }
        //}
        //if (MST != "")
        //{
        //    float n;
        //    bool rs = float.TryParse(MST.Replace(",", ""), out n);
        //    if (!rs)
        //    {
        //        MessMST.InnerHtml = "Mã số thuế phải là số";
        //        return;
        //    }
        //}
		string linkHinhAnhavata= "";	
        if (fuHinhDaiDienavata.HasFile && DaThayDoi_imgHinhAnhavata.Value == "1")	
        {	
            linkHinhAnhavata = StaticData.BoDauTiengViet(HoTen) + "_" + DateTime.Now.ToString("dddd-dd-MMM-yyyy-HH-mm-ss") + Path.GetExtension(fuHinhDaiDienavata.PostedFile.FileName);	
        }	
        else	
            linkHinhAnhavata = linkHinhAnhavata_OLD;	
		
        if (linkHinhAnhavata == "" && DaThayDoi_imgHinhAnhavata.Value == "1")	
            linkHinhAnhavata_DB = "NULL";	
        else	
            linkHinhAnhavata_DB = " N'" + linkHinhAnhavata + "'";

        string linkHinhAnh = "";
        if (fuHinhDaiDien.HasFile && DaThayDoi_imgHinhAnh.Value == "1")
        {
            linkHinhAnh = StaticData.BoDauTiengViet(HoTen) + "_" + DateTime.Now.ToString("dddd-dd-MMM-yyyy-HH-mm-ss") + Path.GetExtension(fuHinhDaiDien.PostedFile.FileName);
        }
        else
            linkHinhAnh = linkHinhAnh_OLD;

        if (linkHinhAnh == "" && DaThayDoi_imgHinhAnh.Value == "1")
            linkHinhAnh_DB = "NULL";
        else
            linkHinhAnh_DB = " N'" + linkHinhAnh + "'";

        //HeadCMND
        MessHeadCMND.InnerText = "";
        if (linkHinhAnh == "" && DaThayDoi_imgHinhAnh.Value == "0")
            {
            MessHeadCMND.InnerText = " Vui lòng Up ảnh CMND/CCCD Mặt trước";
            return;
        }

        string linkHinhAnhA = "";
        if (fuHinhDaiDienA.HasFile && DaThayDoi_imgHinhAnhA.Value == "1")
        {
            linkHinhAnhA = StaticData.BoDauTiengViet(HoTen) + "A_" + DateTime.Now.ToString("dddd-dd-MMM-yyyy-HH-mm-ss") + Path.GetExtension(fuHinhDaiDienA.PostedFile.FileName);
        }
        else
            linkHinhAnhA = linkHinhAnhA_OLD;

        if (linkHinhAnhA == "" && DaThayDoi_imgHinhAnhA.Value == "1")
            linkHinhAnhA_DB = "NULL";
        else
            linkHinhAnhA_DB = " N'" + linkHinhAnhA + "'";

        //FooterCND
        MessFooterdCMND.InnerText = "";
        if (linkHinhAnhA =="" && DaThayDoi_imgHinhAnhA.Value == "0")
        {
            MessFooterdCMND.InnerText = " Vui lòng Up ảnh CMND/CCCD Mặt sau";
            return;
        }
        MesDieuKhoan.InnerText = "";
        if (!this.chbxPrivacy.Checked)
        {
            MesDieuKhoan.InnerText = " Vui lòng chấp nhận điều khoản và chính sách!";
            return;
        }

            string sql = @" UPDATE tb_thanhvien 
                        SET
                            TenCuaHang = N'" + HoTenCuaHang + @"' ,
                            Email = N'" + Email + @"' ,
                            DiaChi = N'" + DiaChi + @"' ,
                            idTinh = N'" + TinhThanh + @"' ,
                            idHuyen = N'" + QuanHuyen + @"' ,
                            idPhuongXa = N'" + PhuongXa + @"' ,
                            SoCMND = N'" + HoTen + @"' ,";

						sql += "LinkAnh = " + linkHinhAnhavata_DB + @",";
                        sql += "HeadCMND = " + linkHinhAnh_DB + @",";
                        sql += "FooterCMND = " + linkHinhAnhA_DB + @",";
                        sql += "StsPro = N'0',";
                        sql += "isDuyet = N'0',";
                        sql += "LyDo_KhongDuyet = N'',";
                        sql += "idAdmin_Duyet = N'-1',";
                        sql += "ProAt ='" + DateTime.Now.ToString() + "'";
                        sql += "WHERE idThanhVien =" + idThanhVien + @"";
        if (Connect.Exec(sql))
        {
            //string value = "select * from tb_ThanhVien where LinkAnh like 'http%' and idThanhVien='"+idThanhVien+"'";
            //DataTable value = Connect.GetTable("select * from tb_ThanhVien where LinkAnh like 'http%' and idThanhVien=" + idThanhVien);
            //if (value.Rows.Count > 0)
            //{
            //    linkHinhAnh = linkHinhAnh_DB;
            //}
            //else
            //{
			try	
            {	
                if (linkHinhAnhavata_OLD.Trim() != "" && linkHinhAnhavata.Trim() != "" && DaThayDoi_imgHinhAnhavata.Value == "1")	
                {	
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))	
                    {	
                        File.Delete(Server.MapPath(linkHinhAnhavata_OLD).Replace("thong-tin\\", "Images\\User\\"));	
                        Bitmap bm = new Bitmap(fuHinhDaiDienavata.PostedFile.InputStream);	
                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY);	
                        bm.Save(Server.MapPath(linkHinhAnhavata).Replace("thong-tin\\", "Images\\User\\"));	
                    }	
                    else	
                    {	
                        File.Delete(Server.MapPath(linkHinhAnhavata_OLD).Replace("thong-tin\\", "Images\\User\\"));	
                        fuHinhDaiDienavata.SaveAs(Server.MapPath(linkHinhAnhavata).Replace("thong-tin\\", "Images\\User\\"));	
                    }	
                }	
                else if (linkHinhAnhavata_OLD.Trim() == "" && linkHinhAnhavata.Trim() != "" && DaThayDoi_imgHinhAnhavata.Value == "1")	
                {	
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))	
                    {	
                        File.Delete(Server.MapPath(linkHinhAnhavata_OLD).Replace("thong-tin\\", "Images\\User\\"));	
                        Bitmap bm = new Bitmap(fuHinhDaiDienavata.PostedFile.InputStream);	
                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY);	
                        bm.Save(Server.MapPath(linkHinhAnhavata).Replace("thong-tin\\", "Images\\User\\"));	
                    }	
                    else	
                        fuHinhDaiDienavata.SaveAs(Server.MapPath(linkHinhAnhavata).Replace("thong-tin\\", "Images\\User\\"));	
                }	
            }	
            catch { }
            try
            {
                if (linkHinhAnh_OLD.Trim() != "" && linkHinhAnh.Trim() != "" && DaThayDoi_imgHinhAnh.Value == "1")
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))
                    {
                        File.Delete(Server.MapPath(linkHinhAnh_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        Bitmap bm = new Bitmap(fuHinhDaiDien.PostedFile.InputStream);

                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY);
                        bm.Save(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));
                    }
                    else
                    {
                        File.Delete(Server.MapPath(linkHinhAnh_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        fuHinhDaiDien.SaveAs(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));
                    }
                }
                else if (linkHinhAnh_OLD.Trim() == "" && linkHinhAnh.Trim() != "" && DaThayDoi_imgHinhAnh.Value == "1")
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))
                    {
                        File.Delete(Server.MapPath(linkHinhAnh_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        Bitmap bm = new Bitmap(fuHinhDaiDien.PostedFile.InputStream);

                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY);
                        bm.Save(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));
                    }
                    else
                        fuHinhDaiDien.SaveAs(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));

                }
            }
            catch { }

            try
            {
                if (linkHinhAnhA_OLD.Trim() != "" && linkHinhAnhA.Trim() != "" && DaThayDoi_imgHinhAnhA.Value == "1")
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))
                    {
                        File.Delete(Server.MapPath(linkHinhAnhA_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        Bitmap bm = new Bitmap(fuHinhDaiDienA.PostedFile.InputStream);

                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY);
                        bm.Save(Server.MapPath(linkHinhAnhA).Replace("thong-tin\\", "Images\\User\\"));
                    }
                    else
                    {
                        File.Delete(Server.MapPath(linkHinhAnhA_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        fuHinhDaiDienA.SaveAs(Server.MapPath(linkHinhAnhA).Replace("thong-tin\\", "Images\\User\\"));
                    }
                }
                else if (linkHinhAnhA_OLD.Trim() == "" && linkHinhAnhA.Trim() != "" && DaThayDoi_imgHinhAnhA.Value == "1")
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))
                    {
                        File.Delete(Server.MapPath(linkHinhAnhA_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        Bitmap bm = new Bitmap(fuHinhDaiDienA.PostedFile.InputStream);

                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY);
                        bm.Save(Server.MapPath(linkHinhAnhA).Replace("thong-tin\\", "Images\\User\\"));
                    }
                    else
                        fuHinhDaiDienA.SaveAs(Server.MapPath(linkHinhAnhA).Replace("thong-tin\\", "Images\\User\\"));

                }
            }
             catch { }
            //updateMaTV();
            //updateMaTV();   
            string message = "Bạn đã đăng ký Thành viên Pro thành công! Chúng tôi sẽ xem xét phê duyệt và phản hồi trong 24h. Xin cảm ơn";
            string url = "/thong-tin-ca-nhan/ttcn";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "');";
            script += "window.location = '";
            script += url;
            script += "'; }";
            ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);
            
        }
       // Response.Redirect("/thong-tin-ca-nhan/ttcn");
    }
    //public void updateMaTV()
    //{
    //    string soDT = txtSoDienThoai.Value.Trim();
    //    string sql = "UPDATE tb_ThanhVien SET MaThanhVien = 'TV" + soDT + @"' WHERE idThanhVien = '" + idThanhVien + "'";
    //    Connect.Exec(sql);

    //}
}