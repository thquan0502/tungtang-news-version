﻿const frontFormatPrice = (price) => {
    if (typeof price !== "number") {
        return undefined;
    }
    return `${price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}đ`;
}

/**
 * 
 * @param {any} sDate DD/MM/YYYY
 */
const frontStringToDate = (sDate) => {
    const items = sDate.split("/");
    if (items.length !== 3) {
        return undefined;
    }
    const day = parseInt(items[0]);
    const month = parseInt(items[1]);
    const year = parseInt(items[2]);
    const date = new Date(year, month - 1, day, 0, 0, 0, 0);
    return date;
}

const frontDateDiff = (date1, date2) => {
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
}

// create post: submit form
function createPost_submitForm() {
    if (g_idTinDang) {
        $('#ContentPlaceHolder1_btDangTinFake').click();
    } else {
        $("#modalChoosePostPackage").modal();
    }
}

const frontFetchPostPackages = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: "/FrontAjax.aspx?Action=getPostPackages",
            data: {},
            success: function success(res) {
                resolve(res);
            },
            error: function error(err) {
                reject(err);
            }
        });
    });
};

// start of modal choose post package
(async function() {
    // type: NEW, UPGRADE, EXTEND
    const choosePostFee = async (type, idWrap) => {
        const modal = $("#" + idWrap);
        const selectPackage = $("#" + idWrap+" .select-package");
        const selectPeriod = $("#" + idWrap+" .select-period");
        const inputDateFrom = $("#" + idWrap +" .input-date-from");
        const inputDateTo = $("#" + idWrap +" .input-date-to");
        const periodWrap = $("#" + idWrap +" .period-wrap");
        const datesWrap = $("#" + idWrap +" .dates-wrap");
        const submitBtn = $("#" + idWrap +" .submit-btn");
        const packagesDataRes = await frontFetchPostPackages();

        let stateDateFrom = new Date();

        if (modal.length === 0 || selectPackage.length === 0 || selectPeriod.length === 0 || !packagesDataRes.isSuccess) {
            return;
        }

        if (type === "EXTEND") {
            selectPackage.attr("disabled", "disabled");
        }

        const packagesData = packagesDataRes.data;
        let datePicker = undefined;

        const setRangeDays = function(dayx, dayy) {
            if (!dayx || !dayy) {
                dayx = new Date();
                dayy = new Date();
            }
            dayx.setHours(0, 0, 0, 0);
            dayy.setHours(0, 0, 0, 0);
            inputDateFrom.datepicker('setDate', dayx);
            inputDateTo.datepicker('setDate', dayy);
        }

        const renderPackagesList = () => {
            let content = "";
            packagesData.forEach((pack) => {
                content += ` <option value="${pack.code}">${pack.name}</option>`;
            });
            selectPackage.html(content);
        }

        const renderPeriodsList = () => {
            const currentPackageValue = selectPackage.val();
            const pack = packagesData.find(element => element.code === currentPackageValue);
            if (!pack) {
                return;
            }
            selectPeriod.html(`
                <option value="pricePer1Days">1 ngày - ${frontFormatPrice(pack.pricePer1Days)}</option>
                <option value="pricePer7Days">7 ngày - ${frontFormatPrice(pack.pricePer7Days)}</option>
                <option value="pricePer14Days">14 ngày - ${frontFormatPrice(pack.pricePer14Days)}</option>
                <option value="pricePer30Days">30 ngày - ${frontFormatPrice(pack.pricePer30Days)}</option>
                <option value="pricePerOption">Tùy chỉnh - ${frontFormatPrice(pack.pricePer1Days)} / ngày</option>
            `);
        }

        const getDiffDays = () => {
            const dateFrom = frontStringToDate(inputDateFrom.val());
            const dateTo = frontStringToDate(inputDateTo.val());
            return frontDateDiff(dateFrom, dateTo);
        }

        const setOptionPriceText = () => {
            const currentPackageValue = selectPackage.val();
            const pack = packagesData.find(element => element.code === currentPackageValue);
            if (!pack) {
                return;
            }

            let days = 1;

            const periodValue = selectPeriod.val();
            if (periodValue === "pricePerOption") {
                days = getDiffDays();
            }

            const option = $("#" + idWrap +" option[value='pricePerOption']");
            if (option.length === 0) {
                return;
            }
            const price = pack.pricePer1Days * days;
            fnCheckBalanceAvailable();
            option.text(`Tùy chỉnh - ${frontFormatPrice(price)} / ${days} ngày`);
        }

        const initDatePicker = () => {
            datePicker = $("#" + idWrap+" .input-date-picker").datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            datePicker.on('changeDate', (e) => {
                setOptionPriceText();
            });
        }

        const renderRangeDays = () => {
            const value = selectPeriod.val();

            let datex = new Date(stateDateFrom.getTime());
            let datey = new Date(stateDateFrom.getTime());
            let days = 1;

            switch (value) {
                case 'pricePer1Days':
                    days = 1;
                    break;
                case 'pricePer7Days':
                    days = 7;
                    break;
                case 'pricePer14Days':
                    days = 14;
                    break;
                case 'pricePer30Days':
                    days = 30;
                    break;
                default:
                    days = 1;
                    break;
            }

            datey.setDate(datey.getDate() + days);
            setRangeDays(datex, datey);

            if (value === 'pricePerOption') {
                inputDateFrom.removeAttr("disabled");
                inputDateTo.removeAttr("disabled");
            } else {
                inputDateFrom.attr("disabled", "disabled");
                inputDateTo.attr("disabled", "disabled");
            }
        }

        const onChangePackage = () => {
            const value = selectPackage.val();
            if (value === "default_pack") {
                periodWrap.addClass("hide");
                datesWrap.addClass("hide");
            } else {
                periodWrap.removeClass("hide");
                datesWrap.removeClass("hide");
            }
        }

        const onSubmitClick = () => {
            let price = getTotalPrice();
            if (price > APP_BALANCE) {
                window.location.href = "/tai-khoan/thong-tin-vi";
                return false;
            }
            switch (type) {
                case "NEW":
                    $("#ContentPlaceHolder1_txtPostPackage").val(selectPackage.val());
                    $("#ContentPlaceHolder1_txtPostPeriod").val(selectPeriod.val());
                    $("#ContentPlaceHolder1_txtPostDays").val("" + getDiffDays());
                    // submit create
                    $('#ContentPlaceHolder1_btDangTinFake').click();
                    break;
                case "UPGRADE":
                    if (selectPackage.val() === "default_pack") {
                        break;
                    } else {
                        $("#ContentPlaceHolder1_txtUpgradePackage").val(selectPackage.val());
                        $("#ContentPlaceHolder1_txtUpgradePeriod").val(selectPeriod.val());
                        $("#ContentPlaceHolder1_txtUpgradeDays").val("" + getDiffDays());
                        $("#ContentPlaceHolder1_txtUpgradeRedirect").val(window.location.href);
                        $('#ContentPlaceHolder1_btnUpgradeSubmit').click();
                    }
                    break;
                case "EXTEND":
                    $("#ContentPlaceHolder1_txtExtendPeriod").val(selectPeriod.val());
                    $("#ContentPlaceHolder1_txtExtendDays").val("" + getDiffDays());
                    $("#ContentPlaceHolder1_txtExtendRedirect").val(window.location.href);
                    $('#ContentPlaceHolder1_btnExtendSubmit').click();
                    break;
                default:
                    break;
            }
            
        }

        const getTotalPrice = () => {
            const diff = getDiffDays();
            const pack_code = selectPackage.val();
            const period_code = selectPeriod.val();

            const pack = packagesDataRes.data.find((e => e.code === pack_code));
            let price = 0;
            if (period_code === 'pricePerOption') {
                let perPrice = pack['pricePer1Days'];
                price = perPrice * diff;
            } else {
                price = pack[period_code];
            }
            return price;
        }

        const fnCheckBalanceAvailable = () => {
            let price = getTotalPrice();
            if (price <= APP_BALANCE) {
                submitBtn.text("Hoàn thành");
            } else {
                submitBtn.text("Nạp thêm tiền");
            }
        }

        const runAtFirst = () => {
            renderPackagesList();
            selectPackage.on('change', () => { renderPeriodsList(); onChangePackage(); fnCheckBalanceAvailable(); });
            renderPeriodsList();
            initDatePicker();
            setRangeDays();
            selectPeriod.on('change', () => { renderRangeDays(); fnCheckBalanceAvailable(); });
            renderRangeDays();
            fnCheckBalanceAvailable();
            onChangePackage();
            submitBtn.on('click', () => { onSubmitClick(); });
        }
        runAtFirst();

        return {
            setStateDateFrom: (date) => {
                stateDateFrom = date;
            },
            renderRangeDays
        };
    }
    if (typeof CHOOSE_POST_FEE_CREATE_POST !== "undefined") {
        choosePostFee("NEW", "modalChoosePostPackage");
    }

    choosePostFee("UPGRADE", "modalUpgradePostPackage");
    $(".btn-upgrade-post").on("click", function() {
        const upgradeId = $(this).data("id");
        if (upgradeId) {
            $("#ContentPlaceHolder1_txtUpgradeId").val(upgradeId);
            $("#modalUpgradePostPackage").modal();
        }
    });

    const extendObj = await choosePostFee("EXTEND", "modalExtendPostPackage");
    $(".btn-extend-post").on("click", function () {
        const extendId = $(this).data("id");
        const extendCode = $(this).data("pack-id");
        const extendTimeEnd = $(this).data("time-end");
        if (extendId && extendCode && extendTimeEnd) {
            $("#ContentPlaceHolder1_txtExtendId").val(extendId);
            $("#ContentPlaceHolder1_txtExtendPackage").val(extendCode);
            $("#ContentPlaceHolder1_txtExtendTimeEnd").val(extendTimeEnd);
            $("#modalExtendPostPackage .select-package").val(extendCode).trigger("change");
            $("#modalExtendPostPackage").modal();
            extendObj.setStateDateFrom(new Date(extendTimeEnd));
            extendObj.renderRangeDays();
        }
    });
}) ();
// end of modal choose post package
