﻿

function OpenNavSide(OnOff) {
    if (OnOff.trim() == "show") {
        $("#btnNAV").attr("onclick", "OpenNavSide('hide')");
        $("#btnNAV").css("background-color", "#f3ab54");
        $("#td_NAV").css("width", "50%");
        $("#td_NAV").children(1).css("display", "block");
        $("#td_NAV").children(1).css("width", "100%");
        $(".black-cover-nav").css("display", "block");
        $("#dvTimKiem").css("display", "none");
    }
    else if (OnOff.trim() == "hide") {
        $("#btnNAV").attr("onclick", "OpenNavSide('show')");
        $("#btnNAV").css("background-color", "#eaa149");
        $("#td_NAV").css("width", "0%");
        $("#td_NAV").children(1).css("width", "0%");
        $("#td_NAV").children(1).css("display", "none");
        $(".black-cover-nav").css("display", "none");
        $("#dvTimKiem").css("display", "block");
    }
}

var menuBar_fixed = false; 

var heightOfPage = $(window).height();

$(window).scroll(function () { 
    var Height_Change = 20; 
    if (heightOfPage > 1020)
        Height_Change = 120;

    if ($(this).scrollTop() >= Height_Change) {
        if (menuBar_fixed == false) {
            $("header").css({ "position": "fixed", "width": "100%", "z-index": "2" });
            menuBar_fixed = true;
        }
    }
    else   {
        if (menuBar_fixed == true) {
            $("header").css({ "position": "relative", "width": "100%", "z-index": "0" });
            menuBar_fixed = false;
        }
    }
});


