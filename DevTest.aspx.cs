﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

public partial class Demo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //object phoneNumber = Session["TempPhoneNumber"];
        //if(phoneNumber == null)
        //{
        //    Session["TempPhoneNumber"] = "vien";
        //    Response.Write("is null");
        //}
        //else
        //{
        //    Response.Write((string)phoneNumber);
        //}
        test2();
    }

    private void test1()
    {
        DateTime x;
        if (DateTime.TryParseExact("2020-12-20", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out x))
        {
            Response.Write(x.ToString("dd/MM/yyyy HH:mm:ss:fff"));
        }
        if (x == DateTime.MinValue)
        {
            Response.Write(x.ToString("dd/MM/yyyy"));
        }
    }

    protected void test2()
    {
        DateTime x = DateTime.Now;
        DateTime y = x.AddDays(7);
        Response.Write(x.ToString("yyyy-MM-dd HH:mm:ss:fff") + " ....... " + y.ToString("yyyy-MM-dd HH:mm:ss:fff"));
    }
}