﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="PostPreview.aspx.cs" Inherits="TinDangChiTiet" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Css/Slide.css" rel="stylesheet" />
    <link href="../Css/Page/ChiTietTinDang.css" rel="stylesheet" />
    <link href="../Css/Page/TinDangChiTiet.css" rel="stylesheet" />

    <%--OwlCarousel--%>
    <script src="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.js"></script>
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.css" rel="stylesheet" />
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.theme.default.min.css" rel="stylesheet" />
		 <style>
			 *, *::before, *::after {
			box-sizing: border-box;
		}

		@keyframes rotate {
			100% {
				transform: rotate(1turn);
			}
		}

		.rainbow {
			position: relative;
			z-index: 0;
			margin-left:10px;
			width: 115px;
			height: 40px;
			border-radius: 25px;
			overflow: hidden;
			padding: 1.2rem;
			}
			.rainbow::before {
				content: '';
				position: absolute;
				z-index: -2;
				left: -61%;
				top: -78%;
				width: 225%;
				height: 348%;
				background-color: #ff9600;
				background-repeat: no-repeat;
				background-size: 50% 50%, 50% 50%;
				background-position: 0 0, 100% 0, 100% 100%, 0 100%;
				background-image: linear-gradient(#4cb050, #00CC00), linear-gradient(#ff9600, #ffba00), linear-gradient(#377af5, #377af5);
				animation: rotate 4s linear infinite;
			}

			.rainbow::after {
				content: '';
				position: absolute;
				z-index: -1;
				left: 3px;
				top: 3px;
				width: calc(100% - 6px);
				height: calc(100% - 6px);
				background: white;
				border-radius: 25px;
			}
			 .google-auto-placed{
				 display:none;
			 }
			  @media (min-width: 900px){
           .mvp-widget-feat2-side-ad iframe{
               margin-left: -77px;
           }
       }
			
			   .mvp-widget-feat2-side {
    margin-left: 20px;
    width: 320px;
}
       .mvp-widget-feat2-side-ad {
    line-height: 0;
    margin-bottom: 20px;
    text-align: center;
    width: 100%;
}
       .relative {
    position: relative;
}
       .left, .alignleft {
    float: left;
}
		._2lwN4F8oWK7ISQo377szov ul li {
            height: 150px;
        }
        .img-responsive {
            width: 100%;
            display: block;
            max-width: 100%;
            object-fit: cover;
            height: 100%;
        }
           .box-register-new {
        border: 1px dotted #e1e1e1;
        padding: 25px 15px 30px 15px;
        margin-bottom: 15px;
    }
            .text-center {
            text-align: center;
}
                .box-register-new .txt {
        margin: 0;
        color: #2d2d2d;
        font-size: 22px;
        padding: 0;
    }
                .box-register-new .list {
    display: block;
}
        @media (max-width: 480px) {
            .box-register-new ul {
                font-size: 16px;
            }
			#googleads{
				display:none;
			}
        }
            .box-register-new ul {
        display: inline-block;
        color: #0884ac;
        font-size: 17px;
        margin: 15px 0 20px 0;
    }
                .box-register-new ul li {
        border-left: 1px solid #ccc;
        padding: 0 7px;
        line-height: 14px;
    }
                @media (max-width: 480px){
                .box-register-new ul li {
                    float: none !important;
                    border: none;
                    margin-top: 10px;
                    display: inline-block;
                }

                }
                        .box-register-new ul li:first-child {
                            border-left: none;
        }
                        .box-register-new .register {
                            line-height: 54px;
                        }
                    @media (max-width: 480px) {
                        .register {
                            max-width: 100%;
                            padding: 0 15px;
                            font-size: 18px;
                        }
                    }
                    .register {
                        white-space: nowrap;
                        background-color: #4cb050;
                        font-size: 24px;
                        font-family: font-helveticaNeueBold;
                        line-height: 65px;
                        padding: 0px 25px;
                        border: none;
                        color: #fff;
                        cursor: pointer;
                        -webkit-transition: all 0.2s ease-in-out;
                        -moz-transition: all 0.2s ease-in-out;
                        -ms-transition: all 0.2s ease-in-out;
                        -o-transition: all 0.2s ease-in-out;
                        border-radius: 3px;
}
                    .register {
    font-family: font-helveticaNeueMedium,arial,sans-serif;
    font-size: 22px;
    padding: 0 45px;
}
			 .Gia{
                        color: #ec3222;
                        font-size: 20px;
                        font-weight: 600;
                    }
    </style>
	
	 <link href="/WowSlider/engine1/style.css" rel="stylesheet" />



<script src="https://sp.zalo.me/plugins/sdk.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <main class="App__Main-fcmPPF dlWmGF">
        <article style="margin-top: 63px;">
            <div class="container _1OmYs5J4aYBAERDzg-6W8a" style="padding-left: 10px; padding-right: 10px;">
                <div class="row hKeDdA_G7No3HepOClD8G">
					<div class="col-sm-12" id="googleads"  >
                      <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
									<!-- BannerNgang_920x90 -->
									<ins class="adsbygoogle"
										 style="display:inline-block;width:920px;height:90px"
										 data-ad-client="ca-pub-2838479428227173"
										 data-ad-slot="8241625289"></ins>
									<script>
										 (adsbygoogle = window.adsbygoogle || []).push({});
									</script>
                    </div>
                    <div class="col-sm-12">
                        <div id="dvLinkTitle" class="linktitle" runat="server"><%--<a href="/">Trang chủ</a> > <a href="#">Cho thuê</a> > <a href="#">Thuê bất động sản</a> > Thuê nhà nguyên căn--%></div>

                    </div>
                </div>
                <div class="row no-gutter-sm-down">
                    <div class="col-md-8" style="padding-bottom: 10px;margin-top:25px;">
                       <div class="center-block TpF18isdCA9nMCxPS07MV">
               	<div id="wowslider-container1" style="z-index:0;">
	<div class="ws_images" id="dvSlide" runat="server" ><ul>
		<%--<li><img src="WowSlider/data1/images/cc_2019hoc180008_02_640_wx.jpg" alt="Hinh1" title="Hinh1" id="wows1_0"/></li>
		<li><img src="WowSlider/data1/images/chonxechophuotthuhondawinnerxyamahaexcitersuzukiraider3.jpg" alt="Hinh2" title="Hinh2" id="wows1_1"/></li>
		<li><img src="WowSlider/data1/images/danhgiawinnerx2019mauxecontaytotnhungchuahoanhao02.jpg" alt="Hinh3" title="Hinh3" id="wows1_2"/></li>
		<li><img src="WowSlider/data1/images/extgall1.jpg" alt="image slider" title="Hinh3" id="wows1_3"/></li>
		<li><img src="WowSlider/data1/images/hondacivictypermugenconcept031024x681.jpg" alt="Hinh4" title="Hinh4" id="wows1_4"/></li>--%>
	</ul></div>
	<div class="ws_bullets"><div id="dvDotSlide" runat="server">
		<a href="#wows1_0" title="hinh1"><span>1</span></a>
		<a href="#wows1_1" title="hinh2"><span>2</span></a>
		<a href="#wows1_2" title="hinh3"><span>3</span></a>
		<a href="#wows1_3" title="hinh3"><span>4</span></a>
		<a href="#wows1_4" title="hinh4"><span>5</span></a>
	</div></div><div class="ws_script" style="position:absolute;left:-99%">
        <a href="http://wowslider.net">slider</a> by WOWSlider.com v8.8</div>
	<div class="ws_shadow"></div>
	</div>	


                        </div>
                        <div class="iltDBORq0FT_OjmouT7Ui">
                            <div class="ICOY-4hTuxlSCBsYzqBxM">
                                <h1 class="_22kG1zbJ4D-6IUEgKvoifC col-xs-12 AdItem__Title-fFamiu ekmzk" id="dvTitle" runat="server"></h1>
                            </div>
                            <div>
                                <div class="col-xs-12 adItemOfferWrapper ">
                                    <div class="AdItem__PriceWrapper-iSokgh cEWkgd">
                                        <p id="dvGia" runat="server"><%--<b>Giá:</b> 50.000 đ--%></p>

                                    </div>
                                </div>
								<div class="col-xs-12 adItemOfferWrapper"  style="margin-top: -12px;">
                                    <div id="dvNgayDang" runat="server">

                                    </div>
                                </div>
                                <div class="col-xs-12 adItemOfferWrapper rainbow">
                                    <p id="dvMaTinDang" class="color-yellow" runat="server" style="margin-bottom: 0; display: flex; align-items: center;height: 18px;"></p>
                                </div>
                            </div>

                            <p id="dvNoiDung" runat="server" class="col-xs-12 text-justify _1No0Ndy5xkVdszNItBaiuv" style="white-space: pre-line;"></p>

                            <div class="col-xs-12">
                                <h5 class="_3fyh_DVQsq_lOU3atjDEh6"><strong>Khu vực</strong></h5>
                                <div class="media margin-top-05">
                                    <div class="media-body media-middle">
                                        <div>
                                            <p id="dvLoaiThue" runat="server"><%--<b>Loại:</b> Cho thuê--%></p>
                                        </div>
                                    </div>
                                </div>
                            </div>                           
                        </div>
                    </div>
                    <div class="col-md-4 _1E45evYilkAmIgtle77ZQk">
                        <div>
                            <div></div>
                            <div class="" style="">
                                <div class="_3mb4oCiyoOiRy41U3DhZcs" style="height: 0;">
                                    <div class="styles__UserProfileInfoWrapper-fAdrVE jQEbod">
                                        <div class="" style="float: left; width: 100%;">
                                            <div class="img-thumbnail img-circle _3r1LxPzm9N9CZVK8hYBlze _38Q-ZJPwiUyqL82cU4XpN6 " style="float: left;">
                                                <p id="dvAnhDaiDien" runat="server" style="float: left; height: 115px; margin-right: 15px;"></p>

                                            </div>
                                            <div class="styles__NameBounder-fVrffo fqYgiV" style="display: inline-block; width: 80%;">
                                                <div class="styles__FlexDiv-gecxmb juHLCK">
                                                    <div class="styles__NameDiv-fClOhj dzrPXg">
                                                        <b id="dvHoTen" runat="server" style="font-size: 14px;"></b>
                                                    </div>
                                                    <a id="btnXemTrang" runat="server" class="styles__SecondaryButton-kJMwTe iluGAh styles__BaseBtn-bLaJmp kFdpfu">Xem trang</a>
                                                </div>
                                                <div class="styles__StatusOnlineDiv-EFyjL ijoFhp">
                                                    <p id="dvEmail" runat="server" style="display: none;"></p>
                                                    <p id="dvDiaChi" runat="server" style="font-size: 13px;"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="_1NPt6YwpCxTsRRk89MWcQe">
                                        <div class="_1qSLCB8OdSh-qqBU-8hsY9">
                                            <div class="btn-block _172F-2PuOv67wKHZRmWZv_" id="dvSoDienThoai" runat="server">
                                                <div class="sc-gisBJw ikcTlv">
                                                    <div class="sc-kjoXOD icTLCU">
                                                        <div class="sc-cHGsZl iTxwzU">
                                                            <span>
                                                                <img alt="phone" class="sc-TOsTZ bnAkpx" src="../images/icons/phone.png" /></span>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="_1qSLCB8OdSh-qqBU-8hsY9 _2yFnwriywXbN31B7bl3L4 text-muted small" style="position: relative;max-width: 100%">
											
											
											
                                            <div class="col-xs-12 margin-top-10">
                                                <div>
                                                    <div class="SafeTips__SafeTipsWrapper-gWgMGO guGpFY">
                                                        <div>
                                                            <img alt="safe tips" class="pull-left" width="100" src="https://st.chotot.com/storage/images/tips/1_mobile.png" />  
                                                            <div class="mb-0 SafeTips__TipText-iJoGDM gYDniF">Tránh đưa lại sản phẩm cho người bán sau khi đã trả tiền.</div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
											 <div class="col-12 margin-top-10">
                                                    <div class="box-register-new text-center">
                                                    <h4 class="txt">Đăng tin mua bán rao vặt miễn phí</h4>
                                                <div class="text-center list">
                                                    <ul class="clearfix" style="padding-left:0px;">
                                                        <li class="pull-left" style="list-style:none;">Với hơn <strong>1000+ </strong>
                                                             người dùng tin tưởng sử dụng
                                                        </li>
                                                       <%-- <li class="pull-left"  style="list-style:none;">
                                                            Chỉ từ: <strong>6.000đ</strong>/ ngày
                                                        </li>--%>
                                                    </ul>
                                                    </div>
                                                    <button class="register box-popup-register"  data-scrolltop="no">
                                                        <a  href="https://tungtang.com.vn/dang-ky/dk" target='_blank'  style="color:white;font-size:18px;">
                                                        <span class="icon-caret">
                                                            <i class="fa fa-external-link-square"></i>
                                                        </span>
                                                         Đăng ký ngay
                                                        </a>
                                                    </button>
                                                </div>
                                                  </div>
											
                                             <div class="col-12 margin-top-10" id="AnhTinDang" runat="server">
                                              </div>
											
											 <div class="mvp-widget-feat2-side left relative" style="margin-left: 33px;margin-top: 6px;max-width: 100%;" >
                                            <div class="col-12 mvp-widget-feat2-side-ad left relative" id="adsense" style="display:none;">
                                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
														<!-- BannerDoc_200x250 -->
														<ins class="adsbygoogle"
															 style="display:inline-block;width:200px;height:250px"
															 data-ad-client="ca-pub-2838479428227173"
															 data-ad-slot="8409146243"></ins>
														<script>
															 (adsbygoogle = window.adsbygoogle || []).push({});
														</script>
                                            </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                
				
        </article>
    </main>

     <script src="/WowSlider/engine1/wowslider.js"></script>
    <script src="/WowSlider/engine1/script.js"></script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        } 
        function CopyLinkURL() {
            var copyText = document.getElementById("ContentPlaceHolder1_myLinkCopy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $('#spSaoChepThanhCong').show();
            setTimeout(function () { $('#spSaoChepThanhCong').hide(); }, 3000);
        }

        function showSlides(n) {
            try
            {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) { slideIndex = 1 }
                if (n < 1) { slideIndex = slides.length }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
            catch(e)
            {

            }
        }
         var x=$(window).width();
         var owl = $('.owl-carousel');
         if (x < 500) {
             owl.owlCarousel({

                 items: 2,
                 loop: true,
                 margin: 10,
                 autoplay: true,
                 slideSpeed: 1000,

                 autoplayTimeout: 5000,
                 autoplayHoverPause: true,
                 
             });
         }
         else {
             owl.owlCarousel({

                 items: 4,
                 loop: true,
                 margin: 10,
                 autoplay: true,
                 autoplayTimeout: 9000,
                 autoplayHoverPause: true
             });
         }
        

    </script>
</asp:Content>

