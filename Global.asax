﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Web" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(RouteTable.Routes);

        /*string path = Server.MapPath("~") + "\\Files\\count.txt";
        if (!File.Exists(path))
            File.WriteAllText(path, "0");

        Application["Access"] = int.Parse(File.ReadAllText(path)); */
    }

    public static void RegisterRoutes(RouteCollection routes)
    {
        routes.Add("EmpOperation", new Route
        (
           "{Dept}/{action}",
                new EmployeeRouteHandler()
        )
        );
        //Quy định riêng: Dept là kkt_ID, Action là tiêu đề
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown
        /*string path = Server.MapPath("~") + "\\Files\\count.txt";

        File.WriteAllText(path, Application["Access"].ToString());*/
    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        /*string path = Server.MapPath("~") + "\\Files\\count.txt";
        // Code that runs when a new session is started
        if (Application["Online"] == null)
            Application["Online"] = 1;
        else
            Application["Online"] = (int)Application["Online"] + 1;

        Application["Access"] = (int)Application["Access"] + 1;
        File.WriteAllText(path, Application["Access"].ToString());*/
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        // Code that runs on application shutdown
        /*int i = (int)Application["Online"];
        if (i > 1)
            Application["Online"] = i - 1;

        string path = Server.MapPath("~") + "\\Files\\count.txt";
        File.WriteAllText(path, Application["Access"].ToString()); */
    }
       
</script>
