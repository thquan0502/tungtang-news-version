﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DangKy : System.Web.UI.Page
{
    string idThanhVien = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["TungTang_Login"] == null || Request.Cookies["TungTang_Login"].Value.Trim() == "")
        {
            Response.Redirect("/dang-nhap/dn");
        }
        else
        {
            idThanhVien = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        if (!IsPostBack)
        {
            //LoadDiaDiem();
            LoadThongTinNguoiDung();

        }
    }
    void LoadThongTinNguoiDung()
    {
        DataTable table = Connect.GetTable("select * from tb_ThanhVien where idThanhVien=" + idThanhVien);
        if (table.Rows.Count > 0)
        {
            txtTenCuaHang.Value = table.Rows[0]["TenCuaHang"].ToString().Trim();
            txtEmail.Value = table.Rows[0]["Email"].ToString().Trim();
            txtDiaChi.Value = table.Rows[0]["DiaChi"].ToString().Trim();
            txtSoDienThoai.Value = table.Rows[0]["TenDangNhap"].ToString().Trim();

            txtQuanHuyen.Value = table.Rows[0]["idHuyen"].ToString().Trim();
            txtTinhThanh.Value = table.Rows[0]["idTinh"].ToString().Trim();
            txtPhuongXa.Value = table.Rows[0]["idPhuongXa"].ToString().Trim();
            if (File.Exists(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim()))
                imgLinkAnh.Src = "/images/user/" + table.Rows[0]["linkAnh"].ToString().Trim();
            txtDiaDiem.Value = StaticData.getField("tb_PhuongXa", "Ten", "id", table.Rows[0]["idPhuongXa"].ToString().Trim()) + ", " + StaticData.getField("District", "Ten", "id", table.Rows[0]["idHuyen"].ToString().Trim()) + ", " + StaticData.getField("City", "Ten", "id", table.Rows[0]["idTinh"].ToString().Trim());

        }
    }
    private void LoadDiaDiem()
    {
        //string strSql = "select * from City";
        //ddlTinh.DataSource = Connect.GetTable(strSql);
        //ddlTinh.DataTextField = "Ten";
        //ddlTinh.DataValueField = "id";
        //ddlTinh.DataBind();
        //ddlTinh.Items.Add(new ListItem("Chọn tỉnh", "0"));
        //ddlTinh.Items.FindByText("Chọn tỉnh").Selected = true;

        //string sqlHuyen = "select * from District";
        //slHuyen.DataSource = Connect.GetTable(sqlHuyen);
        //slHuyen.DataTextField = "Ten";
        //slHuyen.DataValueField = "id";
        //slHuyen.DataBind();
        //slHuyen.Items.Add(new ListItem("Tất cả quận huyện", "0"));
        //slHuyen.Items.FindByText("Tất cả quận huyện").Selected = true;

        //string sqlXA= "select * from tb_PhuongXa";
        //slXa.DataSource = Connect.GetTable(sqlXA);
        //slXa.DataTextField = "Ten";
        //slXa.DataValueField = "id";
        //slXa.DataBind();
        //slXa.Items.Add(new ListItem("Tất cả phường xã", "0"));
        //slXa.Items.FindByText("Tất cả phường xã").Selected = true;

    }

    protected void btnDangKy_Click(object sender, EventArgs e)
    {
        string linkHinhAnh_OLD = StaticData.getField("tb_ThanhVien", "LinkAnh", "idThanhVien", idThanhVien);
        string linkHinhAnh_DB = "";

        string HoTen = txtTenCuaHang.Value.Trim();
        string Email = txtEmail.Value.Trim();
        string DiaChi = txtDiaChi.Value.Trim();
        string TinhThanh = txtTinhThanh.Value.Trim();
        string QuanHuyen = txtQuanHuyen.Value.Trim();
        string PhuongXa = txtPhuongXa.Value.Trim();

        string linkHinhAnh = "";
        if (fuHinhDaiDien.HasFile && DaThayDoi_imgHinhAnh.Value == "1")
        {
            linkHinhAnh = StaticData.BoDauTiengViet(HoTen) + "_" + DateTime.Now.ToString("dddd-dd-MMM-yyyy-HH-mm-ss") + Path.GetExtension(fuHinhDaiDien.PostedFile.FileName);
        }
        else
            linkHinhAnh = linkHinhAnh_OLD;

        if (linkHinhAnh == "" && DaThayDoi_imgHinhAnh.Value == "1")
            linkHinhAnh_DB = "NULL";
        else
            linkHinhAnh_DB = " N'" + linkHinhAnh + "'";

        string sql = @" update tb_thanhvien 
                        SET
                            TenCuaHang = N'" + HoTen + @"' ,
                            Email = '" + Email + @"' ,
                            DiaChi = N'" + DiaChi + @"' ,
                            idTinh = N'" + TinhThanh + @"' ,
                            idHuyen = N'" + QuanHuyen + @"' ,
                            idPhuongXa = N'" + PhuongXa + @"' , 
                            LinkAnh = " + linkHinhAnh_DB + @" 
                        WHERE idThanhVien =" + idThanhVien + @"and TenDangNhap='" + txtSoDienThoai.Value.Trim() + @"'";
        if (Connect.Exec(sql))
        {
            try
            {
                if (linkHinhAnh_OLD.Trim() != "" && linkHinhAnh.Trim() != "" && DaThayDoi_imgHinhAnh.Value == "1")
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))
                    {
                        File.Delete(Server.MapPath(linkHinhAnh_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        Bitmap bm = new Bitmap(fuHinhDaiDien.PostedFile.InputStream);

                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY); 
                        bm.Save(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));
                    }
                    else
                    {
                        File.Delete(Server.MapPath(linkHinhAnh_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        fuHinhDaiDien.SaveAs(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));
                    }
                }
                else if (linkHinhAnh_OLD.Trim() == "" && linkHinhAnh.Trim() != "" && DaThayDoi_imgHinhAnh.Value == "1")
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].Contains("iOS"))
                    {
                        File.Delete(Server.MapPath(linkHinhAnh_OLD).Replace("thong-tin\\", "Images\\User\\"));
                        Bitmap bm = new Bitmap(fuHinhDaiDien.PostedFile.InputStream);

                        bm.RotateFlip(RotateFlipType.Rotate270FlipXY); 
                        bm.Save(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));
                    }
                    else
                        fuHinhDaiDien.SaveAs(Server.MapPath(linkHinhAnh).Replace("thong-tin\\", "Images\\User\\"));

                }
            }
            catch { }

            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Lưu thông tin thành công')", true);
            Response.Redirect("/thong-tin-ca-nhan/ttcn");
        }
    }
}