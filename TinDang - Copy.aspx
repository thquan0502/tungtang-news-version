﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="TinDang - Copy.aspx.cs" Inherits="TinDang" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="../../asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="../../asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>

      <link href="../Css/Page/TinDang.css" rel="stylesheet" />        
    <link href="../Css/Slide.css" rel="stylesheet" />    
    <script src="../Js/Page/TinDang.min.js"></script>
        
    <link href="../Css/Slide.css" rel="stylesheet" />
    <link href="../Css/Page/ChiTietTinDang.css" rel="stylesheet" />
    <link href="../Css/Page/TinDangBlog.css" rel="stylesheet" />
	<link href="../Css/Page/TinDangChiTiet.css" rel="stylesheet" />

    <link href="WowSlider/engine1/style.css" rel="stylesheet" /> 
      <%--OwlCarousel--%>
    <script src="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.js"></script>
    <link href="/Plugins/OwlCarousel2-2.2.1/owl.carousel.min.css" rel="stylesheet" />
    <style>

        @media (min-width: 992px) {
            .category-main-title {
                width: 992px;
                margin-left: auto;
                margin-right: auto;
            }
        }
        @media (max-width: 991px) {
            .category-main-title {
                padding-left: 10px;
            }
        }
        .category-main-title {
            font-weight: bold;
            color: #555;
            padding-top: 32px;
            font-size: 18px;
            margin: 0;
        }

        @media (min-width: 992px) {
            .category-main-container {
                width: 992px;
                margin-left: auto;
                margin-right: auto;
            }
        }

        @media (max-width: 991px) {
            .category-main-container {
                padding-left: 10px;
            }
        }
            .category-main-container {
                padding-top: 12px;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
            }
        @media (min-width: 992px) {
            .category-main__item {
                width: 20%;
            }
        }
        @media (max-width: 991px) {
            .category-main__item {
                width: 50%;
            }
        }
            .category-main__item {
                display: flex;
                line-height: 32px;
                color: #666;
                margin-bottom: 10px;
            }

            .category-main__item-icon {
                height: 32px;
                width: 32px;
                margin-right: 7px;
            }

         .MoTaHomePage a{
      color: #ffba00;
  }
		 @media (min-width: 1100px){
           #googleads iframe{
               margin-left: -18px;
           }
		  .list{
                top: 4px;
            right: 10px;
           }    
       }
		 .mvp-widget-feat2-side-ad iframe{
			 margin-left: -43px;
    		margin-top: 6px;
		}
		        @media (min-width: 920px){
         .google-auto-placed{
                 display:none;
             }
			.list{
              top: 4px;
            right: 10px;
         }
				
        }
		  @media (max-width: 920px){
			 #googleads{
                 display:none;
             }
			  	.list{
                 top: 4px;
                 right: 5px;
             }
		}
		 .list{
            background-image: url(/images/uutien1.png);
            position: absolute;
           
            display: block;
            width: 40px;
            height: 41px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
            text-align: center;
            line-height: 33px;
            font-size: 12px;
            font-weight:500;
            color: white;
            z-index: 3;
        }
		  .mvp-widget-feat2-side {
    margin-left: 20px;
    width: 320px;
}
       .mvp-widget-feat2-side-ad {
    line-height: 0;
    margin-bottom: 20px;
    text-align: center;
    width: 100%;
}
       .relative {
    position: relative;
}
       .left, .alignleft {
    float: left;
}
        .form-control {
            border: 0.5px solid #ddd;
            padding-right: 20px;
        }

        #vnw-home .top-job ul li {
            margin: 3px 0;
        }

        .jDyZht a.first {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .ctStickyAdsListing .ctAdListingWrapper .ctAdListingLowerBody .adItemPostedTime span {
            opacity: 0.5;
        }
		 .ckbGia{
            display:inline-block;
        }
    </style>
	
	<style>
           .box-register-new {
        border: 1px dotted #e1e1e1;
        padding: 25px 15px 30px 15px;
        margin-bottom: 15px;
    }
            .text-center {
            text-align: center;
}
                .box-register-new .txt {
        margin: 0;
        color: #2d2d2d;
        font-size: 18px;
        padding: 0;
    }
                .box-register-new .list {
    display: block;
}
        @media (max-width: 480px) {
            .box-register-new ul {
                font-size: 16px;
            }
        }
            .box-register-new ul {
        display: inline-block;
        color: #0884ac;
        font-size: 17px;
        margin: 15px 0 20px 0;
    }
                .box-register-new ul li {
        border-left: 1px solid #ccc;
        padding: 0 7px;
        line-height: 14px;
    }
                @media (max-width: 480px){
                .box-register-new ul li {
                    float: none !important;
                    border: none;
                    margin-top: 10px;
                    display: inline-block;
                }

                }
                        .box-register-new ul li:first-child {
                            border-left: none;
        }
                        .box-register-new .register {
                            line-height: 54px;
                        }
                    @media (max-width: 480px) {
                        .register {
                            max-width: 100%;
                            padding: 0 15px;
                            font-size: 18px;
                        }
                    }
                    .register {
                        white-space: nowrap;
                        background-color: #4cb050;
                        font-size: 24px;
                        font-family: font-helveticaNeueBold;
                        line-height: 65px;
                        padding: 0px 25px;
                        border: none;
                        color: #fff;
                        cursor: pointer;
                        -webkit-transition: all 0.2s ease-in-out;
                        -moz-transition: all 0.2s ease-in-out;
                        -ms-transition: all 0.2s ease-in-out;
                        -o-transition: all 0.2s ease-in-out;
                        border-radius: 3px;
}
                    .register {
    font-family: font-helveticaNeueMedium,arial,sans-serif;
    font-size: 22px;
    padding: 0 45px;
}
		     #checkbox-container{
          margin: 10px 5px;
        }

        #checkbox-container div{
          margin-bottom: 0px;
        }

        #checkbox-container button{
          margin-top: 5px;
        }
    </style>
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	
    <div class="modal-DiaDiem-cls">
        <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
            <div id="dvDSDiaDiem" style="overflow: auto;">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbDiaDiem_tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal-DiaDiem-cls">
        <div id="modalDanhMuc" class="modal" style="overflow: hidden;">
            <div id="dvDSDanhMuc" style="overflow: auto;">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                <div style="text-align: center; font-size: 17px;">Chọn danh mục</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tbDanhMuc_tbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal-DiaDiem-cls">
        <div id="modalBoLoc" class="modal" style="overflow: hidden;">
            <div id="dvBoLoc" style="overflow: auto;">
                <table class="table table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <div style="text-align: center; font-size: 17px;">Lọc kết quả</div>
                            </th>
                        </tr>
                    </thead>
                </table>
                <br />
                <div class="col-sm-6">
                    <label style="display: none;">Giá từ <span id="spGiaTu" runat="server">0</span></label>
                    <input type="range" id="rgGiaTu" value="0" max="10000000" step="0" oninput="GiaTu(this.value)" runat="server" style="display: none;"/>
                </div>
                <div class="col-sm-6">
                    <label style="display: none;">Đến <span id="spGiaDen" runat="server">10000000</span></label>
                    <input type="range" id="rgMaxGiaDen" runat="server" style="display: none;" />
                    <input type="range" id="rgGiaDen" value="0"  max="10000000" step="0" oninput="GiaDen(this.value)" runat="server" style="display: none;" />
                </div>
                <label>Tầm giá</label>
                <div class="col-sm-12">
                    <div id="checkbox-container">                                      
                     <div>
                    <label class="col-12" style="font-weight:bold"><input type="checkbox" id="chonrulerhh" name="checkhh" runat="server" value="Chọn"/> Chọn Range lọc</label><input type="text" id="filterPriceRange"/>
                     <%--<a id="btnThemLuotXem" runat="server" class="btn btn-success btn-flat pd" style="margin-top:5px" onclick="thaydoi()">Thêm Lọc</a><br />--%>   
                    <br />                 
                    <a id="textHH" style="color:blue;"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thêm bộ lọc <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                    <a id="textHHAN" style="color:blue;display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ẩn <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                     <p style="margin-top:10px "></p>                    
                    <div id="BoLocHH" style="display:none;">            
                        <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="check" value="Từ 10 đến 100" runat="server" id="ckb10100" />
                        <label for="ContentPlaceHolder1_ckb10100"><i class="fa fa-dollar"></i> Từ 10 - 100 triệu</label>
                        </div>  
                        <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="check" value="Từ 100 đến 500" runat="server" id="ckb100500" />
                        <label for="ContentPlaceHolder1_ckb100500"><i class="fa fa-dollar"></i> Từ 100 - 500 triệu</label>
                        </div> 
                        <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="check" value="Từ 500 trở lên" runat="server" id="ckb500" />
                        <label for="ContentPlaceHolder1_ckb500"><i class="fa fa-dollar"></i> 500 triệu trở lên</label>
                        </div>   
                        </div>
                         </div>                             
                        <%--<div class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb10100hh" name="checkhh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu</div>
                        <div class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb100500hh" name="checkhh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu</div>
                        <div class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb500hh" name="checkhh" value="Từ 500 trở lên" /> 500 triệu trở lên</div>       --%>                                                                             
                    </div>
                    <asp:Label ID="listLabel" runat="server" Visible="false"/> 
                    <asp:Label ID="ruler" runat="server" Visible="false" />
                                                                
                    <script>
                        //function thaydoi(){
                        //    $("#loc").css("display", "");
                        //    $("#gom").css("display", "");
                        //}
                        document.getElementById('textHH').onclick = function () {
                            document.getElementById("BoLocHH").style.display = "";
                            document.getElementById("textHH").style.display = "none";
                            document.getElementById("textHHAN").style.display = "";
                        }
                        document.getElementById("textHHAN").onclick = function () {
                            document.getElementById("textHH").style.display = "";
                            document.getElementById("BoLocHH").style.display = "none";
                            document.getElementById("textHHAN").style.display = "none";
                        };
                    </script>
                </div>
                <br />
                <label>Sắp xếp theo</label>
                <div class="DanhMucBoLoc">
                    <div class="styled-input-single radio">
                        <input type="radio" name="fieldset-3" id="radTinMoiTruoc" runat="server" />
                        <label for="ContentPlaceHolder1_radTinMoiTruoc"><i class="fa fa-clock-o"></i>Tin mới trước</label>
                    </div>
                    <div class="styled-input-single radio">
                        <input type="radio" name="fieldset-3" id="radGiaThapTruoc" runat="server" />
                        <label for="ContentPlaceHolder1_radGiaThapTruoc"><i class="fa fa-dollar"></i>Giá thấp trước</label>
                    </div>
                </div>
                <br />
                <label>Bạn cần</label>
                <div class="DanhMucBoLoc">
                    <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="fieldset-5" id="ckbCanMua" runat="server" checked />
                        <label for="ContentPlaceHolder1_ckbCanMua"><i class="fa fa-cart-arrow-down"></i>Mua</label>
                    </div>
                    <div class="styled-input-single radio styled-input--square">
                        <input type="checkbox" name="fieldset-5" id="ckbCanBan" runat="server" checked />
                        <label for="ContentPlaceHolder1_ckbCanBan"><i class="fa fa-archive"></i>Bán</label>
                    </div>
                </div>

                <div class="col-sm-12">
                    <a class="btn btn-primary btn-search-all" style="width: 100%; margin: 10px 0;" onclick="btnBoLoc_click();">ÁP DỤNG</a>
                </div>
            </div>
        </div>
    </div>

    <main class="App__Main-fcmPPF dlWmGF">
        <div class="XauAgRup4nyZifS5XeIT8 container" style="padding-right:0px;padding-left:0px;">
            <div class="row v68hxsOjZ91PAUhcMxbPI" style="margin-left: unset; margin-right: unset;">
				
                <div class="col-xs-12 _13_3ohNSwkOXu5Kk_Rwn4F">
                    <header>
						<div class="col-sm-12" id="googleads" style="margin-top:5px;"  >
                      <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
									<!-- BannerNgang_920x90 -->
									<ins class="adsbygoogle"
										 style="display:inline-block;width:920px;height:90px"
										 data-ad-client="ca-pub-2838479428227173"
										 data-ad-slot="8241625289"></ins>
									<script>
										 (adsbygoogle = window.adsbygoogle || []).push({});
									</script>
                    </div>
                        <div class="gEQeZ-pfX6E9qs-T0zFQG">
                            <ol class="breadcrumb _3WIL_EScB1tm02Oqj0vbu8" id="dvLinkTitle2" runat="server">
                                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu"><a href="/"><span>Trang chủ</span></a></li>
                                <li class="breadcrumb-item Z9Bevb8OoGSgkMEl67zPu">
                                    <span itemprop="item"><strong itemprop="name">Tin đăng</strong></span>
                                </li>
                            </ol>
                        </div>
                        <div>
                            <h1 itemprop="name" class="omc8MxSN2eDjZGw_fyTkB _3AWJOhn6FPpAfbe5MWEDQp">Mua bán, rao vặt nhanh chóng tại Tung tăng</h1>
                        </div>
                    </header>
                    <main>
                        <div class="row no-margin">
                            <div class="col-md-8 _2kTkY3Nmr5OemSQ8lNaNal">
                                <div>
                                    <div>
                                        <div class="SearchSuggestion__SearchSuggestionWrapper-bWXfvU gWoVfQ">
                                            <div>
                                                <div class="SearchSuggestion__Div-hoVDnM WjNjN">
                                                    <div class="InputItem__InputWrapper-bYnTYy hrSofa">
                                                        <img src="../images/icons/searchActive.svg" alt="Tìm kiếm" style="position: absolute; left: 5px; bottom: 18%; height: 20px; width: 25px;" />
                                                        <input type="text" class="InputItem__TextField-dHKBVS daymOP" placeholder="Tìm kiếm trên Tung tăng..." value="" id="txtKeySearch" runat="server" onkeypress="EnterPress(event);" />
                                                        <asp:LinkButton ID="btnTimKiem" runat="server" class="btn btn-primary btn-search-all" OnClick="btnTimKiem_Click" Style="display: none;">TK</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row _3pPkNjKR8UOHx7qTOxuDb0">
                                    <div class="col-xs-12 undefined" style="margin-top:5px;">
                                        <div class="row">
                                            <div class="UKoJK4TUh1CFDy77SUIO2 col-xs-12">
                                                <select id="ddlTinh" runat="server" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                                <input type="text" id="txtBoLocThem" value="" runat="server" style="display: none;" />
                                                <input type="text" id="txtTinhThanh" name="name" value="0" runat="server" style="display: none;" />
                                                <select id="slHuyen" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                                <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" style="display: none;" />
                                                <select id="slXa" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                                <input type="text" id="txtPhuongXa" name="name" value="" runat="server" style="display: none;" />
                                                <input id="ddlLoaiDanhMuc" runat="server" style="padding-left: 0; padding-right: 0; display: none;" />
                                                <input id="ddlLoaiDanhMucCap2" runat="server" style="padding-left: 0; padding-right: 0; display: none;" />
                                                <span>
                                                    <a class="btn btn-default btn-lg col-xs-5 _1E0IIm3NmYiLEQdOsQrn_Q" style="padding: 6px 16px; font-size: 1em;width:31%;text-align:center;" id="txtDiaDiem" runat="server" onclick="ShowDiaDiem();">
                                                        <span itemprop="name"><i class="fa fa-globe" aria-hidden="true"></i> Toàn quốc
                                                        </span><span class="caret _2UAALXHJVap4y2kAfwkH6_"></span>
                                                    </a>
                                                </span>
                                                <span>
                                                    <a class="btn btn-default btn-lg col-xs-5 _1E0IIm3NmYiLEQdOsQrn_Q" style="padding: 6px 16px; font-size: 1em;margin-left:3px;text-align:center;" onclick="ShowDanhMuc();" id="txtLoaiDanhMuc" runat="server">
                                                        <span itemprop="name"> <i class="fa fa-cart-plus" aria-hidden="true"></i> Danh mục
                                                        </span><span class="caret _2UAALXHJVap4y2kAfwkH6_"></span>
                                                    </a>
                                                </span>
                                                <div class="_3_wJ4COG8uo7lCz9rNJfiv">
                                                    <a class="btn btn-default btn-lg col-xs-2 _1E0IIm3NmYiLEQdOsQrn_Q" style="padding: 6px 16px; font-size: 1em; border-radius: 10px; border: 1px solid #cacaca;width:25%;margin-left:3px;text-align:center;" onclick="ShowBoLoc();" id="txtLocThem" runat="server">
                                                        <span> <i class="fa fa-filter" aria-hidden="true"></i> Lọc
                                                        </span><span class="caret _2UAALXHJVap4y2kAfwkH6_"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <ul class="sc-chPdSV UEYyu" width="100%" style="border-top: none; margin: 0px 0px 1px; padding-bottom: 5px; display: none;"
                                        id="dvDanhMucCap2" runat="server">
                                    </ul>
                                </div>
								 <div class="pgjTolSNq4-L--tpQ7Xp5" style="margin-bottom: 19px;"> 
                                    <strong style="margin-left:5px;color: #ffba00;font-style: italic;font-size: 18px;">Tin ưu tiên </strong>
                              <div class="owl-carousel" id="abc" runat="server" style="z-index: 0; overflow: auto;height: auto;"></div> 
                                    </div>
                                 	                          
   
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }
        function CopyLinkURL() {
            var copyText = document.getElementById("ContentPlaceHolder1_myLinkCopy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $('#spSaoChepThanhCong').show();
            setTimeout(function () { $('#spSaoChepThanhCong').hide(); }, 3000);
        }

        function showSlides(n) {
            try {
                var i;
                var slides = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("dot");
                if (n > slides.length) { slideIndex = 1 }
                if (n < 1) { slideIndex = slides.length }
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex - 1].style.display = "block";
                dots[slideIndex - 1].className += " active";
            }
            catch (e) {

            }
        }
        var x = $(window).width();
        var owl = $('.owl-carousel');
        if (x < 500) {
            owl.owlCarousel({

                items: 2,
                loop: true,
                speed: 800,
                margin: 10,
                autoplay: true,
                autoplaySpeed: 2000,

                autoplayTimeout: 5000,
                autoplayHoverPause: true,

            });
        }
        else {
            owl.owlCarousel({

                items: 4,
                loop: true,
                speed: 800,
                margin: 10,
                autoplay: true,
                autoplaySpeed: 2000,
                autoplayTimeout: 5000,
                autoplayHoverPause: true
            });
        }

        var $owl = $('.owl-carousel');
        var autoplayDelay = 5000;
        if (autoplayDelay) {
            $owl.trigger('stop.owl.autoplay');
            setTimeout(function () {
                $owl.trigger('play.owl.autoplay');
            }, autoplayDelay);
        }


    </script>
                                <div class="pgjTolSNq4-L--tpQ7Xp5" style="margin-top: 13px;">
									
                                    <div>
                                        <div class="ctStickyAdsListing" id="dvTinDang" runat="server">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="sc-bRBYWo sjho">
                                        <ul class="sc-hzDkRC bmmuID" id="ulTrang" runat="server">
                                        </ul>
                                    </div>
                                </div>

                       

                            </div>
                            <div class="col-md-4">
								<div class="col-12 margin-top-10">
                                                    <div class="box-register-new text-center">
                                                    <h4 class="txt">Đăng tin mua bán rao vặt miễn phí</h4>
                                                <div class="text-center">
                                                    <ul class="clearfix" style="padding-left:0px;">
                                                        <li class="pull-left" style="list-style:none;">Với hơn <strong>1000+ </strong>
                                                             người dùng tin tưởng sử dụng
                                                        </li>
                                                       <%-- <li class="pull-left"  style="list-style:none;">
                                                            Chỉ từ: <strong>6.000đ</strong>/ ngày
                                                        </li>--%>
                                                    </ul>
                                                    </div>
                                                    <button class="register box-popup-register"  data-scrolltop="no">
                                                        <a href="https://tungtang.com.vn/dang-ky/dk" target='_blank' style="color:white;font-size:18px;">
                                                        <span class="icon-caret">
                                                            <i class="fa fa-external-link-square"></i>
                                                        </span>
                                                         Đăng ký ngay
                                                        </a>
                                                    </button>
                                                </div>
                                                  </div>
								
                                <div class="col-12 margin-top-10" id="AnhTinDang" runat="server">
                                              </div>
								 <div class="mvp-widget-feat2-side left relative">
                                            <div class="col-12 mvp-widget-feat2-side-ad left relative" id="adsense" style="">
                                              <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
														<!-- BannerDoc_260x750 -->
														<ins class="adsbygoogle"
															 style="display:inline-block;width:260px;height:750px"
															 data-ad-client="ca-pub-2838479428227173"
															 data-ad-slot="4106847261"></ins>
														<script>
															 (adsbygoogle = window.adsbygoogle || []).push({});
														</script>

                                            </div>
                                                </div>
								
								<div class="mvp-widget-feat2-side left relative">
                                            <div class="col-12 mvp-widget-feat2-side-ad left relative" id="adsense" style="">
                                              <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
														<!-- BannerDoc_260x750 -->
														<ins class="adsbygoogle"
															 style="display:inline-block;width:260px;height:750px"
															 data-ad-client="ca-pub-2838479428227173"
															 data-ad-slot="4106847261"></ins>
														<script>
															 (adsbygoogle = window.adsbygoogle || []).push({});
														</script>

                                            </div>
                                                </div>
								
                            </div>
                                       <%--  div mô tả homepage--%>
                         <div class="col-md-12" >
                             <div class="MoTaHomePage" runat="server" id="Motahomepage">

                             </div>
                             </div>


                    <h4 class="category-main-title">Tin rao vặt - TPHCM</h4>
                            <div class="category-main-container">
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/bat-dong-san">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.735 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.472c0 2.895 2.369 5.264 5.264 5.264h21.471c2.896 0 5.265-2.368 5.265-5.264v-21.472c0-2.896-2.369-5.264-5.264-5.264zM28.552 14.618c-0.174 0.393-1.356 0.286-1.91 0.286h-0.325v8.337c0 2.424-1.983 4.407-4.406 4.407h-3.623v-8.051h-4.575v8.051h-3.623c-2.424 0-4.407-1.983-4.407-4.407v-8.338h-0.325c-0.553 0-1.736 0.107-1.909-0.286-0.175-0.393 0.054-0.692 0.667-1.206l9.084-7.612c1.104-0.925 1.68-1.45 2.8-1.45s1.697 0.525 2.8 1.45l3.005 2.517v-1.595c0-0.495 0.405-0.901 0.901-0.901h1.584c0.495 0 0.901 0.405 0.901 0.901v4.433l2.694 2.257c0.611 0.514 0.84 0.813 0.666 1.206z"></path>
                                </svg>
                                Bất động sản
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/o-to-xe-may">
                                  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.337 16.477c0 0.968-0.785 1.753-1.753 1.753s-1.753-0.785-1.753-1.753c0-0.968 0.785-1.753 1.753-1.753s1.753 0.785 1.753 1.753zM9.168 16.477c0 0.968-0.785 1.753-1.753 1.753s-1.753-0.785-1.753-1.753c0-0.968 0.785-1.753 1.753-1.753s1.753 0.785 1.753 1.753zM22.614 8.702c-0.304-0.808-0.454-1.204-1.447-1.204h-10.335c-0.992 0-1.142 0.396-1.446 1.204l-1.534 4.067h16.296l-1.535-4.067zM26.735 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.472c0 2.895 2.369 5.264 5.264 5.264h21.471c2.896 0 5.265-2.368 5.265-5.264v-21.472c0-2.896-2.369-5.264-5.264-5.264zM28.445 22.333h-2.017v2.247c0 1.015-0.829 1.843-1.844 1.843-1.014 0-1.843-0.829-1.843-1.843v-2.247h-13.482v2.247c0 1.015-0.829 1.843-1.843 1.843s-1.844-0.829-1.844-1.843v-2.247h-2.016v-6.971c0-1.296 0.963-2.377 2.208-2.563l2.094-5.36c0.55-1.409 1.424-1.861 3.229-1.861h9.824c1.806 0 2.679 0.452 3.229 1.861l2.094 5.36c1.245 0.187 2.209 1.268 2.209 2.563v6.971z"></path>
                                </svg>
                                Ô Tô - Xe Máy
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/dien-tu-phu-kien">
                                     <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M11.751 14.177c1.513 0 2.817-0.89 3.418-2.175h-1.267c-0.488 0.656-1.27 1.082-2.152 1.082-1.48 0-2.68-1.2-2.68-2.68s1.2-2.679 2.68-2.679c0.882 0 1.664 0.426 2.152 1.083h1.267c-0.601-1.285-1.905-2.175-3.418-2.175-2.084 0-3.772 1.689-3.772 3.772s1.689 3.773 3.772 3.773zM12.232 4.228c0 0.201-0.163 0.364-0.364 0.364s-0.364-0.163-0.364-0.364c0-0.201 0.163-0.364 0.364-0.364s0.364 0.163 0.364 0.364zM15.55 4.144c0 0.438-0.355 0.793-0.793 0.793s-0.793-0.355-0.793-0.793c0-0.438 0.355-0.793 0.793-0.793s0.793 0.355 0.793 0.793zM14.152 10.403c0 0.414-0.055 0.812-0.155 1.179h1.742c0.11-0.374 0.17-0.77 0.17-1.179s-0.060-0.805-0.17-1.179h-1.742c0.1 0.368 0.155 0.765 0.155 1.179zM8.78 4.228c0 0.201-0.163 0.364-0.364 0.364s-0.364-0.163-0.364-0.364c0-0.201 0.163-0.364 0.364-0.364s0.364 0.163 0.364 0.364zM9.931 4.228c0 0.201-0.163 0.364-0.364 0.364s-0.364-0.163-0.364-0.364c0-0.201 0.163-0.364 0.364-0.364s0.364 0.163 0.364 0.364zM11.081 4.228c0 0.201-0.163 0.364-0.364 0.364s-0.364-0.163-0.364-0.364c0-0.201 0.163-0.364 0.364-0.364s0.364 0.163 0.364 0.364zM26.736 0h-21.472c-2.897 0-5.264 2.368-5.264 5.263v21.472c0 2.895 2.369 5.264 5.264 5.264h21.472c2.895 0.001 5.264-2.369 5.264-5.264v-21.473c0-2.896-2.369-5.263-5.264-5.263zM19.158 5.090c0.395-0.589 1.196-0.817 1.856-0.58 0.875 0.316 1.75 0.631 2.624 0.949 1.437 0.523 2.476 1.232 2.659 2.835l0.691 6.037h1.373c0.105 0 0.192 0.086 0.192 0.193v0.482c0 0.106-0.087 0.193-0.192 0.193h-3.084c-0.106 0-0.194-0.086-0.194-0.193v-0.482c0-0.106 0.087-0.193 0.194-0.193h0.715l-0.576-5.601c-0.070-0.714-0.323-1.301-0.805-1.694-0.294-0.24-0.619-0.377-0.906-0.521l-2.869-1.023c-0.43-0.154-0.765-0.11-0.907 0.252-0.158 0.404 0.035 0.726 0.52 0.907 0.858 0.321 1.727 0.612 2.583 0.939 0.489 0.187 0.921 0.58 1.007 1.090 0.091 0.533-0.012 0.979-0.2 1.309l-0.823-0.503 0.050-0.252c0.076-0.382-0.096-0.599-0.369-0.755-0.861-0.302-1.723-0.604-2.584-0.906-0.673-0.289-1.064-0.694-1.192-1.208-0.082-0.393-0.052-0.846 0.235-1.274zM13.236 22.666v-6.304h5.529v6.304h-5.529zM18.764 23.337v1.963h-5.528v-1.963h5.528zM17.821 13.392c0-0.905 0.734-1.639 1.639-1.639s1.637 0.734 1.637 1.639c0 0.904-0.733 1.638-1.637 1.638s-1.639-0.734-1.639-1.638zM19.746 23.337h5.529v1.963h-5.529v-1.963zM19.746 22.666v-6.304h5.529v6.304h-5.529zM19.905 9.284c1.162-0.027 2.256 0.304 3.288 0.952 0.651 0.45 1.172 0.995 1.547 1.647 1.402 2.434-1.782 2.762-3.454 2.678 0.216-0.338 0.342-0.739 0.342-1.169 0-1.198-0.971-2.168-2.168-2.168-0.706 0-1.334 0.338-1.729 0.861-0.265-1.676 0.514-2.602 2.174-2.8zM6.724 3.689c0-0.461 0.377-0.837 0.838-0.837h8.377c0.46 0 0.837 0.377 0.837 0.837v1.569h-10.053v-1.569zM6.724 5.859h10.052v8.502c0 0.46-0.377 0.837-0.838 0.837h-8.377c-0.461 0-0.838-0.377-0.838-0.837v-8.502zM6.725 22.666v-6.304h5.528v6.304h-5.529zM12.254 23.337v1.963h-5.529v-1.963h5.529zM28.608 21.116h-0.982v6.665h-1.569l-0.278 1.366h-1.416l-0.279-1.366h-16.171l-0.278 1.366h-1.417l-0.277-1.366h-1.569v-6.665h-0.982v-2.326h2.583v7.080h20.049v-7.080h2.584v2.326zM9.457 10.403c0 1.15 0.848 2.104 1.952 2.268-0.645-0.759-1.030-1.728-1.030-2.783 0-0.526 0.096-1.030 0.272-1.499-0.711 0.39-1.194 1.146-1.194 2.014z"></path>
                                </svg>
                                Điện Tử - Phụ Kiện
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/thoi-trang-lam-dep">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M10.46 3.659c-1.617 0-2.197 1.473-1.621 3.106l1.621 3.957 1.621-3.957c0.575-1.633-0.004-3.106-1.621-3.106zM6.875 16.884c1.038-2.090 1.271-2.973 0.072-4.756-0.309 1.585-0.335 3.171-0.072 4.756zM14.045 16.884c0.262-1.585 0.236-3.171-0.073-4.756-1.197 1.782-0.966 2.666 0.073 4.756zM25.582 10.22l0.75-1.454-0.861-2.845-1.728 3.351 0.755 0.389zM26.735 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0 5.265-2.369 5.265-5.264v-21.471c0-2.896-2.369-5.264-5.264-5.264zM20.369 12.37l0.677 0.35 1.972-3.823 0.338 0.175 2.221-4.307 1.223 4.043-0.831 1.611 0.339 0.175-1.971 3.822 0.677 0.35-0.649 1.258-4.645-2.396 0.649-1.258zM19.52 14.015l4.645 2.396-2.52 4.887-4.646-2.395 2.52-4.887zM3.362 25.372l1.83-5.561c-0.575-2.401-1.056-5.318-0.753-7.962l0.508-3.439c0.096-0.841 0.048-2.018 0.835-2.49l1.239-0.744-0.659-0.878 1.47-1.088c0.766-0.568 1.39-0.888 2.628-0.888 1.236 0 1.862 0.32 2.627 0.888l1.47 1.088-0.658 0.878 1.239 0.744c0.787 0.471 0.739 1.649 0.835 2.49l0.508 3.439c0.303 2.643-0.177 5.561-0.752 7.962l1.83 5.561c-2.367 1.097-4.761 1.976-7.098 1.976-2.338 0-4.731-0.878-7.098-1.976zM28.558 21.109l-0.983 3.273v4.992h-0.802v-4.87c0-1.084-1.19-1.659-1.82-0.544l-1.806 3.76c-0.665 1.385-1.696 1.956-3.245 1.956h-3.539c-1.913-0.125-2.598-0.153-1.728-1.358l3.704-2.017c1.177 0.251 2.157-0.087 3.004-0.823l4.843-9.525 1.511 1.567c0.804 0.927 1.099 1.976 0.863 3.587z"></path>
                                </svg>
                                Thời Trang - Làm Đẹp
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/nong-thuy-hai-san">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.735 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0 5.265-2.369 5.265-5.264v-21.471c0-2.896-2.369-5.264-5.264-5.264zM10.252 3.187c1.61 0 2.915 1.305 2.915 2.916 0 1.609-1.304 2.914-2.915 2.914s-2.915-1.305-2.915-2.915c0-1.611 1.305-2.916 2.915-2.916zM2.735 14.479c-0.348-0.11-0.542-0.483-0.433-0.831l0.917-2.912c-0.437-0.321-0.72-0.838-0.72-1.422 0-0.957 0.763-1.736 1.713-1.764l-0.483 1.535 1.262 0.398 0.465-1.47c0.353 0.323 0.573 0.786 0.573 1.302 0 0.893-0.662 1.63-1.523 1.748l-0.941 2.984c-0.108 0.348-0.482 0.542-0.83 0.432zM24.39 24.523c-0.412 0.745-1.651 0.648-2.498 0.277l-4.997-2.181c-0.583-0.255-1.173-0.799-1.27-1.427l-0.277-1.824-2.419 2.34 1.23 4.759c0.323 1.256-0.085 2.087-1.007 2.305s-1.748-0.423-2.008-1.511l-1.309-5.473c-0.145-0.608-0.021-1.398 0.437-1.824l2.419-2.261-1.864-4.044-0.794 1.254c-0.329 0.521-1.072 0.714-1.784 0.491l-4.045-1.269 0.753-2.617 3.331 0.873 0.506-0.948c0.298-0.559 0.835-0.96 1.398-1.232l3.729-1.587c0.575-0.245 1.089-0.154 1.705 0l2.22 0.556c0.762 0.19 1.494 0.444 2.062 0.912l3.371 2.777-1.864 2.102-3.529-2.934-2.142-0.437 2.459 5.592 0.436 2.855 4.719 2.102c1.212 0.537 1.444 1.633 1.032 2.378zM29.511 15.624l-4.482 5.050c-0.317 0.357-0.866 0.389-1.223 0.073l-3.052-2.708c-0.357-0.316-0.39-0.866-0.073-1.223l4.482-5.050c0.317-0.357 0.866-0.389 1.223-0.073l3.051 2.708c0.357 0.316 0.39 0.867 0.073 1.223z"></path>
                                </svg>
                                Nông - Thủy Hải Sản
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/tuyen-dung-viec-lam">
                                 <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.736 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0.001 5.264-2.369 5.264-5.264v-21.472c0-2.896-2.369-5.264-5.264-5.264zM23.864 4.366c1.22 0 2.208 0.989 2.208 2.208s-0.988 2.208-2.208 2.208c-1.218 0-2.207-0.989-2.207-2.208s0.988-2.208 2.207-2.208zM16 2.931c1.524 0 2.76 1.236 2.76 2.76s-1.236 2.76-2.76 2.76-2.76-1.236-2.76-2.76c-0.001-1.524 1.236-2.76 2.76-2.76zM8.134 4.366c1.219 0 2.207 0.989 2.207 2.208s-0.989 2.208-2.207 2.208c-1.219 0-2.207-0.989-2.207-2.208s0.989-2.208 2.207-2.208zM10.8 24.020c0 0.733-0.6 1.333-1.332 1.333s-1.332-0.601-1.332-1.333c0 0.733-0.599 1.333-1.332 1.333s-1.332-0.601-1.332-1.333v-6.613h-0.46c-0.746 0-1.357-0.611-1.357-1.357v-5.136c0-0.746 0.611-1.357 1.357-1.357h4.694c-0.194 0.309-0.309 0.673-0.309 1.063v7.568c0 0.893 0.593 1.654 1.405 1.908v3.922zM19.86 19.253h-0.569v8.17c0 0.905-0.741 1.645-1.646 1.645s-1.646-0.74-1.646-1.645c0 0.905-0.741 1.645-1.646 1.645s-1.646-0.74-1.646-1.645v-8.17h-0.569c-0.921 0-1.675-0.755-1.675-1.675v-6.345c0-0.921 0.754-1.675 1.675-1.675h2.172l1.689 2.223 1.689-2.223h2.172c0.922 0 1.675 0.754 1.675 1.675v6.345c-0.001 0.921-0.755 1.675-1.675 1.675zM28.346 16.050c0 0.747-0.61 1.357-1.357 1.357h-0.46v6.613c0 0.733-0.6 1.333-1.333 1.333s-1.332-0.601-1.332-1.333c0 0.733-0.599 1.333-1.331 1.333s-1.333-0.601-1.333-1.333v-3.922c0.812-0.255 1.404-1.014 1.404-1.908v-7.568c0-0.39-0.113-0.755-0.308-1.063h4.694c0.746 0 1.357 0.611 1.357 1.357v5.135z"></path>
                                </svg>
                                Tuyển Dụng - Việc Làm
                                </a>
                                <a class="category-main__item" href="https://tungtang.com.vn/tin-dang/tim-hop-tac-kinh-doanh">
                                 <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.736 0h-21.472c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0 5.265-2.369 5.265-5.264v-21.471c0-2.896-2.369-5.264-5.264-5.264zM13.713 24.828c-0.153 0.444-0.428 0.785-0.767 1.026-0.442 0.314-0.931 0.392-1.415 0.367-0.649-0.061-1.044-0.457-1.005-1.124 0.015-0.255 0.070-0.497 0.158-0.72-0.277 0.081-0.56 0.102-0.844 0.088-0.649-0.061-1.044-0.457-1.005-1.123 0.014-0.255 0.069-0.496 0.158-0.72-0.277 0.082-0.56 0.102-0.844 0.089-0.649-0.061-1.045-0.457-1.005-1.124 0.015-0.254 0.069-0.497 0.157-0.72-0.276 0.082-0.56 0.103-0.844 0.089-0.647-0.061-1.044-0.457-1.005-1.123 0.036-0.628 0.314-1.184 0.735-1.556 0.711-0.63 1.593-0.608 2.16-0.032 0.358 0.363 0.451 0.857 0.292 1.318-0.007 0.020-0.014 0.038-0.021 0.055 0.518-0.142 1.040-0.005 1.421 0.381 0.357 0.363 0.451 0.857 0.291 1.319-0.006 0.018-0.012 0.036-0.020 0.054 0.517-0.142 1.039-0.005 1.421 0.382 0.357 0.363 0.45 0.856 0.291 1.318-0.006 0.019-0.012 0.038-0.020 0.055 0.517-0.142 1.039-0.005 1.421 0.382 0.358 0.36 0.451 0.854 0.291 1.316zM23.449 20.777c-0.463 0.483-1.108 0.3-1.662-0.097-0.498-0.485-0.995-0.971-1.492-1.456-0.2-0.227-0.355-0.308-0.445-0.206-0.107 0.12 0.023 0.271 0.154 0.377 0.502 0.526 1.003 1.052 1.504 1.577 0.474 0.565 0.562 1.145 0.085 1.602-0.517 0.497-1.124 0.386-1.686-0.036-0.587-0.587-1.174-1.172-1.76-1.759-0.203-0.205-0.407-0.349-0.501-0.263-0.105 0.096 0.049 0.301 0.186 0.433 0.534 0.561 1.099 1.090 1.567 1.709 0.449 0.596 0.359 1.168-0.11 1.518-0.615 0.459-1.198 0.299-1.662-0.255-0.514-0.522-1.028-1.044-1.541-1.565-0.216-0.211-0.388-0.273-0.486-0.158-0.080 0.094 0.016 0.232 0.121 0.366 0.449 0.45 0.899 0.9 1.349 1.35 0.479 0.567 0.507 1.157 0.026 1.609-0.461 0.435-1.020 0.412-1.479 0.078-0.282-0.206-0.508-0.464-0.714-0.729 0.312-1.335-0.386-2.566-1.7-2.767-0.116-0.956-0.786-1.703-1.7-1.845-0.13-0.877-0.851-1.66-1.7-1.797-0.427-1.455-1.682-2.076-3.156-1.748-1.043 0.233-1.657 0.997-2.040 1.797l-1.214-0.486v-8.692l2.331 0.825c0.907 0.321 1.774 0.378 2.574-0.146 1.239-0.811 2.411-1.594 3.691-1.893 1.363-0.32 2.424-0.041 3.060 0.679l-3.417 2.116c-1.053 0.652-1.349 2.105-0.818 3.194 0.591 1.209 2.062 1.691 3.281 1.138l2.584-1.171c0.811-0.368 1.37-0.271 2.052 0.407l4.695 4.671c0.413 0.412 0.588 1.038 0.024 1.627zM28.608 17.802l-2.325 0.442c-0.799-0.332-1.599-0.664-2.398-0.995l-4.538-4.465c-0.715-0.703-1.43-0.747-2.434-0.221l-3.017 1.575c-0.584 0.305-1.536 0.348-1.979-0.32-0.556-0.839-0.239-1.725 0.552-2.176l5.881-3.36c0.876-0.5 1.667-0.55 2.472 0.148l2.214 1.918c0.7 0.607 1.396 0.742 2.214 0.553l3.358-0.774v7.674z"></path>
                                </svg>
                                Hợp Tác Kinh Doanh
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/thiet-bi-cong-nghiep-xay-dung">
                                 <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M8.275 21.073c0 0.511-0.414 0.925-0.925 0.925s-0.925-0.414-0.925-0.925c0-0.511 0.414-0.925 0.925-0.925s0.925 0.414 0.925 0.925zM5.764 13.172h1.609v0.857h-1.609v-0.857zM15.363 23.011c0 0.283-0.23 0.513-0.513 0.513s-0.513-0.23-0.513-0.513c0-0.283 0.23-0.513 0.513-0.513s0.513 0.23 0.513 0.513zM26.735 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.472c0 2.895 2.369 5.264 5.264 5.264h21.471c2.896 0 5.265-2.368 5.265-5.264v-21.472c0-2.896-2.369-5.264-5.264-5.264zM12.117 26.581h-6.436c-1.26 0-2.289-1.030-2.289-2.289v-16.585c0-1.259 1.031-2.288 2.289-2.288h6.436c1.26 0 2.289 1.029 2.289 2.288v1.62h-2.371v-0.831h-6.27v0.857h5.68c-0.816 0.127-1.487 0.712-1.739 1.481h-3.941v0.856h3.833v11.027c0 1.199 0.982 2.181 2.181 2.181h2.544c-0.268 0.966-1.158 1.682-2.207 1.682zM28.608 22.232c0 1.095-0.896 1.991-1.991 1.991h-5.939v0.799h0.515c0.779 0 1.372 0.227 1.9 0.517 0.25 0.157 0.427 0.311 0.558 0.451 0.238 0.255 0.254 0.592-0.183 0.592h-7.867c-0.437 0-0.421-0.337-0.184-0.592 0.13-0.14 0.307-0.296 0.558-0.451 0.529-0.29 1.121-0.517 1.899-0.517h0.516v-0.799h-5.939c-1.095 0-1.992-0.896-1.992-1.991v-10.236c0-1.095 0.897-1.992 1.992-1.992h14.165c1.095 0 1.991 0.896 1.991 1.992v10.236zM25.922 12.428h-12.774c-0.144 0-0.262 0.118-0.262 0.262v8.846c0 0.145 0.119 0.262 0.262 0.262h12.774c0.143 0 0.262-0.118 0.262-0.262v-8.845c0-0.144-0.118-0.262-0.262-0.262z"></path>
                                </svg>
                                Thiết Bị Công Nghiệp
                                </a>
                                <a class="category-main__item"  href="https://tungtang.com.vn/tin-dang/noi-that-gia-dung">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.736 0h-21.472c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0 5.265-2.369 5.265-5.264v-21.471c0-2.896-2.369-5.264-5.264-5.264zM13.713 24.828c-0.153 0.444-0.428 0.785-0.767 1.026-0.442 0.314-0.931 0.392-1.415 0.367-0.649-0.061-1.044-0.457-1.005-1.124 0.015-0.255 0.070-0.497 0.158-0.72-0.277 0.081-0.56 0.102-0.844 0.088-0.649-0.061-1.044-0.457-1.005-1.123 0.014-0.255 0.069-0.496 0.158-0.72-0.277 0.082-0.56 0.102-0.844 0.089-0.649-0.061-1.045-0.457-1.005-1.124 0.015-0.254 0.069-0.497 0.157-0.72-0.276 0.082-0.56 0.103-0.844 0.089-0.647-0.061-1.044-0.457-1.005-1.123 0.036-0.628 0.314-1.184 0.735-1.556 0.711-0.63 1.593-0.608 2.16-0.032 0.358 0.363 0.451 0.857 0.292 1.318-0.007 0.020-0.014 0.038-0.021 0.055 0.518-0.142 1.040-0.005 1.421 0.381 0.357 0.363 0.451 0.857 0.291 1.319-0.006 0.018-0.012 0.036-0.020 0.054 0.517-0.142 1.039-0.005 1.421 0.382 0.357 0.363 0.45 0.856 0.291 1.318-0.006 0.019-0.012 0.038-0.020 0.055 0.517-0.142 1.039-0.005 1.421 0.382 0.358 0.36 0.451 0.854 0.291 1.316zM23.449 20.777c-0.463 0.483-1.108 0.3-1.662-0.097-0.498-0.485-0.995-0.971-1.492-1.456-0.2-0.227-0.355-0.308-0.445-0.206-0.107 0.12 0.023 0.271 0.154 0.377 0.502 0.526 1.003 1.052 1.504 1.577 0.474 0.565 0.562 1.145 0.085 1.602-0.517 0.497-1.124 0.386-1.686-0.036-0.587-0.587-1.174-1.172-1.76-1.759-0.203-0.205-0.407-0.349-0.501-0.263-0.105 0.096 0.049 0.301 0.186 0.433 0.534 0.561 1.099 1.090 1.567 1.709 0.449 0.596 0.359 1.168-0.11 1.518-0.615 0.459-1.198 0.299-1.662-0.255-0.514-0.522-1.028-1.044-1.541-1.565-0.216-0.211-0.388-0.273-0.486-0.158-0.080 0.094 0.016 0.232 0.121 0.366 0.449 0.45 0.899 0.9 1.349 1.35 0.479 0.567 0.507 1.157 0.026 1.609-0.461 0.435-1.020 0.412-1.479 0.078-0.282-0.206-0.508-0.464-0.714-0.729 0.312-1.335-0.386-2.566-1.7-2.767-0.116-0.956-0.786-1.703-1.7-1.845-0.13-0.877-0.851-1.66-1.7-1.797-0.427-1.455-1.682-2.076-3.156-1.748-1.043 0.233-1.657 0.997-2.040 1.797l-1.214-0.486v-8.692l2.331 0.825c0.907 0.321 1.774 0.378 2.574-0.146 1.239-0.811 2.411-1.594 3.691-1.893 1.363-0.32 2.424-0.041 3.060 0.679l-3.417 2.116c-1.053 0.652-1.349 2.105-0.818 3.194 0.591 1.209 2.062 1.691 3.281 1.138l2.584-1.171c0.811-0.368 1.37-0.271 2.052 0.407l4.695 4.671c0.413 0.412 0.588 1.038 0.024 1.627zM28.608 17.802l-2.325 0.442c-0.799-0.332-1.599-0.664-2.398-0.995l-4.538-4.465c-0.715-0.703-1.43-0.747-2.434-0.221l-3.017 1.575c-0.584 0.305-1.536 0.348-1.979-0.32-0.556-0.839-0.239-1.725 0.552-2.176l5.881-3.36c0.876-0.5 1.667-0.55 2.472 0.148l2.214 1.918c0.7 0.607 1.396 0.742 2.214 0.553l3.358-0.774v7.674z"></path>
                                </svg>
                                Nội Thất Gia Dụng
                                </a>
                                <a class="category-main__item" href="https://tungtang.com.vn/tin-dang/me-va-be">
                                 <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M9.265 14.373c0 0.308-0.249 0.557-0.557 0.557s-0.557-0.249-0.557-0.557c0-0.308 0.249-0.557 0.557-0.557s0.557 0.249 0.557 0.557zM21.886 5.865c0 0.739-0.599 1.337-1.337 1.337s-1.337-0.599-1.337-1.338c0-0.739 0.599-1.338 1.337-1.338s1.337 0.599 1.337 1.338zM26.735 0h-21.471c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0 5.265-2.369 5.265-5.264v-21.471c0-2.896-2.369-5.264-5.264-5.264zM13.451 5.546c0.425 0.444 0.312 1.308-0.252 1.933s-1.365 0.772-1.79 0.33c-0.425-0.442-0.313-1.308 0.252-1.933s1.365-0.773 1.79-0.33zM11.303 10.319c-0.455 0.178-0.941 0.099-1.451-0.125-0.396-0.161-0.912-0.291-1.781 0.081-0.386 0.124-0.801 0.208-1.388 0.111-0.727-0.12-1-0.608-0.609-1.443 0.317-0.677 0.804-1.236 1.375-1.726 0.482-0.411 0.98-0.777 1.551-0.776 0.594 0.003 1.016 0.289 1.398 0.654 0.605 0.578 1.161 1.142 1.419 1.861 0.191 0.534 0.056 1.138-0.516 1.363zM9.68 3.486c0.411-0.806 1.202-1.183 1.767-0.841s0.689 1.272 0.277 2.078c-0.411 0.806-1.203 1.183-1.767 0.841s-0.689-1.272-0.277-2.078zM7.207 2.443c0.614-0.274 1.368 0.208 1.682 1.076s0.070 1.795-0.544 2.069c-0.615 0.274-1.369-0.207-1.683-1.075-0.314-0.869-0.071-1.795 0.545-2.070zM4.928 6.954c-0.563-0.625-0.676-1.49-0.251-1.933s1.226-0.295 1.79 0.33c0.564 0.625 0.676 1.49 0.252 1.933s-1.226 0.295-1.79-0.33zM23.265 26.603c-2.049 1.898-4.819 3.064-7.867 3.064s-5.819-1.166-7.867-3.064c-0.498-0.462-0.447-0.834-0.244-1.092s0.599-0.36 1.121 0.128c1.815 1.697 4.277 2.739 6.99 2.739s5.175-1.042 6.99-2.739c0.522-0.488 0.919-0.386 1.122-0.128s0.254 0.631-0.244 1.092zM22.328 19.884c-0.225-0.698-0.704-1.224-1.577-1.261-0.524-0.023-0.914 0.25-1.231 0.587 1.108 1.354 1.302 4.076 0.974 6.689-0.195 0.654-1.145 0.56-1.277-0.114-0.107-2.3-1.274-3.709-3.886-3.602-2.356 0.097-3.703 1.305-4.142 3.545-0.046 0.548-0.556 0.548-0.938 0.147-2.477-2.698-2.829-7.464-1.050-9.517-1.181 0.655-1.966 0.832-2.591 0.552-0.41-0.184-0.819-0.652-0.85-1.421-0.037-0.905 0.563-1.444 1.34-1.807l-0.575-1.253c2.104-1.201 4.972-1.592 6.503 0.44 0.957 1.271 0.683 3.021 0.101 4.471 2.114-0.617 4.028-0.313 5.681 0.983 0.093 0.104 0.181 0.21 0.266 0.319 0.39-0.71 0.866-1.285 1.582-1.485 1.038-0.174 2.104 0.206 3.192 1.020l-1.521 1.706zM24.824 16.015h-5.625l-0.681-2.044h6.988l-0.681 2.044zM26.703 12.086l-0.633 1.371h-8.117l-0.632-1.371c-0.218-0.472 0.026-0.548 0.332-0.548h4.061c0.029-0.994-0.032-1.989-0.305-2.982-0.145 0.665-0.639 1.156-1.227 1.159-0.705 0.004-1.282-0.697-1.285-1.563-0.002-0.294 0.063-0.569 0.176-0.804-0.125 0.066-0.259 0.119-0.404 0.157-0.84 0.219-1.663-0.157-1.842-0.84s0.357-1.415 1.195-1.634c0.282-0.073 0.559-0.079 0.814-0.030-0.166-0.116-0.319-0.266-0.447-0.446-0.504-0.705-0.447-1.609 0.128-2.021s1.448-0.171 1.952 0.534c0.206 0.289 0.319 0.611 0.34 0.918 0.097-0.25 0.261-0.49 0.485-0.692 0.645-0.579 1.55-0.623 2.023-0.099 0.472 0.526 0.333 1.42-0.311 2-0.183 0.164-0.386 0.285-0.595 0.363 0.232 0.013 0.473 0.072 0.706 0.184 0.78 0.377 1.164 1.198 0.857 1.833-0.279 0.577-1.032 0.804-1.752 0.558 0.343 1.136 0.418 2.271 0.384 3.405h3.768c0.305-0.001 0.549 0.075 0.331 0.547zM25.365 10.706c-0.72 0.438-1.519 0.457-2.071-0.024 0.878-0.229 1.465-0.763 1.881-1.476-0.725 0.107-1.443 0.377-2.146 0.947-0.144-0.623 0.104-1.315 1.131-1.806 1.090-0.521 2.279-0.377 3.419-0.457-0.677 1.062-1.287 2.254-2.215 2.817z"></path>
                                </svg>
                                Mẹ Và Bé
                                </a>
                                 <a class="category-main__item" href="https://tungtang.com.vn/tin-dang/am-thuc-du-lich">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M26.736 0h-21.472c-2.896 0-5.264 2.369-5.264 5.264v21.471c0 2.896 2.369 5.265 5.264 5.265h21.471c2.896 0 5.265-2.369 5.265-5.264v-21.471c0-2.896-2.369-5.264-5.264-5.264zM13.713 24.828c-0.153 0.444-0.428 0.785-0.767 1.026-0.442 0.314-0.931 0.392-1.415 0.367-0.649-0.061-1.044-0.457-1.005-1.124 0.015-0.255 0.070-0.497 0.158-0.72-0.277 0.081-0.56 0.102-0.844 0.088-0.649-0.061-1.044-0.457-1.005-1.123 0.014-0.255 0.069-0.496 0.158-0.72-0.277 0.082-0.56 0.102-0.844 0.089-0.649-0.061-1.045-0.457-1.005-1.124 0.015-0.254 0.069-0.497 0.157-0.72-0.276 0.082-0.56 0.103-0.844 0.089-0.647-0.061-1.044-0.457-1.005-1.123 0.036-0.628 0.314-1.184 0.735-1.556 0.711-0.63 1.593-0.608 2.16-0.032 0.358 0.363 0.451 0.857 0.292 1.318-0.007 0.020-0.014 0.038-0.021 0.055 0.518-0.142 1.040-0.005 1.421 0.381 0.357 0.363 0.451 0.857 0.291 1.319-0.006 0.018-0.012 0.036-0.020 0.054 0.517-0.142 1.039-0.005 1.421 0.382 0.357 0.363 0.45 0.856 0.291 1.318-0.006 0.019-0.012 0.038-0.020 0.055 0.517-0.142 1.039-0.005 1.421 0.382 0.358 0.36 0.451 0.854 0.291 1.316zM23.449 20.777c-0.463 0.483-1.108 0.3-1.662-0.097-0.498-0.485-0.995-0.971-1.492-1.456-0.2-0.227-0.355-0.308-0.445-0.206-0.107 0.12 0.023 0.271 0.154 0.377 0.502 0.526 1.003 1.052 1.504 1.577 0.474 0.565 0.562 1.145 0.085 1.602-0.517 0.497-1.124 0.386-1.686-0.036-0.587-0.587-1.174-1.172-1.76-1.759-0.203-0.205-0.407-0.349-0.501-0.263-0.105 0.096 0.049 0.301 0.186 0.433 0.534 0.561 1.099 1.090 1.567 1.709 0.449 0.596 0.359 1.168-0.11 1.518-0.615 0.459-1.198 0.299-1.662-0.255-0.514-0.522-1.028-1.044-1.541-1.565-0.216-0.211-0.388-0.273-0.486-0.158-0.080 0.094 0.016 0.232 0.121 0.366 0.449 0.45 0.899 0.9 1.349 1.35 0.479 0.567 0.507 1.157 0.026 1.609-0.461 0.435-1.020 0.412-1.479 0.078-0.282-0.206-0.508-0.464-0.714-0.729 0.312-1.335-0.386-2.566-1.7-2.767-0.116-0.956-0.786-1.703-1.7-1.845-0.13-0.877-0.851-1.66-1.7-1.797-0.427-1.455-1.682-2.076-3.156-1.748-1.043 0.233-1.657 0.997-2.040 1.797l-1.214-0.486v-8.692l2.331 0.825c0.907 0.321 1.774 0.378 2.574-0.146 1.239-0.811 2.411-1.594 3.691-1.893 1.363-0.32 2.424-0.041 3.060 0.679l-3.417 2.116c-1.053 0.652-1.349 2.105-0.818 3.194 0.591 1.209 2.062 1.691 3.281 1.138l2.584-1.171c0.811-0.368 1.37-0.271 2.052 0.407l4.695 4.671c0.413 0.412 0.588 1.038 0.024 1.627zM28.608 17.802l-2.325 0.442c-0.799-0.332-1.599-0.664-2.398-0.995l-4.538-4.465c-0.715-0.703-1.43-0.747-2.434-0.221l-3.017 1.575c-0.584 0.305-1.536 0.348-1.979-0.32-0.556-0.839-0.239-1.725 0.552-2.176l5.881-3.36c0.876-0.5 1.667-0.55 2.472 0.148l2.214 1.918c0.7 0.607 1.396 0.742 2.214 0.553l3.358-0.774v7.674z"></path>
                                </svg>
                                Ẩm Thực - Du Lịch
                                </a>
                                <a class="category-main__item" href="https://tungtang.com.vn/tin-dang/dich-vu-khac">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="category-main__item-icon" viewBox="0 0 32 32">
                                <path fill="#FAA519" d="M12.104 18.917v1.921h7.792v-1.921c0-2.142-1.753-3.896-3.895-3.896s-3.896 1.753-3.896 3.896zM26.736 0h-21.472c-2.896 0-5.264 2.369-5.264 5.264v21.472c0 2.895 2.369 5.264 5.264 5.264h21.472c2.895 0 5.264-2.368 5.264-5.264v-21.472c0-2.896-2.368-5.264-5.264-5.264zM8.227 7.952h-2.612c-0.157 0-0.287-0.128-0.287-0.286v-0.849c0-0.157 0.129-0.286 0.287-0.286h2.612v1.421zM22.799 8.13c0 0.421-0.206 0.55-0.719 0.55h-1.702c-0.343 0-0.773 0.066-0.936 0.332-0.244 0.396-0.492 0.773-0.84 1.073-0.267 0.315-0.279 0.837 0.215 0.897 0.323 0.040 0.532-0.159 0.721-0.429 0.209-0.296 0.443-0.572 0.663-0.859 0.176 0 0.352 0 0.528 0 1.172 1.073 2.069 2.983 2.069 4.587v6.611c0 2.515-1.323 4.366-3.884 5.276v-3.064c-0.314-0.31-0.636-0.576-0.972-0.781h-0.001v4.875c0 0.774-0.633 1.409-1.407 1.409h-1.069c-0.774 0-1.409-0.634-1.409-1.409v-4.875c-0.336 0.204-0.659 0.471-0.972 0.781v3.064c-2.56-0.91-3.884-2.761-3.884-5.276v-6.611c0-1.603 0.897-3.513 2.069-4.586 0.175 0 0.351 0 0.527 0 0.222 0.287 0.456 0.563 0.665 0.859 0.191 0.27 0.399 0.469 0.722 0.429 0.495-0.060 0.482-0.584 0.216-0.898-0.347-0.3-0.595-0.678-0.841-1.073-0.164-0.266-0.595-0.332-0.937-0.332h-1.702c-0.513 0-0.718-0.129-0.718-0.55v-1.659c0-0.545 0.296-0.718 0.645-0.718h0.936c0.784 0 1.768-0.514 2.088-0.859 0.771-0.827 1.671-1.502 3.13-1.502s2.358 0.675 3.129 1.502c0.322 0.345 1.305 0.859 2.090 0.859h0.936c0.348 0 0.644 0.174 0.644 0.718v1.659zM26.673 7.667c0 0.157-0.129 0.286-0.286 0.286h-2.613v-1.42h2.614c0.157 0 0.286 0.128 0.286 0.286v0.848zM17.961 7.272c0 1.083-0.878 1.961-1.96 1.961s-1.96-0.878-1.96-1.96c0-1.083 0.878-1.96 1.96-1.96s1.96 0.878 1.96 1.96z"></path>
                                </svg>
                                Dịch Vụ Khác
                                </a>
                            </div>



                        </div>
                    </main>
                </div>
            </div>
        </div>
    </main>

    <script>
       (function () {
            window.CustomModalJQuery.onShows.push(function (obj) {
                if (obj[0].id == "modalBoLoc") {
                    var from = $('#ContentPlaceHolder1_rgGiaTu').val();
                    var to = $('#ContentPlaceHolder1_rgGiaDen').val();
                    $('#filterPriceRange').val(from + ";" + to);
                    $('#filterPriceRange').ionRangeSlider({
                        min: 0,
                        max: 10000000,
                        type: 'double',
                        step: 100000,                        
                        postfix: "",                        
                        hasGrid: true,
                        prettifyFunc: function(num){                            
                            var le = parseInt(num % 1000);
                            var chan = parseInt(num / 1000);
                            var trieu = parseInt(chan / 1000);
                            var tram = chan % 1000;
                            var text = "";
                            if (chan > 1000 && le < 1000 && trieu % 1000) {
                                if (tram == 0)
                                {
                                    text += " " + trieu + " Triệu ";
                                }
                                else {
                                    text += " " + trieu + " Triệu " + tram + " Nghìn";
                                }                                
                            }
                            else if (trieu / 1000) {
                                text += " " + trieu + " Triệu";
                            }
                            else if (chan < 1000) {
                                text += " " + chan + " Nghìn";
                            }
                            text = text.trim();
                            if (text == "") {
                                text = "0 Nghìn";
                            }
                            return text;
                        },
                    });
                }
            });
        })();
        function btnBoLoc_click() {
            var rangePrice = $('#filterPriceRange').val();
            var prices = rangePrice.split(";");
			var Range = "0";
            if (prices.length == 2) {
                $("#ContentPlaceHolder1_rgGiaTu").val(prices[0]);
                $("#ContentPlaceHolder1_rgGiaDen").val(prices[1]);
            } else {
                $("#ContentPlaceHolder1_rgGiaTu").val(0);
                $("#ContentPlaceHolder1_rgGiaDen").val(0);
            }

            if (document.getElementById("ContentPlaceHolder1_chonrulerhh").checked) {                
                var GiaTu = $("#ContentPlaceHolder1_rgGiaTu").val();
                var GiaDen = $("#ContentPlaceHolder1_rgGiaDen").val();
            }   
            var SapXepTheo = "DESC";
            if (!document.getElementById("ContentPlaceHolder1_radTinMoiTruoc").checked)
                SapXepTheo = "PRICE";
            var CanXem = "SEaBU";
            if (document.getElementById("ContentPlaceHolder1_ckbCanMua").checked && !document.getElementById("ContentPlaceHolder1_ckbCanBan").checked)
                CanXem = "BU";
            else if (!document.getElementById("ContentPlaceHolder1_ckbCanMua").checked && document.getElementById("ContentPlaceHolder1_ckbCanBan").checked)
                CanXem = "SE";
          var Gia = "0";
            if (document.getElementById("ContentPlaceHolder1_ckb10100").checked && !document.getElementById("ContentPlaceHolder1_ckb100500").checked && !document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "10100";
            else if (!document.getElementById("ContentPlaceHolder1_ckb10100").checked && document.getElementById("ContentPlaceHolder1_ckb100500").checked && !document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "100500";
            else if (!document.getElementById("ContentPlaceHolder1_ckb10100").checked && !document.getElementById("ContentPlaceHolder1_ckb100500").checked && document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "500";
            else if (document.getElementById("ContentPlaceHolder1_ckb10100").checked && document.getElementById("ContentPlaceHolder1_ckb100500").checked && !document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "10100100500";
            else if (document.getElementById("ContentPlaceHolder1_ckb10100").checked && !document.getElementById("ContentPlaceHolder1_ckb100500").checked && document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "10100500";
            else if (!document.getElementById("ContentPlaceHolder1_ckb10100").checked && document.getElementById("ContentPlaceHolder1_ckb100500").checked && document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "100500500";
            else if (document.getElementById("ContentPlaceHolder1_ckb10100").checked && document.getElementById("ContentPlaceHolder1_ckb100500").checked && document.getElementById("ContentPlaceHolder1_ckb500").checked)
                Gia = "10100100500500";
            if (GiaTu == 0 && GiaDen == 0)
            {
                $("#ContentPlaceHolder1_txtBoLocThem").val(SapXepTheo + "|" + CanXem + "|"+ Gia);
            }
            else
            {
                $("#ContentPlaceHolder1_txtBoLocThem").val(GiaTu + "|" + GiaDen + "|" + SapXepTheo + "|" + CanXem + "|" + Gia);
            }           
            document.getElementById("ContentPlaceHolder1_btnTimKiem").click();
        }
    </script>
	<script>                                            
        var checkboxValues = JSON.parse(sessionStorage.getItem('checkboxValues')) || {},
        $checkboxes = $("#checkbox-container :checkbox");

        $checkboxes.on("change", function () {
            $checkboxes.each(function () {
                checkboxValues[this.id] = this.checked;
            });

            sessionStorage.setItem("checkboxValues", JSON.stringify(checkboxValues));
        });

        // On page load
        $.each(checkboxValues, function (key, value) {
            $("#" + key).prop('checked', value);
        });

    </script>

</asp:Content>

