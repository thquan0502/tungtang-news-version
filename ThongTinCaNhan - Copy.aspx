﻿<%@ Page Title="Thông tin cá nhân" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="ThongTinCaNhan - Copy.aspx.cs" Inherits="ThongTinCaNhan" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css"/>
    <link href="/asset/vendors/ion.rangeslider/css/ion.rangeSlider.Metronic.css" rel="stylesheet" type="text/css"/>
    <script src="/asset/vendors/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.js"></script>
	
	<script src="../Js/Page/TinDang.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>

    <link href="../Css/Page/ThongTinCaNhan.css" rel="stylesheet" />
    <link href="../Css/Page/AccountPost.css" rel="stylesheet" />
	<link href="/asset/css/zalo/style.css" rel="stylesheet" />
    <script src="../Js/Page/ThongTinCaNhan.min.js"></script>
   <%-- <link href="../Css/Page/TabCss.css" rel="stylesheet" />--%>

<%--   style='margin: 0 0 1px;cursor:pointer;color:#fe9900;font-weight: 600;'
    style='color:#ec3222;font-weight:600;'--%>
       <style>
        .table-bordered1 td {
        border: 1px solid #ddd !important;
    }
        .table-bordered2 td {
        border: 1px solid #ddd !important;
    }
        .dataTables_paginate.paging_simple_numbers{
            font-size:10px;
        }
        .dataTables_info{
            font-size:10px;
        }
        .dataTables_length{
            font-size:10px;
        }
        .dataTables_filter{
            font-size:10px;
        }
            .nav_pagi .pagination .page-link.page-db {
    width: auto;
    padding: 0 20px;
}

.nav_pagi .pagination .page-link:hover {
    background-color: #eee;
    border-color: #ebebeb;
}

.nav_pagi .pagination .page-item.disabled .page-link {
    border: 0;
    display: none;
}

.nav_pagi .pagination .page-item.active .page-link {
    border: solid 1px #ebebeb;
    color: #f53f2c;
    display: block;
}
.relative {
    position: relative !important;
}
.f-left {
    float: left !important;
}
.w_100 {
    width: 100%;
}
        .container-sliderTrang {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            margin-top: 5px;
            width: 100%;
        }

        .box-minmax {
            width: 100%;
            display: flex;
            justify-content: space-between;
            font-size: 10px;
            color: #888;
        }

        .range-slider {
            width: 100%;
            position: relative;
            padding-top: 35px;
        }

        .rs-range {
            margin-top: 10px;
            width: 100%;
            -webkit-appearance: none;
        }

            .rs-range:focus {
                outline: none;
            }

            .rs-range::-webkit-slider-runnable-track {
                width: 100%;
                height: 28px;
                cursor: pointer;
                box-shadow: none;
                background: url('../../images/icons/colors.png') no-repeat no-repeat;
                background-size: contain;
                border-radius: 0px;
                border: 0px solid black;
            }

            .rs-range::-moz-range-track {
                width: 100%;
                height: 28px;
                cursor: pointer;
                box-shadow: none;
                background: url('../../images/icons/colors.png') no-repeat no-repeat;
                background-size: contain;
                border-radius: 0px;
                border: 0px solid black;
            }

            .rs-range::-webkit-slider-thumb {
                display: none;
            }

            .rs-range::-moz-focus-outer {
                border: 0;
            }

        .rs-label {
            position: absolute;
            display: block;
            width: max-content;
            min-width: 73px;
            height: 30px;
            margin-left: -52px;
            border-radius: 5px;
            top: 0;
            text-align: center;
            padding-top: 5px;
            border: 1px solid #ccc;
            left: 19.07914% !important;
            color: black;
            font-style: normal;
            font-weight: normal;
            line-height: normal;
            font-size: 12px;
            cursor: pointer;
        }

            .rs-label:hover .ArrowBullet {
                background-color: #ededed;
            }

            .rs-label:hover {
                background-color: #ededed;
            }

        .ArrowBullet {
            position: absolute;
            height: 10px;
            width: 10px;
            background-color: white;
            border-bottom: 1px #cccccc solid;
            border-right: 1px #cccccc solid;
            bottom: -5.5px;
            left: 45%;
            transform: rotate(45deg);
        }

        .listItem {
            max-height: 500px;
            overflow-y:unset;
        }
    </style> 
	<script src="https://sp.zalo.me/plugins/sdk.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
     <style>
         .colordanhmuc {             	
             padding: 10px;	
              box-shadow: 5px 5px 20px 5px #e7bb78;	
              color: white;	
            }
         .btnclick{
             box-shadow: 0 4px 8px 0 rgba(255, 235, 205, 0.2), 0 6px 20px 0 rgba(255, 250, 240, 0.19);
         }
        .modal a.close-modal{
            top: 5px;
            right: 5px;
        }
        @media (max-width:600px) {
			.titlethh{	
                display:block;	
                text-align:center;                	
            }	
            .nbdaduyet{	
                display:block;	
            }	
            .nbchoduyet{	
                display:block;	
            }	
            .nbbituchoi{	
                display:block;	
            }	
            nbtrichhh{	
                display:block;	
            }	
            .btnTabql{	
                margin-left:-5px;	
                word-wrap:break-word;	
            }
            .btndaduyet{		
                width:23%;		
                margin-bottom:5px;                                                            		
            }		
            .btnchoduyet{		
                width:24%;		
                margin-bottom:5px;               		
            }		
            .btnbituchoi{		
                width:24%;		
                margin-bottom:5px;	                      		
            }		
            .btntrichhh{		
                width:24%;	              		
                margin-bottom:5px;                		
            }
        }
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        }
        @media (min-width:992px) {
			.btnTabql{	
                display:inline-block;	
                margin-left:5px;	
            }
            .btndaduyet{	
                width:22%;  	
                margin-left:45px;              	
            }	
            .btnchoduyet{	
                width:22%;	
            }	
            .btnbituchoi{	
                width:22%;                	
            }	
            .btntrichhh{
                width:24%;
            }
        }
         #tdaduyet li{
            cursor:pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-content">
        <div class="container WrapperContainer">
            <div class="breadcrumWrapper" style="margin-top: 10px;">
                <ul class="sc-jzJRlG cIiMVT">
                    <li class="sc-cSHVUG jbltJC"><a href="/" class="sc-kAzzGY eZQMkE">Trang chủ</a></li>
                    <li class="sc-cSHVUG jbltJC"><a class="sc-kAzzGY llFQGq">Trang cá nhân</a></li>
                </ul>
            </div>
            <div class="PaperContainer contactInfo false">
                <div class="PaperInfoWrapper" style="color: rgba(0, 0, 0, 0.87); background-color: #ffffff; transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms; box-sizing: border-box; font-family: Verdana, Arial, sans-serif; -webkit-tap-highlight-color: rgba(0,0,0,0); box-shadow: 0 1px 2px rgba(0,0,0,.1); border-radius: 2px">
                    <div class="row">
                        <div class="col-xs-12 col-md-6 BasicInfo">
                            <div class="AvatarWrapper">
                                <img id="imgLinkAnh" runat="server" src="/images/icons/signin.png" style="color: #ffffff; border: .25px solid #ffba00; user-select: none; display: inline-flex; align-items: center; justify-content: center; font-size: 40px; border-radius: 50%; height: 80px; width: 80px"
                                    size="80">
                            </div>
                            <div class="InfoWrapper">
                                <span class="name" id="txtNameShopID" runat="server"></span>
                                <span><p id="txtInfoAccountCode" class="color-yellow" runat="server" style="font-weight: 600;display:inline-block;"></p></span><br />
                                <p style="display: inline-block;">Loại TV: </p>
                                <h6 id="txtLoaiUser" runat="server" style="font-weight: 600;color:#4cb050;display: inline-block;"></h6>
                                 <img id="ImagePro" runat="server" src="/images/vuongniem_tungtang.png" alt="Thành viên Pro Tung Tăng" style="width: 21px;margin-top: -17px;margin-left: 3px;display:none;" />
                                <div class="UltiRow" id="ProNow" runat="server" style="margin-bottom:8px;">
                                    <span>
                                   <a class="MainFunctionButton EditProfile" id="buttonUser" style="background-color:#4cb050;color:white;margin-bottom:10px;font-size: 9px;" href="/thong-tin/pro" runat="server">Đăng ký PRO</a>    
                                    </span>
                                </div>
                               <%-- <div class="UltiRow" id="UserIdenti" runat="server">
                                    <a class="MainFunctionButton EditProfile" id="btnEditInfoUserID" runat="server" href="/thong-tin/tt">Chỉnh sửa thông tin</a>
                                </div>--%>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ExtraInfo">
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày tham gia:<span id="txtDateShopApprovedID" runat="server"></span>
                            </div>
                            <div class="itemRow" style="height:auto;display:block;">
                                <i class="fa fa-map-marker" style="padding: 0 13px;"></i>Địa chỉ:<span id="txtShopAddressID" runat="server" style="white-space:normal;"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-phone" style="padding: 0 11px;"></i>Số điện thoại:<span id="txtShopPhoneID" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-envelope" style="padding: 0 10px;"></i>Email:<span id="txtShopEmailID" runat="server"></span>
                            </div>
                            <%--<div class="itemRow">
                                <i class="fa fa-credit-card" style="padding: 0 10px;"></i>Mã số thuế: &nbsp;  <span id="txtMST" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-calendar" style="padding: 0 10px;"></i>Ngày cấp: &nbsp;  <span id="txtNgayCap" runat="server"></span>
                            </div>
                            <div class="itemRow">
                                <i class="fa fa-map-marker" style="padding: 0 10px;"></i>Nơi cấp: &nbsp;  <span id="txtNoiCap" runat="server"></span>
                            </div>--%>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
                        <% if (!string.IsNullOrWhiteSpace(renovateSuccessMessage))
                { %>
            <script>
                Swal.fire('Thành công', '<%= renovateSuccessMessage %>', 'success');
            </script>
            <% } %>
           <div class="container indexPage PaperContainer" style="height:auto">	
            <div class="PaperWrapper app-box" >	
                <div class="TitleHeading" style="margin-top:-17px;">
                <h4><span id="TitleHeader" style="color:#33a837;font-weight: bold;font-size: 13px;" runat="server">QUẢN LÝ TIN ĐĂNG</span></h4>
                    <p class="qltin"></p>
                  <div id="btnTin" runat="server">
                    <button id="daduyet" runat="server" onserverclick="daduyet_ServerClick" type="button" class="btn btn-success btndaduyet"><img src="../../Images/duyet.png" width="40px" /><p class="btnTabql">Đã duyệt<span class="titlethh" style="color:white"> (<span id="nbdaduyet" style="color:#fff" runat="server"></span>)</span></p></button>	
                    <button id="choduyet" runat="server" onserverclick="choduyet_ServerClick" type="button" class="btn btn-info btnchoduyet"><img src="../../Images/choduyet.png" width="40px" /><p class="btnTabql">Chờ duyệt<span class="titlethh" style="color:white"> (<span id="nbchoduyet" style="color:#fff" runat="server"></span>)</span></p></button>	
                    <button id="bituchoi" runat="server" onserverclick="bituchoi_ServerClick" type="button" class="btn btn-danger btnbituchoi"><img src="../../Images/khongduyet.png" width="40px" /><p class="btnTabql">Bị từ chối<span class="titlethh" style="color:white"> (<span id="nbbituchoi" style="color:#fff" runat="server"></span>)</span></p></button>	
                    <button id="trichhh" runat="server" onserverclick="trichhh_ServerClick" type="button" class="btn btn-primary btntrichhh"><img src="../../Images/tien.png" width="40px" /><p class="btnTabql">Trích hoa<span class="titlethh" style="color:white"> hồng (<span id="nbtrichhh" style="color:#fff" runat="server"></span>)</span></p></button>
                </div>
                    <input id="Tabip" style="display:none" runat="server" />
                    <script>	
                        var dduyet = document.getElementById('<%= daduyet.ClientID %>');	
                        var cduyet = document.getElementById('<%= choduyet.ClientID %>');	
                        var btuchoi = document.getElementById('<%= bituchoi.ClientID %>');	
                        var thoahong = document.getElementById('<%= trichhh.ClientID %>');	
                        const queryString = window.location.search;	
                        const urlParams = new URLSearchParams(queryString);	
                        const product = urlParams.get('Tab');	
                        //alert(product);	
                        if (product == '1' || product == null)	
                        {                       	
                            cduyet.classList.remove("colordanhmuc");	
                            btuchoi.classList.remove("colordanhmuc");	
                            thoahong.classList.remove("colordanhmuc");	
                            dduyet.classList.add("colordanhmuc");	
                        }	
                        if (product == '2')	
                        {                     	
                            dduyet.classList.remove("colordanhmuc");	
                            btuchoi.classList.remove("colordanhmuc");	
                            thoahong.classList.remove("colordanhmuc");	
                            cduyet.classList.add("colordanhmuc");	
                        }	
                        if (product == '3')	
                        {                       	
                            dduyet.classList.remove("colordanhmuc");	
                            cduyet.classList.remove("colordanhmuc");	
                            thoahong.classList.remove("colordanhmuc");	
                            btuchoi.classList.add("colordanhmuc");	
                        }	
                        if (product == '4')	
                        {           	
                            dduyet.classList.remove("colordanhmuc");	
                            btuchoi.classList.remove("colordanhmuc");	
                            cduyet.classList.remove("colordanhmuc");	
                            thoahong.classList.add("colordanhmuc");	
                        }	
                    </script>
                    <div class="row list"  style="margin-top: -4px;" >
                        <div style="padding: 8px 0px 8px 0px; width: 100%;">
                            <div class="portlet box">
                            <div class="portlet-body" id="mainqltd">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card account-filter-card">
                                            <div class="card-body" style="font-size: 11px;">
                                                <div id="filterPostCodeWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Mã bài đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterPostCodeInput" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostOwnerWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Mã CTV:</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterPostOwnerInput" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostTitleWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Tiêu đề bài đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <input id="filterPostTitleInput" type="text" class="form-control" />
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostPriceWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Giá</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div id="checkbox-container">                                      
                                                            <div>
                                                                <label style="font-weight:bold"><input type="checkbox" id="chonruler" name="check" value="Chọn"/> Chọn Range lọc</label> 
                                                                <input id="filterPostPriceInput" type="text" class="form-control" />
                                                                <p></p>
                                                                <a id="textLX" style='color:blue'><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thêm bộ lọc <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <a id="textLXAN" style="color:blue;display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ẩn <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <div id="BoLocLX" style="display:none;">
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb10100" name="check" value="Từ 10 đến 100"/> Từ 10 - 100 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb100500" name="check" value="Từ 100 đến 500" /> Từ 100 - 500 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb500" name="check" value="Từ 500 trở lên" /> 500 triệu trở lên</label>
                                                                </div>
                                                                <div style="margin-top:9px;">
                                                                         <input type="button" id="btn" value="Áp dụng Giá" class="btn btn-success btn-flat"/>
                                                                        <p id="messagex" style="color:#ffba00;padding-top:5px"></p>
                                                                    </div>
                                                                <input type="hidden" id="txttest" />
                                                                <input type="hidden" id="txtRangeLuotXem" runat="server" />
                                                                <input type="hidden" id="ruler" />
                                                                    <input type="hidden" id="rulersv" runat="server" />
                                         <script language="javascript">
                                            document.getElementById('btn').onclick = function()
                                            {
                                                document.getElementById('messagex').innerText = "Đã áp dụng!";
                                                // Khai báo tham số
                                                var checkbox = document.getElementsByName('check');
                                                var result = "";
                 
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkbox.length; i++){
                                                    if (checkbox[i].checked === true){
                                                        result += '' + checkbox[i].value + '';
                                                    }
                                                }                                              
                                                document.getElementById('txttest').value = result;
                                                document.getElementById('<%= txtRangeLuotXem.ClientID %>').value = document.getElementById('txttest').value;
                                                if(document.getElementById('chonruler').checked)
                                                {
                                                    document.getElementById('ruler').value = document.getElementById('chonruler').value;
                                                    document.getElementById('<%= rulersv.ClientID %>').value = document.getElementById('ruler').value;
                                                }
                                                
                                            };
                                        </script>   
                                        <script>
                                               document.getElementById('textLX').onclick = function () {
                                                   document.getElementById("BoLocLX").style.display = "";
                                                   document.getElementById("textLX").style.display = "none";
                                                   document.getElementById("textLXAN").style.display = "";
                                               }
                                               document.getElementById("textLXAN").onclick = function () {
                                                   document.getElementById("textLX").style.display = "";
                                                   document.getElementById("BoLocLX").style.display = "none";
                                                   document.getElementById("textLXAN").style.display = "none";
                                               };
                                               </script>   
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterPostCommissionWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Hoa hồng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                             <div id="checkbox-container1">                                      
                                                            <div>
                                                                <label style="font-weight:bold"><input type="checkbox" id="chonrulerhh" name="checkhh" value="Chọn"/> Chọn Range lọc</label> 
                                                                <input id="filterPostCommissionInput" type="text" class="form-control" />
                                                                <p></p>
                                                                <a id="textHH" style="color:blue"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thêm bộ lọc <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <a id="textHHAN" style="color:blue;display:none"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Ẩn <i class="fa fa-angle-double-left" aria-hidden="true"></i></a>
                                                                <div id="BoLocHH" style="display:none;">
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb10100hh" name="checkhh" value="Từ 10 đến 100"/> Từ 10 - 100 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb100500hh" name="checkhh" value="Từ 100 đến 500" /> Từ 100 - 500 triệu</label>
                                                                <label class="col-12" style="font-weight:bold"><input type="checkbox" id="ckb500hh" name="checkhh" value="Từ 500 trở lên" /> 500 triệu trở lên</label>
                                                                </div>
                                                                <div style="margin-top:9px;">
                                                                         <input type="button" id="btnhh" value="Áp dụng Hoa Hồng" class="btn btn-success btn-flat"/>
                                                                        <p id="messagexhh" style="color:#ffba00;padding-top:5px"></p>
                                                                    </div>
                                                                <input type="hidden" id="txttesthh" />
                                                                <input type="hidden" id="txtRangeHoaHong" runat="server" />
                                                                <input type="hidden" id="rulerhh" />
                                                                <input type="hidden" id="rulersvhh" runat="server" />
                                         <script language="javascript">                                            
                                            document.getElementById('btnhh').onclick = function()
                                            {
                                                document.getElementById('messagexhh').innerText = "Đã áp dụng!";
                                                // Khai báo tham số
                                                var checkboxhh = document.getElementsByName('checkhh');
                                                var resulthh = "";
                 
                                                // Lặp qua từng checkbox để lấy giá trị
                                                for (var i = 0; i < checkboxhh.length; i++){
                                                    if (checkboxhh[i].checked === true){
                                                        resulthh += '' + checkboxhh[i].value + '';
                                                    }
                                                }                                                                                              
                                                document.getElementById('txttesthh').value = resulthh;
                                                document.getElementById('<%= txtRangeHoaHong.ClientID %>').value = document.getElementById('txttesthh').value;
                                                if(document.getElementById('chonrulerhh').checked)
                                                {
                                                    document.getElementById('rulerhh').value = document.getElementById('chonrulerhh').value;
                                                    document.getElementById('<%= rulersvhh.ClientID %>').value = document.getElementById('rulerhh').value;
                                                }
                                            };                                        
                                             function chonLuotXemchh(obj) {

                                                 window.onload = function () {
                                                     //Giá
                                                     if (sessionStorage.getItem('select') == "select") {
                                                         return;
                                                     }
                                                     if (sessionStorage.getItem('luotxem') == "luotxem") {
                                                         return;
                                                     } 
                                                     if (sessionStorage.getItem('rangegia') == "rangegia") {
                                                         return;
                                                     }

                                                     var name = sessionStorage.getItem('select');
                                                     if (name !== null) $('#filterPostPriceInput').val(name);

                                                     var range = sessionStorage.getItem('luotxem');
                                                     if (range !== null) $('#txttest').val(range);

                                                     var rangeg = sessionStorage.getItem('rangegia');
                                                     if (rangeg !== null) $('#ruler').val(rangeg);
                                                     //Hoa Hồng
                                                     if (sessionStorage.getItem('selecthh') == "selecthh") {
                                                         return;
                                                     }
                                                     if (sessionStorage.getItem('luotxemhh') == "luotxemhh") {
                                                         return;
                                                     }
                                                     if (sessionStorage.getItem('rangegiahh') == "rangegiahh") {
                                                         return;
                                                     }

                                                     var rangeghh = sessionStorage.getItem('rangegiahh');
                                                     if (rangeghh !== null) $('#rulerhh').val(rangeghh);

                                                     var namehh = sessionStorage.getItem('selecthh');
                                                     if (namehh !== null) $('#filterPostCommissionInput').val(namehh);

                                                     var rangehh = sessionStorage.getItem('luotxemhh');
                                                     if (rangehh !== null) $('#txttesthh').val(rangehh);               

                                                 }
                                                 window.onbeforeunload = function () {
                                                     //Giá
                                                     sessionStorage.setItem("select", $('#filterPostPriceInput').val());
                                                     sessionStorage.setItem("luotxem", $('#txttest').val());
                                                     sessionStorage.setItem("rangegia", $('#ruler').val());
                                                     //Hoa Hồng
                                                     sessionStorage.setItem("selecthh", $('#filterPostCommissionInput').val());
                                                     sessionStorage.setItem("luotxemhh", $('#txttesthh').val());
                                                     sessionStorage.setItem("rangegiahh", $('#rulerhh').val());
                                                 }
                                             }    
                                        </script>   
                                        <script>
                                               document.getElementById('textHH').onclick = function () {
                                                   document.getElementById("BoLocHH").style.display = "";
                                                   document.getElementById("textHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "";
                                               }
                                               document.getElementById("textHHAN").onclick = function () {
                                                   document.getElementById("textHH").style.display = "";
                                                   document.getElementById("BoLocHH").style.display = "none";
                                                   document.getElementById("textHHAN").style.display = "none";
                                               };
                                               </script>   
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterCreatedAtWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Ngày duyệt</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                                <input id="filterCreatedAtFromInput" type="text" class="form-control"/>
										                        <span class="input-group-addon"> đến </span>
										                        <input id="filterCreatedAtToInput" type="text" class="form-control"/>
									                        </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                  <div id="filterCreatedUp" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Ngày đăng</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                                <input id="filterCreatedUpFromInput" type="text" class="form-control"/>
										                        <span class="input-group-addon"> đến </span>
										                        <input id="filterCreatedUpToInput" type="text" class="form-control"/>
									                        </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div id="filterCreatedPost" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Ngày gửi duyệt</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                                <input id="filterCreatedPostFromInput" type="text" class="form-control"/>
										                        <span class="input-group-addon"> đến </span>
										                        <input id="filterCreatedPostToInput" type="text" class="form-control"/>
									                        </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterCreatedDenied" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Ngày từ chối</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <div class="input-group input-large date-picker input-daterange" data-date-format="dd/mm/yyyy">
                                                                <input id="filterCreatedDeniedFromInput" type="text" class="form-control"/>
										                        <span class="input-group-addon"> đến </span>
										                        <input id="filterCreatedDeniedToInput" type="text" class="form-control"/>
									                        </div>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterCategoryWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Danh mục</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterCategory1" class="form-control"></select>
                                                            <select id="filterCategory2" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterAddressWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Khu vực</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterCity" class="form-control"></select>
                                                            <select id="filterDistrict" class="form-control"></select>
                                                            <select id="filterWard" class="form-control"></select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                 <div id="filterReferralStatusWrap" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Trạng thái</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterReferralStatusInput" class="form-control" style="padding: 5px 10px;">

                                                            </select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterReferralStatusWrapOther2" class="filter-row">
                                                    <div class="row mb-2">
                                                        <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                            <label>Trạng thái</label>
                                                        </div>
                                                        <div class="col col-md-5">
                                                            <select id="filterReferralStatusInputOther2" class="form-control" style="padding: 5px 10px;">

                                                            </select>
                                                        </div>
                                                        <div class="col-auto col-md-1 d-flex justify-content-start align-items-center">
                                                            <button type="button" class="btn btn-danger btn-sm filter-minus-btn" style="padding: 0px 5px;"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="filterAddWrap" class="filter-row last">
                                                    <div class="row mb-2">
                                                            <div class="col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
                                                                <button type="button" class="btn btn-primary btn-sm filter-add-btn" style="padding: 0px 5px;"><i class="fa fa-plus"></i></button>
                                                            </div>
                                                            <div class="col-md-5">
                                                                
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="filter-bar d-flex flex-row mt-3">
                                                <div class="filter-bar-pagination flex-grow-1 d-flex justify-content-start align-items-center">
                                                        <label class="text-sm">Phân trang:&nbsp;&nbsp;</label>                                                    	
                                                    <div id="divAccountPostPagination2" class="app-table-pagination a"  runat="server"></div>
                                                    <div id="divAccountPostPagination4" class="app-table-pagination b"  runat="server"></div>
                                                    <div id="divAccountPostPagination6" class="app-table-pagination c"  runat="server"></div>
                                                    <div id="divAccountPostPagination8" class="app-table-pagination d"  runat="server"></div> 
                                                    <div id="divAccountPostPagination10" class="app-table-pagination e" runat="server"></div>                                                   
                                                </div>
                                                <div class="filter-bar-buttons flex-grow-0" id="btnLoc" runat="server">
                                                    <button id="btnFilterSubmit" type="button" class="btn btn-primary text-sm">Lọc tin</button>
                                                    <button id="btnFilterCancel" type="button" class="btn btn-danger text-sm" style="display: none;">Hủy lọc tin</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-scrollable app-table-wrap mt-5" id="divAccountPostTable2" runat="server">                                   
                                </div>
                                <div class="table-scrollable app-table-wrap mt-5"  id="divAccountPostTable" runat="server">                                    
                                </div>                                
                                <div class="table-scrollable app-table-wrap mt-5"  id="divAccountPostTable3" runat="server">                                    
                                </div>
                                <div class="table-scrollable app-table-wrap mt-5"  id="divAccountPostTable4" runat="server">
                                </div>
                                 <div class="table-scrollable app-table-wrap mt-5" id="divAccountPostTable6" runat="server">                                    
                                </div>                                                                                                     
                                <script>  
                                    $('#postTable').dataTable( {
                                        "bInfo": false,
                                        "paging": false,            
                                        "bPaginate": false,
                                        "searching": false,
                                        "bLengthChange": false,
                                        "targets": 'no-sort',
                                        "bSort": false,
                                        "order": [],
                                    } );                            
                                    $('#postTable2').dataTable( {
                                        "bInfo": false,
                                        "paging": false,            
                                        "bPaginate": false,
                                        "searching": false,
                                        "bLengthChange": false,
                                        "targets": 'no-sort',
                                        "bSort": false,
                                        "order": [],
                                    } ); 
                                    $('#postTable3').dataTable( {
                                        "bInfo": false,
                                        "paging": false,            
                                        "bPaginate": false,
                                        "searching": false,
                                        "bLengthChange": false,
                                        "targets": 'no-sort',
                                        "bSort": false,
                                        "order": [],
                                    } ); 
                                    $('#postTable4').dataTable( {
                                        "bInfo": false,
                                        "paging": false,            
                                        "bPaginate": false,
                                        "searching": false,
                                        "bLengthChange": false,
                                        "targets": 'no-sort',
                                        "bSort": false,
                                        "order": [],
                                    } ); 
                                </script>
                               <div id="divAccountPostPagination1" class="app-table-pagination a"  runat="server"></div>
                                <div id="divAccountPostPagination3" class="app-table-pagination b"  runat="server"></div>
                                <div id="divAccountPostPagination5" class="app-table-pagination c"  runat="server"></div>
                                <div id="divAccountPostPagination7" class="app-table-pagination d"  runat="server"></div>
                                 <div id="divAccountPostPagination9" class="app-table-pagination d" runat="server"></div>
                                <input type="hidden" id="ipPagedd" runat="server" />
                                <input type="hidden" id="ipPagecd" runat="server" />
                                <input type="hidden" id="ipPagebtc" runat="server" />
                                <input type="hidden" id="ipPagethh" runat="server" />
                                <input type="hidden" id="ipkt" />
                                <input type="hidden" id="ipkttab" />
                                <input type="hidden" id="ipktsv" runat="server"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalSelectFilter" class="modal new-modal modal-sm" style="overflow: hidden;">
        <ul class="list-group" style="margin-bottom: 0;">
            <a href="javascript:void(0);" data-target="filterPostCodeWrap" class="list-group-item filter-anchor">Mã bài đăng</a>
            <a href="javascript:void(0);" id="ac" style="display:none;" data-target="filterPostOwnerWrap" class="list-group-item filter-anchor">Mã CTV</a>
            <a href="javascript:void(0);" data-target="filterPostTitleWrap" class="list-group-item filter-anchor">Tiêu đề bài đăng</a>
            <a href="javascript:void(0);" data-target="filterPostPriceWrap" class="list-group-item filter-anchor">Giá</a>
            <a href="javascript:void(0);" data-target="filterPostCommissionWrap" class="list-group-item filter-anchor">Hoa hồng</a>
            <a href="javascript:void(0);" id="nd" data-target="filterCreatedAtWrap" class="list-group-item filter-anchor">Ngày duyệt</a>
            <a href="javascript:void(0);" id="ndb" style="display:none" data-target="filterCreatedUp" class="list-group-item filter-anchor">Ngày đăng</a>
            <a href="javascript:void(0);" id="ngd" data-target="filterCreatedPost" style="display:none" class="list-group-item filter-anchor">Ngày gửi duyệt</a>
            <a href="javascript:void(0);" id="ntc" data-target="filterCreatedDenied" style="display:none" class="list-group-item filter-anchor">Ngày từ chối</a>
            <a href="javascript:void(0);" data-target="filterCategoryWrap" class="list-group-item filter-anchor">Danh mục</a>
            <a href="javascript:void(0);" data-target="filterAddressWrap" class="list-group-item filter-anchor">Khu vực</a>
            <a href="javascript:void(0);" id="tt" style="display:none" data-status="1" class="list-group-item filter-anchor">Trạng thái</a>
          <%--   <a href="javascript:void(0);" id="ctv" style="display:none" data-status="1" class="list-group-item filter-anchor">Trạng thái</a>--%>
          </ul>
    </div>
    <div id="modalAccountInfo" class="modal new-modal" style="overflow: hidden;">
        <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">
            Thông tin Người đăng bài
        </div>
        <div class="mt-4">
            <div class="row">
                <div class="col-md-3 profile-col">
                    <p>
                        <img class="modal-account-thumb mt-3" id="modalAccountInfo_imgThumbnail" src="" />
                    </p>
                </div>
                <div class="col-md-9 app-grid">
                    <div class="row">
                        <div class="col-md-12">
                            <p><label>Họ tên: </label> <span id="modalAccountInfo_spanFullName">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Mã NĐB: </label> <span id="modalAccountInfo_spanCode">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Số điện thoại: </label> <a id="modalAccountInfo_linkPhone" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Email: </label> <a id="modalAccountInfo_linkEmail" href="javascript:void(0);">(Chưa có thông tin)</a></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Địa chỉ: </label> <span id="modalAccountInfo_spanAddr">(Chưa có thông tin)</span></p>
                        </div>
                        <div class="col-md-12">
                            <p><label>Đăng ký lúc: </label> <span id="modalAccountInfo_spanRegisterAt">(Chưa có thông tin)</span></p>
                        </div>
					</div>
                </div>
			</div>
        </div>
    </div>
    <div class="modal-ThongKe-cls">
        <div id="modalThongKe" class="modal" style="overflow: hidden;">
            <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">Thống kê tin đăng</div>
            <div id="dvDSThongKe" style="overflow: hidden; padding: 10px;">
            </div>
        </div>
    </div>
    <div id="modalApproveCollab" class="new-modal modal" style="overflow: hidden;">
        <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">
            Xác nhận hoa hồng cho CTV
        </div>
        <div style="margin-top: 10px;">
            <input id="modalAprrove_postId" type="hidden" />
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-12">
                    <p><strong>Bài viết:</strong> <a id="modalApprove_postTitle"></a></p>
                    <p><strong>Mã bài viết:</strong> <span id="modalApprove_postCode"></span></p>
                    <p><strong>Ngày đăng:</strong> <span id="modalApprove_postCreatedAt"></span></p>
                    <p><strong>Giá:</strong> <span id="modalApprove_postPrice"></span></p>
                </div>
            </div>
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">CTV đã cộng tác</label>
                </div>
                <div class="col-md-12 col-12">
                    <div class="row">
                        <div class="col-md-12 col-12 mb-2">
                            <asp:DropDownList ID="drdlModalAccountSelled" CssClass="form-control" runat="server" style="width: 100%"></asp:DropDownList>
                        </div>
                        <div class="col-md-12 col-12">
                            <div id="modalAprrove_detailCollab" style="padding-left: 15px;">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">Tỉ lệ hoa hồng (%)</label>
                </div>
                <div class="col-md-5 col-12">
                    <div class="input-group main-gr">
                        <input id="modalApprove_postRate" class="form-control" type="text" step="1" min="0" />
						<span class="input-group-addon">
							%
						</span>
					</div>
                    <div class="layer-wartermark"></div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">Hoa hồng cố định (vnđ)</label>
                </div>
                <div class="col-md-5 col-12">
                    <div class="input-group main-gr">
                        <input id="modalApprove_postAmount" class="form-control" type="text" step="1" min="0" />
						<span class="input-group-addon">
							VNĐ
						</span>
					</div>
                    <div class="layer-wartermark"></div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">Thành tiền (vnđ)</label>
                </div>
                <div class="col-md-5 col-12">
                    <input id="modalApprove_postCommission" class="form-control" type="text" readonly step="1" min="0" />
                </div>
            </div>
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                        
                </div>
                <div class="col-md-5 col-12">
                    <button id="btnApproveCollab" type="button" class="btn btn-primary">Xác nhận hoa hồng</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalUpdateReferral" class="new-modal modal" style="overflow: hidden;">
        <div style="background-color: #00000012; font-weight: bold; text-align: center; padding: 10px;">
            Hoa hồng của Cộng tác viên
        </div>
        <div style="margin-top: 10px;">
            <input id="modalUpdateReferral_referralId" type="hidden" />
            <input id="modalUpdateReferral_collabId" type="hidden" />
            <input id="modalUpdateReferral_postId" type="hidden" />
            <div class="row" style="margin-bottom: 8px;">
                <div class="col-md-12">
                    <p><strong>Bài viết:</strong> <a id="modalUpdateReferral_postTitle"></a></p>
                    <p><strong>Mã bài viết:</strong> <span id="modalUpdateReferral_postCode"></span></p>
                    <p><strong>Ngày đăng:</strong> <span id="modalUpdateReferral_postCreatedAt"></span></p>
                    <p><strong>Giá:</strong> <span id="modalUpdateReferral_postPrice"></span></p>
                    <p><strong>Hoa hồng:</strong> <span id="modalUpdateReferral_referralCommission"></span></p>
                    <div id="modalUpdateReferral_detailCollab"></div>
                </div>
            </div>
            <div class="row wrap-readonly" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">Tỉ lệ hoa hồng (%)</label>
                </div>
                <div class="col-md-5 col-12">
                    <div class="input-group main-gr">
                        <input id="modalUpdateReferral_referralRate" class="form-control" type="text" step="1" min="0" />
						<span class="input-group-addon">
							%
						</span>
					</div>
                    <div class="layer-wartermark"></div>
                </div>
            </div>
            <div class="row wrap-readonly" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">Hoa hồng cố định (vnđ)</label>
                </div>
                <div class="col-md-5 col-12">
                    <div class="input-group main-gr">
                        <input id="modalUpdateReferral_referralAmount" class="form-control" type="text" step="1" min="0" />
						<span class="input-group-addon">
							VNĐ
						</span>
					</div>
                    <div class="layer-wartermark"></div>
                </div>
            </div>
            <div class="row wrap-readonly" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                    <label class="control-label">Thành tiền (vnđ)</label>
                </div>
                <div class="col-md-5 col-12">
                    <input id="modalUpdateReferral_referralCommissionInput" class="form-control" type="text" readonly step="1" min="0" />
                </div>
            </div>
            <div class="row wrap-readonly" style="margin-bottom: 8px;">
                <div class="col-md-5 col-12">
                        
                </div>
                <div class="col-md-5 col-12">
                    <button id="btnUpdateReferral" type="button" class="btn btn-primary">Xác nhận hoa hồng</button>
                </div>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtUpgradePackage" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtUpgradePeriod" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtUpgradeDays" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtUpgradeId" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtUpgradeRedirect" runat="server" type="hidden"></asp:TextBox>
    <asp:Button ID="btnUpgradeSubmit" runat="server" Style="display: none"  OnClick="btnUpgradeSubmit_Click" />
    <div id="modalUpgradePostPackage" class="modal new-modal modal-choose-post-package">
        <h1 class="title">NÂNG CẤP TIN ĐĂNG</h1>
        <div class="row mb-2">
            <div class="col-md-3">
                <label>Gói tin</label>
            </div>
            <div class="col col-md-9">
                <select class="form-control select-package">
                </select>
                <p class="text-success mt-2">Chọn <strong>gói tin</strong> thích hợp giúp cho bài tin của bạn được hiển thị tốt hơn trên nền tảng <strong>tungtang.com.vn</strong></p>
            </div>
        </div>
        <div class="row mb-2 period-wrap hide">
            <div class="col-md-3">
                <label>Thời hạn</label>
            </div>
            <div class="col col-md-9">
                <select class="form-control select-period"></select>
            </div>
        </div>
        <div class="row mb-2 dates-wrap hide">
            <div class="col-md-3">
            </div>
            <div class="col col-md-9">
                <div class="input-group input-large input-date-picker input-daterange " data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control input-date-from"/>
					<span class="input-group-addon"> đến </span>
					<input type="text" class="form-control input-date-to"/>
				</div>
                <p class="text-success mt-2">Sau khi hết thời hạn, tin đăng vẫn sẽ được hiển thị và trở thành loại tin đăng bình thường</p>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-3">
            </div>
            <div class="col col-md-9 submit-wrap">
                <button type="button" class="btn btn-primary submit-btn">Hoàn thành</button>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtExtendPackage" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtExtendPeriod" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtExtendDays" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtExtendId" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtExtendRedirect" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtExtendTimeEnd" runat="server" type="hidden"></asp:TextBox>
    <asp:Button ID="btnExtendSubmit" runat="server" Style="display: none"  OnClick="btnExtendSubmit_Click" />
    <div id="modalExtendPostPackage" class="modal new-modal modal-choose-post-package">
        <h1 class="title">GIA HẠN TIN ĐĂNG</h1>
        <div class="row mb-2">
            <div class="col-md-3">
                <label>Gói tin</label>
            </div>
            <div class="col col-md-9">
                <select class="form-control select-package"></select>
                <p class="text-success mt-2">Chọn <strong>gói tin</strong> thích hợp giúp cho bài tin của bạn được hiển thị tốt hơn trên nền tảng <strong>tungtang.com.vn</strong></p>
            </div>
        </div>
        <div class="row mb-2 period-wrap hide">
            <div class="col-md-3">
                <label>Thời hạn</label>
            </div>
            <div class="col col-md-9">
                <select class="form-control select-period"></select>
            </div>
        </div>
        <div class="row mb-2 dates-wrap hide">
            <div class="col-md-3">
            </div>
            <div class="col col-md-9">
                <div class="input-group input-large input-date-picker input-daterange " data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control input-date-from"/>
					<span class="input-group-addon"> đến </span>
					<input type="text" class="form-control input-date-to"/>
				</div>
                <p class="text-success mt-2">Sau khi hết thời hạn, tin đăng vẫn sẽ được hiển thị và trở thành loại tin đăng bình thường</p>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-3">
            </div>
            <div class="col col-md-9 submit-wrap">
                <button type="button" class="btn btn-primary submit-btn">Hoàn thành</button>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtRenovateId" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtRenovateRedirect" runat="server" type="hidden"></asp:TextBox>
    <asp:Button ID="btnRenovateSubmit" runat="server" Style="display: none" OnClick="btnRenovateSubmit_Click" />


    <script>
        (function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        })();
        (function () {
            $('.filter-add-btn').click(function () {
                var btc = document.getElementById('<%= Tabip.ClientID %>').value;
                if(btc === '2')
                {
                    document.getElementById('ngd').style.display = "";
                    document.getElementById('ntc').style.display = "none";
                    document.getElementById('nd').style.display = "none";
                    document.getElementById('tt').style.display = "none";
                    document.getElementById('ndb').style.display = "none";
                    document.getElementById('ac').style.display = "none";
                }               
                if(btc === '3')
                {
                    document.getElementById('ngd').style.display = "";
                    document.getElementById('ntc').style.display = "";
                    document.getElementById('nd').style.display = "none";
                    document.getElementById('tt').style.display = "none";
                    document.getElementById('ndb').style.display = "none";
                    document.getElementById('ac').style.display = "none";
                }
                if(btc === '1')
                {
                    document.getElementById('ngd').style.display = "none";
                    document.getElementById('ntc').style.display = "none";
                    document.getElementById('nd').style.display = "";
                    document.getElementById('tt').style.display = "none";
                    document.getElementById('ndb').style.display = "";
                    document.getElementById('ac').style.display = "none";

                }
                if(btc === '4')
                {
                    document.getElementById('ngd').style.display = "none";
                    document.getElementById('ntc').style.display = "none";
                    document.getElementById('nd').style.display = "";
                    document.getElementById('tt').style.display = "";
                    document.getElementById('ac').style.display = "";
                    document.getElementById('ndb').style.display = "none";
                }
                $('#modalSelectFilter').modal();
            });
            var fnCheckShowBtnAdd = function () {
                if (
                    $('#filterPostCodeWrap').is(':visible') ||
                    $('#filterPostOwnerWrap').is(':visible') ||
                    $('#filterPostTitleWrap').is(':visible') ||
                    $('#filterPostPriceWrap').is(':visible') ||
                    $('#filterPostCommissionWrap').is(':visible') ||
                    $('#filterCreatedAtWrap').is(':visible') ||
                     $('#filterCreatedUp').is(':visible') ||
                    $('#filterCreatedPost').is(':visible') ||
                    $('#filterReferralStatusWrap').is(':visible') ||
                    $('#filterReferralStatusWrapOther2').is(':visible') ||
                    $('#filterCreatedDenied').is(':visible') ||
                    $('#filterCategoryWrap').is(':visible') ||
                    $('#filterAddressWrap').is(':visible')
                ) {
                    $('#filterAddWrap').show();
                    $('#btnFilterCancel').show();
                } else {
                    $('#filterAddWrap').hide();
                    $('#btnFilterCancel').hide();
                }
            }
            $('.filter-anchor').click(function () {
                document.getElementById('ipkttab').value = 1;
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
                var target = $(this).data('target');
                var dataStatus = $(this).data("status");
                if (dataStatus == '1') {
                    var arrTargets = ['filterReferralStatusWrap', 'filterReferralStatusWrapOther2'];
                    for (var i = 0; i < arrTargets.length; i = i + 1) {
                        if ($("#" + arrTargets[i]).is(":visible") == false) {
                            target = arrTargets[i];
                            break;
                        }
                    }
                }
                if (target != undefined && target != null && target != "") {
                $('#' + target).show();
                if (target == 'filterPostPriceWrap') {
                    console.log("aaa");
                     $('#filterPostPriceInput').ionRangeSlider({
                        min: 0,
                        max: 10000000,
                        type: 'double',
                        step: 100000,                        
                        postfix: "",                        
                        hasGrid: true,
                        prettifyFunc: function(num){                            
                            var le = parseInt(num % 1000);
                            var chan = parseInt(num / 1000);
                            var trieu = parseInt(chan / 1000);
                            var tram = chan % 1000;
                            var text = "";
                            if (chan > 1000 && le < 1000 && trieu % 1000) {
                                if (tram == 0)
                                {
                                    text += " " + trieu + " Triệu ";
                                }
                                else {
                                    text += " " + trieu + " Triệu " + tram + " Nghìn";
                                }                                
                            }
                            else if (trieu / 1000) {
                                text += " " + trieu + " Triệu";
                            }
                            else if (chan < 1000) {
                                text += " " + chan + " Nghìn";
                            }
                            text = text.trim();
                            if (text == "") {
                                text = "0 Nghìn";
                            }
                            return text;
                        },
                    });
                } else if (target == 'filterPostCommissionWrap') {
                    $('#filterPostCommissionInput').ionRangeSlider({
                        min: 0,
                        max: 10000000,
                        type: 'double',
                        step: 100000,                        
                        postfix: "",                        
                        hasGrid: true,
                        prettifyFunc: function(num){                            
                            var le = parseInt(num % 1000);
                            var chan = parseInt(num / 1000);
                            var trieu = parseInt(chan / 1000);
                            var tram = chan % 1000;
                            var text = "";
                            if (chan > 1000 && le < 1000 && trieu % 1000) {
                                if (tram == 0)
                                {
                                    text += " " + trieu + " Triệu ";
                                }
                                else {
                                    text += " " + trieu + " Triệu " + tram + " Nghìn";
                                }                                
                            }
                            else if (trieu / 1000) {
                                text += " " + trieu + " Triệu";
                            }
                            else if (chan < 1000) {
                                text += " " + chan + " Nghìn";
                            }
                            text = text.trim();
                            if (text == "") {
                                text = "0 Nghìn";
                            }
                            return text;
                        },
                    });
                } 
                else if (target == 'filterPostCodeWrap') {
                    $('#filterPostCodeInput').select2();
                }
                }
                $('#modalSelectFilter .close-modal').click();
            });

            $('#filterPostCodeWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterPostCodeInput').val("");
                $('#filterPostCodeWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostOwnerWrap .filter-minus-btn').click(function () {
                $('#filterPostOwnerInput').val("");
                $('#filterPostOwnerWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostTitleWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterPostTitleInput').val("");
                $('#filterPostTitleWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostPriceWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterPostPriceInput').val("");
                $('#filterPostPriceWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterPostCommissionWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterPostPriceInput').val("");
                $('#filterPostCommissionWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterCategoryWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterCategory1').val("");
                $('#filterCategory2').val("");
                $('#filterCategoryWrap').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterAddressWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterCity').val("");
                $('#filterDistrict').val("");
                $('#filterWard').val("");
                $('#filterAddressWrap').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterCreatedAtWrap .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterCreatedAtFromInput').datepicker('setDate', null);
                $('#filterCreatedAtToInput').datepicker('setDate', null);
                $('#filterCreatedAtWrap').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterCreatedUp .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterCreatedUpFromInput').datepicker('setDate', null);
                $('#filterCreatedUpToInput').datepicker('setDate', null);
                $('#filterCreatedUp').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterCreatedPost .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterCreatedPostFromInput').datepicker('setDate', null);
                $('#filterCreatedPostToInput').datepicker('setDate', null);
                $('#filterCreatedPost').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterCreatedDenied .filter-minus-btn').click(function () {
                document.getElementById('ipkttab').value = 0;
                $('#filterCreatedDeniedFromInput').datepicker('setDate', null);
                $('#filterCreatedDeniedToInput').datepicker('setDate', null);
                $('#filterCreatedDenied').hide();
                fnCheckShowBtnAdd();
            });
            $('#filterReferralStatusWrap .filter-minus-btn').click(function(){
                $('#filterReferralStatusInput').val("");
                $('#filterReferralStatusWrap').hide();
                fnCheckShowBtnAdd();
            });

            $('#filterReferralStatusWrapOther2 .filter-minus-btn').click(function () {
                $('#filterReferralStatusInputOther2').val("");
                $('#filterReferralStatusWrapOther2').hide();
                fnCheckShowBtnAdd();
            });
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterCreatedAtFromInput').datepicker('setDate', dayx);
                $('#filterCreatedAtToInput').datepicker('setDate', dayy);
                $('#filterCreatedAtWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterCreatedAtFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterCreatedAtToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterCreatedPostFromInput').datepicker('setDate', dayx);
                $('#filterCreatedPostToInput').datepicker('setDate', dayy);
                $('#filterCreatedPost').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterCreatedPostFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterCreatedPostToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterCreatedUpFromInput').datepicker('setDate', dayx);
                $('#filterCreatedUpToInput').datepicker('setDate', dayy);
                $('#filterCreatedUp').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterCreatedUpFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterCreatedUpToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterCreatedPostFromInput').datepicker('setDate', dayx);
                $('#filterCreatedPostToInput').datepicker('setDate', dayy);
                $('#filterCreatedPost').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterCreatedPostFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterCreatedPostToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
        (function () {
            $('.date-picker').datepicker({
                language: 'vi',
                orientation: "left",
                autoclose: true
            });
            var fnSetRangeDays = function (dayx, dayy) {
                dayx.setHours(0, 0, 0, 0);
                dayy.setHours(0, 0, 0, 0);
                $('#filterCreatedDeniedFromInput').datepicker('setDate', dayx);
                $('#filterCreatedDeniedToInput').datepicker('setDate', dayy);
                $('#filterCreatedDenied').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var dayx = "<%= filterCreatedDeniedFromAltDate %>";
            dayx = dayx.trim();
            var dayy = "<%= filterCreatedDeniedToAltDate %>";
            dayy = dayy.trim();
            if (dayx != "" && dayy != "") {
                try {
                    dayx = new Date(dayx);
                    dayy = new Date(dayy);
                } catch (err) {
                    dayx = "";
                    dayy = "";
                }
                if (dayx != "" && dayy != "") {
                    fnSetRangeDays(dayx, dayy);
                }
            }
        })();
        (function () {
            var templateJsonCategories1 = <%= templateJsonCategories1 %>;
            var templateJsonCategories2 = <%= templateJsonCategories2 %>;

            var templateDefaultOption = "<option value=''>Chọn</option>";

            var selectHtml1 = "";
            templateJsonCategories1.forEach(function (ele1, pos1, arr1) {
                selectHtml1 += "<option value='" + ele1.id1+"'>" + ele1.name1 + "</option>";
            });
            selectHtml1 = templateDefaultOption + selectHtml1;
            $('#filterCategory1').html(selectHtml1);
            $('#filterCategory2').hide();

            $('#filterCategory1').on("change", function () {
                var selectHtml2 = templateDefaultOption;
                var val1 = $('#filterCategory1').val();   
                var countSubs = 0;
                templateJsonCategories2.forEach(function (ele1, pos1, arr1) {
                    if (ele1.id1 == val1) {
                        countSubs++;
                        selectHtml2 += "<option value='" + ele1.id2 + "'>" + ele1.name2+"</option>"
                    }
                });
                $('#filterCategory2').html(selectHtml2);
                $('#filterCategory2').val("");
                if (countSubs > 0) {
                    $('#filterCategory2').show();
                } else {
                    $('#filterCategory2').hide();
                }
            });
        })();
          (function () {
            var template = "";
            var data = <%= templateJsonReferralStatus %>;
           // template += "<option value=''>Tất cả</option>";
            data.forEach(function (ele) {
                template += "<option value='" + ele.value+"'>" + ele.text + "</option>";
            });
            $('#filterReferralStatusInput').html(template);
            $('#filterReferralStatusInput').val("<%= templateFilterReferralStatus %>");
            $('#filterReferralStatusInputOther2').html(template);
            $('#filterReferralStatusInputOther2').val("<%= templateFilterReferralStatusOther2 %>");

            var initFilterCode = "<%= templateFilterCode %>";
            $('#filterCodeInput').val(initFilterCode);
            if(initFilterCode != ""){
                $('#filterCodeWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterReferralStatus = "<%= templateFilterReferralStatus %>";
            $('#filterReferralStatusInput').val(initFilterReferralStatus);
            if(initFilterReferralStatus != ""){
                $('#filterReferralStatusWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterReferralStatusOther2 = "<%= templateFilterReferralStatusOther2 %>";
            $('#filterReferralStatusInputOther2').val(initFilterReferralStatusOther2);
            if (initFilterReferralStatusOther2 != "") {
                $('#filterReferralStatusWrapOther2').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
        })();

           (function (){
            //var checkTaba = document.getElementById('ipkt').value;
            var url_string = window.location;
                var url = new URL(url_string);
                var name = url.searchParams.get("Tab");
                //alert(name);
                //alert('hi' +checkTaba);
                //if (checkTaba == "")
                //{
                //    checkTaba = "1";
                //}
                var templateDefaultOption = "<option value=''>Chọn</option>";
                if (name == "1")
                  {
                      var templateJsonCode = <%= templateJsonCode %>;
                      var selectHtml1 = "";
                      templateJsonCode.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);
                  }
                if (name == "2")
                  {
                      var templateJsonCode2 = <%= templateJsonCode2 %>;
                      var selectHtml1 = "";
                      templateJsonCode2.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);
                }
                if (name == "3")
                  {
                      var templateJsonCode3 = <%= templateJsonCode3 %>;
                      var selectHtml1 = "";
                      templateJsonCode3.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);
                }
               if (name == "4")
               {
                   var templateJsonCode4 = <%= templateJsonCode4 %>;
                   var selectHtml1 = "";
                   templateJsonCode4.forEach(function (ele1, pos1, arr1) {
                       selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                   });
                   selectHtml1 = templateDefaultOption + selectHtml1;
                   $('#filterPostCodeInput').html(selectHtml1);

                  var templateDefaultOption = "<option value=''>Chọn</option>";
                       var templateJsonOwners = <%= templateJsonOwners %>;
                       var selectHtml2 = "";
                       templateJsonOwners.forEach(function (ele1, pos1, arr1) {
                           selectHtml2 += "<option value='" + ele1.id + "'>" + ele1.code + " - " + ele1.phone + " - " + ele1.fullname + "</option>";
                       });
                       selectHtml2 = templateDefaultOption + selectHtml2;
                       $('#filterPostOwnerInput').html(selectHtml2);
               }
        })();
        (function () {
            var defaultCity = "<%= templateFilterCity %>";
            var defaultDistrict = "<%= templateFilterDistrict %>";
            var defaultWard = "<%= templateFilterWard %>";
            var fnInitCities = function () {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLCities";
                var defValue = defaultCity;
                defaultCity = "";
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterCity').html(xmlhttp.responseText);
                        $('#filterCity').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
            var fnInitDistricts = function (cityId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLDistricts";
                var defValue = defaultDistrict;
                defaultDistrict = "";
                if (cityId != undefined && cityId != null && cityId != "") {
                    url += "&CityId=" + cityId;
                } else {
                    cityId = "";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterDistrict').html(xmlhttp.responseText);
                        $('#filterDistrict').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
            var fnInitWards = function (districtId) {
                var url = "/Admin/adAjax.aspx?Action=GetHTMLWards";
                var defValue = defaultWard;
                defaultWard = "";
                if (districtId != undefined && districtId != null && districtId != "") {
                    url += "&DistrictId=" + districtId;
                } else {
                    districtId = "";
                }
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        $('#filterWard').html(xmlhttp.responseText);
                        $('#filterWard').val(defValue).trigger("change");
                    }
                }
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            };
            $('#filterCity').on("change", function () {
                var value = $(this).val();
                fnInitDistricts(value);
            });
            $('#filterDistrict').on("change", function () {
                var value = $(this).val();
                fnInitWards(value);
            });
            fnInitCities();
        })();
        function $_GET(q,s) {
            s = (s) ? s : window.location.search;
            var re = new RegExp('&amp;'+q+'=([^&amp;]*)','i');
            return (s=s.replace(/^\?/,'&amp;').match(re)) ?s=s[1] :s='';
        }
        (function () {
            var fnSlashToDash = function (sSlash) {
                var rs = "";
                if (sSlash != undefined && sSlash != null && sSlash != "") {
                    sSlash = sSlash.trim();
                    var parts = sSlash.split("/");
                    if (parts.length == 3) {
                        return parts[2] + "-" + parts[1] + "-" + parts[0];
                    }
                }
                return rs;
            }
             var checkTaba = document.getElementById('<%= Tabip.ClientID %>').value;               
                //alert(checkTaba);
            var baseUrl = "/thong-tin-ca-nhan/ttcn?Tab=" + checkTaba;
            $("#btnFilterSubmit").click(function () {                
                var filterPostCodeInput = $('#filterPostCodeInput').val();
                var filterPostOwnerInput = $('#filterPostOwnerInput').val();
                var filterPostPriceInput = $('#filterPostPriceInput').val();
                var filterPostCommissionInput = $('#filterPostCommissionInput').val();
                var filterPostTitleInput = $('#filterPostTitleInput').val();
                var filterCreatedAtFromInput = fnSlashToDash($('#filterCreatedAtFromInput').val());
                var filterCreatedAtToInput = fnSlashToDash($('#filterCreatedAtToInput').val());
                var filterCreatedUpFromInput = fnSlashToDash($('#filterCreatedUpFromInput').val());
                var filterCreatedUpToInput = fnSlashToDash($('#filterCreatedUpToInput').val());
                var filterCreatedPostFromInput = fnSlashToDash($('#filterCreatedPostFromInput').val());
                var filterCreatedPostToInput = fnSlashToDash($('#filterCreatedPostToInput').val());
                var filterCreatedDeniedFromInput = fnSlashToDash($('#filterCreatedDeniedFromInput').val());
                var filterCreatedDeniedToInput = fnSlashToDash($('#filterCreatedDeniedToInput').val());
                var filterReferralStatusInput = $('#filterReferralStatusInput').val();
                var filterReferralStatusInputOther2 = $('#filterReferralStatusInputOther2').val();
                var filterCategory1 = $('#filterCategory1').val();
                var filterCategory2 = $('#filterCategory2').val();
                var filterCity = $('#filterCity').val();
                var filterDistrict = $('#filterDistrict').val();
                var filterWard = $('#filterWard').val();
				var txttest = $('#txttest').val();
                var txttesthh = $('#txttesthh').val();
                var txtruler = $('#ruler').val();
                var txtrulerhh = $('#rulerhh').val();
                //var checkTaba = document.getElementById('ipkt').value;
                //var url_string = window.location;
                //var url = new URL(url_string);
                //var name = url.searchParams.get("Tab");
                //alert(name);
                if (checkTaba == "")
                {
                    checkTaba = "1";
                }
                var templateDefaultOption = "<option value=''>Chọn</option>";
                if (checkTaba == "1" || name == "1")
                  {
                      var templateJsonCode = <%= templateJsonCode %>;
                      var selectHtml1 = "";
                      templateJsonCode.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);
                  }
                if (checkTaba == "2" || name == "2")
                  {
                      var templateJsonCode2 = <%= templateJsonCode2 %>;
                      var selectHtml1 = "";
                      templateJsonCode2.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);
                }
                if (checkTaba == "3" || name == "3")
                  {
                      var templateJsonCode3 = <%= templateJsonCode3 %>;
                      var selectHtml1 = "";
                      templateJsonCode3.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);
                }
                if (checkTaba == "4" || name == "4")
                  {
                      var templateJsonCode4 = <%= templateJsonCode4 %>;
                      var selectHtml1 = "";
                      templateJsonCode4.forEach(function (ele1, pos1, arr1) {
                          selectHtml1 += "<option value='" + ele1.id + "'>" + ele1.code + "</option>";
                      });
                      selectHtml1 = templateDefaultOption + selectHtml1;
                      $('#filterPostCodeInput').html(selectHtml1);

                       var templateDefaultOption = "<option value=''>Chọn</option>";
                       var templateJsonOwners = <%= templateJsonOwners %>;
                       var selectHtml2 = "";
                       templateJsonOwners.forEach(function (ele1, pos1, arr1) {
                           selectHtml2 += "<option value='" + ele1.id + "'>" + ele1.code + " - " + ele1.phone + " - " + ele1.fullname + "</option>";
                       });
                       selectHtml2 = templateDefaultOption + selectHtml2;
                       $('#filterPostOwnerInput').html(selectHtml2);
                  }              
                var url = baseUrl;
                var counterParams = 0;
                var counterid = 0;
                var countertitle = 0;
                var counterprice = 0;
                var counterhh = 0;
                var counterdate = 0;
                var counterdm = 0;
                var counterkv = 0;
                if (filterPostCodeInput != undefined && filterPostCodeInput != null && filterPostCodeInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCode=" + filterPostCodeInput;                    
                }
                if (filterPostOwnerInput != undefined && filterPostOwnerInput != null && filterPostOwnerInput != "") {
                    counterParams++;                    
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostOwner=" + filterPostOwnerInput;                    
                }
                if (filterPostTitleInput != undefined && filterPostTitleInput != null && filterPostTitleInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostTitle=" + filterPostTitleInput;                    
                }
                if (filterReferralStatusInput != undefined && filterReferralStatusInput != null && filterReferralStatusInput != "") {
                    counterParams++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterReferralStatus=" + filterReferralStatusInput;
                }
                if (filterReferralStatusInputOther2 != undefined && filterReferralStatusInputOther2 != null && filterReferralStatusInputOther2 != "") {
                    counterParams++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterReferralStatusOther2=" + filterReferralStatusInputOther2;
                }
                //Giá                        
                if (txtruler == "Chọn" && txttest == "Chọn"){
                    if (filterPostPriceInput != undefined && filterPostPriceInput != null && filterPostPriceInput != "") {                    
                        counterParams++;
                        counterid++;
                        //counterParams == 1 ? url += "?" : url += "&";
                        url += "&FilterPostPrice=" + filterPostPriceInput;
                        url += "&Gia=0";                        
                    }
                }                                                                       
                
                if (txttest == 'Từ 10 đến 100' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=10100";
                    url += "&FilterPostPrice=" +0;                         
                }

                if (txttest == 'ChọnTừ 10 đến 100' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100";                    
                }
                if (txttest == 'Từ 100 đến 500' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=100500";
                    url += "&FilterPostPrice=" +0;                     
                }
                if (txttest == 'ChọnTừ 100 đến 500' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=100500";                    
                }
                if (txttest == 'Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=500";
                    url += "&FilterPostPrice=" +0;                     
                }
                if (txttest == 'ChọnTừ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=500";                    
                }
                if (txttest == 'Từ 10 đến 100Từ 100 đến 500' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=10100100500";
                    url += "&FilterPostPrice=" +0;                     
                }
                if (txttest == 'ChọnTừ 10 đến 100Từ 100 đến 500' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100100500";                    
                }
                if (txttest == 'Từ 100 đến 500Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=100500500"; 
                    url += "&FilterPostPrice=" +0;                     
                }
                if (txttest == 'ChọnTừ 100 đến 500Từ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=100500500";                    
                }
                if (txttest == 'Từ 10 đến 100Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=10100500";
                    url += "&FilterPostPrice=" +0;                     
                }
                if (txttest == 'ChọnTừ 10 đến 100Từ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100500";                    
                }
                if (txttest == 'Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtruler == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&Gia=10100100500500";  
                    url += "&FilterPostPrice=" +0;                     
                }
                if (txttest == 'ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtruler == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostPrice=" + filterPostPriceInput;
                    url += "&Gia=10100100500500";                    
                }
                //Hoa Hồng               
                if (txtrulerhh == "Chọn" && txttesthh == "Chọn"){
                    if (filterPostCommissionInput != undefined && filterPostCommissionInput != null && filterPostCommissionInput != "") {
                        counterParams++;
                        counterid++;
                        //counterParams == 1 ? url += "?" : url += "&";
                        url += "&FilterPostCommission=" + filterPostCommissionInput;
                        url += "&HoaHong=0";                        
                    }
                }
                if (txttesthh == 'Từ 10 đến 100' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=10100";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 10 đến 100' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100";                    
                }
                if (txttesthh == 'Từ 100 đến 500' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=100500";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 100 đến 500' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=100500";                    
                }
                if (txttesthh == 'Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=500";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=500";                    
                }
                if (txttesthh == 'Từ 10 đến 100Từ 100 đến 500' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=10100100500";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 10 đến 100Từ 100 đến 500' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100100500";                    
                }
                if (txttesthh == 'Từ 100 đến 500Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=100500500";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 100 đến 500Từ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=100500500";                     
                }
                if (txttesthh == 'Từ 10 đến 100Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=10100500";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 10 đến 100Từ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100500";                    
                }
                if (txttesthh == 'Từ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtrulerhh == '')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&HoaHong=10100100500500";
                    url += "&FilterPostCommission=" +0;                    
                }
                if (txttesthh == 'ChọnTừ 10 đến 100Từ 100 đến 500Từ 500 trở lên' && txtrulerhh == 'Chọn')
                {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterPostCommission=" + filterPostCommissionInput;
                    url += "&HoaHong=10100100500500";                    
                }
                if (filterCreatedAtFromInput != undefined && filterCreatedAtFromInput != null && filterCreatedAtFromInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedAtFrom=" + filterCreatedAtFromInput;                    
                }
                if (filterCreatedAtToInput != undefined && filterCreatedAtToInput != null && filterCreatedAtToInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedAtTo=" + filterCreatedAtToInput;                    
                }
                if (filterCreatedUpFromInput != undefined && filterCreatedUpFromInput != null && filterCreatedUpFromInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedUpFrom=" + filterCreatedUpFromInput;                    
                }
                if (filterCreatedUpToInput != undefined && filterCreatedUpToInput != null && filterCreatedUpToInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedUpTo=" + filterCreatedUpToInput;                    
                }
                if (filterCreatedPostFromInput != undefined && filterCreatedPostFromInput != null && filterCreatedPostFromInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedPostFrom=" + filterCreatedPostFromInput;                    
                }
                if (filterCreatedPostToInput != undefined && filterCreatedPostToInput != null && filterCreatedPostToInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedPostTo=" + filterCreatedPostToInput;                    
                }
                if (filterCreatedDeniedFromInput != undefined && filterCreatedDeniedFromInput != null && filterCreatedDeniedFromInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedDeniedFrom=" + filterCreatedDeniedFromInput;                    
                }
                if (filterCreatedDeniedToInput != undefined && filterCreatedDeniedToInput != null && filterCreatedDeniedToInput != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCreatedDeniedTo=" + filterCreatedDeniedToInput;                    
                }
                if (filterCategory1 != undefined && filterCategory1 != null && filterCategory1 != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCategory1=" + filterCategory1;                    
                }
                if (filterCategory2 != undefined && filterCategory2 != null && filterCategory2 != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCategory2=" + filterCategory2;                    
                }
                if (filterCity != undefined && filterCity != null && filterCity != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterCity=" + filterCity;                    
                }
                if (filterDistrict != undefined && filterDistrict != null && filterDistrict != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterDistrict=" + filterDistrict;                    
                }
                if (filterWard != undefined && filterWard != null && filterWard != "") {
                    counterParams++;
                    counterid++;
                    //counterParams == 1 ? url += "?" : url += "&";
                    url += "&FilterWard=" + filterWard;                    
                }                                
                if (counterParams > 0) {
                    window.location.href = url;                    
                } else {
                    $('.filter-add-btn').click();
                }                             
            });
            $('#btnFilterCancel').click(function () {
                document.getElementById('ipkttab').value = 0;
                window.location.href = baseUrl;
            });
        })();   
        (function () {
            var initFilterCategory1 = "<%= templateFilterCategory1 %>";
            var initFilterCategory2 = "<%= templateFilterCategory2 %>";
            var initFilterPostOwner = "<%= templateFilterPostOwner %>";

            $('#filterCategory1').val(initFilterCategory1).trigger("change");
            $('#filterCategory2').val(initFilterCategory2).trigger("change");
            $('#filterPostOwnerInput').val(initFilterPostOwner)

            if (initFilterCategory1 != "" || initFilterCategory2 != "") {
                $('#filterCategoryWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }

            if (initFilterPostOwner != "") {
                $('#filterPostOwnerWrap').show();
                $('#filterPostOwnerInput').select2();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }

            var defaultCity = "<%= templateFilterCity %>";
            var defaultDistrict = "<%= templateFilterDistrict %>";
            var defaultWard = "<%= templateFilterWard %>";
            if (defaultCity != "" || defaultDistrict != "" || defaultWard != "") {
                $('#filterAddressWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterPostCode = "<%= templateFilterPostCode %>";
            $('#filterPostCodeInput').val(initFilterPostCode)
            if (initFilterPostCode != "") {
                $('#filterPostCodeWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
                $('#filterPostCodeInput').select2();
            }
            var initFilterPostTitle = "<%= templateFilterPostTitle %>";
            $("#filterPostTitleInput").val(initFilterPostTitle);
            if (initFilterPostTitle != "") {
                $('#filterPostTitleWrap').show();
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterPostPrice = "<%= templateFilterPostPrice %>";
            $("#filterPostPriceInput").val(initFilterPostPrice);
            if (initFilterPostPrice != "") {
                $('#filterPostPriceWrap').show();
               $('#filterPostPriceInput').ionRangeSlider({
                    min: 0,
                    max: 10000000,
                    type: 'double',
                    step: 100000,                        
                    postfix: "",                        
                    hasGrid: true,
                    prettifyFunc: function(num){                            
                        var le = parseInt(num % 1000);
                        var chan = parseInt(num / 1000);
                        var trieu = parseInt(chan / 1000);
                        var tram = chan % 1000;
                        var text = "";
                        if (chan > 1000 && le < 1000 && trieu % 1000) {
                            if (tram == 0)
                            {
                                text += " " + trieu + " Triệu ";
                            }
                            else {
                                text += " " + trieu + " Triệu " + tram + " Nghìn";
                            }                                
                        }
                        else if (trieu / 1000) {
                            text += " " + trieu + " Triệu";
                        }
                        else if (chan < 1000) {
                            text += " " + chan + " Nghìn";
                        }
                        text = text.trim();
                        if (text == "") {
                            text = "0 Nghìn";
                        }
                        return text;
                    },
                });
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
            var initFilterPostCommission = "<%= templateFilterPostCommission %>";
            $("#filterPostCommissionInput").val(initFilterPostCommission);
            if (initFilterPostCommission != "") {
                $('#filterPostCommissionWrap').show();
                $('#filterPostCommissionInput').ionRangeSlider({
                    min: 0,
                    max: 10000000,
                    type: 'double',
                    step: 100000,                        
                    postfix: "",                        
                    hasGrid: true,
                    prettifyFunc: function(num){                            
                        var le = parseInt(num % 1000);
                        var chan = parseInt(num / 1000);
                        var trieu = parseInt(chan / 1000);
                        var tram = chan % 1000;
                        var text = "";
                        if (chan > 1000 && le < 1000 && trieu % 1000) {
                            if (tram == 0)
                            {
                                text += " " + trieu + " Triệu ";
                            }
                            else {
                                text += " " + trieu + " Triệu " + tram + " Nghìn";
                            }                                
                        }
                        else if (trieu / 1000) {
                            text += " " + trieu + " Triệu";
                        }
                        else if (chan < 1000) {
                            text += " " + chan + " Nghìn";
                        }
                        text = text.trim();
                        if (text == "") {
                            text = "0 Nghìn";
                        }
                        return text;
                    },
                });
                $('#filterAddWrap').show();
                $('#btnFilterCancel').show();
            }
        })();
        (function () {
            $('.user-info-btn').click(function () {
                var dataAccountId = $(this).data("account-id");
                var dataThumbnail = $(this).data("thumbnail");
                var dataFullname = $(this).data("fullname");
                var dataCode = $(this).data("code");
                var dataPhone = $(this).data("phone");
                var dataEmail = $(this).data("email");
                var dataAddr = $(this).data("addr");
                var dataRegisterAt = $(this).data("register-at");
                if (dataAccountId == undefined || dataAccountId == null || dataAccountId == "") {
                    return;
                }
                $('#modalAccountInfo_imgThumbnail').attr("src", (dataThumbnail != "" ? dataThumbnail : "#"));
                $('#modalAccountInfo_spanFullName').text((dataFullname != "" ? dataFullname : "(Chưa có thông tin)"));
                $('#modalAccountInfo_spanCode').text((dataCode != "" ? dataCode : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkPhone').text((dataPhone != "" ? dataPhone : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkPhone').attr("href", (dataPhone != "" ? ("tel:" + dataPhone) : "javascript:void(0);"));
                $('#modalAccountInfo_linkEmail').text((dataEmail != "" ? dataEmail : "(Chưa có thông tin)"));
                $('#modalAccountInfo_linkEmail').attr("href", (dataEmail != "" ? "mailto:" + dataEmail : "javascript:void(0);"));
                $('#modalAccountInfo_spanAddr').text((dataAddr != "" ? dataAddr : "(Chưa có thông tin)"));
                $('#modalAccountInfo_spanRegisterAt').text((dataRegisterAt != "" ? dataRegisterAt : "(Chưa có thông tin)"));
                $('#modalAccountInfo').modal();
            });
        })();
    </script>
	
    <script>
        function thongbao() {
            alert("Tin của bạn đã ngưng hoạt động");
        }
        $(document).ready(function () {
            $('.table.table-bordered').DataTable();
        });
        function showSliderValue() {
            var rangeSlider = document.getElementById("rs-range-line");
            var rangeBullet = document.getElementById("rs-bullet");
            var rangeBullet2 = document.getElementById("rs-bullet2");
            var bulletPosition = (rangeBullet2.innerHTML / rangeSlider.max);
            if (bulletPosition == 1) {
                $(".ArrowBullet").attr("style", "left:75%;");
                rangeBullet.style.right = "0%";
            }
            else
                rangeBullet.style.left = (bulletPosition * 100) + "%";

            if (rangeBullet2.innerHTML == "1") {
                $(".ArrowBullet").attr("style", "left:15%;");
                rangeBullet.style.left = "12%";
            }
        }
        $(document).ready(function () {
            // Variables
            var clickedTab = $(".tabs > .active");
            var tabWrapper = $(".tab__content");
            var activeTab = tabWrapper.find(".active");
            var activeTabHeight = activeTab.outerHeight();

            // Show tab on page load
            activeTab.show();

            // Set height of wrapper on page load
            tabWrapper.height(activeTabHeight);

            $(".tabs > li").on("click", function () {
                
                // Remove class from active tab
                $(".tabs > li").removeClass("active");

                // Add class active to clicked tab
                $(this).addClass("active");

                // Update clickedTab variable
                clickedTab = $(".tabs .active");

                // fade out active tab
                activeTab.fadeOut(250, function () {
                    
                    // Remove active class all tabs
                    $(".tab__content > li").removeClass("active");

                    // Get index of clicked tab
                    var clickedTabIndex = clickedTab.index();

                    // Add class active to corresponding tab
                    $(".tab__content > li").eq(clickedTabIndex).addClass("active");

                    // update new active tab
                    activeTab = $(".tab__content > .active");

                    // Update variable
                    activeTabHeight = activeTab.outerHeight();

                    // Animate height of wrapper to new tab height
                    tabWrapper.stop().delay(50).animate({
                        height:activeTabHeight
                    }, 500, function () {

                        // Fade in active tab
                        activeTab.delay(50).fadeIn(250);
                       
                    });
                    
                });

                
            });

            $('.table.table-active > tbody tr.data-row').each(function (index) {
                $(this).children('td:nth-child(1)').html(index + 1);
            });
            $('.table.table-pending > tbody tr.data-row').each(function (index) {
                $(this).children('td:nth-child(1)').html(index + 1)
            });
            $('.table.table-denied > tbody tr.data-row').each(function (index) {
                $(this).children('td:nth-child(1)').html(index + 1)
            });
            $('.table.table-completed > tbody tr.data-row').each(function (index) {
                $(this).children('td:nth-child(1)').html(index + 1)
            });

        });
        function CapNhatHetHan2(idTinDang) {
            var HetHan = document.getElementById("slHetHan_" + idTinDang);
            if (HetHan.disabled == true) {
                document.getElementById("slHetHan_" + idTinDang).disabled = false;
                document.getElementById("btHetHan_" + idTinDang).innerHTML = "<img class='imgedit' src='../images/icons/save.png' style='width:25px; cursor:pointer'/>Lưu</a>";
            } else {
                var xmlhttp;
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                }
                else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if (xmlhttp.responseText == "True")
                            window.location.reload();
                        else
                            alert("Lỗi !")
                    }
                }
                xmlhttp.open("GET", "../Ajax.aspx?Action=UpdatePostStatusUser&IdTinDang=" + idTinDang + "&Status=" + HetHan.value, true);
                xmlhttp.send();
            }
        }
        // Modal approve collab
        (function () {
            var itemPrice = 0;
            var fnFormatCurrency = function (amount) {
                amount = parseInt(amount);
                if (isNaN(amount)) {
                    amount = 0;
                }
                amount = '' + amount;
                var rs = "";
                do {
                    var t = amount.substr(-3);
                    if (t.length == 3 && amount.length > 3) {
                        rs = "," + t + rs;
                    } else {
                        rs = t + rs;
                    }
                    amount = amount.substr(0, amount.length - t.length)
                } while (amount.length > 0);
                return rs;
            };
            var fnCheckInput = function () {
                var valRate = $("#modalApprove_postRate").val().replaceAll(" ", "").replaceAll(",", "");
                valRate = parseInt(valRate);
                if (isNaN(valRate)) {
                    valRate = 0;
                } else {
                    if (valRate < 0) {
                        valRate = 0;
                    }
                }
                $('#modalApprove_postRate').val(valRate);
                var valAmount = $("#modalApprove_postAmount").val().replaceAll(" ", "").replaceAll(",", "");
                valAmount = parseInt(valAmount);
                if (isNaN(valAmount)) {
                    valAmount = 0;
                } else {
                    if (valAmount < 0) {
                        valAmount = 0;
                    }
                }
                $('#modalApprove_postAmount').val(valAmount);
            }
            var fnCalCommission = function () {
                fnCheckInput();
                var rate = parseInt($('#modalApprove_postRate').val());
                var amount = parseInt($('#modalApprove_postAmount').val().replaceAll(" ", "").replaceAll(",", ""));
                var commission = (rate * itemPrice / 100) + amount;
                commission = parseInt(commission);
                $('#modalApprove_postCommission').val(fnFormatCurrency("" + commission));
            }
            $('#modalApprove_postRate').on("change", function () {
                fnCalCommission();
            });
            $('#modalApprove_postAmount').on("change", function () {
                fnCalCommission();
            });
            $('.btn-approve-collab').click(function () {
                $('#modalAprrove_postId').val($(this).data('id'));
                $('#modalApprove_postTitle').text($(this).data('title'));
                $('#modalApprove_postTitle').attr('href', $(this).data('link'));
                $('#modalApprove_postCode').text($(this).data('code'));
                $('#modalApprove_postCreatedAt').text($(this).data('created-at'));
                $('#modalApprove_postPrice').text($(this).data('price-format'));
                $('#modalApprove_postRate').val($(this).data('rate'));
                $('#modalApprove_postAmount').val($(this).data('amount'));
                $('#modalApprove_postCommission').val("0");
                itemPrice = parseInt($(this).data('price'));
                fnCalCommission();
                $('#modalApproveCollab').modal();
                $('#modalApprove_postRate').trigger("change");
                $('#modalApprove_postAmount').trigger("change");
            });
            $(document).ready(function () {
                $('#ContentPlaceHolder1_drdlModalAccountSelled').select2();
            });
            $('#btnApproveCollab').click(function () {
                var postId = $('#modalAprrove_postId').val();
                var postRate = $('#modalApprove_postRate').val();
                var postAmount = $('#modalApprove_postAmount').val();
                var collabId = $('#ContentPlaceHolder1_drdlModalAccountSelled').val();

                $('#GFake_ThongTinCaNhanCopy_ModalApprove_PostId').val(postId);
                $('#GFake_ThongTinCaNhanCopy_ModalApprove_PostRate').val(postRate);
                $('#GFake_ThongTinCaNhanCopy_ModalApprove_PostAmount').val(postAmount);
                $('#GFake_ThongTinCaNhanCopy_ModalApprove_CollabId').val(collabId);
                $('#GFake_ThongTinCaNhanCopy_ModalApprove_Submit').click();
            });
        })();
        (function () {
            var rate = $('#modalApprove_postRate');
            var rateWrap = rate.parents('.row');
            var rateLayer = rateWrap.find('.layer-wartermark');
            var amount = $('#modalApprove_postAmount');
            var amountWrap = amount.parents('.row');
            var amountLayer = amountWrap.find('.layer-wartermark');

            var fnFormatCurrency = function (amount) {
                amount = parseInt(amount);
                if (isNaN(amount)) {
                    amount = 0;
                }
                amount = '' + amount;
                var rs = "";
                do {
                    var t = amount.substr(-3);
                    if (t.length == 3 && amount.length > 3) {
                        rs = "," + t + rs;
                    } else {
                        rs = t + rs;
                    }
                    amount = amount.substr(0, amount.length - t.length)
                } while (amount.length > 0);
                return rs;
            };

            var fnCheck = function () {
                var rateVal = rate.val();
                rateVal = parseFloat(rateVal.replaceAll(" ", "").replaceAll(",", ""));
                var amountVal = amount.val();
                amountVal = parseFloat(amountVal.replaceAll(" ", "").replaceAll(",", ""));

                if (!isNaN(rateVal) && rateVal == 0 && !isNaN(amountVal) && amountVal == 0) {
                    rateLayer.hide();
                    rate.val(0);
                    amountLayer.hide();
                    amount.val(0);
                } else if (!isNaN(rateVal) && rateVal > 0) {
                    rate.val(rateVal);
                    rateLayer.hide();
                    amount.val(0);
                    amountLayer.show();
                } else if (!isNaN(amountVal) && amountVal > 0) {
                    amount.val(fnFormatCurrency(amountVal));
                    amountLayer.hide();
                    rate.val(0);
                    rateLayer.show();
                } else {
                    rate.val(0);
                    rateLayer.hide();
                    amount.val(0);
                    amountLayer.hide();
                }
            }

            rate.on('change', function () {
                fnCheck();
            });
            rate.on('input', function () {
                fnCheck();
            });
            amount.on('change', function () {
                fnCheck();
            });
            amount.on('input', function () {
                fnCheck();
            });
            fnCheck();
        })();
        (function () {
            $('#ContentPlaceHolder1_drdlModalAccountSelled').on('change', function () {
                var detail = $('#modalAprrove_detailCollab');
                detail.html("");

                var selectValue = $(this).val();
                if (selectValue == "") {
                    return;
                }

                var option = $(this).find(" [data-collab-id='" + selectValue + "'] ");

                var collabId = option.data("collab-id");
                var collabCode = option.data("collab-code");
                var collabFullname = option.data("collab-fullname");
                var collabPhone = option.data("collab-phone");
                var collabEmail = option.data("collab-email");
                var collabAddress = option.data("collab-address");
                var collabCity = option.data("collab-city");
                var collabDistrict = option.data("collab-district");
                var collabWard = option.data("collab-ward");
                var collabAddressLine = "";

                if (collabAddress != undefined && collabAddress != null && collabAddress != "") {
                    collabAddressLine += collabAddress;
                }
                if (collabWard != undefined && collabWard != null && collabWard != "") {
                    collabAddressLine += ", " + collabWard;
                }
                if (collabDistrict != undefined && collabDistrict != null && collabDistrict != "") {
                    collabAddressLine += ", " + collabDistrict;
                }
                if (collabCity != undefined && collabCity != null && collabCity != "") {
                    collabAddressLine += ", " + collabCity;
                }

                
                if (collabFullname != undefined && collabFullname != null && collabFullname != "") {
                    detail.append('<p><i>Tên: <strong>' + collabFullname+'</strong></i></p>');
                }
                if (collabCode != undefined && collabCode != null && collabCode != "") {
                    detail.append('<p><i>Mã CTV: <strong>' + collabCode + '</strong></i></p>');
                }
                if (collabPhone != undefined && collabPhone != null && collabPhone != "") {
                    detail.append('<p><i>Số điện thoại: <a href="tel:' + collabPhone+'"><strong>' + collabPhone + '</strong></a></i></p>');
                }
                if (collabEmail != undefined && collabEmail != null && collabEmail != "") {
                    detail.append('<p><i>Email: <a href="mailto:' + collabEmail + '"><strong>' + collabEmail + '</strong></a></i></p>');
                }
                if (collabAddressLine != undefined && collabAddressLine != null && collabAddressLine != "") {
                    detail.append('<p><i>Địa chỉ: <strong>' + collabAddressLine + '</strong></i></p>');
                }

            });
        })();
        (function () {
            var itemPrice = 0;
            var fnFormatCurrency = function (amount) {
                amount = parseInt(amount);
                if (isNaN(amount)) {
                    amount = 0;
                }
                amount = '' + amount;
                var rs = "";
                do {
                    var t = amount.substr(-3);
                    if (t.length == 3 && amount.length > 3) {
                        rs = "," + t + rs;
                    } else {
                        rs = t + rs;
                    }
                    amount = amount.substr(0, amount.length - t.length)
                } while (amount.length > 0);
                return rs;
            };
            var fnCheckInput = function () {
                var valRate = $("#modalUpdateReferral_referralRate").val().replaceAll(" ", "").replaceAll(",", "");
                valRate = parseInt(valRate);
                if (isNaN(valRate)) {
                    valRate = 0;
                } else {
                    if (valRate < 0) {
                        valRate = 0;
                    }
                }
                $('#modalUpdateReferral_referralRate').val(valRate);
                var valAmount = $("#modalUpdateReferral_referralAmount").val().replaceAll(" ", "").replaceAll(",", "");
                valAmount = parseInt(valAmount);
                if (isNaN(valAmount)) {
                    valAmount = 0;
                } else {
                    if (valAmount < 0) {
                        valAmount = 0;
                    }
                }
                $('#modalUpdateReferral_referralAmount').val(valAmount);
            }
            var fnCalCommission = function () {
                fnCheckInput();
                var rate = parseInt($('#modalUpdateReferral_referralRate').val());
                var amount = parseInt($('#modalUpdateReferral_referralAmount').val().replaceAll(" ", "").replaceAll(",", ""));
                var commission = (rate * itemPrice / 100) + amount;
                commission = parseInt(commission);
                $('#modalUpdateReferral_referralCommissionInput').val(fnFormatCurrency("" + commission));
            }
            $('#modalUpdateReferral_referralRate').on("change", function () {
                fnCalCommission();
            });
            $('#modalUpdateReferral_referralAmount').on("change", function () {
                fnCalCommission();
            });
            $('.btn-update-referral').click(function () {
                var isReadOnly = $(this).data('readonly');
                if(isReadOnly != undefined && isReadOnly != null){
                    isReadOnly = "" + isReadOnly;
                    isReadOnly = isReadOnly.trim().toUpperCase();
                    if(isReadOnly == "TRUE" || isReadOnly == "1"){
                        isReadOnly = true;
                    }else{
                        isReadOnly = false;
                    }
                }else{
                    isReadOnly = false;
                }

                if(isReadOnly){
                    $('.wrap-readonly').css("display", "none");
                }else{
                    $('.wrap-readonly').css("display", "");
                }
                
                $('#modalUpdateReferral_referralId').val($(this).data('referral-id'));
                $('#modalUpdateReferral_collabId').val($(this).data('collab-id'));
                $('#modalUpdateReferral_postId').val($(this).data('post-id'));
                $('#modalUpdateReferral_postTitle').text($(this).data('title'));
                $('#modalUpdateReferral_postTitle').attr('href', $(this).data('link'));
                $('#modalUpdateReferral_postCode').text($(this).data('code'));
                $('#modalUpdateReferral_postCreatedAt').text($(this).data('created-at'));
                $('#modalUpdateReferral_postPrice').text($(this).data('price-format'));
                $('#modalUpdateReferral_referralCommission').text($(this).data('referral-commission'));
                $('#modalUpdateReferral_referralRate').val($(this).data('rate'));
                $('#modalUpdateReferral_referralAmount').val($(this).data('amount'));
                $('#modalUpdateReferral_referralCommissionInput').val("0");

                $('#modalUpdateReferral_detailCollab').html('');
                var collabFullname = $(this).data('collab-fullname');
                if(collabFullname != undefined && collabFullname != null && collabFullname != ""){
                    $('#modalUpdateReferral_detailCollab').append('<p><strong>Cộng tác viên:</strong> <span>'+collabFullname+'</span></p>');
                }
                var collabCode = $(this).data('collab-code');
                if(collabCode != undefined && collabCode != null && collabCode != ""){
                    $('#modalUpdateReferral_detailCollab').append('<p><strong>Mã CTV:</strong> <span>'+collabCode+'</span></p>');
                }
                var collabPhone = $(this).data('collab-phone');
                if(collabPhone != undefined && collabPhone != null && collabPhone != ""){
                    $('#modalUpdateReferral_detailCollab').append('<p><strong>Số điện thoại:</strong> <a href="tel:'+collabPhone+'">'+collabPhone+'</a></p>');
                }
                var collabEmail = $(this).data('collab-email');
                if(collabEmail != undefined && collabEmail != null && collabEmail != ""){
                    $('#modalUpdateReferral_detailCollab').append('<p><strong>Email:</strong> <a href="mailto:'+collabEmail+'">'+collabEmail+'</a></p>');
                }
                var collabAddress = $(this).data('collab-address');
                var collabCity = $(this).data('collab-city');
                var collabDistrict = $(this).data('collab-district');
                var collabWard = $(this).data('collab-ward');
                var collabAddressLine = '';
                if (collabAddress != undefined && collabAddress != null && collabAddress != "") {
                    collabAddressLine += collabAddress;
                }
                if (collabWard != undefined && collabWard != null && collabWard != "") {
                    collabAddressLine += ", " + collabWard;
                }
                if (collabDistrict != undefined && collabDistrict != null && collabDistrict != "") {
                    collabAddressLine += ", " + collabDistrict;
                }
                if (collabCity != undefined && collabCity != null && collabCity != "") {
                    collabAddressLine += ", " + collabCity;
                }
                if(collabAddressLine != undefined && collabAddressLine != null && collabAddressLine != ""){
                    $('#modalUpdateReferral_detailCollab').append('<p><strong>Địa chỉ:</strong> <span>'+collabAddressLine+'</span></p>');
                }
                itemPrice = parseInt($(this).data('price'));
                fnCalCommission();
                $('#modalUpdateReferral').modal();
                $('#modalUpdateReferral_referralRate').trigger("change");
                $('#modalUpdateReferral_referralAmount').trigger("change");
            });
            $('#btnUpdateReferral').click(function () {
                var referralId = $('#modalUpdateReferral_referralId').val();
                var collabId = $('#modalUpdateReferral_collabId').val();
                var postId = $('#modalUpdateReferral_postId').val();
                var referralRate = $('#modalUpdateReferral_referralRate').val();
                var referralAmount = $('#modalUpdateReferral_referralAmount').val();

                $('#GFake_ThongTinCaNhanCopy_ModalUpdateReferral_ReferralId').val(referralId);
                $('#GFake_ThongTinCaNhanCopy_ModalUpdateReferral_CollabId').val(collabId);
                $('#GFake_ThongTinCaNhanCopy_ModalUpdateReferral_PostId').val(postId);
                $('#GFake_ThongTinCaNhanCopy_ModalUpdateReferral_ReferralRate').val(referralRate);
                $('#GFake_ThongTinCaNhanCopy_ModalUpdateReferral_ReferralAmount').val(referralAmount);
                $('#GFake_ThongTinCaNhanCopy_ModalUpdateReferral_Submit').click();
            });
        })();
        (function () {
            var rate = $('#modalUpdateReferral_referralRate');
            var rateWrap = rate.parents('.row');
            var rateLayer = rateWrap.find('.layer-wartermark');
            var amount = $('#modalUpdateReferral_referralAmount');
            var amountWrap = amount.parents('.row');
            var amountLayer = amountWrap.find('.layer-wartermark');

            var fnFormatCurrency = function (amount) {
                amount = parseInt(amount);
                if (isNaN(amount)) {
                    amount = 0;
                }
                amount = '' + amount;
                var rs = "";
                do {
                    var t = amount.substr(-3);
                    if (t.length == 3 && amount.length > 3) {
                        rs = "," + t + rs;
                    } else {
                        rs = t + rs;
                    }
                    amount = amount.substr(0, amount.length - t.length)
                } while (amount.length > 0);
                return rs;
            };

            var fnCheck = function () {
                var rateVal = rate.val();
                rateVal = parseFloat(rateVal.replaceAll(" ", "").replaceAll(",", ""));
                var amountVal = amount.val();
                amountVal = parseFloat(amountVal.replaceAll(" ", "").replaceAll(",", ""));

                if (!isNaN(rateVal) && rateVal == 0 && !isNaN(amountVal) && amountVal == 0) {
                    rateLayer.hide();
                    rate.val(0);
                    amountLayer.hide();
                    amount.val(0);
                } else if (!isNaN(rateVal) && rateVal > 0) {
                    rate.val(rateVal);
                    rateLayer.hide();
                    amount.val(0);
                    amountLayer.show();
                } else if (!isNaN(amountVal) && amountVal > 0) {
                    amount.val(fnFormatCurrency(amountVal));
                    amountLayer.hide();
                    rate.val(0);
                    rateLayer.show();
                } else {
                    rate.val(0);
                    rateLayer.hide();
                    amount.val(0);
                    amountLayer.hide();
                }
            }

            rate.on('change', function () {
                fnCheck();
            });
            rate.on('input', function () {
                fnCheck();
            });
            amount.on('change', function () {
                fnCheck();
            });
            amount.on('input', function () {
                fnCheck();
            });
            fnCheck();
        })();
        (function () {
            $('.btn-dump-post').click(function () {
                var postId = $(this).data('id');

                if (confirm("Bạn muốn hủy bỏ tin đăng này?")) {
                    $('#GFake_ThongTinCaNhanCopy_BtnDump_PostId').val(postId);
                    $('#GFake_ThongTinCaNhanCopy_BtnDump_Submit').click();
                }
            });
        })();
        (function () {
            $('.btn-cancel-referral').click(function () {
                var referralId = $(this).data('referral-id');

                if (confirm("Bạn muốn hủy trích hoa hồng cộng tác này ?")) {
                    $('#GFake_ThongTinCaNhanCopy_OwnerCancelReferral_ReferralId').val(referralId);
                    $('#GFake_ThongTinCaNhanCopy_OwnerCancelReferral_Submit').click();
                }
            });
        })();
    </script>

    <script>
        $(".btn-renovate").on('click', function () {
            const id = $(this).data("id");
            const title = $(this).data("title");
            const renovatePrice = <%= Services.RenovateFees.getPrice() %>;
            let message = "";
            let fnAllow = undefined;
            let fnDeny = undefined;
            let okText = "";

            if(APP_BALANCE >= renovatePrice) {
                message = "Lên TOP cho tin đăng: " + title;
                okText = "Lên TOP";
                fnAllow = function() {
                    $("#ContentPlaceHolder1_txtRenovateId").val(id);
                    $("#ContentPlaceHolder1_txtRenovateRedirect").val(window.location.href);
                    $('#ContentPlaceHolder1_btnRenovateSubmit').click();
                };
            }else{
                message = "Nạp thêm tiền để thực hiện lên TOP cho tin đăng";
                okText = "Nạp tiền";
                fnAllow = function() {
                    window.location.href = "/tai-khoan/thong-tin-vi";
                };
            }

            alertify.confirm(message, fnAllow, fnDeny).set({
                title: "Xác nhận",
                labels: {
                    ok: okText,
                    cancel: "Hủy",
                }
            });
        });
    </script>
</asp:Content>

