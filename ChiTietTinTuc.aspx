﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChiTietTinTuc.aspx.cs" Inherits="Admin_ChiTietTinTuc" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section class="feature container">
      <div class="row">
         <!--Top Jobs-->
         <div class="col-md-9 col-sm-12">
            <div class="top-job">
               <div class="panel jobs-board-listing with-mc no-padding no-border">
                   <div id="dvLinkTitle" class="linktitle" runat="server"><%--<a href="/">Trang chủ</a> > <a href="#">Cho thuê</a> > <a href="#">Thuê bất động sản</a> > Thuê nhà nguyên căn--%></div>
                  <div class="panel-content" id="tabChoThue" style="padding:10px">
                      <div class="job-list scrollbar m-t-lg">
                          <h1 id="dvTitle" runat="server" style="text-align:center"><%--Cho thuê xe máy giá rẻ--%></h1>
                          <div id="dvNoiDung" runat="server" style="padding:5px">
                              <%--<p>Salon gỗ Nội thất Nam Phương xin trân trọng giới thiệu quý khách các loại bàn ăn chất liệu gỗ: Bích, Còng,Xoan đào,Sồi Nga, Sồi Mỹ, Cao su... Nhiều mẫu mã kích thước khác nhau, Quý vị có thể xem hàng tại showroom hoặc tại kho.</p>
                              <p>- Sản phẩm tốt</p>--%>
                          </div>
                      </div>
                  </div>
                   <div class="panel-content" id="tabTinCungDanhMuc">
                     <div class="job-list scrollbar m-t-lg">
                        <div class="title">+ Tin Hot</div>
                        <ul id="ulTinCungDanhMuc" runat="server">
                        </ul>
                        <div style="text-align: center; margin: 10px">
                           <%--<button type="submit" class="btn btn-primary1 btn-search-all">
                                Xem tất cả
                           </button>--%>
                            <asp:Button ID="btXemTatCa" runat="server" Text="Xem tất cả ..." class="btn btn-primary1 btn-search-all" OnClick="btXemTatCa_Click" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</asp:Content>
