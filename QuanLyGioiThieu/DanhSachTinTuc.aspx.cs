﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuanLyTinTuc_DanhSachTinTuc : System.Web.UI.Page
{
    string sTuNgay = "";
    string sDenNgay = "";
    string sTieuDe = "";

    string txtFistPage = "1";
    string txtPage1 = "";
    string txtPage2 = "";
    string txtPage3 = "";
    string txtPage4 = "";
    string txtPage5 = "";
    string txtLastPage = "";
    int Page = 0;
    int MaxPage = 0;
    int PageSize = 70;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["AdminTungTang_Login"] != null)
        {
            string TenDangNhap_Cookie = HttpContext.Current.Request.Cookies["AdminTungTang_Login"].Value;
            string idLoaiNguoiDung = StaticData.getField("tb_admin", "idLoaiAdmin", "TenDangNhap", TenDangNhap_Cookie.ToString());
            //if (idLoaiNguoiDung == "2")//2 - Người kiểm duyệt
            //    Response.Redirect("../Home/Default.aspx");
        }
        else
        {
            Response.Redirect("../Home/Default.aspx");
        }
        try
        {
            Page = int.Parse(Request.QueryString["Page"].ToString());
        }
        catch
        {
            Page = 1;
        }

        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString["TieuDe"].Trim() != "")
                {
                    sTieuDe = Request.QueryString["TieuDe"].Trim();
                    txtTenDangNhap.Value = sTieuDe;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["TuNgay"].Trim() != "")
                {
                    sTuNgay = Request.QueryString["TuNgay"].Trim();
                    txtTuNgay.Value = sTuNgay;
                }
            }
            catch { }

            try
            {
                if (Request.QueryString["DenNgay"].Trim() != "")
                {
                    sDenNgay = Request.QueryString["DenNgay"].Trim();
                    txtDenNgay.Value = sDenNgay;
                }
            }
            catch { }

            
            LoadTinDang();
        }
    }
    #region paging
    private void SetPage()
    {
        string sql = "select count(*) from tb_GioiThieu where '1'='1'";
        if (sTieuDe != "")
            sql += " and TieuDe like N'%" + sTieuDe + "%'";
        if (sTuNgay != "")
            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";
        
        DataTable tbTotalRows = Connect.GetTable(sql);
        int TotalRows = int.Parse(tbTotalRows.Rows[0][0].ToString());
        if (TotalRows % PageSize == 0)
            MaxPage = TotalRows / PageSize;
        else
            MaxPage = TotalRows / PageSize + 1;
        txtLastPage = MaxPage.ToString();
        if (Page == 1)
        {
            for (int i = 1; i <= MaxPage; i++)
            {
                if (i <= 5)
                {
                    switch (i)
                    {
                        case 1: txtPage1 = i.ToString(); break;
                        case 2: txtPage2 = i.ToString(); break;
                        case 3: txtPage3 = i.ToString(); break;
                        case 4: txtPage4 = i.ToString(); break;
                        case 5: txtPage5 = i.ToString(); break;
                    }
                }
                else
                    return;
            }
        }
        else
        {
            if (Page == 2)
            {
                for (int i = 1; i <= MaxPage; i++)
                {
                    if (i == 1)
                        txtPage1 = "1";
                    if (i <= 5)
                    {
                        switch (i)
                        {
                            case 2: txtPage2 = i.ToString(); break;
                            case 3: txtPage3 = i.ToString(); break;
                            case 4: txtPage4 = i.ToString(); break;
                            case 5: txtPage5 = i.ToString(); break;
                        }
                    }
                    else
                        return;
                }
            }
            else
            {
                int Cout = 1;
                if (Page <= MaxPage)
                {
                    for (int i = Page; i <= MaxPage; i++)
                    {
                        if (i == Page)
                        {
                            txtPage1 = (Page - 2).ToString();
                            txtPage2 = (Page - 1).ToString();
                        }
                        if (Cout <= 3)
                        {
                            if (i == Page)
                                txtPage3 = i.ToString();
                            if (i == (Page + 1))
                                txtPage4 = i.ToString();
                            if (i == (Page + 2))
                                txtPage5 = i.ToString();
                            Cout++;
                        }
                        else
                            return;
                    }
                }
                else
                {
                    //Page = MaxPage;
                    SetPage();
                }
            }
        }
    }
    #endregion
    private void LoadTinDang()
    {
        string sql = "";
        sql += @"select * from
            (
	            SELECT 
	              *
                  FROM tb_GioiThieu where '1'='1'
            ";
        if (sTieuDe != "")
            sql += " and TieuDe like N'%" + sTieuDe + "%'";
        if (sTuNgay != "")
            sql += " and NgayDang >= '" + StaticData.ConvertDDMMtoMMDD(sTuNgay) + " 00:00:00'";
        if (sDenNgay != "")
            sql += " and NgayDang <= '" + StaticData.ConvertDDMMtoMMDD(sDenNgay) + " 23:59:59'";

        sql += ") as tb1 ";


        DataTable table = Connect.GetTable(sql);
        //txtNoiDung.InnerHtml = table.Rows[0]["NoiDung"].ToString();
        SetPage();
        string html = @"<table class='table table-bordered table-striped' id='myTable'>
                            <tr>
                                <th class='th'>
                                    STT
                                </th>
                               
                                <th class='th'>
                                    Nội dung
                                </th>
                                <th class='th'></th>
                            </tr>";
        for (int i = 0; i < table.Rows.Count; i++)
        {
            html += "       <tr>";
            html += "       <td>" + ( i + 1).ToString() + "</td>";
            //if (table.Rows[i]["NgayDang"].ToString().ToString() != "")
            //    html += "       <td>" + DateTime.Parse(table.Rows[i]["NgayDang"].ToString()).ToString("dd/MM/yyyy hh:mm") + "</td>";
            //else
            //    html += "   <td></td>";
            html += "       <td>" + table.Rows[i]["GioiThieu"].ToString() + "</td>";
            //html += "       <td><img src='/Images/news/" + table.Rows[i]["AnhDaiDien"].ToString() + "' style='width:100px' alt='img'</td>";
            //if (table.Rows[i]["isHot"].ToString() == "True")
            //    html += "<td><input type='checkbox' disabled checked/></td>";
            //else
            //    html += "<td><input type='checkbox' disabled/></td>";

            //if (table.Rows[i]["KichHoat"].ToString() == "True")
            //    html += "<td><input type='checkbox' disabled checked/></td>";
            //else
            //    html += "<td><input type='checkbox' disabled/></td>";

            html += "       <td>";
            html += "       <a style='cursor:pointer' onclick='window.location=\"QuanLyTinTuc-CapNhat.aspx" + "" + "\"'><img class='imgedit' src='../images/edit.png'/>Sửa</a>";
            //html += "       |<a style='cursor:pointer'  onclick='DeleteTinTuc(\"" + table.Rows[i]["idTinTuc"].ToString() + "\")'> <img class='imgedit' src='../images/delete.png' />Xóa</a>";
            html += "       </td>";
            html += "       </tr>";
        }
        //html += "   <tr>";
        //html += "       <td colspan='11' class='footertable'>";
        //string url = "DanhSachTinTuc.aspx?";
        //if (sTuNgay != "")
        //    url += "TuNgay=" + sTuNgay + "&";
        //if (sDenNgay != "")
        //    url += "DenNgay=" + sDenNgay + "&";
        //if (sTieuDe != "")
        //    url += "TieuDe=" + sTieuDe + "&";

        //url += "Page=";
        //html += "           <a class='notepaging' id='page_fist' href='" + url + txtFistPage + "' /><<</a>";
        ////Page 1
        //if (txtPage1 != "")
        //{
        //    if (Page.ToString() == txtPage1)
        //        html += "           <a id='page_1' class='notepagingactive' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        //    else
        //        html += "           <a id='page_1' class='notepaging' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        //}
        //else
        //{
        //    html += "           <a id='page_1' class='notepagingnone' href='" + url + txtPage1 + "' />" + txtPage1 + "</a>";
        //}
        ////Page 2
        //if (txtPage2 != "")
        //{
        //    if (Page.ToString() == txtPage2)
        //        html += "           <a id='page_2' class='notepagingactive' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        //    else
        //        html += "           <a id='page_2' class='notepaging' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        //}
        //else
        //{
        //    html += "           <a id='page_2' class='notepagingnone' href='" + url + txtPage2 + "' />" + txtPage2 + "</a>";
        //}
        ////Page 3
        //if (txtPage3 != "")
        //{
        //    if (Page.ToString() == txtPage3)
        //        html += "           <a id='page_3' class='notepagingactive' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        //    else
        //        html += "           <a id='page_3' class='notepaging' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        //}
        //else
        //{
        //    html += "           <a id='page_3' class='notepagingnone' href='" + url + txtPage3 + "' />" + txtPage3 + "</a>";
        //}
        ////Page 4
        //if (txtPage4 != "")
        //{
        //    if (Page.ToString() == txtPage4)
        //        html += "           <a id='page_4' class='notepagingactive' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        //    else
        //        html += "           <a id='page_4' class='notepaging' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        //}
        //else
        //{
        //    html += "           <a id='page_4' class='notepagingnone' href='" + url + txtPage4 + "' />" + txtPage4 + "</a>";
        //}
        ////Page 5
        //if (txtPage5 != "")
        //{
        //    if (Page.ToString() == txtPage5)
        //        html += "           <a id='page_5' class='notepagingactive' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        //    else
        //        html += "           <a id='page_5' class='notepaging' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        //}
        //else
        //{
        //    html += "           <a id='page_5' class='notepagingnone' href='" + url + txtPage5 + "' />" + txtPage5 + "</a>";
        //}

        //html += "           <a id='page_last' class='notepaging' href='" + url + txtLastPage + "' />>></a>";
        //html += "   </td></tr>";
        html += "     </table>";
        dvTinDang.InnerHtml = html;
    }
    protected void btTimKiem_Click(object sender, EventArgs e)
    {
        string TuNgay = txtTuNgay.Value.Trim();
        string DenNgay = txtDenNgay.Value.Trim();
        string TieuDe = txtTenDangNhap.Value.Trim();

        string url = "DanhSachTinTuc.aspx?";
        if (TuNgay != "")
            url += "TuNgay=" + TuNgay + "&";
        if (DenNgay != "")
            url += "DenNgay=" + DenNgay + "&";
        if (TieuDe != "")
            url += "TieuDe=" + TieuDe + "&";
        Response.Redirect(url);
    }
    protected void btXemTatCa_Click(object sender, EventArgs e)
    {
        string url = "DanhSachTinTuc.aspx";
        Response.Redirect(url);
    }
}