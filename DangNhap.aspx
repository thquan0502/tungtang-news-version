﻿<%@ Page Title="" enableEventValidation="false" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DangNhap.aspx.cs" Inherits="DangNhap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script> 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <section class="feature container">
      <div class="row">
         <!--Top Jobs-->
         <div class="col-md-9 col-sm-12">
            <div class="top-job">
               <div class="panel jobs-board-listing with-mc no-padding no-border">
                  <div class="panel-content" id="tabChoThue" style="padding:10px">
                     <div class="job-list scrollbar m-t-lg">
                        <div id="vnw-log-in" class="container main-content">
                           <div class="col-sm-8 col-sm-push-2">
                              <h1 class="text-center">ĐĂNG NHẬP</h1>
                                 <div>
                                         <div class="form-group">
                                             <div class="col-md-8 col-md-offset-2">
                                             <input type="text" id="txtTenDangNhap" name="form[username]" required="required" placeholder="Số điện thoại" tabindex="1" class="form-control" runat="server" />
                                             </div>
                                         </div>
                                         <div class="form-group">
                                             <div class="col-md-8 col-md-offset-2">
                                             <input type="password" id="txtMatKhau" name="form[password]" required="required" placeholder="Mật khẩu" tabindex="2" class="form-control" runat="server" />
                                             </div>
                                         </div>
                                         <!-- Buttons-->
                                         <div class="form-group">
                                             <div class="col-md-offset-2 col-md-8">
                                             <%--<button type="submit" id="form_sign_in" name="form[sign_in]" tabindex="3" class="btn btn-primary btn-block">Đăng Nhập</button>--%>
                                                 <asp:Button ID="btDangNhap" runat="server" Text="Đăng nhập"  OnClick="btDangNhap_Click" class="btn btn-primary btn-block"/>
                                             </div>
                                         </div>
                                         <div class="form-group" style="text-align:center;">
                                             <a href="/dang-ky/dk" >Bạn chưa có tải khoản?</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#"  style="display:none;">Quên mật khẩu?</a>
                                         </div>
                                        <%--<div class="form-group">
                                             <div class="col-md-offset-2 col-md-8">
                                             <button type="submit" id="form_sign_in" name="form[sign_in]" tabindex="3" class="btn btn-facebook btn-block">Đăng Nhập Bằng Facebook</button>
                                             </div>
                                         </div>
                                        <div class="form-group">
                                             <div class="col-md-offset-2 col-md-8">
                                             <button type="submit" id="form_sign_in" name="form[sign_in]" tabindex="3" class="btn btn-google btn-block">Đăng Nhập Bằng Google</button>
                                             </div>
                                         </div>--%>
                                     </div>
                               <div id="dvDKTC" runat="server" style="text-align: center;color: #05c705;font-style: italic;"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</asp:Content>