﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text.RegularExpressions;	

public partial class MasterPage : System.Web.UI.MasterPage
{
    string Domain = "";
    string idDanhMucCap1 = "";
    string idDanhMucCap2 = "";
    string idTinh = "";
    string idHuyen = "";
    string idPhuongXa = "";
    string DiaChi = "";
    string TenCuaHang = "";
    string htmlInnerHtmlTitle = "";
    protected string metaroBot = "";
    string idDanhMucCap1New = "", idDanhMucCap2New = "";
    string idTinTuc = "";
    string metaTitle = "";
    string metaUrl = "";
    string metaHinhAnh = "";
    string id_title = "";
    string mTenDangNhap = "";
	string Tinh = "";
    string QuanHuyen = "";
    string PhuongXa = "";
    string TenDMC1 = "";
    string TenDMC2 = "";

    protected bool isAuth = false;

    protected long balance = -1;

    protected DataRow authAccount = null;

    protected void Page_LoadComplete(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        loadBalance();
        string urlNow = HttpContext.Current.Request.Url.AbsoluteUri;
        if (urlNow.ToUpper().Contains("GIDZL") || urlNow.ToLower().Contains("fbclid"))
        {
            string[] io = urlNow.ToLower().Split(new[] { "?gidzl", "&gidzl", "?fbclid", "&fbclid" }, StringSplitOptions.None);
            try
            {
                Response.Redirect(io[0]);
            }
            catch
            {

            }
        }
        this.authAccount = Utilities.Auth.getAccount();
        string sqlDomain = "select * from tb_Domain";
        DataTable tbDomain = Connect.GetTable(sqlDomain);
        if (tbDomain.Rows.Count > 0)
        {
            Domain = "";
            LoadDangNhap();
        }
        if (!IsPostBack)
        {
            //try
            //         {
            //            if (!string.IsNullOrWhiteSpace(Request.QueryString["CH"]))
            //            {
            //                TenCuaHang = Request.QueryString["CH"].ToString().Trim();
            //            }
            //            txtSearch.Value = TenCuaHang;
            //        }
            //        catch { }
            //        try
            //        {
            //            if (!string.IsNullOrWhiteSpace(Request.QueryString["C2"]))
            //            {
            //                idDanhMucCap2 = Request.QueryString["C2"].ToString().Trim();
            //            }
            //        }
            //        catch { }
            string Url = HttpContext.Current.Request.Url.AbsoluteUri.ToUpper().Replace("://", "").Replace(":\\", "");
            string[] arrUrl = Url.Split('/', '&', '?');
            try
            {
                if (arrUrl[2] != null && arrUrl[2].Trim() != "")
                {
                    string[] arrTinDang = arrUrl[2].Trim().Split('?');
                    if (arrTinDang[0] != "")
                    {
                        string LinkidCap1 = StaticData.getField("tb_DanhMucCap1", "idDanhMucCap1", "LinkTenDanhMucCap1", arrTinDang[0]);
                        if (LinkidCap1 != "")
                        {
                            idDanhMucCap1 = LinkidCap1;
                        }
                    }
                    if (arrUrl[3] != "")
                    {
                        string LinkidCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[3]);
                        if (LinkidCap2 != "")
                        {
                            idDanhMucCap2 = LinkidCap2;
                        }
                        if (LinkidCap2 == "")
                        {
                            Tinh = StaticData.getField("City", "Ten", "Link", arrUrl[3]);
                        }
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[4] != "")
                {
                    string LinkidCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[4]);
                    if (LinkidCap2 != "")
                    {
                        idDanhMucCap2 = LinkidCap2;
                    }
                    if (LinkidCap2 == "")
                    {
                        QuanHuyen = StaticData.getField("District", "Ten", "Link", arrUrl[4]);
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[5] != "")
                {
                    string LinkidCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[5]);
                    if (LinkidCap2 != "")
                    {
                        idDanhMucCap2 = LinkidCap2;
                    }
                    if (LinkidCap2 == "")
                    {
                        PhuongXa = StaticData.getField("tb_PhuongXa", "Ten", "Link", arrUrl[5]);
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[6] != "")
                {
                    string LinkidCap2 = StaticData.getField("tb_DanhMucCap2", "idDanhMucCap2", "LinkTenDanhMucCap2", arrUrl[6]);
                    if (LinkidCap2 != "")
                    {
                        idDanhMucCap2 = LinkidCap2;
                    }
                }
            }
            catch { }
            try
            {
                if (arrUrl[2] != null && arrUrl[2].Trim() != "")
                {
                    string[] arrTinDang = arrUrl[2].Trim().Split('?');
                    if (arrTinDang[0] != "")
                    {
                        string LinkidTenDMCap1 = StaticData.getField("tb_DanhMucCap1", "idDanhMucCap1", "LinkTenDanhMucCap1", arrTinDang[0]);
                        idDanhMucCap1New = LinkidTenDMCap1;
                    }
                }
            }
            catch { }
            idDanhMucCap2New = idDanhMucCap2;
            try
            {
                if (idDanhMucCap1New == "" && idDanhMucCap1New == "" && Tinh == "")
                {
                    DataTable tableTag = Connect.GetTable(@"select    Title
                      ,Desciption
                     from Seo where 1=1");
                    if (tableTag.Rows.Count > 0)
                    {
                        ogtitle.Content = tableTag.Rows[0]["Title"].ToString();
                        ogtype.Content = tableTag.Rows[0]["Title"].ToString();
                        ogtwtitle.Content = tableTag.Rows[0]["Title"].ToString();
                        ogdes.Content = tableTag.Rows[0]["Desciption"].ToString();
                        ogtwdes.Content = tableTag.Rows[0]["Desciption"].ToString();
                    }
                }



                else if (idDanhMucCap1New == "" && Tinh != "")
                {
                    //DataTable tableTag = Connect.GetTable(@"select    Titlte
                    //      ,MoTa,LinkAnh
                    //      from tb_DanhMucCap1 where idDanhMucCap1='" + idDanhMucCap1New + "' ");
                    //if (tableTag.Rows.Count > 0)
                    //{
                    //string Tinh = "";
                    //string QuanHuyen = "";
                    //string PhuongXa = "";                    
                    string urlHinhAnh = "";
                    string Titlte = "";
                    string Desciption = "";
                    urlHinhAnh = Domain + "";
                    if (QuanHuyen == "" && PhuongXa == "")
                    {
                        Titlte = "Rao vặt " + Tinh + " miễn phí";
                        Desciption = "Rao vặt " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                    }
                    if (QuanHuyen != "" && PhuongXa == "")
                    {
                        Titlte = "Rao vặt " + QuanHuyen + " " + Tinh + " miễn phí";
                        Desciption = "Rao vặt " + QuanHuyen + " " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                    }
                    if (QuanHuyen != "" && PhuongXa != "")
                    {
                        Titlte = "Rao vặt " + PhuongXa + " " + QuanHuyen + " " + Tinh + " miễn phí";
                        Desciption = "Rao vặt " + PhuongXa + " " + QuanHuyen + " " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                    }
                    title.Text = Titlte;
                    description.Content = Desciption;
                    ogtitle.Content = Titlte;
                    ogdes.Content = Desciption;
                    ogtype.Content = Titlte;
                    ogurl.Content = HttpContext.Current.Request.Url.OriginalString;
                    ogtwdes.Content = Desciption;
                    ogtwtitle.Content = Titlte;
                    igurl.Content = urlHinhAnh;
                    ogtwimg.Content = urlHinhAnh;
                    ogimage.Content = urlHinhAnh;
					  Meta1.Content = Desciption;


                    metaTitle = title.Text;


                    string metaU = HttpContext.Current.Request.Url.OriginalString;
                    string name = "name";
                    string mainEntityOfPage = "mainEntityOfPage";
                    string idd = "id";
                    string context = "@context";
                    string type = "@type";
                    string iddd = "@id";
                    string hl = "headline";
                    string d = "description";
                    string ig = "image";
                    string ul = "url";
                    string con = "@context";
                    string t = "@type";
                    string dp = "datePublished";
                    string dm = "dateModified";
                    string au = "author";
                    string na = "name";
                    string c = "@context";
                    string tp = "@type";
                    string pb = "publisher";
                    string lg = "logo";
                    string width = "width";
                    string he = "height";
                    string hea = "application/ld+json";
                    string urlchu = "https://tungtang.com.vn";
                    string paa = "WebPage";
                    string imgo = "ImageObject";
                    string per = "Person";
                    string og = "Organization";
                    string news = "NewsArticle";
                    string chema = "https://schema.org";
                    string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                    string Crea = "CreativeWorkSeries";
                    Page.Header.Controls.AddAt(1,
                               new LiteralControl(
                                   "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                               )
                               );
                    //    }
                }
                else if (idDanhMucCap2New == "" && Tinh == "")
                {
                    DataTable tableTag = Connect.GetTable(@"select    Titlte
                          ,MoTa,LinkAnh
                          from tb_DanhMucCap1 where idDanhMucCap1='" + idDanhMucCap1New + "' ");
                    if (tableTag.Rows.Count > 0)
                    {
                        string urlHinhAnh = "";
                        urlHinhAnh = Domain + tableTag.Rows[0]["LinkAnh"].ToString();
                        string Titlte = boSpace(tableTag.Rows[0]["Titlte"].ToString());
                        string Desciption = boSpace(tableTag.Rows[0]["MoTa"].ToString());
                        title.Text = Titlte;
                        description.Content = Desciption;
                        ogtitle.Content = Titlte;
                        ogdes.Content = Desciption;
                        ogtype.Content = Titlte;
                        ogurl.Content = HttpContext.Current.Request.Url.OriginalString;
                        ogtwdes.Content = Desciption;
                        ogtwtitle.Content = Titlte;
                        igurl.Content = urlHinhAnh;
                        ogtwimg.Content = urlHinhAnh;
                        ogimage.Content = urlHinhAnh;
						  Meta1.Content = Desciption;


                        metaTitle = tableTag.Rows[0]["Titlte"].ToString();


                        string metaU = HttpContext.Current.Request.Url.OriginalString;
                        string name = "name";
                        string mainEntityOfPage = "mainEntityOfPage";
                        string idd = "id";
                        string context = "@context";
                        string type = "@type";
                        string iddd = "@id";
                        string hl = "headline";
                        string d = "description";
                        string ig = "image";
                        string ul = "url";
                        string con = "@context";
                        string t = "@type";
                        string dp = "datePublished";
                        string dm = "dateModified";
                        string au = "author";
                        string na = "name";
                        string c = "@context";
                        string tp = "@type";
                        string pb = "publisher";
                        string lg = "logo";
                        string width = "width";
                        string he = "height";
                        string hea = "application/ld+json";
                        string urlchu = "https://tungtang.com.vn";
                        string paa = "WebPage";
                        string imgo = "ImageObject";
                        string per = "Person";
                        string og = "Organization";
                        string news = "NewsArticle";
                        string chema = "https://schema.org";
                        string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                        string Crea = "CreativeWorkSeries";
                        Page.Header.Controls.AddAt(1,
                                   new LiteralControl(
                                       "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                                   )
                                   );
                    }
                }
                else if (idDanhMucCap2New != "" && Tinh == "")
                {
                    DataTable tableTag = Connect.GetTable(@"select    Titlte
                          ,MoTa,LinkIcon
                          from tb_DanhMucCap2 where idDanhMucCap2='" + idDanhMucCap2New + "' ");
                    if (tableTag.Rows.Count > 0)
                    {
                        string urlHinhAnh = "";
                        urlHinhAnh = Domain + tableTag.Rows[0]["LinkIcon"].ToString();
                        string Titlte = boSpace(tableTag.Rows[0]["Titlte"].ToString());
                        string Desciption = boSpace(tableTag.Rows[0]["MoTa"].ToString());

                        title.Text = Titlte;
                        description.Content = Desciption;
                        ogtitle.Content = Titlte;
                        ogdes.Content = Desciption;
                        ogtype.Content = Titlte;
                        ogurl.Content = HttpContext.Current.Request.Url.OriginalString;
                        ogtwdes.Content = Desciption;
                        ogtwtitle.Content = Titlte;
                        igurl.Content = urlHinhAnh;
                        ogtwimg.Content = urlHinhAnh;
                        ogimage.Content = urlHinhAnh;
						  Meta1.Content = Desciption;

                        metaTitle = tableTag.Rows[0]["Titlte"].ToString();


                        string metaU = HttpContext.Current.Request.Url.OriginalString;
                        string name = "name";
                        string mainEntityOfPage = "mainEntityOfPage";
                        string idd = "id";
                        string context = "@context";
                        string type = "@type";
                        string iddd = "@id";
                        string hl = "headline";
                        string d = "description";
                        string ig = "image";
                        string ul = "url";
                        string con = "@context";
                        string t = "@type";
                        string dp = "datePublished";
                        string dm = "dateModified";
                        string au = "author";
                        string na = "name";
                        string c = "@context";
                        string tp = "@type";
                        string pb = "publisher";
                        string lg = "logo";
                        string width = "width";
                        string he = "height";
                        string hea = "application/ld+json";
                        string urlchu = "https://tungtang.com.vn";
                        string paa = "WebPage";
                        string imgo = "ImageObject";
                        string per = "Person";
                        string og = "Organization";
                        string news = "NewsArticle";
                        string chema = "https://schema.org";
                        string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                        string Crea = "CreativeWorkSeries";
                        Page.Header.Controls.AddAt(1,
                                   new LiteralControl(
                                       "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 6068}}</script> "
                                   )
                                   );
                    }
                }
                else if (idDanhMucCap2New != "" && Tinh == "")
                {
                    DataTable tableTag = Connect.GetTable(@"select    Titlte
                          ,MoTa,LinkIcon
                          from tb_DanhMucCap2 where idDanhMucCap2='" + idDanhMucCap2New + "' ");
                    if (tableTag.Rows.Count > 0)
                    {
                        string urlHinhAnh = "";
                        urlHinhAnh = Domain + tableTag.Rows[0]["LinkIcon"].ToString();
                        string Titlte = boSpace(tableTag.Rows[0]["Titlte"].ToString());
                        string Desciption = boSpace(tableTag.Rows[0]["MoTa"].ToString());

                        title.Text = Titlte;
                        description.Content = Desciption;
                        ogtitle.Content = Titlte;
                        ogdes.Content = Desciption;
                        ogtype.Content = Titlte;
                        ogurl.Content = HttpContext.Current.Request.Url.OriginalString;
                        ogtwdes.Content = Desciption;
                        ogtwtitle.Content = Titlte;
                        igurl.Content = urlHinhAnh;
                        ogtwimg.Content = urlHinhAnh;
                        ogimage.Content = urlHinhAnh;
						  Meta1.Content = Desciption;

                        metaTitle = tableTag.Rows[0]["Titlte"].ToString();


                        string metaU = HttpContext.Current.Request.Url.OriginalString;
                        string name = "name";
                        string mainEntityOfPage = "mainEntityOfPage";
                        string idd = "id";
                        string context = "@context";
                        string type = "@type";
                        string iddd = "@id";
                        string hl = "headline";
                        string d = "description";
                        string ig = "image";
                        string ul = "url";
                        string con = "@context";
                        string t = "@type";
                        string dp = "datePublished";
                        string dm = "dateModified";
                        string au = "author";
                        string na = "name";
                        string c = "@context";
                        string tp = "@type";
                        string pb = "publisher";
                        string lg = "logo";
                        string width = "width";
                        string he = "height";
                        string hea = "application/ld+json";
                        string urlchu = "https://tungtang.com.vn";
                        string paa = "WebPage";
                        string imgo = "ImageObject";
                        string per = "Person";
                        string og = "Organization";
                        string news = "NewsArticle";
                        string chema = "https://schema.org";
                        string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                        string Crea = "CreativeWorkSeries";
                        Page.Header.Controls.AddAt(1,
                                   new LiteralControl(
                                       "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 6068}}</script> "
                                   )
                                   );
                    }
                }
                else if (idDanhMucCap2New == "" && Tinh != "")
                {
                    DataTable tableTag = Connect.GetTable(@"select    Titlte
                          ,MoTa,LinkAnh
                          from tb_DanhMucCap1 where idDanhMucCap1='" + idDanhMucCap1New + "' ");
                    TenDMC1 = StaticData.getField("tb_DanhMucCap1", "TenDanhMucCap1", "idDanhMucCap1", idDanhMucCap1New);
                    if (tableTag.Rows.Count > 0)
                    {
                        string urlHinhAnh = "";
                        string Titlte = "";
                        string Desciption = "";
                        urlHinhAnh = Domain + tableTag.Rows[0]["LinkAnh"].ToString();
                        if (QuanHuyen == "" && PhuongXa == "")
                        {
                            Titlte = "Rao vặt " + TenDMC1 + " " + Tinh + " miễn phí";
                            Desciption = "Rao vặt " + TenDMC1 + " " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                        }
                        else if (QuanHuyen != "" && PhuongXa == "")
                        {
                            Titlte = "Rao vặt " + TenDMC1 + " " + QuanHuyen + ", " + Tinh + " miễn phí";
                            Desciption = "Rao vặt " + TenDMC1 + " " + QuanHuyen + ", " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                        }
                        else if (QuanHuyen != "" && PhuongXa != "")
                        {
                            Titlte = "Rao vặt " + TenDMC1 + " " + PhuongXa + ", " + QuanHuyen + ", " + Tinh + " miễn phí";
                            Desciption = "Rao vặt " + TenDMC1 + " " + PhuongXa + ", " + QuanHuyen + ", " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                        }
                        title.Text = Titlte;
                        description.Content = Desciption;
                        ogtitle.Content = Titlte;
                        ogdes.Content = Desciption;
                        ogtype.Content = Titlte;
                        ogurl.Content = HttpContext.Current.Request.Url.OriginalString;
                        ogtwdes.Content = Desciption;
                        ogtwtitle.Content = Titlte;
                        igurl.Content = urlHinhAnh;
                        ogtwimg.Content = urlHinhAnh;
                        ogimage.Content = urlHinhAnh;
						  Meta1.Content = Desciption;


                        metaTitle = tableTag.Rows[0]["Titlte"].ToString();


                        string metaU = HttpContext.Current.Request.Url.OriginalString;
                        string name = "name";
                        string mainEntityOfPage = "mainEntityOfPage";
                        string idd = "id";
                        string context = "@context";
                        string type = "@type";
                        string iddd = "@id";
                        string hl = "headline";
                        string d = "description";
                        string ig = "image";
                        string ul = "url";
                        string con = "@context";
                        string t = "@type";
                        string dp = "datePublished";
                        string dm = "dateModified";
                        string au = "author";
                        string na = "name";
                        string c = "@context";
                        string tp = "@type";
                        string pb = "publisher";
                        string lg = "logo";
                        string width = "width";
                        string he = "height";
                        string hea = "application/ld+json";
                        string urlchu = "https://tungtang.com.vn";
                        string paa = "WebPage";
                        string imgo = "ImageObject";
                        string per = "Person";
                        string og = "Organization";
                        string news = "NewsArticle";
                        string chema = "https://schema.org";
                        string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                        string Crea = "CreativeWorkSeries";
                        Page.Header.Controls.AddAt(1,
                                   new LiteralControl(
                                       "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                                   )
                                   );
                    }
                }
                else if (idDanhMucCap2New != "" && Tinh != "")
                {
                    DataTable tableTag = Connect.GetTable(@"select    Titlte
                          ,MoTa,LinkIcon
                          from tb_DanhMucCap2 where idDanhMucCap2='" + idDanhMucCap2New + "' ");
                    TenDMC2 = StaticData.getField("tb_DanhMucCap2", "TenDanhMucCap2", "idDanhMucCap2", idDanhMucCap2New);
                    if (tableTag.Rows.Count > 0)
                    {
                        string urlHinhAnh = "";
                        string Titlte = "";
                        string Desciption = "";
                        urlHinhAnh = Domain + tableTag.Rows[0]["LinkIcon"].ToString();
                        if (QuanHuyen == "" && PhuongXa == "")
                        {
                            Titlte = "Rao vặt " + TenDMC2 + " " + Tinh + " miễn phí";
                            Desciption = "Rao vặt " + TenDMC2 + " " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                        }
                        else if (QuanHuyen != "" && PhuongXa == "")
                        {
                            Titlte = "Rao vặt " + TenDMC2 + " " + QuanHuyen + ", " + Tinh + " miễn phí";
                            Desciption = "Rao vặt " + TenDMC2 + " " + QuanHuyen + ", " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                        }
                        else if (QuanHuyen != "" && PhuongXa != "")
                        {
                            Titlte = "Rao vặt " + TenDMC2 + " " + PhuongXa + ", " + QuanHuyen + ", " + Tinh + " miễn phí";
                            Desciption = "Rao vặt " + TenDMC2 + " " + PhuongXa + ", " + QuanHuyen + ", " + Tinh + " miễn phí toàn quốc, đăng tin nhanh, duyệt bài trong vòng 24 giờ.";
                        }
                        title.Text = Titlte;
                        description.Content = Desciption;
                        ogtitle.Content = Titlte;
                        ogdes.Content = Desciption;
                        ogtype.Content = Titlte;
                        ogurl.Content = HttpContext.Current.Request.Url.OriginalString;
                        ogtwdes.Content = Desciption;
                        ogtwtitle.Content = Titlte;
                        igurl.Content = urlHinhAnh;
                        ogtwimg.Content = urlHinhAnh;
                        ogimage.Content = urlHinhAnh;
						  Meta1.Content = Desciption;


                        metaTitle = tableTag.Rows[0]["Titlte"].ToString();


                        string metaU = HttpContext.Current.Request.Url.OriginalString;
                        string name = "name";
                        string mainEntityOfPage = "mainEntityOfPage";
                        string idd = "id";
                        string context = "@context";
                        string type = "@type";
                        string iddd = "@id";
                        string hl = "headline";
                        string d = "description";
                        string ig = "image";
                        string ul = "url";
                        string con = "@context";
                        string t = "@type";
                        string dp = "datePublished";
                        string dm = "dateModified";
                        string au = "author";
                        string na = "name";
                        string c = "@context";
                        string tp = "@type";
                        string pb = "publisher";
                        string lg = "logo";
                        string width = "width";
                        string he = "height";
                        string hea = "application/ld+json";
                        string urlchu = "https://tungtang.com.vn";
                        string paa = "WebPage";
                        string imgo = "ImageObject";
                        string per = "Person";
                        string og = "Organization";
                        string news = "NewsArticle";
                        string chema = "https://schema.org";
                        string imoc = "https://tungtang.com.vn/images/icons/logo.png";
                        string Crea = "CreativeWorkSeries";
                        Page.Header.Controls.AddAt(1,
                                   new LiteralControl(
                                       "<script type=" + '"' + hea + '"' + ">{" + '"' + con + '"' + ": " + '"' + chema + '"' + "," + '"' + t + '"' + ": " + '"' + Crea + '"' + "," + '"' + na + '"' + ": " + '"' + metaTitle + '"' + "," + '"' + "aggregateRating" + '"' + ": {" + '"' + t + '"' + ": " + '"' + "AggregateRating" + '"' + " , " + '"' + "ratingValue" + '"' + ": 5," + '"' + "bestRating" + '"' + ": 5, " + '"' + "ratingCount" + '"' + ": 7}}</script> "
                                   )
                                   );
                    }
                }

                if (Url.ToUpper().Contains("TDTT"))
                {
                    string[] tachurl = Url.ToUpper().Split('/');
                    string[] tach2 = tachurl[2].Split('-');
                    string idTinTuc = tach2[0];

                    DataTable tableTagTinTuc = Connect.GetTable(@"select    [Titlte]
                          ,[Desciption]
                          ,[metaCanonical]
                          ,[metarobots]
                          ,[metaOpenGraph]
                          ,[metatwitter] from tb_TinTuc where idTinTuc='" + idTinTuc + "' ");

                    string Titlte = boSpace(tableTagTinTuc.Rows[0]["Titlte"].ToString());
                    string Desciption = boSpace(tableTagTinTuc.Rows[0]["MoTa"].ToString());
                    string Ismetarobots = boSpace(tableTagTinTuc.Rows[0]["metarobots"].ToString());
                    string metatwitter = boSpace(tableTagTinTuc.Rows[0]["metatwitter"].ToString());
                    string metaCanonical = boSpace(tableTagTinTuc.Rows[0]["metaCanonical"].ToString());
                    string metaOpenGraph = boSpace(tableTagTinTuc.Rows[0]["metaOpenGraph"].ToString());

                    title.Text = Titlte;
                    description.Content = Desciption;
					Meta1.Content = Desciption;



                    var keywords = new HtmlMeta { Name = "Canonical", Content = "" + metaCanonical };
                    this.head.Controls.Add(keywords);


                    var keywords2 = new HtmlMeta { Name = "twitter", Content = "" + metatwitter };
                    this.head.Controls.Add(keywords2);


                    var keywords3 = new HtmlMeta { Name = "Open Graph", Content = "" + metaOpenGraph };
                    this.head.Controls.Add(keywords3);


                    var keywords4 = new HtmlMeta { Name = "robots", Content = "" + Ismetarobots };
                    this.head.Controls.Add(keywords4);
                }

            }
            catch
            {

            }

        }
    }

    public void loadBalance()
    {
        string accountId = Utilities.Auth.getAccountId(Request);
        this.balance = Services.Account.getBalance(accountId);
    }
    
    public string boSpace(string str)
    {
        if (str.Replace(" ", "") == "")
            str = "TungTang.Com";

        return str;
    }
    private void LoadDangNhap()
    {
        if (Request.Cookies["TungTang_Login"] != null && Request.Cookies["TungTang_Login"].Value.Trim() != "")
        {
            string html = "";
			string htmluser = "";
            string idThanhVien = Request.Cookies["TungTang_Login"].Value.ToString();
            string sqlThanhVien = "select * from tb_ThanhVien where idThanhVien='" + idThanhVien + "'";
            DataTable tbThanhVien = Connect.GetTable(sqlThanhVien);
            if (tbThanhVien.Rows.Count > 0)
            {
				   if ((tbThanhVien.Rows[0]["StsPro"].ToString() == "1") || (tbThanhVien.Rows[0]["StsPro"].ToString().Trim() == "0" && tbThanhVien.Rows[0]["idAdmin_Duyet"].ToString().Trim() == "-1"))
                {
                    htmluser += " <a href='/thong-tin/pro' class='acc-color acc-link-hover'>Chỉnh sửa thông tin</a>&ensp;";
                    htmluser += " <a href='/doi-mat-khau/dmk' class='acc-color acc-link-hover' style='color: mediumblue;'>Đổi mật khẩu</a>";
                }
                else
                {
                    htmluser += " <a href='/thong-tin/tt' class='acc-color acc-link-hover'>Chỉnh sửa thông tin</a>&ensp;";
                    htmluser += " <a href='/doi-mat-khau/dmk' class='acc-color acc-link-hover' style='color: mediumblue;'>Đổi mật khẩu</a>";
                }
				UserLinkPf.InnerHtml = htmluser;
                isAuth = true;
                txtUser.InnerText = tbThanhVien.Rows[0]["TenDangNhap"].ToString();
                dvQuanLyTinDang.Visible = true;
                dvUser.Visible = true;
                dvAccOption.Visible = true;
                imgAccThumbnail.Src = Utilities.Account.getLinkThumbWithPath(tbThanhVien.Rows[0]["LinkAnh"].ToString());
                if(tbThanhVien.Rows[0]["TenCuaHang"].ToString() != ""){
                    spanAccTitle.InnerText = tbThanhVien.Rows[0]["TenCuaHang"].ToString() + " (" + tbThanhVien.Rows[0]["TenDangNhap"].ToString() + ")";
                }
                else
                {
                    spanAccTitle.InnerText = tbThanhVien.Rows[0]["TenDangNhap"].ToString();
                }
                spanAccCode.InnerText = tbThanhVien.Rows[0]["Code"].ToString();
            }
            dvDangKy.Visible = false;
            dvDangNhap.Visible = false;
        }
        else
        {
            dvQuanLyTinDang.Visible = false;
            dvAccOption.Visible = false;
            dvUser.Visible = false;
        }
    }
    protected void GFake_ThongTinCaNhanCopy_ModalApprove_Submit_Click(object sender, EventArgs e)
    {
        string ownerId = null;
        try
        {
            ownerId = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch
        {
            Response.Redirect("/dang-nhap/dn");
            return;
        }
        string postId = GFake_ThongTinCaNhanCopy_ModalApprove_PostId.Text;
        string postRate = GFake_ThongTinCaNhanCopy_ModalApprove_PostRate.Text.Trim().Replace(" ", "").Replace(",", "");
        string postAmount = GFake_ThongTinCaNhanCopy_ModalApprove_PostAmount.Text.Trim().Replace(" ", "").Replace(",", "");
        string collabId = GFake_ThongTinCaNhanCopy_ModalApprove_CollabId.Text;
        if (
            !string.IsNullOrWhiteSpace(ownerId) &&
            !string.IsNullOrWhiteSpace(postId) &&
            !string.IsNullOrWhiteSpace(postRate) &&
            !string.IsNullOrWhiteSpace(postAmount) &&
            !string.IsNullOrWhiteSpace(collabId)
        )
        {
            int referralRateValue; double referralAmountValue;
            try
            {
                referralRateValue = Int32.Parse(postRate);
                if(referralRateValue < 0)
                {
                    referralRateValue = 0;
                }
            }
            catch (Exception ex)
            {
                referralRateValue = 0;
            }
            try
            {
                referralAmountValue = Double.Parse(postAmount);
                if (referralAmountValue < 0)
                {
                    referralAmountValue = 0;
                }
            }
            catch (Exception ex)
            {
                referralAmountValue = 0;
            }
            if(referralRateValue > 0 && referralAmountValue > 0)
            {
                referralRateValue = 0;
            }
            Utilities.Referral.ownerApproveReferral(ownerId, collabId, postId, referralRateValue, referralAmountValue);
        }
        Response.Redirect("/thong-tin-ca-nhan/ttcn");
    }
    protected void GFake_ThongTinCaNhanCopy_BtnDump_Submit_Click(object sender, EventArgs e)
    {
        string ownerId = null;
        try
        {
            ownerId = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch
        {
            Response.Redirect("/dang-nhap/dn");
            return;
        }
        string postId = GFake_ThongTinCaNhanCopy_BtnDump_PostId.Text;
        if (
            !string.IsNullOrWhiteSpace(ownerId) &&
            !string.IsNullOrWhiteSpace(postId)
        )
        {
            string sql1 = "UPDATE tb_TinDang SET tb_TinDang.isHetHan = 1 WHERE tb_TinDang.idThanhVien = '"+ ownerId + "' AND tb_TinDang.idTinDang = '"+ postId + "'";
            Connect.Exec(sql1);
        }
        Response.Redirect("/thong-tin-ca-nhan/ttcn");
    }
    protected void GFake_ThongTinCaNhanCopy_ModalUpdateReferral_Submit_Click(object sender, EventArgs e)
    {
        string ownerId = null;
        try
        {
            ownerId = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch
        {
            Response.Redirect("/dang-nhap/dn");
            return;
        }
        string referralId = GFake_ThongTinCaNhanCopy_ModalUpdateReferral_ReferralId.Text;
        string collabId = GFake_ThongTinCaNhanCopy_ModalUpdateReferral_CollabId.Text;
        string postId = GFake_ThongTinCaNhanCopy_ModalUpdateReferral_PostId.Text;
        string referralRate = GFake_ThongTinCaNhanCopy_ModalUpdateReferral_ReferralRate.Text.Trim().Replace(" ", "").Replace(",", "");
        string referralAmount = GFake_ThongTinCaNhanCopy_ModalUpdateReferral_ReferralAmount.Text.Trim().Replace(" ", "").Replace(",", "");

        if (
            !string.IsNullOrWhiteSpace(ownerId) &&
            !string.IsNullOrWhiteSpace(referralId) &&
            !string.IsNullOrWhiteSpace(collabId) &&
            !string.IsNullOrWhiteSpace(referralRate) &&
            !string.IsNullOrWhiteSpace(referralAmount)
        )
        {
            int referralRateValue; double referralAmountValue;
            try
            {
                referralRateValue = Int32.Parse(referralRate);
                if(referralRateValue < 0)
                {
                    referralRateValue = 0;
                }
            }
            catch (Exception ex)
            {
                referralRateValue = 0;
            }
            try
            {
                referralAmountValue = Double.Parse(referralAmount);
                if (referralAmountValue < 0)
                {
                    referralAmountValue = 0;
                }
            }
            catch (Exception ex)
            {
                referralAmountValue = 0;
            }
            if(referralRateValue > 0 && referralAmountValue > 0)
            {
                referralRateValue = 0;
            }
            Utilities.Referral.ownerUpdateReferral(ownerId, referralId, postId, referralRateValue, referralAmountValue);
        }
        Response.Redirect("/thong-tin-ca-nhan/ttcn");
    }
    protected void GFake_ThongTinCaNhanCopy_OwnerCancelReferral_Submit_Click(object sender, EventArgs e)
    {
        string ownerId = null;
        try
        {
            ownerId = Request.Cookies["TungTang_Login"].Value.Trim();
        }
        catch
        {
            Response.Redirect("/dang-nhap/dn");
            return;
        }
        string referralId = GFake_ThongTinCaNhanCopy_OwnerCancelReferral_ReferralId.Text;
        if (
            !string.IsNullOrWhiteSpace(ownerId) &&
            !string.IsNullOrWhiteSpace(referralId)
        )
        {
            Utilities.Referral.ownerCancelReferral(ownerId, referralId);
        }
        Response.Redirect("/thong-tin-ca-nhan/ttcn");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string TenCuaHang = txtSearch.Value.Trim();
        string url = "/tin-dang/td?";
        if (TenCuaHang != "")
            url += "CH=" + TenCuaHang + "&";
        Response.Redirect(url);
    }
    protected void btnTimKiem_Click(object sender, EventArgs e) {}


    protected void GFake_MasterPageNew_VnpSubmit_Submit_Click(object sender, EventArgs e)
    {
        DataRow account = Utilities.Auth.getAccount(Request);
        if (account == null)
        {
            Response.Redirect("/Error404.aspx");
            return;
        }
        string code = GFake_MasterPageNew_VnpSubmit_Code.Text;

        DataRow pack = Services.RechargePackages.findByCode(code);
        if(pack == null)
        {
            Utilities.Redirect.notFound(Response);
        }

        long price = long.Parse(pack["RechargePrice"].ToString());
        long actualAmount = long.Parse(pack["ActualAmount"].ToString());
        string id = AppVnPay.VnPayService.createTransaction();
        AppVnPay.VnPayService.sendTransaction(Response, id, price, actualAmount, "thanh toan");
    }
}
