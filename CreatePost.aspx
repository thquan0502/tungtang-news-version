﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="CreatePost.aspx.cs" Inherits="DangTin"  EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <%-- ckeditor --%>
    <script src="/asset/vendors/ckeditor/ckeditor.js"></script>

    <link href="../Css/Page/TinDang.css" rel="stylesheet" />
    <style>
        @media (max-width:600px) {
        }
        @media (min-width:992px) {
            .PrevTin
            {                
            }
            .PostTin{

            }
        }
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
            box-shadow: none;
        }

        #ContentPlaceHolder1_txtLoaiDanhMuc[readonly] {
            background-color: white;
            box-shadow: none;
        }
		@media(max-width: 750px){
            .margin75{
                    margin-bottom: -32px;

            }
            .margind{
                 margin-top: -15px;
            }
        }
        .bgpri{
            background:#cce5ff;
        }
    </style>
    <script src="../Js/Page/DangTin.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/asset/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/asset/vendors/bootstrap-datepicker/locales/bootstrap-datepicker.vi.min.js"></script>
    <script>
        const g_idTinDang = "<%= idTinDang %>";
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px; overflow: auto;margin-top:40px;">

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal-DiaDiem-cls">
            <div id="modalDanhMuc" class="modal" style="overflow: hidden;">
                <div id="dvDSDanhMuc" style="overflow: auto;">
                    <table class="table table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackDanhMuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn danh mục</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDanhMuc_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="job-list scrollbar m-t-lg" style="margin-top: 50px;">
            <h4 class="text-center">ĐĂNG TIN</h4>
            <div>
                <div id="dvThongBao" runat="server" class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="background: #eea751; padding: 3px 6px; border-radius: 3px; color: white; border: 1px solid #FF9800;">
                        Bạn nên đăng nhập để quản lý được tin đăng
                    </div>
                </div>
                <div class="form-group" style="height:38px !important">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Danh mục<span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtLoaiDanhMuc" value="Chọn danh mục" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDanhMuc()" />
                                    <input id="slLoaiDanhMuc" runat="server" style="padding-left: 0; padding-right: 0; display: none;" value="0" />
                                    <input id="slLoaiDanhMucCap2" runat="server" style="padding-left: 0; padding-right: 0; display: none;" value="0" />
                                    <div id="dvLinhVuc" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="form-group" style="height:38px !important">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Khu vực:
                                </td>
                                <td>
                                    <select id="ddlTinh" runat="server" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtTinhThanh" name="name" value="0" runat="server" style="display: none;" />
                                    <select id="slHuyen" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" style="display: none;" />
                                    <select id="slXa" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtPhuongXa" name="name" value="" runat="server" style="display: none;" />

                                    <input type="text" id="txtDiaDiem" value="Toàn quốc" tabindex="1" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDiaDiem();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="height:38px !important">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Giá:
                                </td>
                                <td>
                                    <input type="text" id="txtTuGia" placeholder="VNĐ" class="form-control" runat="server" oninput='DinhDangTien(this.id)' onkeypress='onlyNumber(event)' />
                                    <div id="divErrorGia" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div id="divHasCommission" style="margin-bottom:10px;">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">
                                    
                                </td>
                                <td>
                                    <div class="row margind">
                                        <div class="col-md-12" style="border-radius: 5px;">
                                            <div class="p-3 mb-1 bg-success" style="border-radius: 5px;margin-top: 35px;">
                                                <p><i class="fa fa-lightbulb-o" style="font-size: 150%;"></i> <i>Bằng cách <strong>hợp tác với Cộng tác viên</strong>, tin đăng của bạn sẽ được lan tỏa tốt hơn. Hãy đặt mức <strong>hoa hồng</strong> phù hợp để khuyến khích Cộng tác viên chia sẻ ngay !</i></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="border-radius: 5px; margin-bottom: 5px; margin-top:10px;">
                                            <div class="d-flex align-items-center">
                                                <input id="inputHasCommission" runat="server" name="checkCommission" type="radio" class="form-control" checked style="display: inline-block; max-width: 15px; height: unset;margin-top: 0;" />
                                                <label for="inputHasCommission" style="margin-bottom: 0;white-space: nowrap;">&nbsp;&nbsp;Áp dụng chia sẻ hoa hồng với CTV ?</label>   
                                            </div>
                                            </div>
                                            <div class="col-12" style="margin-bottom: 10px;">
                                            <div id="inputnon">
                                                <input id="inputUnCommission" runat="server" type="radio" name="checkCommission" class="form-control" style="display: inline-block;max-width: 15px; height: unset;margin-top: 0;" />
                                                <label for="inputUnCommission" style="margin-bottom: 0;white-space: nowrap;">&nbsp;Không áp dụng</label>
                                                 </div>
                                        </div>
                                            <div id="dvContainerReferral" runat="server">
                                                <div class="col-md-11 col-md-offset-1 margin75">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td class="nametd" style="width: 100px;">
                                                                <label class="control-label">Hoa hồng:</label>
                                                            </td>
                                                            <td>
                                                                <div class="row mb-4">
                                                                    <div class="col-md-4 col-xs-10 col-sm-10 referral-input-wrap">
                                                                        <div class="form-group mb-0" style="position: relative;">
                                                                            <label>Tỉ lệ hoa hồng/giá</label>
                                                                            <div class="input-group main-gr">
											                                    <asp:TextBox ID="txtReferralRate" runat="server" type="text" CssClass="form-control referral-input" value="0" step="1"  min="0"/>
											                                    <span class="input-group-addon">
											                                        %
											                                    </span>
										                                    </div>
                                                                            <div class="layer-wartermark"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-xs-10 col-sm-10 referral-input-wrap">
                                                                        <div class="form-group mb-0" style="position: relative;">
                                                                            <label>Hoa hồng cố định</label>
                                                                            <div class="input-group main-gr">
											                                    <asp:TextBox ID="txtReferralAmount" runat="server" type="text" CssClass="form-control referral-input" value="0" step="1" min="0" />
											                                    <span class="input-group-addon">
											                                        VNĐ
											                                    </span>
										                                    </div>
                                                                            <div class="layer-wartermark"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div id="divErrorCommission" runat="server" style="color: red;"></div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div><br /><br />
                                        </div>
                                            </div>
                                        </div>
                                        <div class="col-12" id="divArlertCommission" style="margin-top: -5px;margin-bottom:5px;">
                                            <div class="p-3 mb-1 bgpri" id="divAlert" runat="server" style="border-radius: 5px;">
                                               
                                            </div>
                                        </div>

                                        
                                        <div class="col-md-12">
                                            <div id="divMessCommission" style="color: red;" runat="server"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                

                <div class="form-group" style="display: none;">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd"></td>
                                <td class="nametd">
                                    <label for="ContentPlaceHolder1_rdGiaoHangMatPhi">Giao hàng mất phí</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="rdGiaoHangMatPhi" runat="server" type="radio" checked name="GiaoHang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_rdGiaoHangKhongMatPhi">Giao hàng không mất phí</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="rdGiaoHangKhongMatPhi" runat="server" type="radio" name="GiaoHang" style="margin-top: 3px;" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;"></td>
								<td style="width: 50px;">
                                    <input id="radCanBan" runat="server" type="radio" checked name="LoaiTinDang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_radCanBan">Bạn đang cần bán</label>  
                                </td>
								<td></td>
                                <td style="width: 50px;">
                                    <input id="radCanMua" runat="server" type="radio" name="LoaiTinDang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_radCanMua">Bạn đang cần mua</label>
                                </td>
                                
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">
                            THÔNG TIN LIÊN HỆ
                        </div>
                        <div>
                            <table style="width: 100%; margin-top: 10px;">
                                <tr>
                                    <td class="nametd" style="padding-top: 4px;">
                                        <input type="checkbox" id="ckbLayThongTinDangKy" onclick="LayThongTinDangKy()" />
                                    </td>
                                    <td>Lấy thông tin đăng ký
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Họ tên <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtHoTen" name="form[password]" placeholder="Họ tên" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvHoTen" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Số điện thoại <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtSoDienThoai" name="form[password]" placeholder="Số điện thoại" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvSoDienThoai" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Email:
                                </td>
                                <td>
                                    <input type="text" id="txtEmail" name="form[password]" placeholder="Email" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvEmail" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Địa chỉ:
                                </td>
                                <td>
                                    <input type="text" id="txtDiaChi" name="form[password]" placeholder="Số nhà, Đường, Phường - Quận huyện - Tỉnh thành" tabindex="2" class="form-control" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group margind">
                    <div class="col-md-10 col-md-offset-1">
                        <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">NỘI DUNG TIN ĐĂNG</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Tiêu đề <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtTieuDe" name="form[password]" placeholder="Tiêu đề" class="form-control" runat="server" />
                                    <div id="dvTieuDe" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="height: auto;margin-top:5px;">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="vertical-align: baseline;width: 100px;">Nội dung <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <%--  <CKEditor:CKEditorControl  Height="300" runat="server"></CKEditor:CKEditorControl> 
                                         <CKFinder:FileBrowser ID="FileBrowser1"   Width    ="0" Height="0" runat="server" OnLoad="FileBrowser1_Load"></CKFinder:FileBrowser>--%>

                                    <textarea runat="server" id="txtNoiDung" style="height: 200px; resize: none;width:100%;"  class="form-control"> </textarea>
                                    <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvConHinhAnh" class="form-group margin75">
                    <div class="col-md-10 col-md-offset-1">
                         <input type="hidden" id="hdHinhAnh" runat="server" />
                       <%-- <asp:HiddenField id="hdHinhAnh" runat="server" />--%>
                        <table style="width: 100%;">
                            <tr>
                               
                                <td class="nametd" style="width: 100px;">Thêm hình ảnh:</td>
                                <td>
                                    <div class="BgUploadAnh">
                                        <div>
                                            <a class="btn btn-info btn-sm" onclick="javascript:document.getElementById('fileHinhAnh').click();" style="margin-top:10px;">THÊM HÌNH</a>
                                            <asp:FileUpload ID="fileHinhAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" AllowMultiple="true" Style="display: none;" />
                                        </div>
                                        <div style="padding: 0px 0px; height: auto; overflow: hidden;margin-top:10px;" id="dvHinhAnh" runat="server">
                                        
                                        </div>
                                    </div>
                                     <div id="DivmessageImg" class="alert alert-warning" style="margin-top:3px; border-radius:5px; font-style: italic;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Khuyến cáo nên dùng <strong>hình ảnh thật</strong> của sản phẩm (Không được để <strong>nội dung và số điện thoại</strong> trên ảnh) và bắt buộc đăng ít nhất <strong>3 hình ảnh</strong></div>
                                    <div id="dvMessageHinhAnh" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div><br /><br />
                </div>
                 <div class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="height: auto;margin-top:5px;">
                        <table style="width: 100%;">
                            <tr>
                                 <td class="nametd" style="width: 100px;">
                                    
                                </td>
                                <td>
                                <div class="row margind" style="margin-top: -49px; margin-bottom: 8px;">
                                        <div class="col-md-12" style="border-radius: 5px;">
                                            <div class="p-3 mb-1 bg-success" style="border-radius: 5px;margin-top: 35px;">
                                                <p> <strong>Nâng cấp tài khoản PRO tiếp cận thêm nhiều khách hàng</strong><br />
                                                ✅ Nâng cấp tài khoản <strong>PRO</strong> ưu tiên duyệt tin đăng trong vòng 30 phút và hiển thị ngoài trang chủ và đầu danh mục giá 300k/ 1 tháng <br />
                                                ✅ Up tin đăng lên đầu trang với giá 1000đ/ 1 lần <br />
                                                ✅ Đặt banner quảng cáo tin đăng trang chủ giá 3tr/ 1 tháng <br />
                                                ✅ Quảng cáo Google Adword đưa tin đăng <strong>Top 1</strong> thứ hạng Google giúp nhanh chốt đơn!!! <a href="/blogtt/bang-gia-dich-vu-quang-cao-google-adword-2021" style="color: red;" target="_blank"> Xem thêm </a> <br />
                                                --------------------------------------<br />
                                                ⛔Liên hệ tư vấn ZALO: <a href='https://zalo.me/0906905757' target='_blank' style="color: red;font-weight: 600;">090 690 5757</a>
                                                    </p>
                                            </div>
                                        </div>
                                    </div>
                                    </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- Buttons-->
                <div class="form-group">
                    <div style="text-align: center">
                         <ul style="list-style-type:none">
                            <li style="display:inline-block">
                                <button type="button" id="btnPrev" class="btn btn-info btn-block" style="width: 110px; padding: 10px; margin: auto;" onclick="submitPreView()" >Xem Trước</button>
                            <asp:Button ID="btnPreview" runat="server" Style="display: none" OnClick="btnPreview_Click" />
                            </li>
                            <li style="display:inline-block">
                        <button type="button" id="btnDangTin" class="btn btn-primary btn-block" style="width: 110px; padding: 10px; margin: auto;" onclick="createPost_submitForm()" >Đăng Tin</button>
                        <asp:Button ID="btDangTinFake" runat="server" Style="display: none"  OnClick="btDangTinFake_Click" />
                             </li>
                        </ul>     
                    </div>
                </div>
        <input id="idloaimay" type="hidden" runat="server"/>
    </section>



    <!-- start modal choose post package -->
    <asp:TextBox ID="txtPostPackage" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtPostPeriod" runat="server" type="hidden"></asp:TextBox>
    <asp:TextBox ID="txtPostDays" runat="server" type="hidden"></asp:TextBox>
    <div id="modalChoosePostPackage" class="modal new-modal modal-choose-post-package">
        <h1 class="title">NÂNG CẤP TIN ĐĂNG</h1>
        <div class="row mb-2">
            <div class="col-md-3">
                <label>Gói tin</label>
            </div>
            <div class="col col-md-9">
                <select class="form-control select-package">
                </select>
                <p class="text-success mt-2">Chọn <strong>gói tin</strong> thích hợp giúp cho bài tin của bạn được hiển thị tốt hơn trên nền tảng <strong>tungtang.com.vn</strong></p>
            </div>
        </div>
        <div class="row mb-2 period-wrap hide">
            <div class="col-md-3">
                <label>Thời hạn</label>
            </div>
            <div class="col col-md-9">
                <select class="form-control select-period"></select>
            </div>
        </div>
        <div class="row mb-2 dates-wrap hide">
            <div class="col-md-3">
            </div>
            <div class="col col-md-9">
                <div class="input-group input-large input-date-picker input-daterange " data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control input-date-from"/>
					<span class="input-group-addon"> đến </span>
					<input type="text" class="form-control input-date-to"/>
				</div>
                <p class="text-success mt-2">Sau khi hết thời hạn, tin đăng vẫn sẽ được hiển thị và trở thành loại tin đăng bình thường</p>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-3">
            </div>
            <div class="col col-md-9 submit-wrap">
                <button type="button" class="btn btn-primary submit-btn">Hoàn thành</button>
            </div>
        </div>
    </div>
    <script>
        const CHOOSE_POST_FEE_CREATE_POST = 1;
        <% if (postFees != null) { %>
        const modalChoosePostPackage_packagesData = [
           <% for (int i = 0; i < postFees.Rows.Count; i = i + 1) { %>
            {
                code: "<%= postFees.Rows[i]["Code"].ToString() %>",
                name: "<%= postFees.Rows[i]["Name"].ToString() %>",
                pricePer1Days: parseInt("<%= postFees.Rows[i]["PricePer1Days"].ToString() %>"),
                pricePer7Days: parseInt("<%= postFees.Rows[i]["PricePer7Days"].ToString() %>"),
                pricePer14Days: parseInt("<%= postFees.Rows[i]["PricePer14Days"].ToString() %>"),
                pricePer30Days: parseInt("<%= postFees.Rows[i]["PricePer30Days"].ToString() %>"),
            },
            <% } %>
        ];
        <% } %>
    </script>
    <!-- end modal choose post package -->

    <script type="text/javascript">

        //check isErrorCommission
       <%-- (function () {
            var isErrorCommission = Boolean(<%= isErrorCommission ? '1' : '0' %>);
            console.log(isErrorCommission);
            if (isErrorCommission == 'true') {
                $("#ContentPlaceHolder1_divErrorCommission").show();
            }
            else {
                $("#ContentPlaceHolder1_divErrorCommission").hide();
            }
           
        })();--%>

        //(function() {
        //    $(document).ready(function () {
        //        setTimeout(function () {
        //            var str = '';
        //            $('.uploaded-img').each(function () {
        //                var name = $(this).data('name');
        //                str += name + '|~~~~|';
        //            });
        //            $('#ContentPlaceHolder1_hdHinhAnh').val(str);
        //        }, 500);
        //    });
        //})();

        $('input[type="checkbox"]').on('change', function () {
            $('input[type="checkbox"]').not(this).prop('checked', false);
        });
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        (function () {
            var rate = $('#ContentPlaceHolder1_txtReferralRate');
            var rateWrap = rate.parents('.referral-input-wrap');
            var rateLayer = rateWrap.find('.layer-wartermark');
            var amount = $('#ContentPlaceHolder1_txtReferralAmount');
            var amountWrap = amount.parents('.referral-input-wrap');
            var amountLayer = amountWrap.find('.layer-wartermark');

            var fnFormatCurrency = function (amount) {
                amount = parseInt(amount);
                if (isNaN(amount)) {
                    amount = 0;
                }
                amount = '' + amount;
                var rs = "";
                do {
                    var t = amount.substr(-3);
                    if (t.length == 3 && amount.length > 3) {
                        rs = "," + t + rs;
                    } else {
                        rs = t + rs;
                    }
                    amount = amount.substr(0, amount.length - t.length)
                } while (amount.length > 0);
                return rs;
            };

            var fnCheck = function () {
                var rateVal = rate.val();
                rateVal = parseFloat(rateVal.replaceAll(" ", "").replaceAll(",", ""));
                var amountVal = amount.val();
                amountVal = parseFloat(amountVal.replaceAll(" ", "").replaceAll(",", ""));

                if (!isNaN(rateVal) && rateVal == 0 && !isNaN(amountVal) && amountVal == 0) {
                    rateLayer.hide();
                    rate.val(0);
                    amountLayer.hide();
                    amount.val(0);
                } else if (!isNaN(rateVal) && rateVal > 0) {
                    rate.val(rateVal);
                    rateLayer.hide();
                    amount.val(0);
                    amountLayer.show();
                } else if (!isNaN(amountVal) && amountVal > 0) {
                    amount.val(fnFormatCurrency(amountVal));
                    amountLayer.hide();
                    rate.val(0);
                    rateLayer.show();
                } else {
                    rate.val(0);
                    rateLayer.hide();
                    amount.val(0);
                    amountLayer.hide();
                }
            }

            rate.on('change', function () {
                fnCheck();
            });
            rate.on('input', function () {
                fnCheck();
            });
            amount.on('change', function () {
                fnCheck();
            });
            amount.on('input', function () {
                fnCheck();
            });
            fnCheck();
        })();
        function submitPreView() {
            $('#ContentPlaceHolder1_btnPreview').click();
        }
        (function () {
            $("#ContentPlaceHolder1_inputHasCommission").change(function () {
                if (this.checked) {
                    $('#ContentPlaceHolder1_dvContainerReferral').show();
                    $('#divArlertCommission').show();
                   
                } else {
                    $('#ContentPlaceHolder1_txtReferralRate').val(0);
                    $('#ContentPlaceHolder1_txtReferralAmount').val(0);
                    $('#ContentPlaceHolder1_txtReferralAmount').trigger('input');
                    $('#ContentPlaceHolder1_dvContainerReferral').hide();
                    $('#divArlertCommission').hide();
                }
            });
            $("#ContentPlaceHolder1_inputUnCommission").change(function (){
                if (this.checked)
                {
                    $('#ContentPlaceHolder1_txtReferralRate').val(0);
                    $('#ContentPlaceHolder1_txtReferralAmount').val(0);
                    $('#ContentPlaceHolder1_txtReferralAmount').trigger('input');
                    $('#ContentPlaceHolder1_dvContainerReferral').hide();
                    $('#divArlertCommission').hide();
                }
                else {
                    $('#ContentPlaceHolder1_dvContainerReferral').show();
                    $('#divArlertCommission').show();
                }
            });
        })();
    </script>
</asp:Content>


