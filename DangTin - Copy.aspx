﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageNew.master" AutoEventWireup="true" CodeFile="DangTin - Copy.aspx.cs" Inherits="DangTin"  EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link href="../Css/Page/TinDang.css" rel="stylesheet" />
    <style>
        #ContentPlaceHolder1_txtDiaDiem[readonly] {
            background-color: white;
            box-shadow: none;
        }

        #ContentPlaceHolder1_txtLoaiDanhMuc[readonly] {
            background-color: white;
            box-shadow: none;
        }
    </style>
    <script src="../Js/Page/DangTin.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="feature container" style="background-color: white; border-radius: 3px; overflow: auto;margin-top:40px;">

        <div class="modal-DiaDiem-cls">
            <div id="modalDiaDiem" class="modal" style="overflow: hidden;">
                <div id="dvDSDiaDiem" style="overflow: auto;">
                    <table class="table table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackKhuVuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn khu vực</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDiaDiem_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal-DiaDiem-cls">
            <div id="modalDanhMuc" class="modal" style="overflow: hidden;">
                <div id="dvDSDanhMuc" style="overflow: auto;">
                    <table class="table table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th>
                                    <div style="float: left; padding: 2px; display: none; cursor: pointer;" id="btnBackDanhMuc"><i class="fa fa-arrow-left"></i></div>
                                    <div style="text-align: center; font-size: 17px;">Chọn danh mục</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbDanhMuc_tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="job-list scrollbar m-t-lg">
            <h4 class="text-center">ĐĂNG TIN</h4>
            <div>
                <div id="dvThongBao" runat="server" class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="background: #eea751; padding: 3px 6px; border-radius: 3px; color: white; border: 1px solid #FF9800;">
                        Bạn nên đăng nhập để quản lý được tin đăng
                    </div>
                </div>
                <div class="form-group" style="height:38px !important">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Danh mục<span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtLoaiDanhMuc" value="Chọn danh mục" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDanhMuc()" />
                                    <input id="slLoaiDanhMuc" runat="server" style="padding-left: 0; padding-right: 0; display: none;" value="0" />
                                    <input id="slLoaiDanhMucCap2" runat="server" style="padding-left: 0; padding-right: 0; display: none;" value="0" />
                                    <div id="dvLinhVuc" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="form-group" style="height:38px !important">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Khu vực:
                                </td>
                                <td>
                                    <select id="ddlTinh" runat="server" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtTinhThanh" name="name" value="0" runat="server" style="display: none;" />
                                    <select id="slHuyen" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtQuanHuyen" name="name" value="" runat="server" style="display: none;" />
                                    <select id="slXa" runat="server" disabled="disabled" style="padding-left: 0; padding-right: 0; display: none;"></select>
                                    <input type="text" id="txtPhuongXa" name="name" value="" runat="server" style="display: none;" />

                                    <input type="text" id="txtDiaDiem" value="Toàn quốc" tabindex="1" class="form-control" runat="server" readonly style="cursor: pointer;" onclick="ShowDiaDiem();" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="height:38px !important">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Giá:
                                </td>
                                <td>
                                    <input type="text" id="txtTuGia" placeholder="VNĐ" class="form-control" runat="server" oninput='DinhDangTien(this.id)' onkeypress='onlyNumber(event)' />
                                    <div id="divErrorGia" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <div class="form-group" style="display: none;">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd"></td>
                                <td class="nametd">
                                    <label for="ContentPlaceHolder1_rdGiaoHangMatPhi">Giao hàng mất phí</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="rdGiaoHangMatPhi" runat="server" type="radio" checked name="GiaoHang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_rdGiaoHangKhongMatPhi">Giao hàng không mất phí</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="rdGiaoHangKhongMatPhi" runat="server" type="radio" name="GiaoHang" style="margin-top: 3px;" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;"></td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_radCanBan">Bạn đang cần bán</label>  
                                </td>
                                <td style="width: 50px;">
                                    <input id="radCanBan" runat="server" type="radio" checked name="LoaiTinDang" style="margin-top: 3px;" />
                                </td>
                                <td class="nametd" style="width: 175px;">
                                    <label for="ContentPlaceHolder1_radCanMua">Bạn đang cần mua</label>
                                </td>
                                <td style="width: 50px;">
                                    <input id="radCanMua" runat="server" type="radio" name="LoaiTinDang" style="margin-top: 3px;" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">
                            THÔNG TIN LIÊN HỆ
                        </div>
                        <div>
                            <table style="width: 100%; margin-top: 10px;">
                                <tr>
                                    <td class="nametd" style="padding-top: 4px;">
                                        <input type="checkbox" id="ckbLayThongTinDangKy" onclick="LayThongTinDangKy()" />
                                    </td>
                                    <td>Lấy thông tin đăng ký
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Họ tên <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtHoTen" name="form[password]" placeholder="Họ tên" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvHoTen" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Số điện thoại <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtSoDienThoai" name="form[password]" placeholder="Số điện thoại" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvSoDienThoai" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Email:
                                </td>
                                <td>
                                    <input type="text" id="txtEmail" name="form[password]" placeholder="Email" tabindex="2" class="form-control" runat="server" />
                                    <div id="dvEmail" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd">Địa chỉ:
                                </td>
                                <td>
                                    <input type="text" id="txtDiaChi" name="form[password]" placeholder="Số nhà, Đường, Phường - Quận huyện - Tỉnh thành" tabindex="2" class="form-control" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <div style="font-weight: bold; font-size: 18px; padding-top: 10px; color: #555;">NỘI DUNG TIN ĐĂNG</div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Tiêu đề <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <input type="text" id="txtTieuDe" name="form[password]" placeholder="Tiêu đề" class="form-control" runat="server" />
                                    <div id="dvTieuDe" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1" style="height: auto">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="vertical-align: baseline;width: 100px;">Nội dung <span style="color: red">(*)</span>:
                                </td>
                                <td>
                                    <%--  <CKEditor:CKEditorControl  Height="300" runat="server"></CKEditor:CKEditorControl> 
                                         <CKFinder:FileBrowser ID="FileBrowser1"   Width    ="0" Height="0" runat="server" OnLoad="FileBrowser1_Load"></CKFinder:FileBrowser>--%>

                                    <textarea runat="server" id="txtNoiDung" style="height: 200px; resize: none;width:100%;"  class="form-control"> </textarea>
                                    <div id="dvNoiDung" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvConHinhAnh" class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">Thêm hình ảnh:</td>
                                <td>
                                    <div class="BgUploadAnh">
                                        <div>
                                            <a class="btn btn-info btn-sm" onclick="javascript:document.getElementById('fileHinhAnh').click();">THÊM HÌNH</a>
                                            <asp:FileUpload ID="fileHinhAnh" ClientIDMode="Static" onchange="this.form.submit()" runat="server" AllowMultiple="true" Style="display: none;" />
                                        </div>
                                        <div style="padding: 0px 0px; height: auto; overflow: hidden;" id="dvHinhAnh" runat="server">
                                            <%--<div class="imgupload">
                                                   <p style="margin:0px"><img id="imgAnhCuaBan" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                   <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                               </div>
                                               <div class="imgupload">
                                                   <p style="margin:0px"><img id="img1" runat="server" src="http://icons.iconarchive.com/icons/gakuseisean/aire/256/Images-icon.png" style="width:100%; height:100%" /></p>
                                                   <p style="text-align:center;margin: 0px;padding: 1px;background: #d6d6d6;"><img src="../images/icons/delete.png" style="width:25px; height:25px" /></p>
                                               </div>--%>
                                        </div>
                                    </div>
                                    <span style="color: red">(*) Hình đầu tiên sẽ là hình đại diện của hàng hoá và bắt buộc đăng ít nhất 3 hình</span>
                                    <div id="dvMessageHinhAnh" style="color: red;" runat="server"></div>
                                </td>
                            </tr>
                        </table>
                    </div><br /><br />
                </div>
                <div id="dvContainerReferral">
                    <div class="col-md-10 col-md-offset-1">
                        <table style="width: 100%;">
                            <tr>
                                <td class="nametd" style="width: 100px;">
                                    <label class="control-label">Hoa hồng:</label>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-3 referral-input-wrap">
                                            <div class="form-group">
                                                <label>Tỉ lệ hoa hồng</label>
                                                <asp:TextBox ID="txtReferralRate" runat="server" type="number" CssClass="form-control referral-input" value="0" step="1"/>
                                                <asp:RangeValidator ID="txtReferralRateValidator" Display="Dynamic" runat="server" ControlToValidate="txtReferralRate" MinimumValue="0" MaximumValue="100" Type="Integer" Text="Giá trị không hợp lệ" EnableClientScript="false"></asp:RangeValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3 referral-input-wrap">
                                            <div class="form-group">
                                                <label>Hoa hồng cố định</label>
                                                <asp:TextBox ID="txtReferralAmount" runat="server" type="number" CssClass="form-control referral-input" value="0" step="1"/>
                                                <asp:RangeValidator ID="txtReferralAmountValidator" Display="Dynamic" runat="server" ControlToValidate="txtReferralAmount" MinimumValue="0" MaximumValue="10000000000" Type="Double" Text="Giá trị không hợp lệ" EnableClientScript="false"></asp:RangeValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3" id="dvAccountSelledWrap" runat="server">
                                            <div class="form-group">
                                                <label>CTV bán được</label>
                                                <asp:DropDownList ID="drdlAccountSelled" CssClass="form-control" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div><br /><br />
                </div>
                <!-- Buttons-->
                <div class="form-group">
                    <div style="text-align: center">
                        <button type="button" id="btnDangTin" class="btn btn-primary btn-block" style="width: 110px; padding: 10px; margin: auto;" onclick="submitForm()" >Đăng Tin</button>
                        <asp:Button ID="btDangTinFake" runat="server" Style="display: none"  OnClick="btDangTinFake_Click" />
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" id="hdHinhAnh" runat="server" />
    </section>
    <script type="text/javascript">
        function DinhDangTien(id) {
            var check = $('#' + id).val().replace(/\,/g, '');
            if (isNaN(check)) {
                $('#' + id).val("0");
            }
            else {
                $('#' + id).val($('#' + id).val().replace(/\,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace(/^\s+/, '').replace(/\s+$/, ''));
            }
        }
        function onlyNumber(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        $(document).ready(function(){
            var rate = $('#ContentPlaceHolder1_txtReferralRate');
            var rateWrap = rate.parents('.referral-input-wrap');
            var amount = $('#ContentPlaceHolder1_txtReferralAmount');
            var amountWrap = amount.parents('.referral-input-wrap');

            var fnCheck = function () {
                var rateVal = rate.val();
                rateVal = parseFloat(rateVal);
                var amountVal = amount.val();
                amountVal = parseFloat(amountVal);
                if (!isNaN(rateVal) && rateVal == 0 && !isNaN(amountVal) && amountVal == 0) {
                    rateWrap.slideDown();
                    amountWrap.slideDown();
                } else if (!isNaN(rateVal) && rateVal > 0) {
                    amount.val(0);
                    amountWrap.slideUp();
                } else if (!isNaN(amountVal) && amountVal > 0) {
                    rate.val(0);
                    rateWrap.slideUp();
                } else {
                    rateWrap.slideDown();
                    amountWrap.slideDown();
                }

                if (isNaN(rateVal)) {
                    rate.val(0);
                }
                if (isNaN(amountVal)) {
                    amount.val(0);
                }

            }
            rate.on('change', function () {
                fnCheck();
            });
            rate.on('input', function () {
                fnCheck();
            });
            amount.on('change', function () {
                fnCheck();
            });
            amount.on('input', function () {
                fnCheck();
            });
            fnCheck();
        });
        $(document).ready(function () {
            $('#ContentPlaceHolder1_drdlAccountSelled').select2();
        });
        function submitForm() {
            var val = $('#ContentPlaceHolder1_drdlAccountSelled').val();
            var textShow = $('#ContentPlaceHolder1_drdlAccountSelled option[value="'+val+'"]').text();
            if (val != 'None') {
                if (val == 'Me') {
                    textShow = 'chính mình';
                }
                var x = alertify.confirm("Bạn đã chọn ctv hỗ trợ thành công là " + textShow + " ?", function () {
                    $('#ContentPlaceHolder1_btDangTinFake').click();
                }, function () {
                    // do nothing
                }).set({ labels: { ok: 'Xác nhận', cancel: 'Không, chỉnh sửa lại' },title : "Xác nhận thay đổi"  });
                console.log(x);
            } else {
                $('#ContentPlaceHolder1_btDangTinFake').click();
            }
        }
    </script>
</asp:Content>

